#!/bin/bash
#Se no arquivo .env QUEUE_DRIVER=sync
#Levanta um servidor que somente aceita conexão local (127.0.0.1)
#Coloca o PID do processo na varável $PID
#php artisan serve --host=127.0.0.1 --port=8080 & PID=$!

#sleep 10

#Executa o cURL sem proxy e com tempo máximo de espera de 24h
#curl --noproxy "*" --max-time "86400" "http://127.0.0.1:8080/atualizar-cadastur"

#sleep 10

#Mata processo do servidor
#kill -9 $PID
#Se no arquivo .env QUEUE_DRIVER=database
#nohup php artisan queue:work --daemon --sleep=3 --tries=3 &
curl --max-time "86400" "http://192.168.105/atualizar-cadastur"