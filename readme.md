# Plataforma Integrada de Turismo (PIT)

A  Plataforma Integrada de Turismo é desenvolvida utilizando o framework Laravel

## como executar

1) instale o php e o composer
      
     apt install php7.2-cli php7.2-zip composer php7.2-mysql php7.2-mbstring php7.2-xml openssl php7.2-curl php7.2-gd curl wget

Obs: cada projeto pode utilizar e solicitar bibliotecas adicionais

2) instale os componentes do laravel

```shell
    composer global require laravel/installer
```

se estiver em uma rede corporativa pode ser necessario antes configurar 

```shell
    export http_proxy='http://usuario:senha@proxyurlorip:porta'
    export https_proxy='http://usuario:senha@proxyurlorip:porta'
    export HTTP_PROXY_REQUEST_FULLURI=0 
    export HTTPS_PROXY_REQUEST_FULLURI=0
```
e no arquivo /etc/wgetrc

    http_proxy = http://IPdoServidorProxy:PortadoProxy/
    proxy_user = UsuarioProxy
    proxy_passwd = SenhaUsuarioProxy
    use_proxy = on 

https_proxy = http://proxycamg.prodemge.gov.br:8080
proxy_user = m6667566   
proxy_passwd = Loi8uhg9
use_proxy = on 

3) Execute o servidor de desenvolvimento:
   
```shell    
    php artisan serve
```

4) Executar arquivo /var/www/descubramatogrosso/database/migrations/busca.sql

## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)


### Localização do routes.php

```
app\Http\routes.php
```
