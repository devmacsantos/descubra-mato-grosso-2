function validateAddress(p_Form)
{
    $('#cep').mask('99999999');
    var v_Return = true;
    if(p_Form['cep'].value === null || p_Form['cep'].value === '')
        v_Return = 'O campo de CEP deve ser preenchido';
    else if(p_Form['cidade'].value === null || p_Form['cidade'].value === '')
        v_Return = 'O campo de cidade deve ser preenchido';
    else if(p_Form['estado'].value === null || p_Form['estado'].value === '')
        v_Return = 'O campo de estado deve ser preenchido';
    else if(p_Form['logradouro'].value === null || p_Form['logradouro'].value === '')
        v_Return = 'O campo de logradouro deve ser preenchido';
    else if(p_Form['numero'].value === null || p_Form['numero'].value === '')
        v_Return = 'O campo de número deve ser preenchido';
    else if(p_Form['bairro'].value === null || p_Form['bairro'].value === '')
        v_Return = 'O campo de bairro deve ser preenchido';
    else if(p_Form['latitude'].value === '' || p_Form['longitude'].value === '')
        v_Return = 'O endereço deve ser preenchido corretamente';
    if(v_Return != true)
        $('#cep').mask('99.999-999');
    return v_Return;
}

$(document).ready(function ()
{
    $('#cep').mask('99.999-999');

    $('form input:not(#cep), form select').keypress(function (event) {
        var keyCode = (event.keyCode ? event.keyCode : event.which);
        if (keyCode == '13') {
            event.preventDefault();
            $('input[type="submit"]').click();
        }
    });
    $('#cep').keypress(function (event) {
        var keyCode = (event.keyCode ? event.keyCode : event.which);
        if (keyCode == '13') {
            event.preventDefault();
            $('.btn.search-cep').click();
        }
    });
    $('#state, #city, input[name="cidade"], input[name="estado"], input[name="numero"], input[name="logradouro"], input[name="bairro"]').change(function(){
        var v_AddressString = $('input[name="logradouro"]').val() + '+' + $('input[name="numero"]').val() + '+' + $('input[name="bairro"]').val() + '+' + $('input[name="cidade"]').val() + '+' + $('input[name="estado"]').val();
        v_AddressString = v_AddressString.replace(/ /g, '+');
        $('.map-row iframe').attr('src', "https://www.google.com/maps/embed/v1/search?key=AIzaSyByS6W9I7X4NnvrIjqaZlMv1J8lVtMOhuw&q=" + v_AddressString);

        $.ajax({
            url : 'http://maps.google.com/maps/api/geocode/json?sensor=false&address=' + v_AddressString,
            type: "GET",
            dataType: "html",
            success: function(p_Data, textStatus, jqXHR)
            {
                var v_Location = JSON.parse(p_Data)['results'][0]['geometry']['location'];
                $('input[name="longitude"]').val(v_Location['lng']);
                $('input[name="latitude"]').val(v_Location['lat']);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('input[name="longitude"], input[name="latitude"]').empty();
            }
        });
    });
});