
if (typeof(recCategoria) == 'object' && typeof(recCategoria["categorias"]) == 'object') {
    recCategoria['categorias'].forEach(function(currentValue, index, arr){
        objDetach = $("[category-id='" + currentValue + "']").detach();
        $('.customer-logos').append(objDetach);
      });
      
}

/*Coloca a categoria selecionada em primeira posição*/
selCategory = $($(".selected-category").parent()[0]).detach();
$('.customer-logos').prepend(selCategory);

$('.customer-logos').slick({
    slidesToShow: 7,
    slidesToScroll: 7,
    autoplay: false,
    autoplaySpeed: 5000,
    prevArrow: '<a data-role="none" class="carousel-control-prev slick-control-prev" aria-label="Previous" tabindex="0" role="button"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a>',
    nextArrow: '<a data-role="none" class="carousel-control-next slick-control-next" aria-label="Next" tabindex="0" role="button"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>',
    arrows: true,
    dots: false,
    pauseOnHover: false,
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 5,
            slidesToScroll: 5
        }
    },{
        breakpoint: 992,
        settings: {
            slidesToShow: 4,
            slidesToScroll: 4
        }
    },{
        breakpoint: 768,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3
        }
    }, {
        breakpoint: 530,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3
        }
    }, {
        breakpoint: 430,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    }]
});



/*Coloca a categoria selecionada em primeira posição*/
selCity = $($(".selected-city").parent()[0]).detach();
$('.cities-logos').prepend(selCity);

$('.cities-logos').slick({
    slidesToShow: 4,
    slidesToScroll: 7,
    autoplay: false,
    autoplaySpeed: 5000,
    prevArrow: '<a data-role="none" class="carousel-control-prev slick-control-prev" aria-label="Previous" tabindex="0" role="button"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="sr-only">Previous</span></a>',
    nextArrow: '<a data-role="none" class="carousel-control-next slick-control-next" aria-label="Next" tabindex="0" role="button"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="sr-only">Next</span></a>',
    arrows: true,
    dots: false,
    pauseOnHover: false,
    responsive: [{
        breakpoint: 1200,
        settings: {
            slidesToShow: 5,
            slidesToScroll: 5
        }
    },{
        breakpoint: 992,
        settings: {
            slidesToShow: 4,
            slidesToScroll: 4
        }
    },{
        breakpoint: 768,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3
        }
    }, {
        breakpoint: 530,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3
        }
    }, {
        breakpoint: 430,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2
        }
    }]
});


$('.btnSearchCategory').on('click', function(){
    $('[name="category_id"]').val($(this).attr('category-id'));
    $('#SearchFormButton').click();
    //alert('Category ID' + $('[name="category_id"]').val());
});

$('.btnSearchCity').on('click', function(){
    //var $city_id = $(this).attr('city-id');
    $('#inputGroupSelect01').val($(this).attr('city-id')).change();
    $('#SearchFormButton').click();
    //alert('Category ID' + $('[name="category_id"]').val());
});

