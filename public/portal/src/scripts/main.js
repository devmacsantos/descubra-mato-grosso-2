$(document).ready(function () {

	$(window).scroll(function() {
	       
		var containerHeight = $('#box-menu-top').outerHeight(true);
		var scrollTopVal = $(this).scrollTop();

		if (scrollTopVal > containerHeight) {

		    $('#box-menu-top').hide();
		    $('#box-menu-top-resume').show();

		} else {

			$('#box-menu-top-resume').hide();
		    $('#box-menu-top').show();

		}
    });

    $('#myTab a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	});

	$('#myTabs a').click(function (e) {
		 e.preventDefault()
	  $(this).tab('show')
	    // if($(this).parent('li').hasClass('active')){
	    //     $($(this).attr('href')).hide();
	    // }
	    // else {
	    //     e.preventDefault();
	    //     $(this).tab('show');
	    // }
	});

	$('#myModal').on('show.bs.modal', function (e) {
        if ($(window).width() < 768) {
            $('body').css("padding-right","15px");
        }
    });

    $('#myModal').on('hide.bs.modal', function (e) {
        if ($(window).width() > 768) {
            $("body").css("padding-right","0px");
        }
    });
    
    $( ".calendar .list-years").click(function() {

    	var ano = $(this).attr('data-ano');
    	$('#list-months-'+ano).slideToggle("fast", function() {});

    });

    $("#tabs").tabs();

    //MENNU COMPLETO
	$( "#dropdownMenu1 .nav-link").click(function() {
		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").hide();

		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();
		$("#dropdownMenu1_anchor").css({'color': '#fcb731 !important'});
		$("#listDropdownMenu1").slideToggle("fast", function() {});
	});

	$( "#dropdownMenu2 .nav-link").click(function() {
		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").hide();

		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();
		$("#dropdownMenu2_anchor").css({'color': '#fcb731 !important'});
		$( "#listDropdownMenu2" ).slideToggle("fast", function() {});
	});

	$( "#dropdownMenu3 .nav-link").click(function() {
		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").hide();

		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu4").hide();
		$("#dropdownMenu3_anchor").css({'color': '#fcb731 !important'});
		$("#listDropdownMenu3").slideToggle("fast", function() {});
	});

	//MENNU RESUMIDO
	$("#box-menu-top-resume #dropdownResumeMenu1").click(function() {
		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();

		$("#listDropdownResumidoMenu2").hide();
		$("#listDropdownResumidoMenu3").hide();
		$("#listDropdownResumidoMenu4").hide();
		
		$("#listDropdownResumidoMenu1").slideToggle("fast", function() {});
	});

	$("#box-menu-top-resume #dropdownResumeMenu2").click(function() {
		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();

		$("#listDropdownResumidoMenu1").hide();
		$("#listDropdownResumidoMenu3").hide();
		$("#listDropdownResumidoMenu4").hide();
		
		$( "#listDropdownResumidoMenu2" ).slideToggle("fast", function() {});
	});

	$("#box-menu-top-resume #dropdownResumeMenu3").click(function() {
		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();

		$("#listDropdownResumidoMenu1").hide();
		$("#listDropdownResumidoMenu2").hide();
		$("#listDropdownResumidoMenu4").hide();

		$("#listDropdownResumidoMenu3").slideToggle("fast", function() {});
	});

	$("#box-menu-top-resume #dropdownResumeMenu4 #imgHamburguer").click(function() {
        $("#box-menu-top-resume #dropdownResumeMenu4 #imgHamburguer").hide();
        $("#box-menu-top-resume #dropdownResumeMenu4 #imgClose").show();

		$("#listDropdownResumidoMenu1").hide();
		$("#listDropdownResumidoMenu2").hide();
		$("#listDropdownResumidoMenu3").hide();
		$("#listDropdownResumidoMenu4").slideToggle("fast", function() {});
	});

	$("#box-menu-top-resume #dropdownResumeMenu4 #imgClose").click(function() {
        $("#box-menu-top-resume #dropdownResumeMenu4 #imgHamburguer").show();
        $("#box-menu-top-resume #dropdownResumeMenu4 #imgClose").hide();
        
		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").slideToggle("fast", function() {});
	});

	//MENNU MOBILE
	$("#dropdownResumeMenu1").click(function() {
		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();

		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").hide();
		
		$("#listDropdownResumeMenu1").slideToggle("fast", function() {});
	});

	$("#dropdownResumeMenu2").click(function() {
		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();

		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").hide();
		
		$( "#listDropdownResumeMenu2" ).slideToggle("fast", function() {});
	});

	$("#dropdownResumeMenu3").click(function() {
		$("#listDropdownMenu1").hide();
		$("#listDropdownMenu2").hide();
		$("#listDropdownMenu3").hide();
		$("#listDropdownMenu4").hide();

		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu4").hide();

		$("#listDropdownResumeMenu3").slideToggle("fast", function() {});
	});

	$("#dropdownResumeMenu4 #imgHamburguer").click(function() {
        $("#imgHamburguer").hide();
        $("#imgClose").show();

		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").slideToggle("fast", function() {});
	});

	$("#dropdownResumeMenu4 #imgClose").click(function() {
        $("#imgHamburguer").show();
        $("#imgClose").hide();
        
		$("#listDropdownResumeMenu1").hide();
		$("#listDropdownResumeMenu2").hide();
		$("#listDropdownResumeMenu3").hide();
		$("#listDropdownResumeMenu4").slideToggle("fast", function() {});
	});

});