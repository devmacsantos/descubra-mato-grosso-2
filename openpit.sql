-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: openpit
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accomodation_service`
--

DROP TABLE IF EXISTS `accomodation_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accomodation_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `tipo_atividade_cadastur` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `nome_rede` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `registro_cadastur` varchar(20) DEFAULT NULL,
  `inicio_atividade` varchar(7) DEFAULT NULL,
  `data_tombamento` datetime DEFAULT NULL,
  `organizacao_responsavel` varchar(300) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` text,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `descricao_arredores_distancia_pontos` text NOT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `meses_maior_ocupacao` varchar(27) DEFAULT NULL,
  `valor_medio_diaria` varchar(20) DEFAULT NULL,
  `unidades_habitacionais` text NOT NULL,
  `unidades_habitacionais_facilidades` text,
  `unidades_habitacionais_voltagem` text,
  `unidades_habitacionais_facilidades_outras` text,
  `meios_hospedagem_extra_hoteleiros` text,
  `tipo_diaria` varchar(100) DEFAULT NULL,
  `tipo_diaria_outros` text,
  `servicos_equipamentos_area_social` text,
  `servicos_equipamentos_recreacao_lazer` text,
  `aceita_animais` tinyint(1) DEFAULT NULL,
  `gay_friendly` tinyint(1) DEFAULT NULL,
  `nao_aceita_criancas` tinyint(1) DEFAULT NULL,
  `restricoes_hospedes` text,
  `formas_pagamento` text,
  `descricoes_observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `tem_cadastur` tinyint(1) NOT NULL DEFAULT '0',
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  `validade_cadastur` datetime DEFAULT NULL,
  `atracao` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_city_other_services_type2_idx` (`sub_type_id`),
  KEY `fk_accomodation_service_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_accomodation_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_city100000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district100000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type10` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type20` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accomodation_service`
--

LOCK TABLES `accomodation_service` WRITE;
/*!40000 ALTER TABLE `accomodation_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `accomodation_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attraction`
--

DROP TABLE IF EXISTS `attraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attraction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_inventario` varchar(2) NOT NULL,
  `item_inventario_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `trade` tinyint(1) NOT NULL DEFAULT '0',
  `atracao` tinyint(1) NOT NULL DEFAULT '0',
  `parque` tinyint(1) NOT NULL DEFAULT '0',
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `nome` text NOT NULL,
  `nome_pt` varchar(200) DEFAULT NULL,
  `slug` varchar(80) NOT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `cep` varchar(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) DEFAULT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `descricao_arredores_distancia_pontos` text,
  `ponto_referencia` text,
  `localizacao` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `funcionamento_1` text,
  `funcionamento_2` text,
  `funcionamento_observacao` text,
  `visita_tipo` varchar(30) DEFAULT NULL,
  `entrada_tipo` varchar(10) DEFAULT NULL,
  `entrada_valor` varchar(20) DEFAULT NULL,
  `atividades_realizadas` text,
  `necessaria_autorizacao_previa` tinyint(1) DEFAULT '0',
  `autorizacao_previa_tipo` text,
  `formas_pagamento` text,
  `descricao_curta` text,
  `descricao` text,
  `observacoes_complementares` text,
  `aceita_animais` tinyint(1) DEFAULT NULL,
  `gay_friendly` tinyint(1) DEFAULT NULL,
  `nao_aceita_criancas` tinyint(1) DEFAULT NULL,
  `acessibilidade` text,
  `unidades_habitacionais_facilidades` text,
  `unidades_habitacionais_voltagem` text,
  `tipo_diaria` text,
  `tipo_diaria_outros` text,
  `servicos_equipamentos_recreacao_lazer` text,
  `restricoes` text,
  `servicos_equipamentos` text,
  `culinaria_nacionalidades` text,
  `culinaria_nacionalidade_outras` text,
  `culinaria_servicos` text,
  `culinaria_temas` text,
  `culinaria_tema_outros` text,
  `culinaria_regioes` text,
  `culinaria_regiao_outras` text,
  `segmentos_agencia` text,
  `tipos_servico` text,
  `quantidade_veiculos_adaptados` varchar(20) DEFAULT NULL,
  `servicos_equipamentos_apoio` text,
  `principais_atividades` text,
  `municipios_participantes` text,
  `integrante_minas_recebe` tinyint(1) NOT NULL DEFAULT '0',
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `nome_oficial` varchar(250) DEFAULT NULL,
  `type_slug` varchar(250) DEFAULT NULL,
  `slug_trade` varchar(250) DEFAULT NULL,
  `sinonimos` text,
  `city_name` varchar(250) DEFAULT NULL,
  `antonimos` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `slug` (`slug`),
  KEY `fk_atracao_district1_idx` (`district_id`),
  KEY `fk_atracao_city1_idx` (`city_id`),
  KEY `fk_atracao_type1_idx` (`type_id`),
  KEY `fk_atracao_type2_idx` (`sub_type_id`),
  FULLTEXT KEY `fts_exclude` (`antonimos`),
  FULLTEXT KEY `fts` (`nome_pt`,`nome_oficial`,`type_slug`,`slug_trade`,`sinonimos`,`city_name`),
  CONSTRAINT `fk_atracao_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_atracao_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_atracao_type1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_atracao_type2` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attraction`
--

LOCK TABLES `attraction` WRITE;
/*!40000 ALTER TABLE `attraction` DISABLE KEYS */;
/*!40000 ALTER TABLE `attraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attraction_hashtag`
--

DROP TABLE IF EXISTS `attraction_hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attraction_hashtag` (
  `hashtag_id` int(11) NOT NULL,
  `attraction_id` int(11) NOT NULL,
  PRIMARY KEY (`hashtag_id`,`attraction_id`),
  KEY `fk_attraction_has_hashtag_hashtag1_idx` (`hashtag_id`),
  KEY `fk_attraction_hashtag_attraction1_idx` (`attraction_id`),
  CONSTRAINT `fk_attraction_has_hashtag_hashtag1` FOREIGN KEY (`hashtag_id`) REFERENCES `hashtag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_attraction_hashtag_attraction1` FOREIGN KEY (`attraction_id`) REFERENCES `attraction` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attraction_hashtag`
--

LOCK TABLES `attraction_hashtag` WRITE;
/*!40000 ALTER TABLE `attraction_hashtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `attraction_hashtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attraction_photo`
--

DROP TABLE IF EXISTS `attraction_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attraction_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(2083) NOT NULL,
  `attraction_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_attraction_photo_copy1_attraction1_idx` (`attraction_id`),
  CONSTRAINT `fk_attraction_photo_copy1_attraction1` FOREIGN KEY (`attraction_id`) REFERENCES `attraction` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attraction_photo`
--

LOCK TABLES `attraction_photo` WRITE;
/*!40000 ALTER TABLE `attraction_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `attraction_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_article`
--

DROP TABLE IF EXISTS `blog_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(80) NOT NULL,
  `foto_capa_url` varchar(2083) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(250) NOT NULL,
  `titulo` text,
  `descricao_curta` text,
  `conteudo` text,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `slug_index` (`slug`),
  FULLTEXT KEY `fts` (`titulo_pt`,`conteudo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_article`
--

LOCK TABLES `blog_article` WRITE;
/*!40000 ALTER TABLE `blog_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_article_hashtag`
--

DROP TABLE IF EXISTS `blog_article_hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_article_hashtag` (
  `blog_article_id` int(11) NOT NULL,
  `hashtag_id` int(11) NOT NULL,
  PRIMARY KEY (`blog_article_id`,`hashtag_id`),
  KEY `fk_blog_article_has_hashtag_hashtag1_idx` (`hashtag_id`),
  KEY `fk_blog_article_has_hashtag_blog_article1_idx` (`blog_article_id`),
  CONSTRAINT `fk_blog_article_has_hashtag_blog_article1` FOREIGN KEY (`blog_article_id`) REFERENCES `blog_article` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_blog_article_has_hashtag_hashtag1` FOREIGN KEY (`hashtag_id`) REFERENCES `hashtag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_article_hashtag`
--

LOCK TABLES `blog_article_hashtag` WRITE;
/*!40000 ALTER TABLE `blog_article_hashtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `blog_article_hashtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cadastur_activity`
--

DROP TABLE IF EXISTS `cadastur_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cadastur_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cadastur_activity`
--

LOCK TABLES `cadastur_activity` WRITE;
/*!40000 ALTER TABLE `cadastur_activity` DISABLE KEYS */;
INSERT INTO `cadastur_activity` VALUES (20,'Meio de Hospedagem'),(25,'Centro de Convenções'),(35,'Acampamento Turístico'),(40,'Prestador de Infraestrutura de Apoio para Eventos'),(45,'Empreendimento de Apoio ao Turismo Náutico ou à Pesca Desportiva'),(55,'Casa de Espetáculos & Equipamento de Animação Turística'),(60,'Parque Temático'),(65,'Empreendimento de Entretenimento e Lazer & Parque Aquático'),(70,'Entretenimento e Lazer'),(75,'Prestador Especializado em Segmentos Turísticos'),(80,'Organizadora de Eventos');
/*!40000 ALTER TABLE `cadastur_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `slug` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `fts_search` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_access`
--

DROP TABLE IF EXISTS `city_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `tipo` varchar(15) DEFAULT NULL,
  `em_funcionamento` tinyint(1) DEFAULT NULL,
  `categoria` varchar(15) DEFAULT NULL,
  `tipo_administracao` varchar(15) DEFAULT NULL,
  `nome` varchar(200) DEFAULT NULL,
  `cep` varchar(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) DEFAULT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `administradora_nome` varchar(200) DEFAULT NULL,
  `administradora_cep` varchar(11) DEFAULT NULL,
  `administradora_bairro` varchar(100) DEFAULT NULL,
  `administradora_logradouro` varchar(200) DEFAULT NULL,
  `administradora_numero` varchar(50) DEFAULT NULL,
  `administradora_complemento` varchar(50) DEFAULT NULL,
  `administradora_telefone` varchar(150) DEFAULT NULL,
  `administradora_site` varchar(2083) DEFAULT NULL,
  `administradora_email` varchar(200) DEFAULT NULL,
  `utilizacao` varchar(15) DEFAULT NULL,
  `ano_base` int(11) DEFAULT NULL,
  `fluxo_passageiros_nacionais` varchar(20) DEFAULT NULL,
  `fluxo_passageiros_internacionais` varchar(20) DEFAULT NULL,
  `terminal_desembarque_turistas` tinyint(1) DEFAULT NULL,
  `conservacao` varchar(10) NOT NULL,
  `observacao` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_access_type1_idx` (`type_id`),
  KEY `fk_city_access_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_type1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_access`
--

LOCK TABLES `city_access` WRITE;
/*!40000 ALTER TABLE `city_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_access_general`
--

DROP TABLE IF EXISTS `city_access_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_access_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `rodovia_federal` text,
  `rodovia_estadual` text,
  `rodovia_municipal` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_general_city1_idx` (`city_id`),
  KEY `fk_city_access_general_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_general_city10` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_general_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_access_general`
--

LOCK TABLES `city_access_general` WRITE;
/*!40000 ALTER TABLE `city_access_general` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_access_general` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_hospital_system`
--

DROP TABLE IF EXISTS `city_hospital_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_hospital_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `economic_activity_id` int(11) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `tipo_atendimento` varchar(70) DEFAULT NULL,
  `servicos_prestados` text,
  `informacoes_observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_hospital_system_type1_idx` (`type_id`),
  KEY `fk_city_hospital_system_economic_activity1_idx` (`economic_activity_id`),
  KEY `fk_city_hospital_system_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city1000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district1000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_hospital_system_economic_activity1` FOREIGN KEY (`economic_activity_id`) REFERENCES `economic_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_hospital_system_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_hospital_system_type1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_hospital_system`
--

LOCK TABLES `city_hospital_system` WRITE;
/*!40000 ALTER TABLE `city_hospital_system` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_hospital_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_info`
--

DROP TABLE IF EXISTS `city_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `prefeitura_endereco_cep` varchar(11) NOT NULL,
  `prefeitura_endereco_bairro` varchar(100) DEFAULT NULL,
  `prefeitura_endereco_logradouro` varchar(200) NOT NULL,
  `prefeitura_endereco_numero` varchar(50) DEFAULT NULL,
  `prefeitura_endereco_complemento` varchar(50) DEFAULT NULL,
  `prefeitura_telefone` varchar(150) NOT NULL,
  `prefeitura_site` varchar(2083) NOT NULL,
  `prefeitura_email` varchar(200) NOT NULL,
  `registro_estadual` varchar(200) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `populacao_total` varchar(20) DEFAULT NULL,
  `populacao_urbana` varchar(20) DEFAULT NULL,
  `populacao_rural` varchar(20) DEFAULT NULL,
  `area_total` varchar(20) NOT NULL,
  `municipios_limitrofes` text NOT NULL,
  `temperatura_media` float NOT NULL,
  `temperatura_min` float DEFAULT NULL,
  `temperatura_max` float DEFAULT NULL,
  `meses_secos` varchar(27) DEFAULT NULL,
  `meses_chuvosos` varchar(27) DEFAULT NULL,
  `clima` text,
  `altitude` varchar(20) DEFAULT NULL,
  `principais_atividades_economicas` text NOT NULL,
  `servicos_telefonia_movel` text NOT NULL,
  `servicos_telefonia_movel_outros` text,
  `postos_telefonicos` tinyint(1) NOT NULL DEFAULT '0',
  `emissora_radio` tinyint(1) NOT NULL DEFAULT '0',
  `prefeito_nome` varchar(250) NOT NULL,
  `prefeito_telefone` varchar(150) NOT NULL,
  `prefeito_email` varchar(200) NOT NULL,
  `prefeito_partido_id` int(11) NOT NULL,
  `prefeitura_funcionarios_total` varchar(20) NOT NULL,
  `prefeitura_funcionarios_fixos` varchar(20) NOT NULL,
  `prefeitura_funcionarios_temporarios` varchar(20) NOT NULL,
  `prefeitura_funcionarios_deficiencia` varchar(20) NOT NULL,
  `prefeitura_nomes_departamentos` text NOT NULL,
  `possui_orgao_turismo` tinyint(1) NOT NULL DEFAULT '0',
  `orgao_turismo_tipo` varchar(20) DEFAULT NULL,
  `orgao_turismo_nome` varchar(250) DEFAULT NULL,
  `orgao_turismo_titular` varchar(250) DEFAULT NULL,
  `orgao_turismo_orcamento` tinyint(1) NOT NULL,
  `orgao_turismo_orcamento_ultimo_ano` varchar(20) DEFAULT NULL,
  `orgao_turismo_orcamento_ano_referencia` int(11) DEFAULT NULL,
  `orgao_turismo_acoes` text,
  `orgao_turismo_acoes_outras` text,
  `orgao_turismo_endereco_cep` varchar(11) NOT NULL,
  `orgao_turismo_endereco_bairro` varchar(100) DEFAULT NULL,
  `orgao_turismo_endereco_logradouro` varchar(200) NOT NULL,
  `orgao_turismo_endereco_numero` varchar(50) DEFAULT NULL,
  `orgao_turismo_endereco_complemento` varchar(50) DEFAULT NULL,
  `orgao_turismo_site` varchar(2083) DEFAULT NULL,
  `orgao_turismo_email` varchar(200) NOT NULL,
  `orgao_turismo_telefone` varchar(150) NOT NULL,
  `legislacao_lei_organica` varchar(250) DEFAULT NULL,
  `legislacao_ocupacao_solo` varchar(250) NOT NULL,
  `legislacao_regulamentacao_turismo` varchar(250) NOT NULL,
  `legislacao_plano_desenvolvimento_turismo` varchar(250) NOT NULL,
  `legislacao_protecao_ambiental` varchar(250) NOT NULL,
  `legislacao_apoio_cultura` varchar(250) NOT NULL,
  `legislacao_incentivo_fiscal_turismo` varchar(250) NOT NULL,
  `legislacao_plano_diretor` tinyint(1) NOT NULL,
  `legislacao_criacao_conselho_turismo` varchar(250) NOT NULL,
  `legislacao_outras` text,
  `agua_abastecimento` text,
  `esgotamento` text,
  `energia_abastecimento` text,
  `destinacao_lixo` text,
  `visitantes_total` varchar(20) DEFAULT NULL,
  `visitantes_alta_temporada` varchar(20) DEFAULT NULL,
  `visitantes_baixa_temporada` varchar(20) DEFAULT NULL,
  `ano_base` int(11) DEFAULT NULL,
  `visitantes_regionais` varchar(20) DEFAULT NULL,
  `visitantes_estaduais` varchar(20) DEFAULT NULL,
  `visitantes_nacionais` varchar(20) DEFAULT NULL,
  `visitantes_internacionais` varchar(20) DEFAULT NULL,
  `historico_municipio` text NOT NULL,
  `descricao_informacoes_complementares` text,
  `parceirias_cooperacoes_intercambios_interfaces` text,
  `fontes_dados_instituicao` varchar(250) NOT NULL,
  `fontes_dados_site` text NOT NULL,
  `fontes_dados_publicacoes` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  KEY `fk_city_info_city2_idx` (`city_id`),
  KEY `fk_city_info_political_party1_idx` (`prefeito_partido_id`),
  KEY `fk_city_info_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_info_city2` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_info_political_party1` FOREIGN KEY (`prefeito_partido_id`) REFERENCES `political_party` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_info_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_info`
--

LOCK TABLES `city_info` WRITE;
/*!40000 ALTER TABLE `city_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_other_service`
--

DROP TABLE IF EXISTS `city_other_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_other_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `economic_activity_id` int(11) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `servicos_prestados` text,
  `informacoes_observacoes_complementares` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_city_other_services_type2_idx` (`sub_type_id`),
  KEY `fk_city_other_service_economic_activity1_idx` (`economic_activity_id`),
  KEY `fk_city_other_service_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city10000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district10000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_service_economic_activity1` FOREIGN KEY (`economic_activity_id`) REFERENCES `economic_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type2` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_other_service`
--

LOCK TABLES `city_other_service` WRITE;
/*!40000 ALTER TABLE `city_other_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_other_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city_security_system`
--

DROP TABLE IF EXISTS `city_security_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city_security_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `economic_activity_id` int(11) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `informacoes_observacoes_complementares` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_security_system_type1_idx` (`type_id`),
  KEY `fk_city_security_system_economic_activity1_idx` (`economic_activity_id`),
  KEY `fk_city_city_security_system_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city100` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district100` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_city_security_system_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_security_system_economic_activity1` FOREIGN KEY (`economic_activity_id`) REFERENCES `economic_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_security_system_type1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city_security_system`
--

LOCK TABLES `city_security_system` WRITE;
/*!40000 ALTER TABLE `city_security_system` DISABLE KEYS */;
/*!40000 ALTER TABLE `city_security_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `climate`
--

DROP TABLE IF EXISTS `climate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `climate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `climate`
--

LOCK TABLES `climate` WRITE;
/*!40000 ALTER TABLE `climate` DISABLE KEYS */;
INSERT INTO `climate` VALUES (1,'Tropical '),(2,'Tropical de Altitude'),(3,'Equatorial'),(4,'Tropical atlântico ou Tropical úmido'),(5,'Subtropical'),(6,'Semiárido');
/*!40000 ALTER TABLE `climate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_subject_id` int(11) NOT NULL,
  `contact_city_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL,
  `ip` varchar(16) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contact_contact_subject_idx` (`contact_subject_id`),
  KEY `fk_contact_contact_city_idx` (`contact_city_id`),
  CONSTRAINT `fk_contact_contact_city` FOREIGN KEY (`contact_city_id`) REFERENCES `contact_city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contact_contact_subject` FOREIGN KEY (`contact_subject_id`) REFERENCES `contact_subject` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_city`
--

DROP TABLE IF EXISTS `contact_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_city` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `state` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_city`
--

LOCK TABLES `contact_city` WRITE;
/*!40000 ALTER TABLE `contact_city` DISABLE KEYS */;
INSERT INTO `contact_city` VALUES (1,'Acrelândia','AC'),(2,'Assis Brasil','AC'),(3,'Brasiléia','AC'),(4,'Bujari','AC'),(5,'Capixaba','AC'),(6,'Cruzeiro do Sul','AC'),(7,'Epitaciolândia','AC'),(8,'Feijó','AC'),(9,'Jordão','AC'),(10,'Mâncio Lima','AC'),(11,'Manoel Urbano','AC'),(12,'Marechal Thaumaturgo','AC'),(13,'Plácido de Castro','AC'),(14,'Porto Acre','AC'),(15,'Porto Walter','AC'),(16,'Rio Branco','AC'),(17,'Rodrigues Alves','AC'),(18,'Santa Rosa do Purus','AC'),(19,'Sena Madureira','AC'),(20,'Senador Guiomard','AC'),(21,'Tarauacá','AC'),(22,'Xapuri','AC'),(23,'Água Branca','AL'),(24,'Anadia','AL'),(25,'Arapiraca','AL'),(26,'Atalaia','AL'),(27,'Barra de Santo Antônio','AL'),(28,'Barra de São Miguel','AL'),(29,'Batalha','AL'),(30,'Belém','AL'),(31,'Belo Monte','AL'),(32,'Boca da Mata','AL'),(33,'Branquinha','AL'),(34,'Cacimbinhas','AL'),(35,'Cajueiro','AL'),(36,'Campestre','AL'),(37,'Campo Alegre','AL'),(38,'Campo Grande','AL'),(39,'Canapi','AL'),(40,'Capela','AL'),(41,'Carneiros','AL'),(42,'Chã Preta','AL'),(43,'Coité do Nóia','AL'),(44,'Colônia Leopoldina','AL'),(45,'Coqueiro Seco','AL'),(46,'Coruripe','AL'),(47,'Craíbas','AL'),(48,'Delmiro Gouveia','AL'),(49,'Dois Riachos','AL'),(50,'Estrela de Alagoas','AL'),(51,'Feira Grande','AL'),(52,'Feliz Deserto','AL'),(53,'Flexeiras','AL'),(54,'Girau do Ponciano','AL'),(55,'Ibateguara','AL'),(56,'Igaci','AL'),(57,'Igreja Nova','AL'),(58,'Inhapi','AL'),(59,'Jacaré dos Homens','AL'),(60,'Jacuípe','AL'),(61,'Japaratinga','AL'),(62,'Jaramataia','AL'),(63,'Jequiá da Praia','AL'),(64,'Joaquim Gomes','AL'),(65,'Jundiá','AL'),(66,'Junqueiro','AL'),(67,'Lagoa da Canoa','AL'),(68,'Limoeiro de Anadia','AL'),(69,'Maceió','AL'),(70,'Major Isidoro','AL'),(71,'Mar Vermelho','AL'),(72,'Maragogi','AL'),(73,'Maravilha','AL'),(74,'Marechal Deodoro','AL'),(75,'Maribondo','AL'),(76,'Mata Grande','AL'),(77,'Matriz de Camaragibe','AL'),(78,'Messias','AL'),(79,'Minador do Negrão','AL'),(80,'Monteirópolis','AL'),(81,'Murici','AL'),(82,'Novo Lino','AL'),(83,'Olho d\'Água das Flores','AL'),(84,'Olho d\'Água do Casado','AL'),(85,'Olho d\'Água Grande','AL'),(86,'Olivença','AL'),(87,'Ouro Branco','AL'),(88,'Palestina','AL'),(89,'Palmeira dos Índios','AL'),(90,'Pão de Açúcar','AL'),(91,'Pariconha','AL'),(92,'Paripueira','AL'),(93,'Passo de Camaragibe','AL'),(94,'Paulo Jacinto','AL'),(95,'Penedo','AL'),(96,'Piaçabuçu','AL'),(97,'Pilar','AL'),(98,'Pindoba','AL'),(99,'Piranhas','AL'),(100,'Poço das Trincheiras','AL'),(101,'Porto Calvo','AL'),(102,'Porto de Pedras','AL'),(103,'Porto Real do Colégio','AL'),(104,'Quebrangulo','AL'),(105,'Rio Largo','AL'),(106,'Roteiro','AL'),(107,'Santa Luzia do Norte','AL'),(108,'Santana do Ipanema','AL'),(109,'Santana do Mundaú','AL'),(110,'São Brás','AL'),(111,'São José da Laje','AL'),(112,'São José da Tapera','AL'),(113,'São Luís do Quitunde','AL'),(114,'São Miguel dos Campos','AL'),(115,'São Miguel dos Milagres','AL'),(116,'São Sebastião','AL'),(117,'Satuba','AL'),(118,'Senador Rui Palmeira','AL'),(119,'Tanque d\'Arca','AL'),(120,'Taquarana','AL'),(121,'Teotônio Vilela','AL'),(122,'Traipu','AL'),(123,'União dos Palmares','AL'),(124,'Viçosa','AL'),(125,'Alvarães','AM'),(126,'Amaturá','AM'),(127,'Anamã','AM'),(128,'Anori','AM'),(129,'Apuí','AM'),(130,'Atalaia do Norte','AM'),(131,'Autazes','AM'),(132,'Barcelos','AM'),(133,'Barreirinha','AM'),(134,'Benjamin Constant','AM'),(135,'Beruri','AM'),(136,'Boa Vista do Ramos','AM'),(137,'Boca do Acre','AM'),(138,'Borba','AM'),(139,'Caapiranga','AM'),(140,'Canutama','AM'),(141,'Carauari','AM'),(142,'Careiro','AM'),(143,'Careiro da Várzea','AM'),(144,'Coari','AM'),(145,'Codajás','AM'),(146,'Eirunepé','AM'),(147,'Envira','AM'),(148,'Fonte Boa','AM'),(149,'Guajará','AM'),(150,'Humaitá','AM'),(151,'Ipixuna','AM'),(152,'Iranduba','AM'),(153,'Itacoatiara','AM'),(154,'Itamarati','AM'),(155,'Itapiranga','AM'),(156,'Japurá','AM'),(157,'Juruá','AM'),(158,'Jutaí','AM'),(159,'Lábrea','AM'),(160,'Manacapuru','AM'),(161,'Manaquiri','AM'),(162,'Manaus','AM'),(163,'Manicoré','AM'),(164,'Maraã','AM'),(165,'Maués','AM'),(166,'Nhamundá','AM'),(167,'Nova Olinda do Norte','AM'),(168,'Novo Airão','AM'),(169,'Novo Aripuanã','AM'),(170,'Parintins','AM'),(171,'Pauini','AM'),(172,'Presidente Figueiredo','AM'),(173,'Rio Preto da Eva','AM'),(174,'Santa Isabel do Rio Negro','AM'),(175,'Santo Antônio do Içá','AM'),(176,'São Gabriel da Cachoeira','AM'),(177,'São Paulo de Olivença','AM'),(178,'São Sebastião do Uatumã','AM'),(179,'Silves','AM'),(180,'Tabatinga','AM'),(181,'Tapauá','AM'),(182,'Tefé','AM'),(183,'Tonantins','AM'),(184,'Uarini','AM'),(185,'Urucará','AM'),(186,'Urucurituba','AM'),(187,'Amapá','AP'),(188,'Calçoene','AP'),(189,'Cutias','AP'),(190,'Ferreira Gomes','AP'),(191,'Itaubal','AP'),(192,'Laranjal do Jari','AP'),(193,'Macapá','AP'),(194,'Mazagão','AP'),(195,'Oiapoque','AP'),(196,'Pedra Branca do Amapari','AP'),(197,'Porto Grande','AP'),(198,'Pracuúba','AP'),(199,'Santana','AP'),(200,'Serra do Navio','AP'),(201,'Tartarugalzinho','AP'),(202,'Vitória do Jari','AP'),(203,'Abaíra','BA'),(204,'Abaré','BA'),(205,'Acajutiba','BA'),(206,'Adustina','BA'),(207,'Água Fria','BA'),(208,'Aiquara','BA'),(209,'Alagoinhas','BA'),(210,'Alcobaça','BA'),(211,'Almadina','BA'),(212,'Amargosa','BA'),(213,'Amélia Rodrigues','BA'),(214,'América Dourada','BA'),(215,'Anagé','BA'),(216,'Andaraí','BA'),(217,'Andorinha','BA'),(218,'Angical','BA'),(219,'Anguera','BA'),(220,'Antas','BA'),(221,'Antônio Cardoso','BA'),(222,'Antônio Gonçalves','BA'),(223,'Aporá','BA'),(224,'Apuarema','BA'),(225,'Araças','BA'),(226,'Aracatu','BA'),(227,'Araci','BA'),(228,'Aramari','BA'),(229,'Arataca','BA'),(230,'Aratuípe','BA'),(231,'Aurelino Leal','BA'),(232,'Baianópolis','BA'),(233,'Baixa Grande','BA'),(234,'Banzaê','BA'),(235,'Barra','BA'),(236,'Barra da Estiva','BA'),(237,'Barra do Choça','BA'),(238,'Barra do Mendes','BA'),(239,'Barra do Rocha','BA'),(240,'Barreiras','BA'),(241,'Barro Alto','BA'),(242,'Barrocas','BA'),(243,'Barro Preto','BA'),(244,'Belmonte','BA'),(245,'Belo Campo','BA'),(246,'Biritinga','BA'),(247,'Boa Nova','BA'),(248,'Boa Vista do Tupim','BA'),(249,'Bom Jesus da Lapa','BA'),(250,'Bom Jesus da Serra','BA'),(251,'Boninal','BA'),(252,'Bonito','BA'),(253,'Boquira','BA'),(254,'Botuporã','BA'),(255,'Brejões','BA'),(256,'Brejolândia','BA'),(257,'Brotas de Macaúbas','BA'),(258,'Brumado','BA'),(259,'Buerarema','BA'),(260,'Buritirama','BA'),(261,'Caatiba','BA'),(262,'Cabaceiras do Paraguaçu','BA'),(263,'Cachoeira','BA'),(264,'Caculé','BA'),(265,'Caém','BA'),(266,'Caetanos','BA'),(267,'Caetité','BA'),(268,'Cafarnaum','BA'),(269,'Cairu','BA'),(270,'Caldeirão Grande','BA'),(271,'Camacan','BA'),(272,'Camaçari','BA'),(273,'Camamu','BA'),(274,'Campo Alegre de Lourdes','BA'),(275,'Campo Formoso','BA'),(276,'Canápolis','BA'),(277,'Canarana','BA'),(278,'Canavieiras','BA'),(279,'Candeal','BA'),(280,'Candeias','BA'),(281,'Candiba','BA'),(282,'Cândido Sales','BA'),(283,'Cansanção','BA'),(284,'Canudos','BA'),(285,'Capela do Alto Alegre','BA'),(286,'Capim Grosso','BA'),(287,'Caraíbas','BA'),(288,'Caravelas','BA'),(289,'Cardeal da Silva','BA'),(290,'Carinhanha','BA'),(291,'Casa Nova','BA'),(292,'Castro Alves','BA'),(293,'Catolândia','BA'),(294,'Catu','BA'),(295,'Caturama','BA'),(296,'Central','BA'),(297,'Chorrochó','BA'),(298,'Cícero Dantas','BA'),(299,'Cipó','BA'),(300,'Coaraci','BA'),(301,'Cocos','BA'),(302,'Conceição da Feira','BA'),(303,'Conceição do Almeida','BA'),(304,'Conceição do Coité','BA'),(305,'Conceição do Jacuípe','BA'),(306,'Conde','BA'),(307,'Condeúba','BA'),(308,'Contendas do Sincorá','BA'),(309,'Coração de Maria','BA'),(310,'Cordeiros','BA'),(311,'Coribe','BA'),(312,'Coronel João Sá','BA'),(313,'Correntina','BA'),(314,'Cotegipe','BA'),(315,'Cravolândia','BA'),(316,'Crisópolis','BA'),(317,'Cristópolis','BA'),(318,'Cruz das Almas','BA'),(319,'Curaçá','BA'),(320,'Dário Meira','BA'),(321,'Dias d\'Ávila','BA'),(322,'Dom Basílio','BA'),(323,'Dom Macedo Costa','BA'),(324,'Elísio Medrado','BA'),(325,'Encruzilhada','BA'),(326,'Entre Rios','BA'),(327,'Érico Cardoso','BA'),(328,'Esplanada','BA'),(329,'Euclides da Cunha','BA'),(330,'Eunápolis','BA'),(331,'Fátima','BA'),(332,'Feira da Mata','BA'),(333,'Feira de Santana','BA'),(334,'Filadélfia','BA'),(335,'Firmino Alves','BA'),(336,'Floresta Azul','BA'),(337,'Formosa do Rio Preto','BA'),(338,'Gandu','BA'),(339,'Gavião','BA'),(340,'Gentio do Ouro','BA'),(341,'Glória','BA'),(342,'Gongogi','BA'),(343,'Governador Mangabeira','BA'),(344,'Guajeru','BA'),(345,'Guanambi','BA'),(346,'Guaratinga','BA'),(347,'Heliópolis','BA'),(348,'Iaçu','BA'),(349,'Ibiassucê','BA'),(350,'Ibicaraí','BA'),(351,'Ibicoara','BA'),(352,'Ibicuí','BA'),(353,'Ibipeba','BA'),(354,'Ibipitanga','BA'),(355,'Ibiquera','BA'),(356,'Ibirapitanga','BA'),(357,'Ibirapuã','BA'),(358,'Ibirataia','BA'),(359,'Ibitiara','BA'),(360,'Ibititá','BA'),(361,'Ibotirama','BA'),(362,'Ichu','BA'),(363,'Igaporã','BA'),(364,'Igrapiúna','BA'),(365,'Iguaí','BA'),(366,'Ilhéus','BA'),(367,'Inhambupe','BA'),(368,'Ipecaetá','BA'),(369,'Ipiaú','BA'),(370,'Ipirá','BA'),(371,'Ipupiara','BA'),(372,'Irajuba','BA'),(373,'Iramaia','BA'),(374,'Iraquara','BA'),(375,'Irará','BA'),(376,'Irecê','BA'),(377,'Itabela','BA'),(378,'Itaberaba','BA'),(379,'Itabuna','BA'),(380,'Itacaré','BA'),(381,'Itaeté','BA'),(382,'Itagi','BA'),(383,'Itagibá','BA'),(384,'Itagimirim','BA'),(385,'Itaguaçu da Bahia','BA'),(386,'Itaju do Colônia','BA'),(387,'Itajuípe','BA'),(388,'Itamaraju','BA'),(389,'Itamari','BA'),(390,'Itambé','BA'),(391,'Itanagra','BA'),(392,'Itanhém','BA'),(393,'Itaparica','BA'),(394,'Itapé','BA'),(395,'Itapebi','BA'),(396,'Itapetinga','BA'),(397,'Itapicuru','BA'),(398,'Itapitanga','BA'),(399,'Itaquara','BA'),(400,'Itarantim','BA'),(401,'Itatim','BA'),(402,'Itiruçu','BA'),(403,'Itiúba','BA'),(404,'Itororó','BA'),(405,'Ituaçu','BA'),(406,'Ituberá','BA'),(407,'Iuiú','BA'),(408,'Jaborandi','BA'),(409,'Jacaraci','BA'),(410,'Jacobina','BA'),(411,'Jaguaquara','BA'),(412,'Jaguarari','BA'),(413,'Jaguaripe','BA'),(414,'Jandaíra','BA'),(415,'Jequié','BA'),(416,'Jeremoabo','BA'),(417,'Jiquiriçá','BA'),(418,'Jitaúna','BA'),(419,'João Dourado','BA'),(420,'Juazeiro','BA'),(421,'Jucuruçu','BA'),(422,'Jussara','BA'),(423,'Jussari','BA'),(424,'Jussiape','BA'),(425,'Lafaiete Coutinho','BA'),(426,'Lagoa Real','BA'),(427,'Laje','BA'),(428,'Lajedão','BA'),(429,'Lajedinho','BA'),(430,'Lajedo do Tabocal','BA'),(431,'Lamarão','BA'),(432,'Lapão','BA'),(433,'Lauro de Freitas','BA'),(434,'Lençóis','BA'),(435,'Licínio de Almeida','BA'),(436,'Livramento de Nossa Senhora','BA'),(437,'Luís Eduardo Magalhães','BA'),(438,'Macajuba','BA'),(439,'Macarani','BA'),(440,'Macaúbas','BA'),(441,'Macururé','BA'),(442,'Madre de Deus','BA'),(443,'Maetinga','BA'),(444,'Maiquinique','BA'),(445,'Mairi','BA'),(446,'Malhada','BA'),(447,'Malhada de Pedras','BA'),(448,'Manoel Vitorino','BA'),(449,'Mansidão','BA'),(450,'Maracás','BA'),(451,'Maragogipe','BA'),(452,'Maraú','BA'),(453,'Marcionílio Souza','BA'),(454,'Mascote','BA'),(455,'Mata de São João','BA'),(456,'Matina','BA'),(457,'Medeiros Neto','BA'),(458,'Miguel Calmon','BA'),(459,'Milagres','BA'),(460,'Mirangaba','BA'),(461,'Mirante','BA'),(462,'Monte Santo','BA'),(463,'Morpará','BA'),(464,'Morro do Chapéu','BA'),(465,'Mortugaba','BA'),(466,'Mucugê','BA'),(467,'Mucuri','BA'),(468,'Mulungu do Morro','BA'),(469,'Mundo Novo','BA'),(470,'Muniz Ferreira','BA'),(471,'Muquém de São Francisco','BA'),(472,'Muritiba','BA'),(473,'Mutuípe','BA'),(474,'Nazaré','BA'),(475,'Nilo Peçanha','BA'),(476,'Nordestina','BA'),(477,'Nova Canaã','BA'),(478,'Nova Fátima','BA'),(479,'Nova Ibiá','BA'),(480,'Nova Itarana','BA'),(481,'Nova Redenção','BA'),(482,'Nova Soure','BA'),(483,'Nova Viçosa','BA'),(484,'Novo Horizonte','BA'),(485,'Novo Triunfo','BA'),(486,'Olindina','BA'),(487,'Oliveira dos Brejinhos','BA'),(488,'Ouriçangas','BA'),(489,'Ourolândia','BA'),(490,'Palmas de Monte Alto','BA'),(491,'Palmeiras','BA'),(492,'Paramirim','BA'),(493,'Paratinga','BA'),(494,'Paripiranga','BA'),(495,'Pau Brasil','BA'),(496,'Paulo Afonso','BA'),(497,'Pé de Serra','BA'),(498,'Pedrão','BA'),(499,'Pedro Alexandre','BA'),(500,'Piatã','BA'),(501,'Pilão Arcado','BA'),(502,'Pindaí','BA'),(503,'Pindobaçu','BA'),(504,'Pintadas','BA'),(505,'Piraí do Norte','BA'),(506,'Piripá','BA'),(507,'Piritiba','BA'),(508,'Planaltino','BA'),(509,'Planalto','BA'),(510,'Poções','BA'),(511,'Pojuca','BA'),(512,'Ponto Novo','BA'),(513,'Porto Seguro','BA'),(514,'Potiraguá','BA'),(515,'Prado','BA'),(516,'Presidente Dutra','BA'),(517,'Presidente Jânio Quadros','BA'),(518,'Presidente Tancredo Neves','BA'),(519,'Queimadas','BA'),(520,'Quijingue','BA'),(521,'Quixabeira','BA'),(522,'Rafael Jambeiro','BA'),(523,'Remanso','BA'),(524,'Retirolândia','BA'),(525,'Riachão das Neves','BA'),(526,'Riachão do Jacuípe','BA'),(527,'Riacho de Santana','BA'),(528,'Ribeira do Amparo','BA'),(529,'Ribeira do Pombal','BA'),(530,'Ribeirão do Largo','BA'),(531,'Rio de Contas','BA'),(532,'Rio do Antônio','BA'),(533,'Rio do Pires','BA'),(534,'Rio Real','BA'),(535,'Rodelas','BA'),(536,'Ruy Barbosa','BA'),(537,'Salinas da Margarida','BA'),(538,'Salvador','BA'),(539,'Santa Bárbara','BA'),(540,'Santa Brígida','BA'),(541,'Santa Cruz Cabrália','BA'),(542,'Santa Cruz da Vitória','BA'),(543,'Santa Inês','BA'),(544,'Santa Luzia','BA'),(545,'Santa Maria da Vitória','BA'),(546,'Santa Rita de Cássia','BA'),(547,'Santa Teresinha','BA'),(548,'Santaluz','BA'),(549,'Santana','BA'),(550,'Santanópolis','BA'),(551,'Santo Amaro','BA'),(552,'Santo Antônio de Jesus','BA'),(553,'Santo Estêvão','BA'),(554,'São Desidério','BA'),(555,'São Domingos','BA'),(556,'São Felipe','BA'),(557,'São Félix','BA'),(558,'São Félix do Coribe','BA'),(559,'São Francisco do Conde','BA'),(560,'São Gabriel','BA'),(561,'São Gonçalo dos Campos','BA'),(562,'São José da Vitória','BA'),(563,'São José do Jacuípe','BA'),(564,'São Miguel das Matas','BA'),(565,'São Sebastião do Passé','BA'),(566,'Sapeaçu','BA'),(567,'Sátiro Dias','BA'),(568,'Saubara','BA'),(569,'Saúde','BA'),(570,'Seabra','BA'),(571,'Sebastião Laranjeiras','BA'),(572,'Senhor do Bonfim','BA'),(573,'Sento Sé','BA'),(574,'Serra do Ramalho','BA'),(575,'Serra Dourada','BA'),(576,'Serra Preta','BA'),(577,'Serrinha','BA'),(578,'Serrolândia','BA'),(579,'Simões Filho','BA'),(580,'Sítio do Mato','BA'),(581,'Sítio do Quinto','BA'),(582,'Sobradinho','BA'),(583,'Souto Soares','BA'),(584,'Tabocas do Brejo Velho','BA'),(585,'Tanhaçu','BA'),(586,'Tanque Novo','BA'),(587,'Tanquinho','BA'),(588,'Taperoá','BA'),(589,'Tapiramutá','BA'),(590,'Teixeira de Freitas','BA'),(591,'Teodoro Sampaio','BA'),(592,'Teofilândia','BA'),(593,'Teolândia','BA'),(594,'Terra Nova','BA'),(595,'Tremedal','BA'),(596,'Tucano','BA'),(597,'Uauá','BA'),(598,'Ubaíra','BA'),(599,'Ubaitaba','BA'),(600,'Ubatã','BA'),(601,'Uibaí','BA'),(602,'Umburanas','BA'),(603,'Una','BA'),(604,'Urandi','BA'),(605,'Uruçuca','BA'),(606,'Utinga','BA'),(607,'Valença','BA'),(608,'Valente','BA'),(609,'Várzea da Roça','BA'),(610,'Várzea do Poço','BA'),(611,'Várzea Nova','BA'),(612,'Varzedo','BA'),(613,'Vera Cruz','BA'),(614,'Vereda','BA'),(615,'Vitória da Conquista','BA'),(616,'Wagner','BA'),(617,'Wanderley','BA'),(618,'Wenceslau Guimarães','BA'),(619,'Xique-Xique','BA'),(620,'Abaiara','CE'),(621,'Acarapé','CE'),(622,'Acaraú','CE'),(623,'Acopiara','CE'),(624,'Aiuaba','CE'),(625,'Alcântaras','CE'),(626,'Altaneira','CE'),(627,'Alto Santo','CE'),(628,'Amontada','CE'),(629,'Antonina do Norte','CE'),(630,'Apuiarés','CE'),(631,'Aquiraz','CE'),(632,'Aracati','CE'),(633,'Aracoiaba','CE'),(634,'Ararendá','CE'),(635,'Araripe','CE'),(636,'Aratuba','CE'),(637,'Arneiroz','CE'),(638,'Assaré','CE'),(639,'Aurora','CE'),(640,'Baixio','CE'),(641,'Banabuiú','CE'),(642,'Barbalha','CE'),(643,'Barreira','CE'),(644,'Barro','CE'),(645,'Barroquinha','CE'),(646,'Baturité','CE'),(647,'Beberibe','CE'),(648,'Bela Cruz','CE'),(649,'Boa Viagem','CE'),(650,'Brejo Santo','CE'),(651,'Camocim','CE'),(652,'Campos Sales','CE'),(653,'Canindé','CE'),(654,'Capistrano','CE'),(655,'Caridade','CE'),(656,'Cariré','CE'),(657,'Caririaçu','CE'),(658,'Cariús','CE'),(659,'Carnaubal','CE'),(660,'Cascavel','CE'),(661,'Catarina','CE'),(662,'Catunda','CE'),(663,'Caucaia','CE'),(664,'Cedro','CE'),(665,'Chaval','CE'),(666,'Choró','CE'),(667,'Chorozinho','CE'),(668,'Coreaú','CE'),(669,'Crateús','CE'),(670,'Crato','CE'),(671,'Croatá','CE'),(672,'Cruz','CE'),(673,'Deputado Irapuan Pinheiro','CE'),(674,'Ererê','CE'),(675,'Eusébio','CE'),(676,'Farias Brito','CE'),(677,'Forquilha','CE'),(678,'Fortaleza','CE'),(679,'Fortim','CE'),(680,'Frecheirinha','CE'),(681,'General Sampaio','CE'),(682,'Graça','CE'),(683,'Granja','CE'),(684,'Granjeiro','CE'),(685,'Groaíras','CE'),(686,'Guaiúba','CE'),(687,'Guaraciaba do Norte','CE'),(688,'Guaramiranga','CE'),(689,'Hidrolândia','CE'),(690,'Horizonte','CE'),(691,'Ibaretama','CE'),(692,'Ibiapina','CE'),(693,'Ibicuitinga','CE'),(694,'Icapuí','CE'),(695,'Icó','CE'),(696,'Iguatu','CE'),(697,'Independência','CE'),(698,'Ipaporanga','CE'),(699,'Ipaumirim','CE'),(700,'Ipu','CE'),(701,'Ipueiras','CE'),(702,'Iracema','CE'),(703,'Irauçuba','CE'),(704,'Itaiçaba','CE'),(705,'Itaitinga','CE'),(706,'Itapagé','CE'),(707,'Itapipoca','CE'),(708,'Itapiúna','CE'),(709,'Itarema','CE'),(710,'Itatira','CE'),(711,'Jaguaretama','CE'),(712,'Jaguaribara','CE'),(713,'Jaguaribe','CE'),(714,'Jaguaruana','CE'),(715,'Jardim','CE'),(716,'Jati','CE'),(717,'Jijoca de Jericoaroara','CE'),(718,'Juazeiro do Norte','CE'),(719,'Jucás','CE'),(720,'Lavras da Mangabeira','CE'),(721,'Limoeiro do Norte','CE'),(722,'Madalena','CE'),(723,'Maracanaú','CE'),(724,'Maranguape','CE'),(725,'Marco','CE'),(726,'Martinópole','CE'),(727,'Massapê','CE'),(728,'Mauriti','CE'),(729,'Meruoca','CE'),(730,'Milagres','CE'),(731,'Milhã','CE'),(732,'Miraíma','CE'),(733,'Missão Velha','CE'),(734,'Mombaça','CE'),(735,'Monsenhor Tabosa','CE'),(736,'Morada Nova','CE'),(737,'Moraújo','CE'),(738,'Morrinhos','CE'),(739,'Mucambo','CE'),(740,'Mulungu','CE'),(741,'Nova Olinda','CE'),(742,'Nova Russas','CE'),(743,'Novo Oriente','CE'),(744,'Ocara','CE'),(745,'Orós','CE'),(746,'Pacajus','CE'),(747,'Pacatuba','CE'),(748,'Pacoti','CE'),(749,'Pacujá','CE'),(750,'Palhano','CE'),(751,'Palmácia','CE'),(752,'Paracuru','CE'),(753,'Paraipaba','CE'),(754,'Parambu','CE'),(755,'Paramoti','CE'),(756,'Pedra Branca','CE'),(757,'Penaforte','CE'),(758,'Pentecoste','CE'),(759,'Pereiro','CE'),(760,'Pindoretama','CE'),(761,'Piquet Carneiro','CE'),(762,'Pires Ferreira','CE'),(763,'Poranga','CE'),(764,'Porteiras','CE'),(765,'Potengi','CE'),(766,'Potiretama','CE'),(767,'Quiterianópolis','CE'),(768,'Quixadá','CE'),(769,'Quixelô','CE'),(770,'Quixeramobim','CE'),(771,'Quixeré','CE'),(772,'Redenção','CE'),(773,'Reriutaba','CE'),(774,'Russas','CE'),(775,'Saboeiro','CE'),(776,'Salitre','CE'),(777,'Santa Quitéria','CE'),(778,'Santana do Acaraú','CE'),(779,'Santana do Cariri','CE'),(780,'São Benedito','CE'),(781,'São Gonçalo do Amarante','CE'),(782,'São João do Jaguaribe','CE'),(783,'São Luís do Curu','CE'),(784,'Senador Pompeu','CE'),(785,'Senador Sá','CE'),(786,'Sobral','CE'),(787,'Solonópole','CE'),(788,'Tabuleiro do Norte','CE'),(789,'Tamboril','CE'),(790,'Tarrafas','CE'),(791,'Tauá','CE'),(792,'Tejuçuoca','CE'),(793,'Tianguá','CE'),(794,'Trairi','CE'),(795,'Tururu','CE'),(796,'Ubajara','CE'),(797,'Umari','CE'),(798,'Umirim','CE'),(799,'Uruburetama','CE'),(800,'Uruoca','CE'),(801,'Varjota','CE'),(802,'Várzea Alegre','CE'),(803,'Viçosa do Ceará','CE'),(804,'Brasília','DF'),(805,'Afonso Cláudio','ES'),(806,'Água Doce do Norte','ES'),(807,'Águia Branca','ES'),(808,'Alegre','ES'),(809,'Alfredo Chaves','ES'),(810,'Alto Rio Novo','ES'),(811,'Anchieta','ES'),(812,'Apiacá','ES'),(813,'Aracruz','ES'),(814,'Atilio Vivacqua','ES'),(815,'Baixo Guandu','ES'),(816,'Barra de São Francisco','ES'),(817,'Boa Esperança','ES'),(818,'Bom Jesus do Norte','ES'),(819,'Brejetuba','ES'),(820,'Cachoeiro de Itapemirim','ES'),(821,'Cariacica','ES'),(822,'Castelo','ES'),(823,'Colatina','ES'),(824,'Conceição da Barra','ES'),(825,'Conceição do Castelo','ES'),(826,'Divino de São Lourenço','ES'),(827,'Domingos Martins','ES'),(828,'Dores do Rio Preto','ES'),(829,'Ecoporanga','ES'),(830,'Fundão','ES'),(831,'Governador Lindenberg','ES'),(832,'Guaçuí','ES'),(833,'Guarapari','ES'),(834,'Ibatiba','ES'),(835,'Ibiraçu','ES'),(836,'Ibitirama','ES'),(837,'Iconha','ES'),(838,'Irupi','ES'),(839,'Itaguaçu','ES'),(840,'Itapemirim','ES'),(841,'Itarana','ES'),(842,'Iúna','ES'),(843,'Jaguaré','ES'),(844,'Jerônimo Monteiro','ES'),(845,'João Neiva','ES'),(846,'Laranja da Terra','ES'),(847,'Linhares','ES'),(848,'Mantenópolis','ES'),(849,'Marataizes','ES'),(850,'Marechal Floriano','ES'),(851,'Marilândia','ES'),(852,'Mimoso do Sul','ES'),(853,'Montanha','ES'),(854,'Mucurici','ES'),(855,'Muniz Freire','ES'),(856,'Muqui','ES'),(857,'Nova Venécia','ES'),(858,'Pancas','ES'),(859,'Pedro Canário','ES'),(860,'Pinheiros','ES'),(861,'Piúma','ES'),(862,'Ponto Belo','ES'),(863,'Presidente Kennedy','ES'),(864,'Rio Bananal','ES'),(865,'Rio Novo do Sul','ES'),(866,'Santa Leopoldina','ES'),(867,'Santa Maria de Jetibá','ES'),(868,'Santa Teresa','ES'),(869,'São Domingos do Norte','ES'),(870,'São Gabriel da Palha','ES'),(871,'São José do Calçado','ES'),(872,'São Mateus','ES'),(873,'São Roque do Canaã','ES'),(874,'Serra','ES'),(875,'Sooretama','ES'),(876,'Vargem Alta','ES'),(877,'Venda Nova do Imigrante','ES'),(878,'Viana','ES'),(879,'Vila Pavão','ES'),(880,'Vila Valério','ES'),(881,'Vila Velha','ES'),(882,'Vitória','ES'),(883,'Abadia de Goiás','GO'),(884,'Abadiânia','GO'),(885,'Acreúna','GO'),(886,'Adelândia','GO'),(887,'Água Fria de Goiás','GO'),(888,'Água Limpa','GO'),(889,'Águas Lindas de Goiás','GO'),(890,'Alexânia','GO'),(891,'Aloândia','GO'),(892,'Alto Horizonte','GO'),(893,'Alto Paraíso de Goiás','GO'),(894,'Alvorada do Norte','GO'),(895,'Amaralina','GO'),(896,'Americano do Brasil','GO'),(897,'Amorinópolis','GO'),(898,'Anápolis','GO'),(899,'Anhanguera','GO'),(900,'Anicuns','GO'),(901,'Aparecida de Goiânia','GO'),(902,'Aparecida do Rio Doce','GO'),(903,'Aporé','GO'),(904,'Araçu','GO'),(905,'Aragarças','GO'),(906,'Aragoiânia','GO'),(907,'Araguapaz','GO'),(908,'Arenópolis','GO'),(909,'Aruanã','GO'),(910,'Aurilândia','GO'),(911,'Avelinópolis','GO'),(912,'Baliza','GO'),(913,'Barro Alto','GO'),(914,'Bela Vista de Goiás','GO'),(915,'Bom Jardim de Goiás','GO'),(916,'Bom Jesus de Goiás','GO'),(917,'Bonfinópolis','GO'),(918,'Bonópolis','GO'),(919,'Brazabrantes','GO'),(920,'Britânia','GO'),(921,'Buriti Alegre','GO'),(922,'Buriti de Goiás','GO'),(923,'Buritinópolis','GO'),(924,'Cabeceiras','GO'),(925,'Cachoeira Alta','GO'),(926,'Cachoeira de Goiás','GO'),(927,'Cachoeira Dourada','GO'),(928,'Caçu','GO'),(929,'Caiapônia','GO'),(930,'Caldas Novas','GO'),(931,'Caldazinha','GO'),(932,'Campestre de Goiás','GO'),(933,'Campinaçu','GO'),(934,'Campinorte','GO'),(935,'Campo Alegre de Goiás','GO'),(936,'Campos Limpo de Goiás','GO'),(937,'Campos Belos','GO'),(938,'Campos Verdes','GO'),(939,'Carmo do Rio Verde','GO'),(940,'Castelândia','GO'),(941,'Catalão','GO'),(942,'Caturaí','GO'),(943,'Cavalcante','GO'),(944,'Ceres','GO'),(945,'Cezarina','GO'),(946,'Chapadão do Céu','GO'),(947,'Cidade Ocidental','GO'),(948,'Cocalzinho de Goiás','GO'),(949,'Colinas do Sul','GO'),(950,'Córrego do Ouro','GO'),(951,'Corumbá de Goiás','GO'),(952,'Corumbaíba','GO'),(953,'Cristalina','GO'),(954,'Cristianópolis','GO'),(955,'Crixás','GO'),(956,'Cromínia','GO'),(957,'Cumari','GO'),(958,'Damianópolis','GO'),(959,'Damolândia','GO'),(960,'Davinópolis','GO'),(961,'Diorama','GO'),(962,'Divinópolis de Goiás','GO'),(963,'Doverlândia','GO'),(964,'Edealina','GO'),(965,'Edéia','GO'),(966,'Estrela do Norte','GO'),(967,'Faina','GO'),(968,'Fazenda Nova','GO'),(969,'Firminópolis','GO'),(970,'Flores de Goiás','GO'),(971,'Formosa','GO'),(972,'Formoso','GO'),(973,'Gameleira de Goiás','GO'),(974,'Goianápolis','GO'),(975,'Goiandira','GO'),(976,'Goianésia','GO'),(977,'Goiânia','GO'),(978,'Goianira','GO'),(979,'Goiás','GO'),(980,'Goiatuba','GO'),(981,'Gouvelândia','GO'),(982,'Guapó','GO'),(983,'Guaraíta','GO'),(984,'Guarani de Goiás','GO'),(985,'Guarinos','GO'),(986,'Heitoraí','GO'),(987,'Hidrolândia','GO'),(988,'Hidrolina','GO'),(989,'Iaciara','GO'),(990,'Inaciolândia','GO'),(991,'Indiara','GO'),(992,'Inhumas','GO'),(993,'Ipameri','GO'),(994,'Ipiranga de Goiás','GO'),(995,'Iporá','GO'),(996,'Israelândia','GO'),(997,'Itaberaí','GO'),(998,'Itaguari','GO'),(999,'Itaguaru','GO'),(1000,'Itajá','GO'),(1001,'Itapaci','GO'),(1002,'Itapirapuã','GO'),(1003,'Itapuranga','GO'),(1004,'Itarumã','GO'),(1005,'Itauçu','GO'),(1006,'Itumbiara','GO'),(1007,'Ivolândia','GO'),(1008,'Jandaia','GO'),(1009,'Jaraguá','GO'),(1010,'Jataí','GO'),(1011,'Jaupaci','GO'),(1012,'Jesúpolis','GO'),(1013,'Joviânia','GO'),(1014,'Jussara','GO'),(1015,'Lagoa Santa','GO'),(1016,'Leopoldo de Bulhões','GO'),(1017,'Luziânia','GO'),(1018,'Mairipotaba','GO'),(1019,'Mambaí','GO'),(1020,'Mara Rosa','GO'),(1021,'Marzagão','GO'),(1022,'Matrinchã','GO'),(1023,'Maurilândia','GO'),(1024,'Mimoso de Goiás','GO'),(1025,'Minaçu','GO'),(1026,'Mineiros','GO'),(1027,'Moiporá','GO'),(1028,'Monte Alegre de Goiás','GO'),(1029,'Montes Claros de Goiás','GO'),(1030,'Montividiu','GO'),(1031,'Montividiu do Norte','GO'),(1032,'Morrinhos','GO'),(1033,'Morro Agudo de Goiás','GO'),(1034,'Mossâmedes','GO'),(1035,'Mozarlândia','GO'),(1036,'Mundo Novo','GO'),(1037,'Mutunópolis','GO'),(1038,'Nazário','GO'),(1039,'Nerópolis','GO'),(1040,'Niquelândia','GO'),(1041,'Nova América','GO'),(1042,'Nova Aurora','GO'),(1043,'Nova Crixás','GO'),(1044,'Nova Glória','GO'),(1045,'Nova Iguaçu de Goiás','GO'),(1046,'Nova Roma','GO'),(1047,'Nova Veneza','GO'),(1048,'Novo Brasil','GO'),(1049,'Novo Gama','GO'),(1050,'Novo Planalto','GO'),(1051,'Orizona','GO'),(1052,'Ouro Verde de Goiás','GO'),(1053,'Ouvidor','GO'),(1054,'Padre Bernardo','GO'),(1055,'Palestina de Goiás','GO'),(1056,'Palmeiras de Goiás','GO'),(1057,'Palmelo','GO'),(1058,'Palminópolis','GO'),(1059,'Panamá','GO'),(1060,'Paranaiguara','GO'),(1061,'Paraúna','GO'),(1062,'Perolândia','GO'),(1063,'Petrolina de Goiás','GO'),(1064,'Pilar de Goiás','GO'),(1065,'Piracanjuba','GO'),(1066,'Piranhas','GO'),(1067,'Pirenópolis','GO'),(1068,'Pires do Rio','GO'),(1069,'Planaltina','GO'),(1070,'Pontalina','GO'),(1071,'Porangatu','GO'),(1072,'Porteirão','GO'),(1073,'Portelândia','GO'),(1074,'Posse','GO'),(1075,'Professor Jamil','GO'),(1076,'Quirinópolis','GO'),(1077,'Rialma','GO'),(1078,'Rianápolis','GO'),(1079,'Rio Quente','GO'),(1080,'Rio Verde','GO'),(1081,'Rubiataba','GO'),(1082,'Sanclerlândia','GO'),(1083,'Santa Bárbara de Goiás','GO'),(1084,'Santa Cruz de Goiás','GO'),(1085,'Santa Fé de Goiás','GO'),(1086,'Santa Helena de Goiás','GO'),(1087,'Santa Isabel','GO'),(1088,'Santa Rita do Araguaia','GO'),(1089,'Santa Rita do Novo Destino','GO'),(1090,'Santa Rosa de Goiás','GO'),(1091,'Santa Tereza de Goiás','GO'),(1092,'Santa Terezinha de Goiás','GO'),(1093,'Santo Antônio da Barra','GO'),(1094,'Santo Antônio de Goiás','GO'),(1095,'Santo Antônio do Descoberto','GO'),(1096,'São Domingos','GO'),(1097,'São Francisco de Goiás','GO'),(1098,'São João d\'Aliança','GO'),(1099,'São João da Paraúna','GO'),(1100,'São Luís de Montes Belos','GO'),(1101,'São Luíz do Norte','GO'),(1102,'São Miguel do Araguaia','GO'),(1103,'São Miguel do Passa Quatro','GO'),(1104,'São Patrício','GO'),(1105,'São Simão','GO'),(1106,'Senador Canedo','GO'),(1107,'Serranópolis','GO'),(1108,'Silvânia','GO'),(1109,'Simolândia','GO'),(1110,'Sítio d\'Abadia','GO'),(1111,'Taquaral de Goiás','GO'),(1112,'Teresina de Goiás','GO'),(1113,'Terezópolis de Goiás','GO'),(1114,'Três Ranchos','GO'),(1115,'Trindade','GO'),(1116,'Trombas','GO'),(1117,'Turvânia','GO'),(1118,'Turvelândia','GO'),(1119,'Uirapuru','GO'),(1120,'Uruaçu','GO'),(1121,'Uruana','GO'),(1122,'Urutaí','GO'),(1123,'Valparaíso de Goiás','GO'),(1124,'Varjão','GO'),(1125,'Vianópolis','GO'),(1126,'Vicentinópolis','GO'),(1127,'Vila Boa','GO'),(1128,'Vila Propício','GO'),(1129,'Açailândia','MA'),(1130,'Afonso Cunha','MA'),(1131,'Água Doce do Maranhão','MA'),(1132,'Alcântara','MA'),(1133,'Aldeias Altas','MA'),(1134,'Altamira do Maranhão','MA'),(1135,'Alto Alegre do Maranhão','MA'),(1136,'Alto Alegre do Pindaré','MA'),(1137,'Alto Parnaíba','MA'),(1138,'Amapá do Maranhão','MA'),(1139,'Amarante do Maranhão','MA'),(1140,'Anajatuba','MA'),(1141,'Anapurus','MA'),(1142,'Apicum-Açu','MA'),(1143,'Araguanã','MA'),(1144,'Araioses','MA'),(1145,'Arame','MA'),(1146,'Arari','MA'),(1147,'Axixá','MA'),(1148,'Bacabal','MA'),(1149,'Bacabeira','MA'),(1150,'Bacuri','MA'),(1151,'Bacurituba','MA'),(1152,'Balsas','MA'),(1153,'Barão de Grajaú','MA'),(1154,'Barra do Corda','MA'),(1155,'Barreirinhas','MA'),(1156,'Bela Vista do Maranhão','MA'),(1157,'Belágua','MA'),(1158,'Benedito Leite','MA'),(1159,'Bequimão','MA'),(1160,'Bernardo do Mearim','MA'),(1161,'Boa Vista do Gurupi','MA'),(1162,'Bom Jardim','MA'),(1163,'Bom Jesus das Selvas','MA'),(1164,'Bom Lugar','MA'),(1165,'Brejo','MA'),(1166,'Brejo de Areia','MA'),(1167,'Buriti','MA'),(1168,'Buriti Bravo','MA'),(1169,'Buriticupu','MA'),(1170,'Buritirana','MA'),(1171,'Cachoeira Grande','MA'),(1172,'Cajapió','MA'),(1173,'Cajari','MA'),(1174,'Campestre do Maranhão','MA'),(1175,'Cândido Mendes','MA'),(1176,'Cantanhede','MA'),(1177,'Capinzal do Norte','MA'),(1178,'Carolina','MA'),(1179,'Carutapera','MA'),(1180,'Caxias','MA'),(1181,'Cedral','MA'),(1182,'Central do Maranhão','MA'),(1183,'Centro do Guilherme','MA'),(1184,'Centro Novo do Maranhão','MA'),(1185,'Chapadinha','MA'),(1186,'Cidelândia','MA'),(1187,'Codó','MA'),(1188,'Coelho Neto','MA'),(1189,'Colinas','MA'),(1190,'Conceição do Lago-Açu','MA'),(1191,'Coroatá','MA'),(1192,'Cururupu','MA'),(1193,'Davinópolis','MA'),(1194,'Dom Pedro','MA'),(1195,'Duque Bacelar','MA'),(1196,'Esperantinópolis','MA'),(1197,'Estreito','MA'),(1198,'Feira Nova do Maranhão','MA'),(1199,'Fernando Falcão','MA'),(1200,'Formosa da Serra Negra','MA'),(1201,'Fortaleza dos Nogueiras','MA'),(1202,'Fortuna','MA'),(1203,'Godofredo Viana','MA'),(1204,'Gonçalves Dias','MA'),(1205,'Governador Archer','MA'),(1206,'Governador Edison Lobão','MA'),(1207,'Governador Eugênio Barros','MA'),(1208,'Governador Luiz Rocha','MA'),(1209,'Governador Newton Bello','MA'),(1210,'Governador Nunes Freire','MA'),(1211,'Graça Aranha','MA'),(1212,'Grajaú','MA'),(1213,'Guimarães','MA'),(1214,'Humberto de Campos','MA'),(1215,'Icatu','MA'),(1216,'Igarapé do Meio','MA'),(1217,'Igarapé Grande','MA'),(1218,'Imperatriz','MA'),(1219,'Itaipava do Grajaú','MA'),(1220,'Itapecuru Mirim','MA'),(1221,'Itinga do Maranhão','MA'),(1222,'Jatobá','MA'),(1223,'Jenipapo dos Vieiras','MA'),(1224,'João Lisboa','MA'),(1225,'Joselândia','MA'),(1226,'Junco do Maranhão','MA'),(1227,'Lago da Pedra','MA'),(1228,'Lago do Junco','MA'),(1229,'Lago dos Rodrigues','MA'),(1230,'Lago Verde','MA'),(1231,'Lagoa do Mato','MA'),(1232,'Lagoa Grande do Maranhão','MA'),(1233,'Lajeado Novo','MA'),(1234,'Lima Campos','MA'),(1235,'Loreto','MA'),(1236,'Luís Domingues','MA'),(1237,'Magalhães de Almeida','MA'),(1238,'Maracaçumé','MA'),(1239,'Marajá do Sena','MA'),(1240,'Maranhãozinho','MA'),(1241,'Mata Roma','MA'),(1242,'Matinha','MA'),(1243,'Matões','MA'),(1244,'Matões do Norte','MA'),(1245,'Milagres do Maranhão','MA'),(1246,'Mirador','MA'),(1247,'Miranda do Norte','MA'),(1248,'Mirinzal','MA'),(1249,'Monção','MA'),(1250,'Montes Altos','MA'),(1251,'Morros','MA'),(1252,'Nina Rodrigues','MA'),(1253,'Nova Colinas','MA'),(1254,'Nova Iorque','MA'),(1255,'Nova Olinda do Maranhão','MA'),(1256,'Olho d\'Água das Cunhãs','MA'),(1257,'Olinda Nova do Maranhão','MA'),(1258,'Paço do Lumiar','MA'),(1259,'Palmeirândia','MA'),(1260,'Paraibano','MA'),(1261,'Parnarama','MA'),(1262,'Passagem Franca','MA'),(1263,'Pastos Bons','MA'),(1264,'Paulino Neves','MA'),(1265,'Paulo Ramos','MA'),(1266,'Pedreiras','MA'),(1267,'Pedro do Rosário','MA'),(1268,'Penalva','MA'),(1269,'Peri Mirim','MA'),(1270,'Peritoró','MA'),(1271,'Pindaré Mirim','MA'),(1272,'Pinheiro','MA'),(1273,'Pio XII','MA'),(1274,'Pirapemas','MA'),(1275,'Poção de Pedras','MA'),(1276,'Porto Franco','MA'),(1277,'Porto Rico do Maranhão','MA'),(1278,'Presidente Dutra','MA'),(1279,'Presidente Juscelino','MA'),(1280,'Presidente Médici','MA'),(1281,'Presidente Sarney','MA'),(1282,'Presidente Vargas','MA'),(1283,'Primeira Cruz','MA'),(1284,'Raposa','MA'),(1285,'Riachão','MA'),(1286,'Ribamar Fiquene','MA'),(1287,'Rosário','MA'),(1288,'Sambaíba','MA'),(1289,'Santa Filomena do Maranhão','MA'),(1290,'Santa Helena','MA'),(1291,'Santa Inês','MA'),(1292,'Santa Luzia','MA'),(1293,'Santa Luzia do Paruá','MA'),(1294,'Santa Quitéria do Maranhão','MA'),(1295,'Santa Rita','MA'),(1296,'Santana do Maranhão','MA'),(1297,'Santo Amaro do Maranhão','MA'),(1298,'Santo Antônio dos Lopes','MA'),(1299,'São Benedito do Rio Preto','MA'),(1300,'São Bento','MA'),(1301,'São Bernardo','MA'),(1302,'São Domingos do Azeitão','MA'),(1303,'São Domingos do Maranhão','MA'),(1304,'São Félix de Balsas','MA'),(1305,'São Francisco do Brejão','MA'),(1306,'São Francisco do Maranhão','MA'),(1307,'São João Batista','MA'),(1308,'São João do Carú','MA'),(1309,'São João do Paraíso','MA'),(1310,'São João do Soter','MA'),(1311,'São João dos Patos','MA'),(1312,'São José de Ribamar','MA'),(1313,'São José dos Basílios','MA'),(1314,'São Luís','MA'),(1315,'São Luís Gonzaga do Maranhão','MA'),(1316,'São Mateus do Maranhão','MA'),(1317,'São Pedro da Água Branca','MA'),(1318,'São Pedro dos Crentes','MA'),(1319,'São Raimundo das Mangabeiras','MA'),(1320,'São Raimundo do Doca Bezerra','MA'),(1321,'São Roberto','MA'),(1322,'São Vicente Ferrer','MA'),(1323,'Satubinha','MA'),(1324,'Senador Alexandre Costa','MA'),(1325,'Senador La Rocque','MA'),(1326,'Serrano do Maranhão','MA'),(1327,'Sítio Novo','MA'),(1328,'Sucupira do Norte','MA'),(1329,'Sucupira do Riachão','MA'),(1330,'Tasso Fragoso','MA'),(1331,'Timbiras','MA'),(1332,'Timon','MA'),(1333,'Trizidela do Vale','MA'),(1334,'Tufilândia','MA'),(1335,'Tuntum','MA'),(1336,'Turiaçu','MA'),(1337,'Turilândia','MA'),(1338,'Tutóia','MA'),(1339,'Urbano Santos','MA'),(1340,'Vargem Grande','MA'),(1341,'Viana','MA'),(1342,'Vila Nova dos Martírios','MA'),(1343,'Vitória do Mearim','MA'),(1344,'Vitorino Freire','MA'),(1345,'Zé Doca','MA'),(1346,'Abadia dos Dourados','MG'),(1347,'Abaeté','MG'),(1348,'Abre Campo','MG'),(1349,'Acaiaca','MG'),(1350,'Açucena','MG'),(1351,'Água Boa','MG'),(1352,'Água Comprida','MG'),(1353,'Aguanil','MG'),(1354,'Águas Formosas','MG'),(1355,'Águas Vermelhas','MG'),(1356,'Aimorés','MG'),(1357,'Aiuruoca','MG'),(1358,'Alagoa','MG'),(1359,'Albertina','MG'),(1360,'Além Paraíba','MG'),(1361,'Alfenas','MG'),(1362,'Alfredo Vasconcelos','MG'),(1363,'Almenara','MG'),(1364,'Alpercata','MG'),(1365,'Alpinópolis','MG'),(1366,'Alterosa','MG'),(1367,'Alto Caparaó','MG'),(1368,'Alto Jequitibá','MG'),(1369,'Alto Rio Doce','MG'),(1370,'Alvarenga','MG'),(1371,'Alvinópolis','MG'),(1372,'Alvorada de Minas','MG'),(1373,'Amparo do Serra','MG'),(1374,'Andradas','MG'),(1375,'Andrelândia','MG'),(1376,'Angelândia','MG'),(1377,'Antônio Carlos','MG'),(1378,'Antônio Dias','MG'),(1379,'Antônio Prado de Minas','MG'),(1380,'Araçaí','MG'),(1381,'Aracitaba','MG'),(1382,'Araçuaí','MG'),(1383,'Araguari','MG'),(1384,'Arantina','MG'),(1385,'Araponga','MG'),(1386,'Araporã','MG'),(1387,'Arapuá','MG'),(1388,'Araújos','MG'),(1389,'Araxá','MG'),(1390,'Arceburgo','MG'),(1391,'Arcos','MG'),(1392,'Areado','MG'),(1393,'Argirita','MG'),(1394,'Aricanduva','MG'),(1395,'Arinos','MG'),(1396,'Astolfo Dutra','MG'),(1397,'Ataléia','MG'),(1398,'Augusto de Lima','MG'),(1399,'Baependi','MG'),(1400,'Baldim','MG'),(1401,'Bambuí','MG'),(1402,'Bandeira','MG'),(1403,'Bandeira do Sul','MG'),(1404,'Barão de Cocais','MG'),(1405,'Barão de Monte Alto','MG'),(1406,'Barbacena','MG'),(1407,'Barra Longa','MG'),(1408,'Barroso','MG'),(1409,'Bela Vista de Minas','MG'),(1410,'Belmiro Braga','MG'),(1411,'Belo Horizonte','MG'),(1412,'Belo Oriente','MG'),(1413,'Belo Vale','MG'),(1414,'Berilo','MG'),(1415,'Berizal','MG'),(1416,'Bertópolis','MG'),(1417,'Betim','MG'),(1418,'Bias Fortes','MG'),(1419,'Bicas','MG'),(1420,'Biquinhas','MG'),(1421,'Boa Esperança','MG'),(1422,'Bocaina de Minas','MG'),(1423,'Bocaiúva','MG'),(1424,'Bom Despacho','MG'),(1425,'Bom Jardim de Minas','MG'),(1426,'Bom Jesus da Penha','MG'),(1427,'Bom Jesus do Amparo','MG'),(1428,'Bom Jesus do Galho','MG'),(1429,'Bom Repouso','MG'),(1430,'Bom Sucesso','MG'),(1431,'Bonfim','MG'),(1432,'Bonfinópolis de Minas','MG'),(1433,'Bonito de Minas','MG'),(1434,'Borda da Mata','MG'),(1435,'Botelhos','MG'),(1436,'Botumirim','MG'),(1437,'Brás Pires','MG'),(1438,'Brasilândia de Minas','MG'),(1439,'Brasília de Minas','MG'),(1440,'Brasópolis','MG'),(1441,'Braúnas','MG'),(1442,'Brumadinho','MG'),(1443,'Bueno Brandão','MG'),(1444,'Buenópolis','MG'),(1445,'Bugre','MG'),(1446,'Buritis','MG'),(1447,'Buritizeiro','MG'),(1448,'Cabeceira Grande','MG'),(1449,'Cabo Verde','MG'),(1450,'Cachoeira da Prata','MG'),(1451,'Cachoeira de Minas','MG'),(1452,'Cachoeira de Pajeú','MG'),(1453,'Cachoeira Dourada','MG'),(1454,'Caetanópolis','MG'),(1455,'Caeté','MG'),(1456,'Caiana','MG'),(1457,'Cajuri','MG'),(1458,'Caldas','MG'),(1459,'Camacho','MG'),(1460,'Camanducaia','MG'),(1461,'Cambuí','MG'),(1462,'Cambuquira','MG'),(1463,'Campanário','MG'),(1464,'Campanha','MG'),(1465,'Campestre','MG'),(1466,'Campina Verde','MG'),(1467,'Campo Azul','MG'),(1468,'Campo Belo','MG'),(1469,'Campo do Meio','MG'),(1470,'Campo Florido','MG'),(1471,'Campos Altos','MG'),(1472,'Campos Gerais','MG'),(1473,'Cana Verde','MG'),(1474,'Canaã','MG'),(1475,'Canápolis','MG'),(1476,'Candeias','MG'),(1477,'Cantagalo','MG'),(1478,'Caparaó','MG'),(1479,'Capela Nova','MG'),(1480,'Capelinha','MG'),(1481,'Capetinga','MG'),(1482,'Capim Branco','MG'),(1483,'Capinópolis','MG'),(1484,'Capitão Andrade','MG'),(1485,'Capitão Enéas','MG'),(1486,'Capitólio','MG'),(1487,'Caputira','MG'),(1488,'Caraí','MG'),(1489,'Caranaíba','MG'),(1490,'Carandaí','MG'),(1491,'Carangola','MG'),(1492,'Caratinga','MG'),(1493,'Carbonita','MG'),(1494,'Careaçu','MG'),(1495,'Carlos Chagas','MG'),(1496,'Carmésia','MG'),(1497,'Carmo da Cachoeira','MG'),(1498,'Carmo da Mata','MG'),(1499,'Carmo de Minas','MG'),(1500,'Carmo do Cajuru','MG'),(1501,'Carmo do Paranaíba','MG'),(1502,'Carmo do Rio Claro','MG'),(1503,'Carmópolis de Minas','MG'),(1504,'Carneirinho','MG'),(1505,'Carrancas','MG'),(1506,'Carvalhópolis','MG'),(1507,'Carvalhos','MG'),(1508,'Casa Grande','MG'),(1509,'Cascalho Rico','MG'),(1510,'Cássia','MG'),(1511,'Cataguases','MG'),(1512,'Catas Altas','MG'),(1513,'Catas Altas da Noruega','MG'),(1514,'Catuji','MG'),(1515,'Catuti','MG'),(1516,'Caxambu','MG'),(1517,'Cedro do Abaeté','MG'),(1518,'Central de Minas','MG'),(1519,'Centralina','MG'),(1520,'Chácara','MG'),(1521,'Chalé','MG'),(1522,'Chapada do Norte','MG'),(1523,'Chapada Gaúcha','MG'),(1524,'Chiador','MG'),(1525,'Cipotânea','MG'),(1526,'Claraval','MG'),(1527,'Claro dos Poções','MG'),(1528,'Cláudio','MG'),(1529,'Coimbra','MG'),(1530,'Coluna','MG'),(1531,'Comendador Gomes','MG'),(1532,'Comercinho','MG'),(1533,'Conceição da Aparecida','MG'),(1534,'Conceição da Barra de Minas','MG'),(1535,'Conceição das Alagoas','MG'),(1536,'Conceição das Pedras','MG'),(1537,'Conceição de Ipanema','MG'),(1538,'Conceição do Mato Dentro','MG'),(1539,'Conceição do Pará','MG'),(1540,'Conceição do Rio Verde','MG'),(1541,'Conceição dos Ouros','MG'),(1542,'Cônego Marinho','MG'),(1543,'Confins','MG'),(1544,'Congonhal','MG'),(1545,'Congonhas','MG'),(1546,'Congonhas do Norte','MG'),(1547,'Conquista','MG'),(1548,'Conselheiro Lafaiete','MG'),(1549,'Conselheiro Pena','MG'),(1550,'Consolação','MG'),(1551,'Contagem','MG'),(1552,'Coqueiral','MG'),(1553,'Coração de Jesus','MG'),(1554,'Cordisburgo','MG'),(1555,'Cordislândia','MG'),(1556,'Corinto','MG'),(1557,'Coroaci','MG'),(1558,'Coromandel','MG'),(1559,'Coronel Fabriciano','MG'),(1560,'Coronel Murta','MG'),(1561,'Coronel Pacheco','MG'),(1562,'Coronel Xavier Chaves','MG'),(1563,'Córrego Danta','MG'),(1564,'Córrego do Bom Jesus','MG'),(1565,'Córrego Fundo','MG'),(1566,'Córrego Novo','MG'),(1567,'Couto de Magalhães de Minas','MG'),(1568,'Crisólita','MG'),(1569,'Cristais','MG'),(1570,'Cristália','MG'),(1571,'Cristiano Otoni','MG'),(1572,'Cristina','MG'),(1573,'Crucilândia','MG'),(1574,'Cruzeiro da Fortaleza','MG'),(1575,'Cruzília','MG'),(1576,'Cuparaque','MG'),(1577,'Curral de Dentro','MG'),(1578,'Curvelo','MG'),(1579,'Datas','MG'),(1580,'Delfim Moreira','MG'),(1581,'Delfinópolis','MG'),(1582,'Delta','MG'),(1583,'Descoberto','MG'),(1584,'Desterro de Entre Rios','MG'),(1585,'Desterro do Melo','MG'),(1586,'Diamantina','MG'),(1587,'Diogo de Vasconcelos','MG'),(1588,'Dionísio','MG'),(1589,'Divinésia','MG'),(1590,'Divino','MG'),(1591,'Divino das Laranjeiras','MG'),(1592,'Divinolândia de Minas','MG'),(1593,'Divinópolis','MG'),(1594,'Divisa Alegre','MG'),(1595,'Divisa Nova','MG'),(1596,'Divisópolis','MG'),(1597,'Dom Bosco','MG'),(1598,'Dom Cavati','MG'),(1599,'Dom Joaquim','MG'),(1600,'Dom Silvério','MG'),(1601,'Dom Viçoso','MG'),(1602,'Dona Euzébia','MG'),(1603,'Dores de Campos','MG'),(1604,'Dores de Guanhães','MG'),(1605,'Dores do Indaiá','MG'),(1606,'Dores do Turvo','MG'),(1607,'Doresópolis','MG'),(1608,'Douradoquara','MG'),(1609,'Durandé','MG'),(1610,'Elói Mendes','MG'),(1611,'Engenheiro Caldas','MG'),(1612,'Engenheiro Navarro','MG'),(1613,'Entre Folhas','MG'),(1614,'Entre Rios de Minas','MG'),(1615,'Ervália','MG'),(1616,'Esmeraldas','MG'),(1617,'Espera Feliz','MG'),(1618,'Espinosa','MG'),(1619,'Espírito Santo do Dourado','MG'),(1620,'Estiva','MG'),(1621,'Estrela Dalva','MG'),(1622,'Estrela do Indaiá','MG'),(1623,'Estrela do Sul','MG'),(1624,'Eugenópolis','MG'),(1625,'Ewbank da Câmara','MG'),(1626,'Extrema','MG'),(1627,'Fama','MG'),(1628,'Faria Lemos','MG'),(1629,'Felício dos Santos','MG'),(1630,'Felisburgo','MG'),(1631,'Felixlândia','MG'),(1632,'Fernandes Tourinho','MG'),(1633,'Ferros','MG'),(1634,'Fervedouro','MG'),(1635,'Florestal','MG'),(1636,'Formiga','MG'),(1637,'Formoso','MG'),(1638,'Fortaleza de Minas','MG'),(1639,'Fortuna de Minas','MG'),(1640,'Francisco Badaró','MG'),(1641,'Francisco Dumont','MG'),(1642,'Francisco Sá','MG'),(1643,'Franciscópolis','MG'),(1644,'Frei Gaspar','MG'),(1645,'Frei Inocêncio','MG'),(1646,'Frei Lagonegro','MG'),(1647,'Fronteira','MG'),(1648,'Fronteira dos Vales','MG'),(1649,'Fruta de Leite','MG'),(1650,'Frutal','MG'),(1651,'Funilândia','MG'),(1652,'Galiléia','MG'),(1653,'Gameleiras','MG'),(1654,'Glaucilândia','MG'),(1655,'Goiabeira','MG'),(1656,'Goianá','MG'),(1657,'Gonçalves','MG'),(1658,'Gonzaga','MG'),(1659,'Gouveia','MG'),(1660,'Governador Valadares','MG'),(1661,'Grão Mogol','MG'),(1662,'Grupiara','MG'),(1663,'Guanhães','MG'),(1664,'Guapé','MG'),(1665,'Guaraciaba','MG'),(1666,'Guaraciama','MG'),(1667,'Guaranésia','MG'),(1668,'Guarani','MG'),(1669,'Guarará','MG'),(1670,'Guarda-Mor','MG'),(1671,'Guaxupé','MG'),(1672,'Guidoval','MG'),(1673,'Guimarânia','MG'),(1674,'Guiricema','MG'),(1675,'Gurinhatã','MG'),(1676,'Heliodora','MG'),(1677,'Iapu','MG'),(1678,'Ibertioga','MG'),(1679,'Ibiá','MG'),(1680,'Ibiaí','MG'),(1681,'Ibiracatu','MG'),(1682,'Ibiraci','MG'),(1683,'Ibirité','MG'),(1684,'Ibitiúra de Minas','MG'),(1685,'Ibituruna','MG'),(1686,'Icaraí de Minas','MG'),(1687,'Igarapé','MG'),(1688,'Igaratinga','MG'),(1689,'Iguatama','MG'),(1690,'Ijaci','MG'),(1691,'Ilicínea','MG'),(1692,'Imbé de Minas','MG'),(1693,'Inconfidentes','MG'),(1694,'Indaiabira','MG'),(1695,'Indianópolis','MG'),(1696,'Ingaí','MG'),(1697,'Inhapim','MG'),(1698,'Inhaúma','MG'),(1699,'Inimutaba','MG'),(1700,'Ipaba','MG'),(1701,'Ipanema','MG'),(1702,'Ipatinga','MG'),(1703,'Ipiaçu','MG'),(1704,'Ipuiúna','MG'),(1705,'Iraí de Minas','MG'),(1706,'Itabira','MG'),(1707,'Itabirinha de Mantena','MG'),(1708,'Itabirito','MG'),(1709,'Itacambira','MG'),(1710,'Itacarambi','MG'),(1711,'Itaguara','MG'),(1712,'Itaipé','MG'),(1713,'Itajubá','MG'),(1714,'Itamarandiba','MG'),(1715,'Itamarati de Minas','MG'),(1716,'Itambacuri','MG'),(1717,'Itambé do Mato Dentro','MG'),(1718,'Itamogi','MG'),(1719,'Itamonte','MG'),(1720,'Itanhandu','MG'),(1721,'Itanhomi','MG'),(1722,'Itaobim','MG'),(1723,'Itapagipe','MG'),(1724,'Itapecerica','MG'),(1725,'Itapeva','MG'),(1726,'Itatiaiuçu','MG'),(1727,'Itaú de Minas','MG'),(1728,'Itaúna','MG'),(1729,'Itaverava','MG'),(1730,'Itinga','MG'),(1731,'Itueta','MG'),(1732,'Ituiutaba','MG'),(1733,'Itumirim','MG'),(1734,'Iturama','MG'),(1735,'Itutinga','MG'),(1736,'Jaboticatubas','MG'),(1737,'Jacinto','MG'),(1738,'Jacuí','MG'),(1739,'Jacutinga','MG'),(1740,'Jaguaraçu','MG'),(1741,'Jaíba','MG'),(1742,'Jampruca','MG'),(1743,'Janaúba','MG'),(1744,'Januária','MG'),(1745,'Japaraíba','MG'),(1746,'Japonvar','MG'),(1747,'Jeceaba','MG'),(1748,'Jenipapo de Minas','MG'),(1749,'Jequeri','MG'),(1750,'Jequitaí','MG'),(1751,'Jequitibá','MG'),(1752,'Jequitinhonha','MG'),(1753,'Jesuânia','MG'),(1754,'Joaíma','MG'),(1755,'Joanésia','MG'),(1756,'João Monlevade','MG'),(1757,'João Pinheiro','MG'),(1758,'Joaquim Felício','MG'),(1759,'Jordânia','MG'),(1760,'José Gonçalves de Minas','MG'),(1761,'José Raydan','MG'),(1762,'Josenópolis','MG'),(1763,'Juatuba','MG'),(1764,'Juiz de Fora','MG'),(1765,'Juramento','MG'),(1766,'Juruaia','MG'),(1767,'Juvenília','MG'),(1768,'Ladainha','MG'),(1769,'Lagamar','MG'),(1770,'Lagoa da Prata','MG'),(1771,'Lagoa dos Patos','MG'),(1772,'Lagoa Dourada','MG'),(1773,'Lagoa Formosa','MG'),(1774,'Lagoa Grande','MG'),(1775,'Lagoa Santa','MG'),(1776,'Lajinha','MG'),(1777,'Lambari','MG'),(1778,'Lamim','MG'),(1779,'Laranjal','MG'),(1780,'Lassance','MG'),(1781,'Lavras','MG'),(1782,'Leandro Ferreira','MG'),(1783,'Leme do Prado','MG'),(1784,'Leopoldina','MG'),(1785,'Liberdade','MG'),(1786,'Lima Duarte','MG'),(1787,'Limeira do Oeste','MG'),(1788,'Lontra','MG'),(1789,'Luisburgo','MG'),(1790,'Luislândia','MG'),(1791,'Luminárias','MG'),(1792,'Luz','MG'),(1793,'Machacalis','MG'),(1794,'Machado','MG'),(1795,'Madre de Deus de Minas','MG'),(1796,'Malacacheta','MG'),(1797,'Mamonas','MG'),(1798,'Manga','MG'),(1799,'Manhuaçu','MG'),(1800,'Manhumirim','MG'),(1801,'Mantena','MG'),(1802,'Mar de Espanha','MG'),(1803,'Maravilhas','MG'),(1804,'Maria da Fé','MG'),(1805,'Mariana','MG'),(1806,'Marilac','MG'),(1807,'Mário Campos','MG'),(1808,'Maripá de Minas','MG'),(1809,'Marliéria','MG'),(1810,'Marmelópolis','MG'),(1811,'Martinho Campos','MG'),(1812,'Martins Soares','MG'),(1813,'Mata Verde','MG'),(1814,'Materlândia','MG'),(1815,'Mateus Leme','MG'),(1816,'Mathias Lobato','MG'),(1817,'Matias Barbosa','MG'),(1818,'Matias Cardoso','MG'),(1819,'Matipó','MG'),(1820,'Mato Verde','MG'),(1821,'Matozinhos','MG'),(1822,'Matutina','MG'),(1823,'Medeiros','MG'),(1824,'Medina','MG'),(1825,'Mendes Pimentel','MG'),(1826,'Mercês','MG'),(1827,'Mesquita','MG'),(1828,'Minas Novas','MG'),(1829,'Minduri','MG'),(1830,'Mirabela','MG'),(1831,'Miradouro','MG'),(1832,'Miraí','MG'),(1833,'Miravânia','MG'),(1834,'Moeda','MG'),(1835,'Moema','MG'),(1836,'Monjolos','MG'),(1837,'Monsenhor Paulo','MG'),(1838,'Montalvânia','MG'),(1839,'Monte Alegre de Minas','MG'),(1840,'Monte Azul','MG'),(1841,'Monte Belo','MG'),(1842,'Monte Carmelo','MG'),(1843,'Monte Formoso','MG'),(1844,'Monte Santo de Minas','MG'),(1845,'Monte Sião','MG'),(1846,'Montes Claros','MG'),(1847,'Montezuma','MG'),(1848,'Morada Nova de Minas','MG'),(1849,'Morro da Garça','MG'),(1850,'Morro do Pilar','MG'),(1851,'Munhoz','MG'),(1852,'Muriaé','MG'),(1853,'Mutum','MG'),(1854,'Muzambinho','MG'),(1855,'Nacip Raydan','MG'),(1856,'Nanuque','MG'),(1857,'Naque','MG'),(1858,'Natalândia','MG'),(1859,'Natércia','MG'),(1860,'Nazareno','MG'),(1861,'Nepomuceno','MG'),(1862,'Ninheira','MG'),(1863,'Nova Belém','MG'),(1864,'Nova Era','MG'),(1865,'Nova Lima','MG'),(1866,'Nova Módica','MG'),(1867,'Nova Ponte','MG'),(1868,'Nova Porteirinha','MG'),(1869,'Nova Resende','MG'),(1870,'Nova Serrana','MG'),(1871,'Nova União','MG'),(1872,'Novo Cruzeiro','MG'),(1873,'Novo Oriente de Minas','MG'),(1874,'Novorizonte','MG'),(1875,'Olaria','MG'),(1876,'Olhos-d\'Água','MG'),(1877,'Olímpio Noronha','MG'),(1878,'Oliveira','MG'),(1879,'Oliveira Fortes','MG'),(1880,'Onça de Pitangui','MG'),(1881,'Oratórios','MG'),(1882,'Orizânia','MG'),(1883,'Ouro Branco','MG'),(1884,'Ouro Fino','MG'),(1885,'Ouro Preto','MG'),(1886,'Ouro Verde de Minas','MG'),(1887,'Padre Carvalho','MG'),(1888,'Padre Paraíso','MG'),(1889,'Pai Pedro','MG'),(1890,'Paineiras','MG'),(1891,'Pains','MG'),(1892,'Paiva','MG'),(1893,'Palma','MG'),(1894,'Palmópolis','MG'),(1895,'Papagaios','MG'),(1896,'Pará de Minas','MG'),(1897,'Paracatu','MG'),(1898,'Paraguaçu','MG'),(1899,'Paraisópolis','MG'),(1900,'Paraopeba','MG'),(1901,'Passa Quatro','MG'),(1902,'Passa Tempo','MG'),(1903,'Passa-Vinte','MG'),(1904,'Passabém','MG'),(1905,'Passos','MG'),(1906,'Patis','MG'),(1907,'Patos de Minas','MG'),(1908,'Patrocínio','MG'),(1909,'Patrocínio do Muriaé','MG'),(1910,'Paula Cândido','MG'),(1911,'Paulistas','MG'),(1912,'Pavão','MG'),(1913,'Peçanha','MG'),(1914,'Pedra Azul','MG'),(1915,'Pedra Bonita','MG'),(1916,'Pedra do Anta','MG'),(1917,'Pedra do Indaiá','MG'),(1918,'Pedra Dourada','MG'),(1919,'Pedralva','MG'),(1920,'Pedras de Maria da Cruz','MG'),(1921,'Pedrinópolis','MG'),(1922,'Pedro Leopoldo','MG'),(1923,'Pedro Teixeira','MG'),(1924,'Pequeri','MG'),(1925,'Pequi','MG'),(1926,'Perdigão','MG'),(1927,'Perdizes','MG'),(1928,'Perdões','MG'),(1929,'Periquito','MG'),(1930,'Pescador','MG'),(1931,'Piau','MG'),(1932,'Piedade de Caratinga','MG'),(1933,'Piedade de Ponte Nova','MG'),(1934,'Piedade do Rio Grande','MG'),(1935,'Piedade dos Gerais','MG'),(1936,'Pimenta','MG'),(1937,'Pingo-d\'Água','MG'),(1938,'Pintópolis','MG'),(1939,'Piracema','MG'),(1940,'Pirajuba','MG'),(1941,'Piranga','MG'),(1942,'Piranguçu','MG'),(1943,'Piranguinho','MG'),(1944,'Pirapetinga','MG'),(1945,'Pirapora','MG'),(1946,'Piraúba','MG'),(1947,'Pitangui','MG'),(1948,'Piumhi','MG'),(1949,'Planura','MG'),(1950,'Poço Fundo','MG'),(1951,'Poços de Caldas','MG'),(1952,'Pocrane','MG'),(1953,'Pompéu','MG'),(1954,'Ponte Nova','MG'),(1955,'Ponto Chique','MG'),(1956,'Ponto dos Volantes','MG'),(1957,'Porteirinha','MG'),(1958,'Porto Firme','MG'),(1959,'Poté','MG'),(1960,'Pouso Alegre','MG'),(1961,'Pouso Alto','MG'),(1962,'Prados','MG'),(1963,'Prata','MG'),(1964,'Pratápolis','MG'),(1965,'Pratinha','MG'),(1966,'Presidente Bernardes','MG'),(1967,'Presidente Juscelino','MG'),(1968,'Presidente Kubitschek','MG'),(1969,'Presidente Olegário','MG'),(1970,'Prudente de Morais','MG'),(1971,'Quartel Geral','MG'),(1972,'Queluzito','MG'),(1973,'Raposos','MG'),(1974,'Raul Soares','MG'),(1975,'Recreio','MG'),(1976,'Reduto','MG'),(1977,'Resende Costa','MG'),(1978,'Resplendor','MG'),(1979,'Ressaquinha','MG'),(1980,'Riachinho','MG'),(1981,'Riacho dos Machados','MG'),(1982,'Ribeirão das Neves','MG'),(1983,'Ribeirão Vermelho','MG'),(1984,'Rio Acima','MG'),(1985,'Rio Casca','MG'),(1986,'Rio do Prado','MG'),(1987,'Rio Doce','MG'),(1988,'Rio Espera','MG'),(1989,'Rio Manso','MG'),(1990,'Rio Novo','MG'),(1991,'Rio Paranaíba','MG'),(1992,'Rio Pardo de Minas','MG'),(1993,'Rio Piracicaba','MG'),(1994,'Rio Pomba','MG'),(1995,'Rio Preto','MG'),(1996,'Rio Vermelho','MG'),(1997,'Ritápolis','MG'),(1998,'Rochedo de Minas','MG'),(1999,'Rodeiro','MG'),(2000,'Romaria','MG'),(2001,'Rosário da Limeira','MG'),(2002,'Rubelita','MG'),(2003,'Rubim','MG'),(2004,'Sabará','MG'),(2005,'Sabinópolis','MG'),(2006,'Sacramento','MG'),(2007,'Salinas','MG'),(2008,'Salto da Divisa','MG'),(2009,'Santa Bárbara','MG'),(2010,'Santa Bárbara do Leste','MG'),(2011,'Santa Bárbara do Monte Verde','MG'),(2012,'Santa Bárbara do Tugúrio','MG'),(2013,'Santa Cruz de Minas','MG'),(2014,'Santa Cruz de Salinas','MG'),(2015,'Santa Cruz do Escalvado','MG'),(2016,'Santa Efigênia de Minas','MG'),(2017,'Santa Fé de Minas','MG'),(2018,'Santa Helena de Minas','MG'),(2019,'Santa Juliana','MG'),(2020,'Santa Luzia','MG'),(2021,'Santa Margarida','MG'),(2022,'Santa Maria de Itabira','MG'),(2023,'Santa Maria do Salto','MG'),(2024,'Santa Maria do Suaçuí','MG'),(2025,'Santa Rita de Caldas','MG'),(2026,'Santa Rita de Ibitipoca','MG'),(2027,'Santa Rita de Jacutinga','MG'),(2028,'Santa Rita de Minas','MG'),(2029,'Santa Rita do Itueto','MG'),(2030,'Santa Rita do Sapucaí','MG'),(2031,'Santa Rosa da Serra','MG'),(2032,'Santa Vitória','MG'),(2033,'Santana da Vargem','MG'),(2034,'Santana de Cataguases','MG'),(2035,'Santana de Pirapama','MG'),(2036,'Santana do Deserto','MG'),(2037,'Santana do Garambéu','MG'),(2038,'Santana do Jacaré','MG'),(2039,'Santana do Manhuaçu','MG'),(2040,'Santana do Paraíso','MG'),(2041,'Santana do Riacho','MG'),(2042,'Santana dos Montes','MG'),(2043,'Santo Antônio do Amparo','MG'),(2044,'Santo Antônio do Aventureiro','MG'),(2045,'Santo Antônio do Grama','MG'),(2046,'Santo Antônio do Itambé','MG'),(2047,'Santo Antônio do Jacinto','MG'),(2048,'Santo Antônio do Monte','MG'),(2049,'Santo Antônio do Retiro','MG'),(2050,'Santo Antônio do Rio Abaixo','MG'),(2051,'Santo Hipólito','MG'),(2052,'Santos Dumont','MG'),(2053,'São Bento Abade','MG'),(2054,'São Brás do Suaçuí','MG'),(2055,'São Domingos das Dores','MG'),(2056,'São Domingos do Prata','MG'),(2057,'São Félix de Minas','MG'),(2058,'São Francisco','MG'),(2059,'São Francisco de Paula','MG'),(2060,'São Francisco de Sales','MG'),(2061,'São Francisco do Glória','MG'),(2062,'São Geraldo','MG'),(2063,'São Geraldo da Piedade','MG'),(2064,'São Geraldo do Baixio','MG'),(2065,'São Gonçalo do Abaeté','MG'),(2066,'São Gonçalo do Pará','MG'),(2067,'São Gonçalo do Rio Abaixo','MG'),(2068,'São Gonçalo do Rio Preto','MG'),(2069,'São Gonçalo do Sapucaí','MG'),(2070,'São Gotardo','MG'),(2071,'São João Batista do Glória','MG'),(2072,'São João da Lagoa','MG'),(2073,'São João da Mata','MG'),(2074,'São João da Ponte','MG'),(2075,'São João das Missões','MG'),(2076,'São João del Rei','MG'),(2077,'São João do Manhuaçu','MG'),(2078,'São João do Manteninha','MG'),(2079,'São João do Oriente','MG'),(2080,'São João do Pacuí','MG'),(2081,'São João do Paraíso','MG'),(2082,'São João Evangelista','MG'),(2083,'São João Nepomuceno','MG'),(2084,'São Joaquim de Bicas','MG'),(2085,'São José da Barra','MG'),(2086,'São José da Lapa','MG'),(2087,'São José da Safira','MG'),(2088,'São José da Varginha','MG'),(2089,'São José do Alegre','MG'),(2090,'São José do Divino','MG'),(2091,'São José do Goiabal','MG'),(2092,'São José do Jacuri','MG'),(2093,'São José do Mantimento','MG'),(2094,'São Lourenço','MG'),(2095,'São Miguel do Anta','MG'),(2096,'São Pedro da União','MG'),(2097,'São Pedro do Suaçuí','MG'),(2098,'São Pedro dos Ferros','MG'),(2099,'São Romão','MG'),(2100,'São Roque de Minas','MG'),(2101,'São Sebastião da Bela Vista','MG'),(2102,'São Sebastião da Vargem Alegre','MG'),(2103,'São Sebastião do Anta','MG'),(2104,'São Sebastião do Maranhão','MG'),(2105,'São Sebastião do Oeste','MG'),(2106,'São Sebastião do Paraíso','MG'),(2107,'São Sebastião do Rio Preto','MG'),(2108,'São Sebastião do Rio Verde','MG'),(2109,'São Thomé das Letras','MG'),(2110,'São Tiago','MG'),(2111,'São Tomás de Aquino','MG'),(2112,'São Vicente de Minas','MG'),(2113,'Sapucaí-Mirim','MG'),(2114,'Sardoá','MG'),(2115,'Sarzedo','MG'),(2116,'Sem-Peixe','MG'),(2117,'Senador Amaral','MG'),(2118,'Senador Cortes','MG'),(2119,'Senador Firmino','MG'),(2120,'Senador José Bento','MG'),(2121,'Senador Modestino Gonçalves','MG'),(2122,'Senhora de Oliveira','MG'),(2123,'Senhora do Porto','MG'),(2124,'Senhora dos Remédios','MG'),(2125,'Sericita','MG'),(2126,'Seritinga','MG'),(2127,'Serra Azul de Minas','MG'),(2128,'Serra da Saudade','MG'),(2129,'Serra do Salitre','MG'),(2130,'Serra dos Aimorés','MG'),(2131,'Serrania','MG'),(2132,'Serranópolis de Minas','MG'),(2133,'Serranos','MG'),(2134,'Serro','MG'),(2135,'Sete Lagoas','MG'),(2136,'Setubinha','MG'),(2137,'Silveirânia','MG'),(2138,'Silvianópolis','MG'),(2139,'Simão Pereira','MG'),(2140,'Simonésia','MG'),(2141,'Sobrália','MG'),(2142,'Soledade de Minas','MG'),(2143,'Tabuleiro','MG'),(2144,'Taiobeiras','MG'),(2145,'Taparuba','MG'),(2146,'Tapira','MG'),(2147,'Tapiraí','MG'),(2148,'Taquaraçu de Minas','MG'),(2149,'Tarumirim','MG'),(2150,'Teixeiras','MG'),(2151,'Teófilo Otoni','MG'),(2152,'Timóteo','MG'),(2153,'Tiradentes','MG'),(2154,'Tiros','MG'),(2155,'Tocantins','MG'),(2156,'Tocos do Moji','MG'),(2157,'Toledo','MG'),(2158,'Tombos','MG'),(2159,'Três Corações','MG'),(2160,'Três Marias','MG'),(2161,'Três Pontas','MG'),(2162,'Tumiritinga','MG'),(2163,'Tupaciguara','MG'),(2164,'Turmalina','MG'),(2165,'Turvolândia','MG'),(2166,'Ubá','MG'),(2167,'Ubaí','MG'),(2168,'Ubaporanga','MG'),(2169,'Uberaba','MG'),(2170,'Uberlândia','MG'),(2171,'Umburatiba','MG'),(2172,'Unaí','MG'),(2173,'União de Minas','MG'),(2174,'Uruana de Minas','MG'),(2175,'Urucânia','MG'),(2176,'Urucuia','MG'),(2177,'Vargem Alegre','MG'),(2178,'Vargem Bonita','MG'),(2179,'Vargem Grande do Rio Pardo','MG'),(2180,'Varginha','MG'),(2181,'Varjão de Minas','MG'),(2182,'Várzea da Palma','MG'),(2183,'Varzelândia','MG'),(2184,'Vazante','MG'),(2185,'Verdelândia','MG'),(2186,'Veredinha','MG'),(2187,'Veríssimo','MG'),(2188,'Vermelho Novo','MG'),(2189,'Vespasiano','MG'),(2190,'Viçosa','MG'),(2191,'Vieiras','MG'),(2192,'Virgem da Lapa','MG'),(2193,'Virgínia','MG'),(2194,'Virginópolis','MG'),(2195,'Virgolândia','MG'),(2196,'Visconde do Rio Branco','MG'),(2197,'Volta Grande','MG'),(2198,'Wenceslau Braz','MG'),(2199,'Água Clara','MS'),(2200,'Alcinópolis','MS'),(2201,'Amambaí','MS'),(2202,'Anastácio','MS'),(2203,'Anaurilândia','MS'),(2204,'Angélica','MS'),(2205,'Antônio João','MS'),(2206,'Aparecida do Taboado','MS'),(2207,'Aquidauana','MS'),(2208,'Aral Moreira','MS'),(2209,'Bandeirantes','MS'),(2210,'Bataguassu','MS'),(2211,'Bataiporã','MS'),(2212,'Bela Vista','MS'),(2213,'Bodoquena','MS'),(2214,'Bonito','MS'),(2215,'Brasilândia','MS'),(2216,'Caarapó','MS'),(2217,'Camapuã','MS'),(2218,'Campo Grande','MS'),(2219,'Caracol','MS'),(2220,'Cassilândia','MS'),(2221,'Chapadão do Sul','MS'),(2222,'Corguinho','MS'),(2223,'Coronel Sapucaia','MS'),(2224,'Corumbá','MS'),(2225,'Costa Rica','MS'),(2226,'Coxim','MS'),(2227,'Deodápolis','MS'),(2228,'Dois Irmãos do Buriti','MS'),(2229,'Douradina','MS'),(2230,'Dourados','MS'),(2231,'Eldorado','MS'),(2232,'Fátima do Sul','MS'),(2233,'Glória de Dourados','MS'),(2234,'Guia Lopes da Laguna','MS'),(2235,'Iguatemi','MS'),(2236,'Inocência','MS'),(2237,'Itaporã','MS'),(2238,'Itaquiraí','MS'),(2239,'Ivinhema','MS'),(2240,'Japorã','MS'),(2241,'Jaraguari','MS'),(2242,'Jardim','MS'),(2243,'Jateí','MS'),(2244,'Juti','MS'),(2245,'Ladário','MS'),(2246,'Laguna Carapã','MS'),(2247,'Maracaju','MS'),(2248,'Miranda','MS'),(2249,'Mundo Novo','MS'),(2250,'Naviraí','MS'),(2251,'Nioaque','MS'),(2252,'Nova Alvorada do Sul','MS'),(2253,'Nova Andradina','MS'),(2254,'Novo Horizonte do Sul','MS'),(2255,'Paranaíba','MS'),(2256,'Paranhos','MS'),(2257,'Pedro Gomes','MS'),(2258,'Ponta Porã','MS'),(2259,'Porto Murtinho','MS'),(2260,'Ribas do Rio Pardo','MS'),(2261,'Rio Brilhante','MS'),(2262,'Rio Negro','MS'),(2263,'Rio Verde de Mato Grosso','MS'),(2264,'Rochedo','MS'),(2265,'Santa Rita do Pardo','MS'),(2266,'São Gabriel do Oeste','MS'),(2267,'Selvíria','MS'),(2268,'Sete Quedas','MS'),(2269,'Sidrolândia','MS'),(2270,'Sonora','MS'),(2271,'Tacuru','MS'),(2272,'Taquarussu','MS'),(2273,'Terenos','MS'),(2274,'Três Lagoas','MS'),(2275,'Vicentina','MS'),(2276,'Acorizal','MT'),(2277,'Água Boa','MT'),(2278,'Alta Floresta','MT'),(2279,'Alto Araguaia','MT'),(2280,'Alto Boa Vista','MT'),(2281,'Alto Garças','MT'),(2282,'Alto Paraguai','MT'),(2283,'Alto Taquari','MT'),(2284,'Apiacás','MT'),(2285,'Araguaiana','MT'),(2286,'Araguainha','MT'),(2287,'Araputanga','MT'),(2288,'Arenápolis','MT'),(2289,'Aripuanã','MT'),(2290,'Barão de Melgaço','MT'),(2291,'Barra do Bugres','MT'),(2292,'Barra do Garças','MT'),(2293,'Bom Jesus do Araguaia','MT'),(2294,'Brasnorte','MT'),(2295,'Cáceres','MT'),(2296,'Campinápolis','MT'),(2297,'Campo Novo do Parecis','MT'),(2298,'Campo Verde','MT'),(2299,'Campos de Júlio','MT'),(2300,'Canabrava do Norte','MT'),(2301,'Canarana','MT'),(2302,'Carlinda','MT'),(2303,'Castanheira','MT'),(2304,'Chapada dos Guimarães','MT'),(2305,'Cláudia','MT'),(2306,'Cocalinho','MT'),(2307,'Colíder','MT'),(2308,'Colniza','MT'),(2309,'Comodoro','MT'),(2310,'Confresa','MT'),(2311,'Conquista d\'Oeste','MT'),(2312,'Cotriguaçu','MT'),(2313,'Curvelândia','MT'),(2314,'Cuiabá','MT'),(2315,'Denise','MT'),(2316,'Diamantino','MT'),(2317,'Dom Aquino','MT'),(2318,'Feliz Natal','MT'),(2319,'Figueirópolis d\'Oeste','MT'),(2320,'Gaúcha do Norte','MT'),(2321,'General Carneiro','MT'),(2322,'Glória d\'Oeste','MT'),(2323,'Guarantã do Norte','MT'),(2324,'Guiratinga','MT'),(2325,'Indiavaí','MT'),(2326,'Itaúba','MT'),(2327,'Itiquira','MT'),(2328,'Jaciara','MT'),(2329,'Jangada','MT'),(2330,'Jauru','MT'),(2331,'Juara','MT'),(2332,'Juína','MT'),(2333,'Juruena','MT'),(2334,'Juscimeira','MT'),(2335,'Lambari d\'Oeste','MT'),(2336,'Lucas do Rio Verde','MT'),(2337,'Luciára','MT'),(2338,'Marcelândia','MT'),(2339,'Matupá','MT'),(2340,'Mirassol d\'Oeste','MT'),(2341,'Nobres','MT'),(2342,'Nortelândia','MT'),(2343,'Nossa Senhora do Livramento','MT'),(2344,'Nova Bandeirantes','MT'),(2345,'Nova Brasilândia','MT'),(2346,'Nova Canãa do Norte','MT'),(2347,'Nova Guarita','MT'),(2348,'Nova Lacerda','MT'),(2349,'Nova Marilândia','MT'),(2350,'Nova Maringá','MT'),(2351,'Nova Monte Verde','MT'),(2352,'Nova Mutum','MT'),(2353,'Nova Nazaré','MT'),(2354,'Nova Olímpia','MT'),(2355,'Nova Santa Helena','MT'),(2356,'Nova Ubiratã','MT'),(2357,'Nova Xavantina','MT'),(2358,'Novo Horizonte do Norte','MT'),(2359,'Novo Mundo','MT'),(2360,'Novo Santo Antônio','MT'),(2361,'Novo São Joaquim','MT'),(2362,'Paranaíta','MT'),(2363,'Paranatinga','MT'),(2364,'Pedra Preta','MT'),(2365,'Peixoto de Azevedo','MT'),(2366,'Planalto da Serra','MT'),(2367,'Poconé','MT'),(2368,'Pontal do Araguaia','MT'),(2369,'Ponte Branca','MT'),(2370,'Pontes e Lacerda','MT'),(2371,'Porto Alegre do Norte','MT'),(2372,'Porto dos Gaúchos','MT'),(2373,'Porto Esperidião','MT'),(2374,'Porto Estrela','MT'),(2375,'Poxoréo','MT'),(2376,'Primavera do Leste','MT'),(2377,'Querência','MT'),(2378,'Reserva do Cabaçal','MT'),(2379,'Ribeirão Cascalheira','MT'),(2380,'Ribeirãozinho','MT'),(2381,'Rio Branco','MT'),(2382,'Rondolândia','MT'),(2383,'Rondonópolis','MT'),(2384,'Rosário Oeste','MT'),(2385,'Salto do Céu','MT'),(2386,'Santa Carmem','MT'),(2387,'Santa Cruz do Xingu','MT'),(2388,'Santa Rita do Trivelato','MT'),(2389,'Santa Terezinha','MT'),(2390,'Santo Afonso','MT'),(2391,'Santo Antônio do Leste','MT'),(2392,'Santo Antônio do Leverger','MT'),(2393,'São Félix do Araguaia','MT'),(2394,'São José do Povo','MT'),(2395,'São José do Rio Claro','MT'),(2396,'São José do Xingu','MT'),(2397,'São José dos Quatro Marcos','MT'),(2398,'São Pedro da Cipa','MT'),(2399,'Sapezal','MT'),(2400,'Serra Nova Dourada','MT'),(2401,'Sinop','MT'),(2402,'Sorriso','MT'),(2403,'Tabaporã','MT'),(2404,'Tangará da Serra','MT'),(2405,'Tapurah','MT'),(2406,'Terra Nova do Norte','MT'),(2407,'Tesouro','MT'),(2408,'Torixoréu','MT'),(2409,'União do Sul','MT'),(2410,'Vale de São Domingos','MT'),(2411,'Várzea Grande','MT'),(2412,'Vera','MT'),(2413,'Vila Bela da Santíssima Trindade','MT'),(2414,'Vila Rica','MT'),(2415,'Abaetetuba','PA'),(2416,'Abel Figueiredo','PA'),(2417,'Acará','PA'),(2418,'Afuá','PA'),(2419,'Água Azul do Norte','PA'),(2420,'Alenquer','PA'),(2421,'Almeirim','PA'),(2422,'Altamira','PA'),(2423,'Anajás','PA'),(2424,'Ananindeua','PA'),(2425,'Anapu','PA'),(2426,'Augusto Corrêa','PA'),(2427,'Aurora do Pará','PA'),(2428,'Aveiro','PA'),(2429,'Bagre','PA'),(2430,'Baião','PA'),(2431,'Bannach','PA'),(2432,'Barcarena','PA'),(2433,'Belém','PA'),(2434,'Belterra','PA'),(2435,'Benevides','PA'),(2436,'Bom Jesus do Tocantins','PA'),(2437,'Bonito','PA'),(2438,'Bragança','PA'),(2439,'Brasil Novo','PA'),(2440,'Brejo Grande do Araguaia','PA'),(2441,'Breu Branco','PA'),(2442,'Breves','PA'),(2443,'Bujaru','PA'),(2444,'Cachoeira do Arari','PA'),(2445,'Cachoeira do Piriá','PA'),(2446,'Cametá','PA'),(2447,'Canaã dos Carajás','PA'),(2448,'Capanema','PA'),(2449,'Capitão Poço','PA'),(2450,'Castanhal','PA'),(2451,'Chaves','PA'),(2452,'Colares','PA'),(2453,'Conceição do Araguaia','PA'),(2454,'Concórdia do Pará','PA'),(2455,'Cumaru do Norte','PA'),(2456,'Curionópolis','PA'),(2457,'Curralinho','PA'),(2458,'Curuá','PA'),(2459,'Curuçá','PA'),(2460,'Dom Eliseu','PA'),(2461,'Eldorado dos Carajás','PA'),(2462,'Faro','PA'),(2463,'Floresta do Araguaia','PA'),(2464,'Garrafão do Norte','PA'),(2465,'Goianésia do Pará','PA'),(2466,'Gurupá','PA'),(2467,'Igarapé-Açu','PA'),(2468,'Igarapé-Miri','PA'),(2469,'Inhangapi','PA'),(2470,'Ipixuna do Pará','PA'),(2471,'Irituia','PA'),(2472,'Itaituba','PA'),(2473,'Itupiranga','PA'),(2474,'Jacareacanga','PA'),(2475,'Jacundá','PA'),(2476,'Juruti','PA'),(2477,'Limoeiro do Ajuru','PA'),(2478,'Mãe do Rio','PA'),(2479,'Magalhães Barata','PA'),(2480,'Marabá','PA'),(2481,'Maracanã','PA'),(2482,'Marapanim','PA'),(2483,'Marituba','PA'),(2484,'Medicilândia','PA'),(2485,'Melgaço','PA'),(2486,'Mocajuba','PA'),(2487,'Moju','PA'),(2488,'Monte Alegre','PA'),(2489,'Muaná','PA'),(2490,'Nova Esperança do Piriá','PA'),(2491,'Nova Ipixuna','PA'),(2492,'Nova Timboteua','PA'),(2493,'Novo Progresso','PA'),(2494,'Novo Repartimento','PA'),(2495,'Óbidos','PA'),(2496,'Oeiras do Pará','PA'),(2497,'Oriximiná','PA'),(2498,'Ourém','PA'),(2499,'Ourilândia do Norte','PA'),(2500,'Pacajá','PA'),(2501,'Palestina do Pará','PA'),(2502,'Paragominas','PA'),(2503,'Parauapebas','PA'),(2504,'Pau d\'Arco','PA'),(2505,'Peixe-Boi','PA'),(2506,'Piçarra','PA'),(2507,'Placas','PA'),(2508,'Ponta de Pedras','PA'),(2509,'Portel','PA'),(2510,'Porto de Moz','PA'),(2511,'Prainha','PA'),(2512,'Primavera','PA'),(2513,'Quatipuru','PA'),(2514,'Redenção','PA'),(2515,'Rio Maria','PA'),(2516,'Rondon do Pará','PA'),(2517,'Rurópolis','PA'),(2518,'Salinópolis','PA'),(2519,'Salvaterra','PA'),(2520,'Santa Bárbara do Pará','PA'),(2521,'Santa Cruz do Arari','PA'),(2522,'Santa Isabel do Pará','PA'),(2523,'Santa Luzia do Pará','PA'),(2524,'Santa Maria das Barreiras','PA'),(2525,'Santa Maria do Pará','PA'),(2526,'Santana do Araguaia','PA'),(2527,'Santarém','PA'),(2528,'Santarém Novo','PA'),(2529,'Santo Antônio do Tauá','PA'),(2530,'São Caetano de Odivela','PA'),(2531,'São Domingos do Araguaia','PA'),(2532,'São Domingos do Capim','PA'),(2533,'São Félix do Xingu','PA'),(2534,'São Francisco do Pará','PA'),(2535,'São Geraldo do Araguaia','PA'),(2536,'São João da Ponta','PA'),(2537,'São João de Pirabas','PA'),(2538,'São João do Araguaia','PA'),(2539,'São Miguel do Guamá','PA'),(2540,'São Sebastião da Boa Vista','PA'),(2541,'Sapucaia','PA'),(2542,'Senador José Porfírio','PA'),(2543,'Soure','PA'),(2544,'Tailândia','PA'),(2545,'Terra Alta','PA'),(2546,'Terra Santa','PA'),(2547,'Tomé-Açu','PA'),(2548,'Tracuateua','PA'),(2549,'Trairão','PA'),(2550,'Tucumã','PA'),(2551,'Tucuruí','PA'),(2552,'Ulianópolis','PA'),(2553,'Uruará','PA'),(2554,'Vigia','PA'),(2555,'Viseu','PA'),(2556,'Vitória do Xingu','PA'),(2557,'Xinguara','PA'),(2558,'Água Branca','PB'),(2559,'Aguiar','PB'),(2560,'Alagoa Grande','PB'),(2561,'Alagoa Nova','PB'),(2562,'Alagoinha','PB'),(2563,'Alcantil','PB'),(2564,'Algodão de Jandaíra','PB'),(2565,'Alhandra','PB'),(2566,'Amparo','PB'),(2567,'Aparecida','PB'),(2568,'Araçagi','PB'),(2569,'Arara','PB'),(2570,'Araruna','PB'),(2571,'Areia','PB'),(2572,'Areia de Baraúnas','PB'),(2573,'Areial','PB'),(2574,'Aroeiras','PB'),(2575,'Assunção','PB'),(2576,'Baía da Traição','PB'),(2577,'Bananeiras','PB'),(2578,'Baraúna','PB'),(2579,'Barra de Santa Rosa','PB'),(2580,'Barra de Santana','PB'),(2581,'Barra de São Miguel','PB'),(2582,'Bayeux','PB'),(2583,'Belém','PB'),(2584,'Belém do Brejo do Cruz','PB'),(2585,'Bernardino Batista','PB'),(2586,'Boa Ventura','PB'),(2587,'Boa Vista','PB'),(2588,'Bom Jesus','PB'),(2589,'Bom Sucesso','PB'),(2590,'Bonito de Santa Fé','PB'),(2591,'Boqueirão','PB'),(2592,'Borborema','PB'),(2593,'Brejo do Cruz','PB'),(2594,'Brejo dos Santos','PB'),(2595,'Caaporã','PB'),(2596,'Cabaceiras','PB'),(2597,'Cabedelo','PB'),(2598,'Cachoeira dos Índios','PB'),(2599,'Cacimba de Areia','PB'),(2600,'Cacimba de Dentro','PB'),(2601,'Cacimbas','PB'),(2602,'Caiçara','PB'),(2603,'Cajazeiras','PB'),(2604,'Cajazeirinhas','PB'),(2605,'Caldas Brandão','PB'),(2606,'Camalaú','PB'),(2607,'Campina Grande','PB'),(2608,'Campo de Santana','PB'),(2609,'Capim','PB'),(2610,'Caraúbas','PB'),(2611,'Carrapateira','PB'),(2612,'Casserengue','PB'),(2613,'Catingueira','PB'),(2614,'Catolé do Rocha','PB'),(2615,'Caturité','PB'),(2616,'Conceição','PB'),(2617,'Condado','PB'),(2618,'Conde','PB'),(2619,'Congo','PB'),(2620,'Coremas','PB'),(2621,'Coxixola','PB'),(2622,'Cruz do Espírito Santo','PB'),(2623,'Cubati','PB'),(2624,'Cuité','PB'),(2625,'Cuité de Mamanguape','PB'),(2626,'Cuitegi','PB'),(2627,'Curral de Cima','PB'),(2628,'Curral Velho','PB'),(2629,'Damião','PB'),(2630,'Desterro','PB'),(2631,'Diamante','PB'),(2632,'Dona Inês','PB'),(2633,'Duas Estradas','PB'),(2634,'Emas','PB'),(2635,'Esperança','PB'),(2636,'Fagundes','PB'),(2637,'Frei Martinho','PB'),(2638,'Gado Bravo','PB'),(2639,'Guarabira','PB'),(2640,'Gurinhém','PB'),(2641,'Gurjão','PB'),(2642,'Ibiara','PB'),(2643,'Igaracy','PB'),(2644,'Imaculada','PB'),(2645,'Ingá','PB'),(2646,'Itabaiana','PB'),(2647,'Itaporanga','PB'),(2648,'Itapororoca','PB'),(2649,'Itatuba','PB'),(2650,'Jacaraú','PB'),(2651,'Jericó','PB'),(2652,'João Pessoa','PB'),(2653,'Juarez Távora','PB'),(2654,'Juazeirinho','PB'),(2655,'Junco do Seridó','PB'),(2656,'Juripiranga','PB'),(2657,'Juru','PB'),(2658,'Lagoa','PB'),(2659,'Lagoa de Dentro','PB'),(2660,'Lagoa Seca','PB'),(2661,'Lastro','PB'),(2662,'Livramento','PB'),(2663,'Logradouro','PB'),(2664,'Lucena','PB'),(2665,'Mãe d\'Água','PB'),(2666,'Malta','PB'),(2667,'Mamanguape','PB'),(2668,'Manaíra','PB'),(2669,'Marcação','PB'),(2670,'Mari','PB'),(2671,'Marizópolis','PB'),(2672,'Massaranduba','PB'),(2673,'Mataraca','PB'),(2674,'Matinhas','PB'),(2675,'Mato Grosso','PB'),(2676,'Maturéia','PB'),(2677,'Mogeiro','PB'),(2678,'Montadas','PB'),(2679,'Monte Horebe','PB'),(2680,'Monteiro','PB'),(2681,'Mulungu','PB'),(2682,'Natuba','PB'),(2683,'Nazarezinho','PB'),(2684,'Nova Floresta','PB'),(2685,'Nova Olinda','PB'),(2686,'Nova Palmeira','PB'),(2687,'Olho d\'Água','PB'),(2688,'Olivedos','PB'),(2689,'Ouro Velho','PB'),(2690,'Parari','PB'),(2691,'Passagem','PB'),(2692,'Patos','PB'),(2693,'Paulista','PB'),(2694,'Pedra Branca','PB'),(2695,'Pedra Lavrada','PB'),(2696,'Pedras de Fogo','PB'),(2697,'Pedro Régis','PB'),(2698,'Piancó','PB'),(2699,'Picuí','PB'),(2700,'Pilar','PB'),(2701,'Pilões','PB'),(2702,'Pilõezinhos','PB'),(2703,'Pirpirituba','PB'),(2704,'Pitimbu','PB'),(2705,'Pocinhos','PB'),(2706,'Poço Dantas','PB'),(2707,'Poço de José de Moura','PB'),(2708,'Pombal','PB'),(2709,'Prata','PB'),(2710,'Princesa Isabel','PB'),(2711,'Puxinanã','PB'),(2712,'Queimadas','PB'),(2713,'Quixabá','PB'),(2714,'Remígio','PB'),(2715,'Riachão','PB'),(2716,'Riachão do Bacamarte','PB'),(2717,'Riachão do Poço','PB'),(2718,'Riacho de Santo Antônio','PB'),(2719,'Riacho dos Cavalos','PB'),(2720,'Rio Tinto','PB'),(2721,'Salgadinho','PB'),(2722,'Salgado de São Félix','PB'),(2723,'Santa Cecília','PB'),(2724,'Santa Cruz','PB'),(2725,'Santa Helena','PB'),(2726,'Santa Inês','PB'),(2727,'Santa Luzia','PB'),(2728,'Santa Rita','PB'),(2729,'Santa Teresinha','PB'),(2730,'Santana de Mangueira','PB'),(2731,'Santana dos Garrotes','PB'),(2732,'Santarém','PB'),(2733,'Santo André','PB'),(2734,'São Bentinho','PB'),(2735,'São Bento','PB'),(2736,'São Domingos de Pombal','PB'),(2737,'São Domingos do Cariri','PB'),(2738,'São Francisco','PB'),(2739,'São João do Cariri','PB'),(2740,'São João do Rio do Peixe','PB'),(2741,'São João do Tigre','PB'),(2742,'São José da Lagoa Tapada','PB'),(2743,'São José de Caiana','PB'),(2744,'São José de Espinharas','PB'),(2745,'São José de Piranhas','PB'),(2746,'São José de Princesa','PB'),(2747,'São José do Bonfim','PB'),(2748,'São José do Brejo do Cruz','PB'),(2749,'São José do Sabugi','PB'),(2750,'São José dos Cordeiros','PB'),(2751,'São José dos Ramos','PB'),(2752,'São Mamede','PB'),(2753,'São Miguel de Taipu','PB'),(2754,'São Sebastião de Lagoa de Roça','PB'),(2755,'São Sebastião do Umbuzeiro','PB'),(2756,'Sapé','PB'),(2757,'Seridó','PB'),(2758,'Serra Branca','PB'),(2759,'Serra da Raiz','PB'),(2760,'Serra Grande','PB'),(2761,'Serra Redonda','PB'),(2762,'Serraria','PB'),(2763,'Sertãozinho','PB'),(2764,'Sobrado','PB'),(2765,'Solânea','PB'),(2766,'Soledade','PB'),(2767,'Sossêgo','PB'),(2768,'Sousa','PB'),(2769,'Sumé','PB'),(2770,'Taperoá','PB'),(2771,'Tavares','PB'),(2772,'Teixeira','PB'),(2773,'Tenório','PB'),(2774,'Triunfo','PB'),(2775,'Uiraúna','PB'),(2776,'Umbuzeiro','PB'),(2777,'Várzea','PB'),(2778,'Vieirópolis','PB'),(2779,'Vista Serrana','PB'),(2780,'Zabelê','PB'),(2781,'Abreu e Lima','PE'),(2782,'Afogados da Ingazeira','PE'),(2783,'Afrânio','PE'),(2784,'Agrestina','PE'),(2785,'Água Preta','PE'),(2786,'Águas Belas','PE'),(2787,'Alagoinha','PE'),(2788,'Aliança','PE'),(2789,'Altinho','PE'),(2790,'Amaraji','PE'),(2791,'Angelim','PE'),(2792,'Araçoiaba','PE'),(2793,'Araripina','PE'),(2794,'Arcoverde','PE'),(2795,'Barra de Guabiraba','PE'),(2796,'Barreiros','PE'),(2797,'Belém de Maria','PE'),(2798,'Belém de São Francisco','PE'),(2799,'Belo Jardim','PE'),(2800,'Betânia','PE'),(2801,'Bezerros','PE'),(2802,'Bodocó','PE'),(2803,'Bom Conselho','PE'),(2804,'Bom Jardim','PE'),(2805,'Bonito','PE'),(2806,'Brejão','PE'),(2807,'Brejinho','PE'),(2808,'Brejo da Madre de Deus','PE'),(2809,'Buenos Aires','PE'),(2810,'Buíque','PE'),(2811,'Cabo de Santo Agostinho','PE'),(2812,'Cabrobó','PE'),(2813,'Cachoeirinha','PE'),(2814,'Caetés','PE'),(2815,'Calçado','PE'),(2816,'Calumbi','PE'),(2817,'Camaragibe','PE'),(2818,'Camocim de São Félix','PE'),(2819,'Camutanga','PE'),(2820,'Canhotinho','PE'),(2821,'Capoeiras','PE'),(2822,'Carnaíba','PE'),(2823,'Carnaubeira da Penha','PE'),(2824,'Carpina','PE'),(2825,'Caruaru','PE'),(2826,'Casinhas','PE'),(2827,'Catende','PE'),(2828,'Cedro','PE'),(2829,'Chã de Alegria','PE'),(2830,'Chã Grande','PE'),(2831,'Condado','PE'),(2832,'Correntes','PE'),(2833,'Cortês','PE'),(2834,'Cumaru','PE'),(2835,'Cupira','PE'),(2836,'Custódia','PE'),(2837,'Dormentes','PE'),(2838,'Escada','PE'),(2839,'Exu','PE'),(2840,'Feira Nova','PE'),(2841,'Fernando de Noronha','PE'),(2842,'Ferreiros','PE'),(2843,'Flores','PE'),(2844,'Floresta','PE'),(2845,'Frei Miguelinho','PE'),(2846,'Gameleira','PE'),(2847,'Garanhuns','PE'),(2848,'Glória do Goitá','PE'),(2849,'Goiana','PE'),(2850,'Granito','PE'),(2851,'Gravatá','PE'),(2852,'Iati','PE'),(2853,'Ibimirim','PE'),(2854,'Ibirajuba','PE'),(2855,'Igarassu','PE'),(2856,'Iguaraci','PE'),(2857,'Inajá','PE'),(2858,'Ingazeira','PE'),(2859,'Ipojuca','PE'),(2860,'Ipubi','PE'),(2861,'Itacuruba','PE'),(2862,'Itaíba','PE'),(2863,'Itamaracá','PE'),(2864,'Itambé','PE'),(2865,'Itapetim','PE'),(2866,'Itapissuma','PE'),(2867,'Itaquitinga','PE'),(2868,'Jaboatão dos Guararapes','PE'),(2869,'Jaqueira','PE'),(2870,'Jataúba','PE'),(2871,'Jatobá','PE'),(2872,'João Alfredo','PE'),(2873,'Joaquim Nabuco','PE'),(2874,'Jucati','PE'),(2875,'Jupi','PE'),(2876,'Jurema','PE'),(2877,'Lagoa do Carro','PE'),(2878,'Lagoa do Itaenga','PE'),(2879,'Lagoa do Ouro','PE'),(2880,'Lagoa dos Gatos','PE'),(2881,'Lagoa Grande','PE'),(2882,'Lajedo','PE'),(2883,'Limoeiro','PE'),(2884,'Macaparana','PE'),(2885,'Machados','PE'),(2886,'Manari','PE'),(2887,'Maraial','PE'),(2888,'Mirandiba','PE'),(2889,'Moreilândia','PE'),(2890,'Moreno','PE'),(2891,'Nazaré da Mata','PE'),(2892,'Olinda','PE'),(2893,'Orobó','PE'),(2894,'Orocó','PE'),(2895,'Ouricuri','PE'),(2896,'Palmares','PE'),(2897,'Palmeirina','PE'),(2898,'Panelas','PE'),(2899,'Paranatama','PE'),(2900,'Parnamirim','PE'),(2901,'Passira','PE'),(2902,'Paudalho','PE'),(2903,'Paulista','PE'),(2904,'Pedra','PE'),(2905,'Pesqueira','PE'),(2906,'Petrolândia','PE'),(2907,'Petrolina','PE'),(2908,'Poção','PE'),(2909,'Pombos','PE'),(2910,'Primavera','PE'),(2911,'Quipapá','PE'),(2912,'Quixaba','PE'),(2913,'Recife','PE'),(2914,'Riacho das Almas','PE'),(2915,'Ribeirão','PE'),(2916,'Rio Formoso','PE'),(2917,'Sairé','PE'),(2918,'Salgadinho','PE'),(2919,'Salgueiro','PE'),(2920,'Saloá','PE'),(2921,'Sanharó','PE'),(2922,'Santa Cruz','PE'),(2923,'Santa Cruz da Baixa Verde','PE'),(2924,'Santa Cruz do Capibaribe','PE'),(2925,'Santa Filomena','PE'),(2926,'Santa Maria da Boa Vista','PE'),(2927,'Santa Maria do Cambucá','PE'),(2928,'Santa Terezinha','PE'),(2929,'São Benedito do Sul','PE'),(2930,'São Bento do Una','PE'),(2931,'São Caitano','PE'),(2932,'São João','PE'),(2933,'São Joaquim do Monte','PE'),(2934,'São José da Coroa Grande','PE'),(2935,'São José do Belmonte','PE'),(2936,'São José do Egito','PE'),(2937,'São Lourenço da Mata','PE'),(2938,'São Vicente Ferrer','PE'),(2939,'Serra Talhada','PE'),(2940,'Serrita','PE'),(2941,'Sertânia','PE'),(2942,'Sirinhaém','PE'),(2943,'Solidão','PE'),(2944,'Surubim','PE'),(2945,'Tabira','PE'),(2946,'Tacaimbó','PE'),(2947,'Tacaratu','PE'),(2948,'Tamandaré','PE'),(2949,'Taquaritinga do Norte','PE'),(2950,'Terezinha','PE'),(2951,'Terra Nova','PE'),(2952,'Timbaúba','PE'),(2953,'Toritama','PE'),(2954,'Tracunhaém','PE'),(2955,'Trindade','PE'),(2956,'Triunfo','PE'),(2957,'Tupanatinga','PE'),(2958,'Tuparetama','PE'),(2959,'Venturosa','PE'),(2960,'Verdejante','PE'),(2961,'Vertente do Lério','PE'),(2962,'Vertentes','PE'),(2963,'Vicência','PE'),(2964,'Vitória de Santo Antão','PE'),(2965,'Xexéu','PE'),(2966,'Acauã','PI'),(2967,'Agricolândia','PI'),(2968,'Água Branca','PI'),(2969,'Alagoinha do Piauí','PI'),(2970,'Alegrete do Piauí','PI'),(2971,'Alto Longá','PI'),(2972,'Altos','PI'),(2973,'Alvorada do Gurguéia','PI'),(2974,'Amarante','PI'),(2975,'Angical do Piauí','PI'),(2976,'Anísio de Abreu','PI'),(2977,'Antônio Almeida','PI'),(2978,'Aroazes','PI'),(2979,'Arraial','PI'),(2980,'Assunção do Piauí','PI'),(2981,'Avelino Lopes','PI'),(2982,'Baixa Grande do Ribeiro','PI'),(2983,'Barra d\'Alcântara','PI'),(2984,'Barras','PI'),(2985,'Barreiras do Piauí','PI'),(2986,'Barro Duro','PI'),(2987,'Batalha','PI'),(2988,'Bela Vista do Piauí','PI'),(2989,'Belém do Piauí','PI'),(2990,'Beneditinos','PI'),(2991,'Bertolínia','PI'),(2992,'Betânia do Piauí','PI'),(2993,'Boa Hora','PI'),(2994,'Bocaina','PI'),(2995,'Bom Jesus','PI'),(2996,'Bom Princípio do Piauí','PI'),(2997,'Bonfim do Piauí','PI'),(2998,'Boqueirão do Piauí','PI'),(2999,'Brasileira','PI'),(3000,'Brejo do Piauí','PI'),(3001,'Buriti dos Lopes','PI'),(3002,'Buriti dos Montes','PI'),(3003,'Cabeceiras do Piauí','PI'),(3004,'Cajazeiras do Piauí','PI'),(3005,'Cajueiro da Praia','PI'),(3006,'Caldeirão Grande do Piauí','PI'),(3007,'Campinas do Piauí','PI'),(3008,'Campo Alegre do Fidalgo','PI'),(3009,'Campo Grande do Piauí','PI'),(3010,'Campo Largo do Piauí','PI'),(3011,'Campo Maior','PI'),(3012,'Canavieira','PI'),(3013,'Canto do Buriti','PI'),(3014,'Capitão de Campos','PI'),(3015,'Capitão Gervásio Oliveira','PI'),(3016,'Caracol','PI'),(3017,'Caraúbas do Piauí','PI'),(3018,'Caridade do Piauí','PI'),(3019,'Castelo do Piauí','PI'),(3020,'Caxingó','PI'),(3021,'Cocal','PI'),(3022,'Cocal de Telha','PI'),(3023,'Cocal dos Alves','PI'),(3024,'Coivaras','PI'),(3025,'Colônia do Gurguéia','PI'),(3026,'Colônia do Piauí','PI'),(3027,'Conceição do Canindé','PI'),(3028,'Coronel José Dias','PI'),(3029,'Corrente','PI'),(3030,'Cristalândia do Piauí','PI'),(3031,'Cristino Castro','PI'),(3032,'Curimatá','PI'),(3033,'Currais','PI'),(3034,'Curral Novo do Piauí','PI'),(3035,'Curralinhos','PI'),(3036,'Demerval Lobão','PI'),(3037,'Dirceu Arcoverde','PI'),(3038,'Dom Expedito Lopes','PI'),(3039,'Dom Inocêncio','PI'),(3040,'Domingos Mourão','PI'),(3041,'Elesbão Veloso','PI'),(3042,'Eliseu Martins','PI'),(3043,'Esperantina','PI'),(3044,'Fartura do Piauí','PI'),(3045,'Flores do Piauí','PI'),(3046,'Floresta do Piauí','PI'),(3047,'Floriano','PI'),(3048,'Francinópolis','PI'),(3049,'Francisco Ayres','PI'),(3050,'Francisco Macedo','PI'),(3051,'Francisco Santos','PI'),(3052,'Fronteiras','PI'),(3053,'Geminiano','PI'),(3054,'Gilbués','PI'),(3055,'Guadalupe','PI'),(3056,'Guaribas','PI'),(3057,'Hugo Napoleão','PI'),(3058,'Ilha Grande','PI'),(3059,'Inhuma','PI'),(3060,'Ipiranga do Piauí','PI'),(3061,'Isaías Coelho','PI'),(3062,'Itainópolis','PI'),(3063,'Itaueira','PI'),(3064,'Jacobina do Piauí','PI'),(3065,'Jaicós','PI'),(3066,'Jardim do Mulato','PI'),(3067,'Jatobá do Piauí','PI'),(3068,'Jerumenha','PI'),(3069,'João Costa','PI'),(3070,'Joaquim Pires','PI'),(3071,'Joca Marques','PI'),(3072,'José de Freitas','PI'),(3073,'Juazeiro do Piauí','PI'),(3074,'Júlio Borges','PI'),(3075,'Jurema','PI'),(3076,'Lagoa Alegre','PI'),(3077,'Lagoa de São Francisco','PI'),(3078,'Lagoa do Barro do Piauí','PI'),(3079,'Lagoa do Piauí','PI'),(3080,'Lagoa do Sítio','PI'),(3081,'Lagoinha do Piauí','PI'),(3082,'Landri Sales','PI'),(3083,'Luís Correia','PI'),(3084,'Luzilândia','PI'),(3085,'Madeiro','PI'),(3086,'Manoel Emídio','PI'),(3087,'Marcolândia','PI'),(3088,'Marcos Parente','PI'),(3089,'Massapê do Piauí','PI'),(3090,'Matias Olímpio','PI'),(3091,'Miguel Alves','PI'),(3092,'Miguel Leão','PI'),(3093,'Milton Brandão','PI'),(3094,'Monsenhor Gil','PI'),(3095,'Monsenhor Hipólito','PI'),(3096,'Monte Alegre do Piauí','PI'),(3097,'Morro Cabeça no Tempo','PI'),(3098,'Morro do Chapéu do Piauí','PI'),(3099,'Murici dos Portelas','PI'),(3100,'Nazaré do Piauí','PI'),(3101,'Nossa Senhora de Nazaré','PI'),(3102,'Nossa Senhora dos Remédios','PI'),(3103,'Nova Santa Rita','PI'),(3104,'Novo Oriente do Piauí','PI'),(3105,'Novo Santo Antônio','PI'),(3106,'Oeiras','PI'),(3107,'Olho d\'Água do Piauí','PI'),(3108,'Padre Marcos','PI'),(3109,'Paes Landim','PI'),(3110,'Pajeú do Piauí','PI'),(3111,'Palmeira do Piauí','PI'),(3112,'Palmeirais','PI'),(3113,'Paquetá','PI'),(3114,'Parnaguá','PI'),(3115,'Parnaíba','PI'),(3116,'Passagem Franca do Piauí','PI'),(3117,'Patos do Piauí','PI'),(3118,'Pau d\'Arco do Piauí','PI'),(3119,'Paulistana','PI'),(3120,'Pavussu','PI'),(3121,'Pedro II','PI'),(3122,'Pedro Laurentino','PI'),(3123,'Picos','PI'),(3124,'Pimenteiras','PI'),(3125,'Pio IX','PI'),(3126,'Piracuruca','PI'),(3127,'Piripiri','PI'),(3128,'Porto','PI'),(3129,'Porto Alegre do Piauí','PI'),(3130,'Prata do Piauí','PI'),(3131,'Queimada Nova','PI'),(3132,'Redenção do Gurguéia','PI'),(3133,'Regeneração','PI'),(3134,'Riacho Frio','PI'),(3135,'Ribeira do Piauí','PI'),(3136,'Ribeiro Gonçalves','PI'),(3137,'Rio Grande do Piauí','PI'),(3138,'Santa Cruz do Piauí','PI'),(3139,'Santa Cruz dos Milagres','PI'),(3140,'Santa Filomena','PI'),(3141,'Santa Luz','PI'),(3142,'Santa Rosa do Piauí','PI'),(3143,'Santana do Piauí','PI'),(3144,'Santo Antônio de Lisboa','PI'),(3145,'Santo Antônio dos Milagres','PI'),(3146,'Santo Inácio do Piauí','PI'),(3147,'São Braz do Piauí','PI'),(3148,'São Félix do Piauí','PI'),(3149,'São Francisco de Assis do Piauí','PI'),(3150,'São Francisco do Piauí','PI'),(3151,'São Gonçalo do Gurguéia','PI'),(3152,'São Gonçalo do Piauí','PI'),(3153,'São João da Canabrava','PI'),(3154,'São João da Fronteira','PI'),(3155,'São João da Serra','PI'),(3156,'São João da Varjota','PI'),(3157,'São João do Arraial','PI'),(3158,'São João do Piauí','PI'),(3159,'São José do Divino','PI'),(3160,'São José do Peixe','PI'),(3161,'São José do Piauí','PI'),(3162,'São Julião','PI'),(3163,'São Lourenço do Piauí','PI'),(3164,'São Luis do Piauí','PI'),(3165,'São Miguel da Baixa Grande','PI'),(3166,'São Miguel do Fidalgo','PI'),(3167,'São Miguel do Tapuio','PI'),(3168,'São Pedro do Piauí','PI'),(3169,'São Raimundo Nonato','PI'),(3170,'Sebastião Barros','PI'),(3171,'Sebastião Leal','PI'),(3172,'Sigefredo Pacheco','PI'),(3173,'Simões','PI'),(3174,'Simplício Mendes','PI'),(3175,'Socorro do Piauí','PI'),(3176,'Sussuapara','PI'),(3177,'Tamboril do Piauí','PI'),(3178,'Tanque do Piauí','PI'),(3179,'Teresina','PI'),(3180,'União','PI'),(3181,'Uruçuí','PI'),(3182,'Valença do Piauí','PI'),(3183,'Várzea Branca','PI'),(3184,'Várzea Grande','PI'),(3185,'Vera Mendes','PI'),(3186,'Vila Nova do Piauí','PI'),(3187,'Wall Ferraz','PI'),(3188,'Abatiá','PR'),(3189,'Adrianópolis','PR'),(3190,'Agudos do Sul','PR'),(3191,'Almirante Tamandaré','PR'),(3192,'Altamira do Paraná','PR'),(3193,'Alto Paraná','PR'),(3194,'Alto Piquiri','PR'),(3195,'Altônia','PR'),(3196,'Alvorada do Sul','PR'),(3197,'Amaporã','PR'),(3198,'Ampére','PR'),(3199,'Anahy','PR'),(3200,'Andirá','PR'),(3201,'Ângulo','PR'),(3202,'Antonina','PR'),(3203,'Antônio Olinto','PR'),(3204,'Apucarana','PR'),(3205,'Arapongas','PR'),(3206,'Arapoti','PR'),(3207,'Arapuã','PR'),(3208,'Araruna','PR'),(3209,'Araucária','PR'),(3210,'Ariranha do Ivaí','PR'),(3211,'Assaí','PR'),(3212,'Assis Chateaubriand','PR'),(3213,'Astorga','PR'),(3214,'Atalaia','PR'),(3215,'Balsa Nova','PR'),(3216,'Bandeirantes','PR'),(3217,'Barbosa Ferraz','PR'),(3218,'Barra do Jacaré','PR'),(3219,'Barracão','PR'),(3220,'Bela Vista da Caroba','PR'),(3221,'Bela Vista do Paraíso','PR'),(3222,'Bituruna','PR'),(3223,'Boa Esperança','PR'),(3224,'Boa Esperança do Iguaçu','PR'),(3225,'Boa Ventura de São Roque','PR'),(3226,'Boa Vista da Aparecida','PR'),(3227,'Bocaiúva do Sul','PR'),(3228,'Bom Jesus do Sul','PR'),(3229,'Bom Sucesso','PR'),(3230,'Bom Sucesso do Sul','PR'),(3231,'Borrazópolis','PR'),(3232,'Braganey','PR'),(3233,'Brasilândia do Sul','PR'),(3234,'Cafeara','PR'),(3235,'Cafelândia','PR'),(3236,'Cafezal do Sul','PR'),(3237,'Califórnia','PR'),(3238,'Cambará','PR'),(3239,'Cambé','PR'),(3240,'Cambira','PR'),(3241,'Campina da Lagoa','PR'),(3242,'Campina do Simão','PR'),(3243,'Campina Grande do Sul','PR'),(3244,'Campo Bonito','PR'),(3245,'Campo do Tenente','PR'),(3246,'Campo Largo','PR'),(3247,'Campo Magro','PR'),(3248,'Campo Mourão','PR'),(3249,'Cândido de Abreu','PR'),(3250,'Candói','PR'),(3251,'Cantagalo','PR'),(3252,'Capanema','PR'),(3253,'Capitão Leônidas Marques','PR'),(3254,'Carambeí','PR'),(3255,'Carlópolis','PR'),(3256,'Cascavel','PR'),(3257,'Castro','PR'),(3258,'Catanduvas','PR'),(3259,'Centenário do Sul','PR'),(3260,'Cerro Azul','PR'),(3261,'Céu Azul','PR'),(3262,'Chopinzinho','PR'),(3263,'Cianorte','PR'),(3264,'Cidade Gaúcha','PR'),(3265,'Clevelândia','PR'),(3266,'Colombo','PR'),(3267,'Colorado','PR'),(3268,'Congonhinhas','PR'),(3269,'Conselheiro Mairinck','PR'),(3270,'Contenda','PR'),(3271,'Corbélia','PR'),(3272,'Cornélio Procópio','PR'),(3273,'Coronel Domingos Soares','PR'),(3274,'Coronel Vivida','PR'),(3275,'Corumbataí do Sul','PR'),(3276,'Cruz Machado','PR'),(3277,'Cruzeiro do Iguaçu','PR'),(3278,'Cruzeiro do Oeste','PR'),(3279,'Cruzeiro do Sul','PR'),(3280,'Cruzmaltina','PR'),(3281,'Curitiba','PR'),(3282,'Curiúva','PR'),(3283,'Diamante d\'Oeste','PR'),(3284,'Diamante do Norte','PR'),(3285,'Diamante do Sul','PR'),(3286,'Dois Vizinhos','PR'),(3287,'Douradina','PR'),(3288,'Doutor Camargo','PR'),(3289,'Doutor Ulysses','PR'),(3290,'Enéas Marques','PR'),(3291,'Engenheiro Beltrão','PR'),(3292,'Entre Rios do Oeste','PR'),(3293,'Esperança Nova','PR'),(3294,'Espigão Alto do Iguaçu','PR'),(3295,'Farol','PR'),(3296,'Faxinal','PR'),(3297,'Fazenda Rio Grande','PR'),(3298,'Fênix','PR'),(3299,'Fernandes Pinheiro','PR'),(3300,'Figueira','PR'),(3301,'Flor da Serra do Sul','PR'),(3302,'Floraí','PR'),(3303,'Floresta','PR'),(3304,'Florestópolis','PR'),(3305,'Flórida','PR'),(3306,'Formosa do Oeste','PR'),(3307,'Foz do Iguaçu','PR'),(3308,'Foz do Jordão','PR'),(3309,'Francisco Alves','PR'),(3310,'Francisco Beltrão','PR'),(3311,'General Carneiro','PR'),(3312,'Godoy Moreira','PR'),(3313,'Goioerê','PR'),(3314,'Goioxim','PR'),(3315,'Grandes Rios','PR'),(3316,'Guaíra','PR'),(3317,'Guairaçá','PR'),(3318,'Guamiranga','PR'),(3319,'Guapirama','PR'),(3320,'Guaporema','PR'),(3321,'Guaraci','PR'),(3322,'Guaraniaçu','PR'),(3323,'Guarapuava','PR'),(3324,'Guaraqueçaba','PR'),(3325,'Guaratuba','PR'),(3326,'Honório Serpa','PR'),(3327,'Ibaiti','PR'),(3328,'Ibema','PR'),(3329,'Ibiporã','PR'),(3330,'Icaraíma','PR'),(3331,'Iguaraçu','PR'),(3332,'Iguatu','PR'),(3333,'Imbaú','PR'),(3334,'Imbituva','PR'),(3335,'Inácio Martins','PR'),(3336,'Inajá','PR'),(3337,'Indianópolis','PR'),(3338,'Ipiranga','PR'),(3339,'Iporã','PR'),(3340,'Iracema do Oeste','PR'),(3341,'Irati','PR'),(3342,'Iretama','PR'),(3343,'Itaguajé','PR'),(3344,'Itaipulândia','PR'),(3345,'Itambaracá','PR'),(3346,'Itambé','PR'),(3347,'Itapejara d\'Oeste','PR'),(3348,'Itaperuçu','PR'),(3349,'Itaúna do Sul','PR'),(3350,'Ivaí','PR'),(3351,'Ivaiporã','PR'),(3352,'Ivaté','PR'),(3353,'Ivatuba','PR'),(3354,'Jaboti','PR'),(3355,'Jacarezinho','PR'),(3356,'Jaguapitã','PR'),(3357,'Jaguariaíva','PR'),(3358,'Jandaia do Sul','PR'),(3359,'Janiópolis','PR'),(3360,'Japira','PR'),(3361,'Japurá','PR'),(3362,'Jardim Alegre','PR'),(3363,'Jardim Olinda','PR'),(3364,'Jataizinho','PR'),(3365,'Jesuítas','PR'),(3366,'Joaquim Távora','PR'),(3367,'Jundiaí do Sul','PR'),(3368,'Juranda','PR'),(3369,'Jussara','PR'),(3370,'Kaloré','PR'),(3371,'Lapa','PR'),(3372,'Laranjal','PR'),(3373,'Laranjeiras do Sul','PR'),(3374,'Leópolis','PR'),(3375,'Lidianópolis','PR'),(3376,'Lindoeste','PR'),(3377,'Loanda','PR'),(3378,'Lobato','PR'),(3379,'Londrina','PR'),(3380,'Luiziana','PR'),(3381,'Lunardelli','PR'),(3382,'Lupionópolis','PR'),(3383,'Mallet','PR'),(3384,'Mamborê','PR'),(3385,'Mandaguaçu','PR'),(3386,'Mandaguari','PR'),(3387,'Mandirituba','PR'),(3388,'Manfrinópolis','PR'),(3389,'Mangueirinha','PR'),(3390,'Manoel Ribas','PR'),(3391,'Marechal Cândido Rondon','PR'),(3392,'Maria Helena','PR'),(3393,'Marialva','PR'),(3394,'Marilândia do Sul','PR'),(3395,'Marilena','PR'),(3396,'Mariluz','PR'),(3397,'Maringá','PR'),(3398,'Mariópolis','PR'),(3399,'Maripá','PR'),(3400,'Marmeleiro','PR'),(3401,'Marquinho','PR'),(3402,'Marumbi','PR'),(3403,'Matelândia','PR'),(3404,'Matinhos','PR'),(3405,'Mato Rico','PR'),(3406,'Mauá da Serra','PR'),(3407,'Medianeira','PR'),(3408,'Mercedes','PR'),(3409,'Mirador','PR'),(3410,'Miraselva','PR'),(3411,'Missal','PR'),(3412,'Moreira Sales','PR'),(3413,'Morretes','PR'),(3414,'Munhoz de Melo','PR'),(3415,'Nossa Senhora das Graças','PR'),(3416,'Nova Aliança do Ivaí','PR'),(3417,'Nova América da Colina','PR'),(3418,'Nova Aurora','PR'),(3419,'Nova Cantu','PR'),(3420,'Nova Esperança','PR'),(3421,'Nova Esperança do Sudoeste','PR'),(3422,'Nova Fátima','PR'),(3423,'Nova Laranjeiras','PR'),(3424,'Nova Londrina','PR'),(3425,'Nova Olímpia','PR'),(3426,'Nova Prata do Iguaçu','PR'),(3427,'Nova Santa Bárbara','PR'),(3428,'Nova Santa Rosa','PR'),(3429,'Nova Tebas','PR'),(3430,'Novo Itacolomi','PR'),(3431,'Ortigueira','PR'),(3432,'Ourizona','PR'),(3433,'Ouro Verde do Oeste','PR'),(3434,'Paiçandu','PR'),(3435,'Palmas','PR'),(3436,'Palmeira','PR'),(3437,'Palmital','PR'),(3438,'Palotina','PR'),(3439,'Paraíso do Norte','PR'),(3440,'Paranacity','PR'),(3441,'Paranaguá','PR'),(3442,'Paranapoema','PR'),(3443,'Paranavaí','PR'),(3444,'Pato Bragado','PR'),(3445,'Pato Branco','PR'),(3446,'Paula Freitas','PR'),(3447,'Paulo Frontin','PR'),(3448,'Peabiru','PR'),(3449,'Perobal','PR'),(3450,'Pérola','PR'),(3451,'Pérola d\'Oeste','PR'),(3452,'Piên','PR'),(3453,'Pinhais','PR'),(3454,'Pinhal de São Bento','PR'),(3455,'Pinhalão','PR'),(3456,'Pinhão','PR'),(3457,'Piraí do Sul','PR'),(3458,'Piraquara','PR'),(3459,'Pitanga','PR'),(3460,'Pitangueiras','PR'),(3461,'Planaltina do Paraná','PR'),(3462,'Planalto','PR'),(3463,'Ponta Grossa','PR'),(3464,'Pontal do Paraná','PR'),(3465,'Porecatu','PR'),(3466,'Porto Amazonas','PR'),(3467,'Porto Barreiro','PR'),(3468,'Porto Rico','PR'),(3469,'Porto Vitória','PR'),(3470,'Prado Ferreira','PR'),(3471,'Pranchita','PR'),(3472,'Presidente Castelo Branco','PR'),(3473,'Primeiro de Maio','PR'),(3474,'Prudentópolis','PR'),(3475,'Quarto Centenário','PR'),(3476,'Quatiguá','PR'),(3477,'Quatro Barras','PR'),(3478,'Quatro Pontes','PR'),(3479,'Quedas do Iguaçu','PR'),(3480,'Querência do Norte','PR'),(3481,'Quinta do Sol','PR'),(3482,'Quitandinha','PR'),(3483,'Ramilândia','PR'),(3484,'Rancho Alegre','PR'),(3485,'Rancho Alegre d\'Oeste','PR'),(3486,'Realeza','PR'),(3487,'Rebouças','PR'),(3488,'Renascença','PR'),(3489,'Reserva','PR'),(3490,'Reserva do Iguaçu','PR'),(3491,'Ribeirão Claro','PR'),(3492,'Ribeirão do Pinhal','PR'),(3493,'Rio Azul','PR'),(3494,'Rio Bom','PR'),(3495,'Rio Bonito do Iguaçu','PR'),(3496,'Rio Branco do Ivaí','PR'),(3497,'Rio Branco do Sul','PR'),(3498,'Rio Negro','PR'),(3499,'Rolândia','PR'),(3500,'Roncador','PR'),(3501,'Rondon','PR'),(3502,'Rosário do Ivaí','PR'),(3503,'Sabáudia','PR'),(3504,'Salgado Filho','PR'),(3505,'Salto do Itararé','PR'),(3506,'Salto do Lontra','PR'),(3507,'Santa Amélia','PR'),(3508,'Santa Cecília do Pavão','PR'),(3509,'Santa Cruz Monte Castelo','PR'),(3510,'Santa Fé','PR'),(3511,'Santa Helena','PR'),(3512,'Santa Inês','PR'),(3513,'Santa Isabel do Ivaí','PR'),(3514,'Santa Izabel do Oeste','PR'),(3515,'Santa Lúcia','PR'),(3516,'Santa Maria do Oeste','PR'),(3517,'Santa Mariana','PR'),(3518,'Santa Mônica','PR'),(3519,'Santa Tereza do Oeste','PR'),(3520,'Santa Terezinha de Itaipu','PR'),(3521,'Santana do Itararé','PR'),(3522,'Santo Antônio da Platina','PR'),(3523,'Santo Antônio do Caiuá','PR'),(3524,'Santo Antônio do Paraíso','PR'),(3525,'Santo Antônio do Sudoeste','PR'),(3526,'Santo Inácio','PR'),(3527,'São Carlos do Ivaí','PR'),(3528,'São Jerônimo da Serra','PR'),(3529,'São João','PR'),(3530,'São João do Caiuá','PR'),(3531,'São João do Ivaí','PR'),(3532,'São João do Triunfo','PR'),(3533,'São Jorge d\'Oeste','PR'),(3534,'São Jorge do Ivaí','PR'),(3535,'São Jorge do Patrocínio','PR'),(3536,'São José da Boa Vista','PR'),(3537,'São José das Palmeiras','PR'),(3538,'São José dos Pinhais','PR'),(3539,'São Manoel do Paraná','PR'),(3540,'São Mateus do Sul','PR'),(3541,'São Miguel do Iguaçu','PR'),(3542,'São Pedro do Iguaçu','PR'),(3543,'São Pedro do Ivaí','PR'),(3544,'São Pedro do Paraná','PR'),(3545,'São Sebastião da Amoreira','PR'),(3546,'São Tomé','PR'),(3547,'Sapopema','PR'),(3548,'Sarandi','PR'),(3549,'Saudade do Iguaçu','PR'),(3550,'Sengés','PR'),(3551,'Serranópolis do Iguaçu','PR'),(3552,'Sertaneja','PR'),(3553,'Sertanópolis','PR'),(3554,'Siqueira Campos','PR'),(3555,'Sulina','PR'),(3556,'Tamarana','PR'),(3557,'Tamboara','PR'),(3558,'Tapejara','PR'),(3559,'Tapira','PR'),(3560,'Teixeira Soares','PR'),(3561,'Telêmaco Borba','PR'),(3562,'Terra Boa','PR'),(3563,'Terra Rica','PR'),(3564,'Terra Roxa','PR'),(3565,'Tibagi','PR'),(3566,'Tijucas do Sul','PR'),(3567,'Toledo','PR'),(3568,'Tomazina','PR'),(3569,'Três Barras do Paraná','PR'),(3570,'Tunas do Paraná','PR'),(3571,'Tuneiras do Oeste','PR'),(3572,'Tupãssi','PR'),(3573,'Turvo','PR'),(3574,'Ubiratã','PR'),(3575,'Umuarama','PR'),(3576,'União da Vitória','PR'),(3577,'Uniflor','PR'),(3578,'Uraí','PR'),(3579,'Ventania','PR'),(3580,'Vera Cruz do Oeste','PR'),(3581,'Verê','PR'),(3582,'Vila Alta','PR'),(3583,'Virmond','PR'),(3584,'Vitorino','PR'),(3585,'Wenceslau Braz','PR'),(3586,'Xambrê','PR'),(3587,'Angra dos Reis','RJ'),(3588,'Aperibé','RJ'),(3589,'Araruama','RJ'),(3590,'Areal','RJ'),(3591,'Armação de Búzios','RJ'),(3592,'Arraial do Cabo','RJ'),(3593,'Barra do Piraí','RJ'),(3594,'Barra Mansa','RJ'),(3595,'Belford Roxo','RJ'),(3596,'Bom Jardim','RJ'),(3597,'Bom Jesus do Itabapoana','RJ'),(3598,'Cabo Frio','RJ'),(3599,'Cachoeiras de Macacu','RJ'),(3600,'Cambuci','RJ'),(3601,'Campos dos Goytacazes','RJ'),(3602,'Cantagalo','RJ'),(3603,'Carapebus','RJ'),(3604,'Cardoso Moreira','RJ'),(3605,'Carmo','RJ'),(3606,'Casimiro de Abreu','RJ'),(3607,'Comendador Levy Gasparian','RJ'),(3608,'Conceição de Macabu','RJ'),(3609,'Cordeiro','RJ'),(3610,'Duas Barras','RJ'),(3611,'Duque de Caxias','RJ'),(3612,'Engenheiro Paulo de Frontin','RJ'),(3613,'Guapimirim','RJ'),(3614,'Iguaba Grande','RJ'),(3615,'Itaboraí','RJ'),(3616,'Itaguaí','RJ'),(3617,'Italva','RJ'),(3618,'Itaocara','RJ'),(3619,'Itaperuna','RJ'),(3620,'Itatiaia','RJ'),(3621,'Japeri','RJ'),(3622,'Laje do Muriaé','RJ'),(3623,'Macaé','RJ'),(3624,'Macuco','RJ'),(3625,'Magé','RJ'),(3626,'Mangaratiba','RJ'),(3627,'Maricá','RJ'),(3628,'Mendes','RJ'),(3629,'Mesquita','RJ'),(3630,'Miguel Pereira','RJ'),(3631,'Miracema','RJ'),(3632,'Natividade','RJ'),(3633,'Nilópolis','RJ'),(3634,'Niterói','RJ'),(3635,'Nova Friburgo','RJ'),(3636,'Nova Iguaçu','RJ'),(3637,'Paracambi','RJ'),(3638,'Paraíba do Sul','RJ'),(3639,'Parati','RJ'),(3640,'Paty do Alferes','RJ'),(3641,'Petrópolis','RJ'),(3642,'Pinheiral','RJ'),(3643,'Piraí','RJ'),(3644,'Porciúncula','RJ'),(3645,'Porto Real','RJ'),(3646,'Quatis','RJ'),(3647,'Queimados','RJ'),(3648,'Quissamã','RJ'),(3649,'Resende','RJ'),(3650,'Rio Bonito','RJ'),(3651,'Rio Claro','RJ'),(3652,'Rio das Flores','RJ'),(3653,'Rio das Ostras','RJ'),(3654,'Rio de Janeiro','RJ'),(3655,'Santa Maria Madalena','RJ'),(3656,'Santo Antônio de Pádua','RJ'),(3657,'São Fidélis','RJ'),(3658,'São Francisco de Itabapoana','RJ'),(3659,'São Gonçalo','RJ'),(3660,'São João da Barra','RJ'),(3661,'São João de Meriti','RJ'),(3662,'São José de Ubá','RJ'),(3663,'São José do Vale do Rio Preto','RJ'),(3664,'São Pedro da Aldeia','RJ'),(3665,'São Sebastião do Alto','RJ'),(3666,'Sapucaia','RJ'),(3667,'Saquarema','RJ'),(3668,'Seropédica','RJ'),(3669,'Silva Jardim','RJ'),(3670,'Sumidouro','RJ'),(3671,'Tanguá','RJ'),(3672,'Teresópolis','RJ'),(3673,'Trajano de Morais','RJ'),(3674,'Três Rios','RJ'),(3675,'Valença','RJ'),(3676,'Varre-Sai','RJ'),(3677,'Vassouras','RJ'),(3678,'Volta Redonda','RJ'),(3679,'Acari','RN'),(3680,'Açu','RN'),(3681,'Afonso Bezerra','RN'),(3682,'Água Nova','RN'),(3683,'Alexandria','RN'),(3684,'Almino Afonso','RN'),(3685,'Alto do Rodrigues','RN'),(3686,'Angicos','RN'),(3687,'Antônio Martins','RN'),(3688,'Apodi','RN'),(3689,'Areia Branca','RN'),(3690,'Arês','RN'),(3691,'Augusto Severo','RN'),(3692,'Baía Formosa','RN'),(3693,'Baraúna','RN'),(3694,'Barcelona','RN'),(3695,'Bento Fernandes','RN'),(3696,'Bodó','RN'),(3697,'Bom Jesus','RN'),(3698,'Brejinho','RN'),(3699,'Caiçara do Norte','RN'),(3700,'Caiçara do Rio do Vento','RN'),(3701,'Caicó','RN'),(3702,'Campo Redondo','RN'),(3703,'Canguaretama','RN'),(3704,'Caraúbas','RN'),(3705,'Carnaúba dos Dantas','RN'),(3706,'Carnaubais','RN'),(3707,'Ceará-Mirim','RN'),(3708,'Cerro Corá','RN'),(3709,'Coronel Ezequiel','RN'),(3710,'Coronel João Pessoa','RN'),(3711,'Cruzeta','RN'),(3712,'Currais Novos','RN'),(3713,'Doutor Severiano','RN'),(3714,'Encanto','RN'),(3715,'Equador','RN'),(3716,'Espírito Santo','RN'),(3717,'Extremoz','RN'),(3718,'Felipe Guerra','RN'),(3719,'Fernando Pedroza','RN'),(3720,'Florânia','RN'),(3721,'Francisco Dantas','RN'),(3722,'Frutuoso Gomes','RN'),(3723,'Galinhos','RN'),(3724,'Goianinha','RN'),(3725,'Governador Dix-Sept Rosado','RN'),(3726,'Grossos','RN'),(3727,'Guamaré','RN'),(3728,'Ielmo Marinho','RN'),(3729,'Ipanguaçu','RN'),(3730,'Ipueira','RN'),(3731,'Itajá','RN'),(3732,'Itaú','RN'),(3733,'Jaçanã','RN'),(3734,'Jandaíra','RN'),(3735,'Janduís','RN'),(3736,'Januário Cicco','RN'),(3737,'Japi','RN'),(3738,'Jardim de Angicos','RN'),(3739,'Jardim de Piranhas','RN'),(3740,'Jardim do Seridó','RN'),(3741,'João Câmara','RN'),(3742,'João Dias','RN'),(3743,'José da Penha','RN'),(3744,'Jucurutu','RN'),(3745,'Jundiá','RN'),(3746,'Lagoa d\'Anta','RN'),(3747,'Lagoa de Pedras','RN'),(3748,'Lagoa de Velhos','RN'),(3749,'Lagoa Nova','RN'),(3750,'Lagoa Salgada','RN'),(3751,'Lajes','RN'),(3752,'Lajes Pintadas','RN'),(3753,'Lucrécia','RN'),(3754,'Luís Gomes','RN'),(3755,'Macaíba','RN'),(3756,'Macau','RN'),(3757,'Major Sales','RN'),(3758,'Marcelino Vieira','RN'),(3759,'Martins','RN'),(3760,'Maxaranguape','RN'),(3761,'Messias Targino','RN'),(3762,'Montanhas','RN'),(3763,'Monte Alegre','RN'),(3764,'Monte das Gameleiras','RN'),(3765,'Mossoró','RN'),(3766,'Natal','RN'),(3767,'Nísia Floresta','RN'),(3768,'Nova Cruz','RN'),(3769,'Olho-d\'Água do Borges','RN'),(3770,'Ouro Branco','RN'),(3771,'Paraná','RN'),(3772,'Paraú','RN'),(3773,'Parazinho','RN'),(3774,'Parelhas','RN'),(3775,'Parnamirim','RN'),(3776,'Passa e Fica','RN'),(3777,'Passagem','RN'),(3778,'Patu','RN'),(3779,'Pau dos Ferros','RN'),(3780,'Pedra Grande','RN'),(3781,'Pedra Preta','RN'),(3782,'Pedro Avelino','RN'),(3783,'Pedro Velho','RN'),(3784,'Pendências','RN'),(3785,'Pilões','RN'),(3786,'Poço Branco','RN'),(3787,'Portalegre','RN'),(3788,'Porto do Mangue','RN'),(3789,'Presidente Juscelino','RN'),(3790,'Pureza','RN'),(3791,'Rafael Fernandes','RN'),(3792,'Rafael Godeiro','RN'),(3793,'Riacho da Cruz','RN'),(3794,'Riacho de Santana','RN'),(3795,'Riachuelo','RN'),(3796,'Rio do Fogo','RN'),(3797,'Rodolfo Fernandes','RN'),(3798,'Ruy Barbosa','RN'),(3799,'Santa Cruz','RN'),(3800,'Santa Maria','RN'),(3801,'Santana do Matos','RN'),(3802,'Santana do Seridó','RN'),(3803,'Santo Antônio','RN'),(3804,'São Bento do Norte','RN'),(3805,'São Bento do Trairí','RN'),(3806,'São Fernando','RN'),(3807,'São Francisco do Oeste','RN'),(3808,'São Gonçalo do Amarante','RN'),(3809,'São João do Sabugi','RN'),(3810,'São José de Mipibu','RN'),(3811,'São José do Campestre','RN'),(3812,'São José do Seridó','RN'),(3813,'São Miguel','RN'),(3814,'São Miguel de Touros','RN'),(3815,'São Paulo do Potengi','RN'),(3816,'São Pedro','RN'),(3817,'São Rafael','RN'),(3818,'São Tomé','RN'),(3819,'São Vicente','RN'),(3820,'Senador Elói de Souza','RN'),(3821,'Senador Georgino Avelino','RN'),(3822,'Serra de São Bento','RN'),(3823,'Serra do Mel','RN'),(3824,'Serra Negra do Norte','RN'),(3825,'Serrinha','RN'),(3826,'Serrinha dos Pintos','RN'),(3827,'Severiano Melo','RN'),(3828,'Sítio Novo','RN'),(3829,'Taboleiro Grande','RN'),(3830,'Taipu','RN'),(3831,'Tangará','RN'),(3832,'Tenente Ananias','RN'),(3833,'Tenente Laurentino Cruz','RN'),(3834,'Tibau','RN'),(3835,'Tibau do Sul','RN'),(3836,'Timbaúba dos Batistas','RN'),(3837,'Touros','RN'),(3838,'Triunfo Potiguar','RN'),(3839,'Umarizal','RN'),(3840,'Upanema','RN'),(3841,'Várzea','RN'),(3842,'Venha-Ver','RN'),(3843,'Vera Cruz','RN'),(3844,'Viçosa','RN'),(3845,'Vila Flor','RN'),(3846,'Alta Floresta d\'Oeste','RO'),(3847,'Alto Alegre do Parecis','RO'),(3848,'Alto Paraíso','RO'),(3849,'Alvorada d\'Oeste','RO'),(3850,'Ariquemes','RO'),(3851,'Buritis','RO'),(3852,'Cabixi','RO'),(3853,'Cacaulândia','RO'),(3854,'Cacoal','RO'),(3855,'Campo Novo de Rondônia','RO'),(3856,'Candeias do Jamari','RO'),(3857,'Castanheiras','RO'),(3858,'Cerejeiras','RO'),(3859,'Chupinguaia','RO'),(3860,'Colorado do Oeste','RO'),(3861,'Corumbiara','RO'),(3862,'Costa Marques','RO'),(3863,'Cujubim','RO'),(3864,'Espigão d\'Oeste','RO'),(3865,'Governador Jorge Teixeira','RO'),(3866,'Guajará-Mirim','RO'),(3867,'Itapuã do Oeste','RO'),(3868,'Jaru','RO'),(3869,'Ji-Paraná','RO'),(3870,'Machadinho d\'Oeste','RO'),(3871,'Ministro Andreazza','RO'),(3872,'Mirante da Serra','RO'),(3873,'Monte Negro','RO'),(3874,'Nova Brasilândia d\'Oeste','RO'),(3875,'Nova Mamoré','RO'),(3876,'Nova União','RO'),(3877,'Novo Horizonte do Oeste','RO'),(3878,'Ouro Preto do Oeste','RO'),(3879,'Parecis','RO'),(3880,'Pimenta Bueno','RO'),(3881,'Pimenteiras do Oeste','RO'),(3882,'Porto Velho','RO'),(3883,'Presidente Médici','RO'),(3884,'Primavera de Rondônia','RO'),(3885,'Rio Crespo','RO'),(3886,'Rolim de Moura','RO'),(3887,'Santa Luzia d\'Oeste','RO'),(3888,'São Felipe d\'Oeste','RO'),(3889,'São Francisco do Guaporé','RO'),(3890,'São Miguel do Guaporé','RO'),(3891,'Seringueiras','RO'),(3892,'Teixeirópolis','RO'),(3893,'Theobroma','RO'),(3894,'Urupá','RO'),(3895,'Vale do Anari','RO'),(3896,'Vale do Paraíso','RO'),(3897,'Vilhena','RO'),(3898,'Alto Alegre','RR'),(3899,'Amajari','RR'),(3900,'Boa Vista','RR'),(3901,'Bonfim','RR'),(3902,'Cantá','RR'),(3903,'Caracaraí','RR'),(3904,'Caroebe','RR'),(3905,'Iracema','RR'),(3906,'Mucajaí','RR'),(3907,'Normandia','RR'),(3908,'Pacaraima','RR'),(3909,'Rorainópolis','RR'),(3910,'São João da Baliza','RR'),(3911,'São Luiz','RR'),(3912,'Uiramutã','RR'),(3913,'Aceguá','RS'),(3914,'Água Santa','RS'),(3915,'Agudo','RS'),(3916,'Ajuricaba','RS'),(3917,'Alecrim','RS'),(3918,'Alegrete','RS'),(3919,'Alegria','RS'),(3920,'Almirante Tamandaré do Sul','RS'),(3921,'Alpestre','RS'),(3922,'Alto Alegre','RS'),(3923,'Alto Feliz','RS'),(3924,'Alvorada','RS'),(3925,'Amaral Ferrador','RS'),(3926,'Ametista do Sul','RS'),(3927,'André da Rocha','RS'),(3928,'Anta Gorda','RS'),(3929,'Antônio Prado','RS'),(3930,'Arambaré','RS'),(3931,'Araricá','RS'),(3932,'Aratiba','RS'),(3933,'Arroio do Meio','RS'),(3934,'Arroio do Padre','RS'),(3935,'Arroio do Sal','RS'),(3936,'Arroio do Tigre','RS'),(3937,'Arroio dos Ratos','RS'),(3938,'Arroio Grande','RS'),(3939,'Arvorezinha','RS'),(3940,'Augusto Pestana','RS'),(3941,'Áurea','RS'),(3942,'Bagé','RS'),(3943,'Balneário Pinhal','RS'),(3944,'Barão','RS'),(3945,'Barão de Cotegipe','RS'),(3946,'Barão do Triunfo','RS'),(3947,'Barra do Guarita','RS'),(3948,'Barra do Quaraí','RS'),(3949,'Barra do Ribeiro','RS'),(3950,'Barra do Rio Azul','RS'),(3951,'Barra Funda','RS'),(3952,'Barracão','RS'),(3953,'Barros Cassal','RS'),(3954,'Benjamin Constan do Sul','RS'),(3955,'Bento Gonçalves','RS'),(3956,'Boa Vista das Missões','RS'),(3957,'Boa Vista do Buricá','RS'),(3958,'Boa Vista do Cadeado','RS'),(3959,'Boa Vista do Incra','RS'),(3960,'Boa Vista do Sul','RS'),(3961,'Bom Jesus','RS'),(3962,'Bom Princípio','RS'),(3963,'Bom Progresso','RS'),(3964,'Bom Retiro do Sul','RS'),(3965,'Boqueirão do Leão','RS'),(3966,'Bossoroca','RS'),(3967,'Bozano','RS'),(3968,'Braga','RS'),(3969,'Brochier','RS'),(3970,'Butiá','RS'),(3971,'Caçapava do Sul','RS'),(3972,'Cacequi','RS'),(3973,'Cachoeira do Sul','RS'),(3974,'Cachoeirinha','RS'),(3975,'Cacique Doble','RS'),(3976,'Caibaté','RS'),(3977,'Caiçara','RS'),(3978,'Camaquã','RS'),(3979,'Camargo','RS'),(3980,'Cambará do Sul','RS'),(3981,'Campestre da Serra','RS'),(3982,'Campina das Missões','RS'),(3983,'Campinas do Sul','RS'),(3984,'Campo Bom','RS'),(3985,'Campo Novo','RS'),(3986,'Campos Borges','RS'),(3987,'Candelária','RS'),(3988,'Cândido Godói','RS'),(3989,'Candiota','RS'),(3990,'Canela','RS'),(3991,'Canguçu','RS'),(3992,'Canoas','RS'),(3993,'Canudos do Vale','RS'),(3994,'Capão Bonito do Sul','RS'),(3995,'Capão da Canoa','RS'),(3996,'Capão do Cipó','RS'),(3997,'Capão do Leão','RS'),(3998,'Capela de Santana','RS'),(3999,'Capitão','RS'),(4000,'Capivari do Sul','RS'),(4001,'Caraá','RS'),(4002,'Carazinho','RS'),(4003,'Carlos Barbosa','RS'),(4004,'Carlos Gomes','RS'),(4005,'Casca','RS'),(4006,'Caseiros','RS'),(4007,'Catuípe','RS'),(4008,'Caxias do Sul','RS'),(4009,'Centenário','RS'),(4010,'Cerrito','RS'),(4011,'Cerro Branco','RS'),(4012,'Cerro Grande','RS'),(4013,'Cerro Grande do Sul','RS'),(4014,'Cerro Largo','RS'),(4015,'Chapada','RS'),(4016,'Charqueadas','RS'),(4017,'Charrua','RS'),(4018,'Chiapeta','RS'),(4019,'Chuí','RS'),(4020,'Chuvisca','RS'),(4021,'Cidreira','RS'),(4022,'Ciríaco','RS'),(4023,'Colinas','RS'),(4024,'Colorado','RS'),(4025,'Condor','RS'),(4026,'Constantina','RS'),(4027,'Coqueiro Baixo','RS'),(4028,'Coqueiros do Sul','RS'),(4029,'Coronel Barros','RS'),(4030,'Coronel Bicaco','RS'),(4031,'Coronel Pilar','RS'),(4032,'Cotiporã','RS'),(4033,'Coxilha','RS'),(4034,'Crissiumal','RS'),(4035,'Cristal','RS'),(4036,'Cristal do Sul','RS'),(4037,'Cruz Alta','RS'),(4038,'Cruzaltense','RS'),(4039,'Cruzeiro do Sul','RS'),(4040,'David Canabarro','RS'),(4041,'Derrubadas','RS'),(4042,'Dezesseis de Novembro','RS'),(4043,'Dilermando de Aguiar','RS'),(4044,'Dois Irmãos','RS'),(4045,'Dois Irmãos das Missões','RS'),(4046,'Dois Lajeados','RS'),(4047,'Dom Feliciano','RS'),(4048,'Dom Pedrito','RS'),(4049,'Dom Pedro de Alcântara','RS'),(4050,'Dona Francisca','RS'),(4051,'Doutor Maurício Cardoso','RS'),(4052,'Doutor Ricardo','RS'),(4053,'Eldorado do Sul','RS'),(4054,'Encantado','RS'),(4055,'Encruzilhada do Sul','RS'),(4056,'Engenho Velho','RS'),(4057,'Entre Rios do Sul','RS'),(4058,'Entre-Ijuís','RS'),(4059,'Erebango','RS'),(4060,'Erechim','RS'),(4061,'Ernestina','RS'),(4062,'Erval Grande','RS'),(4063,'Erval Seco','RS'),(4064,'Esmeralda','RS'),(4065,'Esperança do Sul','RS'),(4066,'Espumoso','RS'),(4067,'Estação','RS'),(4068,'Estância Velha','RS'),(4069,'Esteio','RS'),(4070,'Estrela','RS'),(4071,'Estrela Velha','RS'),(4072,'Eugênio de Castro','RS'),(4073,'Fagundes Varela','RS'),(4074,'Farroupilha','RS'),(4075,'Faxinal do Soturno','RS'),(4076,'Faxinalzinho','RS'),(4077,'Fazenda Vilanova','RS'),(4078,'Feliz','RS'),(4079,'Flores da Cunha','RS'),(4080,'Floriano Peixoto','RS'),(4081,'Fontoura Xavier','RS'),(4082,'Formigueiro','RS'),(4083,'Forquetinha','RS'),(4084,'Fortaleza dos Valos','RS'),(4085,'Frederico Westphalen','RS'),(4086,'Garibaldi','RS'),(4087,'Garruchos','RS'),(4088,'Gaurama','RS'),(4089,'General Câmara','RS'),(4090,'Gentil','RS'),(4091,'Getúlio Vargas','RS'),(4092,'Giruá','RS'),(4093,'Glorinha','RS'),(4094,'Gramado','RS'),(4095,'Gramado dos Loureiros','RS'),(4096,'Gramado Xavier','RS'),(4097,'Gravataí','RS'),(4098,'Guabiju','RS'),(4099,'Guaíba','RS'),(4100,'Guaporé','RS'),(4101,'Guarani das Missões','RS'),(4102,'Harmonia','RS'),(4103,'Herval','RS'),(4104,'Herveiras','RS'),(4105,'Horizontina','RS'),(4106,'Hulha Negra','RS'),(4107,'Humaitá','RS'),(4108,'Ibarama','RS'),(4109,'Ibiaçá','RS'),(4110,'Ibiraiaras','RS'),(4111,'Ibirapuitã','RS'),(4112,'Ibirubá','RS'),(4113,'Igrejinha','RS'),(4114,'Ijuí','RS'),(4115,'Ilópolis','RS'),(4116,'Imbé','RS'),(4117,'Imigrante','RS'),(4118,'Independência','RS'),(4119,'Inhacorá','RS'),(4120,'Ipê','RS'),(4121,'Ipiranga do Sul','RS'),(4122,'Iraí','RS'),(4123,'Itaara','RS'),(4124,'Itacurubi','RS'),(4125,'Itapuca','RS'),(4126,'Itaqui','RS'),(4127,'Itati','RS'),(4128,'Itatiba do Sul','RS'),(4129,'Ivorá','RS'),(4130,'Ivoti','RS'),(4131,'Jaboticaba','RS'),(4132,'Jacuizinho','RS'),(4133,'Jacutinga','RS'),(4134,'Jaguarão','RS'),(4135,'Jaguari','RS'),(4136,'Jaquirana','RS'),(4137,'Jari','RS'),(4138,'Jóia','RS'),(4139,'Júlio de Castilhos','RS'),(4140,'Lagoa Bonita do Sul','RS'),(4141,'Lagoa dos Três Cantos','RS'),(4142,'Lagoa Vermelha','RS'),(4143,'Lagoão','RS'),(4144,'Lajeado','RS'),(4145,'Lajeado do Bugre','RS'),(4146,'Lavras do Sul','RS'),(4147,'Liberato Salzano','RS'),(4148,'Lindolfo Collor','RS'),(4149,'Linha Nova','RS'),(4150,'Maçambara','RS'),(4151,'Machadinho','RS'),(4152,'Mampituba','RS'),(4153,'Manoel Viana','RS'),(4154,'Maquiné','RS'),(4155,'Maratá','RS'),(4156,'Marau','RS'),(4157,'Marcelino Ramos','RS'),(4158,'Mariana Pimentel','RS'),(4159,'Mariano Moro','RS'),(4160,'Marques de Souza','RS'),(4161,'Mata','RS'),(4162,'Mato Castelhano','RS'),(4163,'Mato Leitão','RS'),(4164,'Mato Queimado','RS'),(4165,'Maximiliano de Almeida','RS'),(4166,'Minas do Leão','RS'),(4167,'Miraguaí','RS'),(4168,'Montauri','RS'),(4169,'Monte Alegre dos Campos','RS'),(4170,'Monte Belo do Sul','RS'),(4171,'Montenegro','RS'),(4172,'Mormaço','RS'),(4173,'Morrinhos do Sul','RS'),(4174,'Morro Redondo','RS'),(4175,'Morro Reuter','RS'),(4176,'Mostardas','RS'),(4177,'Muçum','RS'),(4178,'Muitos Capões','RS'),(4179,'Muliterno','RS'),(4180,'Não-Me-Toque','RS'),(4181,'Nicolau Vergueiro','RS'),(4182,'Nonoai','RS'),(4183,'Nova Alvorada','RS'),(4184,'Nova Araçá','RS'),(4185,'Nova Bassano','RS'),(4186,'Nova Boa Vista','RS'),(4187,'Nova Bréscia','RS'),(4188,'Nova Candelária','RS'),(4189,'Nova Esperança do Sul','RS'),(4190,'Nova Hartz','RS'),(4191,'Nova Pádua','RS'),(4192,'Nova Palma','RS'),(4193,'Nova Petrópolis','RS'),(4194,'Nova Prata','RS'),(4195,'Nova Ramada','RS'),(4196,'Nova Roma do Sul','RS'),(4197,'Nova Santa Rita','RS'),(4198,'Novo Barreiro','RS'),(4199,'Novo Cabrais','RS'),(4200,'Novo Hamburgo','RS'),(4201,'Novo Machado','RS'),(4202,'Novo Tiradentes','RS'),(4203,'Novo Xingu','RS'),(4204,'Osório','RS'),(4205,'Paim Filho','RS'),(4206,'Palmares do Sul','RS'),(4207,'Palmeira das Missões','RS'),(4208,'Palmitinho','RS'),(4209,'Panambi','RS'),(4210,'Pântano Grande','RS'),(4211,'Paraí','RS'),(4212,'Paraíso do Sul','RS'),(4213,'Pareci Novo','RS'),(4214,'Parobé','RS'),(4215,'Passa Sete','RS'),(4216,'Passo do Sobrado','RS'),(4217,'Passo Fundo','RS'),(4218,'Paulo Bento','RS'),(4219,'Paverama','RS'),(4220,'Pedras Altas','RS'),(4221,'Pedro Osório','RS'),(4222,'Pejuçara','RS'),(4223,'Pelotas','RS'),(4224,'Picada Café','RS'),(4225,'Pinhal','RS'),(4226,'Pinhal da Serra','RS'),(4227,'Pinhal Grande','RS'),(4228,'Pinheirinho do Vale','RS'),(4229,'Pinheiro Machado','RS'),(4230,'Pirapó','RS'),(4231,'Piratini','RS'),(4232,'Planalto','RS'),(4233,'Poço das Antas','RS'),(4234,'Pontão','RS'),(4235,'Ponte Preta','RS'),(4236,'Portão','RS'),(4237,'Porto Alegre','RS'),(4238,'Porto Lucena','RS'),(4239,'Porto Mauá','RS'),(4240,'Porto Vera Cruz','RS'),(4241,'Porto Xavier','RS'),(4242,'Pouso Novo','RS'),(4243,'Presidente Lucena','RS'),(4244,'Progresso','RS'),(4245,'Protásio Alves','RS'),(4246,'Putinga','RS'),(4247,'Quaraí','RS'),(4248,'Quatro Irmãos','RS'),(4249,'Quevedos','RS'),(4250,'Quinze de Novembro','RS'),(4251,'Redentora','RS'),(4252,'Relvado','RS'),(4253,'Restinga Seca','RS'),(4254,'Rio dos Índios','RS'),(4255,'Rio Grande','RS'),(4256,'Rio Pardo','RS'),(4257,'Riozinho','RS'),(4258,'Roca Sales','RS'),(4259,'Rodeio Bonito','RS'),(4260,'Rolador','RS'),(4261,'Rolante','RS'),(4262,'Ronda Alta','RS'),(4263,'Rondinha','RS'),(4264,'Roque Gonzales','RS'),(4265,'Rosário do Sul','RS'),(4266,'Sagrada Família','RS'),(4267,'Saldanha Marinho','RS'),(4268,'Salto do Jacuí','RS'),(4269,'Salvador das Missões','RS'),(4270,'Salvador do Sul','RS'),(4271,'Sananduva','RS'),(4272,'Santa Bárbara do Sul','RS'),(4273,'Santa Cecília do Sul','RS'),(4274,'Santa Clara do Sul','RS'),(4275,'Santa Cruz do Sul','RS'),(4276,'Santa Margarida do Sul','RS'),(4277,'Santa Maria','RS'),(4278,'Santa Maria do Herval','RS'),(4279,'Santa Rosa','RS'),(4280,'Santa Tereza','RS'),(4281,'Santa Vitória do Palmar','RS'),(4282,'Santana da Boa Vista','RS'),(4283,'Santana do Livramento','RS'),(4284,'Santiago','RS'),(4285,'Santo Ângelo','RS'),(4286,'Santo Antônio da Patrulha','RS'),(4287,'Santo Antônio das Missões','RS'),(4288,'Santo Antônio do Palma','RS'),(4289,'Santo Antônio do Planalto','RS'),(4290,'Santo Augusto','RS'),(4291,'Santo Cristo','RS'),(4292,'Santo Expedito do Sul','RS'),(4293,'São Borja','RS'),(4294,'São Domingos do Sul','RS'),(4295,'São Francisco de Assis','RS'),(4296,'São Francisco de Paula','RS'),(4297,'São Gabriel','RS'),(4298,'São Jerônimo','RS'),(4299,'São João da Urtiga','RS'),(4300,'São João do Polêsine','RS'),(4301,'São Jorge','RS'),(4302,'São José das Missões','RS'),(4303,'São José do Herval','RS'),(4304,'São José do Hortêncio','RS'),(4305,'São José do Inhacorá','RS'),(4306,'São José do Norte','RS'),(4307,'São José do Ouro','RS'),(4308,'São José do Sul','RS'),(4309,'São José dos Ausentes','RS'),(4310,'São Leopoldo','RS'),(4311,'São Lourenço do Sul','RS'),(4312,'São Luiz Gonzaga','RS'),(4313,'São Marcos','RS'),(4314,'São Martinho','RS'),(4315,'São Martinho da Serra','RS'),(4316,'São Miguel das Missões','RS'),(4317,'São Nicolau','RS'),(4318,'São Paulo das Missões','RS'),(4319,'São Pedro da Serra','RS'),(4320,'São Pedro das Missões','RS'),(4321,'São Pedro do Butiá','RS'),(4322,'São Pedro do Sul','RS'),(4323,'São Sebastião do Caí','RS'),(4324,'São Sepé','RS'),(4325,'São Valentim','RS'),(4326,'São Valentim do Sul','RS'),(4327,'São Valério do Sul','RS'),(4328,'São Vendelino','RS'),(4329,'São Vicente do Sul','RS'),(4330,'Sapiranga','RS'),(4331,'Sapucaia do Sul','RS'),(4332,'Sarandi','RS'),(4333,'Seberi','RS'),(4334,'Sede Nova','RS'),(4335,'Segredo','RS'),(4336,'Selbach','RS'),(4337,'Senador Salgado Filho','RS'),(4338,'Sentinela do Sul','RS'),(4339,'Serafina Corrêa','RS'),(4340,'Sério','RS'),(4341,'Sertão','RS'),(4342,'Sertão Santana','RS'),(4343,'Sete de Setembro','RS'),(4344,'Severiano de Almeida','RS'),(4345,'Silveira Martins','RS'),(4346,'Sinimbu','RS'),(4347,'Sobradinho','RS'),(4348,'Soledade','RS'),(4349,'Tabaí','RS'),(4350,'Tapejara','RS'),(4351,'Tapera','RS'),(4352,'Tapes','RS'),(4353,'Taquara','RS'),(4354,'Taquari','RS'),(4355,'Taquaruçu do Sul','RS'),(4356,'Tavares','RS'),(4357,'Tenente Portela','RS'),(4358,'Terra de Areia','RS'),(4359,'Teutônia','RS'),(4360,'Tio Hugo','RS'),(4361,'Tiradentes do Sul','RS'),(4362,'Toropi','RS'),(4363,'Torres','RS'),(4364,'Tramandaí','RS'),(4365,'Travesseiro','RS'),(4366,'Três Arroios','RS'),(4367,'Três Cachoeiras','RS'),(4368,'Três Coroas','RS'),(4369,'Três de Maio','RS'),(4370,'Três Forquilhas','RS'),(4371,'Três Palmeiras','RS'),(4372,'Três Passos','RS'),(4373,'Trindade do Sul','RS'),(4374,'Triunfo','RS'),(4375,'Tucunduva','RS'),(4376,'Tunas','RS'),(4377,'Tupanci do Sul','RS'),(4378,'Tupanciretã','RS'),(4379,'Tupandi','RS'),(4380,'Tuparendi','RS'),(4381,'Turuçu','RS'),(4382,'Ubiretama','RS'),(4383,'União da Serra','RS'),(4384,'Unistalda','RS'),(4385,'Uruguaiana','RS'),(4386,'Vacaria','RS'),(4387,'Vale do Sol','RS'),(4388,'Vale Real','RS'),(4389,'Vale Verde','RS'),(4390,'Vanini','RS'),(4391,'Venâncio Aires','RS'),(4392,'Vera Cruz','RS'),(4393,'Veranópolis','RS'),(4394,'Vespasiano Correa','RS'),(4395,'Viadutos','RS'),(4396,'Viamão','RS'),(4397,'Vicente Dutra','RS'),(4398,'Victor Graeff','RS'),(4399,'Vila Flores','RS'),(4400,'Vila Lângaro','RS'),(4401,'Vila Maria','RS'),(4402,'Vila Nova do Sul','RS'),(4403,'Vista Alegre','RS'),(4404,'Vista Alegre do Prata','RS'),(4405,'Vista Gaúcha','RS'),(4406,'Vitória das Missões','RS'),(4407,'Westfália','RS'),(4408,'Xangri-lá','RS'),(4409,'Abdon Batista','SC'),(4410,'Abelardo Luz','SC'),(4411,'Agrolândia','SC'),(4412,'Agronômica','SC'),(4413,'Água Doce','SC'),(4414,'Águas de Chapecó','SC'),(4415,'Águas Frias','SC'),(4416,'Águas Mornas','SC'),(4417,'Alfredo Wagner','SC'),(4418,'Alto Bela Vista','SC'),(4419,'Anchieta','SC'),(4420,'Angelina','SC'),(4421,'Anita Garibaldi','SC'),(4422,'Anitápolis','SC'),(4423,'Antônio Carlos','SC'),(4424,'Apiúna','SC'),(4425,'Arabutã','SC'),(4426,'Araquari','SC'),(4427,'Araranguá','SC'),(4428,'Armazém','SC'),(4429,'Arroio Trinta','SC'),(4430,'Arvoredo','SC'),(4431,'Ascurra','SC'),(4432,'Atalanta','SC'),(4433,'Aurora','SC'),(4434,'Balneário Arroio do Silva','SC'),(4435,'Balneário Barra do Sul','SC'),(4436,'Balneário Camboriú','SC'),(4437,'Balneário Gaivota','SC'),(4438,'Bandeirante','SC'),(4439,'Barra Bonita','SC'),(4440,'Barra Velha','SC'),(4441,'Bela Vista do Toldo','SC'),(4442,'Belmonte','SC'),(4443,'Benedito Novo','SC'),(4444,'Biguaçu','SC'),(4445,'Blumenau','SC'),(4446,'Bocaina do Sul','SC'),(4447,'Bom Jardim da Serra','SC'),(4448,'Bom Jesus','SC'),(4449,'Bom Jesus do Oeste','SC'),(4450,'Bom Retiro','SC'),(4451,'Bombinhas','SC'),(4452,'Botuverá','SC'),(4453,'Braço do Norte','SC'),(4454,'Braço do Trombudo','SC'),(4455,'Brunópolis','SC'),(4456,'Brusque','SC'),(4457,'Caçador','SC'),(4458,'Caibi','SC'),(4459,'Calmon','SC'),(4460,'Camboriú','SC'),(4461,'Campo Alegre','SC'),(4462,'Campo Belo do Sul','SC'),(4463,'Campo Erê','SC'),(4464,'Campos Novos','SC'),(4465,'Canelinha','SC'),(4466,'Canoinhas','SC'),(4467,'Capão Alto','SC'),(4468,'Capinzal','SC'),(4469,'Capivari de Baixo','SC'),(4470,'Catanduvas','SC'),(4471,'Caxambu do Sul','SC'),(4472,'Celso Ramos','SC'),(4473,'Cerro Negro','SC'),(4474,'Chapadão do Lageado','SC'),(4475,'Chapecó','SC'),(4476,'Cocal do Sul','SC'),(4477,'Concórdia','SC'),(4478,'Cordilheira Alta','SC'),(4479,'Coronel Freitas','SC'),(4480,'Coronel Martins','SC'),(4481,'Correia Pinto','SC'),(4482,'Corupá','SC'),(4483,'Criciúma','SC'),(4484,'Cunha Porã','SC'),(4485,'Cunhataí','SC'),(4486,'Curitibanos','SC'),(4487,'Descanso','SC'),(4488,'Dionísio Cerqueira','SC'),(4489,'Dona Emma','SC'),(4490,'Doutor Pedrinho','SC'),(4491,'Entre Rios','SC'),(4492,'Ermo','SC'),(4493,'Erval Velho','SC'),(4494,'Faxinal dos Guedes','SC'),(4495,'Flor do Sertão','SC'),(4496,'Florianópolis','SC'),(4497,'Formosa do Sul','SC'),(4498,'Forquilhinha','SC'),(4499,'Fraiburgo','SC'),(4500,'Frei Rogério','SC'),(4501,'Galvão','SC'),(4502,'Garopaba','SC'),(4503,'Garuva','SC'),(4504,'Gaspar','SC'),(4505,'Governador Celso Ramos','SC'),(4506,'Grão Pará','SC'),(4507,'Gravatal','SC'),(4508,'Guabiruba','SC'),(4509,'Guaraciaba','SC'),(4510,'Guaramirim','SC'),(4511,'Guarujá do Sul','SC'),(4512,'Guatambú','SC'),(4513,'Herval d\'Oeste','SC'),(4514,'Ibiam','SC'),(4515,'Ibicaré','SC'),(4516,'Ibirama','SC'),(4517,'Içara','SC'),(4518,'Ilhota','SC'),(4519,'Imaruí','SC'),(4520,'Imbituba','SC'),(4521,'Imbuia','SC'),(4522,'Indaial','SC'),(4523,'Iomerê','SC'),(4524,'Ipira','SC'),(4525,'Iporã do Oeste','SC'),(4526,'Ipuaçu','SC'),(4527,'Ipumirim','SC'),(4528,'Iraceminha','SC'),(4529,'Irani','SC'),(4530,'Irati','SC'),(4531,'Irineópolis','SC'),(4532,'Itá','SC'),(4533,'Itaiópolis','SC'),(4534,'Itajaí','SC'),(4535,'Itapema','SC'),(4536,'Itapiranga','SC'),(4537,'Itapoá','SC'),(4538,'Ituporanga','SC'),(4539,'Jaborá','SC'),(4540,'Jacinto Machado','SC'),(4541,'Jaguaruna','SC'),(4542,'Jaraguá do Sul','SC'),(4543,'Jardinópolis','SC'),(4544,'Joaçaba','SC'),(4545,'Joinville','SC'),(4546,'José Boiteux','SC'),(4547,'Jupiá','SC'),(4548,'Lacerdópolis','SC'),(4549,'Lages','SC'),(4550,'Laguna','SC'),(4551,'Lajeado Grande','SC'),(4552,'Laurentino','SC'),(4553,'Lauro Muller','SC'),(4554,'Lebon Régis','SC'),(4555,'Leoberto Leal','SC'),(4556,'Lindóia do Sul','SC'),(4557,'Lontras','SC'),(4558,'Luiz Alves','SC'),(4559,'Luzerna','SC'),(4560,'Macieira','SC'),(4561,'Mafra','SC'),(4562,'Major Gercino','SC'),(4563,'Major Vieira','SC'),(4564,'Maracajá','SC'),(4565,'Maravilha','SC'),(4566,'Marema','SC'),(4567,'Massaranduba','SC'),(4568,'Matos Costa','SC'),(4569,'Meleiro','SC'),(4570,'Mirim Doce','SC'),(4571,'Modelo','SC'),(4572,'Mondaí','SC'),(4573,'Monte Carlo','SC'),(4574,'Monte Castelo','SC'),(4575,'Morro da Fumaça','SC'),(4576,'Morro Grande','SC'),(4577,'Navegantes','SC'),(4578,'Nova Erechim','SC'),(4579,'Nova Itaberaba','SC'),(4580,'Nova Trento','SC'),(4581,'Nova Veneza','SC'),(4582,'Novo Horizonte','SC'),(4583,'Orleans','SC'),(4584,'Otacílio Costa','SC'),(4585,'Ouro','SC'),(4586,'Ouro Verde','SC'),(4587,'Paial','SC'),(4588,'Painel','SC'),(4589,'Palhoça','SC'),(4590,'Palma Sola','SC'),(4591,'Palmeira','SC'),(4592,'Palmitos','SC'),(4593,'Papanduva','SC'),(4594,'Paraíso','SC'),(4595,'Passo de Torres','SC'),(4596,'Passos Maia','SC'),(4597,'Paulo Lopes','SC'),(4598,'Pedras Grandes','SC'),(4599,'Penha','SC'),(4600,'Peritiba','SC'),(4601,'Petrolândia','SC'),(4602,'Piçarras','SC'),(4603,'Pinhalzinho','SC'),(4604,'Pinheiro Preto','SC'),(4605,'Piratuba','SC'),(4606,'Planalto Alegre','SC'),(4607,'Pomerode','SC'),(4608,'Ponte Alta','SC'),(4609,'Ponte Alta do Norte','SC'),(4610,'Ponte Serrada','SC'),(4611,'Porto Belo','SC'),(4612,'Porto União','SC'),(4613,'Pouso Redondo','SC'),(4614,'Praia Grande','SC'),(4615,'Presidente Castelo Branco','SC'),(4616,'Presidente Getúlio','SC'),(4617,'Presidente Nereu','SC'),(4618,'Princesa','SC'),(4619,'Quilombo','SC'),(4620,'Rancho Queimado','SC'),(4621,'Rio das Antas','SC'),(4622,'Rio do Campo','SC'),(4623,'Rio do Oeste','SC'),(4624,'Rio do Sul','SC'),(4625,'Rio dos Cedros','SC'),(4626,'Rio Fortuna','SC'),(4627,'Rio Negrinho','SC'),(4628,'Rio Rufino','SC'),(4629,'Riqueza','SC'),(4630,'Rodeio','SC'),(4631,'Romelândia','SC'),(4632,'Salete','SC'),(4633,'Saltinho','SC'),(4634,'Salto Veloso','SC'),(4635,'Sangão','SC'),(4636,'Santa Cecília','SC'),(4637,'Santa Helena','SC'),(4638,'Santa Rosa de Lima','SC'),(4639,'Santa Rosa do Sul','SC'),(4640,'Santa Terezinha','SC'),(4641,'Santa Terezinha do Progresso','SC'),(4642,'Santiago do Sul','SC'),(4643,'Santo Amaro da Imperatriz','SC'),(4644,'São Bento do Sul','SC'),(4645,'São Bernardino','SC'),(4646,'São Bonifácio','SC'),(4647,'São Carlos','SC'),(4648,'São Cristovão do Sul','SC'),(4649,'São Domingos','SC'),(4650,'São Francisco do Sul','SC'),(4651,'São João Batista','SC'),(4652,'São João do Itaperiú','SC'),(4653,'São João do Oeste','SC'),(4654,'São João do Sul','SC'),(4655,'São Joaquim','SC'),(4656,'São José','SC'),(4657,'São José do Cedro','SC'),(4658,'São José do Cerrito','SC'),(4659,'São Lourenço do Oeste','SC'),(4660,'São Ludgero','SC'),(4661,'São Martinho','SC'),(4662,'São Miguel da Boa Vista','SC'),(4663,'São Miguel do Oeste','SC'),(4664,'São Pedro de Alcântara','SC'),(4665,'Saudades','SC'),(4666,'Schroeder','SC'),(4667,'Seara','SC'),(4668,'Serra Alta','SC'),(4669,'Siderópolis','SC'),(4670,'Sombrio','SC'),(4671,'Sul Brasil','SC'),(4672,'Taió','SC'),(4673,'Tangará','SC'),(4674,'Tigrinhos','SC'),(4675,'Tijucas','SC'),(4676,'Timbé do Sul','SC'),(4677,'Timbó','SC'),(4678,'Timbó Grande','SC'),(4679,'Três Barras','SC'),(4680,'Treviso','SC'),(4681,'Treze de Maio','SC'),(4682,'Treze Tílias','SC'),(4683,'Trombudo Central','SC'),(4684,'Tubarão','SC'),(4685,'Tunápolis','SC'),(4686,'Turvo','SC'),(4687,'União do Oeste','SC'),(4688,'Urubici','SC'),(4689,'Urupema','SC'),(4690,'Urussanga','SC'),(4691,'Vargeão','SC'),(4692,'Vargem','SC'),(4693,'Vargem Bonita','SC'),(4694,'Vidal Ramos','SC'),(4695,'Videira','SC'),(4696,'Vitor Meireles','SC'),(4697,'Witmarsum','SC'),(4698,'Xanxerê','SC'),(4699,'Xavantina','SC'),(4700,'Xaxim','SC'),(4701,'Zortéa','SC'),(4702,'Amparo de São Francisco','SE'),(4703,'Aquidabã','SE'),(4704,'Aracaju','SE'),(4705,'Arauá','SE'),(4706,'Areia Branca','SE'),(4707,'Barra dos Coqueiros','SE'),(4708,'Boquim','SE'),(4709,'Brejo Grande','SE'),(4710,'Campo do Brito','SE'),(4711,'Canhoba','SE'),(4712,'Canindé de São Francisco','SE'),(4713,'Capela','SE'),(4714,'Carira','SE'),(4715,'Carmópolis','SE'),(4716,'Cedro de São João','SE'),(4717,'Cristinápolis','SE'),(4718,'Cumbe','SE'),(4719,'Divina Pastora','SE'),(4720,'Estância','SE'),(4721,'Feira Nova','SE'),(4722,'Frei Paulo','SE'),(4723,'Gararu','SE'),(4724,'General Maynard','SE'),(4725,'Gracho Cardoso','SE'),(4726,'Ilha das Flores','SE'),(4727,'Indiaroba','SE'),(4728,'Itabaiana','SE'),(4729,'Itabaianinha','SE'),(4730,'Itabi','SE'),(4731,'Itaporanga d\'Ajuda','SE'),(4732,'Japaratuba','SE'),(4733,'Japoatã','SE'),(4734,'Lagarto','SE'),(4735,'Laranjeiras','SE'),(4736,'Macambira','SE'),(4737,'Malhada dos Bois','SE'),(4738,'Malhador','SE'),(4739,'Maruim','SE'),(4740,'Moita Bonita','SE'),(4741,'Monte Alegre de Sergipe','SE'),(4742,'Muribeca','SE'),(4743,'Neópolis','SE'),(4744,'Nossa Senhora Aparecida','SE'),(4745,'Nossa Senhora da Glória','SE'),(4746,'Nossa Senhora das Dores','SE'),(4747,'Nossa Senhora de Lourdes','SE'),(4748,'Nossa Senhora do Socorro','SE'),(4749,'Pacatuba','SE'),(4750,'Pedra Mole','SE'),(4751,'Pedrinhas','SE'),(4752,'Pinhão','SE'),(4753,'Pirambu','SE'),(4754,'Poço Redondo','SE'),(4755,'Poço Verde','SE'),(4756,'Porto da Folha','SE'),(4757,'Propriá','SE'),(4758,'Riachão do Dantas','SE'),(4759,'Riachuelo','SE'),(4760,'Ribeirópolis','SE'),(4761,'Rosário do Catete','SE'),(4762,'Salgado','SE'),(4763,'Santa Luzia do Itanhy','SE'),(4764,'Santa Rosa de Lima','SE'),(4765,'Santana do São Francisco','SE'),(4766,'Santo Amaro das Brotas','SE'),(4767,'São Cristóvão','SE'),(4768,'São Domingos','SE'),(4769,'São Francisco','SE'),(4770,'São Miguel do Aleixo','SE'),(4771,'Simão Dias','SE'),(4772,'Siriri','SE'),(4773,'Telha','SE'),(4774,'Tobias Barreto','SE'),(4775,'Tomar do Geru','SE'),(4776,'Umbaúba','SE'),(4777,'Adamantina','SP'),(4778,'Adolfo','SP'),(4779,'Aguaí','SP'),(4780,'Águas da Prata','SP'),(4781,'Águas de Lindóia','SP'),(4782,'Águas de Santa Bárbara','SP'),(4783,'Águas de São Pedro','SP'),(4784,'Agudos','SP'),(4785,'Alambari','SP'),(4786,'Alfredo Marcondes','SP'),(4787,'Altair','SP'),(4788,'Altinópolis','SP'),(4789,'Alto Alegre','SP'),(4790,'Alumínio','SP'),(4791,'Álvares Florence','SP'),(4792,'Álvares Machado','SP'),(4793,'Álvaro de Carvalho','SP'),(4794,'Alvinlândia','SP'),(4795,'Americana','SP'),(4796,'Américo Brasiliense','SP'),(4797,'Américo de Campos','SP'),(4798,'Amparo','SP'),(4799,'Analândia','SP'),(4800,'Andradina','SP'),(4801,'Angatuba','SP'),(4802,'Anhembi','SP'),(4803,'Anhumas','SP'),(4804,'Aparecida','SP'),(4805,'Aparecida d\'Oeste','SP'),(4806,'Apiaí','SP'),(4807,'Araçariguama','SP'),(4808,'Araçatuba','SP'),(4809,'Araçoiaba da Serra','SP'),(4810,'Aramina','SP'),(4811,'Arandu','SP'),(4812,'Arapeí','SP'),(4813,'Araraquara','SP'),(4814,'Araras','SP'),(4815,'Arco-Íris','SP'),(4816,'Arealva','SP'),(4817,'Areias','SP'),(4818,'Areiópolis','SP'),(4819,'Ariranha','SP'),(4820,'Artur Nogueira','SP'),(4821,'Arujá','SP'),(4822,'Aspásia','SP'),(4823,'Assis','SP'),(4824,'Atibaia','SP'),(4825,'Auriflama','SP'),(4826,'Avaí','SP'),(4827,'Avanhandava','SP'),(4828,'Avaré','SP'),(4829,'Bady Bassitt','SP'),(4830,'Balbinos','SP'),(4831,'Bálsamo','SP'),(4832,'Bananal','SP'),(4833,'Barão de Antonina','SP'),(4834,'Barbosa','SP'),(4835,'Bariri','SP'),(4836,'Barra Bonita','SP'),(4837,'Barra do Chapéu','SP'),(4838,'Barra do Turvo','SP'),(4839,'Barretos','SP'),(4840,'Barrinha','SP'),(4841,'Barueri','SP'),(4842,'Bastos','SP'),(4843,'Batatais','SP'),(4844,'Bauru','SP'),(4845,'Bebedouro','SP'),(4846,'Bento de Abreu','SP'),(4847,'Bernardino de Campos','SP'),(4848,'Bertioga','SP'),(4849,'Bilac','SP'),(4850,'Birigui','SP'),(4851,'Biritiba-Mirim','SP'),(4852,'Boa Esperança do Sul','SP'),(4853,'Bocaina','SP'),(4854,'Bofete','SP'),(4855,'Boituva','SP'),(4856,'Bom Jesus dos Perdões','SP'),(4857,'Bom Sucesso de Itararé','SP'),(4858,'Borá','SP'),(4859,'Boracéia','SP'),(4860,'Borborema','SP'),(4861,'Borebi','SP'),(4862,'Botucatu','SP'),(4863,'Bragança Paulista','SP'),(4864,'Braúna','SP'),(4865,'Brejo Alegre','SP'),(4866,'Brodowski','SP'),(4867,'Brotas','SP'),(4868,'Buri','SP'),(4869,'Buritama','SP'),(4870,'Buritizal','SP'),(4871,'Cabrália Paulista','SP'),(4872,'Cabreúva','SP'),(4873,'Caçapava','SP'),(4874,'Cachoeira Paulista','SP'),(4875,'Caconde','SP'),(4876,'Cafelândia','SP'),(4877,'Caiabu','SP'),(4878,'Caieiras','SP'),(4879,'Caiuá','SP'),(4880,'Cajamar','SP'),(4881,'Cajati','SP'),(4882,'Cajobi','SP'),(4883,'Cajuru','SP'),(4884,'Campina do Monte Alegre','SP'),(4885,'Campinas','SP'),(4886,'Campo Limpo Paulista','SP'),(4887,'Campos do Jordão','SP'),(4888,'Campos Novos Paulista','SP'),(4889,'Cananéia','SP'),(4890,'Canas','SP'),(4891,'Cândido Mota','SP'),(4892,'Cândido Rodrigues','SP'),(4893,'Canitar','SP'),(4894,'Capão Bonito','SP'),(4895,'Capela do Alto','SP'),(4896,'Capivari','SP'),(4897,'Caraguatatuba','SP'),(4898,'Carapicuíba','SP'),(4899,'Cardoso','SP'),(4900,'Casa Branca','SP'),(4901,'Cássia dos Coqueiros','SP'),(4902,'Castilho','SP'),(4903,'Catanduva','SP'),(4904,'Catiguá','SP'),(4905,'Cedral','SP'),(4906,'Cerqueira César','SP'),(4907,'Cerquilho','SP'),(4908,'Cesário Lange','SP'),(4909,'Charqueada','SP'),(4910,'Chavantes','SP'),(4911,'Clementina','SP'),(4912,'Colina','SP'),(4913,'Colômbia','SP'),(4914,'Conchal','SP'),(4915,'Conchas','SP'),(4916,'Cordeirópolis','SP'),(4917,'Coroados','SP'),(4918,'Coronel Macedo','SP'),(4919,'Corumbataí','SP'),(4920,'Cosmópolis','SP'),(4921,'Cosmorama','SP'),(4922,'Cotia','SP'),(4923,'Cravinhos','SP'),(4924,'Cristais Paulista','SP'),(4925,'Cruzália','SP'),(4926,'Cruzeiro','SP'),(4927,'Cubatão','SP'),(4928,'Cunha','SP'),(4929,'Descalvado','SP'),(4930,'Diadema','SP'),(4931,'Dirce Reis','SP'),(4932,'Divinolândia','SP'),(4933,'Dobrada','SP'),(4934,'Dois Córregos','SP'),(4935,'Dolcinópolis','SP'),(4936,'Dourado','SP'),(4937,'Dracena','SP'),(4938,'Duartina','SP'),(4939,'Dumont','SP'),(4940,'Echaporã','SP'),(4941,'Eldorado','SP'),(4942,'Elias Fausto','SP'),(4943,'Elisiário','SP'),(4944,'Embaúba','SP'),(4945,'Embu','SP'),(4946,'Embu-Guaçu','SP'),(4947,'Emilianópolis','SP'),(4948,'Engenheiro Coelho','SP'),(4949,'Espírito Santo do Pinhal','SP'),(4950,'Espírito Santo do Turvo','SP'),(4951,'Estiva Gerbi','SP'),(4952,'Estrela d\'Oeste','SP'),(4953,'Estrela do Norte','SP'),(4954,'Euclides da Cunha Paulista','SP'),(4955,'Fartura','SP'),(4956,'Fernando Prestes','SP'),(4957,'Fernandópolis','SP'),(4958,'Fernão','SP'),(4959,'Ferraz de Vasconcelos','SP'),(4960,'Flora Rica','SP'),(4961,'Floreal','SP'),(4962,'Florínia','SP'),(4963,'Flórida Paulista','SP'),(4964,'Franca','SP'),(4965,'Francisco Morato','SP'),(4966,'Franco da Rocha','SP'),(4967,'Gabriel Monteiro','SP'),(4968,'Gália','SP'),(4969,'Garça','SP'),(4970,'Gastão Vidigal','SP'),(4971,'Gavião Peixoto','SP'),(4972,'General Salgado','SP'),(4973,'Getulina','SP'),(4974,'Glicério','SP'),(4975,'Guaiçara','SP'),(4976,'Guaimbê','SP'),(4977,'Guaíra','SP'),(4978,'Guapiaçu','SP'),(4979,'Guapiara','SP'),(4980,'Guará','SP'),(4981,'Guaraçaí','SP'),(4982,'Guaraci','SP'),(4983,'Guarani d\'Oeste','SP'),(4984,'Guarantã','SP'),(4985,'Guararapes','SP'),(4986,'Guararema','SP'),(4987,'Guaratinguetá','SP'),(4988,'Guareí','SP'),(4989,'Guariba','SP'),(4990,'Guarujá','SP'),(4991,'Guarulhos','SP'),(4992,'Guatapará','SP'),(4993,'Guzolândia','SP'),(4994,'Herculândia','SP'),(4995,'Holambra','SP'),(4996,'Hortolândia','SP'),(4997,'Iacanga','SP'),(4998,'Iacri','SP'),(4999,'Iaras','SP'),(5000,'Ibaté','SP'),(5001,'Ibirá','SP'),(5002,'Ibirarema','SP'),(5003,'Ibitinga','SP'),(5004,'Ibiúna','SP'),(5005,'Icém','SP'),(5006,'Iepê','SP'),(5007,'Igaraçu do Tietê','SP'),(5008,'Igarapava','SP'),(5009,'Igaratá','SP'),(5010,'Iguape','SP'),(5011,'Ilha Comprida','SP'),(5012,'Ilha Solteira','SP'),(5013,'Ilhabela','SP'),(5014,'Indaiatuba','SP'),(5015,'Indiana','SP'),(5016,'Indiaporã','SP'),(5017,'Inúbia Paulista','SP'),(5018,'Ipauçu','SP'),(5019,'Iperó','SP'),(5020,'Ipeúna','SP'),(5021,'Ipiguá','SP'),(5022,'Iporanga','SP'),(5023,'Ipuã','SP'),(5024,'Iracemápolis','SP'),(5025,'Irapuã','SP'),(5026,'Irapuru','SP'),(5027,'Itaberá','SP'),(5028,'Itaí','SP'),(5029,'Itajobi','SP'),(5030,'Itaju','SP'),(5031,'Itanhaém','SP'),(5032,'Itaóca','SP'),(5033,'Itapecerica da Serra','SP'),(5034,'Itapetininga','SP'),(5035,'Itapeva','SP'),(5036,'Itapevi','SP'),(5037,'Itapira','SP'),(5038,'Itapirapuã Paulista','SP'),(5039,'Itápolis','SP'),(5040,'Itaporanga','SP'),(5041,'Itapuí','SP'),(5042,'Itapura','SP'),(5043,'Itaquaquecetuba','SP'),(5044,'Itararé','SP'),(5045,'Itariri','SP'),(5046,'Itatiba','SP'),(5047,'Itatinga','SP'),(5048,'Itirapina','SP'),(5049,'Itirapuã','SP'),(5050,'Itobi','SP'),(5051,'Itu','SP'),(5052,'Itupeva','SP'),(5053,'Ituverava','SP'),(5054,'Jaborandi','SP'),(5055,'Jaboticabal','SP'),(5056,'Jacareí','SP'),(5057,'Jaci','SP'),(5058,'Jacupiranga','SP'),(5059,'Jaguariúna','SP'),(5060,'Jales','SP'),(5061,'Jambeiro','SP'),(5062,'Jandira','SP'),(5063,'Jardinópolis','SP'),(5064,'Jarinu','SP'),(5065,'Jaú','SP'),(5066,'Jeriquara','SP'),(5067,'Joanópolis','SP'),(5068,'João Ramalho','SP'),(5069,'José Bonifácio','SP'),(5070,'Júlio Mesquita','SP'),(5071,'Jumirim','SP'),(5072,'Jundiaí','SP'),(5073,'Junqueirópolis','SP'),(5074,'Juquiá','SP'),(5075,'Juquitiba','SP'),(5076,'Lagoinha','SP'),(5077,'Laranjal Paulista','SP'),(5078,'Lavínia','SP'),(5079,'Lavrinhas','SP'),(5080,'Leme','SP'),(5081,'Lençóis Paulista','SP'),(5082,'Limeira','SP'),(5083,'Lindóia','SP'),(5084,'Lins','SP'),(5085,'Lorena','SP'),(5086,'Lourdes','SP'),(5087,'Louveira','SP'),(5088,'Lucélia','SP'),(5089,'Lucianópolis','SP'),(5090,'Luís Antônio','SP'),(5091,'Luiziânia','SP'),(5092,'Lupércio','SP'),(5093,'Lutécia','SP'),(5094,'Macatuba','SP'),(5095,'Macaubal','SP'),(5096,'Macedônia','SP'),(5097,'Magda','SP'),(5098,'Mairinque','SP'),(5099,'Mairiporã','SP'),(5100,'Manduri','SP'),(5101,'Marabá Paulista','SP'),(5102,'Maracaí','SP'),(5103,'Marapoama','SP'),(5104,'Mariápolis','SP'),(5105,'Marília','SP'),(5106,'Marinópolis','SP'),(5107,'Martinópolis','SP'),(5108,'Matão','SP'),(5109,'Mauá','SP'),(5110,'Mendonça','SP'),(5111,'Meridiano','SP'),(5112,'Mesópolis','SP'),(5113,'Miguelópolis','SP'),(5114,'Mineiros do Tietê','SP'),(5115,'Mira Estrela','SP'),(5116,'Miracatu','SP'),(5117,'Mirandópolis','SP'),(5118,'Mirante do Paranapanema','SP'),(5119,'Mirassol','SP'),(5120,'Mirassolândia','SP'),(5121,'Mococa','SP'),(5122,'Mogi das Cruzes','SP'),(5123,'Mogi-Guaçu','SP'),(5124,'Mogi-Mirim','SP'),(5125,'Mombuca','SP'),(5126,'Monções','SP'),(5127,'Mongaguá','SP'),(5128,'Monte Alegre do Sul','SP'),(5129,'Monte Alto','SP'),(5130,'Monte Aprazível','SP'),(5131,'Monte Azul Paulista','SP'),(5132,'Monte Castelo','SP'),(5133,'Monte Mor','SP'),(5134,'Monteiro Lobato','SP'),(5135,'Morro Agudo','SP'),(5136,'Morungaba','SP'),(5137,'Motuca','SP'),(5138,'Murutinga do Sul','SP'),(5139,'Nantes','SP'),(5140,'Narandiba','SP'),(5141,'Natividade da Serra','SP'),(5142,'Nazaré Paulista','SP'),(5143,'Neves Paulista','SP'),(5144,'Nhandeara','SP'),(5145,'Nipoã','SP'),(5146,'Nova Aliança','SP'),(5147,'Nova Campina','SP'),(5148,'Nova Canaã Paulista','SP'),(5149,'Nova Castilho','SP'),(5150,'Nova Europa','SP'),(5151,'Nova Granada','SP'),(5152,'Nova Guataporanga','SP'),(5153,'Nova Independência','SP'),(5154,'Nova Luzitânia','SP'),(5155,'Nova Odessa','SP'),(5156,'Novais','SP'),(5157,'Novo Horizonte','SP'),(5158,'Nuporanga','SP'),(5159,'Ocauçu','SP'),(5160,'Óleo','SP'),(5161,'Olímpia','SP'),(5162,'Onda Verde','SP'),(5163,'Oriente','SP'),(5164,'Orindiúva','SP'),(5165,'Orlândia','SP'),(5166,'Osasco','SP'),(5167,'Oscar Bressane','SP'),(5168,'Osvaldo Cruz','SP'),(5169,'Ourinhos','SP'),(5170,'Ouro Verde','SP'),(5171,'Ouroeste','SP'),(5172,'Pacaembu','SP'),(5173,'Palestina','SP'),(5174,'Palmares Paulista','SP'),(5175,'Palmeira d\'Oeste','SP'),(5176,'Palmital','SP'),(5177,'Panorama','SP'),(5178,'Paraguaçu Paulista','SP'),(5179,'Paraibuna','SP'),(5180,'Paraíso','SP'),(5181,'Paranapanema','SP'),(5182,'Paranapuã','SP'),(5183,'Parapuã','SP'),(5184,'Pardinho','SP'),(5185,'Pariquera-Açu','SP'),(5186,'Parisi','SP'),(5187,'Patrocínio Paulista','SP'),(5188,'Paulicéia','SP'),(5189,'Paulínia','SP'),(5190,'Paulistânia','SP'),(5191,'Paulo de Faria','SP'),(5192,'Pederneiras','SP'),(5193,'Pedra Bela','SP'),(5194,'Pedranópolis','SP'),(5195,'Pedregulho','SP'),(5196,'Pedreira','SP'),(5197,'Pedrinhas Paulista','SP'),(5198,'Pedro de Toledo','SP'),(5199,'Penápolis','SP'),(5200,'Pereira Barreto','SP'),(5201,'Pereiras','SP'),(5202,'Peruíbe','SP'),(5203,'Piacatu','SP'),(5204,'Piedade','SP'),(5205,'Pilar do Sul','SP'),(5206,'Pindamonhangaba','SP'),(5207,'Pindorama','SP'),(5208,'Pinhalzinho','SP'),(5209,'Piquerobi','SP'),(5210,'Piquete','SP'),(5211,'Piracaia','SP'),(5212,'Piracicaba','SP'),(5213,'Piraju','SP'),(5214,'Pirajuí','SP'),(5215,'Pirangi','SP'),(5216,'Pirapora do Bom Jesus','SP'),(5217,'Pirapozinho','SP'),(5218,'Pirassununga','SP'),(5219,'Piratininga','SP'),(5220,'Pitangueiras','SP'),(5221,'Planalto','SP'),(5222,'Platina','SP'),(5223,'Poá','SP'),(5224,'Poloni','SP'),(5225,'Pompéia','SP'),(5226,'Pongaí','SP'),(5227,'Pontal','SP'),(5228,'Pontalinda','SP'),(5229,'Pontes Gestal','SP'),(5230,'Populina','SP'),(5231,'Porangaba','SP'),(5232,'Porto Feliz','SP'),(5233,'Porto Ferreira','SP'),(5234,'Potim','SP'),(5235,'Potirendaba','SP'),(5236,'Pracinha','SP'),(5237,'Pradópolis','SP'),(5238,'Praia Grande','SP'),(5239,'Pratânia','SP'),(5240,'Presidente Alves','SP'),(5241,'Presidente Bernardes','SP'),(5242,'Presidente Epitácio','SP'),(5243,'Presidente Prudente','SP'),(5244,'Presidente Venceslau','SP'),(5245,'Promissão','SP'),(5246,'Quadra','SP'),(5247,'Quatá','SP'),(5248,'Queiroz','SP'),(5249,'Queluz','SP'),(5250,'Quintana','SP'),(5251,'Rafard','SP'),(5252,'Rancharia','SP'),(5253,'Redenção da Serra','SP'),(5254,'Regente Feijó','SP'),(5255,'Reginópolis','SP'),(5256,'Registro','SP'),(5257,'Restinga','SP'),(5258,'Ribeira','SP'),(5259,'Ribeirão Bonito','SP'),(5260,'Ribeirão Branco','SP'),(5261,'Ribeirão Corrente','SP'),(5262,'Ribeirão do Sul','SP'),(5263,'Ribeirão dos Índios','SP'),(5264,'Ribeirão Grande','SP'),(5265,'Ribeirão Pires','SP'),(5266,'Ribeirão Preto','SP'),(5267,'Rifaina','SP'),(5268,'Rincão','SP'),(5269,'Rinópolis','SP'),(5270,'Rio Claro','SP'),(5271,'Rio das Pedras','SP'),(5272,'Rio Grande da Serra','SP'),(5273,'Riolândia','SP'),(5274,'Riversul','SP'),(5275,'Rosana','SP'),(5276,'Roseira','SP'),(5277,'Rubiácea','SP'),(5278,'Rubinéia','SP'),(5279,'Sabino','SP'),(5280,'Sagres','SP'),(5281,'Sales','SP'),(5282,'Sales Oliveira','SP'),(5283,'Salesópolis','SP'),(5284,'Salmourão','SP'),(5285,'Saltinho','SP'),(5286,'Salto','SP'),(5287,'Salto de Pirapora','SP'),(5288,'Salto Grande','SP'),(5289,'Sandovalina','SP'),(5290,'Santa Adélia','SP'),(5291,'Santa Albertina','SP'),(5292,'Santa Bárbara d\'Oeste','SP'),(5293,'Santa Branca','SP'),(5294,'Santa Clara d\'Oeste','SP'),(5295,'Santa Cruz da Conceição','SP'),(5296,'Santa Cruz da Esperança','SP'),(5297,'Santa Cruz das Palmeiras','SP'),(5298,'Santa Cruz do Rio Pardo','SP'),(5299,'Santa Ernestina','SP'),(5300,'Santa Fé do Sul','SP'),(5301,'Santa Gertrudes','SP'),(5302,'Santa Isabel','SP'),(5303,'Santa Lúcia','SP'),(5304,'Santa Maria da Serra','SP'),(5305,'Santa Mercedes','SP'),(5306,'Santa Rita d\'Oeste','SP'),(5307,'Santa Rita do Passa Quatro','SP'),(5308,'Santa Rosa de Viterbo','SP'),(5309,'Santa Salete','SP'),(5310,'Santana da Ponte Pensa','SP'),(5311,'Santana de Parnaíba','SP'),(5312,'Santo Anastácio','SP'),(5313,'Santo André','SP'),(5314,'Santo Antônio da Alegria','SP'),(5315,'Santo Antônio de Posse','SP'),(5316,'Santo Antônio do Aracanguá','SP'),(5317,'Santo Antônio do Jardim','SP'),(5318,'Santo Antônio do Pinhal','SP'),(5319,'Santo Expedito','SP'),(5320,'Santópolis do Aguapeí','SP'),(5321,'Santos','SP'),(5322,'São Bento do Sapucaí','SP'),(5323,'São Bernardo do Campo','SP'),(5324,'São Caetano do Sul','SP'),(5325,'São Carlos','SP'),(5326,'São Francisco','SP'),(5327,'São João da Boa Vista','SP'),(5328,'São João das Duas Pontes','SP'),(5329,'São João de Iracema','SP'),(5330,'São João do Pau d\'Alho','SP'),(5331,'São Joaquim da Barra','SP'),(5332,'São José da Bela Vista','SP'),(5333,'São José do Barreiro','SP'),(5334,'São José do Rio Pardo','SP'),(5335,'São José do Rio Preto','SP'),(5336,'São José dos Campos','SP'),(5337,'São Lourenço da Serra','SP'),(5338,'São Luís do Paraitinga','SP'),(5339,'São Manuel','SP'),(5340,'São Miguel Arcanjo','SP'),(5341,'São Paulo','SP'),(5342,'São Pedro','SP'),(5343,'São Pedro do Turvo','SP'),(5344,'São Roque','SP'),(5345,'São Sebastião','SP'),(5346,'São Sebastião da Grama','SP'),(5347,'São Simão','SP'),(5348,'São Vicente','SP'),(5349,'Sarapuí','SP'),(5350,'Sarutaiá','SP'),(5351,'Sebastianópolis do Sul','SP'),(5352,'Serra Azul','SP'),(5353,'Serra Negra','SP'),(5354,'Serrana','SP'),(5355,'Sertãozinho','SP'),(5356,'Sete Barras','SP'),(5357,'Severínia','SP'),(5358,'Silveiras','SP'),(5359,'Socorro','SP'),(5360,'Sorocaba','SP'),(5361,'Sud Mennucci','SP'),(5362,'Sumaré','SP'),(5363,'Suzanápolis','SP'),(5364,'Suzano','SP'),(5365,'Tabapuã','SP'),(5366,'Tabatinga','SP'),(5367,'Taboão da Serra','SP'),(5368,'Taciba','SP'),(5369,'Taguaí','SP'),(5370,'Taiaçu','SP'),(5371,'Taiúva','SP'),(5372,'Tambaú','SP'),(5373,'Tanabi','SP'),(5374,'Tapiraí','SP'),(5375,'Tapiratiba','SP'),(5376,'Taquaral','SP'),(5377,'Taquaritinga','SP'),(5378,'Taquarituba','SP'),(5379,'Taquarivaí','SP'),(5380,'Tarabai','SP'),(5381,'Tarumã','SP'),(5382,'Tatuí','SP'),(5383,'Taubaté','SP'),(5384,'Tejupá','SP'),(5385,'Teodoro Sampaio','SP'),(5386,'Terra Roxa','SP'),(5387,'Tietê','SP'),(5388,'Timburi','SP'),(5389,'Torre de Pedra','SP'),(5390,'Torrinha','SP'),(5391,'Trabiju','SP'),(5392,'Tremembé','SP'),(5393,'Três Fronteiras','SP'),(5394,'Tuiuti','SP'),(5395,'Tupã','SP'),(5396,'Tupi Paulista','SP'),(5397,'Turiúba','SP'),(5398,'Turmalina','SP'),(5399,'Ubarana','SP'),(5400,'Ubatuba','SP'),(5401,'Ubirajara','SP'),(5402,'Uchoa','SP'),(5403,'União Paulista','SP'),(5404,'Urânia','SP'),(5405,'Uru','SP'),(5406,'Urupês','SP'),(5407,'Valentim Gentil','SP'),(5408,'Valinhos','SP'),(5409,'Valparaíso','SP'),(5410,'Vargem','SP'),(5411,'Vargem Grande do Sul','SP'),(5412,'Vargem Grande Paulista','SP'),(5413,'Várzea Paulista','SP'),(5414,'Vera Cruz','SP'),(5415,'Vinhedo','SP'),(5416,'Viradouro','SP'),(5417,'Vista Alegre do Alto','SP'),(5418,'Vitória Brasil','SP'),(5419,'Votorantim','SP'),(5420,'Votuporanga','SP'),(5421,'Zacarias','SP'),(5422,'Abreulândia','TO'),(5423,'Aguiarnópolis','TO'),(5424,'Aliança do Tocantins','TO'),(5425,'Almas','TO'),(5426,'Alvorada','TO'),(5427,'Ananás','TO'),(5428,'Angico','TO'),(5429,'Aparecida do Rio Negro','TO'),(5430,'Aragominas','TO'),(5431,'Araguacema','TO'),(5432,'Araguaçu','TO'),(5433,'Araguaína','TO'),(5434,'Araguanã','TO'),(5435,'Araguatins','TO'),(5436,'Arapoema','TO'),(5437,'Arraias','TO'),(5438,'Augustinópolis','TO'),(5439,'Aurora do Tocantins','TO'),(5440,'Axixá do Tocantins','TO'),(5441,'Babaçulândia','TO'),(5442,'Bandeirantes do Tocantins','TO'),(5443,'Barra do Ouro','TO'),(5444,'Barrolândia','TO'),(5445,'Bernardo Sayão','TO'),(5446,'Bom Jesus do Tocantins','TO'),(5447,'Brasilândia do Tocantins','TO'),(5448,'Brejinho de Nazaré','TO'),(5449,'Buriti do Tocantins','TO'),(5450,'Cachoeirinha','TO'),(5451,'Campos Lindos','TO'),(5452,'Cariri do Tocantins','TO'),(5453,'Carmolândia','TO'),(5454,'Carrasco Bonito','TO'),(5455,'Caseara','TO'),(5456,'Centenário','TO'),(5457,'Chapada da Natividade','TO'),(5458,'Chapada de Areia','TO'),(5459,'Colinas do Tocantins','TO'),(5460,'Colméia','TO'),(5461,'Combinado','TO'),(5462,'Conceição do Tocantins','TO'),(5463,'Couto de Magalhães','TO'),(5464,'Cristalândia','TO'),(5465,'Crixás do Tocantins','TO'),(5466,'Darcinópolis','TO'),(5467,'Dianópolis','TO'),(5468,'Divinópolis do Tocantins','TO'),(5469,'Dois Irmãos do Tocantins','TO'),(5470,'Dueré','TO'),(5471,'Esperantina','TO'),(5472,'Fátima','TO'),(5473,'Figueirópolis','TO'),(5474,'Filadélfia','TO'),(5475,'Formoso do Araguaia','TO'),(5476,'Fortaleza do Tabocão','TO'),(5477,'Goianorte','TO'),(5478,'Goiatins','TO'),(5479,'Guaraí','TO'),(5480,'Gurupi','TO'),(5481,'Ipueiras','TO'),(5482,'Itacajá','TO'),(5483,'Itaguatins','TO'),(5484,'Itapiratins','TO'),(5485,'Itaporã do Tocantins','TO'),(5486,'Jaú do Tocantins','TO'),(5487,'Juarina','TO'),(5488,'Lagoa da Confusão','TO'),(5489,'Lagoa do Tocantins','TO'),(5490,'Lajeado','TO'),(5491,'Lavandeira','TO'),(5492,'Lizarda','TO'),(5493,'Luzinópolis','TO'),(5494,'Marianópolis do Tocantins','TO'),(5495,'Mateiros','TO'),(5496,'Maurilândia do Tocantins','TO'),(5497,'Miracema do Tocantins','TO'),(5498,'Miranorte','TO'),(5499,'Monte do Carmo','TO'),(5500,'Monte Santo do Tocantins','TO'),(5501,'Muricilândia','TO'),(5502,'Natividade','TO'),(5503,'Nazaré','TO'),(5504,'Nova Olinda','TO'),(5505,'Nova Rosalândia','TO'),(5506,'Novo Acordo','TO'),(5507,'Novo Alegre','TO'),(5508,'Novo Jardim','TO'),(5509,'Oliveira de Fátima','TO'),(5510,'Palmas','TO'),(5511,'Palmeirante','TO'),(5512,'Palmeiras do Tocantins','TO'),(5513,'Palmeirópolis','TO'),(5514,'Paraíso do Tocantins','TO'),(5515,'Paranã','TO'),(5516,'Pau d\'Arco','TO'),(5517,'Pedro Afonso','TO'),(5518,'Peixe','TO'),(5519,'Pequizeiro','TO'),(5520,'Pindorama do Tocantins','TO'),(5521,'Piraquê','TO'),(5522,'Pium','TO'),(5523,'Ponte Alta do Bom Jesus','TO'),(5524,'Ponte Alta do Tocantins','TO'),(5525,'Porto Alegre do Tocantins','TO'),(5526,'Porto Nacional','TO'),(5527,'Praia Norte','TO'),(5528,'Presidente Kennedy','TO'),(5529,'Pugmil','TO'),(5530,'Recursolândia','TO'),(5531,'Riachinho','TO'),(5532,'Rio da Conceição','TO'),(5533,'Rio dos Bois','TO'),(5534,'Rio Sono','TO'),(5535,'Sampaio','TO'),(5536,'Sandolândia','TO'),(5537,'Santa Fé do Araguaia','TO'),(5538,'Santa Maria do Tocantins','TO'),(5539,'Santa Rita do Tocantins','TO'),(5540,'Santa Rosa do Tocantins','TO'),(5541,'Santa Tereza do Tocantins','TO'),(5542,'Santa Terezinha Tocantins','TO'),(5543,'São Bento do Tocantins','TO'),(5544,'São Félix do Tocantins','TO'),(5545,'São Miguel do Tocantins','TO'),(5546,'São Salvador do Tocantins','TO'),(5547,'São Sebastião do Tocantins','TO'),(5548,'São Valério da Natividade','TO'),(5549,'Silvanópolis','TO'),(5550,'Sítio Novo do Tocantins','TO'),(5551,'Sucupira','TO'),(5552,'Taguatinga','TO'),(5553,'Taipas do Tocantins','TO'),(5554,'Talismã','TO'),(5555,'Tocantínia','TO'),(5556,'Tocantinópolis','TO'),(5557,'Tupirama','TO'),(5558,'Tupiratins','TO'),(5559,'Wanderlândia','TO'),(5560,'Xambioá','TO');
/*!40000 ALTER TABLE `contact_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_subject`
--

DROP TABLE IF EXISTS `contact_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(250) NOT NULL,
  `nome_en` varchar(250) NOT NULL,
  `nome_es` varchar(250) NOT NULL,
  `nome_fr` varchar(250) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_subject`
--

LOCK TABLES `contact_subject` WRITE;
/*!40000 ALTER TABLE `contact_subject` DISABLE KEYS */;
INSERT INTO `contact_subject` VALUES (1,'Dúvida','Doubt','Duda','Doute',1),(2,'Elogio','Compliment','Alabanza','Louange',1),(3,'Outros assuntos','Other issues','Otras cuestiones','Autres questions',1),(4,'Reclamação','Complaint','Queja','Plainte',1),(5,'Sugestão','Suggestion','Sugestión','Suggestion',1);
/*!40000 ALTER TABLE `contact_subject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contemporary_realization`
--

DROP TABLE IF EXISTS `contemporary_realization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contemporary_realization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `nome_oficial` varchar(300) NOT NULL,
  `nome_popular` text NOT NULL,
  `nome_organizacao_mantedora` varchar(300) NOT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `ponto_referencia` text NOT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `acesso` text,
  `via_terrestre` text NOT NULL,
  `acesso_mais_utilizado` text,
  `transporte` text NOT NULL,
  `protecao` text,
  `conservacao` text NOT NULL,
  `entrada_atrativo` text NOT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `visita_tipo` varchar(30) NOT NULL,
  `visita_duracao_media` varchar(5) NOT NULL,
  `visita_guia` varchar(12) DEFAULT NULL,
  `visita_idiomas` text,
  `visita_outros_idiomas` text,
  `entrada_tipo` varchar(10) NOT NULL,
  `entrada_valor` varchar(20) DEFAULT NULL,
  `formas_pagamento` text,
  `necessaria_autorizacao_previa` tinyint(1) NOT NULL DEFAULT '0',
  `autorizacao_previa_tipo` text,
  `limite_numero_visitantes` tinyint(1) NOT NULL DEFAULT '0',
  `limite_numero_visitantes_quantidade` varchar(20) DEFAULT NULL,
  `servicos_equipamentos` text NOT NULL,
  `atividades_realizadas` text,
  `visitantes_epoca_maior_fluxo` varchar(27) DEFAULT NULL,
  `visitantes_epoca_menor_fluxo` varchar(27) DEFAULT NULL,
  `visitantes_origem` varchar(200) DEFAULT NULL,
  `visitantes_origem_internacionais` text,
  `visitantes_numero_anual` varchar(20) DEFAULT NULL,
  `descricao_atrativo` text NOT NULL,
  `observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_contemporary_realization_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city1000000001000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district1000000001000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type100001000` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contemporary_realization_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contemporary_realization`
--

LOCK TABLES `contemporary_realization` WRITE;
/*!40000 ALTER TABLE `contemporary_realization` DISABLE KEYS */;
/*!40000 ALTER TABLE `contemporary_realization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'Alemanha'),(2,'Argentina'),(3,'Bolívia'),(4,'Chile'),(5,'China'),(6,'Espanha'),(7,'Estados Unidos'),(8,'França'),(9,'Itália'),(10,'Japão'),(11,'México'),(12,'Peru'),(13,'Portugal'),(14,'Austrália'),(15,'Nova Zelândia');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuisine_nationality`
--

DROP TABLE IF EXISTS `cuisine_nationality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuisine_nationality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(200) NOT NULL,
  `nome_en` varchar(250) DEFAULT NULL,
  `nome_es` varchar(250) DEFAULT NULL,
  `nome_fr` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuisine_nationality`
--

LOCK TABLES `cuisine_nationality` WRITE;
/*!40000 ALTER TABLE `cuisine_nationality` DISABLE KEYS */;
INSERT INTO `cuisine_nationality` VALUES (1,'Alemã','Germany','Alemán','Allemand'),(2,'Brasileira','Brazilian','Brasileño','Brésilien'),(3,'Francesa','French','La francesa','Française'),(4,'Italiana','Italian','Italiano','Italien'),(5,'Japonesa','Japanese','Japonés','Japonais'),(6,'Árabe','Arabic','Arabe','Arabe'),(7,'Asiática','Asian','Asiático','Asiatique'),(8,'Americana','American','Americano','Américain'),(9,'Boliviana','Bolivian','Boliviano','Bolivien'),(10,'Chinesa','Chinese','Chino','Chinois'),(11,'Mexicana','Mexican','Mexicano','Mexicain'),(12,'Peruana','Peruvian','Peruano','Péruvien'),(13,'Portuguesa','Portuguese','Portugués','Portugais'),(14,'Tailandesa','Thai','Tailandés','Thaï');
/*!40000 ALTER TABLE `cuisine_nationality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuisine_region`
--

DROP TABLE IF EXISTS `cuisine_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuisine_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(200) NOT NULL,
  `nome_en` varchar(250) DEFAULT NULL,
  `nome_es` varchar(250) DEFAULT NULL,
  `nome_fr` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuisine_region`
--

LOCK TABLES `cuisine_region` WRITE;
/*!40000 ALTER TABLE `cuisine_region` DISABLE KEYS */;
INSERT INTO `cuisine_region` VALUES (1,'Mineira','Mineira','Mineira','Mineira'),(2,'Campeira gaúcha','Gaucho champion','Gaucho campeira','Campeira gaucho'),(3,'Sertaneja','Country','País','Pays'),(4,'Baiana','Baiana','Bahía','Bahia'),(5,'Paulista','Paulista','Paulista','Paulista'),(6,'Amazônica','Amazon','Amazonas','Amazone'),(7,'Pantaneira','Pantaneira','Pantaneira','Pantaneira'),(8,'Paraense','Paraense','Paraense','Paraense'),(9,'Carioca','From Rio','Carioca','Carioca'),(10,'Litorânea','Coastal','Litorânea','Litorânea'),(11,'Caipira','Caipira','Rústico','Rustique'),(12,'Goiana','Goiana','Goiana','Goiana');
/*!40000 ALTER TABLE `cuisine_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuisine_service`
--

DROP TABLE IF EXISTS `cuisine_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuisine_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(200) NOT NULL,
  `nome_en` varchar(250) DEFAULT NULL,
  `nome_es` varchar(250) DEFAULT NULL,
  `nome_fr` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuisine_service`
--

LOCK TABLES `cuisine_service` WRITE;
/*!40000 ALTER TABLE `cuisine_service` DISABLE KEYS */;
INSERT INTO `cuisine_service` VALUES (1,'A table d\'hôte','A table d\'hôte','El menú del día','A table d\'hôte'),(2,'A la carte','A la carte','A la carta','A la carte'),(3,'Buffet','Buffet','Bufé','Buffet'),(4,'Por quilo','Per kilo','Por kilo','Par kilo'),(5,'Rápida - Fast food','Fast Food','Rápido','Rapide'),(6,'Drive thru','Drive Thru','Conduzca a través de','Drive thru'),(7,'Delivery/Entrega','Delivery','Entrega','Livraison');
/*!40000 ALTER TABLE `cuisine_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cuisine_theme`
--

DROP TABLE IF EXISTS `cuisine_theme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cuisine_theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(200) NOT NULL,
  `nome_en` varchar(250) DEFAULT NULL,
  `nome_es` varchar(250) DEFAULT NULL,
  `nome_fr` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cuisine_theme`
--

LOCK TABLES `cuisine_theme` WRITE;
/*!40000 ALTER TABLE `cuisine_theme` DISABLE KEYS */;
INSERT INTO `cuisine_theme` VALUES (1,'Cachaçaria','Cachaçaria','Cachaçaria','Cachaçaria'),(2,'Cervejaria','Brewery','Cervecería','Brasserie'),(3,'Cafeteria','Cafeteria','Cafetería','Cafétéria'),(4,'Creperia','Creperie','Crepería','Creperie'),(5,'Confeitaria','Confectionery','Confitería','Confiserie'),(6,'Coffee shop','Coffee shop','Cafetería','Coffee shop'),(7,'Café colonial','Colonial coffe','Cafetería colonial','Café colonial'),(8,'Churrascaria','Steakhouse','Casa del filete','Steak house'),(9,'Grill','Grill','Parrilla','Gril'),(10,'Galeteria','Galeteria','Galeteria','Galeteria'),(11,'Macrobiótica','Macrobiotic','Macrobiótico','Macrobiotique'),(12,'Vegetariana','Vegetarian','Vegetariano','Végétalien'),(13,'Sanduicheria','Sanduicheria','Tienda que venda sándwiches','Bistrot'),(14,'Snack bar','Snack bar','Bar','Snack-bar'),(15,'Scoth bar','Scoth bar','Barra de scoth','Bar Scoth'),(16,'Pizzaria','Pizzeria','Pizzería','Pizzeria'),(17,'Tapiocaria','Tapioca','Tapiocaria','Tapiocaria'),(18,'Peixe e Frutos do Mar','Fish and Seafood','Pescados y mariscos','Poissons et fruits de mer'),(19,'Vegana','Vegan','Vegetariano','Végétalien'),(20,'Temakeria','Temakeria','Temakeria','Temakeria');
/*!40000 ALTER TABLE `cuisine_theme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cultural_attraction`
--

DROP TABLE IF EXISTS `cultural_attraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cultural_attraction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `nome_oficial` varchar(300) NOT NULL,
  `nome_popular` text NOT NULL,
  `nome_organizacao_mantedora` varchar(300) NOT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `cep` varchar(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `ponto_referencia` text NOT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `utilizacao_original` text,
  `utilizacao_atual` text,
  `acesso` text,
  `via_terrestre` text NOT NULL,
  `acesso_mais_utilizado` text,
  `transporte` text NOT NULL,
  `protecao` text,
  `conservacao` text NOT NULL,
  `entrada_atrativo` text NOT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `visita_tipo` varchar(30) NOT NULL,
  `visita_duracao_media` varchar(5) NOT NULL,
  `visita_guia` varchar(12) DEFAULT NULL,
  `visita_idiomas` text,
  `visita_outros_idiomas` text,
  `entrada_tipo` varchar(10) NOT NULL,
  `entrada_valor` varchar(20) DEFAULT NULL,
  `formas_pagamento` text,
  `necessaria_autorizacao_previa` tinyint(1) NOT NULL DEFAULT '0',
  `autorizacao_previa_tipo` text,
  `limite_numero_visitantes` tinyint(1) NOT NULL DEFAULT '0',
  `limite_numero_visitantes_quantidade` varchar(20) DEFAULT NULL,
  `servicos_equipamentos` text NOT NULL,
  `atividades_realizadas` text,
  `visitantes_epoca_maior_fluxo` varchar(27) DEFAULT NULL,
  `visitantes_epoca_menor_fluxo` varchar(27) DEFAULT NULL,
  `visitantes_origem` varchar(200) DEFAULT NULL,
  `visitantes_origem_internacionais` text,
  `visitantes_numero_anual` varchar(20) DEFAULT NULL,
  `descricao_atrativo` text NOT NULL,
  `observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_city_other_services_type2_idx` (`sub_type_id`),
  KEY `fk_cultural_attraction_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city10000000010` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district10000000010` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type1000010` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type2000010` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_cultural_attraction_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cultural_attraction`
--

LOCK TABLES `cultural_attraction` WRITE;
/*!40000 ALTER TABLE `cultural_attraction` DISABLE KEYS */;
/*!40000 ALTER TABLE `cultural_attraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination`
--

DROP TABLE IF EXISTS `destination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `publico` tinyint(1) NOT NULL DEFAULT '1',
  `nome` varchar(250) NOT NULL,
  `slug` varchar(80) NOT NULL,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `turismo_negocios_eventos` tinyint(1) NOT NULL DEFAULT '0',
  `indutor_turismo_lazer` tinyint(1) NOT NULL DEFAULT '0',
  `cep` varchar(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) DEFAULT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `flickr` varchar(200) DEFAULT NULL,
  `tripadvisor_url` varchar(2083) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `depoimento` text,
  `depoimento_autor` text,
  `descricao_curta` text,
  `titulo` text,
  `descricao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_destination_city1_idx` (`city_id`),
  KEY `fk_destination_district1_idx` (`district_id`),
  KEY `slug` (`slug`),
  KEY `fk_destination_region_idx` (`region_id`),
  FULLTEXT KEY `nome` (`nome`),
  CONSTRAINT `fk_destination_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_destination_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_destination_region` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination`
--

LOCK TABLES `destination` WRITE;
/*!40000 ALTER TABLE `destination` DISABLE KEYS */;
/*!40000 ALTER TABLE `destination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination_hashtag`
--

DROP TABLE IF EXISTS `destination_hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination_hashtag` (
  `hashtag_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  PRIMARY KEY (`hashtag_id`,`destination_id`),
  KEY `fk_hashtag_has_destination_destination1_idx` (`destination_id`),
  KEY `fk_hashtag_has_destination_hashtag1_idx` (`hashtag_id`),
  CONSTRAINT `fk_hashtag_has_destination_destination1` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_hashtag_has_destination_hashtag1` FOREIGN KEY (`hashtag_id`) REFERENCES `hashtag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination_hashtag`
--

LOCK TABLES `destination_hashtag` WRITE;
/*!40000 ALTER TABLE `destination_hashtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `destination_hashtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `destination_photo`
--

DROP TABLE IF EXISTS `destination_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destination_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destination_id` int(11) NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0',
  `is_map` tinyint(1) NOT NULL DEFAULT '0',
  `is_testimonial` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(2083) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_destination_photo_destination1_idx` (`destination_id`),
  CONSTRAINT `fk_destination_photo_destination1` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destination_photo`
--

LOCK TABLES `destination_photo` WRITE;
/*!40000 ALTER TABLE `destination_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `destination_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `district`
--

DROP TABLE IF EXISTS `district`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_district_city1_idx` (`city_id`),
  CONSTRAINT `fk_district_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `district`
--

LOCK TABLES `district` WRITE;
/*!40000 ALTER TABLE `district` DISABLE KEYS */;
/*!40000 ALTER TABLE `district` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `donated_media`
--

DROP TABLE IF EXISTS `donated_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `donated_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `url` varchar(2083) NOT NULL,
  `permite_compartilhar` int(11) DEFAULT NULL,
  `permite_uso_comercial` tinyint(1) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `documento` varchar(25) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `descricao` varchar(250) DEFAULT NULL,
  `tipo_midia` varchar(20) DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `nome_original_arquivo` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_donatedmedia_city_idx` (`city_id`),
  CONSTRAINT `fk_donatedmedia_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `donated_media`
--

LOCK TABLES `donated_media` WRITE;
/*!40000 ALTER TABLE `donated_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `donated_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `economic_activity`
--

DROP TABLE IF EXISTS `economic_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `economic_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `economic_activity`
--

LOCK TABLES `economic_activity` WRITE;
/*!40000 ALTER TABLE `economic_activity` DISABLE KEYS */;
INSERT INTO `economic_activity` VALUES (1,'01 - Agricultura, pecuária e serviços relacionados'),(2,'02 - Produção florestal'),(3,'03 - Pesca e aqüicultura'),(4,'05 - Extração de carvão mineral'),(5,' 06 - Extração de petróleo e gás natural'),(6,'07 - Extração de minerais metálicos'),(7,'08 - Extração de minerais não-metálicos'),(8,' 09 - Atividades de apoio à extração de minerais'),(9,'10 - Fabricação de produtos alimentícios'),(10,'11 - Fabricação de bebidas'),(11,'12 - Fabricação de produtos do fumo'),(12,'13 - Fabricação de produtos têxteis'),(13,'14 - Confecção de artigos do vestuário e acessórios'),(14,'15 - Preparação de couros e fabricação de artefatos de couro, artigos para viagem e calçados'),(15,'16 - Fabricação de produtos de madeira'),(16,'17 - Fabricação de celulose, papel e produtos de papel'),(17,'18 - Impressão e reprodução de gravações'),(18,'19 - Fabricação de coque, de produtos derivados do petróleo e de biocombustíveis'),(19,'20 - Fabricação de produtos químicos'),(20,'21 - Fabricação de produtos farmoquímicos e farmacêuticos'),(21,'22 - Fabricação de produtos de borracha e de material plástico'),(22,'23 - Fabricação de produtos de minerais não-metálicos'),(23,'24 - Metalurgia'),(24,'25 - Fabricação de produtos de metal, exceto máquinas e equipamentos'),(25,'26 - Fabricação de equipamentos de informática, produtos eletrônicos e ópticos'),(26,'27 - Fabricação de máquinas, aparelhos e materiais elétricos'),(27,'28 - Fabricação de máquinas e equipamentos'),(28,'29 - Fabricação de veículos automotores, reboques e carrocerias'),(29,'30 - Fabricação de outros equipamentos de transporte, exceto veículos automotores'),(30,'31 - Fabricação de móveis'),(31,'32 - Fabricação de produtos diversos'),(32,'33 - Manutenção, reparação e instalação de máquinas e equipamentos'),(33,'35 - Eletricidade, gás e outras utilidades'),(34,'36 - Captação, tratamento e distribuição de água'),(35,'37 - Esgoto e atividades relacionadas'),(36,'38 - Coleta, tratamento e disposição de resíduos; recuperação de materiais'),(37,'39 - Descontaminação e outros serviços de gestão de resíduos'),(38,'41 - Construção de edifícios'),(39,'42 - Obras de infra-estrutura'),(40,'43 - Serviços especializados para construção'),(41,'45 - Comércio e reparação de veículos automotores e motocicletas'),(42,'46 - Comércio por atacado, exceto veículos automotores e motocicletas'),(43,'47 - Comércio varejista'),(44,'49 - Transporte terrestre'),(45,'50 - Transporte aquaviário'),(46,'51 - Transporte aéreo'),(47,'52 - Armazenamento e atividades auxiliares dos transportes'),(48,'53 - Correio e outras atividades de entrega'),(49,'55 - Alojamento'),(50,'56 - Alimentação'),(51,'58 - Edição e edição integrada à impressão'),(52,'59 - Atividades cinematográficas, produção de vídeos e de programas de televisão; gravação de som e edição de música'),(53,'60 - Atividades de rádio e de televisão'),(54,'61 - Telecomunicações'),(55,'62 - Atividades dos serviços de tecnologia da informação'),(56,'63 - Atividades de prestação de serviços de informação'),(57,'64 - Atividades de serviços financeiros'),(58,'65 - Seguros, resseguros, previdência complementar e planos de saúde'),(59,'66 - Atividades auxiliares dos serviços financeiros, seguros, previdência complementar e planos de saúde'),(60,'68 - Atividades imobiliárias'),(61,'69 - Atividades jurídicas, de contabilidade e de auditoria'),(62,'70 - Atividades de sedes de empresas e de consultoria em gestão empresarial'),(63,'71 - Serviços de arquitetura e engenharia; testes e análises técnicas'),(64,' 72 - Pesquisa e desenvolvimento científico'),(65,'73 - Publicidade e pesquisa de mercado'),(66,'74 - Outras atividades profissionais, científicas e técnicas'),(67,'75 - Atividades veterinárias'),(68,'77 - Aluguéis não-imobiliários e gestão de ativos intangíveis não-financeiros'),(69,'78 - Seleção, agenciamento e locação de mão-de-obra'),(70,'79 - Agências de viagens, operadores turísticos e serviços de reservas'),(71,'80 - Atividades de vigilância, segurança e investigação'),(72,'81 - Serviços para edifícios e atividades paisagísticas'),(73,'82 - Serviços de escritório, de apoio administrativo e outros serviços prestados principalmente às empresas'),(74,'84 - Administração pública, defesa e seguridade social'),(75,'85 - Educação'),(76,'86 - Atividades de atenção à saúde humana'),(77,'87 - Atividades de atenção à saúde humana integradas com assistência social, prestadas em residências coletivas e particulares'),(78,'88 - Serviços de assistência social sem alojamento'),(79,'90 - Atividades artísticas, criativas e de espetáculos'),(80,'91 - Atividades ligadas ao patrimônio cultural e ambiental'),(81,'92 - Atividades de exploração de jogos de azar e apostas'),(82,'93 - Atividades esportivas e de recreação e lazer'),(83,'94 - Atividades de organizações associativas'),(84,'95 - Reparação e manutenção de equipamentos de informática e comunicação e de objetos pessoais e domésticos'),(85,'96 - Outras atividades de serviços pessoais'),(86,'97 - Serviços domésticos'),(87,'99 - Organismos internacionais e outras instituições extraterritoriais');
/*!40000 ALTER TABLE `economic_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `economic_activity_attraction`
--

DROP TABLE IF EXISTS `economic_activity_attraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `economic_activity_attraction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `nome_oficial` varchar(300) NOT NULL,
  `nome_popular` varchar(300) NOT NULL,
  `nome_organizacao_mantedora` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `ponto_referencia` text NOT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `setor_responsavel_visitacao` text NOT NULL,
  `acesso` text,
  `via_terrestre` text NOT NULL,
  `acesso_mais_utilizado` text,
  `transporte` text NOT NULL,
  `protecao` text NOT NULL,
  `conservacao` text NOT NULL,
  `entrada_atrativo` text NOT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `visita_tipo` varchar(30) NOT NULL,
  `visita_duracao_media` varchar(5) NOT NULL,
  `visita_guia` varchar(12) DEFAULT NULL,
  `visita_idiomas` text,
  `visita_outros_idiomas` text,
  `entrada_tipo` varchar(10) NOT NULL,
  `entrada_valor` varchar(20) DEFAULT NULL,
  `formas_pagamento` text,
  `necessaria_autorizacao_previa` tinyint(1) NOT NULL DEFAULT '0',
  `autorizacao_previa_tipo` text,
  `limite_numero_visitantes` tinyint(1) NOT NULL DEFAULT '0',
  `limite_numero_visitantes_quantidade` varchar(20) DEFAULT NULL,
  `servicos_equipamentos` text NOT NULL,
  `atividades_realizadas` text,
  `visitantes_epoca_maior_fluxo` varchar(27) DEFAULT NULL,
  `visitantes_epoca_menor_fluxo` varchar(27) DEFAULT NULL,
  `visitantes_origem` varchar(200) DEFAULT NULL,
  `visitantes_origem_internacionais` text,
  `visitantes_numero_anual` varchar(20) DEFAULT NULL,
  `descricao_curta` text,
  `descricao_atrativo` text NOT NULL,
  `observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_city_other_services_type2_idx` (`sub_type_id`),
  KEY `fk_economic_activity_attraction_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city100000000100` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district100000000100` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type10000100` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type20000100` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_economic_activity_attraction_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `economic_activity_attraction`
--

LOCK TABLES `economic_activity_attraction` WRITE;
/*!40000 ALTER TABLE `economic_activity_attraction` DISABLE KEYS */;
/*!40000 ALTER TABLE `economic_activity_attraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `touristic_circuit_id` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `slug` varchar(80) NOT NULL,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `flickr` varchar(200) DEFAULT NULL,
  `descricao_curta` text,
  `local_evento` varchar(150) DEFAULT NULL,
  `espaco_evento` varchar(300) DEFAULT NULL,
  `tipo_evento` varchar(45) DEFAULT NULL,
  `sobre` text,
  `dados_relevantes` text,
  `informacoes_uteis` text,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `comentario_revisao` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `fk_event_city1_idx` (`city_id`),
  KEY `slug_index` (`slug`),
  KEY `fk_event_touristic_circuit1_idx` (`touristic_circuit_id`),
  KEY `fk_event_user1_idx` (`created_by`),
  KEY `fk_event_user2_idx` (`updated_by`),
  KEY `fk_event_revision_status1_idx` (`revision_status_id`),
  FULLTEXT KEY `nome` (`nome`),
  CONSTRAINT `fk_event_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_touristic_circuit1` FOREIGN KEY (`touristic_circuit_id`) REFERENCES `touristic_circuit` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_user1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_user2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `event_by_date`
--

DROP TABLE IF EXISTS `event_by_date`;
/*!50001 DROP VIEW IF EXISTS `event_by_date`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `event_by_date` AS SELECT 
 1 AS `start`,
 1 AS `end`,
 1 AS `event_id`,
 1 AS `name`,
 1 AS `short_description`,
 1 AS `events_place`,
 1 AS `featured`,
 1 AS `city`,
 1 AS `id`,
 1 AS `city_id`,
 1 AS `touristic_circuit_id`,
 1 AS `revision_status_id`,
 1 AS `active`,
 1 AS `nome`,
 1 AS `slug`,
 1 AS `destaque`,
 1 AS `cep`,
 1 AS `bairro`,
 1 AS `logradouro`,
 1 AS `numero`,
 1 AS `complemento`,
 1 AS `latitude`,
 1 AS `longitude`,
 1 AS `site`,
 1 AS `telefone`,
 1 AS `email`,
 1 AS `facebook`,
 1 AS `instagram`,
 1 AS `twitter`,
 1 AS `youtube`,
 1 AS `flickr`,
 1 AS `descricao_curta`,
 1 AS `local_evento`,
 1 AS `espaco_evento`,
 1 AS `tipo_evento`,
 1 AS `sobre`,
 1 AS `dados_relevantes`,
 1 AS `informacoes_uteis`,
 1 AS `created_at`,
 1 AS `created_by`,
 1 AS `updated_at`,
 1 AS `updated_by`,
 1 AS `comentario_revisao`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `event_category`
--

DROP TABLE IF EXISTS `event_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(250) NOT NULL,
  `nome_en` varchar(250) DEFAULT NULL,
  `nome_es` varchar(250) DEFAULT NULL,
  `nome_fr` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_category`
--

LOCK TABLES `event_category` WRITE;
/*!40000 ALTER TABLE `event_category` DISABLE KEYS */;
INSERT INTO `event_category` VALUES (1,'Feiras e Exposições','Fairs and Exhibitions','Ferias y exposiciones','Foires et Expositions'),(2,'Congressos e Convenções','Conferences and Conventions','Congresos y Convenciones','Conférences et conventions'),(3,'Outros','Others','Otro','Autre'),(4,'Show','Show','Espectáculo','Spectacle'),(5,'Cívicos','Civics','Cívicos','civique'),(6,'Culturais e Artísticos','Cultural and Artistic','Culturales y Artísticos','Culturelle et artistique'),(7,'Esportivo','Sports','deportes','sportif'),(8,'Gastronômicos e Produtos','Gastronomic and Products','Gastronómicos y Productos','Gastronomique et Produits'),(9,'Populares e Folclóricos','Popular & Folkloric','Populares y Folclóricas','Populaire et folklorique'),(10,'Religiosos','Religious','Religiosos','Religieux'),(11,'WQS','WQS','WQS','WQS');
/*!40000 ALTER TABLE `event_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_cities`
--

DROP TABLE IF EXISTS `event_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_cities` (
  `event_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`city_id`),
  KEY `fk_event_has_city_city1_idx` (`city_id`),
  KEY `fk_event_has_city_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_has_city_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_has_city_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_cities`
--

LOCK TABLES `event_cities` WRITE;
/*!40000 ALTER TABLE `event_cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_classification`
--

DROP TABLE IF EXISTS `event_classification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_classification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(250) NOT NULL,
  `ativo` tinyint(1) DEFAULT '1',
  `initial_date` date NOT NULL,
  `final_date` date NOT NULL,
  `public` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_classification`
--

LOCK TABLES `event_classification` WRITE;
/*!40000 ALTER TABLE `event_classification` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_classification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_date`
--

DROP TABLE IF EXISTS `event_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_date` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_date_range_event1_idx` (`event_id`),
  KEY `index` (`id`),
  CONSTRAINT `fk_event_date_range_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_date`
--

LOCK TABLES `event_date` WRITE;
/*!40000 ALTER TABLE `event_date` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_hashtag`
--

DROP TABLE IF EXISTS `event_hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_hashtag` (
  `event_id` int(11) NOT NULL,
  `hashtag_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`hashtag_id`),
  KEY `fk_event_has_hashtag_hashtag1_idx` (`hashtag_id`),
  KEY `fk_event_has_hashtag_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_has_hashtag_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_has_hashtag_hashtag1` FOREIGN KEY (`hashtag_id`) REFERENCES `hashtag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_hashtag`
--

LOCK TABLES `event_hashtag` WRITE;
/*!40000 ALTER TABLE `event_hashtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_hashtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_photo`
--

DROP TABLE IF EXISTS `event_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(2083) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_photo_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_photo_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_photo`
--

LOCK TABLES `event_photo` WRITE;
/*!40000 ALTER TABLE `event_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_selected_categories`
--

DROP TABLE IF EXISTS `event_selected_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_selected_categories` (
  `event_category_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`event_category_id`,`event_id`),
  KEY `fk_event_type_has_event_event1_idx` (`event_id`),
  KEY `fk_event_type_has_event_event_type1_idx` (`event_category_id`),
  CONSTRAINT `fk_event_type_has_event_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_type_has_event_event_type1` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_selected_categories`
--

LOCK TABLES `event_selected_categories` WRITE;
/*!40000 ALTER TABLE `event_selected_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_selected_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_selected_classification`
--

DROP TABLE IF EXISTS `event_selected_classification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_selected_classification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_classification_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_classification_id1_idx` (`event_classification_id`),
  KEY `fk_event_id1_idx` (`event_id`),
  CONSTRAINT `fk_event_selected_classification_1` FOREIGN KEY (`event_classification_id`) REFERENCES `event_classification` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_selected_classification_2` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_selected_classification`
--

LOCK TABLES `event_selected_classification` WRITE;
/*!40000 ALTER TABLE `event_selected_classification` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_selected_classification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_service`
--

DROP TABLE IF EXISTS `event_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `tipo_atividade_cadastur` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `nome_rede` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `registro_cadastur` varchar(20) DEFAULT NULL,
  `inicio_atividade` varchar(7) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `descricao_arredores_distancia_pontos` text NOT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `espaco_fisico` text,
  `servicos_equipamentos_apoio` text,
  `outras_instalacoes_equipamentos` text,
  `descricoes_observacoes_complementares` text,
  `nome_aeroporto1` varchar(100) DEFAULT NULL,
  `distancia_aeroporto1` varchar(20) DEFAULT NULL,
  `nome_aeroporto2` varchar(100) DEFAULT NULL,
  `distancia_aeroporto2` varchar(20) DEFAULT NULL,
  `apresenta_projeto_seguranca` tinyint(1) NOT NULL DEFAULT '0',
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `validade_cadastur` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_event_service_revision_status1_idx` (`revision_status_id`),
  KEY `fk_city_other_services_subtype100000_idx` (`sub_type_id`),
  CONSTRAINT `fk_city_access_city1000000000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district1000000000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_subtype100000` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type100000` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_service`
--

LOCK TABLES `event_service` WRITE;
/*!40000 ALTER TABLE `event_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `events_by_date_category_classification`
--

DROP TABLE IF EXISTS `events_by_date_category_classification`;
/*!50001 DROP VIEW IF EXISTS `events_by_date_category_classification`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `events_by_date_category_classification` AS SELECT 
 1 AS `event_id`,
 1 AS `end`,
 1 AS `start`,
 1 AS `featured`,
 1 AS `name`,
 1 AS `short_description`,
 1 AS `city`,
 1 AS `city_id`,
 1 AS `events_place`,
 1 AS `revision_status_id`,
 1 AS `slug`,
 1 AS `event_category_id`,
 1 AS `event_classification_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text,
  `queue` text,
  `payload` longtext,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `food_drinks_service`
--

DROP TABLE IF EXISTS `food_drinks_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `food_drinks_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `tipo_atividade_cadastur` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `nome_rede` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `registro_cadastur` varchar(20) DEFAULT NULL,
  `inicio_atividade` varchar(7) DEFAULT NULL,
  `data_tombamento` datetime DEFAULT NULL,
  `organizacao_responsavel` varchar(300) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `descricao_arredores_distancia_pontos` text NOT NULL,
  `pontos_referencia` text NOT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `capacidade_pessoas_em_pe` varchar(20) DEFAULT NULL,
  `capacidade_pessoas_sentadas` varchar(20) DEFAULT NULL,
  `capacidade_mesas` varchar(20) NOT NULL,
  `servicos_equipamentos` text,
  `culinaria_nacionalidades` text,
  `culinaria_nacionalidade_outras` text,
  `culinaria_servicos` text,
  `culinaria_temas` text,
  `culinaria_tema_outros` text,
  `culinaria_regioes` text,
  `culinaria_regiao_outras` text,
  `aceita_animais` tinyint(1) DEFAULT NULL,
  `gay_friendly` tinyint(1) DEFAULT NULL,
  `nao_aceita_criancas` tinyint(1) DEFAULT NULL,
  `restricoes` text,
  `formas_pagamento` text,
  `descricoes_observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `tem_cadastur` tinyint(1) NOT NULL DEFAULT '0',
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  `atracao` tinyint(1) NOT NULL DEFAULT '0',
  `validade_cadastur` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_food_drinks_service_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city1000000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district1000000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type100` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_food_drinks_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `food_drinks_service`
--

LOCK TABLES `food_drinks_service` WRITE;
/*!40000 ALTER TABLE `food_drinks_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `food_drinks_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastronomic_primary_product`
--

DROP TABLE IF EXISTS `gastronomic_primary_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastronomic_primary_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `relevancia_economia_cidade` varchar(5) DEFAULT NULL,
  `periodo_colheita` varchar(27) DEFAULT NULL,
  `produto_agricultura_familiar` tinyint(1) DEFAULT NULL,
  `nome_associacao` varchar(250) DEFAULT NULL,
  `responsavel` varchar(250) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `reconhecido_como_patrimonio` tinyint(1) DEFAULT NULL,
  `orgaos_que_reconhecem` varchar(80) DEFAULT NULL,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `revision_status_id` int(11) NOT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_gastronomic_attraction_revision_status1_idx` (`revision_status_id`),
  KEY `fk_gastronomic_primary_product_district1_idx` (`district_id`),
  CONSTRAINT `fk_city_access_city10000000010000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gastronomic_attraction_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gastronomic_primary_product_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastronomic_primary_product`
--

LOCK TABLES `gastronomic_primary_product` WRITE;
/*!40000 ALTER TABLE `gastronomic_primary_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `gastronomic_primary_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastronomic_transformed_product`
--

DROP TABLE IF EXISTS `gastronomic_transformed_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastronomic_transformed_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `receita` text,
  `reconhecido_como_patrimonio` tinyint(1) DEFAULT NULL,
  `orgaos_que_reconhecem` varchar(80) DEFAULT NULL,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `revision_status_id` int(11) NOT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gastronomic_transformed_product_city1_idx` (`city_id`),
  KEY `fk_gastronomic_transformed_product_district1_idx` (`district_id`),
  KEY `fk_gastronomic_transformed_product_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_gastronomic_transformed_product_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gastronomic_transformed_product_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gastronomic_transformed_product_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastronomic_transformed_product`
--

LOCK TABLES `gastronomic_transformed_product` WRITE;
/*!40000 ALTER TABLE `gastronomic_transformed_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `gastronomic_transformed_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gastronomic_typical_dish`
--

DROP TABLE IF EXISTS `gastronomic_typical_dish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gastronomic_typical_dish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `receita` text,
  `reconhecido_como_patrimonio` tinyint(1) DEFAULT NULL,
  `orgaos_que_reconhecem` varchar(80) DEFAULT NULL,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `revision_status_id` int(11) NOT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gastronomic_typical_dish_city1_idx` (`city_id`),
  KEY `fk_gastronomic_typical_dish_district1_idx` (`district_id`),
  KEY `fk_gastronomic_typical_dish_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_gastronomic_typical_dish_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gastronomic_typical_dish_district1` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gastronomic_typical_dish_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gastronomic_typical_dish`
--

LOCK TABLES `gastronomic_typical_dish` WRITE;
/*!40000 ALTER TABLE `gastronomic_typical_dish` DISABLE KEYS */;
/*!40000 ALTER TABLE `gastronomic_typical_dish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hashtag`
--

DROP TABLE IF EXISTS `hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hashtag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(140) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hashtag`
--

LOCK TABLES `hashtag` WRITE;
/*!40000 ALTER TABLE `hashtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `hashtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institutional_photo`
--

DROP TABLE IF EXISTS `institutional_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institutional_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_testimonial` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(2083) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institutional_photo`
--

LOCK TABLES `institutional_photo` WRITE;
/*!40000 ALTER TABLE `institutional_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `institutional_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_item`
--

DROP TABLE IF EXISTS `inventory_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_item`
--

LOCK TABLES `inventory_item` WRITE;
/*!40000 ALTER TABLE `inventory_item` DISABLE KEYS */;
INSERT INTO `inventory_item` VALUES (1,'A2'),(2,'A4'),(3,'A5'),(4,'A7'),(5,'B1'),(6,'B2'),(7,'B3'),(8,'B4'),(9,'B5'),(10,'B6'),(11,'B7'),(12,'C1'),(13,'C2'),(14,'C3'),(15,'C4');
/*!40000 ALTER TABLE `inventory_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_photo`
--

DROP TABLE IF EXISTS `inventory_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_inventario` varchar(4) NOT NULL,
  `item_inventario_id` int(11) NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(2083) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_photo`
--

LOCK TABLES `inventory_photo` WRITE;
/*!40000 ALTER TABLE `inventory_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `inventory_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_home`
--

DROP TABLE IF EXISTS `items_home`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_home` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(5) NOT NULL,
  `foto_capa_url` varchar(2083) NOT NULL,
  `titulo_o_que_fazer` varchar(100) DEFAULT NULL,
  `titulo_destinos` varchar(100) DEFAULT NULL,
  `titulo_instagram` varchar(100) DEFAULT NULL,
  `texto_instagram` text,
  `perfil_instagram` varchar(100) NOT NULL,
  `titulo_aplicativos` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_home`
--

LOCK TABLES `items_home` WRITE;
/*!40000 ALTER TABLE `items_home` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_home` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_institutional`
--

DROP TABLE IF EXISTS `items_institutional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_institutional` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(5) NOT NULL,
  `depoimento` text,
  `depoimento_autor` varchar(200) DEFAULT NULL,
  `titulo` varchar(100) DEFAULT NULL,
  `descricao` text,
  `video_url` varchar(2083) DEFAULT NULL,
  `dado_relevante_1` varchar(50) DEFAULT NULL,
  `dado_relevante_descricao_1` varchar(100) DEFAULT NULL,
  `dado_relevante_2` varchar(50) DEFAULT NULL,
  `dado_relevante_descricao_2` varchar(100) DEFAULT NULL,
  `dado_relevante_3` varchar(50) DEFAULT NULL,
  `dado_relevante_descricao_3` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_institutional`
--

LOCK TABLES `items_institutional` WRITE;
/*!40000 ALTER TABLE `items_institutional` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_institutional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items_page_descriptions`
--

DROP TABLE IF EXISTS `items_page_descriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items_page_descriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(5) NOT NULL,
  `pagina` varchar(45) NOT NULL,
  `descricao` text,
  `descricao_outra` text,
  PRIMARY KEY (`id`),
  KEY `language` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items_page_descriptions`
--

LOCK TABLES `items_page_descriptions` WRITE;
/*!40000 ALTER TABLE `items_page_descriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `items_page_descriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) unsigned NOT NULL,
  `reserved` tinyint(3) unsigned NOT NULL,
  `reserved_at` int(10) unsigned DEFAULT NULL,
  `available_at` int(10) unsigned NOT NULL,
  `created_at` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
INSERT INTO `language` VALUES (1,'Alemão'),(2,'Espanhol'),(3,'Francês'),(4,'Inglês'),(5,'Italiano'),(6,'Português');
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_carrier`
--

DROP TABLE IF EXISTS `mobile_carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_carrier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_carrier`
--

LOCK TABLES `mobile_carrier` WRITE;
/*!40000 ALTER TABLE `mobile_carrier` DISABLE KEYS */;
INSERT INTO `mobile_carrier` VALUES (1,'TIM'),(2,'Vivo'),(3,'Oi'),(4,'Claro'),(5,'Nextel');
/*!40000 ALTER TABLE `mobile_carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mobile_carrier_service`
--

DROP TABLE IF EXISTS `mobile_carrier_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mobile_carrier_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mobile_carrier_service`
--

LOCK TABLES `mobile_carrier_service` WRITE;
/*!40000 ALTER TABLE `mobile_carrier_service` DISABLE KEYS */;
INSERT INTO `mobile_carrier_service` VALUES (3,'3G (dados)'),(4,'Telefonia (voz)'),(5,'Banda Larga Móvel (modem)');
/*!40000 ALTER TABLE `mobile_carrier_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `natural_attraction`
--

DROP TABLE IF EXISTS `natural_attraction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `natural_attraction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `nome_oficial` varchar(300) NOT NULL,
  `nome_popular` text NOT NULL,
  `nome_organizacao_mantedora` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `cep` varchar(11) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `ponto_referencia` text NOT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `acesso` text,
  `via_terrestre` text NOT NULL,
  `acesso_mais_utilizado` text,
  `transporte` text NOT NULL,
  `protecao` text,
  `conservacao` text NOT NULL,
  `entrada_atrativo` text NOT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `visita_tipo` varchar(30) NOT NULL,
  `visita_guia` varchar(12) DEFAULT NULL,
  `visita_idiomas` text,
  `visita_outros_idiomas` text,
  `entrada_tipo` varchar(10) NOT NULL,
  `entrada_valor` varchar(20) DEFAULT NULL,
  `necessaria_autorizacao_previa` tinyint(1) NOT NULL DEFAULT '0',
  `autorizacao_previa_tipo` text,
  `limite_numero_visitantes` tinyint(1) NOT NULL DEFAULT '0',
  `limite_numero_visitantes_quantidade` varchar(30) DEFAULT NULL,
  `limite_numero_visitantes_regra` varchar(5) DEFAULT NULL,
  `servicos_equipamentos` text NOT NULL,
  `atividades_realizadas` text,
  `visitantes_epoca_maior_fluxo` varchar(27) DEFAULT NULL,
  `visitantes_epoca_menor_fluxo` varchar(27) DEFAULT NULL,
  `descricao_atrativo` text NOT NULL,
  `observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_city_other_services_type2_idx` (`sub_type_id`),
  KEY `fk_natural_attraction_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city1000000001` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district1000000001` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type100001` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type200001` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_natural_attraction_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `natural_attraction`
--

LOCK TABLES `natural_attraction` WRITE;
/*!40000 ALTER TABLE `natural_attraction` DISABLE KEYS */;
/*!40000 ALTER TABLE `natural_attraction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `other_service`
--

DROP TABLE IF EXISTS `other_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `other_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `tipo_atividade_cadastur` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `sub_type_id` int(11) DEFAULT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `registro_cadastur` varchar(20) DEFAULT NULL,
  `inicio_atividade` varchar(7) DEFAULT NULL,
  `natureza_entidade` varchar(45) NOT NULL,
  `natureza_entidade_outras` text,
  `registro_filiacao_entidade` varchar(300) DEFAULT NULL,
  `registro_filiacao_numero` int(11) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `descricao_arredores_distancia_pontos` text NOT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `taxa_ocupacao_media_mensal` float DEFAULT NULL,
  `mes_maior_atendimento` varchar(27) DEFAULT NULL,
  `mes_menor_atendimento` varchar(27) DEFAULT NULL,
  `principais_atividades` text,
  `atendimento_bilingue` tinyint(1) NOT NULL DEFAULT '0',
  `atendimento_bilingue_idiomas` text,
  `descricoes_observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `validade_cadastur` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_city_other_services_type2_idx` (`sub_type_id`),
  KEY `fk_other_service_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city100000000000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district100000000000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type10000000` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type20000000` FOREIGN KEY (`sub_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_other_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `other_service`
--

LOCK TABLES `other_service` WRITE;
/*!40000 ALTER TABLE `other_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `other_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parameter`
--

DROP TABLE IF EXISTS `parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parameter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(200) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parameter`
--

LOCK TABLES `parameter` WRITE;
/*!40000 ALTER TABLE `parameter` DISABLE KEYS */;
INSERT INTO `parameter` VALUES (1,'fale_conosco_pt','Este canal é para você estar mais próximo de nós!\r\n\r\nPreencha o formulário abaixo e mande sua mensagem com a dúvida, elogio, sugestão ou reclamação.'),(2,'fale_conosco_en',''),(3,'fale_conosco_es',''),(4,'fale_conosco_fr',''),(5,'planeje-sua-viagem-descricao_pt',''),(6,'planeje-sua-viagem-como-chegar_pt',''),(7,'planeje-sua-viagem-informacoes_pt','<div style=\"line-height: 1.5;\"><span style=\"font-weight: bold;\">Telefones de emergência 24h:</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5; text-decoration: underline;\">Polícia Militar:</span><span style=\"line-height: 1.5;\"> 190, 911 ou 112</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><span style=\"text-decoration: underline;\">Corpo de Bombeiros:</span> 193</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><span style=\"text-decoration: underline;\">Polícia Federal:</span> 194</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><span style=\"text-decoration: underline;\">Polícia Civil:</span> 197</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><span style=\"text-decoration: underline;\">Disque Intoxicação:</span> 0800-722-6001</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><span style=\"font-weight: bold;\">Horário de atendimento dos bancos:</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">As agências funcionam de segunda a sexta, das 10h às 16h. Os caixas eletrônicos funcionam geralmente até às 22h.</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><br></span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><span style=\"font-weight: bold;\">Horário de funcionamento dos Shoppings:</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">Segunda a sábado, das 10h às 22h. Domingos e feriados, das 10h às 20h.</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><br></span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><span style=\"font-weight: bold;\">Horário do comércio de rua:</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">Segunda a sábado, das 9h às 18h.</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><br></span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><span style=\"font-weight: bold; line-height: 1.5;\">Ligações telefônicas:</span><br></div>\r\n\r\n\r\n</div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><span style=\"text-decoration: underline; line-height: 1.5;\">Principais operadoras:</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">21 - Claro/Net/Embratel</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">12 - CTBC</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">77 - Nextel</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">31 - Oi</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">41 - Tim</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">15 - Vivo</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\"><br></span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><span style=\"text-decoration: underline;\">Fazer um DDD (Discagem Direta à Distância) para qualquer lugar do país:&nbsp;</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">0 + XX (código da operadora) + XX (código da cidade) + telefone</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5; text-decoration: underline;\">Fazer um DDD à cobrar para qualquer lugar do país:</span><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">90 + XX (código da operadora) + XX (código da cidade) + telefone</span><br></div>\r\n\r\n\r\n</div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"text-decoration: underline;\">Fazer um DDI (Discagem Direta Internacional) para qualquer cidade do mundo:</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">00 + XX (código da operadora) &nbsp;+ código do país + XX (código da cidade) + telefone</span></div>\r\n\r\n\r\n</div>\r\n\r\n\r\n</div>\r\n\r\n\r\n</div>\r\n\r\n\r\n</div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><br></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><div style=\"line-height: 1.5;\"><span style=\"font-weight: bold;\">Voltagem elétrica:&nbsp;</span></div>\r\n\r\n\r\n<div style=\"line-height: 1.5;\"><span style=\"line-height: 1.5;\">A voltagem em Minas Gerais não é fixa (110 V, 127 V ou 220V), portanto varia de município para município. As tomadas possuem o padrão internacional IEC 60906-1, com padrão Europlug.</span></div>\r\n\r\n\r\n</div>\r\n\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n <div class=\"row-fluid\" id=\"destinos\" style=\"padding:1% 0;\">\r\n        <div class=\"container\">            \r\n        <div class=\"col-lg-12 no-padding\" style=\"margin-top:80px;\">\r\n                 <div class=\"col-lg-8 col-lg-offset-3\">\r\n                                     <div class=\"col-lg-8 listInfos\" style=\"background:transparent\">\r\n                                <a href=\"http://www.minasgerais.com.br/visiteminasgerais.html\" target=\"_blank\">\r\n								<div class=\"position\" style=\"top:10%;\">\r\n                                <img src=\"http://minasgerais.com.br/portal/assets/imgs/ballOne.png\" alt=\"\" style=\"margin-left:auto;margin-right:auto;display:block;padding:7% 0;\" data-pin-nopin=\"true\">\r\n                                                                <p style=\"color:##ef662f;padding: 1% 17%;\">Aplicativo Visite Minas Gerais</p>\r\n\r\n\r\n                            </div>\r\n\r\n\r\n                        </a>                    </div>\r\n\r\n\r\n                    \r\n                                    </div>\r\n\r\n\r\n           </div>\r\n\r\n\r\n        </div>\r\n\r\n\r\n    </div>'),(8,'planeje-sua-viagem-descricao_en',''),(9,'planeje-sua-viagem-como-chegar_en',''),(10,'planeje-sua-viagem-informacoes_en',''),(11,'planeje-sua-viagem-descricao_es',''),(12,'planeje-sua-viagem-como-chegar_es',''),(13,'planeje-sua-viagem-informacoes_es',''),(14,'planeje-sua-viagem-descricao_fr',''),(15,'planeje-sua-viagem-como-chegar_fr',''),(16,'planeje-sua-viagem-informacoes_fr',''),(17,'cadastur_url',''),(18,'cadastur_senha',''),(19,'instagram-client-id',''),(20,'instagram-client-secret',''),(21,'instagram-redirect-uri',''),(22,'instagram-access-token',''),(23,'tooltips-A2.1','[{\"name\":\"2. Rodovias federais (permite mais de uma opção)\",\"text\":\"A avaliação da rodovia deve ser baseada no trecho de \\\"chegada\\\" e \\\"saída\\\" do município, e não de toda a sua extensão.\"}]'),(24,'tooltips-A1','[{\"name\":\"4.5. Número de funcionários permanentes\",\"text\":\"Informe o número de funcionários efetivos\"},{\"name\":\"4.6. Número de funcionários temporários\",\"text\":\"Informe o número de funcionários contratados, estagiários, terceirizados ou comissionados\"},{\"name\":\"4.9. Nome das secretarias, departamentos e outros\",\"text\":\"Informe a estrutura organizacional atual da prefeitura\"},{\"name\":\"5.8. Plano de desenvolvimento do turismo\",\"text\":\"Informe a vigência do Plano de Desenvolvimento do Turismo\"},{\"name\":\"8.1. Nome da instituição\",\"text\":\"Especifique as principais instituições que possam fornecer informações sobre o município\"},{\"name\":\"8.2. Site\",\"text\":\"Especifique os principais sites que possam fornecer informações sobre o município\"},{\"name\":\"8.3. Publicações\",\"text\":\"Especifique as principais publicações que possam fornecer informações sobre o município. Exemplo: Livros, artigos, anuários e etc.\"},{\"name\":\"9. Eventos Permanentes\",\"text\":\"Cite as comemorações e as datas mais importantes do município\"},{\"name\":\"10.1. Histórico do município\",\"text\":\"Informe a origem, a data de criação e emancipação, os fundadores e os principais fatos de importância política\"},{\"name\":\"10.2. Principais parcerias, rede de cooperação, intercâmbios e interfaces com o município\",\"text\":\"Especifique as relações que o município tem com outros municípios e/ou instituições a fim de capacitar e promover a gestão pública e o desenvolvimento do turismo nas esferas sociais, culturais, ambientais, econômicas e de governança\"},{\"name\":\"10.3. Descrições e informações complementares\",\"text\":\"Faça uma breve descrição de aspectos do município que não foram contemplados e tenham relevância\"},{\"name\":\"5.7. Regulamentação específica do turismo\",\"text\":\"Informe se há alguma legislação para o setor de turismo\"},{\"name\":\"3.16. Postos telefônicos público?\",\"text\":\"Informe se há cabines telefônicas, orelhão\"}]'),(25,'home-apps','{\"tem_dados\":0,\"data\":[{\"nome\":\"\",\"tipo\":\"app\",\"url\":\"\",\"url_android\":\"\",\"url_ios\":\"\",\"foto_capa_url\":\"\",\"completo\":0},{\"nome\":\"\",\"tipo\":\"app\",\"url\":\"\",\"url_android\":\"\",\"url_ios\":\"\",\"foto_capa_url\":\"\",\"completo\":0},{\"nome\":\"\",\"tipo\":\"app\",\"url\":\"\",\"url_android\":\"\",\"url_ios\":\"\",\"foto_capa_url\":\"\",\"completo\":0}]}'),(26,'tooltips-B1','[{\"name\":\"13.2. Palavras-chave\",\"text\":\"É um termo que caracteriza o seu conteúdo. Aquele termo que as pessoas devem buscar ao pesquisar no site\"},{\"name\":\"2.1. CNPJ\",\"text\":\"Informe o CNPJ do seu estabelecimento e clique no ícone da lupa para que os dados sejam pesquisados no CADASTUR\"},{\"name\":\"4. Fotos promocionais\",\"text\":\"Resolução ideal 1920x1080 (pixel)\\nTamanho máximo de 3 MB\"},{\"name\":\"15.2. Atração do site?\",\"text\":\"O estabelecimento é considerado um atrativo no município? Se SIM, marque este campo!\"},{\"name\":\"15.3. Atração de destaque?\",\"text\":\"Indica que o Estabelecimento será considerado como um atrativo destaque no Portal, tendo uma maior visibilidade dentre os demais e sendo exibido na Home do FrontEnd.\"},{\"name\":\"15.4. Publicar no portal?\",\"text\":\"Indica que o Estabelecimento será exibido no Portal, ficando visível aos visitantes do site.\"},{\"name\":\"2.7. Data do tombamento\",\"text\":\"Informe quando a construção for histórico/tombado\"},{\"name\":\"6.1.1. Número de UHs\",\"text\":\"Informe o número de quartos do estabelecimento (UHs Unidades Habitacionais)\"},{\"name\":\"6.1.2. Número de Leitos\",\"text\":\"Informe o número de camas existentes no estabelecimento\"},{\"name\":\"3.1. CEP\",\"text\":\"Informe o CEP e/ou Bairro e/ou Logradouro e/ou Número e clique no ícone de geolocalização (ao lado do campo Complemento) para obter as coordenadas de Latitude e Longitude automaticamente.\"},{\"name\":\"3.9. Latitude (formato decimal)\",\"text\":\"Informe o CEP e/ou Bairro e/ou Logradouro e/ou Número e clique no ícone de geolocalização (ao lado do campo Complemento) para obter as coordenadas de Latitude e Longitude automaticamente.\"},{\"name\":\"3.10. Longitude (formato decimal)\",\"text\":\"Informe o CEP e/ou Bairro e/ou Logradouro e/ou Número e clique no ícone de geolocalização (ao lado do campo Complemento) para obter as coordenadas de Latitude e Longitude automaticamente.\"},{\"name\":\"3.8. Latitude e Longitude em decimal\",\"text\":\"Informe o CEP e/ou Bairro e/ou Logradouro e/ou Número e clique no ícone de geolocalização (ao lado do campo Complemento) para obter as coordenadas de Latitude e Longitude automaticamente.\"}]'),(27,'tooltips-B2','[{\"name\":\"2.1. CNPJ\",\"text\":\"Informe o CNPJ do seu estabelecimento e clique no ícone da lupa para que os dados sejam pesquisados no CADASTUR\"},{\"name\":\"4. Fotos promocionais\",\"text\":\"Resolução ideal 1920x1080 (pixel)\\nTamanho máximo de 3 MB\"},{\"name\":\"12.2. Atração do site?\",\"text\":\"O estabelecimento é considerado um atrativo no município? Se SIM, marque este campo!\"},{\"name\":\"12.3. Atração de destaque?\",\"text\":\"Indica que o Estabelecimento será considerado como um atrativo destaque no Portal, tendo uma maior visibilidade dentre os demais e sendo exibido na Home do FrontEnd\"},{\"name\":\"12.4. Publicar no portal?\",\"text\":\"Indica que o Estabelecimento será exibido no Portal, ficando visível aos visitantes do site\"},{\"name\":\"3.1. CEP\",\"text\":\"Informe o CEP e/ou Bairro e/ou Logradouro e/ou Número e clique no ícone de geolocalização (ao lado do campo Complemento) para obter as coordenadas de Latitude e Longitude automaticamente.\"}]'),(28,'tooltips-C1','[{\"name\":\"15.2. Palavras-chave\",\"text\":\"É um termo que caracteriza o seu conteúdo. Aquele termo que as pessoas possam usar para realizar a pesquisa no site.\"},{\"name\":\"2.16. Descrição do atrativo\",\"text\":\"Descrever de forma sucinta e atraente (contendo, se possível, entre 4 e 8 linhas). Lembre-se: Texto promocional!\"},{\"name\":\"2.15. Descrição curta\",\"text\":\"Essa descrição será visibilizada quando o visitante passar o ponteiro do mouse por cima da miniatura do destino, atrativos, eventos, etc. \\nDeve ser atraente e de preferência ter até 10 palavras.\"},{\"name\":\"3.1. CEP\",\"text\":\"teste do jean\"},{\"name\":\"2.16. Descrição do atrativo\",\"text\":\"tesete talasjdlfkja lk lkasj dflkj as\"}]'),(29,'tooltips-C2','[{\"name\":\"16.2. Palavras-chave\",\"text\":\"É um termo que caracteriza o seu conteúdo. Aquele termo que as pessoas devem buscar ao pesquisar no site.\"},{\"name\":\"Destaque?\",\"text\":\"Indica que o Atrativo terá maior visibilidade dentre os demais.\"},{\"name\":\"Publicar no portal?\",\"text\":\"Indica que o Atrativo será exibido no Portal, ficando visível aos visitantes do site.\"},{\"name\":\"13.2.1. Guia de visitação*\",\"text\":\"Refere-se ao Guia de Turismo ( pessoa )\"}]'),(30,'tooltips-eventos','[{\"name\":\"Palavras-chave\",\"text\":\"É um termo que caracterize o seu conteúdo. Aquele termo que as pessoas devem buscar ao pesquisar no site.\"},{\"name\":\"Descrição do Evento\",\"text\":\"Deverá ser sucinta e atraente (contendo, se possível, entre 4 e 8 linhas), constando sua história, objetivos, participantes, atrações, etc. Poderá ser incluída a programação ou uma parte da programação do evento.\"}]'),(31,'tooltips-A2.2','[{\"name\":\"1.3. Tipo\",\"text\":\"Informe quais as formas de chegar ao município\"},{\"name\":\"1.4. Tipo do meio de acesso\",\"text\":\"Detalhe os meios de acesso para chegar ao município\"},{\"name\":\"2.1. Nome\",\"text\":\"Informe o nome da infraestrutura. Exemplo: Aeroporto Internacional Tancredo Neves, Terminal Rodoviário de Belo Horizonte\"},{\"name\":\"3.13. Fluxo de passageiros nacionais\",\"text\":\"Informe o fluxo anual\"},{\"name\":\"3.14. Fluxo de passageiros internacionais\",\"text\":\"Informe o fluxo anual\"},{\"name\":\"4.1. Descrições e informações complementares\",\"text\":\"Caso necessário, descreva informações que sejam relevantes\"}]'),(32,'tooltips-A4','[{\"name\":\"5.2. Descrições e informações complementares\",\"text\":\"Caso necessário, descreva informações que sejam relevantes\"}]'),(33,'tooltips-A5','[{\"name\":\"5.3. Serviços prestados\",\"text\":\"Detalhe o tipo de especialidade ofertada, exemplo: ortopedista, clínico geral, etc.\"},{\"name\":\"5.4. Descrições e informações complementares\",\"text\":\"Caso necessário, descreva informações que sejam relevantes\"}]'),(34,'tooltips-B4','[{\"name\":\"6.3. Oferta total de lugares sentados\",\"text\":\"Informe o número de lugares sentados considerando todos os veículos\"},{\"name\":\"6.4. Categoria dos veículos\",\"text\":\"Informe os tipos dos veículos. Exemplo: ônibus, van, carro\"}]'),(35,'tooltips-B7','[{\"name\":\"2.8. Registro ou filiações - Entidade\",\"text\":\"Informe o nome da Entidade responsável pela regulação, caso exista\"},{\"name\":\"2.9. Registro ou filiações - Número\",\"text\":\"Informe o número do registro na entidade responsável pela regulação, caso exista\"},{\"name\":\"6.4. Principais atividades\",\"text\":\"Informe as principais atividades desempenhadas pelo empreendimento\"}]'),(36,'event_calendar_cover','1531756722DA2vqiEZmC.jpg'),(37,'event_calendar_header',''),(38,'event_calendar_last_page','<style type=\"text/css\">.credits{\r\n                    text-align: center;\r\n                }\r\n                .credits p {\r\n                    margin: 0 0 3px;\r\n                }\r\n</style>\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>O Calend&aacute;rio foi organizado com base em informa&ccedil;&otilde;es fornecidas &agrave; SECRETARIA ADJUNTA DE TURISMO pelas Associa&ccedil;&otilde;es dos Regi&otildees Tur&iacute;sticas, Prefeituras Municipais e/ou Organizadores e Promotores de Eventos, portanto as mesmas s&atilde;o de responsabilidade dos informantes.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n'),(39,'tooltips-C6.3','[{\"name\":\"2.1. Prato típico\",\"text\":\"Os \\\"pratos típicos\\\" são aqueles que possuem um contexto histórico, de tradição, para o município.\"}]');
/*!40000 ALTER TABLE `parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(250) NOT NULL,
  `token` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_method`
--

DROP TABLE IF EXISTS `payment_method`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome_pt` varchar(200) NOT NULL,
  `nome_en` varchar(200) DEFAULT NULL,
  `nome_es` varchar(200) DEFAULT NULL,
  `nome_fr` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_method`
--

LOCK TABLES `payment_method` WRITE;
/*!40000 ALTER TABLE `payment_method` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_method` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permanent_event`
--

DROP TABLE IF EXISTS `permanent_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permanent_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_info_id` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `descricao` varchar(300) DEFAULT NULL,
  `periodicidade` varchar(15) DEFAULT NULL,
  `periodo` varchar(55) DEFAULT NULL,
  `realizacao` varchar(10) DEFAULT NULL,
  `responsavel_realizacao` varchar(150) DEFAULT NULL,
  `tema` varchar(100) NOT NULL,
  `tipo` int(11) DEFAULT NULL,
  `municipios_participantes` text,
  `observacao` text,
  PRIMARY KEY (`id`),
  KEY `fk_permanent_event_city_info1_idx` (`city_info_id`),
  CONSTRAINT `fk_permanent_event_city_info1` FOREIGN KEY (`city_info_id`) REFERENCES `city_info` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permanent_event`
--

LOCK TABLES `permanent_event` WRITE;
/*!40000 ALTER TABLE `permanent_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `permanent_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `political_party`
--

DROP TABLE IF EXISTS `political_party`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `political_party` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abbreviation` varchar(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `political_party`
--

LOCK TABLES `political_party` WRITE;
/*!40000 ALTER TABLE `political_party` DISABLE KEYS */;
INSERT INTO `political_party` VALUES (1,'PMDB','Partido do Movimento Democrático Brasileiro'),(2,'PT','Partido dos Trabalhadores'),(3,'PTB','Partido Trabalhista Brasileiro'),(4,'PDT','Partido Democrático Trabalhista'),(5,'DEM','Democrátas'),(6,'PC do B','Partido Comunista do Brasil'),(7,'PSB','Partido Socialista Brasileiro'),(8,'PSDB','Partido da Social Democracia Brasileira'),(9,'PTC','Partido Trabalhista Cristão'),(10,'PSC','Partido Social Cristão'),(11,'PMN','Partido da Mobilização Nacional'),(12,'PRP','Partido Republicado Progressista'),(13,'PPS','Partido Popular Socialista'),(14,'PV','Partido Verde'),(15,'PT do B','Partido Trabalhista do Brasil'),(16,'PP','Partido Progressista'),(17,'PSTU','Partido Socialista dos Trabalhadores Unificados'),(18,'PCB','Partido Comunista Brasileiro'),(19,'PRTB','Partido Renovador Trabalhista Brasileiro'),(20,'PHS','Partido Humanista da Solidariedade'),(21,'PSDC','Partido Social Democrata Cristão'),(22,'PCO','Partido da Causa Operária'),(23,'PTN','Partido Trabalhista Nacional'),(24,'PSL','Partido Social Liberal'),(25,'PRB','Partido Republicano Brasileiro'),(26,'PSOL','Partido Socialismo e Liberdade'),(27,'PR','Partido da República'),(28,'PSD','Partido Social Democrático'),(29,'PPL','Partido Pátria Livre'),(30,'PEN','Partido Ecológico Nacional'),(31,'PROS','Partido Republicano da Ordem Social'),(32,'SD','Solidariedade'),(33,'NOVO','Partido Novo'),(34,'REDE','Rede Sustentabilidade'),(35,'PMB','Partido da Mulher Brasileira');
/*!40000 ALTER TABLE `political_party` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recreation_service`
--

DROP TABLE IF EXISTS `recreation_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recreation_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `tipo_atividade_cadastur` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `nome_rede` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `inicio_atividade` varchar(7) DEFAULT NULL,
  `registro_filiacao_entidade` varchar(300) DEFAULT NULL,
  `registro_filiacao_numero` int(11) DEFAULT NULL,
  `registro_cadastur` varchar(20) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `servicos_equipamentos_apoio` text,
  `principais_atividades` text,
  `formas_pagamento` text,
  `descricoes_observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `tem_cadastur` tinyint(1) NOT NULL DEFAULT '0',
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `possui_espacos_eventos` tinyint(1) NOT NULL DEFAULT '0',
  `atracao` tinyint(1) NOT NULL DEFAULT '0',
  `validade_cadastur` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_recreation_service_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city10000000000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district10000000000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type1000000` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_recreation_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recreation_service`
--

LOCK TABLES `recreation_service` WRITE;
/*!40000 ALTER TABLE `recreation_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `recreation_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `region`
--

DROP TABLE IF EXISTS `region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `fts` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `region`
--

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;
/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_cover`
--

DROP TABLE IF EXISTS `report_cover`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_cover` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  `pcover` varchar(200) DEFAULT NULL,
  `pheader` varchar(200) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_cover`
--

LOCK TABLES `report_cover` WRITE;
/*!40000 ALTER TABLE `report_cover` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_cover` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `responsible_team`
--

DROP TABLE IF EXISTS `responsible_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsible_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `responsavel` varchar(250) NOT NULL,
  `instituicao` varchar(250) NOT NULL,
  `telefone` varchar(150) NOT NULL,
  `email` varchar(200) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_responsible_team_city_idx` (`city_id`),
  CONSTRAINT `fk_responsible_team_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsible_team`
--

LOCK TABLES `responsible_team` WRITE;
/*!40000 ALTER TABLE `responsible_team` DISABLE KEYS */;
/*!40000 ALTER TABLE `responsible_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revision_status`
--

DROP TABLE IF EXISTS `revision_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revision_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revision_status`
--

LOCK TABLES `revision_status` WRITE;
/*!40000 ALTER TABLE `revision_status` DISABLE KEYS */;
INSERT INTO `revision_status` VALUES (1,'Aguardando conferência do município'),(2,'Aguardando conferência da região turística'),(3,'Aguardando aprovação da SECRETARIA ADJUNTA DE TURISMO'),(4,'Aprovado'),(5,'Rejeitado'),(6,'Cadastro vencido');
/*!40000 ALTER TABLE `revision_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revisions`
--

DROP TABLE IF EXISTS `revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revisions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `revisionable_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `old_value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `new_value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revisions`
--

LOCK TABLES `revisions` WRITE;
/*!40000 ALTER TABLE `revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `road`
--

DROP TABLE IF EXISTS `road`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `road` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(15) NOT NULL,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `road`
--

LOCK TABLES `road` WRITE;
/*!40000 ALTER TABLE `road` DISABLE KEYS */;
/*!40000 ALTER TABLE `road` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supply_type`
--

DROP TABLE IF EXISTS `supply_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `supply_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `nome` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supply_type`
--

LOCK TABLES `supply_type` WRITE;
/*!40000 ALTER TABLE `supply_type` DISABLE KEYS */;
INSERT INTO `supply_type` VALUES (1,'agua','Mananciais subterrâneas (poços, cacimbas, fontes, cisternas)'),(2,'agua','Mananciais superficiais (açudes, rios, lagoa)'),(3,'esgotamento','Fossa séptica'),(4,'esgotamento','Rede de esgoto'),(5,'energia','Rede urbana'),(6,'energia','Abastecimento próprio'),(7,'coleta-lixo','Coleta seletiva e reciclagem'),(8,'coleta-lixo','Lixões'),(9,'coleta-lixo','Aterros sanitários'),(10,'coleta-lixo','Compostagem (transformação adubo)'),(11,'coleta-lixo','Incineração de lixo'),(12,'coleta-lixo','Tratamentos especiais');
/*!40000 ALTER TABLE `supply_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tourism_action`
--

DROP TABLE IF EXISTS `tourism_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourism_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tourism_action`
--

LOCK TABLES `tourism_action` WRITE;
/*!40000 ALTER TABLE `tourism_action` DISABLE KEYS */;
INSERT INTO `tourism_action` VALUES (1,'Infraestrutura básica'),(2,'Infraestrutura turística'),(3,'Promoção');
/*!40000 ALTER TABLE `tourism_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tourism_agency_service`
--

DROP TABLE IF EXISTS `tourism_agency_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourism_agency_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `tipo_atividade_cadastur` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `nome_rede` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `registro_cadastur` varchar(20) DEFAULT NULL,
  `inicio_atividade` varchar(7) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `municipios_participantes` text,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `principal_faixa_etaria_clientes` varchar(20) NOT NULL,
  `descricoes_observacoes_complementares` text,
  `integrante_minas_recebe` tinyint(1) NOT NULL DEFAULT '0',
  `segmentos_agencia` text NOT NULL,
  `espaco_fisico_atendimento_publico` tinyint(1) NOT NULL DEFAULT '0',
  `atendimento_bilingue` tinyint(1) NOT NULL DEFAULT '0',
  `atendimento_bilingue_idiomas` text,
  `possui_informativos_impressos` tinyint(1) NOT NULL DEFAULT '0',
  `informativos_impressos_bilingue` tinyint(1) DEFAULT '0',
  `informativos_impressos_linguas` text,
  `informativos_impressos_linguas_outras` text,
  `possui_email_marketing` tinyint(1) NOT NULL DEFAULT '0',
  `possui_blog` tinyint(1) NOT NULL DEFAULT '0',
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `tem_cadastur` tinyint(1) NOT NULL DEFAULT '0',
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `validade_cadastur` datetime DEFAULT NULL,
  `atracao` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_tourism_agency_service_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city10000000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district10000000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type1000` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tourism_agency_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tourism_agency_service`
--

LOCK TABLES `tourism_agency_service` WRITE;
/*!40000 ALTER TABLE `tourism_agency_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `tourism_agency_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tourism_transportation_service`
--

DROP TABLE IF EXISTS `tourism_transportation_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tourism_transportation_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `tipo_atividade_cadastur` int(11) DEFAULT NULL,
  `revision_status_id` int(11) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `nome_fantasia` varchar(300) NOT NULL,
  `nome_juridico` varchar(300) DEFAULT NULL,
  `nome_rede` varchar(300) DEFAULT NULL,
  `cnpj` varchar(15) DEFAULT NULL,
  `inicio_atividade` varchar(7) DEFAULT NULL,
  `registro_cadastur` varchar(20) DEFAULT NULL,
  `numero_registro` varchar(20) DEFAULT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `telefone` varchar(150) NOT NULL,
  `whatsapp` varchar(15) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `localizacao` varchar(50) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `tripadvisor` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `latitude` decimal(9,7) DEFAULT NULL,
  `longitude` decimal(9,7) DEFAULT NULL,
  `abrangencia` text,
  `tipos_servico` text,
  `funcionamento_1` text NOT NULL,
  `funcionamento_2` text NOT NULL,
  `funcionamento_observacao` text,
  `quantidade_veiculos` varchar(20) NOT NULL,
  `quantidade_veiculos_adaptados` varchar(20) NOT NULL,
  `oferta_total_lugares_sentados` varchar(20) DEFAULT NULL,
  `categoria_veiculos` text,
  `descricoes_observacoes_complementares` text,
  `acessibilidade` text,
  `equipe_responsavel_responsavel` varchar(250) NOT NULL,
  `equipe_responsavel_instituicao` varchar(250) NOT NULL,
  `equipe_responsavel_telefone` varchar(150) NOT NULL,
  `equipe_responsavel_email` varchar(200) NOT NULL,
  `equipe_responsavel_observacao` text,
  `equipamento_fechado` tinyint(1) DEFAULT NULL,
  `equipamento_fechado_motivo` varchar(40) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `comentario_revisao` text,
  `tem_cadastur` tinyint(1) NOT NULL DEFAULT '0',
  `descricao_curta` text,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `hashtags` text,
  `data_publicacao` datetime DEFAULT NULL,
  `tipos_viagem` varchar(250) DEFAULT NULL,
  `validade_cadastur` datetime DEFAULT NULL,
  `atracao` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_city_access_district1_idx` (`district_id`),
  KEY `fk_city_access_city1_idx` (`city_id`),
  KEY `fk_city_other_services_type1_idx` (`type_id`),
  KEY `fk_tourism_transportation_service_revision_status1_idx` (`revision_status_id`),
  CONSTRAINT `fk_city_access_city100000000` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_access_district100000000` FOREIGN KEY (`district_id`) REFERENCES `district` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_city_other_services_type10000` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tourism_transportation_service_revision_status1` FOREIGN KEY (`revision_status_id`) REFERENCES `revision_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPRESSED KEY_BLOCK_SIZE=8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tourism_transportation_service`
--

LOCK TABLES `tourism_transportation_service` WRITE;
/*!40000 ALTER TABLE `tourism_transportation_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `tourism_transportation_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `touristic_circuit`
--

DROP TABLE IF EXISTS `touristic_circuit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `touristic_circuit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(200) NOT NULL,
  `nome_oficial` varchar(200) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `situacao_certificacao` varchar(250) DEFAULT NULL,
  `observacao` text,
  `city_id` int(11) NOT NULL,
  `cep` varchar(11) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `logradouro` varchar(200) NOT NULL,
  `numero` varchar(50) DEFAULT NULL,
  `complemento` varchar(50) DEFAULT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `site` varchar(300) DEFAULT NULL,
  `contato_presidente` tinyint(1) NOT NULL DEFAULT '0',
  `contato_presidente_nome` varchar(150) DEFAULT NULL,
  `contato_presidente_telefone` varchar(150) DEFAULT NULL,
  `contato_presidente_email` varchar(200) DEFAULT NULL,
  `contato_gestor` tinyint(1) NOT NULL DEFAULT '0',
  `contato_gestor_nome` varchar(150) DEFAULT NULL,
  `contato_gestor_telefone` varchar(150) DEFAULT NULL,
  `contato_gestor_email` varchar(200) DEFAULT NULL,
  `contato_assessor` tinyint(1) NOT NULL DEFAULT '0',
  `contato_assessor_nome` varchar(150) DEFAULT NULL,
  `contato_assessor_telefone` varchar(150) DEFAULT NULL,
  `contato_assessor_email` varchar(200) DEFAULT NULL,
  `contato_secretaria` tinyint(1) NOT NULL DEFAULT '0',
  `contato_secretaria_nome` varchar(150) DEFAULT NULL,
  `contato_secretaria_telefone` varchar(150) DEFAULT NULL,
  `contato_secretaria_email` varchar(200) DEFAULT NULL,
  `contato_tesoureiro` tinyint(1) NOT NULL DEFAULT '0',
  `contato_tesoureiro_nome` varchar(150) DEFAULT NULL,
  `contato_tesoureiro_telefone` varchar(150) DEFAULT NULL,
  `contato_tesoureiro_email` varchar(200) DEFAULT NULL,
  `historico` text,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `responsavel_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_touristic_circuit_city1_idx` (`city_id`),
  KEY `fk_touristic_circuit_user1_idx` (`updated_by`),
  KEY `fk_touristic_circuit_user2_idx` (`created_by`),
  KEY `fk_touristic_circuit_user3_idx` (`responsavel_id`),
  CONSTRAINT `fk_touristic_circuit_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_touristic_circuit_user1` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_touristic_circuit_user2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_touristic_circuit_user3` FOREIGN KEY (`responsavel_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `touristic_circuit`
--

LOCK TABLES `touristic_circuit` WRITE;
/*!40000 ALTER TABLE `touristic_circuit` DISABLE KEYS */;
/*!40000 ALTER TABLE `touristic_circuit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `touristic_circuit_cities`
--

DROP TABLE IF EXISTS `touristic_circuit_cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `touristic_circuit_cities` (
  `touristic_circuit_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`touristic_circuit_id`,`city_id`),
  KEY `fk_touristic_circuit_has_city_city1_idx` (`city_id`),
  KEY `fk_touristic_circuit_has_city_touristic_circuit1_idx` (`touristic_circuit_id`),
  CONSTRAINT `fk_touristic_circuit_has_city_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_touristic_circuit_has_city_touristic_circuit1` FOREIGN KEY (`touristic_circuit_id`) REFERENCES `touristic_circuit` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `touristic_circuit_cities`
--

LOCK TABLES `touristic_circuit_cities` WRITE;
/*!40000 ALTER TABLE `touristic_circuit_cities` DISABLE KEYS */;
/*!40000 ALTER TABLE `touristic_circuit_cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `touristic_route`
--

DROP TABLE IF EXISTS `touristic_route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `touristic_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `nome_pt` varchar(300) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `slug` varchar(80) NOT NULL,
  `dias_duracao` int(11) NOT NULL,
  `telefone` varchar(150) DEFAULT NULL,
  `site` varchar(2083) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `instagram` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `youtube` varchar(200) DEFAULT NULL,
  `flickr` varchar(200) DEFAULT NULL,
  `minas_360` varchar(200) DEFAULT NULL,
  `nome` text,
  `descricao_curta` text,
  `descricao` text,
  `depoimento` text,
  `depoimento_autor` text,
  `tipo_roteiro` varchar(140) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `touristic_route`
--

LOCK TABLES `touristic_route` WRITE;
/*!40000 ALTER TABLE `touristic_route` DISABLE KEYS */;
/*!40000 ALTER TABLE `touristic_route` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `touristic_route_day`
--

DROP TABLE IF EXISTS `touristic_route_day`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `touristic_route_day` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `touristic_route_id` int(11) NOT NULL,
  `dia` int(11) NOT NULL,
  `descricao` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_touristic_route_language_touristic_route1_idx` (`touristic_route_id`),
  CONSTRAINT `fk_touristic_route_day_touristic_route1` FOREIGN KEY (`touristic_route_id`) REFERENCES `touristic_route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `touristic_route_day`
--

LOCK TABLES `touristic_route_day` WRITE;
/*!40000 ALTER TABLE `touristic_route_day` DISABLE KEYS */;
/*!40000 ALTER TABLE `touristic_route_day` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `touristic_route_destinations`
--

DROP TABLE IF EXISTS `touristic_route_destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `touristic_route_destinations` (
  `touristic_route_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  PRIMARY KEY (`touristic_route_id`,`destination_id`),
  KEY `fk_touristic_route_has_destination_destination1_idx` (`destination_id`),
  KEY `fk_touristic_route_has_destination_touristic_route1_idx` (`touristic_route_id`),
  CONSTRAINT `fk_touristic_route_has_destination_destination1` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_touristic_route_has_destination_touristic_route1` FOREIGN KEY (`touristic_route_id`) REFERENCES `touristic_route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `touristic_route_destinations`
--

LOCK TABLES `touristic_route_destinations` WRITE;
/*!40000 ALTER TABLE `touristic_route_destinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `touristic_route_destinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `touristic_route_hashtag`
--

DROP TABLE IF EXISTS `touristic_route_hashtag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `touristic_route_hashtag` (
  `hashtag_id` int(11) NOT NULL,
  `touristic_route_id` int(11) NOT NULL,
  PRIMARY KEY (`hashtag_id`,`touristic_route_id`),
  KEY `fk_hashtag_has_destination_hashtag1_idx` (`hashtag_id`),
  KEY `fk_touristic_route_hashtag_touristic_route1_idx` (`touristic_route_id`),
  CONSTRAINT `fk_hashtag_has_destination_hashtag10` FOREIGN KEY (`hashtag_id`) REFERENCES `hashtag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_touristic_route_hashtag_touristic_route1` FOREIGN KEY (`touristic_route_id`) REFERENCES `touristic_route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `touristic_route_hashtag`
--

LOCK TABLES `touristic_route_hashtag` WRITE;
/*!40000 ALTER TABLE `touristic_route_hashtag` DISABLE KEYS */;
/*!40000 ALTER TABLE `touristic_route_hashtag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `touristic_route_photo`
--

DROP TABLE IF EXISTS `touristic_route_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `touristic_route_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `touristic_route_id` int(11) NOT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0',
  `is_map` tinyint(1) NOT NULL DEFAULT '0',
  `is_testimonial` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(2083) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_touristic_route_photo_touristic_route1_idx` (`touristic_route_id`),
  CONSTRAINT `fk_touristic_route_photo_touristic_route1` FOREIGN KEY (`touristic_route_id`) REFERENCES `touristic_route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `touristic_route_photo`
--

LOCK TABLES `touristic_route_photo` WRITE;
/*!40000 ALTER TABLE `touristic_route_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `touristic_route_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_category`
--

DROP TABLE IF EXISTS `trip_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `foto_capa_url` varchar(2083) NOT NULL,
  `slug` varchar(80) NOT NULL,
  `nome_pt` varchar(200) NOT NULL,
  `nome` text,
  `descricao_curta` text,
  `descricao` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_category`
--

LOCK TABLES `trip_category` WRITE;
/*!40000 ALTER TABLE `trip_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_destinations`
--

DROP TABLE IF EXISTS `trip_destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_destinations` (
  `trip_type_id` int(11) NOT NULL,
  `destination_id` int(11) NOT NULL,
  PRIMARY KEY (`trip_type_id`,`destination_id`),
  KEY `fk_trip_type_has_destination_destination1_idx` (`destination_id`),
  KEY `fk_trip_type_has_destination_trip_type1_idx` (`trip_type_id`),
  CONSTRAINT `fk_trip_type_has_destination_destination1` FOREIGN KEY (`destination_id`) REFERENCES `destination` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_trip_type_has_destination_trip_type1` FOREIGN KEY (`trip_type_id`) REFERENCES `trip_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_destinations`
--

LOCK TABLES `trip_destinations` WRITE;
/*!40000 ALTER TABLE `trip_destinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_destinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_touristic_routes`
--

DROP TABLE IF EXISTS `trip_touristic_routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_touristic_routes` (
  `trip_type_id` int(11) NOT NULL,
  `touristic_route_id` int(11) NOT NULL,
  PRIMARY KEY (`trip_type_id`,`touristic_route_id`),
  KEY `fk_trip_type_has_touristic_route_touristic_route1_idx` (`touristic_route_id`),
  KEY `fk_trip_type_has_touristic_route_trip_type1_idx` (`trip_type_id`),
  CONSTRAINT `fk_trip_type_has_touristic_route_touristic_route1` FOREIGN KEY (`touristic_route_id`) REFERENCES `touristic_route` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_trip_type_has_touristic_route_trip_type1` FOREIGN KEY (`trip_type_id`) REFERENCES `trip_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_touristic_routes`
--

LOCK TABLES `trip_touristic_routes` WRITE;
/*!40000 ALTER TABLE `trip_touristic_routes` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_touristic_routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_type`
--

DROP TABLE IF EXISTS `trip_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trip_category_id` int(11) NOT NULL,
  `publico` tinyint(1) NOT NULL DEFAULT '1',
  `destaque` tinyint(1) NOT NULL DEFAULT '0',
  `foto_capa_url` varchar(2083) NOT NULL,
  `slug` varchar(80) NOT NULL,
  `nome_pt` varchar(200) NOT NULL,
  `nome` text,
  `descricao_curta` text,
  `descricao` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_UNIQUE` (`slug`),
  KEY `fk_attraction_type_trip_category1_idx` (`trip_category_id`),
  KEY `slug` (`slug`),
  CONSTRAINT `fk_attraction_type_trip_category1` FOREIGN KEY (`trip_category_id`) REFERENCES `trip_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_type`
--

LOCK TABLES `trip_type` WRITE;
/*!40000 ALTER TABLE `trip_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_type_attractions`
--

DROP TABLE IF EXISTS `trip_type_attractions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_type_attractions` (
  `trip_type_id` int(11) NOT NULL,
  `attraction_id` int(11) NOT NULL,
  PRIMARY KEY (`trip_type_id`,`attraction_id`),
  KEY `fk_attraction_has_trip_type_trip_type1_idx` (`trip_type_id`),
  KEY `fk_trip_attractions_attraction1_idx` (`attraction_id`),
  CONSTRAINT `fk_attraction_has_trip_type_trip_type1` FOREIGN KEY (`trip_type_id`) REFERENCES `trip_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_trip_attractions_attraction1` FOREIGN KEY (`attraction_id`) REFERENCES `attraction` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_type_attractions`
--

LOCK TABLES `trip_type_attractions` WRITE;
/*!40000 ALTER TABLE `trip_type_attractions` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_type_attractions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trip_type_events`
--

DROP TABLE IF EXISTS `trip_type_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trip_type_events` (
  `trip_type_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`trip_type_id`,`event_id`),
  KEY `fk_trip_type_has_event_event1_idx` (`event_id`),
  KEY `fk_trip_type_has_event_trip_type1_idx` (`trip_type_id`),
  CONSTRAINT `fk_trip_type_has_event_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_trip_type_has_event_trip_type1` FOREIGN KEY (`trip_type_id`) REFERENCES `trip_type` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trip_type_events`
--

LOCK TABLES `trip_type_events` WRITE;
/*!40000 ALTER TABLE `trip_type_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `trip_type_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_item_id` int(11) NOT NULL,
  `super_type_id` int(11) DEFAULT NULL,
  `name` varchar(200) NOT NULL,
  `name_en` varchar(200) DEFAULT NULL,
  `name_es` varchar(200) DEFAULT NULL,
  `name_fr` varchar(200) DEFAULT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `trade` varchar(250) DEFAULT NULL,
  `sinonimos` text,
  `excluir` text,
  PRIMARY KEY (`id`),
  KEY `fk_type_type1_idx` (`super_type_id`),
  KEY `fk_type_inventory_item1_idx` (`inventory_item_id`),
  FULLTEXT KEY `fts_exclude` (`excluir`),
  FULLTEXT KEY `fts_search` (`slug`,`trade`,`sinonimos`),
  CONSTRAINT `fk_type_inventory_item1` FOREIGN KEY (`inventory_item_id`) REFERENCES `inventory_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_type_type1` FOREIGN KEY (`super_type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=297 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type`
--

LOCK TABLES `type` WRITE;
/*!40000 ALTER TABLE `type` DISABLE KEYS */;
INSERT INTO `type` VALUES (1,1,NULL,'Terrestres',NULL,NULL,NULL,'acesso-terrestre','Rodovia','Rodovia, Ferrovia',NULL),(2,1,1,'Terminais/estações rodoviárias/serviços rodoviários',NULL,NULL,NULL,'rodoviaria','Rodoviária','Passagens, Onibus',NULL),(3,1,1,'Terminais/estações ferroviárias',NULL,NULL,NULL,'estacao','Estação','Ferrovia',NULL),(4,1,NULL,'Aéreos',NULL,NULL,NULL,'aeroporto','Aeroporto','',NULL),(5,1,4,'Aeroportos/serviços aéreos',NULL,NULL,NULL,'aeroporto','Aeroporto','',NULL),(6,1,NULL,'Hidroviários',NULL,NULL,NULL,'porto','Porto','',NULL),(7,1,6,'Portos/estações/serviços marítimos',NULL,NULL,NULL,'porto','Porto','',NULL),(8,1,6,'Portos/estações/serviços fluviais/lacustres',NULL,NULL,NULL,'porto','Porto','',NULL),(9,2,NULL,'Delegacia/Postos de polícia',NULL,NULL,NULL,'delegacia','Delegacia','',NULL),(10,2,NULL,'Postos de polícia rodoviária',NULL,NULL,NULL,'policia-rodoviaria','Posto de polícia rodoviária','',NULL),(11,2,NULL,'Corpo de bombeiros',NULL,NULL,NULL,'corpo-de-bombeiros','Corpo de bombeiros','',NULL),(12,2,NULL,'Serviços de busca e salvamento',NULL,NULL,NULL,'resgate','Serviço de busca e salvamento','',NULL),(13,2,NULL,'Serviços de busca fluvial/aérea/de fronteiras',NULL,NULL,NULL,'resgate','Serviço de busca marítma/aérea/de fronteiras','',NULL),(14,3,NULL,'Prontos-socorros',NULL,NULL,NULL,'pronto-socorro','Pronto-socorro','Hospital, Médico',NULL),(15,3,NULL,'Hospitais',NULL,NULL,NULL,'hospital','Hospital','',NULL),(16,3,NULL,'Clínicas médicas',NULL,NULL,NULL,'clinica-medica','Clínica médica','Médico',NULL),(17,3,NULL,'Maternidades',NULL,NULL,NULL,'maternidade','Maternidade','Médico',NULL),(18,3,NULL,'Postos de saúde',NULL,NULL,NULL,'posto-de-saude','Posto de saúde','',NULL),(19,3,NULL,'Farmácias/Drogarias',NULL,NULL,NULL,'farmacia','Farmácia/Drogaria','',NULL),(20,3,NULL,'Clínicas odontológicas',NULL,NULL,NULL,'dentista','Dentista','Clínica odontológica',NULL),(21,4,NULL,'Locadoras de imóveis',NULL,NULL,NULL,'aluguel-imoveis','Locadora de imóveis','',NULL),(22,4,NULL,'Locadoras de automóveis/embarcações/aeronaves',NULL,NULL,NULL,'aluguel-veiculos','Locadora de automóveis/embarcações/aeronaves','',NULL),(23,4,NULL,'Comércio',NULL,NULL,NULL,'comercio','Comércio','Loja',NULL),(24,4,23,'Lojas de artesanato/suvenires',NULL,NULL,NULL,'artesanato','Loja de artesanato/suvenires','Loja',NULL),(25,4,23,'Centros comerciais',NULL,NULL,NULL,'shopping','Centros comerciais','Loja',NULL),(26,4,23,'Galerias de arte/antiguidades',NULL,NULL,NULL,'galeria-de-arte','Galerias de arte/antiguidades','Loja',NULL),(27,4,23,'Lojas de artigos fotográficos',NULL,NULL,NULL,'fotografia','Lojas de artigos fotográficos','Loja',NULL),(28,4,NULL,'Agências bancárias/Casas de câmbio',NULL,NULL,NULL,'banco','Agências bancárias/Casas de câmbio','',NULL),(29,4,NULL,'Serviços mecânicos',NULL,NULL,NULL,'mecanico','Serviços mecânicos','',NULL),(30,4,NULL,'Postos de abastecimento',NULL,NULL,NULL,'posto-de-combustível','Posto de Combustível','Posto de Gasolina',NULL),(31,4,NULL,'Locais/Templos de manifestação de fé',NULL,NULL,NULL,'igreja','Igreja','',NULL),(32,4,NULL,'Representações diplomáticas',NULL,NULL,NULL,'embaixada','Representações diplomáticas','Consulado',NULL),(33,4,32,'Embaixadas',NULL,NULL,NULL,'embaixada','Embaixadas','',NULL),(34,4,32,'Consulados',NULL,NULL,NULL,'embaixada','Consulados','',NULL),(35,4,32,'Escritórios comerciais',NULL,NULL,NULL,'escritorio','Escritórios comerciais','',NULL),(36,4,32,'Outras representações',NULL,NULL,NULL,'representantes','Outras representações','',NULL),(37,5,NULL,'Meios de hospedagem com necessidade de cadastro',NULL,NULL,NULL,'hotel','Hotel','',NULL),(38,5,37,'Hotel',NULL,NULL,NULL,'hotel','Hotel','',NULL),(39,5,37,'Hotel histórico',NULL,NULL,NULL,'hotel','Hotel histórico','',NULL),(40,5,37,'Hotel de Lazer/Resort',NULL,NULL,NULL,'hotel','Hotel de Lazer/Resort','',NULL),(41,5,37,'Pousada',NULL,NULL,NULL,'pousada','Pousada','',NULL),(42,5,37,'Hotel de Selva/Lodge',NULL,NULL,NULL,'hotel','Hotel de Selva/Lodge','',NULL),(43,5,37,'Apart-hotel/Flat/Condohotel',NULL,NULL,NULL,'hotel','Apart-hotel/Flat/Condohotel','',NULL),(46,5,NULL,'Meios de hospedagem sem necessidade de cadastro',NULL,NULL,NULL,'hotel','Meios de hospedagem sem necessidade de cadastro','',NULL),(47,5,46,'Hospedaria',NULL,NULL,NULL,'hotel','Hospedaria','',NULL),(48,5,46,'Pensão',NULL,NULL,NULL,'hotel','Pensão','',NULL),(49,5,46,'Motel',NULL,NULL,NULL,'hotel','Motel','',NULL),(50,5,NULL,'Meios de hospedagem extra-hoteleiros',NULL,NULL,NULL,'hotel','Hotel','',NULL),(51,5,50,'Camping','','','','hotel','Camping',' Acampamento',' '),(52,5,50,'Colônia de Férias',NULL,NULL,NULL,'hotel','Colônia de Férias','',NULL),(53,5,50,'Albergue/Hostel',NULL,NULL,NULL,'hotel','Albergue/Hostel','',NULL),(54,5,NULL,'Outros meios de hospedagem',NULL,NULL,NULL,'hotel','Outros meios de hospedagem','',NULL),(55,6,NULL,'Restaurantes',NULL,NULL,NULL,'gastronomia','Restaurantes','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(56,6,NULL,'Bares/Choperias/Lanchonetes','','','','gastronomia','Bares/Choperias/Lanchonetes','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(57,6,NULL,'Casas de chá/Cafés/Padarias/Confeitarias','','','','gastronomia','chá/Cafés/Padarias/Confeitarias','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos  ','hotel '),(58,6,NULL,'Cervejarias',NULL,NULL,NULL,'gastronomia','Cervejarias','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(59,6,NULL,'Quiosques/Barracas',NULL,NULL,NULL,'gastronomia','Quiosques/Barracas','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(60,6,NULL,'Sorveterias',NULL,NULL,NULL,'gastronomia','Sorveterias','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(61,6,NULL,'Casas de sucos',NULL,NULL,NULL,'gastronomia','Casas de sucos','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(62,6,NULL,'Outros',NULL,NULL,NULL,'gastronomia','Outros','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(63,7,NULL,'Agência de viagem',NULL,NULL,NULL,'agencia-viagem','Agência de viagem','',NULL),(64,7,NULL,'Agência de receptivo',NULL,NULL,NULL,'agencia-receptivo','Agência de receptivo','',NULL),(65,8,NULL,'Transportadoras turísticas',NULL,NULL,NULL,'transporte-turistico','Transportadoras turísticas','',NULL),(66,8,NULL,'Locadoras',NULL,NULL,NULL,'transporte-turistico','Locadoras','',NULL),(67,8,NULL,'Taxi','','','','transporte-turistico','Taxi','',NULL),(68,8,NULL,'Outros',NULL,NULL,NULL,'transporte-turistico','Outros','',NULL),(69,9,NULL,'Centros de convenções','','','','organizacao-eventos','Centros de convenções','',NULL),(70,9,NULL,'Parques de exposição/pavilhão','','','','organizacao-eventos','Parques de exposição/pavilhão','',NULL),(71,9,NULL,'Auditórios','','','','organizacao-eventos','Auditórios','',NULL),(72,9,NULL,'Empresas organizadoras/Promotoras de eventos',NULL,NULL,NULL,'organizacao-eventos','Empresas organizadoras/Promotoras de eventos','',NULL),(73,9,NULL,'Outros serviços especializados','','','','organizacao-eventos','Outros serviços especializados','',NULL),(74,10,NULL,'Parques de diversões','','','','lazer','Parques de diversões','',NULL),(75,10,NULL,'Parques/Jardins/Praças','','','','lazer','Jardins/Praças',' ',' '),(76,10,NULL,'Clubes',NULL,NULL,NULL,'lazer','Clubes','',NULL),(77,10,NULL,'Pistas de patinação/Motocross/Bicicross',NULL,NULL,NULL,'lazer','Pistas de patinação/Motocross/Bicicross','',NULL),(78,10,NULL,'Estádios/Ginásios/Quadras',NULL,NULL,NULL,'lazer','Estádios/Ginásios/Quadras','',NULL),(79,10,NULL,'Hipódromos/Autódromos/Kartódromos',NULL,NULL,NULL,'lazer','Hipódromos/Autódromos/Kartódromos','',NULL),(80,10,NULL,'Marinas/Atracadouros',NULL,NULL,NULL,'lazer','Marinas/Atracadouros','',NULL),(81,10,NULL,'Mirantes/Belvederes',NULL,NULL,NULL,'lazer','Mirantes/Belvederes','',NULL),(82,10,NULL,'Prestadores de serviços de lazer e entretenimento',NULL,NULL,NULL,'lazer','Prestadores de serviços de lazer e entretenimento','',NULL),(83,10,NULL,'Boates/Discotecas',NULL,NULL,NULL,'lazer','Boates/Discotecas','',NULL),(84,10,NULL,'Casas de espetáculos',NULL,NULL,NULL,'lazer','Casas de espetáculos','',NULL),(85,10,NULL,'Casas de danças',NULL,NULL,NULL,'lazer','Casas de danças','',NULL),(86,10,NULL,'Cinemas',NULL,NULL,NULL,'lazer','Cinemas','',NULL),(87,10,NULL,'Pistas de boliche/Campos de golfe',NULL,NULL,NULL,'lazer','Pistas de boliche/Campos de golfe','',NULL),(88,10,NULL,'Parques agropecuários','','','','lazer','Parques agropecuários','',NULL),(89,10,NULL,'Outros locais',NULL,NULL,NULL,'lazer','Outros locais','',NULL),(90,11,NULL,'Informações turísticas',NULL,NULL,NULL,'atendimento-turista','Informações turísticas','',NULL),(91,11,90,'Centro de atendimento ao turista',NULL,NULL,NULL,'atendimento-turista','Centro de atendimento ao turista','',NULL),(92,11,NULL,'Entidades',NULL,NULL,NULL,'atendimento-turista','Entidades','',NULL),(93,11,NULL,'Outros',NULL,NULL,NULL,'atendimento-turista','Outros','',NULL),(94,12,NULL,'Montanhas',NULL,NULL,NULL,'natureza','Montanhas','',NULL),(95,12,94,'Picos/Cumes',NULL,NULL,NULL,'natureza','Picos/Cumes','',NULL),(96,12,94,'Serras',NULL,NULL,NULL,'natureza','Serras','',NULL),(97,12,94,'Montes/morros/colinas',NULL,NULL,NULL,'natureza','Montes/morros/colinas','',NULL),(98,12,NULL,'Planaltos e planícies',NULL,NULL,NULL,'natureza','Planaltos e planícies','',NULL),(99,12,98,'Chapadas/tabuleiros',NULL,NULL,NULL,'natureza','Chapadas/tabuleiros','',NULL),(100,12,98,'Patamares',NULL,NULL,NULL,'natureza','Patamares','',NULL),(101,12,98,'Pedras tabulares/matações',NULL,NULL,NULL,'natureza','Pedras tabulares/matações','',NULL),(102,12,98,'Vales',NULL,NULL,NULL,'natureza','Vales','',NULL),(103,12,98,'Rochedos',NULL,NULL,NULL,'natureza','Rochedos','',NULL),(117,12,NULL,'Hidrografia',NULL,NULL,NULL,'rio','Hidrografia','',NULL),(118,12,117,'Rios',NULL,NULL,NULL,'rio','Rios','',NULL),(119,12,117,'Lagos/lagoas',NULL,NULL,NULL,'lagoa','Lagos/lagoas','',NULL),(120,12,117,' Praias fluviais/lacustres',NULL,NULL,NULL,'praia',' Praias fluviais/lacustres','',NULL),(121,12,117,'Alagados',NULL,NULL,NULL,'lagoa','Alagados','',NULL),(122,12,NULL,'Quedas d\'água',NULL,NULL,NULL,'cachoeira','Quedas d\'água','',NULL),(123,12,122,'Catarata',NULL,NULL,NULL,'cachoeira','Catarata','',NULL),(124,12,122,'Cachoeira',NULL,NULL,NULL,'cachoeira','Cachoeira','',NULL),(125,12,122,'Salto',NULL,NULL,NULL,'cachoeira','Salto','',NULL),(126,12,122,'Cascata',NULL,NULL,NULL,'cachoeira','Cascata','',NULL),(127,12,122,'Corredeira',NULL,NULL,NULL,'cachoeira','Corredeira','',NULL),(128,12,NULL,'Fontes hidrominerais e/ou termais',NULL,NULL,NULL,'fonte','Fontes hidrominerais e/ou termais','',NULL),(129,12,NULL,'Unidades de conservação','','','','parques','Parques',' Unidades de conservação  Unidade Parque Parques  ','     '),(130,12,129,'Nacionais','','','','parque','Parques Nacionais','Parque Parques  ','   '),(131,12,129,'Estaduais','','','','parque','Parques Estaduais','Parque Parques     ','     '),(132,12,129,'Municipais','','','','parque','Parques Municipais','Parque Parques   ','   '),(133,12,129,'Particulares','','','','parque','Parques','Parque Parques Particulares ','  '),(134,12,NULL,'Cavernas, grutas e furnas',NULL,NULL,NULL,'natureza','Cavernas, grutas e furnas','',NULL),(135,12,NULL,'Áreas de caça e pesca',NULL,NULL,NULL,'natureza','Áreas de caça e pesca','',NULL),(136,12,NULL,'Flora',NULL,NULL,NULL,'natureza','Flora','',NULL),(137,12,NULL,'Fauna',NULL,NULL,NULL,'natureza','Fauna','',NULL),(138,12,NULL,'Outros',NULL,NULL,NULL,'natureza','Outros','',NULL),(139,13,NULL,'Sítios históricos',NULL,NULL,NULL,'historia','Sítios históricos','',NULL),(140,13,139,'Centro histórico',NULL,NULL,NULL,'historia','Centro histórico','',NULL),(141,13,139,'Cidade histórica',NULL,NULL,NULL,'historia','Cidade histórica','',NULL),(142,13,139,'Conjunto histórico',NULL,NULL,NULL,'historia','Conjunto histórico','',NULL),(143,13,139,'Quilombo',NULL,NULL,NULL,'historia','Quilombo','',NULL),(144,13,139,'Terra indígena',NULL,NULL,NULL,'historia','Terra indígena','',NULL),(145,13,139,'Conjunto paisagístico',NULL,NULL,NULL,'historia','Conjunto paisagístico','',NULL),(146,13,139,'Monumento histórico',NULL,NULL,NULL,'historia','Monumento histórico','',NULL),(147,13,139,'Sítio arqueológico',NULL,NULL,NULL,'historia','Sítio arqueológico','',NULL),(148,13,139,'Sítio paleontológico',NULL,NULL,NULL,'historia','Sítio paleontológico','',NULL),(149,13,139,'Jardim histórico',NULL,NULL,NULL,'historia','Jardim histórico','',NULL),(150,13,NULL,'Edificações',NULL,NULL,NULL,'arquitetura','Edificações','',NULL),(151,13,150,'Arquitetura civil',NULL,NULL,NULL,'arquitetura','Arquitetura civil','',NULL),(152,13,150,'Arquitetura militar',NULL,NULL,NULL,'arquitetura','Arquitetura militar','',NULL),(153,13,150,'Arquitetura religiosa',NULL,NULL,NULL,'arquitetura','Arquitetura religiosa','',NULL),(154,13,150,'Arquitetura industrial/agrícola',NULL,NULL,NULL,'arquitetura','Arquitetura industrial/agrícola','',NULL),(155,13,150,'Arquitetura vernacular',NULL,NULL,NULL,'arquitetura','Arquitetura vernacular','',NULL),(156,13,150,'Arquitetura funerária',NULL,NULL,NULL,'arquitetura','Arquitetura funerária','',NULL),(157,13,150,'Ruínas',NULL,NULL,NULL,'arquitetura','Ruínas','',NULL),(158,13,NULL,'Obras de arte',NULL,NULL,NULL,'arte','Obras de arte','',NULL),(159,13,158,'Escultura/estatuária/monumento/obelisco',NULL,NULL,NULL,'arte','Escultura/estatuária/monumento/obelisco','',NULL),(160,13,158,'Pintura',NULL,NULL,NULL,'arte','Pintura','',NULL),(161,13,158,'Murais',NULL,NULL,NULL,'arte','Murais','',NULL),(162,13,158,'Vitrais',NULL,NULL,NULL,'arte','Vitrais','',NULL),(163,13,158,'Azulejaria',NULL,NULL,NULL,'arte','Azulejaria','',NULL),(164,13,158,'Outros legados',NULL,NULL,NULL,'arte','Outros legados','',NULL),(165,13,NULL,'Instituições culturais',NULL,NULL,NULL,'cultura','Instituições culturais','',NULL),(166,13,165,'Museu/memorial',NULL,NULL,NULL,'cultura','Museu/memorial','',NULL),(167,13,165,'Biblioteca',NULL,NULL,NULL,'cultura','Biblioteca','',NULL),(168,13,165,'Arquivo',NULL,NULL,NULL,'cultura','Arquivo','',NULL),(169,13,165,'Instituto histórico e geográfico',NULL,NULL,NULL,'cultura','Instituto histórico e geográfico','',NULL),(170,13,165,' Centro cultural/casa de cultura',NULL,NULL,NULL,'cultura',' Centro cultural/casa de cultura','',NULL),(171,13,165,'Teatro/anfiteatro',NULL,NULL,NULL,'cultura','Teatro/anfiteatro','',NULL),(172,13,NULL,'Festas e celebrações',NULL,NULL,NULL,'cultura','Festas e celebrações','',NULL),(173,13,172,'Religiosas/de manifestações de fé',NULL,NULL,NULL,'cultura','Religiosas/de manifestações de fé','',NULL),(174,13,172,'Populares/folclóricas',NULL,NULL,NULL,'cultura','Populares/folclóricas','',NULL),(175,13,172,'Cívicas',NULL,NULL,NULL,'cultura','Cívicas','',NULL),(176,13,NULL,'Gastronomia típica',NULL,NULL,NULL,'gastronomia','Gastronomia típica','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(177,13,176,'Pratos típicos',NULL,NULL,NULL,'gastronomia','Pratos típicos','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(178,13,176,'Iguarias regionais/doces/salgados',NULL,NULL,NULL,'gastronomia','Iguarias regionais/doces/salgados','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(179,13,176,'Frutas',NULL,NULL,NULL,'gastronomia','Frutas','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(180,13,176,'Bebidas',NULL,NULL,NULL,'gastronomia','Bebidas','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(181,13,176,'Outros',NULL,NULL,NULL,'gastronomia','Outros','restaurante lanchonete bar cervejaria quiosque barraca alimentação sorveteria sucos ','hotel'),(182,13,NULL,'Artesanatos',NULL,NULL,NULL,'artesanato','Artesanatos','',NULL),(183,13,182,'Cerâmica',NULL,NULL,NULL,'artesanato','Cerâmica','',NULL),(184,13,182,'Cestaria',NULL,NULL,NULL,'artesanato','Cestaria','',NULL),(185,13,182,'Madeira',NULL,NULL,NULL,'artesanato','Madeira','',NULL),(186,13,182,'Tecelagem',NULL,NULL,NULL,'artesanato','Tecelagem','',NULL),(187,13,182,'Bordados',NULL,NULL,NULL,'artesanato','Bordados','',NULL),(188,13,182,'Metal',NULL,NULL,NULL,'artesanato','Metal','',NULL),(189,13,182,'Pedra',NULL,NULL,NULL,'artesanato','Pedra','',NULL),(190,13,182,'Renda',NULL,NULL,NULL,'artesanato','Renda','',NULL),(191,13,182,'Couro',NULL,NULL,NULL,'artesanato','Couro','',NULL),(192,13,182,'Plumaria',NULL,NULL,NULL,'artesanato','Plumaria','',NULL),(193,13,NULL,'Músicas e danças',NULL,NULL,NULL,'cultura','Músicas e danças','',NULL),(194,13,193,'Banda e conjunto musical',NULL,NULL,NULL,'cultura','Banda e conjunto musical','',NULL),(195,13,193,'Salão de dança',NULL,NULL,NULL,'cultura','Salão de dança','',NULL),(196,13,193,'Clube/casa de shows',NULL,NULL,NULL,'cultura','Clube/casa de shows','',NULL),(197,13,193,'Festival',NULL,NULL,NULL,'cultura','Festival','',NULL),(198,13,193,'Folguedos',NULL,NULL,NULL,'cultura','Folguedos','',NULL),(199,13,193,'Centro de tradição',NULL,NULL,NULL,'cultura','Centro de tradição','',NULL),(200,13,193,'Outros',NULL,NULL,NULL,'cultura','Outros','',NULL),(201,13,NULL,'Feiras e mercados',NULL,NULL,NULL,'compras','Feiras e mercados','',NULL),(202,13,201,'Feira agrícola',NULL,NULL,NULL,'compras','Feira agrícola','',NULL),(203,13,201,'Feira pecuária',NULL,NULL,NULL,'compras','Feira pecuária','',NULL),(204,13,201,'Feira livre',NULL,NULL,NULL,'compras','Feira livre','',NULL),(205,13,201,'Feira de turismo',NULL,NULL,NULL,'compras','Feira de turismo','',NULL),(206,13,201,'Outras feiras',NULL,NULL,NULL,'compras','Outras feiras','',NULL),(207,13,201,'Mercado livre',NULL,NULL,NULL,'compras','Mercado livre','',NULL),(208,13,201,'Mercado de carne',NULL,NULL,NULL,'compras','Mercado de carne','',NULL),(209,13,201,'Mercado de frutas',NULL,NULL,NULL,'compras','Mercado de frutas','',NULL),(210,13,201,'Mercado de peixe',NULL,NULL,NULL,'compras','Mercado de peixe','',NULL),(211,13,201,'Mercado de artesanato',NULL,NULL,NULL,'compras','Mercado de artesanato','',NULL),(212,13,201,'Mercado de produtos variados',NULL,NULL,NULL,'compras','Mercado de produtos variados','',NULL),(213,13,201,'Outros mercados',NULL,NULL,NULL,'compras','Outros mercados','',NULL),(214,13,NULL,'Saberes e fazeres',NULL,NULL,NULL,'cultura','Saberes e fazeres','',NULL),(215,13,214,'Contar estórias/causos',NULL,NULL,NULL,'cultura','Contar estórias/causos','',NULL),(216,13,214,'Recitar poesias/rezas',NULL,NULL,NULL,'cultura','Recitar poesias/rezas','',NULL),(217,13,214,'Preparar receitas tradicionais',NULL,NULL,NULL,'cultura','Preparar receitas tradicionais','',NULL),(218,13,214,'Elaborar trabalhos manuais/de arte popular',NULL,NULL,NULL,'cultura','Elaborar trabalhos manuais/de arte popular','',NULL),(219,14,NULL,'Extrativismo e exploração',NULL,NULL,NULL,'economia','Extrativismo e exploração','',NULL),(220,14,219,'Mineral',NULL,NULL,NULL,'economia','Mineral','',NULL),(221,14,219,'Vegetal',NULL,NULL,NULL,'economia','Vegetal','',NULL),(222,14,NULL,'Agropecuária',NULL,NULL,NULL,'economia','Agropecuária','',NULL),(223,14,222,'Agricultura',NULL,NULL,NULL,'economia','Agricultura','',NULL),(224,14,222,'Aqüicultura',NULL,NULL,NULL,'economia','Aqüicultura','',NULL),(225,14,222,'Criação de animais silvestres',NULL,NULL,NULL,'economia','Criação de animais silvestres','',NULL),(226,14,222,'Agroindústria',NULL,NULL,NULL,'economia','Agroindústria','',NULL),(227,14,222,'Outras culturas',NULL,NULL,NULL,'economia','Outras culturas','',NULL),(228,14,NULL,'Indústrias',NULL,NULL,NULL,'economia','Indústrias','',NULL),(229,14,228,'Petrolífera',NULL,NULL,NULL,'economia','Petrolífera','',NULL),(230,14,228,'Automobilística',NULL,NULL,NULL,'economia','Automobilística','',NULL),(231,14,228,'Têxtil',NULL,NULL,NULL,'economia','Têxtil','',NULL),(232,14,228,'De laticínios',NULL,NULL,NULL,'economia','De laticínios','',NULL),(233,14,228,'De bebidas',NULL,NULL,NULL,'economia','De bebidas','',NULL),(234,14,228,'De couro',NULL,NULL,NULL,'economia','De couro','',NULL),(235,14,228,'Joalheira',NULL,NULL,NULL,'economia','Joalheira','',NULL),(236,14,228,'Moveleira',NULL,NULL,NULL,'economia','Moveleira','',NULL),(237,14,228,'Outras',NULL,NULL,NULL,'economia','Outras','',NULL),(238,15,NULL,'Parque tecnológico',NULL,NULL,NULL,'economia','Parque tecnológico','',NULL),(239,15,NULL,'Parque industrial',NULL,NULL,NULL,'economia','Parque industrial','',NULL),(240,15,NULL,'Museu tecnológico',NULL,NULL,NULL,'economia','Museu tecnológico','',NULL),(241,15,NULL,'Centro de pesquisa',NULL,NULL,NULL,'economia','Centro de pesquisa','',NULL),(242,15,NULL,'Usina hidrelétrica/Barragem/Eclusa/Açude','','','','economia','hidrelétrica/Barragem/Eclusa/Açude','Usina Usinas',' '),(243,15,NULL,'Planetário',NULL,NULL,NULL,'cultura','Planetário','',NULL),(244,15,NULL,'Aquário',NULL,NULL,NULL,'cultura','Aquário','',NULL),(245,15,NULL,'Viveiro',NULL,NULL,NULL,'cultura','Viveiro','',NULL),(246,15,NULL,'Exposição técnica',NULL,NULL,NULL,'cultura','Exposição técnica','',NULL),(247,15,NULL,'Exposição artística',NULL,NULL,NULL,'cultura','Exposição artística','',NULL),(248,15,NULL,'Ateliê',NULL,NULL,NULL,'arte','Ateliê','',NULL),(249,15,NULL,'Zoológico',NULL,NULL,NULL,'zoologico','Zoológico','',NULL),(250,15,NULL,'Jardim botânico',NULL,NULL,NULL,'jardim-botanico','Jardim botânico','',NULL),(251,15,NULL,'Outras',NULL,NULL,NULL,'outros','Outras','',NULL),(252,11,NULL,'Associações',NULL,NULL,NULL,'atendimento-turista','Associações','',NULL),(253,11,NULL,'Prestadores de Serviços Turísticos',NULL,NULL,NULL,'atendimento-turista','Prestadores de Serviços Turísticos','',NULL),(254,11,253,'Condutor Ambiental',NULL,NULL,NULL,'atendimento-turista','Condutor Ambiental','',NULL),(255,11,253,'Guia de Turismo',NULL,NULL,NULL,'atendimento-turista','Guia de Turismo','',NULL),(256,7,NULL,'Operadora de turismo',NULL,NULL,NULL,'operadora-turismo','Operadora de turismo','',NULL),(257,12,NULL,'Costas ou litoral',NULL,NULL,NULL,'natureza','Costas ou litoral','',NULL),(258,12,NULL,'Terras insulares',NULL,NULL,NULL,'natureza','Terras insulares','',NULL),(259,1,1,'Ponto de embarque/desembarque','Boarding/disembarking point','Punto de embarque/desembarque','Point d\'embarquement/débarquement','ponto','Ponto de Embarque/Desembarque','',NULL),(260,4,29,'Automóvel','','','','outros','Automóvel','',NULL),(261,4,29,'Motocicleta','','','','outros','Motocicleta','',NULL),(262,4,29,'Ônibus/Caminhão','','','','outros','Ônibus/Caminhão','',NULL),(263,4,29,'Bicicleta','','','','outros','Bicicleta','',NULL),(264,4,29,'Embarcações','','','','outros','Embarcações','',NULL),(265,11,252,'Ciclismo','','','','atendimento-turista','Ciclismo','',NULL),(266,11,252,'Artesanato','','','','atendimento-turista','Artesanato','',NULL),(267,13,139,'Geoparque','Geopark','','','cultura','Geoparque','',NULL),(268,14,222,'Pecuária','','','','economia','Pecuária','',NULL),(269,14,NULL,'Artesanato','','','','artesanato','Artesanato','',NULL),(270,14,269,'Cerâmica','','','','artesanato','Cerâmica','',NULL),(271,14,269,'Cestaria','','','','artesanato','Cestaria','',NULL),(272,14,269,'Madeira','','','','artesanato','Madeira','',NULL),(273,14,269,'Tecelagem','','','','artesanato','Tecelagem','',NULL),(274,14,269,'Bordados','','','','artesanato','Bordados','',NULL),(275,14,269,'Metal','','','','artesanato','Metal','',NULL),(276,14,269,'Pedra','','','','artesanato','Pedra','',NULL),(277,14,269,'Renda','','','','artesanato','Renda','',NULL),(278,14,269,'Couro','','','','artesanato','Couro','',NULL),(279,14,269,'Plumaria','','','','artesanato','Plumaria','',NULL),(281,9,73,'Animação','','','','organizacao-eventos','Animação','',NULL),(282,9,73,'Buffet/Catering','','','','organizacao-eventos','Buffet/Catering','',NULL),(283,9,73,'Convention & Visitors Bureau','','','','organizacao-eventos','Convention & Visitors Bureau','',NULL),(284,9,73,'Decoração','','','','organizacao-eventos','Decoração','',NULL),(285,9,73,'Fotografia','','','','organizacao-eventos','Fotografia','',NULL),(286,9,73,'Gráfica','','','','organizacao-eventos','Gráfica','',NULL),(287,9,73,'Iluminação','','','','organizacao-eventos','Iluminação','',NULL),(288,9,73,'Outros','','','','organizacao-eventos','Outros','',NULL),(289,9,73,'Som','','','','organizacao-eventos','Som','',NULL),(290,9,73,'Tradução e Interpretação','','','','organizacao-eventos','Tradução e Interpretação','',NULL),(291,9,NULL,'Salas de reunião','','','','organizacao-eventos','Salas de reunião','',NULL),(292,9,NULL,'Salão de festas (eventos sociais)','','','','organizacao-eventos','Salão de festas (eventos sociais)','',NULL),(293,9,NULL,'Espaços alternativos','','','','organizacao-eventos','Espaços alternativos','',NULL),(294,9,NULL,'Espaços em hotéis','','','','organizacao-eventos','Espaços em hotéis','',NULL),(295,2,NULL,'Delegacia do turismo','','','','delegacia','Delegacia do turismo','',NULL),(296,10,NULL,'Parques temáticos','','','','lazer','Parques temáticos','',NULL),(297,13,NULL,'Comunidades Tradicionais',NULL,NULL,NULL,'comunidades','Comunidades Tradicionais','',NULL),(298,13,297,'Quilombola',NULL,NULL,NULL,'comunidades','Quilombola','',NULL),(299,13,297,'Indígena',NULL,NULL,NULL,'comunidades','Indígena','',NULL),(300,13,297,'Ribeirinha',NULL,NULL,NULL,'comunidades','Ribeirinha','',NULL),(301,13,297,'De migração',NULL,NULL,NULL,'comunidades','De migração','',NULL),(302,13,297,'Extrativista',NULL,NULL,NULL,'comunidades','Extrativista','',NULL);
/*!40000 ALTER TABLE `type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_has_cadastur_activity`
--

DROP TABLE IF EXISTS `type_has_cadastur_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_has_cadastur_activity` (
  `type_id` int(11) NOT NULL,
  `cadastur_activity_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`,`cadastur_activity_id`),
  KEY `fk_type_has_cadastur_activity_cadastur_activity1_idx` (`cadastur_activity_id`),
  KEY `fk_type_has_cadastur_activity_type1_idx` (`type_id`),
  CONSTRAINT `fk_type_has_cadastur_activity_cadastur_activity1` FOREIGN KEY (`cadastur_activity_id`) REFERENCES `cadastur_activity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_type_has_cadastur_activity_type1` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_has_cadastur_activity`
--

LOCK TABLES `type_has_cadastur_activity` WRITE;
/*!40000 ALTER TABLE `type_has_cadastur_activity` DISABLE KEYS */;
INSERT INTO `type_has_cadastur_activity` VALUES (37,20),(38,20),(39,20),(40,20),(41,20),(42,20),(43,20),(69,25),(70,25),(71,25),(51,35),(73,40),(90,45),(91,45),(253,45),(255,45),(83,55),(84,55),(85,55),(74,60),(76,65),(86,70),(72,80);
/*!40000 ALTER TABLE `type_has_cadastur_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(100) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `attraction_id` int(11) DEFAULT NULL,
  `touristic_circuit_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `attraction_fk` (`attraction_id`),
  KEY `city_fk` (`city_id`),
  KEY `touristic_circuit_fk` (`touristic_circuit_id`),
  KEY `fk_user_user_type_idx` (`user_type_id`),
  CONSTRAINT `fk_attraction` FOREIGN KEY (`attraction_id`) REFERENCES `attraction` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_city` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_touristic_circuit` FOREIGN KEY (`touristic_circuit_id`) REFERENCES `touristic_circuit` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_user_type` FOREIGN KEY (`user_type_id`) REFERENCES `user_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,'Admin','admin@admin.com.br','$2y$10$qS8q8gkGd1OtjP1i1vfkiecpdTTPop.LQQJQb0JmowgZNdS25L6Dq','gNRt150X2hdjsL8VgF5swhWmP28fC6Nu3dOraj8qlxUyLRudpeumEO3g9vvk',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_trade_item`
--

DROP TABLE IF EXISTS `user_trade_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_trade_item` (
  `user_id` int(11) NOT NULL,
  `type` varchar(2) NOT NULL,
  `id` int(11) NOT NULL,
  `authorized` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`type`,`user_id`,`id`),
  KEY `fk_trade_item_user1_idx` (`user_id`),
  CONSTRAINT `fk_trade_item_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_trade_item`
--

LOCK TABLES `user_trade_item` WRITE;
/*!40000 ALTER TABLE `user_trade_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_trade_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_type`
--

DROP TABLE IF EXISTS `user_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_type`
--

LOCK TABLES `user_type` WRITE;
/*!40000 ALTER TABLE `user_type` DISABLE KEYS */;
INSERT INTO `user_type` VALUES (1,'Administrador'),(2,'Master'),(3,'Comunicação'),(4,'Município'),(5,'Região Turística'),(6,'Trade Turístico'),(7,'Parceiro'),(8,'Unidade fiscalizadora');
/*!40000 ALTER TABLE `user_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `voltage`
--

DROP TABLE IF EXISTS `voltage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `voltage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `voltage`
--

LOCK TABLES `voltage` WRITE;
/*!40000 ALTER TABLE `voltage` DISABLE KEYS */;
INSERT INTO `voltage` VALUES (1,110),(2,220);
/*!40000 ALTER TABLE `voltage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `event_by_date`
--

/*!50001 DROP VIEW IF EXISTS `event_by_date`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `event_by_date` AS select min(`event_date`.`date`) AS `start`,max(`event_date`.`date`) AS `end`,`event_date`.`event_id` AS `event_id`,`event`.`nome` AS `name`,`event`.`descricao_curta` AS `short_description`,`event`.`local_evento` AS `events_place`,`event`.`destaque` AS `featured`,`city`.`name` AS `city`,`event`.`id` AS `id`,`event`.`city_id` AS `city_id`,`event`.`touristic_circuit_id` AS `touristic_circuit_id`,`event`.`revision_status_id` AS `revision_status_id`,`event`.`active` AS `active`,`event`.`nome` AS `nome`,`event`.`slug` AS `slug`,`event`.`destaque` AS `destaque`,`event`.`cep` AS `cep`,`event`.`bairro` AS `bairro`,`event`.`logradouro` AS `logradouro`,`event`.`numero` AS `numero`,`event`.`complemento` AS `complemento`,`event`.`latitude` AS `latitude`,`event`.`longitude` AS `longitude`,`event`.`site` AS `site`,`event`.`telefone` AS `telefone`,`event`.`email` AS `email`,`event`.`facebook` AS `facebook`,`event`.`instagram` AS `instagram`,`event`.`twitter` AS `twitter`,`event`.`youtube` AS `youtube`,`event`.`flickr` AS `flickr`,`event`.`descricao_curta` AS `descricao_curta`,`event`.`local_evento` AS `local_evento`,`event`.`espaco_evento` AS `espaco_evento`,`event`.`tipo_evento` AS `tipo_evento`,`event`.`sobre` AS `sobre`,`event`.`dados_relevantes` AS `dados_relevantes`,`event`.`informacoes_uteis` AS `informacoes_uteis`,`event`.`created_at` AS `created_at`,`event`.`created_by` AS `created_by`,`event`.`updated_at` AS `updated_at`,`event`.`updated_by` AS `updated_by`,`event`.`comentario_revisao` AS `comentario_revisao` from ((`event_date` join `event`) join `city`) where ((`event_date`.`event_id` = `event`.`id`) and (`event`.`city_id` = `city`.`id`)) group by `event_date`.`event_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `events_by_date_category_classification`
--

/*!50001 DROP VIEW IF EXISTS `events_by_date_category_classification`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_unicode_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `events_by_date_category_classification` AS select `event_by_date`.`event_id` AS `event_id`,`event_by_date`.`end` AS `end`,`event_by_date`.`start` AS `start`,`event_by_date`.`featured` AS `featured`,`event_by_date`.`name` AS `name`,`event_by_date`.`short_description` AS `short_description`,`event_by_date`.`city` AS `city`,`event_by_date`.`city_id` AS `city_id`,`event_by_date`.`events_place` AS `events_place`,`event_by_date`.`revision_status_id` AS `revision_status_id`,`event_by_date`.`slug` AS `slug`,`event_selected_categories`.`event_category_id` AS `event_category_id`,`event_selected_classification`.`event_classification_id` AS `event_classification_id` from ((`event_by_date` left join `event_selected_categories` on((`event_by_date`.`event_id` = `event_selected_categories`.`event_id`))) left join `event_selected_classification` on((`event_by_date`.`event_id` = `event_selected_classification`.`event_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-21 17:19:57
