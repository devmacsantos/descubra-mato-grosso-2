<?php

return [

	'find_perfect_route'  =>  'Encuentre el guía de viaje perfecto',
	'route_how_many_days'  =>  '¿Durante cuantos días quiere viajar?',
	'where_will_you_pass'  =>  '¿Dónde va a pasear?',
	'how_split_your_trip'  =>  'Como dividir su viaje',
	'attractions_to_visit'  =>  'Atracciones que puede visitar',
	'day' => 'Día ',
	'days'  =>  'Día | días',
	'duration'  =>  'Duración del recurrido',
	'what_will_you_see'  =>  '¿Qué va a ver?',
	'choose_duration'  =>  'Escoja una duración',
	'duration_1'  =>  'Hasta 3 días',
	'duration_2'  =>  'Entre 3 y 7 días',
	'duration_3'  =>  'Más que 7 días',

];
