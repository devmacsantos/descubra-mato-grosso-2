<?php

return [

	'name'  =>  'Nombre',
	'message'  =>  'Mensaje',
	'send'  =>  'Enviar',
	'message_success'  =>  '¡Mensaje enviado con éxito!',
	'message_many_trials_same_ip'  =>  'Muchas tentativas registradas del mismo IP. Intente de nuevo más tarde',
	'confirm_you_are_not_a_robot'  =>  '¡Usted tiene que confirmar que no es un robó!',

];
