<?php

return [

	'search_results_for'  =>  'Resultados de la busca por:',
	'show_more_results'  =>  'Exhibir más resultados',
	'occurrences_in_destinations'  =>  'Ocurrencia en "Destinos"| Ocurrencias en "Destinos"',
	'occurrences_in_routes'  =>  'Ocurrencia en "Guía de viaje"| Ocurrencias en "Guía de viaje"',
	'occurrences_in_attractions'  =>  'Ocurrencia en "Atracciones"| Ocurrencias en "Atracciones"',
	'occurrences_in_accomodation' => 'Ocurrencia en "Hospedaje"|Ocurrencias en "Hospedaje"',
	'occurrences_in_feeding' => 'Ocurrencia en "Alimentación"|Ocurrencias en "Alimentación"',
	'occurrences_in_services' => 'Ocurrencia en "Servicios"|Ocurrencias en "Servicios"',
	'occurrences_in_recreation' => 'Ocurrencia en "Ocio"|Ocurrencias en "Ocio"',
	'occurrences_in_events' => 'Ocurrencia en "Eventos"|Ocurrencias en "Eventos"',
	'occurrences'  =>  'Resultado | Resultados',
	'attractions_in'  =>  'Atracciones en',
	
];
