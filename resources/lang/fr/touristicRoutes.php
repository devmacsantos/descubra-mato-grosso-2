<?php

return [

	'find_perfect_route'  =>  "Trouvez l'itinéraire parfait",
	'route_how_many_days'  =>  'Pendant combien de jours voulez vous voyager?',
	'where_will_you_pass'  =>  'Par où allez vous passer',
	'how_split_your_trip'  =>  'Comment partager votre voyage',
	'attractions_to_visit'  =>  'Attractions que vous devez visiter',
	'day' => 'Jour ',
	'days'  =>  'jour|jours',
	'duration'  =>  'Durée du parcours',
	'what_will_you_see'  =>  "Qu'allez vous voir",
	'choose_duration'  =>  'Choisissez une durée',
	'duration_1'  =>  "Jusqu'à 3 jours",
	'duration_2'  =>  'Entre 3 et 7 jours',
	'duration_3'  =>  'Plus de 7 jours',

];
