<?php

return [

	'discover_routes'  =>  'Découvrez les itinéraires de Minas',
	'discover_destinations'  =>  'Découvrez les destinations de Minas',
	'find_destination'  =>  'Trouvez votre destination à Minas',
	'what_do_you_expect'  =>  'Que souhaitez vous?',
	'choose_trip_category'  =>  'Choisissez un type de voyage',
	'choose_trip_type'  =>  'Choisissez un thème spécifique',
	'search'  =>  'Chercher',
	'destinations_a_z'  =>  'Destinations de A à Z',
	'find_perfect_park'  =>  'Trouvez le parc parfait',

];
