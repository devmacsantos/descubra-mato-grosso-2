<?php

return [

	'allow_sharing_adaptations'  =>  'Permettre que les adaptations de votre travail soient partagées',
	'allow_commercial_usage'  =>  'Permettre les utilisations commerciales de votre travail',
	'yes'  =>  'Oui',
	'yes_others_share_like'  =>  'Oui, pour autant que les autres en partagent autant',
	'no'  =>  'Non',
	'selected_license'  =>  'Licence sélectionnée',
	'attribution4_international'  =>  'Attribution 4.0 Internationale',
	'attribution_non_commercial4_international'  =>  'Attribution-Non-Commerciale 4.0 internationale',
	'attribution_share_like4_international'  =>  'Attribution-Partage-autant 4.0 internationale',
	'attribution_non_commercial_share_like4_international'  =>  'Attribution-Non Commerciale- Partage-autant 4.0 internationale',
	'attribution_no_derivations4_international'  =>  'Attribution-SansDéviations 4.0 internationale',
	'attribution_non_commercial_no_derivations4_international'  =>  'Attribution-NonCommerciale-SansDéviations 4.0 internationale',
	'free_culture_license'  =>  "Il s'agit d'une licence de culture libre",
	'not_free_culture_license'  =>  "Il ne s'agit pas d'une licence de culture libre",
	'donator_name'  =>  'Nom du donnateur',
	'donator_name_placeholder'  =>  'Informez un nom - personne ou entreprise',
	'document_type'  =>  'Type de Document',
	'document_number'  =>  'Numéro du document',
	'phone'  =>  'Téléphone',
	'media_type'  =>  'Type de média pour la donnation',
	'audio'  =>  'Audio',
	'video'  =>  'Vidéo',
	'image'  =>  'Image',
	'file_for_upload'  =>  'Fichier pour upload',
	'description' => 'Description',
	'agree_with'  =>  "Je suis d'accord avec les règles",
	'rules'  =>  'règles',
	'term' => 'Termo de compromisso',
	'of_media_donation'  =>  'de donnation de média',
	'donation_rules'  =>  'Règles de donnation de média',
	'max_size_10mb_exceeded'  =>  'Taille maximum de 10MO excédée',
	'file_required'  =>  'Un fichier doit être envoyé.',
	'success'  =>  'Merci de partager votre expérience avec nous!',


];
