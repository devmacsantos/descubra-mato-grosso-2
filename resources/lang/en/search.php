<?php

return [

	
	'search_results_for'  =>  'Results of the search by:',
	'show_more_results'  =>  'Show more results',
	'occurrences_in_destinations'  =>  'Event in "Destinations"| Events in "Destinations"',
	'occurrences_in_routes'  =>  'Event in "Road trip"| Events in "Road trip"',
	'occurrences_in_attractions'  =>  'Event in "Attractions"| Events in "Attractions"',
	'occurrences_in_accomodation' => 'Event in "Lodging"|Events in "Lodging"',
	'occurrences_in_feeding' => 'Event in "Food"|Events in "Food"',
	'occurrences_in_services' => 'Event in "Services"|Events in "Services"',
	'occurrences_in_recreation' => 'Event in "Leisure"|Events in "Leisure"',
	'occurrences_in_events' => 'Event in "Events"|Events in "Events"',
	'occurrences'  =>  'Result | Results',

	'attractions_in'  =>  'Attractions in',
	
	
];
