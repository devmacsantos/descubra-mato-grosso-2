<?php

return [

	'search_content'  =>  'Look for contents',
	'back'  =>  'Back',
	'related_content'  =>  'Related contents',
	'archive'  =>  'Archive',

];
