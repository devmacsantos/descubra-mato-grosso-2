<?php

return [


    'weather_forecast'  =>  'Weather forecast',
    'directions'  =>  'How to get there',
    'forecast_unavailable'  =>  'Non available forecast',
    'select_destination'  =>  'Select your destination',
    'type_origin'  =>  'Type your point of origin',
    'tourism_guide'  =>  'Minas Gerais Tourism Official Guide',
    'pdf_file'  =>  'PDF file',
		
	
];
