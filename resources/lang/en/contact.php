<?php

return [

	'name'  =>  'Name',
	'message'  =>  'Message',
	'send'  =>  'Send',
	'message_success'  =>  'Message successfully sent!',
	'message_many_trials_same_ip'  =>  'Several attempts registered for a same IP. Try again later',
	'confirm_you_are_not_a_robot'  =>  "You must prove you're not a robot!",
	
	
];
