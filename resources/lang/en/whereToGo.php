<?php

return [

	'discover_routes'  =>  "Find out Minas' road trips",
	'discover_destinations'  =>  'Find out the destinations in Minas',
	'find_destination'  =>  'Find your destination in Minas',
	'what_do_you_expect'  =>  'What do you desire?',
	'choose_trip_category'  =>  'Chose a type of travel',
	'choose_trip_type'  =>  'Chose a specific theme',
	'search'  =>  'Search',
	'destinations_a_z'  =>  'Destinations from A through Z',
	'find_perfect_park'  =>  'Find the perfect park',
	

];
