<?php

return [

    'search_content' => 'Busque por conteúdos',
    'back' => 'Voltar',
    'related_content' => 'Conteúdo Relacionado',
    'archive' => 'Arquivo',

];
