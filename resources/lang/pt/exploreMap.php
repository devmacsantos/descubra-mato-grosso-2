<?php

return [

	'mark_attractions_to_show_or_select_destination' => 'Marque os atrativos a serem exibidos ou filtre por destino',
	'nature' => 'Natureza',
	'waterfalls' => 'Cachoeiras',
	'lakes' => 'Lagos',
	'caves' => 'Grutas',
	'mountains' => 'Montanhas',
	'hidromineral' => 'Estâncias Hidrominerais',
	'culture' => 'Cultura',
	'historic_centers' => 'Centros Históricos',
	'buildings_monuments' => 'Prédios e Monumentos',
	'churches' => 'Igrejas',
	'museums' => 'Museus, Instituições Culturais e Teatros',
	'zoos' => 'Zoológico e Jardim Botânico',
	'markets' => 'Feiras E Mercados',

	'select_destination' => 'Selecione um destino para exibir seus atrativos',

];
