<?php

return [

    'search_results_for' => 'Resultados da busca por:',
    'show_more_results' => 'Exibir mais resultados',
    'occurrences_in_destinations' => 'Ocorrência em "Destinos"|Ocorrências em "Destinos"',
    'occurrences_in_routes' => 'Ocorrência em "Roteiros"|Ocorrências em "Roteiros"',
    'occurrences_in_attractions' => 'Ocorrência em "Atrações"|Ocorrências em "Atrações"',
    'occurrences_in_accomodation' => 'Ocorrência em "Hospedagem"|Ocorrências em "Hospedagem"',
    'occurrences_in_feeding' => 'Ocorrência em "Alimentação"|Ocorrências em "Alimentação"',
    'occurrences_in_services' => 'Ocorrência em "Serviços"|Ocorrências em "Serviços"',
    'occurrences_in_recreation' => 'Ocorrência em "Lazer"|Ocorrências em "Lazer"',
    'occurrences_in_events' => 'Ocorrência em "Eventos"|Ocorrências em "Eventos"',
    'occurrences' => 'Resultado|Resultados',

    'attractions_in' => 'Atrações em',
];
