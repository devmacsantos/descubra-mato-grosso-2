<?php

return [

	'weather_forecast' => 'Previsão do Tempo',
	'directions' => 'Como Chegar',
	'forecast_unavailable' => 'Previsão indisponível',
	'select_destination' => 'Selecione seu destino',
	'type_origin' => 'Digite seu ponto de partida',
	'tourism_guide' => 'Guia Oficial do Turismo de Minas Gerais',
	'pdf_file' => 'Arquivo PDF',

];
