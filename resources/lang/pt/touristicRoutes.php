<?php

return [

    'find_perfect_route' => 'Encontre o roteiro perfeito',
    'route_how_many_days' => 'Por quantos dias você quer viajar?',

    'where_will_you_pass' => 'Por onde você vai passar',
    'how_split_your_trip' => 'Como dividir sua viagem',
    'attractions_to_visit' => 'Atrações para você visitar',
    'day' => 'Dia ',
    'days' => 'dia|dias',
    'duration' => 'Duração do percurso',
    'what_will_you_see' => 'O que você vai ver',

    'choose_duration' => 'Escolha uma duração',
    'duration_1' => 'Até 3 dias',
    'duration_2' => 'Entre 3 e 7 dias',
    'duration_3' => 'Mais de 7 dias',



];
