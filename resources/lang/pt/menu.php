<?php

return [

    'discover' => 'Conheça',
    'what_to_do' => 'O que fazer',
    'where_to_go' => 'Para onde ir',
    'explore_the_map' => 'Explore o mapa',
    'plan_your_trip' => 'Planeje sua viagem',
    'events' => 'Eventos',
    'contact' => 'Fale conosco',
    'register_your_business' => 'Cadastre seu negócio ou evento',
    'media_donation' => 'Doação de Mídias',
    'favorites' => 'Favoritos',
    'show_all' => 'Exibir todos',

    'see_how_plan_trip' => 'Veja como planejar sua viagem',

    'add_favorite' => 'Adicione aos favoritos',
    'remove_favorite' => 'Remova dos favoritos',

    'what_do_you_want' => 'O que você espera da sua viagem?',
    'discover_what_to_do' => 'Descubra o que fazer por aqui',

    'more_destinations' => 'Conheça mais destinos',

    'gallery' => 'Galerias',

    'cities_and_districts' => 'Cidades e Distritos',
    'parks_and_reservations' => 'Parques e Reservas Naturais',

    'routes' => 'Roteiros',
    'destinations' => 'Destinos',
    'parks' => 'Parques',

    'search_in_site' => 'Buscar...',
    'subscribe_newsletter' => 'Assine a newsletter',
    'type_email' => 'Digite seu e-mail',
    'registered_email' => 'E-mail já cadastrado',
    'invalid_email' => 'E-mail inválido',
    'subscription_success' => 'Inscrição feita com sucesso!',
    'unsubscription_success' => 'Inscrição removida com sucesso!',

    'routes_duration_1' => 'Roteiros com até 3 dias',
    'routes_duration_2' => 'Roteiros com 3 a 7 dias',
    'routes_duration_3' => 'Roteiros com mais de 7 dias',
    'routes_duration_1_pt1' => 'Roteiros com até',
    'routes_duration_1_pt2' => '3 dias',
    'routes_duration_2_pt1' => 'Roteiros com',
    'routes_duration_2_pt2' => '3 a 7 dias',
    'routes_duration_3_pt1' => 'Roteiros com mais de',
    'routes_duration_3_pt2' => '7 dias',
    'im_looking_for' => 'Estou procurando por',
    'place' => 'Onde'

];
