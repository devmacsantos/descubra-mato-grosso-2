<?php

return [

	'useful_information' => 'Informações úteis',
	'location' => 'Localização',
	'location_urban' => 'Urbana',
	'location_rururban' => 'Rururbana',
	'location_rural' => 'Rural',
	'surroundings_description' => 'Descrição dos arredores e distância dos principais pontos',
	'reference_points' => 'Pontos de referência',
	'historic_period' => 'Período histórico',

	'functioning_period' => 'Horário de funcionamento',
	'functioning_period_2' => 'Horário de funcionamento',
	'functioning_period_observation' => 'Observação sobre funcionamento',

	'visitation_type' => 'Tipo de visita',
	'visitation_type_not_guided' => 'Não guiada',
	'visitation_type_self_guided' => 'Auto-guiada',
	'visitation_type_guided' => 'Guiada',

	'entrance_type' => 'Entrada',
	'entrance_type_free' => 'Franca',
	'entrance_type_paid' => 'Paga',
	'value' => 'Valor',

	'activities' => 'Atividades realizadas',
	'requires_authorization' => 'Necessário autorização prévia',
	'payment_methods' => 'Métodos de pagamento',
	'complementary_observations' => 'Informações complementares',
	'accepts_animals' => 'Aceita animais',
	'gay_friendly' => 'Gay friendly',
	'doesnt_accept_kids' => 'Não aceita crianças',

	'facilities' => 'Facilidades',
	'cable_tv' => 'TV por assinatura',
	'voltage' => 'Voltagem',

	'daily_rate_type' => 'Tipo de diária',
	'daily_rate_type_no_breakfest' => 'Sem café',
	'daily_rate_type_breakfest' => 'Com café',
	'daily_rate_type_half_pension' => 'Meia pensão',
	'daily_rate_type_full_pension' => 'Pensão completa',



	'recreation' => 'Recreação e lazer',

	'pool' => 'Piscina',
	'adapted_pool' => 'Piscina adaptada',
	'heated_pool' => 'Piscina aquecida',
	'heated_adapted_pool' => 'Piscina aquecida adaptada',
	'dry_sauna' => 'Sauna seca',
	'adapted_dry_sauna' => 'Sauna seca adaptada',
	'sauna' => 'Sauna a vapor',
	'adapted_sauna' => 'Sauna a vapor adaptada',
	'court' => 'Quadra de esportes',
	'adapted_court' => 'Quadra de esportes adaptada',
	'playground' => 'Playground',
	'adapted_playground' => 'Playground adaptado',
	'hottub' => 'Hidromassagem',
	'spa' => 'Spa',
	'day_use' => 'Day Use',
	'touristic_guide' => 'Guia turístico',
	'fishing_guide' => 'Guia de pesca',
	'recreation_monitor' => 'Monitor de recreação/passeios',
	'game_room' => 'Sala de jogos',
	'mounting_animals' => 'Animais para montaria',
	'soccer_court' => 'Campo de futebol',
	'bowling' => 'Boliche',
	'boats' => 'Barcos/Embarcações',
	'fishing' => 'Equipamentos para pesca',
	'cheering_service' => 'Serviço de animação',
	'cooper_track' => 'Pista de cooper',
	'carriages' => 'Charretes',
	'golf' => 'Golfe',
	'diving_equipment' => 'Equipamento de mergulho',
	'ping_pong' => 'Pingue-pongue',
	'eletronic_games' => 'Jogos eletrônicos',

	'bar_restaurant' => 'Bar/Lanchonete/Restaurante',

	'services_equipments' => 'Serviços e equipamentos',

	'event_place' => 'Local para eventos',
	'capacity' => 'Capacidade',
	'live_music' => 'Música ao vivo',
	'mechanic_music' => 'Música mecânica/ambiente',
	'parking_lot' => 'Estacionamento',
	'air_conditioning' => 'Ar condicionado',
	'recreation_area' => 'Recreação/Área de lazer',
	'smoking_lounge' => 'Espaço para fumantes',
	'adapted_toilets' => 'Sanitários adaptados',
	'braille_menu' => 'Cardápio em braille',
	'vallets' => 'Manobristas',
	'internet' => 'Internet',

	'cuisine_nationality' => 'Culinária - Nacionalidade',
	'cuisine_service' => 'Culinária - Serviço',
	'cuisine_theme' => 'Culinária - Tema',
	'cuisine_region' => 'Culinária - Região',

	'restrictions' => 'Restrições',

	'tourism_specialization'=>'Segmentos ou tipos de turismo em que é especializado',

	'specialization_adventure'=>'Aventura',
	'specialization_ecoturism'=>'Ecoturismo',
	'specialization_rural'=>'Rural',
	'specialization_sun_beach'=>'Sol e praia',
	'specialization_studies'=>'Estudos e intercâmbio',
	'specialization_business'=>'Negócios e eventos',
	'specialization_cultural'=>'Cultural',
	'specialization_nautic'=>'Náutico',
	'specialization_health'=>'Saúde (bem-estar e médico)',
	'specialization_fishing'=>'Pesca',


	'service_types' => 'Tipos de serviço',
	'service_type_excursion' => 'Excursão',
	'service_type_local' => 'Passeio local',
	'service_type_special' => 'Especial',
	'service_type_transfer' => 'Traslado',

	'adapted_vehicles_quantity' => 'Quantidade de veículos adaptados',

	'support_services_equipments' => 'Serviços e equipamentos de apoio',

	'security_service' => 'Serviço de segurança',
	'medical_services' => 'Serviços médicos',
	'ambulatorial_services' => 'Serviços ambulatoriais',
	'buffet' => 'Serviços de café/buffet',
	'touristic_info' => 'Informações turísticas',
	'stores' => 'Lojas',
	'bank24hours' => 'Banco 24 horas',
	'parking_lot_spots' => 'vagas',

	'main_activities' => 'Principais atividades',

	'accessibility' => 'Acessibilidade',
	'accessibility_accessible_external_route' => 'Rota externa acessível',
	'accessibility_accessible_external_route_others' => 'Rota externa acessível - Outras',
	'accessibility_boarding_area' => 'Local de embarque e desembarque',
	'accessibility_parking_spot' => 'Vaga em estacionamento',
	'accessibility_access_area' => 'Área de circulação/acesso interno para cadeiras de rodas/Lazer e UHs',
	'accessibility_stairs' => 'Escada',
	'accessibility_elevator' => 'Elevador',
	'accessibility_communication' => 'Comunicação',
	'acessibility_special_seat_obese' => 'Assento especial para obesos',
	'accessibility_toilet' => 'Sanitário',


	'accessibility_low_sidewalk' => 'Calçada rebaixada',
	'accessibility_walking_stripes' => 'Faixa de pedestre',
	'accessibility_ramp' => 'Rampa',
	'accessibility_sound_semaphore' => 'Semáforo sonoro',
	'accessibility_floor_tactile_alert' => 'Piso tátil de alerta',
	'accessibility_regular_floor' => 'Piso regular/antiderrapante',
	'accessibility_obstacle_free' => 'Livre de obstáculos',
	'accessibility_signalized' => 'Sinalizado',
	'accessibility_leveled_access' => 'Com acesso em nível',
	'accessibility_signalized_a' => 'Sinalizada',
	'accessibility_large_wheelchair' => 'Alargada para cadeira de rodas',
	'accessibility_sidewalk_ramp' => 'Rampa de acesso à calçada',
	'accessibility_elevator_platform' => 'Plataforma elevatória',
	'accessibility_circulation_furniture' => 'Com circulação entre mobiliário',
	'accessibility_large_door' => 'Porta larga',
	'accessibility_handrail' => 'Corrimão',
	'accessibility_resting_platform' => 'Patamar para descanso',
	'accessibility_tactile_alert_signaling' => 'Sinalização tátil de alerta',
	'accessibility_non_skid_floor' => 'Piso antiderrapante',
	'accessibility_braille_signaling' => 'Sinalizado em Braille',
	'accessibility_sound_mechanism' => 'Dispositivo sonoro',
	'accessibility_light_mechanism' => 'Dispositivo luminoso',
	'accessibility_eletronic_sensor' => 'Sensor eletrônico (porta)',
	'accessibility_braille_informative_text' => 'Texto informativo em Braille',
	'accessibility_large_informative_text' => 'Texto informativo em fonte ampliada',
	'accessibility_signaling_interpreter' => 'Intérprete em Libras (língua brasileira de sinais)',
	'accessibility_support_bar' => 'Barra de apoio',
	'accessibility_large_door_wheelchair' => 'Porta larga suficiente para entrada de cadeira de rodas',
	'accessibility_wheelchair_spin' => 'Giro para cadeira de rodas',
	'accessibility_wheelchair_access' => 'Acesso para cadeira de rodas',
	'accessibility_low_sink' => 'Pia rebaixada',
	'accessibility_low_mirror' => 'Espelho rebaixado ou com ângulo de alcance visual',
	'accessibility_adapted_bath' => 'Boxe ou banheira adaptada',


	'accessibility_agency_structure' => 'Acessibilidade na estrutura do espaço físico',
	'accessibility_adapted_routes' => 'Roteiros Adaptados',
	'accessibility_accomodation' => 'Meios de hospedagem',
	'accessibility_transport' => 'Transportes',
	'accessibility_prepared_guides' => 'Guias preparados para público com deficiência ou mobilidade reduzida',
	'accessibility_attractions' => 'Atrativos',
	'accessibility_accessible_material' => 'Material acessivo',

	'accessibility_special_parking_spots' => 'Vagas de estacionamento especiais',
	'accessibility_access_ramps' => 'Rampas de acesso',
	'accessibility_capacitated_workers' => 'Funcionários capacitados',
	'accessibility_tactile_signaling' => 'Sinalização tátil (deficiência visual)',
	'accessibility_accessible_toilet' => 'Sanitário acessível',
	'accessibility_displacement_space' => 'Espaço para deslocamento (cadeirantes)',

	'accessibility_physical' => 'Física ou motora',
	'accessibility_visual' => 'Visual',
	'accessibility_hearing' => 'Auditiva',
	'accessibility_mental' => 'Mental',
	'accessibility_reduced_mobility' => 'Mobilidade reduzida (idosos, gestantes, obesos entre outros)',
	'accessibility_large_door_obese' => 'Porta larga o suficiente para entrada de obesos',
	'accessibility_toilet_seat_obese' => 'Sanitário com assento especial para obesos',

	'place_closed' => 'Local fechado.',
	'place_closed_reason' => 'Motivo: ',
	'place_closed_works' => 'Fechado para reforma/obras',
	'place_closed_restricted' => 'Interditado por órgão regulador',
	'place_closed_economic_conjuncture' => 'Conjuntura econômica',
	'place_closed_maintenance' => 'Problemas de manutenção',
	'place_closed_meteorological_phenomena' => 'Fenômeno meteorológico',
	'place_closed_climatic_phenomena' => 'Fenômeno climatólogico',
	'place_closed_access_infrastructure_damaged' => 'Infraestrutura de acesso danificada',
	'place_closed_other' => 'Outros',

];
