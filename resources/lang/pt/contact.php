<?php

return [

	'name' => 'Nome',
	'message' => 'Mensagem',
	'send' => 'Enviar',
	'message_success' => 'Mensagem enviada com sucesso!',
	'message_many_trials_same_ip' => 'Muitas tentativas registradas do mesmo IP. Tente novamente mais tarde',
	'confirm_you_are_not_a_robot' => 'Você deve confirmar que não é um robô!',

];
