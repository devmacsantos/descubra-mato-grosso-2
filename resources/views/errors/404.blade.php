@extends((Auth::check() && strpos(Request::url(),url('/admin')) !== false) ? 'admin.base' : 'public.base')

@section('main-content')
    @if(Auth::check() && strpos(Request::url(),url('/admin')) !== false)
    <section id="content" class="error-page pn">
        <div class="center-block mt50 mw800">
            <h1 class="error-title"> 404! </h1>
            <h2 class="error-subtitle">Página não encontrada.</h2>
        </div>
    </section>
    @else
        <?php
            $p_Language = str_replace(url('/'), '', Request::url());
            $p_Language = explode('/', $p_Language)[1];
            \App::setLocale($p_Language);
        ?>
        <div class="row-fluid" id="useful-information" style="margin-top:180px;">
            <div class="container">
                <div class="col-lg-12">
                    <h2>404!</h2>
                    <p style="text-align: center">{{trans("string.page_not_found")}}</p>
                </div>
            </div>
        </div>
    @endif
@stop

