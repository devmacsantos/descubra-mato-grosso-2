@if(session('message'))
    <div class="alert alert-system alert-block alert-dismissable fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p>{!! session('message') !!}</p>
    </div>
@endif
@if(session('error_message'))
    <div class="alert alert-danger alert-block alert-dismissable fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p>{!! session('error_message') !!}</p>
    </div>
@endif
@foreach($errors->all() as $error)
    <div class="alert alert-danger alert-block alert-dismissable fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p>{{ $error }}</p>
    </div>
@endforeach