@extends('public.templates.base') 
@section('pageCSS')


<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsIndex.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsSearchbar.css')}}">
@endsection
 
@section('header')
        @include('public.templates.header')
@endsection
 
@section('main-content')
<main class="container-fluid mt-4">
        <div class="title container mb-5 pb-5">
                <h1 class="float-md-left display-4">
                        Eventos
                </h1>
                <p class="float-md-right"><a style="color: #db775f" href="{{url('/admin/login')}}"> <span class="oi oi-calendar"></span> Criar Evento</a></p>
        </div>

        <!-- Barra de Pesquisa -->
        @include('public.templates.eventsSearchbar')
        @include('public.templates.eventsCategory')

        @if (isset($eventCarousel) && (count($eventCarousel)>0))  {{--if Carousel de Eventos--}}

        <div class="container mt-5">


                <div id="carouselControls_" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner text-center">
                                
                                @foreach ($eventCarousel as $key => $vEvent) {{-- Faz loop para preencher o carousel --}} 
                                        @if ($vEvent->EventPhotos(true)->first() != null) {{-- Verifica se o evento tem foto --}} 
                                
                                <div class="carousel-item border {{($key==0)?'active':''}}">        
                                        <div class="row featurette">
                                                <div class="col-md-5 order-2 order-md-2 ">
                                                          <!--
                                                                Depuração
                                                                {{$vEvent->id}}
                                                                {{getMonthName(2)}}
                                                                {{date_format(date_create($vEvent->inicio), 'm')}}
                                                                {{getMonthName(date_format(date_create($vEvent->inicio), 'm'))}}
                                                        -->
                                                        <div class="text-center text-uppercase mt-4">
                                                                <a class="link-no-hover title" href="{{url($p_Language.'/eventos/'.$vEvent->Destination->slug.'/'.$vEvent->slug)}}" title="{{$vEvent->nome}}" data-content="{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}" data-placement="top">
                                                                    <h5 class="text-bold">{{$vEvent->nome}}</h5>
                                                                </a>
                                                        </div>

                                                        <table class="float-left mt-4 ml-3">
                                                                <div class="text-center mt-3 mt-md-4 d-none d-md-flex mx-3">
                                                                        <small>{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}</small>
                                                                </div>
                                                                <tr>
                                                                @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'm') != date_format(date_create($vEvent->fim), 'm')))

                                                                        <th class="rounded-top carousel-calendar-month text-center px-1">
                                                                                {{getMonthName(date_format(date_create($vEvent->inicio), 'm'))}}/{{getMonthName(date_format(date_create($vEvent->fim), 'm'))}}
                                                                        </th>
                                                                @else
                                                                        <th class="rounded-top carousel-calendar-month text-center px-1">
                                                                                {{getMonthName(date_format(date_create($vEvent->inicio), 'm'))}}
                                                                        </th>
                                                                @endif
                                                                </tr>
                                                                <tr>
                                                                @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'd') != date_format(date_create($vEvent->fim), 'd')))
                                                                <td class="rounded-bottom border border-0 carousel-calendar-day text-center px-1">
                                                                        {{date_format(date_create($vEvent->inicio), 'd')}} | {{date_format(date_create($vEvent->fim), 'd')}}
                                                                </td>
                                                                @else
                                                                <td class="rounded-bottom border border-0 carousel-calendar-day text-center px-1">
                                                                        {{date_format(date_create($vEvent->inicio), 'd')}}
                                                                </td>
                                                                @endif
                                                                </tr>
                                                        </table>
                                                        <div class="text-uppercase mt-4">
                                                                <div class="row">
                                                                        <div class="col-md-12 font-weight-bold text-left">
                                                                                <small><span class="oi oi-map-marker text-info"></span></small><b>&nbsp; {{$vEvent->City->name}}</b>
                                                                        </div>
                                                                        <div class="col-md-12 text-left">
                                                                                <small><small>{{$vEvent->local_evento}}</small></small>
                                                                        </div>
                                                                </div>
                                                        </div>

                                                        <div class="text-right text-md-center pt-3 mb-3 mr-3"> 
                                                                <a href="{{url($p_Language.'/eventos/'.$vEvent->Destination->slug.'/'.$vEvent->slug)}}" class="btn btn-dark">Detalhes</a>
                                                        </div>
                                                </div>
                                                <div class="col-md-7 order-1 order-md-1 border pr-md-0">
                                                        <a  href="{{url($p_Language.'/eventos/'.$vEvent->Destination->slug.'/'.$vEvent->slug)}}">
                                                                <img class="featurette-image img-fluid mx-auto" src="/image/650/262/true/true/{{$vEvent->EventPhotos(true)->first()->url}}" alt="{{$vEvent->nome}}">
                                                        </a>
                                                </div>
                                        </div>
                                </div>

                                @endif 
                                @endforeach

                        </div>

                        <a class="carousel-control-prev" href="#carouselControls_" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                        <a class="carousel-control-next" href="#carouselControls_" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
                </div>
        </div>
        @endif {{--Fim de if Carousel de Eventos--}}
        
        @if (isset($featuredEvent) && (count($featuredEvent)>0)) {{--if Eventos em Destaque--}}
        <div class="container mt-5">
                <div class="title">
                        <h4 class="text-uppercase">
                                <strong>EVENTOS EM DESTAQUE</strong>
                        </h4>
                </div>

                <section class="top-eventos slider">
                        @foreach ($featuredEvent as $vEvent) {{--Início foreach Eventos em Destaque--}}
                        
                        @if ($vEvent->EventPhotos(true)->first() != null) {{-- Verifica se o evento tem foto de capa --}}

                        <div class="slide text-center">
                                <div>
                                        <div class="card mb-4 shadow">
                                                <a href="{{url($p_Language.'/eventos/'.$vEvent->Destination->slug.'/'.$vEvent->slug)}}">
                                                        <img class="card-img-top fade-image" src="/image/330/248/true/true/{{$vEvent->EventPhotos(true)->first()->url}}">
                                                </a>
                                                <div class="card-body pb-4">
                          
                                                        <div class="border border-top-0 border-left-0 border-right-0 text-center text-uppercase text-truncate text-bold">
                                                                <a class="link-no-hover title" href="{{url($p_Language.'/eventos/'.$vEvent->Destination->slug.'/'.$vEvent->slug)}}" title="{{$vEvent->nome}}"
                                                                                  data-toggle="popover" data-trigger="hover" data-content="{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}"
                                                                                  data-placement="top">{{$vEvent->nome}}
                                                                </a>
                                                        </div>
                                                        <table class="float-left">
                                                                <tr>
                                                                        <th class="border border-0 calendar-month-single text-light px-2 text-uppercase">
                                                                                {{getMonthName(date_format(date_create($vEvent->inicio),'m'))}}
                                                                        </th>
                                                                </tr>
                                                                @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'd') != date_format(date_create($vEvent->fim), 'd')) && (date_format(date_create($vEvent->inicio), 'm') == date_format(date_create($vEvent->fim), 'm')))
                                                                {{--*/ $twoDays=true /*--}} @else {{--*/ $twoDays=false /*--}}
                                                                @endif
                                                                <tr>
                                                                        <td class="border border-0 calendar-day-single text-light text-center px-2"> 
                                                                                <strong>{{date_format(date_create($vEvent->inicio), 'd')}}</strong>
                                                                        </td>
                                                                </tr>
                                          
                                                                <tr>
                                                                        <td class="border border-0 calendar-day-single text-light text-center px-2">
                                                                                <strong @if (!$twoDays) style="visibility: hidden" @endif>
                                                                                        {{date_format(date_create($vEvent->fim), 'd')}}
                                                                                </strong>
                                                                        </td>
                                                                </tr>
                                                        </table>
                                                        <div class="text-uppercase mt-1 text-left pl-4 ml-3">
                                                                <div class="font-weight-bold pl-3"><small><span class="oi oi-map-marker text-info"></span></small><small><b class="text-dark">&nbsp; {{$vEvent->City->name}} </b></small>
                                                                        <p class="text-truncate">
                                                                                <small><small style="color: black">{{$vEvent->local_evento}}</small></small>
                                                                        </p>
                                                                </div>
                                                        </div>
                                                </div>
                                        </div>
                                </div> 

                        </div>
                        @endif {{-- fim verifica se o evento tem foto de capa --}}
                        @endforeach {{--Fim foreach Eventos em Destaque--}}
            
                </section>

        </div>
        @endif {{--Fim if Eventos em Destaque--}}
        
        
        @if (isset($themeEvent) && (count($themeEvent)>0)) {{--Verifica Eventos de tema--}}
        <div class="container">
                <div class="title">
                        <h4 class="text-uppercase pt-4">
                                @if (isset($classification))
                                        <strong>{{$classification->nome}}</strong>
                                @else
                                        <strong>TEMA</strong>
                                @endif

                        </h4>
                </div>

                <section class="eventos-tema slider">

                                @foreach ($themeEvent as $vEvent) {{--Início foreach Eventos de tema--}}
                                @if ($vEvent->EventPhotos(true)->first() != null) {{-- Verifica se o evento tem foto de capa --}}

                                <div class="slide text-center">
                                
                                                <div class="">
                                                        <div class="card mb-4 shadow">
                                                                <a href="{{url($p_Language.'/eventos/'.$vEvent->Destination->slug.'/'.$vEvent->slug)}}">
                                                                        <img class="card-img-top fade-image" src="/image/330/248/true/true/{{$vEvent->EventPhotos(true)->first()->url}}">
                                                                </a>
                                                                <div class="card-body pb-4">
                                                                        <div class="border border-top-0 border-left-0 border-right-0 text-center text-uppercase text-truncate text-bold">
                                                                                <a class="link-no-hover title" href="{{url($p_Language.'/eventos/'.$vEvent->Destination->slug.'/'.$vEvent->slug)}}" title="{{$vEvent->nome}}"
                                                                                          data-toggle="popover" data-trigger="hover" data-content="{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}"
                                                                                          data-placement="top">{{$vEvent->nome}}
                                                                                </a>
                                                                        </div>
                                                                
                                                                        <table class="float-left">
                                                                                <tr>
                                                                                        <th class="border border-0 calendar-month-single text-light px-2 text-uppercase">
                                                                                                {{getMonthName(date_format(date_create($vEvent->inicio),'m'))}}
                                                                                        </th>
                                                                                </tr>
                                                                                        @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'd') != date_format(date_create($vEvent->fim), 'd')) && (date_format(date_create($vEvent->inicio), 'm') == date_format(date_create($vEvent->fim), 'm')))
                                                                                        {{--*/ $twoDays=true /*--}} @else {{--*/ $twoDays=false /*--}}
                                                                                        @endif
                                                                                <tr>
                                                                                        <td class="border border-0 calendar-day-single text-light text-center px-2"> <strong>{{date_format(date_create($vEvent->inicio), 'd')}}</strong></td>
                                                                                </tr>
                                                                        
                                                                                <tr>
                                                                                        <td class="border border-0 calendar-day-single text-light text-center px-2"><strong @if (!$twoDays) style="visibility: hidden" @endif>{{date_format(date_create($vEvent->fim), 'd')}}</strong></td>
                                                                                </tr>
                                                                        </table>

                                                                        <div class="text-uppercase mt-1 text-left pl-4 ml-3">
                                                                                <div class="font-weight-bold pl-3">
                                                                                        <small><span class="oi oi-map-marker text-info"></span></small>
                                                                                        <small><b class="text-dark">&nbsp; {{$vEvent->City->name}} </b></small>
                                                                                        <p class="text-truncate">
                                                                                                <small><small style="color: black">{{$vEvent->local_evento}}</small></small>
                                                                                        </p>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div>
                                                </div> 

                                </div>
                                @endif {{-- Verifica se o evento tem foto de capa --}}
                                @endforeach {{--Fim foreach Eventos de tema--}}
                        
                </section>

        </div>
        @endif {{--Fim Verifica Eventos de tema--}}

        <section class="download title pt-5 d-none d-md-flex">
                <div class="container">
                        <div class="row">
                                <div class="col-6"></div>
                                <div class="col-2">
                                        <h3><span class="oi oi-data-transfer-download"></span>&nbsp;Download</h3>
                                </div>
                                <div class="col-4">
                                        <hr style="height: 3px; background-color: #db775f;">
                                </div>
                                <div class="col-4">
                                        <hr style="height: 3px; background-color: #db775f;">
                                </div>
                                <div class="col-2">
                                        <a tabindex="0"  data-toggle="popover" data-trigger="focus" data-placement="top" data-html="true" title="Selecione o Ano" data-content="<a target='_blank' href='{{url("/portal/assets/docs/Calendario%20de%20Eventos%20Tur%C3%ADsticos%20de%20MG%202018.pdf ")}}'>2018<a><br><a target='_blank' href='{{url("/portal/assets/docs/Calendario%20de%20Eventos%20Tur%C3%ADsticos%20de%20MG%202019.pdf ")}}'>2019<a>">
                                                <figure class="figure text-center">
                                                        <img class="fade-image" src="{{url('/image/53/53/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/ca.png')}}" class="figure-img img-fluid rounded" alt="Agenda">
                                                        <figcaption class="figure-caption">Calendário de Eventos Turísticos.</figcaption>
                                                </figure>
                                        </a>
                                </div>
                                <div class="col-2">
                                                
                                        <a target="_blank" href="{{url("/portal/assets/docs/Agenda_mineira_2018_-_sem_logo_PEleitoral.pdf ")}}">
                                                <figure class="figure text-center">
                                                        <img class="fade-image" src="{{url('/image/53/53/false/false/portal/assets/libs/imgs/categories_of_events/pictograma/ag.png')}}" class="figure-img img-fluid rounded"
                                                                alt="Agenda">
                                                        <figcaption class="figure-caption">Agenda Mineira.
                                                        </figcaption>
                                                </figure>
                                        </a>
                                </div>
                                <div class="col-4"></div>
                        </div>
                </div>
        </section>
</main>
@include('public.templates.menuMobile')
@endsection
 
@section('footer')
        @include('public.templates.footer')
@endsection
 
@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsIndex.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsSearchbar.js')}}"></script>
@endsection