@extends('public.base')
@section('pageCSS')
    <style>
        #destinos #choices_home .options.active {
            background: #db775f;
            border: 1px solid #fff;
            color: #fff;
            text-decoration: none;
        }
    </style>
@stop
@section('main-content')

    <div class="row-fluid " id="destinos" style="padding:1% 0;">
        <section class="parallax-minas360 marginb">

            <div class="caption-content callout">
                <div class="container no-padding">
                    
                    <div class="col-lg-12">
                        <h2 class="branco">{{trans('360.explore_360')}}</h2>
                    </div>
                        

                    <div class="no-padding">
                        <div class="col-lg-12">

                            <div class="col-lg-12 col-md-12 col-sm-12" id="choices_home">
                                <?php
                                    $v_FilteredCategories = $p_CategoryFilter ? explode(',', $p_CategoryFilter) : [];
                                    $p_CategoryFilter = $p_CategoryFilter ? ($p_CategoryFilter . ',') : '';
                                ?>
                                @foreach($p_Categories as $c_Index => $c_Category)
                                    <?php $v_Name = json_decode($c_Category->nome,1)[$p_Language]; ?>
                                    <div class="col-lg-2-5 col-md-2-5 col-sm-3">
                                        @if(in_array($c_Category->slug, $v_FilteredCategories))
                                            <?php
                                                $v_Filter = '';
                                                foreach($v_FilteredCategories as $c_FilteredCategory){
                                                    if($c_FilteredCategory != $c_Category->slug)
                                                        $v_Filter .= (strlen($v_Filter) ? ',' : '') . $c_FilteredCategory;
                                                }
                                                $v_Filter = strlen($v_Filter) ? ('?q=' . $v_Filter) : '';
                                            ?>
                                            <a class="options active" href="{{url($p_Language . '/minas360' . $v_Filter)}}">{{$v_Name}}</a>
                                        @else
                                            <a class="options" href="{{url($p_Language . '/minas360?q=' . $p_CategoryFilter . $c_Category->slug)}}">{{$v_Name}}</a>
                                        @endif
                                    </div>
                                @endforeach
                            </div>

                            <div class="col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4" id="call-to-action-home">
                                <a href="{{url($p_Language . '/minas360')}}">{{trans('menu.show_all')}}</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!--<div class="col-lg-12 text-center" id="call-to-action-section">
                    <p><img src="{{url('/portal/assets/imgs/setaBaixo.png')}}" alt=""></p>
                </div>-->
            </div>

        </section>


        <div class="container">
         

            <div class="col-lg-12 line no-padding" style="margin-bottom:25px;">
                @foreach($p_Places as $c_Index => $c_Place)
                    <?php
                        if(isset($c_Place['destino']))
                            $v_Name = $c_Place['nome'];
                        else
                            $v_Name = json_decode($c_Place['nome'],1)[$p_Language];
                    ?>
                    <div class=" col-sm-12 col-xs-12 list-thumbs-full thumb-treehundreandsixty">
                        <a target="_blank" href="{{$c_Place['minas_360']}}">
                            <div class="hoverzoom">
                                <div class="thumbs-big">
                                    <img src="{{$c_Place['url']}}" alt="{{$v_Name}}" title="{{$v_Name}}">
                                </div>

                                <div class="retina2">
                                    <div class="mascara">
                                        <p class="center">{{$v_Name}}</p>
                                        <div class="container">
                                            <div id="choices_home" class="col-lg-12 col-md-12 col-sm-12 text-center">
                                            <div class="col-sm-6 col-sm-offset-3 text-center" >
                                                @foreach($c_Place['trip_categories'] as $c_Category)
                                                    <?php $v_Name = json_decode($c_Category['nome'],1)[$p_Language]; ?>
                                                    <div class="center-inline">
                                                        <div class="options">{{$v_Name}}</div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            </div>

                                            <div class="col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4" >
                                                <p class="icon360"><img src="{{url('/portal/assets/imgs/binoculo_360.png')}}">{{trans('360.see_360')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop

@section('pageScript')
@stop