@extends('public.base')

@section('pageCSS')
<style>
    #planeje .categorySlim a.active .box {
        background: #db775f;
        transition: all 0.5s ease; }
</style>
@stop

@section('main-content')
 
     <div class="row-fluid margin-titlemenu" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2 class="laranja">{{trans('touristicRoutes.route_how_many_days')}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="planeje">
        <div class="container">
            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 categorySlim">
                <a href="{{url($p_Language . '/roteiros-duracao?d=1')}}" class="{{$p_Duration == 1 ? 'active' : ''}}">
                    <div class="col-lg-12 box">
                        <div class="col-lg-12" id="title-overlay" style="padding: 0 1rem;">
                            <div>
                                <p>{{trans('menu.routes_duration_1')}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 categorySlim">
                <a href="{{url($p_Language . '/roteiros-duracao?d=2')}}" class="{{$p_Duration == 2 ? 'active' : ''}}">
                    <div class="col-lg-12 box">
                        <div class="col-lg-12" id="title-overlay" style="padding: 0 1rem;">
                            <div>
                                <p>{{trans('menu.routes_duration_2')}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 categorySlim">
                <a href="{{url($p_Language . '/roteiros-duracao?d=3')}}" class="{{$p_Duration == 3 ? 'active' : ''}}">
                    <div class="col-lg-12 box">
                        <div class="col-lg-12" id="title-overlay" style="padding: 0 1rem;">
                            <div>
                                <p>{{trans('menu.routes_duration_3')}}</p>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos">
        <div class="col-lg-12" style="padding:2rem 0;">
            <div class="container">
                <div class="col-lg-12 line" style="padding:0;">
                    @foreach($p_TouristicRoutes as $c_Index => $c_TouristicRoute)
                        <?php
                            $v_RouteName = json_decode($c_TouristicRoute->nome,1)[$p_Language];
                            $v_RouteDescription = json_decode($c_TouristicRoute->descricao_curta,1)[$p_Language];
                        ?>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <a href="{{url($p_Language . '/roteiros/' . $c_TouristicRoute->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-four">
                                        <img src="{{$c_TouristicRoute->url}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_RouteName}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_RouteDescription}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_RouteName}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageScript')
@stop