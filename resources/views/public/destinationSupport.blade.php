@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 41px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 41px;
        }
    </style>
@stop

@section('main-content')
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <div class="col-lg-1"  style="padding: 2.6% 0 0;">
                    <a href="javascript:history.back()" id="goBack">{{trans('blog.back')}}</a>
                </div>
                <div class="col-lg-10">
                    <h2>{{$p_Destination->nome}}</h2>
                </div>
                <form action="#" method="post" id="formRegister">
                    <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
                        <div class="input-group" style="width: 100%;display:block;" id="inputDestino">
                            {!! Form::select('destination_slug', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['id' => 'destination_slug', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="destinos">
        <div class="row-fluid" style="padding-top:6rem;">
            <div class="container">
                <div class="col-lg-12 nav-tabs nav-tabs-atrativo" style="padding:2% 0;">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{(empty($p_FilterType) || $p_FilterType == 'atrativo') ? 'active' : ''}} apoio" href="#atrativos" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p style="font-size:29px;">
                                        <img src="{{url('/portal/assets/imgs/atrativoOrange.png')}}" alt="" style="width: 50px;">
                                        <span class="hidden-sm-down">Atrativos</span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$p_FilterType == 'acomodacao' ? 'active' : ''}} apoio" href="#hotel" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p style="font-size:29px;">
                                        <img src="{{url('/portal/assets/imgs/camaOrange.gif')}}" alt="" style="width: 50px;">
                                        <span class="hidden-sm-down">{{trans('destination.accomodation')}}</span>
                                        <span></span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$p_FilterType == 'estabelecimento' ? 'active' : ''}} apoio" href="#alimentacao" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p style="font-size:29px;">
                                        <img src="{{url('/portal/assets/imgs/talherOrange.gif')}}" alt="" style="width: 50px;">
                                        <span class="hidden-sm-down">{{trans('destination.feeding')}}</span>
                                        <span></span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$p_FilterType == 'servico' ? 'active' : ''}} apoio" href="#servico" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p style="font-size:29px;">
                                        <img src="{{url('/portal/assets/imgs/malaOrange.gif')}}" alt="" style="width: 50px;">
                                       <span class="hidden-sm-down"> {{trans('destination.services')}}</span>
                                        <span></span>
                                    </p>
                                </div>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$p_FilterType == 'lazer' ? 'active' : ''}} apoio" href="#lazer" role="tab" data-toggle="tab">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 hospedagem">
                                    <p style="font-size:29px;">
                                        <img src="{{url('/portal/assets/imgs/coposOrange.gif')}}" alt="" style="width: 50px;">
                                        <span class="hidden-sm-down">{{trans('destination.recreation')}}</span>
                                        <span></span>
                                    </p>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">

                <!-- Atrativos -->
                <div role="tabpanel" class="tab-pane {{(empty($p_FilterType) || $p_FilterType == 'atrativo') ? 'active' : ''}}" id="atrativos">
                    
                    <div class="col-lg-12 box-hospedagem" id="tarja">
                        <div class="container no-padding">
                            <div class="col-lg-12 no-padding">
                                <div class="row" id="options">
                                    <form action="#" method="post" id="formSearch">
                                        <div class="col-lg-4 col-lg-offset-3 col-md-4 col-sm-4 col-xs-12 ">
                                            <p style="font-size:20px;">{{trans('destination.find_attractions')}}</p>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 selectoptions">
                                            {!! Form::select('tipo_atrativo', ['' => trans('destination.attraction_type'), 'natureza' => trans('exploreMap.nature'), 'cultura' => trans('exploreMap.culture'), 'experiencia' => trans('destination.experience')], $p_FilterType == 'atrativo' ? $p_FilterValue : '', ['id' => 'tipo_atrativo']) !!}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12" style="padding:2rem 0;">
                        <div class="container">
                            <div class="col-lg-12 line">
                                @foreach($p_Attractions as $c_Attraction)
                                    <?php
                                        $v_Description = json_decode($c_Attraction->descricao_curta,1)[$p_Language];
                                        if($c_Attraction->trade)
                                            $v_Name = $c_Attraction->nome;
                                        else
                                            $v_Name = json_decode($c_Attraction->nome,1)[$p_Language];
                                    ?>
                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                        <a href="{{url($p_Language . '/atracoes/' . $p_Destination->slug . '/' . $c_Attraction->slug)}}">
                                            <div class="hoverzoom">
                                                <div class="thumbs-mini-four">
                                                    <img src="{{$c_Attraction->url}}">
                                                </div>
                                                <div class="retina-hover">
                                                    <div class="col-lg-12 title">
                                                        <p>{{$v_Name}}</p>
                                                    </div>
                                                    <div class="col-lg-12 no-padding">
                                                        <hr>
                                                    </div>
                                                    <div class="col-lg-12 text">
                                                        <p>{{$v_Description}}</p>
                                                    </div>
                                                </div>
                                                <div class="retina">
                                                    <p>{{$v_Name}}</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Hotels -->
                <div role="tabpanel" class="tab-pane {{$p_FilterType == 'acomodacao' ? 'active' : ''}}" id="hotel">
                    <div class="col-lg-12 box-hospedagem" id="tarja">
                        <div class="container no-padding">
                            <div class="col-lg-12 no-padding">
                                <div class="row" id="options">
                                    <form action="#" method="post" id="formSearch">
                                        <div class="col-lg-4  col-lg-offset-3 col-md-4 col-sm-4 col-xs-12 ">
                                            <p style="font-size:20px;">{{trans('destination.find_accomodation')}}</p>
                                        </div>
                                        <?php
                                            $v_AccomodationList = [
                                                '' => trans('destination.accomodation_type'),
                                                '43' => trans('destination.accomodation_flat'),
                                                '51' => trans('destination.accomodation_camping'),
                                                '52' => trans('destination.accomodation_summer_camp'),
                                                '38,39' => trans('destination.accomodation_hotel'),
                                                '40,42' => trans('destination.accomodation_farm_hotel'),
                                                '53' => trans('destination.accomodation_hostel'),
                                                '41' => trans('destination.accomodation_inn'),
                                                '47,48,54' => trans('destination.others')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 selectoptions">
                                            {!! Form::select('tipo_acomodacao', $v_AccomodationList, $p_FilterType == 'acomodacao' ? $p_FilterValue : '', ['id' => 'tipo_acomodacao']) !!}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_Accomodations as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-bottom:3rem;height: 345px;">
                                    <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug . '/' . $c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <!--<h4>{{$c_Service->sub_type_id ? $p_AccomodationList[$c_Service->sub_type_id] : ''}}</h4>-->
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug .'/' . $c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- Restaurantes -->
                <div role="tabpanel" class="tab-pane {{$p_FilterType == 'estabelecimento' ? 'active' : ''}}" id="alimentacao">
                    <div class="col-lg-12 box-restaurante" id="tarja">
                        <div class="container">
                            <div class="col-lg-12">
                                <div class="row" id="options">
                                    <form action="#" method="post" id="formSearch">
                                        <div class="col-lg-4  col-lg-offset-3 col-md-4 col-sm-4 col-xs-12 ">
                                            <p style="font-size:20px;">{{trans('destination.find_feeding')}}</p>
                                        </div>
                                        <?php
                                            $v_FoodDrinkList = [
                                                '' => trans('destination.feeding_type'),
                                                '55' => trans('destination.feeding_restaurants'),
                                                '56,59,61' => trans('destination.feeding_bars'),
                                                '57' => trans('destination.feeding_tea_house'),
                                                '58' => trans('destination.feeding_breweries'),
                                                '60' => trans('destination.feeding_ice_cream_shops'),
                                                '62' => trans('destination.others')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 selectoptions">
                                            {!! Form::select('tipo_estabelecimento', $v_FoodDrinkList, $p_FilterType == 'estabelecimento' ? $p_FilterValue : '', ['id' => 'tipo_estabelecimento']) !!}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_FoodDrinks as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-bottom:3rem;height: 345px;">
                                    <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug .'/' . $c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <!--<h4>{{$p_FoodDrinkList[$c_Service->type_id]}}</h4>-->
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug .'/' . $c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- Passeios -->
                <div role="tabpanel" class="tab-pane {{$p_FilterType == 'servico' ? 'active' : ''}}" id="servico">
                    <div class="col-lg-12 box-passeio" id="tarja">
                        <div class="container">
                            <div class="col-lg-12">
                                <div class="row" id="options">
                                    <form action="#" method="post" id="formSearch">
                                        <div class="col-lg-4 col-lg-offset-3 col-md-4 col-sm-4 col-xs-12 ">
                                            <p style="font-size:20px;">{{trans('destination.find_service')}}</p>
                                        </div>
                                        <?php
                                            $v_ServiceList = [
                                                '' => trans('destination.service_type'),
                                                '64,256' => trans('destination.services_agencies'),
                                                '66' => trans('destination.services_rental_agencies')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 selectoptions">
                                            {!! Form::select('tipo_servico', $v_ServiceList, $p_FilterType == 'servico' ? $p_FilterValue : '', ['id' => 'tipo_servico']) !!}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_Services as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-bottom:3rem;height: 345px;">
                                    @if($c_Service->integrante_minas_recebe)
                                        <div class="minas-recebe">
                                            <img src="{{url('/portal/assets/imgs/logo-minas-recebe.png')}}" alt="Minas Recebe" class="logo-recebe" id="dropdownRecebe" >
                                            <div class="col-lg-12 divdropdownrecebe">
                                                <p class="text-center">Minas Recebe!<br/> Um programa de fortalecimento do turismo receptivo do Estado de Minas Gerais.</p>
                                            </div>
                                        </div>
                                    @endif
                                    <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug .'/' . $c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <!--<h4>{{$p_ServiceList[$c_Service->type_id]}}</h4>-->
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug .'/' . $c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- lazer -->
                <div role="tabpanel" class="tab-pane {{$p_FilterType == 'lazer' ? 'active' : ''}}" id="lazer">
                    <div class="col-lg-12 box-lazer" id="tarja">
                        <div class="container">
                            <div class="col-lg-12">
                                <div class="row" id="options">
                                    <form action="#" method="post" id="formSearch">
                                        <div class="col-lg-4  col-lg-offset-3 col-md-4 col-sm-4 col-xs-12 ">
                                            <p style="font-size:20px;">{{trans('destination.find_recreation')}}</p>
                                        </div>
                                        <?php
                                            $v_RecreationList = [
                                                '' => trans('destination.recreation_type'),
                                                '83,85' => trans('destination.recreation_nightclubs'),
                                                '84' => trans('destination.recreation_shows'),
                                                '86' => trans('destination.recreation_theater'),
                                                '77,78,79,87' => trans('destination.recreation_sports'),
                                                '76' => trans('destination.recreation_clubs'),
                                                '74' => trans('destination.recreation_parks'),
                                                '80,81,88,89' => trans('destination.recreation_others')
                                            ];
                                        ?>
                                        <div class="col-lg-3  col-md-3 col-sm-3 col-xs-12 selectoptions">
                                            {!! Form::select('tipo_lazer', $v_RecreationList, $p_FilterType == 'lazer' ? $p_FilterValue : '', ['id' => 'tipo_lazer']) !!}
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 line no-padding" style="margin-top:4rem;">
                        <div class="container">
                            @foreach($p_Recreations as $c_Service)
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" style="margin-bottom:3rem;height: 345px;">
                                    <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug .'/' . $c_Service->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-four">
                                                <img src="{{$c_Service->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Service->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{$c_Service->cidade}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Service->nome}} <br><small>{{$c_Service->cidade}}</small></p>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details-hotel">
                                        <!--<h4>{{$p_RecreationList[$c_Service->type_id]}}</h4>-->
                                        <hr>
                                        <p id="address" style="text-align:center;">{{$c_Service->logradouro . ', ' . $c_Service->numero}}</p>
                                        <p id="phone" style="text-align:center;">Tel: {{$c_Service->telefone}}</p>
                                        <p id="email" style="text-align:center;">{{$c_Service->email}}</p>
                                    </div>
                                    <div class="col-lg-12" id="call-to-action-hotel">
                                        <a href="{{url($p_Language . '/apoio/' . $p_Destination->slug .'/' . $c_Service->slug)}}">{{trans('destination.more_info')}}</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {!! Form::open(array('method' => 'get', 'url'=> url($p_Language . '/apoio-destino/' . $p_Destination->slug), 'id' => 'destinationSupport')) !!}
    {!! Form::close() !!}
@stop

@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.select2').select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );

            $('.tab-pane select').change(function(){
                var $v_Form = $('#destinationSupport');
                var v_Name = $(this).attr('name');
                $v_Form.html('<input type="hidden" name="'+v_Name+'" value="'+this.value+'">');
                $v_Form.submit();
            });

            $('#destination_slug').change(function(){
                location.href = '{{url($p_Language . '/apoio-destino')}}/' + this.value;
            });
        });
    </script>
@stop