@extends('public.base')

@section('pageCSS')

@stop

@section('main-content')
<!-- FRAGMENTO ATUALIZADO -->
<div class="row-fluid" id="useful-information">
    <div class="container">
        <div class="col-lg-12" id="list-title">
            <h2>{{trans('menu.contact')}}</h2>
        </div>
    </div>
</div>
<!-- FRAGMENTO ATUALIZADO -->

<div class="row-fluid" id="destinos">
    <div class="container">
        
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                <div class="col-lg-12" style="margin-bottom:20px;">
                    <p>{{$p_Content == null ? '' : $p_Content}}</p>
                </div>
            </div>
            <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 mb-big-borda">
               
                    {!! Form::open(array('id' => 'formContact', 'url'=> url($p_Language . '/fale-conosco'), 'onsubmit' => 'return checkRecaptcha();')) !!}
                        <div class="form-group">
                            <input type="text" class="form-control" name="nome" id="nome" placeholder="{{trans('contact.name')}}" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" required>
                        </div>
                        <div class="form-group">
                            {!! Form::select('assunto', $p_Subjects, null, ['id' => 'subject', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::select('', $p_States, null, ['id' => 'state', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::select('cidade', [], null, ['id' => 'city', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                            <textarea name="mensagem" class="form-control" id="textarea" rows="5" placeholder="{{trans('contact.message')}}" required></textarea>
                        </div>
                        <div class="row">
                            <div class="center-block" style="margin-bottom: 10px;">
                                {!! Recaptcha::render() !!}
                            </div>
                        </div>
                        <input type="submit" class="btn" name="enviar" id="enviar" value="{{trans('contact.send')}}">
                    {!! Form::close() !!}
                
            </div>
        
    </div>
</div>
@stop

@section('pageScript')
    <script>
        $(document).ready(function()
        {
            if(window.location.search.substring(1) == 'mensagem-enviada')
                showNotify('info', '{!!trans('contact.message_success')!!}');
            else if(window.location.search.substring(1) == 'tentativas-excessivas')
                showNotify('danger', '{!!trans('contact.message_many_trials_same_ip')!!}.');

            $('#state').change(function () {
                var v_DataString = '<option value="">{{trans('string.city_live')}}</option>';
                $('#city').html(v_DataString).val('');

                var v_SelectedState = $(this).val();
                if (v_SelectedState != "") {
                    $.get("{{url($p_Language . '/fale-conosco-cidades')}}/" + v_SelectedState).done(function (p_Data) {
                        var v_DataString = '<option value="">{{trans('string.city_live')}}</option>';
                        $.each(p_Data, function (c_Key, c_Field) {
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                        });
                        $('#city').html(v_DataString);
                    }).error(function () {
                    });
                }
            }).change();
        });

        function checkRecaptcha()
        {
            if(grecaptcha.getResponse().length == 0) {
                showNotify('danger', '{!!trans('contact.confirm_you_are_not_a_robot')!!}');
                return false;
            }
            return true;
        }
    </script>
@stop