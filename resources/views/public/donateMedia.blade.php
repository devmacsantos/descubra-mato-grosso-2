@extends('public.base')
@section('pageCSS')
    <style>
        .arrow_box {
            position: relative;
            background: #fff;
            border: 1px solid #ccc;
            padding: 4rem;
        }
        .arrow_box:after, .arrow_box:before {
            right: 100%;
            top: 50%;
            border: solid transparent;
            content: " ";
            height: 0;
            width: 0;
            position: absolute;
            pointer-events: none;
        }

        .arrow_box:after {
            border-color: rgba(255, 255, 255, 0);
            border-right-color: #fff;
            border-width: 30px;
            margin-top: -30px;
        }
        .arrow_box:before {
            border-color: rgba(204, 204, 204, 0);
            border-right-color: #ccc;
            border-width: 31px;
            margin-top: -31px;
        }

        #destinos #tarja_intern #options #donation .form-group select {
            background: #e6e6e6;
            border-radius: 0;
            border: .0625rem solid #ccc;
            width: 100%;
            font-family: Signika, sans-serif;
        }
    </style>
@stop

@section('main-content')
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2>{{trans('menu.media_donation')}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12 text-center">
                        <img src="{{url('/portal/assets/imgs/creative.png')}}" alt="Creative Commons">
                        <p>{{$p_Content == null ? '' : $p_Content->descricao}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="col-lg-12" id="tarja_intern">
            <div class="container">
                <div class="col-lg-10 col-lg-offset-1">

                    <div class="row" id="options">

                        {!! Form::open(array('id' => 'donation', 'class' => 'form-inline', 'url'=> url($p_Language . '/doacao-de-midias'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
                            <div class="col-lg-12" style="padding:0 0 2rem;">
                                <div class="col-lg-6 form-group">
                                    <div class="col-lg-12 no-padding" style="padding:0 0 0.5rem;">
                                        <label for="codigo" style="text-align:left;">{{trans('donateMedia.allow_sharing_adaptations')}}?</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_compartilhar]" class="license-type permite_compartilhar" value="1" checked> {{trans('donateMedia.yes')}}
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_compartilhar]" class="license-type permite_compartilhar" value="0"> {{trans('donateMedia.no')}}
                                        </label>
                                    </div>

                                    <div class="col-lg-12 no-padding" style="padding:0 0 0.5rem;">
                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_compartilhar]" class="license-type permite_compartilhar" value="2"> {{trans('donateMedia.yes_others_share_like')}}
                                        </label>
                                    </div>

                                    <div class="col-lg-12 no-padding" style="padding:0 0 0.5rem;">
                                        <label for="codigo" style="text-align:left;">{{trans('donateMedia.allow_commercial_usage')}}?</label>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_uso_comercial]" class="license-type permite_uso_comercial" value="1" checked> {{trans('donateMedia.yes')}}
                                        </label>

                                        <label class="radio-inline">
                                            <input type="radio" name="doacao[permite_uso_comercial]" class="license-type permite_uso_comercial" value="0"> {{trans('donateMedia.no')}}
                                        </label>
                                    </div>
                                </div>

                                <div class="col-lg-6 form-group">
                                    <div class="col-lg-12 arrow_box">
                                        <div class="col-lg-12">
                                            <p style="font-size:16px;color:#4b4b4b;padding:0;margin:0 0 10px;font-weight:300;">{{trans('donateMedia.selected_license')}}:<br>
                                                <span id="license_type">{{trans('donateMedia.attribution4_international')}}</span></p>
                                        </div>
                                        <div class="col-lg-12">
                                            <p style="font-size:14px;color:#cccccc;padding:0;margin:0;font-weight:300;"><small id="license_type_subtitle">{{trans('donateMedia.free_culture_license')}}.</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-12">
                                <hr style="float:left;margin-left:auto;margin-right:auto;display:block;width:100%;height:1px;background:#cccccc;">
                            </div>

                            <div class="row-fluid">
                                <div class="col-md-6 form-group" >
                                    <div class="col-lg-12 no-padding">
                                        <label for="nome">{{trans('donateMedia.donator_name')}}:</label>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <input name="doacao[nome]" type="text" class="form-control" id="nome" placeholder="{{trans('donateMedia.donator_name_placeholder')}}" required>
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <div class="col-lg-12 no-padding">
                                        <label for="numberDoc">{{trans('donateMedia.document_type')}}:</label>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <input name="doacao[documento]" type="text" class="form-control" id="numberDoc" placeholder="{{trans('donateMedia.document_number')}}" required>
                                    </div>
                                </div>
                                
                            </div>

                            

                           
                                <!--<div class="col-md-2 form-group">
                                    <div class="col-lg-12 no-padding">
                                        <label for="typeDoc">{{trans('donateMedia.document_type')}}:</label>
                                    </div>
                                    <div class="col-lg-4" style="display: none;">
                                        <label class="radio-inline">
                                            <input type="radio" class="pessoa_juridica" value="0" checked> CPF
                                        </label>

                                        <label class="radio-inline">
                                            <input name="doacao[pessoa_juridica]" type="radio" class="pessoa_juridica" value="1"> CNPJ
                                        </label>
                                    </div>

                                </div>-->
       

                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-6 form-group no-padding">
                                    <div class="col-lg-12 no-padding">
                                       <div class="col-lg-12">
                                            <label for="email">E-mail:</label>
                                        </div>
                                       <div class="col-lg-12">
                                            <input name="doacao[email]" type="email" class="form-control" id="email" placeholder="E-mail" required>
                                       </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 form-group no-padding">
                                   <div class="col-lg-12">
                                        <label for="phone">{{trans('donateMedia.phone')}}:</label>
                                    </div>
                                    <div class="col-lg-12">
                                        <input name="doacao[telefone]" type="text" class="form-control" id="phone" placeholder="{{trans('donateMedia.donator_phone_placeholder')}}" required>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 no-padding form-group">
                                <div class="col-lg-6 form-group no-padding">
                                    <div class="col-lg-12">
                                        <label for="city">{{trans('donateMedia.city')}}:</label>
                                    </div>
                                    <div class="col-lg-12">
                                        {!! Form::select('doacao[city_id]', $p_Cities, null, ['id' => 'city', 'class' => 'form-control', 'required' => 'required']) !!}
                                    </div>
                                </div>

                                <div class="col-lg-12 form-group no-padding">
                                    <div class="col-lg-12">
                                        <label for="description">{{trans('donateMedia.description')}}:</label>
                                    </div>
                                    <div class="col-lg-12">
                                    <textarea name="doacao[descricao]" class="form-control style-area" style="width: 100%; background:#e6e6e6; min-height: 200px; max-height: 200px;" id="description" maxlength="250" required></textarea>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 no-padding form-group">
                                <div class="col-lg-4">
                                    <span>
                                        <label for="typeDonation">{{trans('donateMedia.media_type')}}:</label>
                                    </span>
                                </div>
                                <div class="col-lg-8">
                                    <label class="radio-inline">
                                        <input type="radio" name="doacao[tipo_midia]" class="tipo_midia" value="Imagem" checked> {{trans('donateMedia.image')}}
                                    </label> 

                                    <label class="radio-inline">
                                        <input type="radio" name="doacao[tipo_midia]" class="tipo_midia" value="Áudio" > {{trans('donateMedia.audio')}}
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="doacao[tipo_midia]" class="tipo_midia" value="Vídeo"> {{trans('donateMedia.video')}}
                                    </label>
                                    
                                </div>
                            </div>

                            <div class="col-lg-12 no-padding form-group">
                                <div class="col-lg-3">
                                    <label for="files">{{trans('donateMedia.file_for_upload')}}:</label>
                                </div>
                                <div class="col-lg-9">
                                    <input id="fakeupload" class="fakeupload" type="text">
                                    <input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="image/*">
                                </div>
                            </div>

                            <div class="col-lg-12 form-group">
                                <div class="col-md-6 no-padding form-group rules">
                                    
                                        <div class="col-lg-1 text-left no-padding">
                                            <input type="radio" class="form-control" id="rules" required>
                                        </div>
                                        <div class="col-lg-11 no-padding">
                                            <label for="rules" style="padding:0;">{{trans('donateMedia.agree_with')}} <a href="javascript:;" data-toggle="modal" data-target="#myModal">{{trans('donateMedia.rules')}}</a> {{trans('donateMedia.of_media_donation')}}</label>
                                        </div>
                                    
                                </div>
                            </div>

                            <div class="col-lg-12 rules">
                                <button type="submit" class="enviar pull-right margint" id="enviar">{{trans('contact.send')}}</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{trans('donateMedia.donation_rules')}}</h4>
                </div>
                <div class="modal-body">
                    {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageScript')
    <script>
        var MAX_FILE_SIZE = 1024*1024*10; //10MB
        $(document).ready(function(){
            $('#numberDoc').mask('999.999.999-99');
//            $('.pessoa_juridica').change(function(){
//                if(this.value == 0)
//                    $('#numberDoc').mask('999.999.999-99');
//                else
//                    $('#numberDoc').mask('99.999.999/9999-99');
//            });

            $('.license-type').change(function(){
                var v_AllowShare = $('.permite_compartilhar:checked').val();
                var v_AllowCommercialUsage = $('.permite_uso_comercial:checked').val();
                var v_LicenseType = '';
                var v_LicenseTypeSubtitle = '{!!trans('donateMedia.not_free_culture_license')!!}.';
                if(v_AllowShare == 2) {
                    if(v_AllowCommercialUsage == 1)
                        v_LicenseType = '{!!trans('donateMedia.attribution4_international')!!}';
                    else
                        v_LicenseType = '{!!trans('donateMedia.attribution_non_commercial4_international')!!}';
                }
                else if(v_AllowShare == 1) {
                    if(v_AllowCommercialUsage == 1) {
                        v_LicenseType = '{!!trans('donateMedia.attribution_share_like4_international')!!}';
                        v_LicenseTypeSubtitle = '{!!trans('donateMedia.free_culture_license')!!}.'
                    }
                    else
                        v_LicenseType = '{!!trans('donateMedia.attribution_non_commercial_share_like4_international')!!}';
                }
                else {
                    if(v_AllowCommercialUsage == 1)
                        v_LicenseType = '{!!trans('donateMedia.attribution_no_derivations4_international')!!}';
                    else
                        v_LicenseType = '{!!trans('donateMedia.attribution_non_commercial_no_derivations4_international')!!}';
                }
                $('#license_type').text(v_LicenseType);
                $('#license_type_subtitle').text(v_LicenseTypeSubtitle);
            });

            $('.tipo_midia').change(function(){
                if($('.tipo_midia').val() == 'Áudio')
                    $('#realupload').replaceWith('<input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="audio/*">');
                else if($('.tipo_midia').val() == 'Vídeo')
                    $('#realupload').replaceWith('<input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="video/*">');
                else
                    $('#realupload').replaceWith('<input id="realupload" name="arquivo" class="realupload" type="file" onchange="onChangeFile(this)" required accept="image/*">');
            }).change();
        });

        function onChangeFile(p_FileInput)
        {
            var v_CancelClicked = (p_FileInput.value == '');

            if(v_CancelClicked)
                $('#fakeupload').val('');
            else
            {
                if(p_FileInput.files[0].size > MAX_FILE_SIZE)
                {
                    showNotify('danger', '{!!trans('donateMedia.max_size_10mb_exceeded')!!}.');
                    $('#fakeupload').val('');
                    return false;
                }
                $('#fakeupload').val(p_FileInput.value.split('\\').pop());
            }
        }
    </script>
@stop                                