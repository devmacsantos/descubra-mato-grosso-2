@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 38px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 38px;
        }
        #destinos #call-to-action a {
            width: auto;
            max-width:280px;
        }
    </style>
@stop
@section('main-content')

<div class="row-fluid banner_home" id="destinos" style="padding:0;">

<a href="{{url($p_Language . '/minas360')}}">
<section class="video-container">
  <video src="{{url('/portal/assets/imgs/setur.mp4')}}" autoplay loop></video>
  <div class="callout">
    <div class="caption" style="opacity:1;width: 100%;height: 100%;top:0;">
            <div class="caption-content">
                <div class="container no-padding">
                    <div class="no-padding">
                        <div class="col-lg-12 text-highlight text-center">
                            <img src="{{url('/portal/assets/imgs/logo_vert.png')}}" alt="">  
                            <h2 style="text-align:center;">{{trans('360.explore_360')}}</h2>
                        </div>
                    </div>
                </div>
                <!-- INSERIDA ANCORA PARA VISUALIZAÇÂO DA SEÇÂO ABAIXO -->
                <div class="col-lg-12 text-center hidden-sm-down" id="call-to-action-section">
                    <!--<p>{{trans('home.scroll_down_more')}}</p>-->
                    <p><img src="{{url('/portal/assets/imgs/setaBaixo.png')}}" alt="" class="arrown-down"></p>
                </div>
                <!-- INSERIDA ANCORA PARA VISUALIZAÇÂO DA SEÇÂO ABAIXO -->
            </div>
        </div>
  </div>
</section>
</a>


<section>
    <div class="caption-content">
        <div class="container no-padding">
            <div class="col-lg-12">
                <h2 class="text-center cyan" style="padding: 5% 2.5%;">{{$p_Content == null ? '' : $p_Content->titulo_o_que_fazer}}</h2>
            </div>
        </div>
    </div>
</section>


<section class="parallax-minas">
    
    <div class="caption-content callout">
        <div class="container no-padding">
            <div class="no-padding">
                <div class="col-lg-12">
                   
                    <div class="col-lg-12 col-md-12 col-sm-12" id="choices_home">
                        @foreach($p_Categories as $c_Index => $c_Category)
                            @if($c_Index < 5)
                                <?php $v_Name = json_decode($c_Category->nome,1)[$p_Language]; ?>
                                <div class="col-lg-2-5 col-md-2-5 col-sm-3">
                                    <a class="options" href="{{url($p_Language . '/o-que-fazer/' . $c_Category->slug)}}">{{$v_Name}}</a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    
                    <div class="col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4" id="call-to-action-home">
                        <a href="{{url($p_Language . '/o-que-fazer')}}">{{trans('home.more_activities')}}</a>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
    
</section>



<div class="row-fluid">

<!--Start atrativos-->
<div class="container">
        <div class="col-lg-12">
            <h2 class="text-center cyan" style="padding: 5% 2.5%;">Atrativos Incríveis</h2>
        </div>

        <div class="line" style="padding:0;">

            @foreach($p_FeaturedAttractions as $c_Index => $c_Attraction)
                <?php
                    $v_GridClasses = $c_Index == 0 ? 'col-lg-6 col-md-12 atrativo-big' : 'col-lg-3 col-md-6 atrativo-small';
                    $v_Description = json_decode($c_Attraction->descricao_curta,1)[$p_Language];
                    if($c_Attraction->trade)
                        $v_Name = $c_Attraction->nome;
                    else
                        $v_Name = json_decode($c_Attraction->nome,1)[$p_Language];
                ?>
                <div class="{{$v_GridClasses}} col-sm-12 col-xs-12 list-thumbs-full">
                    <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Attraction->slug)}}">
                        <div class="hoverzoom">
                            <div class="thumbs-full">
                                <img src="{{$c_Attraction->url}}" alt="{{$v_Name}}" title="{{$v_Name}}">
                            </div>
                            <div class="retina-hover">
                                <div class="col-lg-12 title">
                                    <p>{{$v_Name}}</p>
                                </div>
                                <div class="col-lg-12 no-padding">
                                    <hr>
                                </div>
                                <div class="col-lg-12 text">
                                    <p>{{$v_Description}}</p>
                                </div>
                            </div>
                            <div class="retina">
                                <p>{{$v_Name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
               
        </div>
    </div>
<!--End atrativos-->


    <div class="container">
        <div class="col-lg-12">
            <h2 class="text-center cyan" style="padding: 0 2.5% 5% 2.5%;">{{$p_Content == null ? '' : $p_Content->titulo_destinos}}</h2>
        </div>

        <div class="line" style="padding:0;">
            @foreach($p_Destinations as $c_Index => $c_Destination)
                @if($c_Index < 2)
                    <?php $v_Description = json_decode($c_Destination->descricao_curta,1)[$p_Language]; ?>
                    <div class="col-lg-6 col-md-6 list-thumbs-full">
                        <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                            <div class="hoverzoom">
                                <div class="thumbs-full">
                                    <img src="{{$c_Destination->url}}">
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>{{$c_Destination->nome}}</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p>{{$v_Description}}</p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>{{$c_Destination->nome}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="line" style="padding:0;">
            @foreach($p_Destinations as $c_Index => $c_Destination)
                @if($c_Index >= 2)
                    <?php $v_Description = json_decode($c_Destination->descricao_curta,1)[$p_Language]; ?>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                   <div class="thumbs-mini-recorte" style="background: url('{{$c_Destination->url}}') no-repeat;"></div> 
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>{{$c_Destination->nome}}</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p>{{$v_Description}}</p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>{{$c_Destination->nome}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="col-lg-12" id="call-to-action">
            <a href="{{url($p_Language . '/destinos')}}">{{trans('menu.more_destinations')}}</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="container">
            <div class="col-lg-12 nav-tabs text-center" style="padding:2% 0;">
                <ul class="nav nav-tabs tarja-cyan" role="tablist" style="display: inline-flex;">
                    <li class="nav-item">
                        <a class="nav-link active" href="#hotel" role="tab" data-toggle="tab" >
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-10 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/camaOrange.gif')}}" alt="">
                                    <span class="hidden-md-down">{{trans('destination.accomodation')}}</span>
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#alimentacao" role="tab" data-toggle="tab" >
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-10 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/talherOrange.gif')}}" alt="">
                                   <span class="hidden-md-down">{{trans('destination.feeding')}} </span>
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#servico" role="tab" data-toggle="tab" >
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-10 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/malaOrange.gif')}}" alt="">
                                   <span class="hidden-md-down">{{trans('destination.services')}}</span>
                                </p>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#lazer" role="tab" data-toggle="tab" >
                            <div class="col-lg-3 col-md-2 col-sm-12 col-xs-10 hospedagem">
                                <p><img src="{{url('/portal/assets/imgs/coposOrange.gif')}}" alt="">
                                  <span class="hidden-md-down">{{trans('destination.recreation')}}</span>
                                </p>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">

            <!-- FRAGMENTO ATUALIZADO 18/03/2016-->

            <!-- Hotels -->
            <div role="tabpanel" class="tab-pane active" id="hotel">
                <div class="col-lg-12 box-hospedagem" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_accomodation')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_AccomodationList = [
                                            '' => trans('destination.accomodation_type'),
                                            '43' => trans('destination.accomodation_flat'),
                                            '51' => trans('destination.accomodation_camping'),
                                            '52' => trans('destination.accomodation_summer_camp'),
                                            '38,39' => trans('destination.accomodation_hotel'),
                                            '40,42' => trans('destination.accomodation_farm_hotel'),
                                            '53' => trans('destination.accomodation_hostel'),
                                            '41' => trans('destination.accomodation_inn'),
                                            '47,48,54' => trans('destination.others')
                                        ];
                                    ?>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_acomodacao', $v_AccomodationList, null, ['id' => 'tipo_acomodacao', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Alimentação -->
            <div role="tabpanel" class="tab-pane" id="alimentacao">
                <div class="col-lg-12 box-restaurante" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_feeding')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_FoodDrinkList = [
                                            '' => trans('destination.feeding_type'),
                                            '55' => trans('destination.feeding_restaurants'),
                                            '56,59,61' => trans('destination.feeding_bars'),
                                            '57' => trans('destination.feeding_tea_house'),
                                            '58' => trans('destination.feeding_breweries'),
                                            '60' => trans('destination.feeding_ice_cream_shops'),
                                            '62' => trans('destination.others')
                                        ];
                                    ?>
                                    <div class="col-lg-3  col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_estabelecimento', $v_FoodDrinkList, null, ['id' => 'tipo_estabelecimento', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Serviços -->
            <div role="tabpanel" class="tab-pane" id="servico">
                <div class="col-lg-12 box-passeio" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_service')}}</p>
                                    </div>
                                    <div class="col-lg-3  col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_ServiceList = [
                                            '' => trans('destination.service_type'),
                                            '64,256' => trans('destination.services_agencies'),
                                            '66' => trans('destination.services_rental_agencies')
                                        ];
                                    ?>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_servico', $v_ServiceList, null, ['id' => 'tipo_servico', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Lazer -->
            <div role="tabpanel" class="tab-pane" id="lazer">
                <div class="col-lg-12 box-lazer" id="tarja">
                    <div class="container no-padding">
                        <div class="col-lg-12 no-padding">
                            <div class="row" id="options">
                                {!! Form::open(array('method' => 'get', 'class' => 'destinationSupport', 'onsubmit' => 'return submitTradeForm(this)')) !!}
                                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 ">
                                        <p>{{trans('destination.find_recreation')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12" id="inputDestino">
                                        {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['class' => 'selectCities form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                                    </div>
                                    <?php
                                        $v_RecreationList = [
                                            '' => trans('destination.recreation_type'),
                                            '83,85' => trans('destination.recreation_nightclubs'),
                                            '84' => trans('destination.recreation_shows'),
                                            '86' => trans('destination.recreation_theater'),
                                            '77,78,79,87' => trans('destination.recreation_sports'),
                                            '76' => trans('destination.recreation_clubs'),
                                            '74' => trans('destination.recreation_parks'),
                                            '80,81,88,89' => trans('destination.recreation_others')
                                        ];
                                    ?>
                                    <div class="col-lg-3 col-md-5 col-sm-12 col-xs-12 selectoptions">
                                        {!! Form::select('tipo_lazer', $v_RecreationList, null, ['id' => 'tipo_lazer', 'required' => 'required']) !!}
                                    </div>
                                    <div class="col-lg-1 col-md-2 col-sm-12 col-xs-12 " id="submit-tarja">
                                        <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FRAGMENTO ATUALIZADO 18/03/2016-->
        </div>
    </div>
</div>


<div class="row-fluid" id="titleVisiteMinas">
    <div class="container">
        <div class="col-lg-12">
            <h2 class="margint cyan">{{$p_Content == null ? '' : $p_Content->titulo_instagram}}</h2>
         
        </div>
    </div>
</div>

<div class="row-fluid" id="visiteMinas" style="background:#FFFFFF;padding:2rem 0;">
    @if(count($p_InstagramPictures) >= 12)
    <div class="container">
        <div class="col-lg-12" id="gridInsta">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom:15px;">
                <div class="card card-inverse instagram-photo" style="max-height: 335px;">
                    <a href="{{$p_InstagramPictures[0]['link']}}" id="viewInsta" target="_blank">
                        <img class="card-img" src="{{$p_InstagramPictures[0]['images']['standard_resolution']['url']}}">
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom:15px;" >
                <div class="card card-inverse instagram-photo" style="max-height: 335px;">
                    <a href="{{$p_InstagramPictures[1]['link']}}" id="viewInsta" target="_blank">
                        <img class="card-img" src="{{$p_InstagramPictures[1]['images']['standard_resolution']['url']}}">
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hidden-xs-down">
                <div class="row" style="margin-bottom:30px;">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[2]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[2]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[3]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[3]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom:30px;">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[4]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[4]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[5]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[5]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 hidden-xs-down">
                <div class="row" style="margin-bottom:30px;">
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[6]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[6]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[7]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[7]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom:30px;">
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[8]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[8]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6  col-md-6 col-sm-6 col-xs-12">
                        <div class="card card-inverse instagram-photo" style="max-height: 152px;">
                            <a href="{{$p_InstagramPictures[9]['link']}}" id="viewInsta" target="_blank">
                                <img class="card-img" src="{{$p_InstagramPictures[9]['images']['standard_resolution']['url']}}" style="min-height: 152px;">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom:15px;">
                <div class="card card-inverse instagram-photo" style="max-height: 335px;">
                    <a href="{{$p_InstagramPictures[10]['link']}}" id="viewInsta" target="_blank">
                        <img class="card-img" src="{{$p_InstagramPictures[10]['images']['standard_resolution']['url']}}">
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="margin-bottom:15px;">
                <div class="card card-inverse instagram-photo" style="max-height: 335px;">
                    <a href="{{$p_InstagramPictures[11]['link']}}" id="viewInsta" target="_blank">
                        <img class="card-img" src="{{$p_InstagramPictures[11]['images']['standard_resolution']['url']}}">
                    </a>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>


<div class="row-fluid" id="visiteMinasHashs" style="background:#FFFFFF;padding:0 0 2rem 0;">
    <div class="container">
        <div class="col-lg-12" id="gridInsta">
            <div class="col-lg-12" id="hashtext">
                <p>{!! $p_Content == null ? '' : nl2br($p_Content->texto_instagram) !!}</p>
            </div>

            <div class="col-lg-12" id="call-to-action">
                <a href="{{url($p_Language . '/doacao-de-midias')}}">{{trans('home.send_photos')}}</a>
            </div>
        </div>
    </div>
</div>


<?php
    if($p_Apps != null){
        $v_Apps = json_decode($p_Apps,1);
        $v_HasApps = $v_Apps['tem_dados'];
        $v_AppsData = $v_Apps['data'];
    }
?>
@if($p_Apps != null && $v_HasApps == 1)
<div class="row-fluid">
    <div class="container">
        <div class="col-sm-12 line text-center" style="padding:2% 0;" >
            <h2 style="padding: 6% 0; color: #17a2b8;">{{$p_Content == null ? '' : $p_Content->titulo_aplicativos}}</h2>
        </div>

        <div class="col-sm-offset-2 line" style="padding:2% 0;">

            @foreach($v_AppsData as $c_App)
                @if($c_App['completo'] == 1)
                <div class="col-lg-3 list-thumbs-full text-center" style="display: inline-block; float: none;">
                    <div class="app-donwload">
                        @if($c_App['tipo'] == 'web')
                        <a href="{{$c_App['url']}}" target="_blank">
                        @endif
                        <div class="thumbs-full">
                            <img src="{{$c_App['foto_capa_url']}}" alt="app">
                        </div>
                        <div class="col-lg-12 title">
                            <p style="color: #17a2b8;">{{$c_App['nome']}}</p>
                        </div>
                        @if($c_App['tipo'] == 'web')
                        </a>
                        @else
                            <div class="col-lg-12 text">
                                <p>
                                    @if(!empty($c_App['url_android']))
                                        <a href="{{$c_App['url_android']}}" target="_blank"><img src="{{url('/portal/assets/imgs/android.png')}}" width="65" alt="play store"></a>
                                    @endif
                                    @if(!empty($c_App['url_ios']))
                                        <a href="{{$c_App['url_ios']}}" target="_blank"><img src="{{url('/portal/assets/imgs/apple.png')}}" width="60" alt="app store"></a>
                                    @endif
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
                @endif
            @endforeach

        </div>
    </div>
</div>
@endif

<div class="row-fluid" id="titleVisiteMinas">
    <div class="container">
        <div class="col-lg-12">
            <h2 class="marginb cyan">Venha viver sua história</h2>
        </div>
    </div>
</div>

<div class="row-fluid">
     <div class="container">
            <div id="video-container">
                <img src="{{url('/portal/assets/imgs/btn_video.png')}}" class="btn_video" id="play-pause">
                <!-- Video -->
                <video id="video" width="100%" poster="http://i3.ytimg.com/vi/cyp0R5LKtlA/maxresdefault.jpg">
                  <source src="{{url('/portal/assets/imgs/turismo_em_minas_gerais_SETUR_2016.mp4')}}" type="video/mp4">
                </video>
                <!-- Video Controls -->
                <div id="video-controls">
                    <button type="button" id="play-pause-dois" class="play">Play</button>
                    <input type="range" id="seek-bar" value="0">
                    <button type="button" id="mute">Mute</button>
                    <input type="range" id="volume-bar" min="0" max="1" step="0.1" value="1">
                    <button type="button" id="full-screen">Full-Screen</button>
                </div>
            </div>

    </div>
</div>

</div>

@stop

@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $('.select2').select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );
             
            $('.btn_video').click(function() {
                $(this).addClass("hidden");
                $('#video-controls').addClass("show");
                $('.btn_video').removeClass("show");
            });
            $('.play').click(function() {
                $('#video-controls').removeClass("show");
                 $('#video-controls').addClass("hidden");
                
            });

        });

        function submitTradeForm(p_Form){
            var v_Slug = $(p_Form).find('.selectCities').val();
            $(p_Form).attr('action', '{{url($p_Language . '/apoio-destino')}}/' + v_Slug);
            return true;
        }

       
    </script>

@stop