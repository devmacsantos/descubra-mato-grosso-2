@extends('public.base')
@section('pageCSS')
@stop
@section('main-content')
    <?php
        if($p_Content != null)
        {
            $v_TypeName = json_decode($p_Content->nome,1)[$p_Language];
            $v_TypeDescription = json_decode($p_Content->descricao,1)[$p_Language];
        }
    ?>
    <div class="row-fluid banner-oquefazer" id="destinos" style="padding:0;">
        <div class="col-lg-12 no-padding">
            <div class="caption" style="opacity:1;width: 100%;height: 100%;top:0;">
                <div class="caption-content">
                    <div class="container">
                        <div class="col-lg-12 no-padding">
                            <div class="col-lg-12 text-highlight" style="margin: 2.3% 0 0;">
                                <h2  style="text-align:center;">{{$p_Content == null ? '' : $v_TypeName}}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- FRAGMENTO ATUALIZADO -->

            <div class="highlight-active">
                <!--<img src="{{$p_Content->foto_capa_url}}" alt="">-->
                 <div style="background: url('{{$p_Content->foto_capa_url}}') no-repeat;" class="corte-destaque-interna2"></div>
            </div>

            <!-- FRAGMENTO ATUALIZADO -->

        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2" style="padding:3rem 0 0;">
                    <div class="col-lg-12" style="margin-bottom:50px;">
                        <p>{!! $p_Content == null ? '' : nl2br($v_TypeDescription) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if(count($p_Places) > 0)
        <div class="row-fluid" id="destinos">
            <div class="container">
                <div class="col-lg-12 line">
                    @foreach($p_Places as $c_Place)
                        <?php
                            $v_Description = json_decode($c_Place['descricao_curta'],1)[$p_Language];
                            if($c_Place['tipo'] == 'roteiros' || ($c_Place['tipo'] == 'atracoes' && $c_Place['trade'] == 0))
                                $v_Name = json_decode($c_Place['nome'],1)[$p_Language];
                            else
                                $v_Name = $c_Place['nome'];
                        ?>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        @if($c_Place['tipo'] == 'atracoes' || $c_Place['tipo'] == 'eventos') 
                            <a href="{{url($p_Language . '/' . $c_Place['tipo'] . '/' . $c_Place['slug'])}}">
                        @else
                            <a href="{{url($p_Language . '/' . $c_Place['tipo'] . '/' . $c_Place['slug'])}}">
                        @endif
                            <div class="hoverzoom">
                                    <div class="thumbs-mini-four">
                                        <!--<img src="{{$c_Place['url']}}">-->
                                        <div class="thumbs-mini-recorte" style="background: url('{{$c_Place['url']}}') no-repeat;"></div>
                                    </div>
                                    <div class="retina-hover" >
                                        <div class="col-lg-12 title">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
@stop

@section('pageScript')
@stop