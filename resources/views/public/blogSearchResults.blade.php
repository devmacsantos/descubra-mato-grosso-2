@extends('public.base')
@section('pageCSS')
@stop
@section('main-content')
    <div class="row-fluid" id="destinos" style="margin-top:180px;padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" style="margin-bottom:50px;">
                        <p id="statusResult">{{trans('search.search_results_for')}}</p>
                        @if($p_Hashtag != null && strlen($p_Hashtag) > 0)
                            <p id="result">" #{{$p_Hashtag}} "</p>
                        @elseif($p_Archive != null && strlen($p_Archive) > 0)
                            <p id="result">" {{$p_Archive}} "</p>
                        @else
                            <p id="result">" {{$p_SearchString}} "</p>
                        @endif
                    </div>
                </div>
            </div>

            @if(count($p_Articles) > 0)
                <div class="col-lg-12" id="proximidades">
                    <div class="col-lg-12" style="padding:2% 1%;">
                        <div class="content-result">
                            <span id="span-center">{{count($p_Articles) . ' ' . trans_choice('search.occurrences',count($p_Articles))}}</span>
                            <span id="span-value" >
                                <hr>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- FRAGMENTO ATUALIZADO -->
                <div class="col-lg-12 line">
                    @foreach($p_Articles as $c_Article)
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a href="{{url($p_Language . '/blog/artigo/' . $c_Article->slug)}}">
                                <p style="margin-bottom:0.3rem;color:#b1b1b4;font-size: 14px;">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Article->created_at)->format('d/m/Y')}}</p>
                                <div class="hoverzoom" style="margin:0;">
                                <div class="thumbs-mini-three">
                                   <div class="thumbs-mini-recorte" style="background: url('{{$c_Article->foto_capa_url}}') no-repeat;"></div> 
                                </div>
                                    
                                </div>
                                <p style="margin-bottom:0.5rem;color:#b1b1b4">
                                    @foreach($c_Article->hashtags as $c_Hashtag)
                                        #{{$c_Hashtag}}
                                    @endforeach
                                </p>
                                <p style="color:#67676c">{{json_decode($c_Article->titulo,1)[$p_Language]}}</p>
                                <a class="laranja" href="{{url($p_Language . '/blog/artigo/' . $c_Article->slug)}}">{{trans('institutional.read_more')}}</a>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@stop

@section('pageScript')
@stop