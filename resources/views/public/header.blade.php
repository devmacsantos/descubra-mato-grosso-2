<?php
    $v_HeaderDestinations = \App\Destination::getHeaderDestinations(7);
    $v_HeaderParks = \App\Attraction::getParks(7);
    $v_HeaderRoutes = \App\TouristicRoute::getRoutes(4);
    $v_HeaderTypes = \App\TripType::getHeaderTypes(6);
    $v_Languages = ['pt' => 'pt', 'en' => 'en', 'es' => 'es', 'fr' => 'fr'];
    if(!in_array($p_Language, $v_Languages))
        $p_Language = 'pt';
    unset($v_Languages[$p_Language]);
    $v_Languages = array_keys($v_Languages);
?>
<style>
    .language-dropdown::after {
        display: inline-block;
        width: 0;
        height: 0;
        margin-left: 0.25rem;
        vertical-align: middle;
        content: "";
        visibility: visible;
        border-top: 0.3em solid;
        border-right: 0.3em solid transparent;
        border-left: 0.3em solid transparent;
    }
    .dropdown-toggle:after {
        display: none;
    }
    .open .dropdown-menu.lang-menu{
        display: block !important;
    }
    #list-city.favorites-list li a{
        color: #fff;
    }
        
    #nav-icon1{
    width: 30px;
    height: 25px;
    position: relative;
    /*margin: 50px auto;*/
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: .5s ease-in-out;
    -moz-transition: .5s ease-in-out;
    -o-transition: .5s ease-in-out;
    transition: .5s ease-in-out;
    cursor: pointer;
    }

    #nav-icon1 span {
    display: block;
    position: absolute;
    height: 6px;
    width: 100%;
    background: #666;
    border-radius: 0px;
    opacity: 1;
    left: 0;
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
    -webkit-transition: .25s ease-in-out;
    -moz-transition: .25s ease-in-out;
    -o-transition: .25s ease-in-out;
    transition: .25s ease-in-out;
    }

    #nav-icon1 span:nth-child(1) {
    top: 0px;
    }

    #nav-icon1 span:nth-child(2) {
    top: 10px;
    }

    #nav-icon1 span:nth-child(3) {
    top: 20px;
    }

    #nav-icon1.open span:nth-child(1) {
    top: 12px;
    -webkit-transform: rotate(135deg);
    -moz-transform: rotate(135deg);
    -o-transform: rotate(135deg);
    transform: rotate(135deg);
    }

    #nav-icon1.open span:nth-child(2) {
    opacity: 0;
    left: -60px;
    }

    #nav-icon1.open span:nth-child(3) {
    top: 12px;
    -webkit-transform: rotate(-135deg);
    -moz-transform: rotate(-135deg);
    -o-transform: rotate(-135deg);
    transform: rotate(-135deg);
    }
    .link-gastronomia{
        width: 110px;
        margin: -4px 0 0px 0;
    }
</style>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation" style="padding:0;">
   
    <!-- MENU RESUMIDO -->
    <div class="row-fluid hidden-sm-down menu-resumido-desk" id="box-menu-top-resume">
        <header>
            <div class="container no-padding">
                <div class="col-lg-2 col-md-3 col-sm-6  col-xs-6 logo">
                    <a href="{{url($p_Language)}}">
                        <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Mato Grosso" title="Mato Grosso">
                    </a>
                </div>
                <?php  $today = date("Y-m-d H:i:s");  $date = get_limite_data_especial();  ?>
                    <?php  if ($today < $date){ ?>
                        <div class="col-lg-2 col-md-3 col-sm-6  col-xs-6 logo">
                            <a href="{{url('/pt/semana-santa')}}">
                                <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Semana Santa" title="Mato Grosso">
                            </a>
                        </div>
                    <?php } ?> 
                <div class="col-lg-10 col-md-9 col-sm-12  col-xs-12 no-padding">
                    <div class="row-fluid">
                        <nav class="navbar navbar-light nav-inferior">
                            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar1">
                                <!-- &#9776; -->
                                <img src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Mato Grosso" title="Mato Grosso">
                            </button>
                            <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar1">
                                <ul class="nav navbar-nav " id="menu-inferior" style="float: right;">
                                    <!--
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/conheca')}}">{{trans('menu.discover')}}</a>
                                    </li>
									-->
                                    <!-- O QUE FAZER -->
                                    <li class="nav-item">
                                        <a class="nav-link" href="javascript:;" id="dropdownResumeMenu1">{{trans('menu.what_to_do')}}</a>
                                    </li>
                                    <!-- PARA ONDE IR -->
                                    <li class="nav-item">
                                        <a class="nav-link" href="javascript:;" id="dropdownResumeMenu2">{{trans('menu.where_to_go')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}">{{trans('menu.explore_the_map')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="javascript:;" id="dropdownResumeMenu3">{{trans('menu.plan_your_trip')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/eventos')}}">{{trans('menu.events')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pictures" href="javascript:;" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                        <div class="col-lg-4 dropdown-menu" aria-labelledby="dropdownMenu3" id="divDropdownResume">
                                            <div class="col-lg-12 no-padding list-thumbs">
                                                <h4 style="padding:10% 0 5%;color:#FFFFFF;">{{trans('menu.favorites')}}</h4>
                                                <ul id="list-city" class="favorites-list">
                                                </ul>
                                            </div>
                                            <div class="col-lg-12" id="call-to-action">
                                                <a href="{{url($p_Language . '/favoritos')}}">{{trans('menu.show_all')}}</a>
                                            </div>

                                        </div>
                                    </li>
                                   
                                    <li class="nav-item" id="dropdownResumeMenu4">
                                        <div id="nav-icon1">
                                          <span></span>
                                          <span></span>
                                          <span></span>
                                        </div>
                                    </li>
                                    
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- O que fazer -->
            <div class="row-fluid" id="listDropdownResumidoMenu1">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class="col-lg-12 no-padding">
                        @foreach($p_HeaderCategories as $c_Index => $c_Category)
                        <?php $v_Name = json_decode($c_Category->nome,1)[$p_Language]; ?>
                        <div class="col-lg-2-5 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/o-que-fazer/' . $c_Category->slug)}}">
                                <div class="card card-inverse">
                                    <img class="card-img" src="{{$c_Category->foto_capa_url}}" width="518" height="264" alt="Card image">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12" id="title-overlay">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @if(count($v_HeaderTypes) > 0)
                    <div class="col-lg-12 no-padding text-highlight">
                        <h2>{{trans('menu.what_do_you_want')}}</h2>
                        <div id="choices">
                            @foreach($v_HeaderTypes as $c_Index => $c_Type)
                            <?php $v_Name = json_decode($c_Type->nome,1)[$p_Language]; ?>
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 choices">
                                <a class="options" href="{{url($p_Language . '/o-que-fazer/' . $c_Type->category_slug . '/' . $c_Type->slug)}}"><p style="padding:5px;">{{$v_Name}}</p></a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/o-que-fazer')}}">{{trans('menu.discover_what_to_do')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Para onde ir -->
            <div class="row-fluid" id="listDropdownResumidoMenu2">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class="col-lg-12 no-padding">
                        <div class="col-lg-6 no-padding">
                            @foreach($v_HeaderRoutes as $c_Index => $c_Route)
                            <?php $v_RouteName = json_decode($c_Route->nome,1)[$p_Language]; ?>
                            <div class="col-lg-6 col-sm-3 col-md-6 col-xs-6" style="padding-left:0;">
                                <a href="{{url($p_Language . '/roteiros/' . $c_Route->slug)}}">
                                    <div class="card card-inverse">
                                        <img class="card-img" src="{{$c_Route->url}}" width="518" height="264" alt="Card image">
                                        <div class="card-img-overlay overlay-full">
                                            <div class="col-lg-12" id="title-overlay">
                                                <p>{{$v_RouteName}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            <div class="col-lg-3 pull-right" id="call-to-action-min">
                                <a href="{{url($p_Language . '/roteiros')}}">+ {{trans('menu.routes')}}</a>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-12 no-padding list-thumbs">
                                    <h4 class="branco">{{trans('menu.cities_and_districts')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderDestinations as $c_Index => $c_Destination)
                                            <li>
                                                <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                                                    {{$c_Destination->nome}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-lg-6 pull-right" id="call-to-action-min"  style="padding: 0.4rem 1%;!important;">
                                    <a href="{{url($p_Language . '/destinos')}}">+ {{trans('menu.destinations')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-12 no-padding list-thumbs">
                                    <h4 class="branco">{{trans('menu.parks_and_reservations')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderParks as $c_Index => $c_Park)
                                            <?php $v_Name = json_decode($c_Park->nome,1)[$p_Language]; ?>
                                            <li>
                                                <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Park->slug)}}">
                                                    {{$v_Name}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-lg-6 pull-right" id="call-to-action-min" style="padding: 0.4rem 1%;!important;">
                                    <a href="{{url($p_Language . '/parques')}}">+ {{trans('menu.parks')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/para-onde-ir')}}">{{trans('menu.more_destinations')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Planeje sua viagem -->
            <div class="row-fluid" id="listDropdownResumidoMenu3">
                <div class="container no-padding">
                    <div class="col-lg-12 no-padding">
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=1')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 text-center no-padding" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_1_pt1')}}</small><br>{{trans('menu.routes_duration_1_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=2')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 text-center no-padding" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_2_pt1')}}</small><br>{{trans('menu.routes_duration_2_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=3')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 text-center no-padding" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_3_pt1')}}</small><br>{{trans('menu.routes_duration_3_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#tempo')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/termometro.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.weather_forecast')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#como-chegar')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/destino.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.directions')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-12 text-highlight">
                        <div class="col-lg-3" id="call-to-action">
                            <a href="{{url($p_Language . '/planeje-sua-viagem')}}">{{trans('menu.see_how_plan_trip')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Icone destino menu resumido -->
            <div class="row-fluid" id="listDropdownResumidoMenu4">
                <div class="container no-padding">
                    <div class="col-lg-7">
                        <nav class="navbar navbar-light nav-superior">
                            <ul class="nav navbar-nav navbar-right pull-right" id="menu-superior">                                
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/blog')}}">Blog</a>
                                </li>
                            
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/fale-conosco')}}">{{trans('menu.contact')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url('/admin/login')}}" target="_blank">{{trans('menu.register_your_business')}}</a>
                                </li>
      
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 no-padding">
                        {!! Form::open(array('method' => 'get', 'id' => 'formSearch', 'url'=> url($p_Language . '/busca'))) !!}
                            <div class="input-group">
                                <input name="q" type="text" class="form-control" placeholder="{{trans('menu.search_in_site')}}" required>
								<span class="input-group-btn" id="btn-search">
									<button class="btn btn-secondary btn-submit" type="submit" id="btn-submit">
                                    </button>
								</span>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-lg-2 no-padding">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="nav-item language-links">
                                @foreach($v_Languages as $c_Index => $c_Language)
                                    <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $c_Language . '.png')}}">
                                    <a href="{{url('/' . $c_Language)}}">{{$c_Language}}</a>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <!-- MENU COMPLETO -->
    <div class="row-fluid hidden-md-down hidden-sm-down" id="box-menu-top">
        <header>
            <div class="container no-padding">
                <div class="row-fluid">
                    <div class="col-lg-3 col-md-2 col-sm-3 col-xs-3 no-padding logo">
                        <a class="navbar-brand mb-0" href="{{url($p_Language)}}">
                            <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Mato Grosso" title="Mato Grosso">
                        </a>
                    </div>

        <?php  if ($today < $date){ ?>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 no-padding logo">
            <a  href="{{url('/pt/semana-santa')}}">
                <img class="img-fluid" src="{{url('/assets/img/semanasanta/logo.png')}}" alt="Semana Santa" title="Mato Grosso">
            </a>
        </div>
        <?php } ?>                
                    <div class="col-lg-7 col-md-8 col-sm-6 col-xs-6 no-padding">
                        <div class="row-fluid">
                            <nav class="navbar navbar-light nav-superior">
                                <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar1">
                                    <ul class="nav navbar-nav navbar-right pull-right" id="menu-superior">
                                        <!--
										<li class="nav-item" style="margin-top: 0.2rem;">
                                            <a class="nav-link" href="https://www.facebook.com/VisiteMinasGerais/" target="_blank">
                                                <img src="{{url('/portal/assets/imgs/facebookGray.png')}}" alt="Facebook" title="Facebook">
                                            </a>
                                        </li>
                                        <li class="nav-item"  style="margin-left: 0.5rem;margin-right:1rem;margin-top: 0.2rem;">
                                            <a class="nav-link" href="https://instagram.com/visiteminasgerais" target="_blank">
                                                <img src="{{url('/portal/assets/imgs/instagramGray.png')}}" alt="Instagram" title="Instagram">
                                            </a>
                                        </li>
                                        -->
										<!--
                                        <li class="nav-item">
                                            <a class="nav-link strings" href="{{url('/maisgastronomia')}}" title="Mais Gastronomia" target="_blank"><img src="{{url('/portal/assets/imgs/logo_mais_gastronomia.png')}}" class="link-gastronomia"></a>
                                        </li>
                                        -->
                                        <!--
										<li class="nav-item">
                                            <a class="nav-link strings" href="{{url($p_Language . '/minas360')}}" title="Minas em 360">Minas em 360°</a>
                                        </li>
                                        -->
                                        <li class="nav-item">
                                            <a class="nav-link strings" href="{{url($p_Language . '/blog')}}">Blog</a>
                                        </li>
                                        
                                        <li class="nav-item">
                                            <a class="nav-link strings" href="{{url($p_Language . '/fale-conosco')}}">{{trans('menu.contact')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link strings" href="{{url('/admin')}}" target="_blank">{{trans('menu.register_your_business')}}</a>
                                        </li>
                                        
                                        <li class="dropdown nav-item language-links-dark">
                                            <span class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                <div class="language-dropdown" id="dLabel">
                                                    <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $p_Language . '.png')}}">
                                                    <span>{{$p_Language}}</span>
                                                    <span class="caret"></span>
                                                </div>
                                            </span>
                                            <ul class="dropdown-menu lang-menu" aria-labelledby="dLabel">
                                                @foreach($v_Languages as $c_Index => $c_Language)
                                                    <li class="lang-item">
                                                        <a href="{{url('/' . $c_Language)}}" class="">
                                                            <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $c_Language . '.png')}}">
                                                            {{$c_Language}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>

                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="col-lg-12 no-padding">
                        <nav class="navbar navbar-light nav-inferior">
                            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar2">
                                <!--  &#9776; -->
                                <img src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="">
                            </button>
                            <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar2">
                                <div class="col-lg-9 no-padding">
                                    <ul class="nav navbar-nav " id="menu-inferior">
                                        <!--
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url($p_Language . '/conheca')}}">{{trans('menu.discover')}}</a>
                                        </li>
										-->
                                        <li class="nav-item" id="dropdownMenu1">
                                            <a class="nav-link" href="javascript:;" id="dropdownMenu1_anchor">{{trans('menu.what_to_do')}}</a>
                                        </li>
                                        <li class="nav-item" id="dropdownMenu2">
                                            <a class="nav-link" href="javascript:;" id="dropdownMenu2_anchor">{{trans('menu.where_to_go')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}">{{trans('menu.explore_the_map')}}</a>
                                        </li>
                                        <li class="nav-item" id="dropdownMenu3">
                                            <a class="nav-link" href="javascript:;" id="dropdownMenu3_anchor">{{trans('menu.plan_your_trip')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url($p_Language . '/eventos')}}">{{trans('menu.events')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link pictures" href="javascript:;" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                            <div class="col-lg-4 dropdown-menu" aria-labelledby="dropdownMenu3" id="divDropdown">
                                                <div class="col-lg-12 no-padding">

                                                    <div class="col-lg-12 list-thumbs">
                                                        <h4 class="branco" style="padding:10% 0 5%;">{{trans('menu.favorites')}}</h4>
                                                        <ul id="list-city" class="favorites-list">
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-12" id="call-to-action">
                                                        <a href="{{url($p_Language . '/favoritos')}}">{{trans('menu.show_all')}}</a>
                                                    </div>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 no-padding">
                                    {!! Form::open(array('method' => 'get', 'id' => 'formSearch', 'url'=> url($p_Language . '/busca'))) !!}
                                        <div class="input-group" style="width: 100%;display:block;">
                                            <input name="q" type="text" class="form-control" placeholder="{{trans('menu.search_in_site')}}" required>
											<span class="input-group-btn" id="btn-search">
												<button class="btn btn-secondary btn-submit" type="submit" id="btn-submit"></button>
											</span>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- O que fazer -->
            <div class="row-fluid" id="listDropdownMenu1">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class="col-lg-12 no-padding">
                        @foreach($p_HeaderCategories as $c_Index => $c_Category)
                        <?php $v_Name = json_decode($c_Category->nome,1)[$p_Language]; ?>
                        <div class="col-lg-2-5 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/o-que-fazer/' . $c_Category->slug)}}">
                                <div class="card card-inverse">
                                    <img class="card-img" src="{{$c_Category->foto_capa_url}}" width="518" height="264" alt="Card image">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12" id="title-overlay">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @if(count($v_HeaderTypes) > 0)
                    <div class="col-lg-12 no-padding text-highlight">
                        <h2>{{trans('menu.what_do_you_want')}}</h2>
                        <div id="choices">
                            @foreach($v_HeaderTypes as $c_Index => $c_Type)
                            <?php $v_Name = json_decode($c_Type->nome,1)[$p_Language]; ?>
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 choices">
                                    <a class="options" href="{{url($p_Language . '/o-que-fazer/' . $c_Type->category_slug . '/' . $c_Type->slug)}}"><p style="padding:5px;">{{$v_Name}}</p></a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/o-que-fazer')}}">{{trans('menu.discover_what_to_do')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Para onde ir -->
            <div class="row-fluid" id="listDropdownMenu2">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class="col-lg-12 no-padding">
                        <div class="col-lg-6 no-padding">
                            @foreach($v_HeaderRoutes as $c_Index => $c_Route)
                            <?php $v_RouteName = json_decode($c_Route->nome,1)[$p_Language]; ?>
                            <div class="col-lg-6 col-sm-3 col-md-6 col-xs-6" style="padding-left:0;">
                                <a href="{{url($p_Language . '/roteiros/' . $c_Route->slug)}}">
                                    <div class="card card-inverse">
                                        <img class="card-img" src="{{$c_Route->url}}" width="518" height="264" alt="Card image">
                                        <div class="card-img-overlay overlay-full">
                                            <div class="col-lg-12" id="title-overlay">
                                                <p>{{$v_RouteName}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            <div class="col-lg-3 pull-right" id="call-to-action-min">
                                <a href="{{url($p_Language . '/roteiros')}}">+ {{trans('menu.routes')}}</a>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-12 no-padding list-thumbs">
                                    <h4 class="branco">{{trans('menu.cities_and_districts')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderDestinations as $c_Index => $c_Destination)
                                            <li>
                                                <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                                                    {{$c_Destination->nome}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                               
                            </div>
                             <div class="col-lg-6 pull-right" id="call-to-action-min"  style="padding: 0.4rem 1%;!important;">
                                <a href="{{url($p_Language . '/destinos')}}">+ {{trans('menu.destinations')}}</a>
                            </div>
                        </div>

                        <div class="col-lg-3 ">
                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-12 no-padding list-thumbs">
                                    <h4 class="branco">{{trans('menu.parks_and_reservations')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderParks as $c_Index => $c_Park)
                                            <?php $v_Name = json_decode($c_Park->nome,1)[$p_Language]; ?>
                                            <li>
                                                <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Park->slug)}}">
                                                    {{$v_Name}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                
                            </div>
                            <div class="col-lg-6 pull-right" id="call-to-action-min" style="padding: 0.4rem 1%;!important;">
                                    <a href="{{url($p_Language . '/parques')}}">+ {{trans('menu.parks')}}</a>
                                </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/para-onde-ir')}}">{{trans('menu.more_destinations')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Planeje sua viagem -->
            <div class="row-fluid" id="listDropdownMenu3">
                <div class="container no-padding">
                    <div class="col-lg-12 no-padding">
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=1')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_1_pt1')}}</small><br>{{trans('menu.routes_duration_1_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=2')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_2_pt1')}}</small><br>{{trans('menu.routes_duration_2_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=3')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_3_pt1')}}</small><br>{{trans('menu.routes_duration_3_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#tempo')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/termometro.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.weather_forecast')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#como-chegar')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/destino.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.directions')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-12 text-highlight">
                        <div class="col-lg-3" id="call-to-action">
                            <a href="{{url($p_Language . '/planeje-sua-viagem')}}">{{trans('menu.see_how_plan_trip')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>


     <!-- MENU RESUMIDO MOBILE-->
    <div class="row-fluid hidden-md-up" id="box-menu-top-resume-mobile">
        <header>
            <div class="container no-padding">
                <div class="col-lg-2 col-md-3 col-sm-5 col-xs-5">
                    <a href="{{url($p_Language)}}">
                        <img src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="minasgerais.com.br" class="logo-mobile">
                    </a>
                </div>

                <?php  if ($today < $date){ ?>
                    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-4">
            <a class="navbar-brand mr-0 mr-md-2" href="{{url('/pt/semana-santa')}}">
                <img class="img-fluid" src="{{url('/assets/img/semanasanta/logo.png')}}" alt="Semana Santa" title="Mato Grosso">
            </a>
        </div>
        <?php } ?>  
                <div class="col-lg-8 col-md-6 col-sm-3 col-xs-3 text-right no-padding">
                    <div class="row-fluid">
                        <nav class="navbar navbar-light nav-inferior">
                            <ul> 
                              <li class="nav-item mobile4" id="dropdownResumeMenu4">
                                <div id="nav-icon1" style="float: right; margin-right: 16px;">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                                </div>
                        
                                </li>
                            </ul>
                            
                        </nav>
                    </div>
                </div>
                <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar1">
                    <ul class="nav navbar-nav sub-menu-mobile" id="menu-inferior" >
                        <!--
                        <li class="text-center pull-left">
                            <a class="nav-link" href="{{url($p_Language . '/conheca')}}">{{trans('menu.discover')}}</a>
                        </li>
                    -->
                        <!-- O QUE FAZER -->
                        <li class=" text-center pull-left">
                            <a class="nav-link" href="{{url($p_Language . '/o-que-fazer')}}">{{trans('menu.what_to_do')}}</a>
                        </li>
                        <!-- PARA ONDE IR -->
                        <li class="text-center pull-left">
                            <a class="nav-link" href="{{url($p_Language . '/roteiros')}}" >{{trans('menu.where_to_go')}}</a>
                        </li>
                        <li class="text-center pull-left">
                            <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}">{{trans('menu.explore_the_map')}}</a>
                        </li>
                        <li class=" text-center pull-left">
                            <a class="nav-link" href="{{url($p_Language . '/roteiros-duracao')}}">{{trans('menu.plan_your_trip')}}</a>
                        </li>
                        <li class=" text-center pull-left">
                            <a class="nav-link" href="{{url($p_Language . '/eventos')}}">{{trans('menu.events')}}</a>
                        </li>
                        <li class="">
                            <a class="nav-link pictures" href="javascript:;" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                            <div class="col-lg-12 dropdown-menu" aria-labelledby="dropdownMenu3" id="divDropdownResume" >
                                <div class="col-lg-12 no-padding list-thumbs">
                                    <h4 class="branco" style="padding:10% 0 5%;">{{trans('menu.favorites')}}</h4>
                                    <ul id="list-city" class="favorites-list">
                                    </ul>
                                </div>
                                <div class="col-lg-12" id="call-to-action">
                                    <a href="{{url($p_Language . '/favoritos')}}">{{trans('menu.show_all')}}</a>
                                </div>
                                <!--</div> -->
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </div>
            <!-- O que fazer -->
            <div class="row-fluid" id="listDropdownResumeMenu1">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class="col-lg-12 no-padding">
                        @foreach($p_HeaderCategories as $c_Index => $c_Category)
                        <?php $v_Name = json_decode($c_Category->nome,1)[$p_Language]; ?>
                        <div class="col-lg-2 col-sm-6 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/o-que-fazer/' . $c_Category->slug)}}">
                                <div class="card card-inverse">
                                    <img class="card-img" src="{{$c_Category->foto_capa_url}}" width="518" height="264" alt="Card image">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12" id="title-overlay">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @if(count($v_HeaderTypes) > 0)
                    <div class="col-lg-12 text-highlight">
                        <h2>{{trans('menu.what_do_you_want')}}</h2>
                        <div class="col-lg-12" id="choices">
                            @foreach($v_HeaderTypes as $c_Index => $c_Type)
                            <?php $v_Name = json_decode($c_Type->nome,1)[$p_Language]; ?>
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 choices">
                                <a class="options" href="{{url($p_Language . '/o-que-fazer/' . $c_Type->category_slug . '/' . $c_Type->slug)}}">
                                <p style="padding:5px;">{{$v_Name}}</p></a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/o-que-fazer')}}">{{trans('menu.discover_what_to_do')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Para onde ir -->
            <div class="row-fluid" id="listDropdownResumeMenu2">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class="col-lg-12 no-padding">
                        <div class="col-lg-6 no-padding">
                            @foreach($v_HeaderRoutes as $c_Index => $c_Route)
                            <?php $v_RouteName = json_decode($c_Route->nome,1)[$p_Language]; ?>
                            <div class="col-lg-6 col-sm-3 col-md-6 col-xs-6" style="padding-left:0;">
                                <a href="{{url($p_Language . '/roteiros/' . $c_Route->slug)}}">
                                    <div class="card card-inverse">
                                        <img class="card-img" src="{{$c_Route->url}}" width="518" height="264" alt="Card image">
                                        <div class="card-img-overlay overlay-full">
                                            <div class="col-lg-12" id="title-overlay">
                                                <p>{{$v_RouteName}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            <div class="col-lg-3 pull-right" id="call-to-action-min">
                                <a href="{{url($p_Language . '/roteiros')}}">+ {{trans('menu.routes')}}</a>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-12 list-thumbs">
                                    <h4 class="branco">{{trans('menu.cities_and_districts')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderDestinations as $c_Index => $c_Destination)
                                        <li>
                                            <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                                                {{$c_Destination->nome}}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-lg-6 pull-right" id="call-to-action-min"  style="padding: 0.4rem 1%;!important;">
                                    <a href="{{url($p_Language . '/destinos')}}">+ {{trans('menu.destinations')}}</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 no-padding">
                            <div class="col-lg-12 no-padding">
                                <div class="col-lg-12 list-thumbs">
                                    <h4 class="branco">{{trans('menu.parks_and_reservations')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderParks as $c_Index => $c_Park)
                                            <?php $v_Name = json_decode($c_Park->nome,1)[$p_Language]; ?>
                                            <li>
                                                <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Park->slug)}}">
                                                    {{$v_Name}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-lg-6 pull-right" id="call-to-action-min" style="padding: 0.4rem 1%;!important;">
                                    <a href="{{url($p_Language . '/parques')}}">+ {{trans('menu.parks')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/para-onde-ir')}}">{{trans('menu.more_destinations')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Planeje sua viagem -->
            <div class="row-fluid" id="listDropdownResumeMenu3">
                <div class="container no-padding">
                    <div class="col-lg-12 no-padding">
                        <div class="col-lg-2 col-sm-6 col-md-3 col-xs-12 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=1')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <p style="text-align:center;"><small>{{trans('menu.routes_duration_1_pt1')}}</small><br>{{trans('menu.routes_duration_1_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-6 col-md-3 col-xs-12 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=2')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <p style="text-align:center;"><small>{{trans('menu.routes_duration_2_pt1')}}</small><br>{{trans('menu.routes_duration_2_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-6 col-md-3 col-xs-12 category">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=3')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <p style="text-align:center;"><small>{{trans('menu.routes_duration_3_pt1')}}</small><br>{{trans('menu.routes_duration_3_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-6 col-md-3 col-xs-12 category">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#tempo')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/termometro.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.weather_forecast')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-sm-6 col-md-3 col-xs-12 category">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#como-chegar')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/destino.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.directions')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-12 text-highlight">
                        <div class="col-lg-3" id="call-to-action">
                            <a href="{{url($p_Language . '/planeje-sua-viagem')}}">{{trans('menu.see_how_plan_trip')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Icone destino menu resumido -->
            <div class="row-fluid" id="listDropdownResumeMenu4">
                <div class="container no-padding">
                    <div class="col-lg-7">
                        <nav class="navbar navbar-light nav-superior">
                            <ul class="nav navbar-nav navbar-right" id="menu-superior" style="float: right;">
                                <!--
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/conheca')}}">{{trans('menu.discover')}}</a>
                                </li>
                            -->
                                <!-- O QUE FAZER -->
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/o-que-fazer')}}">{{trans('menu.what_to_do')}}</a>
                                </li>
                                <!-- PARA ONDE IR -->
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/roteiros')}}" >{{trans('menu.where_to_go')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/explore-o-mapa')}}">{{trans('menu.explore_the_map')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/roteiros-duracao')}}">{{trans('menu.plan_your_trip')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/eventos')}}">{{trans('menu.events')}}</a>
                                </li>
                                <!--
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url('/maisgastronomia')}}" title="Mais Gastronomia" target="_blank"><img src="{{url('/portal/assets/imgs/logo_mais_gastronomia.png')}}" class="link-gastronomia"></a>
                                </li>
                                -->
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/minas360')}}" title="Minas em 360">Minas em 360°</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/blog')}}">Blog</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/fale-conosco')}}">{{trans('menu.contact')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url('/admin')}}" target="_blank">{{trans('menu.register_your_business')}}</a>
                                </li>

                                <li class="nav-item">
                                <div class="col-sm-2 col-xs-2 no-padding">
                                    <a class="nav-link" href="https://www.facebook.com/VisiteMinasGerais/" target="_blank">
                                        <img src="{{url('/portal/assets/imgs/facebook.png')}}" alt="">
                                    </a>
                                </div>
                                <div class="col-sm-2 col-xs-2 no-padding">
                                    <a class="nav-link" href="https://instagram.com/visiteminasgerais" target="_blank">
                                        <img src="{{url('/portal/assets/imgs/instagram.png')}}" alt="">
                                    </a>
                                </div>
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                    <div class="col-lg-3 no-padding">
                        {!! Form::open(array('method' => 'get', 'id' => 'formSearch', 'url'=> url($p_Language . '/busca'))) !!}
                            <div class="input-group">
                                <input name="q" type="text" class="form-control" placeholder="{{trans('menu.search_in_site')}}" required>
                                <span class="input-group-btn" id="btn-search">
                                    <button class="btn btn-secondary btn-submit" type="submit" id="btn-submit">
                                    </button>
                                </span>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-lg-2 no-padding">
                        <ul class="nav navbar-nav navbar-right text-center">
                            <li class="nav-item language-links">
                                @foreach($v_Languages as $c_Index => $c_Language)
                                    <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $c_Language . '.png')}}">
                                    <a href="{{url('/' . $c_Language)}}">{{$c_Language}}</a>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>
    <!-- FIM MENU RESUMIDO MOBILE-->
    



    <!--MENU TABLET-->
    <div class="row-fluid hidden-sm-down hidden-lg-up menu-tablet" id="box-menu-top-resume">
        <header>
            <div class="container no-padding">
                <div class="col-lg-12 text-center logo">
                    <a href="{{url($p_Language)}}">
                        <img src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="">
                    </a>
                </div>
                <div class="col-lg-12 no-padding">
                    <div class="row-fluid">
                        <nav class="navbar navbar-light nav-inferior">
                            <button class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar1">
                                <!-- &#9776; -->
                                <img src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="">
                            </button>
                            <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar1">
                                <ul class="nav navbar-nav " id="menu-inferior" style="float: right;">
                                    <!--
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/conheca')}}">{{trans('menu.discover')}}</a>
                                    </li>
                                    -->
                                    <!-- O QUE FAZER -->
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/o-que-fazer')}}" id="dropdownResumeMenu1">{{trans('menu.what_to_do')}}</a>
                                    </li>
                                    <!-- PARA ONDE IR -->
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/roteiros')}}" id="dropdownResumeMenu2">{{trans('menu.where_to_go')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}">{{trans('menu.explore_the_map')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/roteiros-duracao')}}" id="dropdownResumeMenu3">{{trans('menu.plan_your_trip')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url($p_Language . '/eventos')}}">{{trans('menu.events')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link pictures" href="javascript:;" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                        <div class="col-lg-4 dropdown-menu" aria-labelledby="dropdownMenu3" id="divDropdown" >
                                            <div class="col-lg-12 no-padding list-thumbs">
                                                <h4 class="branco" style="padding:10% 0 5%;">{{trans('menu.favorites')}}</h4>
                                                <ul id="list-city" class="favorites-list">
                                                </ul>
                                            </div>
                                            <div class="col-lg-12" id="call-to-action">
                                                <a href="{{url($p_Language . '/favoritos')}}">{{trans('menu.show_all')}}</a>
                                            </div>

                                            <!--   </div> -->
                                        </div>
                                    </li>
                                    <li class="nav-item tablet4" id="dropdownResumeMenu4">
                                        <div id="nav-icon1">
                                          <span></span>
                                          <span></span>
                                          <span></span>
                                        </div>
                                       
                                    </li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- O que fazer -->
            <div class="row-fluid" id="listDropdownResumidoMenu1">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class="col-lg-12 no-padding">
                        @foreach($p_HeaderCategories as $c_Index => $c_Category)
                        <?php $v_Name = json_decode($c_Category->nome,1)[$p_Language]; ?>
                        <div class="col-lg-2-5 col-sm-3 col-md-3 col-xs-6 category">
                            <a href="{{url($p_Language . '/o-que-fazer/' . $c_Category->slug)}}">
                                <div class="card card-inverse">
                                    <img class="card-img" src="{{$c_Category->foto_capa_url}}" width="518" height="264" alt="Card image">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12" id="title-overlay">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    @if(count($v_HeaderTypes) > 0)
                    <div class="col-lg-12 text-highlight">
                        <h2>{{trans('menu.what_do_you_want')}}</h2>
                        <div id="choices">
                            @foreach($v_HeaderTypes as $c_Index => $c_Type)
                            <?php $v_Name = json_decode($c_Type->nome,1)[$p_Language]; ?>
                            <div class="col-sm-4 choices">
                                <a class="options" href="{{url($p_Language . '/o-que-fazer/' . $c_Type->category_slug . '/' . $c_Type->slug)}}"><p style="padding:5px;">{{$v_Name}}</p></a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @endif

                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/o-que-fazer')}}">{{trans('menu.discover_what_to_do')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Para onde ir -->
            <div class="row-fluid" id="listDropdownResumidoMenu2">
                <div class="container no-padding" style="padding-bottom:25px;">
                    <div class=" no-padding">
                        <div class="col-sm-4 no-padding">
                            @foreach($v_HeaderRoutes as $c_Index => $c_Route)
                            <?php $v_RouteName = json_decode($c_Route->nome,1)[$p_Language]; ?>
                            
                                <a href="{{url($p_Language . '/roteiros/' . $c_Route->slug)}}">
                                    <div class="card card-inverse">
                                        <img class="card-img" src="{{$c_Route->url}}" width="518" height="264" alt="Card image">
                                        <div class="card-img-overlay overlay-full">
                                            <div class="col-lg-12" id="title-overlay">
                                                <p>{{$v_RouteName}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            
                            @endforeach
                            <div class="col-lg-12 pull-right" id="call-to-action-min">
                                <a href="{{url($p_Language . '/roteiros')}}">+ {{trans('menu.routes')}}</a>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="no-padding">
                                <div class="list-thumbs">
                                    <h4 class="branco">{{trans('menu.cities_and_districts')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderDestinations as $c_Index => $c_Destination)
                                            <li>
                                                <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                                                    {{$c_Destination->nome}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-lg-6 pull-right" id="call-to-action-min"  style="padding: 0.4rem 1%;!important;">
                                    <a href="{{url($p_Language . '/destinos')}}">+ {{trans('menu.destinations')}}</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4 no-padding">
                            <div class="no-padding">
                                <div class="list-thumbs">
                                    <h4 class="branco">{{trans('menu.parks_and_reservations')}}</h4>
                                    <ul id="list-city">
                                        @foreach($v_HeaderParks as $c_Index => $c_Park)
                                            <?php $v_Name = json_decode($c_Park->nome,1)[$p_Language]; ?>
                                            <li>
                                                <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Park->slug)}}">
                                                    {{$v_Name}}
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="col-lg-6 pull-right" id="call-to-action-min" style="padding: 0.4rem 1%;!important;">
                                    <a href="{{url($p_Language . '/parques')}}">+ {{trans('menu.parks')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 box-call">
                    <div class="container">
                        <div class="col-md-4 col-md-offset-4" id="call-to-action">
                            <a href="{{url($p_Language . '/para-onde-ir')}}">{{trans('menu.more_destinations')}}</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Planeje sua viagem -->
            <div class="row-fluid" id="listDropdownResumidoMenu3">
                <div class="container no-padding">
                    <div class="col-lg-12 no-padding">
                        <div class="col-sm-4">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=1')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding text-center" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_1_pt1')}}</small><br>{{trans('menu.routes_duration_1_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=2')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding text-center" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_2_pt1')}}</small><br>{{trans('menu.routes_duration_2_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="{{url($p_Language . '/roteiros-duracao?d=3')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding text-center" id="title-overlay">
                                            <p><small>{{trans('menu.routes_duration_3_pt1')}}</small><br>{{trans('menu.routes_duration_3_pt2')}}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#tempo')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/termometro.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.weather_forecast')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-4">
                            <a href="{{url($p_Language . '/planeje-sua-viagem#como-chegar')}}">
                                <div class="card card-inverse">
                                    <div class="card-img-overlay overlay-full">
                                        <div class="col-lg-12 no-padding" id="title-overlay">
                                            <img src="{{url('/portal/assets/imgs/destino.png')}}" alt="">
                                            <p style="text-align:center;"><small>{{trans('planYourTrip.directions')}}</small></p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-4 text-highlight">
                            <div class="col-lg-3" id="call-to-action">
                                <a href="{{url($p_Language . '/planeje-sua-viagem')}}">{{trans('menu.see_how_plan_trip')}}</a>
                            </div>
                        </div>

                    </div>

                    
                </div>
            </div>
            <!-- Icone destino menu resumido -->
            <div class="row-fluid tabletresumido4" id="listDropdownResumidoMenu4">
                <div class="container no-padding">
                    
                        <nav class="navbar navbar-light nav-superior">
                            <ul class="nav navbar-nav" id="menu-superior" >
                                <li class="nav-item">
                                    <a class="nav-link" href="https://www.facebook.com/VisiteMinasGerais/" target="_blank">
                                        <img src="{{url('/portal/assets/imgs/facebook.png')}}" alt="">
                                    </a>
                                </li>
                                <li class="nav-item"  style="margin-left: 0.5rem;margin-right:1rem;">
                                    <a class="nav-link" href="https://instagram.com/visiteminasgerais" target="_blank">
                                        <img src="{{url('/portal/assets/imgs/instagram.png')}}" alt="">
                                    </a>
                                </li>
                                <!--<li class="nav-item">
                                    <a class="nav-link strings" href="{{url('/maisgastronomia')}}" title="Mais Gastronomia" target="_blank"><img src="{{url('/portal/assets/imgs/logo_mais_gastronomia.png')}}" class="link-gastronomia"></a>
                                </li>-->
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/minas360')}}" title="Minas em 360">Minas em 360°</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/blog')}}">Blog</a>
                                </li>
                                
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url($p_Language . '/fale-conosco')}}">{{trans('menu.contact')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link strings" href="{{url('/admin')}}" target="_blank">{{trans('menu.register_your_business')}}</a>
                                </li>
                            </ul>
                        </nav>
                    
                    <div class="col-sm-7 no-padding">
                        {!! Form::open(array('method' => 'get', 'id' => 'formSearch', 'url'=> url($p_Language . '/busca'))) !!}
                            <div class="input-group">
                                <input name="q" type="text" class="form-control" placeholder="{{trans('menu.search_in_site')}}" required>
                                <span class="input-group-btn" id="btn-search">
                                    <button class="btn btn-secondary btn-submit" type="submit" id="btn-submit">
                                    </button>
                                </span>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-sm-5 no-padding">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="nav-item language-links">
                                @foreach($v_Languages as $c_Index => $c_Language)
                                    <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $c_Language . '.png')}}">
                                    <a href="{{url('/' . $c_Language)}}">{{$c_Language}}</a>
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>
<!--FIM MENU TABLET-->

</nav>