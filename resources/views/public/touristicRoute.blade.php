@extends('public.base')

@section('pageCSS')
@stop

@section('main-content')
    <?php
        $v_Name = json_decode($p_Content->nome,1)[$p_Language];
        $v_ShortDescription = json_decode($p_Content->descricao_curta,1)[$p_Language];
        $v_Description = json_decode($p_Content->descricao,1)[$p_Language];
        $v_Testimonial = json_decode($p_Content->depoimento,1)[$p_Language];
        $v_TestimonialAuthor = json_decode($p_Content->depoimento_autor,1)[$p_Language];
    ?>
    <div class="row-fluid banner-roteiros some-tablet" id="destinos" style="padding:0;">
        <div class="col-lg-12 no-padding">
            <div class="caption" style="opacity:1;width: 100%;height: 100%;top:0;">
                <div class="caption-content">
                    <div class="container" id="map-roteiro">
                        <div class="col-lg-12 no-padding">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-highlight">
                                <h2>{{$p_Content == null ? '' : $v_Name}}</h2>
                                <p>{{trans('touristicRoutes.duration') . ': ' . $p_Content->dias_duracao . ' ' . trans_choice('touristicRoutes.days', $p_Content->dias_duracao)}}</p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 img-highlight mapa-negativo hidden-sm-down">
                                @if($p_MapPhoto != null)
                                    <img src="{{ $p_MapPhoto->url }}" alt="" >
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 no-padding favorite" style="bottom: 20px; position: absolute;">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2 no-padding" id="favoritos">
                                <p id="toggleFavorite">
                                    <a href="javascript:void(0)">
                                        <img src="{{url('/portal/assets/imgs/heart.png')}}" alt="">
                                        <span class="branco">
                                            <span class="hidden-xs-down">{{--Adicione aos favoritos--}}</span>
                                        </span>
                                    </a>
                                </p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8 no-padding" id="share">
                                <div class="social">
                                    <ul id="icons-social" style="float:left;">
                                        <div class="addthis_sharing_toolbox"></div>
                                    </ul>
                                    <p id="share" style="margin-top: -5px;font-family: Signika;">{{trans('string.share')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="highlight-active-larger">
                <!--<img src="{{$p_Content->url}}" alt="">-->
                <div style="background: url('{{$p_Content->url}}') no-repeat;" class="corte-destaque-interna"></div>
            </div>
        </div>
    </div>


    <div class="row-fluid banner-roteiros some aparece" id="destinos" style="padding:0;">
        <div class="col-lg-12 no-padding">
            <div class="caption" style="opacity:1;width: 100%;height: 100%;top:0;">
                <div class="caption-content">
                    <div class="container" id="map-roteiro">
                        <div class="col-lg-12 no-padding">
                            <div class="col-xs-12 text-highlight">
                                <h2>{{$p_Content == null ? '' : $v_Name}}</h2>
                                <p>{{trans('touristicRoutes.duration') . ': ' . $p_Content->dias_duracao . ' ' . trans_choice('touristicRoutes.days', $p_Content->dias_duracao)}}</p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 img-highlight mapa-negativo hidden-sm-down some">
                                @if($p_MapPhoto != null)
                                    <img src="{{ $p_MapPhoto->url }}" alt="" >
                                @endif
                            </div>
                        </div>

                        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 no-padding favorite" style="bottom: 20px; position: absolute;">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-2 no-padding" id="favoritos">
                                <p id="toggleFavorite">
                                    <a href="javascript:void(0)">
                                        <img src="{{url('/portal/assets/imgs/heart.png')}}" alt="">
                                        <span class="branco">
                                            <span class="hidden-xs-down">{{--Adicione aos favoritos--}}</span>
                                        </span>
                                    </a>
                                </p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-8 no-padding" id="share">
                                <div class="social">
                                    <ul id="icons-social" style="float:left;">
                                        <div class="addthis_sharing_toolbox"></div>
                                    </ul>
                                    <p id="share" style="margin-top: -5px;font-family: Signika;">{{trans('string.share')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="highlight-active-larger">
                <!--<img src="{{$p_Content->url}}" alt="">-->
                <div style="background: url('{{$p_Content->url}}') no-repeat;" class="corte-destaque-interna"></div>
            </div>
        </div>
    </div>

    @if($p_Content != null && strlen($v_Testimonial) > 0)
        <div class="row-fluid" id="testimonials" style="margin:0;">
            <div class="container">
                <div class="col-lg-2 col-lg-offset-3">
                    @if($p_TestimonialPhoto != null)
                        <img src="{{$p_TestimonialPhoto->url}}" alt="">
                    @endif
                </div>
                <div class="col-lg-7">
                    <p>{!! $v_Testimonial !!}</p>
                    <span>{{ $v_TestimonialAuthor }}</span>
                </div>
            </div>
        </div>
    @endif

    <div class="row-fluid" style="margin:4% 0;">
        <div class="container">
            <div class="col-lg-12 title">
                <h2 style="color:#b4b4b4;font-weight:400;text-align:center;">
                    {{$p_Content == null ? '' : $p_Content->titulo}}
                </h2>
            </div>

            <div class="col-lg-12" style="margin:1% 0;">
                <p>{!! $p_Content == null ? '' : Markdown::text($v_Description) !!}</p>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="localization" style="display:block;">
        <div class="container">
            <div class="content-address" style="margin-bottom: 3%;">
                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4" id="contactArea" style="padding:2% 0;border-top:1px solid #dadada;border-bottom:1px solid #dadada;">
                    <?php $v_HasContact = false; ?>
                    @if(strlen($p_Content->telefone) > 0)
                    <?php $v_HasContact = true; ?>
                    <div class="col-lg-12" style="margin:0.2rem 0;">
                        <p>Tel.: {{$p_Content->telefone}}</p>
                    </div>
                    @endif
                     @if(strlen($p_Content->whatsapp) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p>WhatsApp.: {{$p_Content->whatsapp}}</p>
                        </div>
                    @endif
                    @if(strlen($p_Content->site) > 0)
                    <?php $v_HasContact = true; ?>
                    <div class="col-lg-12" style="margin:0.2rem 0;">
                        <p><a target="_blank" href="{{$p_Content->site}}">{{$p_Content->site}}</a></p>
                    </div>
                    @endif
                    @if(strlen($p_Content->site) > 0)
                    <?php $v_HasContact = true; ?>
                    <div class="col-lg-12" style="margin:0.2rem 0;">
                        <p>{{$p_Content->email}}</p>
                    </div>
                    @endif
                    <div class="col-lg-12" style="display:flex;margin:0.5rem 0;">
                        <ul style="display: inline-block;float: left;">
                            @if(strlen($p_Content->facebook) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->facebook}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-fb-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->instagram) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->instagram}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-insta-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->twitter) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->twitter}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-tw-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->youtube) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->youtube}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-youtube-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->flickr) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->flickr}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-flicker-gray.png')}}" alt=""></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                @if(!$v_HasContact)
                    <style>
                        #contactArea{
                            display:none!important;
                        }
                    </style>
                @endif
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:0;">
        <div class="container">
            <div class="col-lg-12">
                <h2 class="laranja">{{trans('touristicRoutes.where_will_you_pass')}}</h2>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-12 line">
                @foreach($p_Destinations as $c_Index => $c_Destination)
                <?php $v_DestinationDescription = json_decode($c_Destination->descricao_curta,1)[$p_Language]; ?>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <a href="{{url($p_Language . '/destinos/' . $c_Destination->slug)}}">
                        <div class="hoverzoom">
                            <div class="thumbs-mini-four">
                                <!--<img src="{{$c_Destination->url}}">-->
                                <div class="thumbs-mini-recorte" style="background: url('{{$c_Destination->url}}') no-repeat;"></div>
                            </div>
                            <div class="retina-hover">
                                <div class="col-lg-12 title">
                                    <p>{{$c_Destination->nome}}</p>
                                </div>
                                <div class="col-lg-12 no-padding">
                                    <hr>
                                </div>
                                <div class="col-lg-12 text">
                                    <p>{{$v_DestinationDescription}}</p>
                                </div>
                            </div>
                            <div class="retina">
                                <p>{{$c_Destination->nome}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row-fluid" id="useful-information" style="margin-top:3%;">
        <div class="container">
            <div class="col-lg-12">
                <h2 class="laranja">{{trans('touristicRoutes.how_split_your_trip')}}</h2>
            </div>

            <div class="col-lg-12 no-padding" style="margin-bottom:7%;">
                @foreach($p_DayDescriptions as $c_DayDescription)
                <div class="col-lg-12" style="margin-bottom:2rem;font-family: 'Signika';font-size:16px;">
                    <div class="dateLocation" style="margin-bottom:15px;font-weight:700;">{{trans('touristicRoutes.day') . $c_DayDescription->dia}}</div>
                    <div class="text">
                        <?php $v_DayDescription = json_decode($c_DayDescription->descricao,1)[$p_Language]; ?>
                        <p>{!! nl2br($v_DayDescription) !!}</p>
                    </div>
                </div>
                @endforeach
            </div>

            @if(count($p_Attractions) > 0)
            <div class="col-lg-12 no-padding" style="margin-bottom:20px;">
                <div class="col-lg-12">
                    <h2 class="laranja">{{trans('touristicRoutes.attractions_to_visit')}}</h2>
                </div>
            </div>
            @endif
        </div>
    </div>

    @if(count($p_Attractions) > 0)
        <div class="row-fluid" id="destinos" style="padding:0;">
            <div class="container no-padding">
                <div class="col-lg-12 line">
                    @foreach($p_Attractions as $c_Index => $c_Attraction)
                        <?php
                            $v_AttractionDescription = json_decode($c_Attraction->descricao_curta,1)[$p_Language];
                            if($c_Attraction->trade)
                                $v_AttractionName = $c_Attraction->nome;
                            else
                                $v_AttractionName = json_decode($c_Attraction->nome,1)[$p_Language];
                        ?>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Attraction->city_id) . '/' . $c_Attraction->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-four">
                                        <img src="{{$c_Attraction->url}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_AttractionName}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_AttractionDescription}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_AttractionName}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if(count($p_PhotoGallery) > 0)
        <div class="row-fluid" id="useful-information" style="margin-top:3%;">
            <div class="container">
                <div class="col-lg-12">
                    <h2 class="laranja">{{trans('touristicRoutes.what_will_you_see')}}.</h2>
                </div>
            </div>
        </div>

        <div class="row-fluid" id="destinos" style="margin-bottom:30px;padding:0;">
            <div class="container">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        @foreach($p_PhotoGallery as $c_Index => $c_Photo)
                        <div class="carousel-item {{$c_Index == 0 ? 'active' : ''}}">
                            <img src="{{$c_Photo->url}}" alt="First slide">
                            <div class="carousel-caption">
                                <div class="col-lg-12" id="call-to-action-home" style="margin:0;">
                                    <a href="">{{trans('institutional.slideshow')}}</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="opacity: 1;">
                        <span class="icon-prev-prev" aria-hidden="true" style="background:none;"><img src="{{url('/portal/assets/imgs/prev.jpg')}}"></span>
                        <span class="sr-only">{{trans('institutional.previous')}}</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="opacity: 1;">
                        <span class="icon-next-next" aria-hidden="true" style="background:none;"><img src="{{url('/portal/assets/imgs/next.jpg')}}"></span>
                        <span class="sr-only">{{trans('institutional.next')}}</span>
                    </a>
                </div>
            </div>
        </div>
    @endif
@stop

@section('pageScript')
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a0debd79357d1c" async="async"></script>
    <script>
        $(document).ready(function()
        {
            $("#toggleFavorite").click(function(){
                var v_Name = '{{$p_Content == null ? '' : $v_Name}}';
                var v_Description = '{{$p_Content == null ? '' : $v_ShortDescription}}';
                var v_Img = '{{$p_Content == null ? '' : $p_Content->url}}';
                toggleFavorite(location.href, v_Name, v_Img, v_Description);
            });
        });
    </script>
@stop