<style>
.btnSearchCity{
        cursor:pointer;
}
</style>


    <div class="container mt-2 fadeInOnLoad" style="display: none;">
    <div class="title">
            <h2  style="font-family:santa_feregular; color:purple;font-size:3em;margin-bottom:-25px;">
                Destinos Religiosos
            </h2>
        </div>
        <h1 class="text-center text-black-10 display-4">&nbsp</h1>
            <section class="cities-logos slider overlap-category-title">
                @if (in_array('44',$cids))
                <div class="slide text-center btnSearchCity"  city-id="44">
                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==44) selected-category @endif mt-2 mb-2 mr-auto ml-auto">
                                <img class="mr-auto ml-auto" src="{{url('/assets/img/semanasanta/5araxa-min.jpg')}}">
                        </div>
                </div>
                @endif                
                @if (in_array('110',$cids))
                <div class="slide text-center btnSearchCity"  city-id="110">
                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==110) selected-category @endif mt-2 mb-2 mr-auto ml-auto">
                                <img class="mr-auto ml-auto" src="{{url('/assets/img/semanasanta/2caete-min.jpg')}}">
                        </div>
                </div>
                @endif
                @if (in_array('200',$cids))
                <div class="slide text-center btnSearchCity"  city-id="200">
                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==200) selected-category @endif mt-2 mb-2 mr-auto ml-auto">
                                <img class="mr-auto ml-auto" src="{{url('/assets/img/semanasanta/6congonhas-min.jpg')}}">
                        </div>
                </div>
                @endif
                @if (in_array('241',$cids))
                <div class="slide text-center btnSearchCity"  city-id="241">
                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==241) selected-category @endif mt-2 mb-2 mr-auto ml-auto">
                                <img class="mr-auto ml-auto" src="{{url('/assets/img/semanasanta/9diamantina-min.jpg')}}">
                        </div>
                </div>
                @endif
                @if (in_array('269',$cids))
                <div class="slide text-center btnSearchCity" city-id="269">
                        <div class=" @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==269) selected-category @endif  mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image  mr-auto ml-auto" src="{{url('/assets/img/semanasanta/10entreriosdeminas-min.jpg')}}">
                        </div>
                </div>
                @endif
                @if (in_array('460',$cids))
                <div class="slide text-center btnSearchCity" city-id="460">
                        <div class=" @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==460) selected-category @endif  mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image  mr-auto ml-auto" src="{{url('/assets/img/semanasanta/4mariana-min.jpg')}}">
                        </div>
                </div>
                @endif
                @if (in_array('540',$cids))
                <div class="slide text-center btnSearchCity"  city-id="540">
                        <div class="@if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==540) selected-category @endif  mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image mr-auto ml-auto" src="{{url('/assets/img/semanasanta/7ouropreto-min.jpg')}}">
                        </div>
                </div>
                @endif
                @if (in_array('659',$cids))
                <div class="slide text-center btnSearchCity" city-id="659">
                        <div class=" @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==659) selected-category @endif  mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image  mr-auto ml-auto" src="{{url('/assets/img/semanasanta/3sabara-min.jpg')}}">
                        </div>
                </div>
                @endif                
                @if (in_array('731',$cids))
                <div class="slide text-center btnSearchCity" city-id="731">
                        <div class=" @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==731) selected-category @endif  mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image  mr-auto ml-auto" src="{{url('/assets/img/semanasanta/1saojoaodelrei-min.jpg')}}">
                        </div>
                </div>
                @endif


                @if (in_array('808',$cids))
                <div class="slide text-center btnSearchCity" city-id="808">
                        <div class=" @if (isset($filter) && isset($filter['city_id']) && $filter['city_id']==808) selected-category @endif  mt-2 mb-2 mr-auto ml-auto">
                                <img class="fade-image  mr-auto ml-auto" src="{{url('/assets/img/semanasanta/8tiradentes-min.jpg')}}">
                        </div>
                </div>
                @endif
            </section>
      </div>

