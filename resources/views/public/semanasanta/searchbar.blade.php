<div class="container mt-2 pt-2">
<form  enctype="multipart/form-data" method="get" action="{{url($p_Language.'/semana-santa/filtro')}}">

        <input type="hidden" name="category_id" value="0">
                <div class="form-row">
                                                <!--
                  <div class="form-group col-md-3">

                        <div class="input-group mb-1">
                                <input type="text" name="event" class="form-control" placeholder="{{trans('menu.im_looking_for', [],'messages', $p_Language)}}" value="@if (isset($filter) && isset($filter['event'])){{$filter['event']}}@endif">

                        </div>

                  </div>
                                          -->
                  <div class="form-group col-md-3">
                        <div class="input-group mb-1">
                                <select name="city" class="custom-select select-city" id="inputGroupSelect01">
                                        <option value="0" @if (isset($filter) && isset($filter['city']) && ($filter['city'] == 0)){{' selected '}}@elseif(!isset($filter)){{' selected '}}@endif>Cidades com Semana Santa</option>
                                                
                                        @if(isset($cities))
                                                @foreach ($cities as $city)
                                                        <option class='opts' value="{{$city['id']}}" @if (isset($filter) && isset($filter['city']) && ($filter['city'] == $city['id'])){{' selected '}}@endif>{{$city['name']}}</option>
                                                @endforeach
                                        @endif
                                </select>

                        </div>
                  </div>

                <input type="hidden" id="reportrange" name="date" autocomplete="off" value="11/03/2019- 21/04/2019'" class="form-control">

                <div class="form-group col-md-2 mb-1">
                <button id="SearchFormButton" class="btn btn-primary w-100" type="submit">{{trans('menu.search_in_site')}}</button>
                </div>
                </div>
</form>
</div>
