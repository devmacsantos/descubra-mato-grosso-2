@extends('public.templates.base')
@section('pageCSS')


<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsIndex.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsSearchbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/fonts/santafe/stylesheet.css')}}">

@endsection



@section('header')
@include('public.semanasanta.header')
@endsection




@section('main-content')
<main class="container-fluid mt-4">

    @include('public.semanasanta.eventsCity')

    @if (isset($themeEvent) && (count($themeEvent)>0)) {{--Verifica Eventos de tema--}}
    <div class="container">
        <div class="title">
        <h4 class="pt-4" style="font-family:santa_feregular; color:purple;font-size:3em;">
                Eventos
            </h4>
        </div>

        <section class="eventos-tema slider">

            @foreach ($themeEvent as $vEvent) {{--Início foreach Eventos de tema--}}
            @if ($vEvent->EventPhotos(true)->first() != null) {{-- Verifica se o evento tem foto de capa --}}

            <div class="slide text-center">

                <div class="">
                    <div class="card mb-4 shadow">
                        <a href="{{url($p_Language.'/semana-santa/'.$vEvent->Destination->slug.'/' .$vEvent->slug)}}">
                            <img class="card-img-top fade-image"
                                src="/image/330/248/true/true/{{$vEvent->EventPhotos(true)->first()->url}}">
                        </a>
                        <div class="card-body pb-4">
                        <div class="border border-top-0 border-left-0 border-right-0 text-center text-uppercase text-truncate text-bold">di
                                <a class="link-no-hover"
                                    href="{{url($p_Language.'/semana-santa/'.$vEvent->Destination->slug.'/' .$vEvent->slug)}}"
                                    title="{{$vEvent->nome}}" data-toggle="popover" data-trigger="hover"
                                    data-content="{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}"
                                    data-placement="top">{{$vEvent->nome}}
                                </a>
                            </div>

                            <table class="float-left">
                                <tr>
                                    <th class="border border-0 calendar-month-single text-light px-2 text-uppercase">
                                        {{getMonthName(date_format(date_create($vEvent->inicio),'m'))}}
                                    </th>
                                </tr>
                                @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'd') !=
                                date_format(date_create($vEvent->fim), 'd')) &&
                                (date_format(date_create($vEvent->inicio), 'm') ==
                                date_format(date_create($vEvent->fim), 'm')))
                                {{--*/ $twoDays=true /*--}} @else {{--*/ $twoDays=false /*--}}
                                @endif
                                <tr>
                                    <td class="border border-0 calendar-day-single text-light text-center px-2">
                                        <strong>{{date_format(date_create($vEvent->inicio), 'd')}}</strong></td>
                                </tr>

                                <tr>
                                    <td class="border border-0 calendar-day-single text-light text-center px-2"><strong
                                            @if (!$twoDays) style="visibility: hidden"
                                            @endif>{{date_format(date_create($vEvent->fim), 'd')}}</strong></td>
                                </tr>
                            </table>

                            <div class="text-uppercase mt-1 text-left pl-4 ml-3">
                                <div class="font-weight-bold pl-3">
                                    <small><span class="oi oi-map-marker text-info"></span></small>
                                    <small><b class="text-dark">&nbsp; {{$vEvent->City->name}} </b></small>
                                    <p class="text-truncate">
                                        <small><small style="color: black">{{$vEvent->local_evento}}</small></small>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @endif {{-- Verifica se o evento tem foto de capa --}}
            @endforeach {{--Fim foreach Eventos de tema--}}

        </section>

    </div>
    @endif {{--Fim Verifica Eventos de tema--}}





</main>
@include('public.semanasanta.menuMobile')
@endsection

@section('footer')
@include('public.semanasanta.footer')
@endsection

@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsIndex.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsSearchbar.js')}}"></script>
@endsection