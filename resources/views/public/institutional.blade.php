@extends('public.base')
@section('pageCSS')
@stop
@section('main-content')
@if($p_Content != null && strlen($p_Content->depoimento) > 0)
    <div class="row-fluid" id="testimonials">
        <div class="container">
            <div class="col-lg-2 col-lg-offset-2">
                @if($p_TestimonialPhoto != null)
                    <img src="{{ $p_TestimonialPhoto->url }}" alt="">
                @endif
            </div>
            <div class="col-lg-8">
                <p>{!! $p_Content->depoimento !!}</p>
                <span>{{ $p_Content->depoimento_autor }}</span>
            </div>
        </div>
    </div>
@else
    <div class="row-fluid" id="testimonials" style="padding:0"></div>
@endif
<div class="row-fluid" id="destinos">
    <div class="container">
        <div class="col-lg-12">
            <h2 class="laranja margint">{{ $p_Content == null ? '' : $p_Content->titulo }}</h2>
        </div>

        <!-- <div class="col-lg-4"> -->
        <div class="col-lg-12" style="margin:1% 0;">
            <p>{!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}</p>
        </div>
        <div id="call-to-action" >
            @if($p_Content != null && strlen($p_Content->video_url) > 0)
                <iframe src="{{ $p_Content->video_url }}" height="480" width="100%" allowfullscreen="" frameborder="0"></iframe>
            @endif
            <div class="col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 margint marginb">
                    <a href="{{url($p_Language . '/conheca/galeria')}}" class="btn-block"><img src="{{url('/portal/assets/imgs/filme35mm.png')}}" alt="" style="padding:0 1rem;">{{trans('institutional.visit_gallery')}}</a>
            </div>
        </div>

        @if($p_Content != null)
            <div class="col-md-10 col-md-offset-1 no-padding">
                @for($c_Counter = 1; $c_Counter <= 3; $c_Counter++)
                    @if(strlen($p_Content['dado_relevante_' . $c_Counter]) > 0 && strlen($p_Content['dado_relevante_descricao_' . $c_Counter]) > 0)
                        <div class="col-lg-4 col-sm-12">
                            <div class="col-lg-12 listInfos">
                                <div class="position">
                                    <h4>{{$p_Content['dado_relevante_' . $c_Counter]}}</h4>
                                    <p>{{$p_Content['dado_relevante_descricao_' . $c_Counter]}}</p>
                                </div>
                            </div>
                            {{--<p class="text-center">Minas Gerais é o estado brasileiro com maior número de cidades.</p>--}}
                        </div>
                    @endif
                @endfor
            </div>
        @endif
    </div>
</div>
@if(count($p_Articles) > 0 )
<div class="row-fluid" id="destinos" style="padding:0;">
    <div class="container">
        <div class="col-lg-12">
            <h2 class="laranja margint marginb">Blog</h2>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-12 line">
            @foreach($p_Articles as $c_Index => $c_Article)
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                <date style="text-align:left;color:#67676c;font-size:13px;">{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Article->created_at)->format('d/m/Y')}}</date>
                <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <!--<img src="{{$c_Article->foto_capa_url}}">-->
                        <div class="thumbs-mini-recorte" style="background: url('{{$c_Article->foto_capa_url}}') no-repeat;"></div>
                    </div>
                </div>
                <p style="text-align:left;color:#67676c;font-size:13px;">
                    @foreach($c_Article->hashtags as $c_Hashtag)
                        #{{$c_Hashtag}}
                    @endforeach
                </p>
                <p>{{json_decode($c_Article->titulo,1)[$p_Language]}}</p>
                <a href="{{url($p_Language . '/blog/artigo/' . $c_Article->slug)}}" class="laranja">{{trans('institutional.read_more')}}</a>
            </div>
            @endforeach
        </div>
        <div class="col-md-4 col-md-offset-4">
                <div id="call-to-action">
                    <a href="{{url($p_Language . '/blog')}}" class="btn-block">{{trans('institutional.read_more_posts')}}</a>
                </div>
        </div>
    </div>
</div>
@endif
@stop

@section('pageScript')
@stop