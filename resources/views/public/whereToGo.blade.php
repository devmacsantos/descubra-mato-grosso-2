@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .destination-select2-container{
            float:left;
            width:calc(100% - 46px);
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 40px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 40px;
        }
    </style>
@stop

@section('main-content')
<div class="row-fluid" id="testimonials" style="padding:0"></div>

<div class="row-fluid" id="destinos" style="padding:1% 0;">
<div class="container">
        <div class="col-lg-12" id="list-title">
            <h2 class="laranja">{{trans('menu.where_to_go')}}</h2>
        </div>
    </div>

    <div class="container">

        <div class="col-lg-12">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="col-lg-12" >
                    <p>{!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}</p>
                </div>
            </div>
        </div>

        <!-- FRAGMENTO ATUALIZADO -->
        <div class="col-lg-12 line no-padding">
            @foreach($p_Places as $c_Index => $c_Place)
                <?php
                    $v_Description = json_decode($c_Place['descricao_curta'],1)[$p_Language];
                    if($c_Place['tipo'] == 'destinos')
                        $v_Name = $c_Place['nome'];
                    else
                        $v_Name = json_decode($c_Place['nome'],1)[$p_Language];
                    ?>
                @if($c_Index < 3)
                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 list-thumbs">
                    <a href="{{url($p_Language . '/' . $c_Place['tipo'] . '/' . $c_Place['slug'])}}">
                        <div class="hoverzoom">
                            <div class="thumbs-mini-three">
                                <!--<img src="{{$c_Place['url']}}">-->
                                <div class="thumbs-mini-recorte" style="background: url('{{$c_Place['url']}}') no-repeat;"></div>
                            </div>
                            <div class="retina-hover">
                                <div class="col-lg-12 title">
                                    <p>{{$v_Name}}</p>
                                </div>
                                <div class="col-lg-12 no-padding">
                                    <hr>
                                </div>
                                <div class="col-lg-12 text">
                                    <p>{{$v_Description}}</p>
                                </div>
                            </div>
                            <div class="retina">
                                <p>{{$v_Name}}</p>
                            </div>
                        </div>
                    </a>
                </div>
                @endif
            @endforeach
        </div>
        <!-- FRAGMENTO ATUALIZADO -->

        <div class="col-lg-12" >
            <div class="col-lg-12 hospedagem">
                <h2 style="color:#b4b4b4;">
                    {{trans('whereToGo.find_destination')}}
                </h2>
                <p style="color:#4b4b4b;font-size:16px;font-family: Signika;">
                    {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                </p>
            </div>
        </div>
        <!-- FRAGMENTO ATUALIZADO -->
    </div>

    <!-- FRAGMENTO ATUALIZADO -->
    <div class="col-lg-12" id="tarja">
        <div class="container no-padding">
            <div class="col-lg-12">
                <div class="row" id="options">
                    {!! Form::open(array('onsubmit' => 'return submitFilter()')) !!}
                        <div class="col-lg-3">
                            <p style="font-size:20px;">{{trans('whereToGo.what_do_you_expect')}}</p>
                        </div>

                        <div class="col-lg-3">
                            <div class="col-lg-12 selectoptions">
                                <?php
                                    $v_CategoriesArray = [];
                                    foreach($p_TripCategories as $c_CategoryIndex => $c_Value)
                                        $v_CategoriesArray += [$c_CategoryIndex => json_decode($c_Value,1)[$p_Language]];
                                ?>
                                {!! Form::select('categoria_viagem', [''=>trans('whereToGo.choose_trip_category')] + $v_CategoriesArray, null, ['id' => 'categoriaViagem']) !!}
                            </div>
                        </div>
                        
                        <div class="col-lg-3">
                            <div class="col-lg-12 selectoptions ">
                                {!! Form::select('tipo_viagem', [''=>trans('whereToGo.choose_trip_type')], null, ['id' => 'tipoViagem', 'required' => 'required']) !!}
                            </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="col-lg-12 selectoptions no-padding">
                            <div id="formSearch">
                                <div class="input-group" style="width: 100%;display:block;">
                                    <div class="destination-select2-container">
                                        {!! Form::select('cidade', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['id' => 'cidade', 'class' => 'form-control select2', 'style' => 'width:100%']) !!}
                                    </div>
                                    <span class="input-group-btn" id="btn-search">
                                        <button class="btn btn-secondary btn-submit" type="submit" id="btn-submit"></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- FRAGMENTO ATUALIZADO -->

    <div class="col-lg-12" id="destinos" style="margin-top:3%;">
        <div class="container">
            <div class="col-lg-12 line no-padding" id="filterResultsDiv">
                @foreach($p_Places as $c_Index => $c_Place)
                    <?php
                        $v_Description = json_decode($c_Place['descricao_curta'],1)[$p_Language];
                        if($c_Place['tipo'] == 'destinos')
                            $v_Name = $c_Place['nome'];
                        else
                            $v_Name = json_decode($c_Place['nome'],1)[$p_Language];
                    ?>
                    @if($c_Index >= 3)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 list-thumbs" style="margin-bottom:25px;">
                            <a href="{{url($p_Language . '/' . $c_Place['tipo'] . '/' . $c_Place['slug'])}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-four">
                                        <img src="{{$c_Place['url']}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
@stop

@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        var v_Types = {!! json_encode($p_TripTypes) !!};
        $(document).ready(function()
        {
            $('.select2').select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );

            $('#categoriaViagem').change(function(){
                var v_SelectedCategoryId = $(this).val();
                var v_DataString = '';
                $.each(v_Types, function (c_Key, c_Field)
                {
                    if(c_Field.trip_category_id == v_SelectedCategoryId)
                        v_DataString += '<option value="' + c_Field.id + '">' + JSON.parse(c_Field.nome).{{$p_Language}} + '</option>';
                });
                $('#tipoViagem').html(v_DataString);
            }).trigger('change');
        });

        function submitFilter()
        {


function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}            
            var v_Data = {
                trip_type_id: $('#tipoViagem').val(),
                destination_id: $('#cidade').val()
            };
            $.get("{{url($p_Language . '/filtro-para-onde-ir')}}",v_Data).done(function(p_Data){
                if (p_Data.error == 'ok')
                {
                    var v_DataString = '';
                    if(p_Data.data.length < 1){
                        v_DataString = '<div class="col-xs-12" id="no-results">' +
                                        '<p>' + "{{trans('destination.no_results')}}" + '</p>' +
                                        '</div>';
                    }

                    $.each(p_Data.data, function (c_Key, c_Field)
                    {

                        if (IsJsonString(c_Field.nome)) {
                            var v_Nome = JSON.parse(c_Field.nome)['pt'] ;
                        } else {
                            var v_Nome = c_Field.nome;
                        }
                        
                        var v_Descricao = JSON.parse(c_Field.descricao_curta).{{$p_Language}};
                        if(c_Field.tipo == 'roteiros' || (c_Field.tipo == 'atracoes' && c_Field.trade == 0))
                            v_Nome = JSON.parse(v_Nome).{{$p_Language}};
                        v_DataString += '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 list-thumbs" style="margin-bottom:25px;">'+
                            '<a href="{{url($p_Language)}}/' + c_Field.tipo + '/' + c_Field.slug + '">'+
                            '<div class="hoverzoom">'+
                            '<div class="thumbs-mini-four">'+
                            '<img src="' + c_Field.url + '">'+
                            '</div>'+
                            '<div class="retina-hover">'+
                            '<div class="col-lg-12 title">'+
                                '<p>' + v_Nome + '</p>' +
                            '</div>'+
                            '<div class="col-lg-12 no-padding">'+
                            '<hr>'+
                            '</div>'+
                            '<div class="col-lg-12 text">'+
                            '<p>' + v_Descricao + '</p>'+
                            '</div>'+
                            '</div>'+
                            '<div class="retina">'+
                                '<p>' + v_Nome + '</p>' +
                            '</div>'+
                            '</div>'+
                            '</a>'+
                            '</div>';
                    });
                    $('#filterResultsDiv').html(v_DataString);
                }
                else
                    showNotify('danger', p_Data.error);
            }).error(function(){
            });
            return false;
        }
    </script>
@stop