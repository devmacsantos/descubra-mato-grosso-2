@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 44px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 44px;
        }
        #destinos #call-to-action a {
            width: auto;
            max-width:280px;
        }
    </style>
@stop
@section('main-content')
    <div class="row-fluid" id="destinos" style="margin-top: 180px;">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2 class="laranja">{{trans('menu.gallery')}}</h2>
            </div>
            <div class="col-lg-12" style="padding-top:4%;">
                @if(count($p_PhotoGallery) > 0)
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            @foreach($p_PhotoGallery as $c_Index => $c_Photo)
                                <div class="carousel-item {{$c_Index == 0 ? 'active' : ''}}">
                                    <img src="{{$c_Photo->url}}" alt="Galeria">
                                    <!--<div class="carousel-caption">
                                        <div class="col-lg-12" id="call-to-action-home" style="margin:0;">
                                            <a href="">{{trans('institutional.slideshow')}}</a>
                                        </div>
                                    </div>-->
                                </div>
                            @endforeach
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev" style="opacity: 1;">
                            <span class="icon-prev-prev" aria-hidden="true" style="background:none;"><img src="{{url('/portal/assets/imgs/prev.jpg')}}"></span>
                            <span class="sr-only">{{trans('institutional.previous')}}</span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next" style="opacity: 1;">
                            <span class="icon-next-next" aria-hidden="true" style="background:none;"><img src="{{url('/portal/assets/imgs/next.jpg')}}"></span>
                            <span class="sr-only">{{trans('institutional.next')}}</span>
                        </a>
                    </div>
                @endif
            </div>
        </div>

        <div class="col-lg-12" id="proximidades" style="padding-top:1rem;">
            <div class="container">
                <div class="col-lg-12">
                    <div class="textGallery">
                        <p style="text-align:center;">{{$p_Content['nome']}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" id="colunms">
                        <p>{{ json_decode($p_Content['descricao_curta'],1)[$p_Language] }}</p>
                    </div>
                    <div class="col-lg-12" id="call-to-action">
                        <a href="{{url($p_Language . '/destinos/' . $p_Content['slug'])}}" style="padding: 1rem;">{{trans('gallery.access_destination')}}</a>
                    </div>
                </div>
            </div>
        </div>

        @if(count($p_Destinations) > 0)

            <div class="col-lg-12" id="proximidades" style="padding:1rem 0;">
                <div class="container">
                    <hr style="background:#5f5f5f;height:1px;width:100%;margin-bottom:2rem;">
                    <div class="col-lg-12" id="list-title">
                        <h2 class="laranja">{{trans('gallery.more_galleries')}}</h2>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="container">
                    <div class="col-lg-12">
                        <div class="col-lg-8 col-lg-offset-2">
                            {!! Form::open(array('method' => 'get', 'id' => 'galleryForm')) !!}
                            <div class="input-group" style="margin-top:1rem;">
                                {!! Form::select('', ["" => trans('planYourTrip.select_destination')] + $p_DestinationList, null, ['id' => 'selectCities', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width:100%;padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
    @if(count($p_Destinations) > 0)
        <!-- FRAGMENTO ATUALIZADO -->
        <div class="row-fluid" id="destinos" style="padding:1% 0;">
            <div class="container">
                <div class="col-lg-12 line">
                    @foreach($p_Destinations as $c_Destination)
                        <?php $v_Description = json_decode($c_Destination['descricao_curta'],1)[$p_Language]; ?>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <a href="{{url($p_Language . '/conheca/galeria/' . $c_Destination['slug'])}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-four">
                                        <!--<img src="{{$c_Destination['url']}}">-->
                                         <div class="thumbs-mini-recorte" style="background: url('{{$c_Destination['url']}}') no-repeat;"></div>
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$c_Destination['nome']}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$c_Destination['nome']}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- FRAGMENTO ATUALIZADO -->
    @endif
@stop

@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $('#selectCities').select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );
            $('#selectCities').change(function(){
                location.href = "{{url($p_Language . '/conheca/galeria')}}" + '/' + $(this).val();
            });
        });
    </script>

@stop