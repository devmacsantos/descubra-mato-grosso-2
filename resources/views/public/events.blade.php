@extends('public.base')
@section('pageCSS')
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/daterange/daterangepicker.css')}}">
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 41px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 41px;
        }
    </style>
@stop
@section('main-content')
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2>{{trans('menu.events')}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" style="margin-bottom:50px;">
                        <p>
                            {!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}
                        </p>
                    </div>
                </div>
            </div>

            @if(count($p_MainEvents) > 0)
                <div class="col-lg-12 line">
                    @foreach($p_MainEvents as $c_Index => $c_Event)
                        @if($c_Index < 2)
                            <div class="col-lg-6 col-md-6 col-sm-6 list-thumbs-full">
                                <a href="{{url($p_Language . '/eventos/' . $c_Event->slug)}}">
                                    <div class="hoverzoom">
                                        <div class="thumbs-full">
                                            <img src="{{$c_Event->url == null ? url('/portal/assets/imgs/avatar-evento.jpg') : $c_Event->url}}">
                                        </div>
                                        <div class="retina-hover">
                                            <div class="col-lg-12 title">
                                                <p>{{$c_Event->nome}}</p>
                                            </div>
                                            <div class="col-lg-12 no-padding">
                                                <hr>
                                            </div>
                                            <div class="col-lg-12 text">
                                                <p>{{json_decode($c_Event->descricao_curta,1)[$p_Language]}}</p>
                                            </div>
                                        </div>
                                        <div class="retina">
                                            <p>{{$c_Event->nome}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
                @if(count($p_MainEvents) >= 2)
                    <div class="col-lg-12 line">
                        @foreach($p_MainEvents as $c_Index => $c_Event)
                            @if($c_Index >= 2 && $c_Index < 5)
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <a href="{{url($p_Language . '/eventos/' . $c_Event->slug)}}">
                                        <div class="hoverzoom">
                                            <div class="thumbs-mini-three">
                                                <img src="{{$c_Event->url == null ? url('/portal/assets/imgs/avatar-evento.jpg') : $c_Event->url}}">
                                            </div>
                                            <div class="retina-hover">
                                                <div class="col-lg-12 title">
                                                    <p>{{$c_Event->nome}}</p>
                                                </div>
                                                <div class="col-lg-12 no-padding">
                                                    <hr>
                                                </div>
                                                <div class="col-lg-12 text">
                                                    <p>{{json_decode($c_Event->descricao_curta,1)[$p_Language]}}</p>
                                                </div>
                                            </div>
                                            <div class="retina">
                                                <p>{{$c_Event->nome}}</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                @endif
            @endif

            <div class="col-lg-12" id="destination-events-helper"></div>
            <div class="col-lg-12" style="padding:2% 0;">
                <div class="col-lg-12 hospedagem">
                    <h2 style="color:#b4b4b4;">
                        {{trans('events.find_event')}}
                    </h2>
                    <p style="color:#4b4b4b;font-size:16px;">
                        {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                    </p>
                </div>
            </div>
        </div>
        <div class="col-lg-12" id="tarja">
            <div class="container">
                <div class="col-lg-12">
                    <div class="row" id="options">
                        {!! Form::open(array('onsubmit' => 'return submitFilter()')) !!}
                            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1">
                                <div class="col-lg-4 col-lg-offset-2 col-md-5 col-sm-3 col-xs-12" id="inputDestino">
                                    {!! Form::select('cidadeEvento', [], null, ['id' => 'cidadeEvento', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12" id="inputDestino">
                                    <input id="dataEvento" name="data_evento" type="text" class="form-control" aria-label="Digite sua busca" placeholder="{{trans('events.event_date')}}">
                                </div>
                                <div class="col-lg-1 col-md-3 col-sm-1 col-xs-12" id="submit-tarja">
                                    <input class="btn" type="submit" value="{{trans('whereToGo.search')}}" style="padding: 0.4rem 1rem;">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12"  style="padding:2rem 0;">
            <div class="container">
                <div class="col-lg-12 line" style="padding:0;" id="filterResultsDiv">
                    @foreach($p_MainEvents as $c_Index => $c_Event)
                        @if($c_Index >= 5)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                <a href="{{url($p_Language . '/eventos/' . $c_Event->slug)}}">
                                    <div class="hoverzoom">
                                        <div class="thumbs-mini-four">
                                            <img src="{{$c_Event->url == null ? url('/portal/assets/imgs/avatar-evento.jpg') : $c_Event->url}}">
                                        </div>
                                        <div class="retina-hover">
                                            <div class="col-lg-12 title">
                                                <p>{{$c_Event->nome}}</p>
                                            </div>
                                            <div class="col-lg-12 no-padding">
                                                <hr>
                                            </div>
                                            <div class="col-lg-12 text">
                                                <p>{{json_decode($c_Event->descricao_curta,1)[$p_Language]}}</p>
                                            </div>
                                        </div>
                                        <div class="retina">
                                            <p>{{$c_Event->nome}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageScript')
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/daterangepicker.js')}}"></script>
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        var v_Destinations = {!! json_encode($p_Destinations) !!};
        var v_SelectedDestination = '{{$p_CityFilter}}';

        function isDestinationSelected(p_DestinationSlug){
            if(v_SelectedDestination.length > 0 && v_SelectedDestination == p_DestinationSlug)
                return 'selected';
            return '';
        }

        $(document).ready(function()
        {
            //cities select2
            var v_DataString = '<option value="">{{trans('planYourTrip.select_destination')}}</option>';
            $.each(v_Destinations, function (c_Key, c_Destination)
            {
                v_DataString += '<option value="' + c_Destination.city_id + '" ' + isDestinationSelected(c_Destination.slug) + '>' + c_Destination.nome + '</option>';
            });

            $('.select2').html(v_DataString).select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );

            // daterange plugin options
            $('input#dataEvento').daterangepicker({
                "autoApply": true,
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "daysOfWeek": ['{!!trans('string.sunday_abbreviation')!!}',
                        '{!!trans('string.monday_abbreviation')!!}',
                        '{!!trans('string.tuesday_abbreviation')!!}',
                        '{!!trans('string.wednesday_abbreviation')!!}',
                        '{!!trans('string.thursday_abbreviation')!!}',
                        '{!!trans('string.friday_abbreviation')!!}',
                        '{!!trans('string.saturday_abbreviation')!!}'],
                    "monthNames": ['{!!trans('string.january')!!}',
                        '{!!trans('string.february')!!}',
                        '{!!trans('string.march')!!}',
                        '{!!trans('string.april')!!}',
                        '{!!trans('string.may')!!}',
                        '{!!trans('string.june')!!}',
                        '{!!trans('string.july')!!}',
                        '{!!trans('string.august')!!}',
                        '{!!trans('string.september')!!}',
                        '{!!trans('string.october')!!}',
                        '{!!trans('string.november')!!}',
                        '{!!trans('string.december')!!}'],
                    "firstDay": 1
                },
                "linkedCalendars": false,
                "autoUpdateInput": false,
                "opens": "center"
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY')).blur();
            }).mask('99/99/9999 - 99/99/9999');

            if(v_SelectedDestination.length > 0){
                $('html, body').animate({scrollTop: $("#destination-events-helper").offset().top - 20}, 200);
                submitFilter();
            }
        });

        function submitFilter()
        {
            if($('#cidadeEvento').val().length > 0 || ($('#dataEvento').val().length > 0 && $('#dataEvento').val().indexOf('_') == -1))
            {
                $.get("{{url($p_Language . '/filtro-eventos')}}",{
                    'city_id': $('#cidadeEvento').val(),
                    'date_range': $('#dataEvento').val()
                }).done(function(p_Data){
                    if (p_Data.error == 'ok')
                    {
                        var v_DataString = '';

                        if(p_Data.data.length < 1){
                            v_DataString = '<div class="col-xs-12" id="no-results">' +
                                    '<p>' + "{{trans('destination.no_results')}}" + '</p>' +
                                    '</div>';
                        }

                        $.each(p_Data.data, function (c_Key, c_Field)
                        {
                            v_DataString += '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">'+
                                '<a href="{{url($p_Language . '/eventos/')}}/' + c_Field.slug + '">'+
                                '<div class="hoverzoom">'+
                                '<div class="thumbs-mini-four">'+
                                '<img src="' + (c_Field.url ? c_Field.url : '{{url('/portal/assets/imgs/avatar-evento.jpg')}}') + '">'+
                                '</div>'+
                                '<div class="retina-hover">'+
                                '<div class="col-lg-12 title">'+
                                '<p>' + c_Field.nome + '</p>'+
                                '</div>'+
                                '<div class="col-lg-12 no-padding">'+
                                '<hr>'+
                                '</div>'+
                                '<div class="col-lg-12 text">'+
                                '<p>' + JSON.parse(c_Field.descricao_curta)['{{$p_Language}}'] + '</p>'+
                                '</div>'+
                                '</div>'+
                                '<div class="retina">'+
                                '<p>' + c_Field.nome + '</p>'+
                                '</div>'+
                                '</div>'+
                                '</a>'+
                                '</div>';
                        });
                        $('#filterResultsDiv').html(v_DataString);
                    }
                    else
                        showNotify('danger', p_Data.error);
                }).error(function(){
                });
            }
            else
                showNotify('danger', '{!!trans('events.fill_form_message')!!}');

            return false;
        }
    </script>
@stop