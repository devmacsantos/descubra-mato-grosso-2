@extends('public.base')
@section('pageCSS')
@stop
@section('main-content')


<div class="row-fluid" id="destinos" >
    <div class="container">
        <div class="col-lg-12">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="col-lg-12" style="margin-bottom:50px;">
                    @if($p_Search)
                        <p id="statusResult">{{count($destinos)+count($atracoes)+count($eventos)}} {{trans('search.search_results_for')}}</p>
                        <p id="result">" {{$p_SearchString}} "</p>
                    @else
                        <p id="statusResult">{{count($p_Attractions) . ' ' .trans('search.attractions_in') . ' ' . $p_SearchString}}</p>
                    @endif
                </div>
            </div>
        </div>


<!-- Destinos -->
<div class="col-lg-12 line">

@if(count($destinos))

    @foreach($destinos as $destino)
        <?php $v_Description = json_decode($destino->descricao_curta,1)[$p_Language]; ?>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12" style="height:280px;">
            <a href="{{url($p_Language . '/destinos/' . $destino->slug)}}">
                <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <!--<img src="{{$destino->url}}">-->
                            <div class="thumbs-mini-recorte" style="background: url('{{$destino->url}}') no-repeat;"></div>
                    </div>
                </div>
                <div class="details-hotel">
                            <h4 style='color:red;' >{{$destino->nome}}</h4>
                     
                </div>   
            </a>
        </div>
        @endforeach

@endif



 <!-- eventos -->            
 @if(count($eventos))

    @foreach($eventos as $evento)
        <?php
            $v_Description = json_decode($evento->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $evento->nome;
            //print_r($v_Name);exit();    
        ?>
        <div class="col-3 col-lg-3 col-md-6 col-sm-12 col-xs-12" style="height:280px;">
            
            <a href="{{url($p_Language . '/eventos/' . $evento->c_slug . '/' . $evento->slug)}}">
                                        <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <img src="{{$evento->url}}">
                    </div>
                </div>
            
            <div class="details-hotel">
                <h4 style='color:red;' >{{$evento->nome}} </h4>
                <p style="text-align:center;">{{getLiteralDate($evento->inicio, $evento->termino)}}</p>
                <p style="text-align:center;">{{$evento->cidade}}</p>
            </div>
            </a>
        </div>
    @endforeach

@endif


 <!-- atrações -->            
@if(count($atracoes))

    @foreach($atracoes as $atracao)
        <?php
            $v_Description = json_decode($atracao->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $atracao->nome_pt;
            //print_r($v_Name);exit();    
        ?>
        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" style="height:280px;">
        @if(in_array($atracao->item_inventario,['C1','C2','C3','C4']))   
            <a  href="{{url($p_Language . '/atracoes/' . $atracao->c_slug .'/'.  $atracao->slug)}}">
        @elseif(in_array($atracao->item_inventario,['B1','B2','B3','B4','B5','B6']))
            <a style='color:red;'  href="{{url($p_Language . '/apoio/' . $atracao->c_slug .'/'. $atracao->slug)}}">
        @endif
                <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <img src="{{$atracao->url}}">
                    </div>
                </div>
           
            <div class="details-hotel">
            
            <p style="text-align:center;">{{$tipo[$atracao->type_id]}} </p>
            <h4 style='color:red;' >{{$atracao->nome_pt}}</h4>
            <p style="text-align:center;">{{$atracao->cidade}}</p>
            </div>
            </a>
        </div>
    @endforeach

@endif
</div>
<!-- mais -->
@if($mais_atracoes)
<h3 class="text-center" style="margin-bottom:2em;font-size:2em">Outras atrações no municipio </h3>
<div class="col-lg-12 line">
@foreach($mais_atracoes as $atracao)
        <?php
            $v_Description = json_decode($atracao->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $atracao->nome_pt;
            //print_r($v_Name);exit();    
        ?>
        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" style="height:280px;">
        @if(in_array($atracao->item_inventario,['C1','C2','C3','C4']))   
            <a  href="{{url($p_Language . '/atracoes/' . $atracao->c_slug .'/'.  $atracao->slug)}}">
        @elseif(in_array($atracao->item_inventario,['B1','B2','B3','B4','B5','B6']))
            <a style='color:red;'  href="{{url($p_Language . '/apoio/' . $atracao->c_slug .'/'. $atracao->slug)}}">
        @endif
                <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <img src="{{$atracao->url}}">
                    </div>
                </div>
           
            <div class="details-hotel">
            
            <p style="text-align:center;">{{$tipo[$atracao->type_id]}} </p>
            <h4 style='color:red;' >{{$atracao->nome_pt}}</h4>
            <p style="text-align:center;">{{$atracao->cidade}}</p>
            </div>
            </a>
        </div>
    @endforeach
</div>
@endif
<!--fim outras atrações -->

<!--outros eventos -->
@if($mais_eventos)
<h3 class="text-center" style="margin-bottom:2em;font-size:2em">Outros eventos no municipio </h3>
<div class="col-lg-12 line">

@foreach($mais_eventos as $evento)
        <?php
            $v_Description = json_decode($evento->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $evento->nome;
            //print_r($v_Name);exit();    
        ?>
        <div class="col-3 col-lg-3 col-md-6 col-sm-12 col-xs-12" style="height:280px;">
            
            <a href="{{url($p_Language . '/eventos/' . $evento->c_slug . '/' . $evento->slug)}}">
                                        <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <img src="{{$evento->url}}">
                    </div>
                </div>
            
            <div class="details-hotel">
                <h4 style='color:red;' >{{$evento->nome}} </h4>
                <p style="text-align:center;">{{getLiteralDate($evento->inicio, $evento->termino)}}</p>
                <p style="text-align:center;">{{$evento->cidade}}</p>
            </div>
            </a>
        </div>
    @endforeach
</div>
@endif
</div>
<!-- fim outros eventos -->

<p class="text-center"><a href="/pt/eventos"> <img src="{{url('/assets/img/eventosanteriores.png')}}" ></a>

</div>
@stop

@section('pageScript')
@stop