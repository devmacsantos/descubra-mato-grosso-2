@extends('public.base')

@section('pageCSS')

@stop

@section('main-content')
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2>{{trans('menu.parks')}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12" style="margin-bottom:50px;">
                        <p>{!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}</p>
                    </div>
                </div>
            </div>


            <div class="col-lg-12 line">
                @foreach($p_Parks as $c_Index => $c_Park)
                    @if($c_Index < 2)
                        <?php
                            $v_Description = json_decode($c_Park->descricao_curta,1)[$p_Language];
                            $v_Name = json_decode($c_Park->nome,1)[$p_Language];
                        ?>
                        <div class="col-lg-6 list-thumbs-full">
                            <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Park->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-full">
                                        <img src="{{$c_Park->url}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="col-lg-12 line">
                @foreach($p_Parks as $c_Index => $c_Park)
                    @if($c_Index >= 2 && $c_Index < 5)
                        <?php
                            $v_Description = json_decode($c_Park->descricao_curta,1)[$p_Language];
                            $v_Name = json_decode($c_Park->nome,1)[$p_Language];
                        ?>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Park->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-three">
                                        <img src="{{$c_Park->url}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_Description}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_Name}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="col-lg-12" style="padding:2% 0;">
                <div class="col-lg-12 hospedagem">
                    <h2 style="color:#b4b4b4; font-size:48px;">
                        {{trans('whereToGo.find_perfect_park')}}
                    </h2>
                    <p style="color:#4b4b4b;font-size:16px;font-family: Signika;">
                        {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                    </p>
                </div>
            </div>
        </div>

        <div class="col-lg-12" id="tarja">
            <div class="container">
                <div class="col-lg-12">
                    <div class="row" id="options">
                        {!! Form::open(array('onsubmit' => 'return submitFilter()')) !!}
                            <div class="col-lg-12">
                                <div class="col-lg-3 no-padding">
                                    <p style="font-size:20px;">{{trans('whereToGo.what_do_you_expect')}}</p>
                                </div>
                                <div class="col-lg-3 selectoptions no-padding">
                                    <?php
                                    $v_CategoriesArray = [];
                                    foreach($p_TripCategories as $c_CategoryIndex => $c_Value)
                                        $v_CategoriesArray += [$c_CategoryIndex => json_decode($c_Value,1)[$p_Language]];
                                    ?>
                                    {!! Form::select('categoria_viagem', [''=>trans('whereToGo.choose_trip_category')] + $v_CategoriesArray, null, ['id' => 'categoriaViagem']) !!}
                                </div>
                                <div class="col-lg-3 selectoptions">
                                    {!! Form::select('tipo_viagem', [''=>trans('whereToGo.choose_trip_type')], null, ['id' => 'tipoViagem', 'required' => 'required']) !!}
                                </div>
                                <div class="col-lg-3 selectoptions no-padding" id="submit-tarja">
                                    <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12"  style="padding:2rem 0;">
            <div class="container">
                <div class="col-lg-12 line" style="padding:0;" id="filterResultsDiv">
                    @foreach($p_Parks as $c_Index => $c_Park)
                        @if($c_Index >= 3)
                            <?php
                                $v_Description = json_decode($c_Park->descricao_curta,1)[$p_Language];
                                $v_Name = json_decode($c_Park->nome,1)[$p_Language];
                            ?>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <a href="{{url($p_Language . '/atracoes/' . get_city_slug($c_Park->city_id) . '/' . $c_Park->slug)}}">
                                    <div class="hoverzoom">
                                        <div class="thumbs-mini-four">
                                            <img src="{{$c_Park->url}}">
                                        </div>
                                        <div class="retina-hover">
                                            <div class="col-lg-12 title">
                                                <p>{{$v_Name}}</p>
                                            </div>
                                            <div class="col-lg-12 no-padding">
                                                <hr>
                                            </div>
                                            <div class="col-lg-12 text">
                                                <p>{{$v_Description}}</p>
                                            </div>
                                        </div>
                                        <div class="retina">
                                            <p>{{$v_Name}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageScript')
    <script>
        var v_Types = {!! json_encode($p_TripTypes) !!};
        $(document).ready(function()
        {
            $('#categoriaViagem').change(function(){
                var v_SelectedCategoryId = $(this).val();
                var v_DataString = '';
                $.each(v_Types, function (c_Key, c_Field)
                {
                    if(c_Field.trip_category_id == v_SelectedCategoryId)
                        v_DataString += '<option value="' + c_Field.id + '">' + JSON.parse(c_Field.nome).{{$p_Language}} + '</option>';
                });
                $('#tipoViagem').html(v_DataString);
            }).trigger('change');
        });

        function submitFilter()
        {
            $.get("{{url($p_Language . '/filtro-parques')}}",{
                'trip_type_id': $('#tipoViagem').val()
            }).done(function(p_Data){
                if (p_Data.error == 'ok')
                {
                    var v_DataString = '';

                    if(p_Data.data.length < 1){
                        v_DataString = '<div class="col-xs-12" id="no-results">' +
                                '<p>' + "{{trans('destination.no_results')}}" + '</p>' +
                                '</div>';
                    }

                    $.each(p_Data.data, function (c_Key, c_Field)
                    {
                        var v_Nome = JSON.parse(c_Field.nome).{{$p_Language}};
                        var v_Descricao = JSON.parse(c_Field.descricao_curta).{{$p_Language}};
                        v_DataString += '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">'+
                            '<a href="{{url($p_Language . '/atracoes/')}}/' + c_Field.slug + '">'+
                            '<div class="hoverzoom">'+
                            '<div class="thumbs-mini-four">'+
                            '<img src="' + c_Field.url + '">'+
                            '</div>'+
                            '<div class="retina-hover">'+
                            '<div class="col-lg-12 title">'+
                                '<p>' + v_Nome + '</p>' +
                            '</div>'+
                            '<div class="col-lg-12 no-padding">'+
                            '<hr>'+
                            '</div>'+
                            '<div class="col-lg-12 text">'+
                            '<p>' + v_Descricao + '</p>'+
                            '</div>'+
                            '</div>'+
                            '<div class="retina">'+
                                '<p>' + v_Nome + '</p>' +
                            '</div>'+
                            '</div>'+
                            '</a>'+
                            '</div>';
                    });
                    $('#filterResultsDiv').html(v_DataString);

                }
                else
                    showNotify('danger', p_Data.error);
            }).error(function(){
            });
            return false;
        }
    </script>
@stop