<!DOCTYPE html>
<html lang="pt-br">
    <head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=REPLACEME"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'REPLACEME');
</script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="no-cache" http-equiv="pragma">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:title" content="">
        <meta property="og:description" content="">
        <meta property="og:image" content="">
        <meta name="description" content="Descubra Mato Grosso">
        <meta name="author" content="Descubra Mato Grosso">
        <link rel="icon" href="{{url('/favicon.ico')}}"/>
        <title>Descubra Mato Grosso</title>
        <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/portal/assets/css/main.css')}}">
        <style>

            @font-face {
                font-family: 'FontAwesome';
                src: url('{{url('/assets/fonts/font-awesome/fontawesome-webfont78ce.eot?v=4.2.0')}}');
                src: url('{{url('/assets/fonts/font-awesome/fontawesome-webfontd41d.eot?#iefix&v=4.2.0')}}') format('embedded-opentype'), url('{{url('/assets/fonts/font-awesome/fontawesome-webfont78ce.woff?v=4.2.0')}}') format('woff'), url('{{url('/assets/fonts/font-awesome/fontawesome-webfont78ce.ttf?v=4.2.0')}}') format('truetype'), url('{{url('/assets/fonts/font-awesome/fontawesome-webfont78ce.svg?v=4.2.0#fontawesomeregular')}}') format('svg');
                font-weight: normal;
                font-style: normal;
            }
            .fa {
                display: inline-block;
                font: normal normal normal 14px/1 FontAwesome;
                font-size: inherit;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                -moz-osx-font-smoothing: grayscale;
            }
            .fa-chevron-left:before {
                content: "\f053";
            }
            .fa-chevron-right:before {
                content: "\f054";
            }
            #destinos .nav-tabs .nav-item .nav-link.active .hospedagem p {
                color: #ef662f;
            }
            body>.alert.alert-danger{
                border-color: #bd1a3a;
                border-radius:0;
                background-color: #c71a3d;
                color:#fff;
            }
            body>.alert.alert-info{
                border-color: #b3e1a9;
                border-radius:0;
                background-color: #b7e6ad;
                color:#555;
            }
            body>.alert>button.close{
                opacity:0.4;
            }
          
            .menu-resumido-desk{
                margin-top:-200px;
                -webkit-transition: margin-top 1s; /* Safari */
                transition: margin-top 1s;
            }
            .menu-resumido-desk.menu-fixo{
                margin-top:0;
                display: block !important;

            }
            em{font-style: italic;}
            @media (max-width: 543px) {
                #destinos{
                    margin-top:80px;
                    padding:1% 0;
                }
            }            
            @media (max-width: 980px) 
            {
                .menu-resumido-desk.menu-fixo{
                    margin-top:0;
                    display: none !important;

                }
            }

            #video-container{
                display: none;
            }
            #titleVisiteMinas{
                display: none;
            }

            @media (min-width: 544px) and (max-width:767)
            {
                #destinos{

                    padding:1% 0;
                }
            }

            @media (min-width: 768px) 
            {
                #destinos{
                    
                    padding:1% 0;
                }
            }


        </style>
        @yield('pageCSS')
    </head>
    <body data-spy="scroll" data-target=".navbar-fixed-top">
        <?php $v_Categories = \App\TripCategory::getCategories(6); ?>
        @include('public.header', ['p_HeaderCategories' => $v_Categories])
        @yield('main-content')
        @include('public.templates.footer', ['p_FooterCategories' => $v_Categories])
        @include('public.scripts')
        @yield('pageScript')



    </body>
</html>
