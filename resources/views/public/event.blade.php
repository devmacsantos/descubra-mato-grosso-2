@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/lightbox/css/lightbox.css')}}" rel="stylesheet">
    <style>
        .col-row-fluid.maps{
            margin-right: 0;
            margin-left: 0;
        }
    </style>
@stop

@section('main-content')
    <div class="row-fluid banner-evento" id="destinos" style="padding:0;">
        <div class="col-lg-12 no-padding">
            <div class="caption" style="opacity:1;width: 100%;height: 100%;top:0;background: rgba(0, 0, 0, 0.6);">
                <div class="caption-content">
                    <div class="container" id="map-atracao">
                        <div class="col-lg-12 no-padding">
                            <div class="col-lg-12 text-highlight">
                                <?php
                                if(count($p_Dates) > 1)
                                {
                                    $v_EventPeriod = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_Dates[0]->date)->format('d/m/Y') . ' - ';
                                    $v_EventPeriod .= \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_Dates[count($p_Dates) - 1]->date)->format('d/m/Y');
                                }
                                else
                                    $v_EventPeriod = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_Dates[0]->date)->format('d/m/Y');
                                ?>
                                <p class="text-center event-date" style="margin:0;">{{ $v_EventPeriod }}</p>
                                <h2 id="nameCity" class="text-center" style="text-transform: uppercase;">{{$p_Content == null ? '' : $p_Content->nome}}</h2>
                                <p style="text-align:center;font-size:28px;">
                                    <img src="{{url('/portal/assets/imgs/geotag-white.png')}}" alt="" style="margin-right:10px;margin-top: -8px;">
                                    {{$p_Content == null ? '' : $p_Content->cidade}}
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-12 no-padding favorite">
                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-sm-12 col-xs-12 no-padding" id="share">
                                <div class="social">
                                    <ul id="icons-social">
                                        <div class="addthis_sharing_toolbox"></div>
                                    </ul>
                                    <p id="share">{{trans('string.share')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="highlight-active-larger">
                <!--<img src="{{$p_Content->url}}" alt="">-->
                 <div style="background: url('{{$p_Content->url == null ? url('/portal/assets/imgs/avatar-evento.jpg') : $p_Content->url}}') no-repeat;" class="corte-destaque-interna"></div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos">
        <div class="container" style="margin-top:1rem;">
            <div class="col-lg-12 no-padding">
                @foreach($p_PhotoGallery as $c_Photo)
                    <div class="col-sm-2 col-xs-3 list-thumbs-zoom" style="margin-bottom: 10px;">
                        <div class="caption-min">
                            <a href="{{$c_Photo->url}}" data-lightbox="image-gallery" class="info">
                                <div class="caption-content">
                                    <img src="{{url('/portal/assets/imgs/zoom.png')}}" alt="">
                                </div>
                            </a>
                        </div>

                        <div class="highlight-atraction">
                            <img src="{{$c_Photo->url}}" alt="">
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-lg-12" style="margin:3rem 0 3rem 0;">
                <div class="col-lg-8 col-lg-offset-2">
                    <?php $v_Description = $p_Content == null ? '' : json_decode($p_Content->sobre,1)[$p_Language]; ?>
                    <p>{!! Markdown::text($v_Description) !!}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="localization" style="display:block;">
        <div class="container">
            <div class="content-address" style="margin-bottom: 3%;">
                <div class="col-lg-4 col-lg-offset-4" id="contactArea" style="padding:2% 0;border-top:1px solid #dadada;border-bottom:1px solid #dadada;">
                    <?php $v_HasContact = false; ?>
                    @if(strlen($p_Content->telefone) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p>Tel.: {{$p_Content->telefone}}</p>
                        </div>
                    @endif
                    @if(strlen($p_Content->whatsapp) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p>WhatsApp.: {{$p_Content->whatsapp}}</p>
                        </div>
                    @endif
                    @if(strlen($p_Content->site) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p><a href="{{$p_Content->site}}" target="_blank">{{$p_Content->site}}</a></p>
                        </div>
                    @endif
                    @if(strlen($p_Content->site) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p><a href="mailto:{{$p_Content->email}}">{{$p_Content->email}}</a></p>
                        </div>
                    @endif
                    <div class="col-lg-12" style="display:flex;margin:0.5rem 0;">
                        <ul style="display: inline-block;float: left;">
                            @if(strlen($p_Content->facebook) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->facebook}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-fb-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->instagram) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->instagram}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-insta-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->twitter) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->twitter}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-tw-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->youtube) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->youtube}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-youtube-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(strlen($p_Content->flickr) > 0)
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->flickr}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-flicker-gray.png')}}" alt=""></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                @if(!$v_HasContact)
                    <style>
                        #contactArea{
                            display:none!important;
                        }
                    </style>
                @endif

                <div class="col-lg-4 col-lg-offset-4" style="padding:2% 0;">
                    <p>{{$p_Content->logradouro . ', ' . $p_Content->numero . (empty($p_Content->complemento) ? '' : (', ' . $p_Content->complemento)) . ' - ' . $p_Content->bairro}}</p>
                    <p>CEP {{$p_Content->cep . ' - ' . $p_Content->cidade}} - MG</p>
                </div>
            </div>
        </div>
    </div>


    @if($p_Content != null && $p_Content->latitude != null && $p_Content->longitude != null)
        <div class="row-fluid maps" id="map_canvas">
            <div class="col-lg-12" id="map_canvas"></div>
        </div>
    @endif

    <?php
    $v_HasUsefulInformation = false;
    $v_RelevantData =  json_decode($p_Content->dados_relevantes,1)[$p_Language];

    foreach($v_RelevantData as $c_Data)
    {
        if(strlen($c_Data['dado']) > 0 && strlen($c_Data['descricao']) > 0)
        {
            $v_HasUsefulInformation = true;
            break;
        }
    }
    $v_Description = $p_Content == null ? '' : json_decode($p_Content->informacoes_uteis,1)[$p_Language];
    ?>

    @if($v_HasUsefulInformation || $v_Description != '')
    <div class="row-fluid" id="useful-information">
        <div class="container">
        <div class="cards col-lg-12">
            <div class="col-lg-12">
                <h2><img src="{{url('/portal/assets/imgs/icon-4.png')}}" alt="" style="margin-right:15px;">{{trans('attraction.useful_information')}}</h2>
            </div>
           
            @if($p_Content != null)
                @foreach($v_RelevantData as $c_Data)
                    @if(strlen($c_Data['dado']) > 0 && strlen($c_Data['descricao']) > 0)
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{$c_Data['dado']}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$c_Data['descricao']}}</p>
                        </div>
                    </div>
                    </div>
                    @endif
                @endforeach
            @endif
            </div>
            <div class="col-lg-12" style="margin:3rem 0 3rem 10px;">
                <div class="col-md-8 col-md-offset-2">
                    <p>{!! nl2br($v_Description) !!}</p>
                </div>
            </div>
        </div>
    </div>
    @endif
@stop

@section('pageScript')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a0debd79357d1c" async="async"></script>
<script src="{{url('/vendor/lightbox/js/lightbox.min.js')}}"></script>
<script>
    $(document).ready(function()
    {
        @if($p_Content != null && $p_Content->latitude != null && $p_Content->longitude != null)
            $('#map_canvas').gmap3({
                marker: {
                    values:[
                        {latLng:[{{ $p_Content->latitude }}, {{ $p_Content->longitude }}]}
                    ],
                    options:{
                        draggable: false
                    }
                },
                map:{
                    options:{
                        zoom:15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        navigationControl: false,
                        scrollwheel: false,
                        streetViewControl: true
                    }
                }
            });
        @endif
    });
</script>
@stop