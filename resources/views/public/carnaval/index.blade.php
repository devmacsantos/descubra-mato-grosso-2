@extends('public.templates.base')
@section('pageCSS')


<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsIndex.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsSearchbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/fonts/pwchalk/stylesheet.css')}}">

@endsection

@section('header')
@include('public.carnaval.header')
@endsection



@section('main-content')
<main class="container-fluid mt-4">

    @include('public.carnaval.eventsCity')

    <!--BH -->
    <div class="container mt-3">
        <div class="title">
            <h2 class="text-uppercase" style="font-family:pwchalk; color:red;">
                <strong>PROGRAMAÇÃO BH</strong>
            </h2>
        </div>

        <div class="row" >
            <div class="col-sm px-5 ">
                <a href="http://www.carnavaldebelohorizonte.com.br/blocos-de-rua/" target="_blank">
                    <img class="card-img-top fade-image"
                        src="{{url('/assets/img/carnaval/p1-blocosderua.png')}}">
                </a>
            </div>
            <div class="col-sm px-5 ">
                <a href="http://www.carnavaldebelohorizonte.com.br/palcos-oficiais/" target="_blank">
                    <img class="card-img-top fade-image"
                        src="{{url('/assets/img/carnaval/p2-palcosoficiais.png')}}">
                </a>
            </div>
            <div class="col-sm px-5">
                <a href="http://www.carnavaldebelohorizonte.com.br/escolas-de-samba/" target="_blank">
                    <img class="card-img-top fade-image"
                        src="{{url('/assets/img/carnaval/p3-escoladesamba.png')}}">
                </a>
            </div>     
             <div class="col-sm px-5 ">
                <a href="http://www.carnavaldebelohorizonte.com.br/blocos-caricatos/" target="_blank">
                    <img class="card-img-top fade-image"
                        src="{{url('/assets/img/carnaval/p4-blocoscaricatos.png')}}">
                </a>
            </div>  
            <div class="col-sm px-5 ">
                <a href="http://www.carnavaldebelohorizonte.com.br/roteiro-para-surpreender-as-criancas/" target="_blank">
                    <img class="card-img-top fade-image"
                        src="{{url('/assets/img/carnaval/p5-paracriancas.png')}}">
                </a>
            </div>                                
        </div>
    </div>
    


    @if (isset($themeEvent) && (count($themeEvent)>0)) {{--Verifica Eventos de tema--}}
    <div class="container">
        <div class="title">
            <h2 class="text-uppercase pt-4" style="font-family:pwchalk; color:red;">
                <strong>CARNAVAL BH</strong>
            </h2>
        </div>

        <section class="eventos-tema slider">

            @foreach ($themeEvent as $vEvent) {{--Início foreach Eventos de tema--}}
            @if ($vEvent->EventPhotos(true)->first() != null) {{-- Verifica se o evento tem foto de capa --}}

            <div class="slide text-center">

                <div class="">
                    <div class="card mb-4 shadow">
                        <a href="{{url($p_Language.'/carnaval-2019/'.$vEvent->Destination->slug.'/' .$vEvent->slug)}}">
                            <img class="card-img-top fade-image"
                                src="/image/330/248/true/true/{{$vEvent->EventPhotos(true)->first()->url}}">
                        </a>
                        <div class="card-body pb-4">
                            <div
                                class="border border-top-0 border-left-0 border-right-0 text-center text-uppercase text-truncate text-bold">
                                <a class="link-no-hover title"
                                    href="{{url($p_Language.'/carnaval-2019/'.$vEvent->Destination->slug.'/' .$vEvent->slug)}}"
                                    title="{{$vEvent->nome}}" data-toggle="popover" data-trigger="hover"
                                    data-content="{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}"
                                    data-placement="top">{{$vEvent->nome}}
                                </a>
                            </div>

                            <table class="float-left">
                                <tr>
                                    <th class="border border-0 calendar-month-single text-light px-2 text-uppercase">
                                        {{getMonthName(date_format(date_create($vEvent->inicio),'m'))}}
                                    </th>
                                </tr>
                                @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'd') !=
                                date_format(date_create($vEvent->fim), 'd')) &&
                                (date_format(date_create($vEvent->inicio), 'm') ==
                                date_format(date_create($vEvent->fim), 'm')))
                                {{--*/ $twoDays=true /*--}} @else {{--*/ $twoDays=false /*--}}
                                @endif
                                <tr>
                                    <td class="border border-0 calendar-day-single text-light text-center px-2">
                                        <strong>{{date_format(date_create($vEvent->inicio), 'd')}}</strong></td>
                                </tr>

                                <tr>
                                    <td class="border border-0 calendar-day-single text-light text-center px-2"><strong
                                            @if (!$twoDays) style="visibility: hidden"
                                            @endif>{{date_format(date_create($vEvent->fim), 'd')}}</strong></td>
                                </tr>
                            </table>

                            <div class="text-uppercase mt-1 text-left pl-4 ml-3">
                                <div class="font-weight-bold pl-3">
                                    <small><span class="oi oi-map-marker text-info"></span></small>
                                    <small><b class="text-dark">&nbsp; {{$vEvent->City->name}} </b></small>
                                    <p class="text-truncate">
                                        <small><small style="color: black">{{$vEvent->local_evento}}</small></small>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            @endif {{-- Verifica se o evento tem foto de capa --}}
            @endforeach {{--Fim foreach Eventos de tema--}}

        </section>

    </div>
    @endif {{--Fim Verifica Eventos de tema--}}



</main>
@include('public.carnaval.menuMobile')
@endsection

@section('footer')
@include('public.carnaval.footer')
@endsection

@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsIndex.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsSearchbar.js')}}"></script>
@endsection