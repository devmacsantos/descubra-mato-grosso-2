@extends('public.templates.base') 
@section('pageCSS')


<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsFiltered.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/eventsSearchbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{url('/assets/fonts/pwchalk/stylesheet.css')}}">
@endsection
 
@section('header')
        @include('public.carnaval.header')
@endsection
 
@section('main-content')
<main class="container-fluid mt-4">

@if (isset($debug))
<!--             
      #Depuração
{{--var_dump($debug)--}}
-->        
@endif
        @if (isset($vEvents))

        <div class="py-5">
                        <div class="container">
                
                        @if ($vEvents->total() == 1)
                                <h5><b><span class="text-danger"> {{$vEvents->total()}}</span></b> evento encontrado</h5><small> Busca {{$textFilter}}</small>
                        @elseif($vEvents->total() > 1)
                                <h5><b><span class="text-danger"> {{$vEvents->total()}}</span></b> eventos encontrados</h5><small> Busca {{$textFilter}}</small>
                        @else
                                <h5>Nenhum evento encontrado </h5><small>Busca {{$textFilter}}</small>
                        @endif

                          <div class="row">
                        @foreach ($vEvents as $vEvent)

                        {{-- Verifica se o evento tem foto de capa --}}
                        @if ($vEvent->EventPhotos(true)->first() != null) 


                            <div class="col-md-4">
                              <div class="card mb-4 shadow">
                                        <a href="{{url($p_Language.'/carnaval-2019/'.$vEvent->Destination->slug.'/' .$vEvent->slug)}}">
                                                @if ($vEvent->EventPhotos(true)->first() != null)
                                                <img class="card-img-top fade-image" src="/image/350/196/true/true/{{$vEvent->EventPhotos(true)->first()->url}}">
                                                @endif
                                        </a>
                                <div class="card-body pb-4">

                                                <div class="border border-top-0 border-left-0 border-right-0 text-center text-uppercase text-truncate"><a class="link-no-hover title" href="{{url($p_Language.'/carnaval-2019/'.$vEvent->Destination->slug.'/' .$vEvent->slug)}}" title="{{$vEvent->name}}"
                                                        data-toggle="popover" data-trigger="hover" data-content="{{json_decode($vEvent->descricao_curta, true)[$p_Language]}}"
                                                        data-placement="top"><small>{{$vEvent->nome}}</small></a></div>


                                                <table class="float-left">
                                                                <tr>
                                                                <th class="border border-0 calendar-month-single text-light px-2 text-uppercase">{{getMonthName(date_format(date_create($vEvent->inicio),'m'))}}<sup><small>{{date_format(date_create($vEvent->inicio),'y')}}</small></sup></th>
                                                                </tr>
                                                                    @if (($vEvent->fim != '') && (date_format(date_create($vEvent->inicio), 'd') != date_format(date_create($vEvent->fim), 'd')) && (date_format(date_create($vEvent->inicio), 'm') == date_format(date_create($vEvent->fim), 'm')))
                                                                    {{--*/ $twoDays=true /*--}} @else {{--*/ $twoDays=false /*--}}
                                                                    @endif
                
                                                                <tr>
                                                                        <td class="border border-0 calendar-day-single text-light text-center px-2"> <strong>{{date_format(date_create($vEvent->inicio), 'd')}}</strong></td>
                                                                </tr>
                
                                                                <tr>
                                                                        <td class="border border-0 calendar-day-single text-light text-center px-2"><strong @if (!$twoDays) style="visibility: hidden" @endif>{{date_format(date_create($vEvent->fim), 'd')}}</strong></td>
                                                                </tr>
                                                </table>
                                                <div class="text-uppercase mt-1 text-left pl-4 ml-3">
                                                        <div class="font-weight-bold pl-4"><small><span class="oi oi-map-marker text-info"></span></small><small><b class="text-dark">&nbsp; {{$vEvent->City->name}} </b></small>
                                                                <p class="text-truncate"><small><small style="color: black">{{$vEvent->local_evento}}</small></small>
                                                                </p>
                                                        </div>
                                                </div>
                                </div>
                              </div>
                            </div>
                            @endif
                        @endforeach

                          </div>
                        </div>
        </div>       

<div class="container">
                <div class="col-md-12">
                                @if (isset($filter))
        
                                    {!! $vEvents->appends($filter)->render() !!}
                                    
                                @else
                                    {{$vEvents->render()}}
                                @endif
                </div>
</div>

        @endif 

</main>
        @include('public.carnaval.menuMobile')
@endsection
 
@section('footer')
        @include('public.carnaval.footer')
@endsection
 
@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsFiltered.js')}}"></script>
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/eventsSearchbar.js')}}"></script>
@endsection