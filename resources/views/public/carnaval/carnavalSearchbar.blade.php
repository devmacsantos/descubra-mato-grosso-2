<div class="container mt-2 pt-2">
<form  enctype="multipart/form-data" method="get" action="{{url($p_Language.'/carnaval-2019/filtro')}}">
        <!--
        <input type="hidden" name="category_id" value="@if (isset($filter) && isset($filter['category_id'])){{$filter['category_id']}}@endif">
        -->
        <input type="hidden" name="category_id" value="0">
                <div class="form-row">
                  <div class="form-group col-md-3">

                        <div class="input-group mb-1">
                                <input type="text" name="event" class="form-control" placeholder="{{trans('menu.im_looking_for', [],'messages', $p_Language)}}" value="@if (isset($filter) && isset($filter['event'])){{$filter['event']}}@endif">

                        </div>
 
                  </div>
                  <div class="form-group col-md-3">
                                <div class="input-group mb-1">
                                        <select name="city" class="custom-select select-city" id="inputGroupSelect01">
                                                <option value="0" @if (isset($filter) && isset($filter['city']) && ($filter['city'] == 0)){{' selected '}}@elseif(!isset($filter)){{' selected '}}@endif>Cidades com Carnaval</option>
                                                        
                                                @if(isset($cities))
                                                        @foreach ($cities as $city)
                                                                <option class='opts' value="{{$city['id']}}" @if (isset($filter) && isset($filter['city']) && ($filter['city'] == $city['id'])){{' selected '}}@endif>{{$city['name']}}</option>
                                                        @endforeach
                                                @endif
                                        </select>
                                        <!--
                                        <div class="input-group-append">
                                                        <span class="input-group-text bg-transparent"><span class="oi oi-map-marker"></span></span>
                                        </div>
                                        -->
                                </div>
                  </div>
                     <!--
                  <div class="form-group col-md-3">
                                <div class="input-group mb-3">
                                
                                               <input type="text" id="reportrange" name="date" autocomplete="off" value="@if (isset($filter) && isset($filter['date'])){{$filter['date']}}@endif" class="form-control">-->

                                                <input type="hidden" id="reportrange" name="date" autocomplete="off" value="01/02/2019 - 10/03/2019'" class="form-control">
                                                <!--
                                                <div class="input-group-append">
                                                        <span class="input-group-text bg-transparent border-left-0"><span class="oi oi-calendar"></span></span>
                                                </div>
                                               
                                </div>
                        
                  </div> -->

                  <div class="form-group col-md-2 mb-1">
                        <button id="SearchFormButton" class="btn btn-primary w-100" type="submit">{{trans('menu.search_in_site')}}</button>
                  </div>
                </div>
</form>
</div>
