
    @if (isset($v_InstagramPictures)){{-- Início Verifica Passagem de Imagens do Instagram --}}
    <div class="title container pt-3 mt-5">
        <div class="row">
            <div class="col-md-8">
                <h3 class="text-left display-5 title">
                    Siga nossas Redes Sociais <a href="https://instagram.com/visiteminasgerais" target="_blank">{{"@VisiteMinasGerais"}}</a>
                </h3>
            </div>
            <div class="col-md-4 text-right">
                <p style="margin-bottom:0px;">Use a hashtag <a href="https://www.instagram.com/explore/tags/turismomg/" target="_blank">#TurismoMG </a> em suas fotos e compartilhe a sua história com a gente!</p>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            {{--*/ $countPictures=0 /*--}}
            @foreach ($v_InstagramPictures as $pictures) {{-- --}}
            @if ($countPictures <= 8 ) {{-- Verifica se foi colocado 9 imagens --}} <div
                class="@if($countPictures >= 1 && $countPictures <= 8) col-6 col-md-6 @else col-md-4 @endif">
                <a class="" href="{{$pictures->link}}" target="_blank">
                    @if($countPictures >= 1 && $countPictures <= 8) <img class="img-fluid rounded m-1 fade-image"
                        src="{{url( '/image/180/180/true/true/' . $pictures->images->standard_resolution->url)}}">
                        @else
                        <img class="img-fluid rounded m-1 fade-image"
                            src="{{url( '/image/360/360/true/true/' . $pictures->images->standard_resolution->url)}}">
                        @endif


                </a>
        </div>

        <!--
                                        {{$countPictures}}
                                -->

        @if ($countPictures == 0 )
        <div class="col-12 col-md-4 pt-2">
            <div class="row">
                @endif

                @if ($countPictures == 4 )
            </div>
        </div>
        <div class="d-none d-md-block col-md-4 pt-2">
            <div class="row">
                @endif

                @if ($countPictures == 8 )
            </div>
        </div>

        @endif


        @endif {{--Fim Verifica se foi colocado 11 imagens --}}
        {{--*/ $countPictures++ /*--}}
        @endforeach

    </div>
    <div class="text-right">
        <ul class="navbar-nav flex-row small justify-content-end">
            <li class="nav-item">
                <a class="nav-link px-1" href="https://instagram.com/visiteminasgerais" target="_blank">
                    <img src="{{url('portal/assets/libs/imgs/rede_sociais/ínstragran.png')}}" alt="Instagram"
                        title="Instagram" style="width: 32px; height: 32px;">
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link px-1" href="https://www.facebook.com/VisiteMinasGerais/" target="_blank">
                    <img src="{{url('portal/assets/libs/imgs/rede_sociais/face.png')}}" alt="Facebook" title="Facebook"
                        style="width: 32px; height: 32px;">
                </a>
            </li>
        </ul>
    </div>
    <hr>
    </div>

    @endif {{-- Fim Verifica Passagem de Imagens do Instagram --}}




<div class="col-xs-1" align="center" >
<a href="{{url('/pt/')}}" style="align-self:center;">
<img  src="{{url('/assets/img/rodapecarnaval.png')}}" > 

</a> 
</div>
