<?php  $today = date("Y-m-d H:i:s");  $date = get_limite_data_especial();  ?>

@extends('public.templates.base') 
@section('pageCSS')
<link rel="stylesheet" type="text/css" href="{{url('/portal/assets/libs/portal/css/homeIndex.css')}}">

@endsection
 
@section('header')
        @include('public.templates.header')
@endsection
 
@section('main-content')

<!-- resultados da busca -->

<div class="row-fluid" id="destinos" >
    <div class="container">
                <div class="col-lg-12 text-center" style="margin-top:30px; margin-bottom:50px;">
                    @if($p_Search)
                        <h2 style="font-size:2em;font-family: Signika !important;"> {{count($destinos)+count($atracoes)+count($eventos)+count($blogs)}} {{trans('search.search_results_for')}}</h2>
                        <p class="text-danger" style="font-size:2em;font-family: Signika !important;">" {{$p_SearchString}} "</p>
                    @endif
                </div>


<!-- Destinos -->

<style>
.coluna{
    width:170px;
    margin-left: 20px;
    margin-bottom: 20px;
    /*font-family: Arial, Helvetica, sans-serif!important; */
    font-family: Signika !important;

}

.detalhes {
    height: 110px;
    width: 100%;
}

.detalhes-destino {
    height: 60px;
    width: 100%;
    display: -webkit-flex;
    display: flex;
    align-items: center;
    justify-content: center;
}
.destino {
    /* background-color: #009933; */  

}
.evento {
    background-color: #Ff0000;
    background-color: white;
    padding-top:5px;
    .l1{
        color:#Ff0000;
    }

}
.atracao {
    /*background-color: #00AFEF;*/
    padding-top:5px;
}

.servico {
    /* background-color: #Ff6600; */
    padding-top:5px;
}
.blog {
    /* background-color: #96989A; */
    padding-top:10px;
}

.l0 {
    font-size: 14px;
    font-weight: bold;
    color:#009933; 
    display: -webkit-flex;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top:5px;
    padding-left:5px;
    padding-right:5px;
}

.l1 {
    font-size: 10px;
    font-weight: bold;
    height: 60px;
    display: -webkit-flex;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left:5px;
    padding-right:5px;
    color:#Ff0000;
}

.l1-atracao {
    font-size: 10px;
    font-weight: bold;
    height: 60px;
    display: -webkit-flex;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left:5px;
    padding-right:5px;
    color:#00AFEF;
}

.l1-servico {
    font-size: 10px;
    font-weight: bold;
    height: 60px;
    display: -webkit-flex;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left:5px;
    padding-right:5px;
    color: #Ff6600;
}

.l1-blog {
    font-size: 10px;
    font-weight: bold;
    height: 60px;
    display: -webkit-flex;
    display: flex;
    align-items: center;
    justify-content: center;
    padding-left:5px;
    padding-right:5px;
    color: black;
}

.l2 {
    font-size: 9px;
    font-weight: bold;
    height: 15px;
    color: gray;
}
.l3 {
    font-size: 9px;
    font-weight: 600;
    height: 15px;
    color: gray;
    padding-left:5px;
    padding-right:5px;
}
.l4 {
    font-size: 8px;
    font-weight: 600;
    height: 25px;
    color: gray;
    padding-left:5px;
    padding-right:5px;
}


</style>    

<div class="row">
@if(count($destinos))

    @foreach($destinos as $destino)
        <?php $v_Description = json_decode($destino->descricao_curta,1)[$p_Language]; ?>
        <div class="coluna card shadow grow" >
            <a class="link-no-hover"  href="{{url($p_Language . '/destinos/' . $destino->slug)}}">
                <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                    <img class="img-fluid fade-image rounded-top rounded-bottom"  src="/image/170/150/true/true/{{$destino->url}}" alt="{{$destino->nome}}">
                    </div>
                </div>
                <div class="detalhes-destino text-white destino" style="vertical-align: middle;">
                    <p class="text-uppercase mt-2 l0 text-center" ><span class="oi oi-map-marker ml-2"></span>&nbsp {{$destino->nome}}</p>                     
                </div>   
            </a>
        </div>
        @endforeach

@endif



 <!-- eventos -->            
 @if(count($eventos))

    @foreach($eventos as $evento)
        <?php
            $v_Description = json_decode($evento->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $evento->nome;
            //print_r($v_Name);exit();    
        ?>
        <div class="coluna card shadow grow" >
        <a class="link-no-hover"  href="{{url($p_Language . '/eventos/' . $evento->c_slug . '/' . $evento->slug)}}">
                <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                    <img class="img-fluid fade-image rounded-top rounded-bottom"  src="/image/170/100/true/true/{{$evento->url}}" alt="{{$evento->nome}}">
                    </div>
                </div>
                <div class="detalhes evento text-center" >

                    <div class="text-uppercase l1">{{$evento->nome}}</div>   
                    <div class="text-uppercase l2"><span class="oi oi-map-marker"></span> {{$evento->cidade}}</div>   
                    <div class="text-uppercase l3">{{getShortDate($evento->inicio, $evento->termino)}}</div>   
                    <div class="text-uppercase l4">{{encurta($evento->local_evento)}} </div>              
                                
                </div>   
            </a>
        </div>

    @endforeach

@endif


 <!-- atrações -->            
@if(count($atracoes))

    @foreach($atracoes as $atracao)
        <?php
            $v_Description = json_decode($atracao->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $atracao->nome_pt;
            //print_r($v_Name);exit();    
        ?>
        <div class="coluna card shadow grow " >
        @if(in_array($atracao->item_inventario,['C1','C2','C3','C4']))   
            <a class="link-no-hover"  href="{{url($p_Language . '/atracoes/' . $atracao->c_slug .'/'. $atracao->type_slug .'/'.  $atracao->slug)}}">
        @elseif(in_array($atracao->item_inventario,['B1','B2','B3','B4','B5','B6']))
            <a class="link-no-hover"  href="{{url($p_Language . '/apoio/' . $atracao->c_slug .'/'. $atracao->type_slug .'/'.  $atracao->slug)}}">
        @endif

        <div class="hoverzoom">
            <div class="thumbs-mini-four">
            <img class="img-fluid fade-image rounded-top rounded-bottom"  src="/image/170/100/true/true/{{$atracao->url}}" alt="{{$atracao->nome_pt}}">
            </div>
        </div>

        @if(in_array($atracao->item_inventario,['C1','C2','C3','C4']))   
            <div class="detalhes atracao text-center text-white" >
            <div class="text-uppercase l1-atracao">{{$atracao->nome_pt}}</div>       
        @elseif(in_array($atracao->item_inventario,['B1','B2','B3','B4','B5','B6']))
            <div class="detalhes servico text-center text-white" >
            <div class="text-uppercase l1-servico">{{$atracao->nome_pt}}</div>       
        @endif
                    
                    <div class="text-uppercase l2"><span class="oi oi-map-marker"></span> {{$atracao->cidade}}</div>   
                    <div class="text-uppercase mt-2 l4">{{$atracao->slug_trade}}</div>                      
                </div>   
            </a>
        </div>
    @endforeach

@endif

 <!-- atrações -->            
 @if(count($blogs))

@foreach($blogs as $blog)
    <div class="coluna card shadow grow" >
        <a class="link-no-hover" href="{{url($p_Language . '/blog/artigo/' . $blog->slug)}}">
    <div class="hoverzoom">
        <div class="thumbs-mini-four">
        <img class="img-fluid fade-image rounded-top rounded-bottom" src="/image/170/100/true/true/{{$blog->foto_capa_url}}" alt="{{$blog->titulo_pt}}">
        </div>
    </div>

    <div class="detalhes blog text-center text-white" >
            <div class="text-uppercase l1-blog">{{$blog->titulo_pt}}</div>   
            <div class="text-uppercase l2 mt-3">BLOG</div>                        
         </div>   
        </a>
    </div>
@endforeach

@endif


</div>
<!-- mais 
@if($mais_atracoes)
<h3 class="text-center" style="margin-bottom:2em;font-size:2em">Outras atrações no municipio </h3>
<div class="col-lg-12 line">
@foreach($mais_atracoes as $atracao)
        <?php
            $v_Description = json_decode($atracao->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $atracao->nome_pt;
            //print_r($v_Name);exit();    
        ?>
        <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12" style="height:280px;">
        @if(in_array($atracao->item_inventario,['C1','C2','C3','C4']))   
            <a  href="{{url($p_Language . '/atracoes/' . $atracao->c_slug .'/'.  $atracao->slug)}}">
        @elseif(in_array($atracao->item_inventario,['B1','B2','B3','B4','B5','B6']))
            <a style='color:red;'  href="{{url($p_Language . '/apoio/' . $atracao->c_slug .'/'. $atracao->slug)}}">
        @endif
                <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <img src="{{$atracao->url}}">
                    </div>
                </div>
           
            <div class="details-hotel">
            
            <p style="text-align:center;">{{$tipo[$atracao->type_id]}} </p>
            <h4 style='color:red;' >{{$atracao->nome_pt}}</h4>
            <p style="text-align:center;">{{$atracao->cidade}}</p>
            </div>
            </a>
        </div>
    @endforeach
</div>
@endif
-->
<!--fim outras atrações -->

<!--outros eventos
@if($mais_eventos)
<h3 class="text-center" style="margin-bottom:2em;font-size:2em">Outros eventos no municipio </h3>
<div class="col-lg-12 line">

@foreach($mais_eventos as $evento)
        <?php
            $v_Description = json_decode($evento->descricao_curta,1)[$p_Language];
            //$v_Description = json_encode($v_Description);
            $v_Name = $evento->nome;
            //print_r($v_Name);exit();    
        ?>
        <div class="col-3 col-lg-3 col-md-6 col-sm-12 col-xs-12" style="height:280px;">
            
            <a href="{{url($p_Language . '/eventos/' . $evento->c_slug . '/' . $evento->slug)}}">
                                        <div class="hoverzoom">
                    <div class="thumbs-mini-four">
                        <img src="{{$evento->url}}">
                    </div>
                </div>
            
            <div class="details-hotel">
                <h4 style='color:red;' >{{$evento->nome}} </h4>
                <p style="text-align:center;">{{getLiteralDate($evento->inicio, $evento->termino)}}</p>
                <p style="text-align:center;">{{$evento->cidade}}</p>
            </div>
            </a>
        </div>
    @endforeach
</div>
@endif
</div>
 -->
<!-- fim outros eventos -->

</div>


</main>
        @include('public.templates.menuMobile')
@endsection
 
@section('footer')
        @include('public.templates.footer')
@endsection
 
@section('pageScript')
<script type="text/javascript" src="{{url('/portal/assets/libs/portal/js/homeIndex.js')}}"></script>
@endsection
