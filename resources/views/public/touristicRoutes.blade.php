@extends('public.base')
@section('pageCSS')
@stop

@section('main-content')
    <div class="row-fluid margin-titlemenu" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2 class="laranja">{{trans('menu.routes')}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
       
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12">
                        <p>{!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}</p>
                    </div>
                </div>
            </div>
     
            <div class="col-lg-12 line">
                @foreach($p_TouristicRoutes as $c_Index => $c_TouristicRoute)
                    @if($c_Index < 2)
                        <?php
                            $v_RouteName = json_decode($c_TouristicRoute->nome,1)[$p_Language];
                            $v_RouteDescription = json_decode($c_TouristicRoute->descricao_curta,1)[$p_Language];
                        ?>
                        <div class="col-lg-6 col-md-6 list-thumbs-full">
                            <a href="{{url($p_Language . '/roteiros/' . $c_TouristicRoute->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-full">
                                        <img src="{{$c_TouristicRoute->url}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_RouteName}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_RouteDescription}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_RouteName}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="col-lg-12 line">
                @foreach($p_TouristicRoutes as $c_Index => $c_TouristicRoute)
                    @if($c_Index >= 2 && $c_Index < 5)
                        <?php
                            $v_RouteName = json_decode($c_TouristicRoute->nome,1)[$p_Language];
                            $v_RouteDescription = json_decode($c_TouristicRoute->descricao_curta,1)[$p_Language];
                        ?>
                        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <a href="{{url($p_Language . '/roteiros/' . $c_TouristicRoute->slug)}}">
                                <div class="hoverzoom">
                                    <div class="thumbs-mini-three">
                                        <img src="{{$c_TouristicRoute->url}}">
                                    </div>
                                    <div class="retina-hover">
                                        <div class="col-lg-12 title">
                                            <p>{{$v_RouteName}}</p>
                                        </div>
                                        <div class="col-lg-12 no-padding">
                                            <hr>
                                        </div>
                                        <div class="col-lg-12 text">
                                            <p>{{$v_RouteDescription}}</p>
                                        </div>
                                    </div>
                                    <div class="retina">
                                        <p>{{$v_RouteName}}</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="col-lg-12" style="padding:2% 0;">
                <div class="col-lg-12 hospedagem">
                    <h2 style="color:#b4b4b4;">
                        {{trans('touristicRoutes.find_perfect_route')}}
                    </h2>
                    <p style="color:#4b4b4b;font-size:16px;font-family: Signika;">
                        {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                    </p>
                </div>
            </div>
        </div>

        {!! Form::open(array('onsubmit' => 'return submitFilter()')) !!}
            <div class="col-lg-12" id="tarja">
                <div class="container">
                    <div class="col-lg-12">
                        <div class="row" id="options">
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-2">
                                <div class="col-lg-6 col-md-3 col-sm-3 col-xs-12" id="inputDestino">
                                    <p style="font-size: 20px;margin-top: 5px;">{{trans('touristicRoutes.route_how_many_days')}}</p>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 selectoptions">
                                    {!! Form::select('duracao', [''=>trans('touristicRoutes.choose_duration'), 1 => trans('touristicRoutes.duration_1'), 2 => trans('touristicRoutes.duration_2'), 3 => trans('touristicRoutes.duration_3')], null, ['id' => 'duracao']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="tarja_intern">
                <div class="container">
                    <div class="col-lg-12">
                        <div class="row" id="options">
                            <div class="col-lg-3 no-padding">
                                <p style="font-size:20px;">{{trans('whereToGo.what_do_you_expect')}}</p>
                            </div>
                            <div class="col-lg-3 selectoptions no-padding">
                                <?php
                                    $v_CategoriesArray = [];
                                    foreach($p_TripCategories as $c_CategoryIndex => $c_Value)
                                        $v_CategoriesArray += [$c_CategoryIndex => json_decode($c_Value,1)[$p_Language]];
                                ?>
                                {!! Form::select('categoria_viagem', [''=>trans('whereToGo.choose_trip_category')] + $v_CategoriesArray, null, ['id' => 'categoriaViagem']) !!}
                            </div>

                            <div class="col-lg-3 selectoptions">
                                {!! Form::select('tipo_viagem', [''=>trans('whereToGo.choose_trip_type')], null, ['id' => 'tipoViagem']) !!}
                            </div>
                            <div class="col-lg-3 selectoptions no-padding" id="submit-tarja">
                                <input class="btn" type="submit" value="{{trans('whereToGo.search')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {!! Form::close() !!}

        <div class="col-lg-12"  style="padding:2rem 0;">
            <div class="container">
                <div class="col-lg-12 line" style="padding:0;" id="filterResultsDiv">
                    @foreach($p_TouristicRoutes as $c_Index => $c_TouristicRoute)
                        @if($c_Index >= 5)
                            <?php
                                $v_RouteName = json_decode($c_TouristicRoute->nome,1)[$p_Language];
                                $v_RouteDescription = json_decode($c_TouristicRoute->descricao_curta,1)[$p_Language];
                            ?>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                <a href="{{url($p_Language . '/roteiros/' . $c_TouristicRoute->slug)}}">
                                    <div class="hoverzoom">
                                        <div class="thumbs-mini-four">
                                            <img src="{{$c_TouristicRoute->url}}">
                                        </div>
                                        <div class="retina-hover">
                                            <div class="col-lg-12 title">
                                                <p>{{$v_RouteName}}</p>
                                            </div>
                                            <div class="col-lg-12 no-padding">
                                                <hr>
                                            </div>
                                            <div class="col-lg-12 text">
                                                <p>{{$v_RouteDescription}}</p>
                                            </div>
                                        </div>
                                        <div class="retina">
                                            <p>{{$v_RouteName}}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop

@section('pageScript')
<script>
    var v_Types = {!! json_encode($p_TripTypes) !!};
    $(document).ready(function()
    {
        $('#categoriaViagem').change(function(){
            var v_SelectedCategoryId = $(this).val();
            var v_DataString = '';
            $.each(v_Types, function (c_Key, c_Field)
            {
                if(c_Field.trip_category_id == v_SelectedCategoryId)
                    v_DataString += '<option value="' + c_Field.id + '">' + JSON.parse(c_Field.nome).{{$p_Language}} + '</option>';
            });
            $('#tipoViagem').html(v_DataString);
        }).trigger('change');
    });

    function submitFilter()
    {
        var v_Data = {
            trip_type_id: $('#tipoViagem').val(),
            duracao: $('#duracao').val()
        };
        $.get("{{url($p_Language . '/filtro-roteiros')}}",v_Data).done(function(p_Data){
            if (p_Data.error == 'ok')
            {
                var v_DataString = '';

                if(p_Data.data.length < 1){
                    v_DataString = '<div class="col-xs-12" id="no-results">' +
                            '<p>' + "{{trans('destination.no_results')}}" + '</p>' +
                            '</div>';
                }

                $.each(p_Data.data, function (c_Key, c_Field)
                {
                    var v_Nome = JSON.parse(c_Field.nome).{{$p_Language}};
                    var v_Descricao = JSON.parse(c_Field.descricao_curta).{{$p_Language}};
                    v_DataString += '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">'+
                        '<a href="{{url($p_Language . '/roteiros/')}}/' + c_Field.slug + '">'+
                        '<div class="hoverzoom">'+
                        '<div class="thumbs-mini-four">'+
                        '<img src="' + c_Field.url + '">'+
                        '</div>'+
                        '<div class="retina-hover">'+
                        '<div class="col-lg-12 title">'+
                            '<p>' + v_Nome + '</p>' +
                        '</div>'+
                        '<div class="col-lg-12 no-padding">'+
                        '<hr>'+
                        '</div>'+
                        '<div class="col-lg-12 text">'+
                        '<p>' + v_Descricao + '</p>'+
                        '</div>'+
                        '</div>'+
                        '<div class="retina">'+
                            '<p>' + v_Nome + '</p>' +
                        '</div>'+
                        '</div>'+
                        '</a>'+
                        '</div>';
                });
                $('#filterResultsDiv').html(v_DataString);

            }
            else
                showNotify('danger', p_Data.error);
        }).error(function(){
        });
        return false;
    }
</script>
@stop