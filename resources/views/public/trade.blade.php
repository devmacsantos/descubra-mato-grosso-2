@extends('public.base')
@section('pageCSS')
    <link href="{{url('/vendor/lightbox/css/lightbox.css')}}" rel="stylesheet">
    <style>
        #exTab1 .tab-content {
            color : white;
            background-color: #428bca;
            padding : 5px 15px;
        }
        #exTab1 .nav-pills > li > a {
            border-radius: 0;
        }
    </style>
@stop

@section('main-content')
    <?php
        $v_Name = $p_Content->nome;
        $v_ShortDescription = json_decode($p_Content->descricao_curta,1)[$p_Language];
        $v_Description = json_decode($p_Content->descricao,1)[$p_Language];
    ?>
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="route">
                <h2>{{$p_Content == null ? '' : $v_Name}}</h2>
                <p style="margin-bottom:25px;">
                    <img src="{{url('/portal/assets/imgs/geotag-white.png')}}" alt="" style="margin-right:10px;margin-top: -8px;">
                    {{$p_Content == null ? '' : $p_Content->cidade}}
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <div class="col-lg-8 col-lg-offset-2">
                <div class="bs-docs-example">

                    <div id="myTabContent" class="tab-content">

                        <div class="tab-pane fade in active" id="home">
                            <img src="{{$p_Content->url}}" style="width: 100%;">
                        </div>
                        @foreach($p_PhotoGallery as $c_PhotoIndex => $c_Photo)
                            <div class="tab-pane fade" id="profile{{$c_PhotoIndex}}">
                                <img src="{{$c_Photo->url}}" style="width: 100%;">
                            </div>
                        @endforeach
                    </div>


                    <ul id="myTabs" class="nav nav-tabs" style="margin-top:2rem;border:none;">
                        <li style="float:left;margin-right:15px;">
                            <a href="#home" data-toggle="tab">
                                <img src="{{$p_Content->url}}" style="background-size:cover;width:70px;height:70px;">
                            </a>
                        </li>
                        @foreach($p_PhotoGallery as $c_PhotoIndex => $c_Photo)
                        <li style="float:left;margin-right:15px;">
                            <a href="#profile{{$c_PhotoIndex}}" data-toggle="tab">
                                <img src="{{$c_Photo->url}}" style="background-size:cover;width:70px;height:70px;">
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos">
        <div class="container" style="margin-top:1rem;">
            <div class="col-lg-12" style="margin:3rem 0 3rem 0;">
                <div class="col-lg-8 col-lg-offset-2">
                    <p>{!! $p_Content == null ? '' : nl2br($v_Description) !!}</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="localization" style="display:block;">
        <div class="container">
            <div class="content-address" style="margin-bottom: 3%;">
                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4" id="contactArea" style="padding:2% 0;border-top:1px solid #dadada;border-bottom:1px solid #dadada;">
                    <?php $v_HasContact = false; ?>
                    @if(!empty($p_Content->telefone))
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p>Tel.: {{$p_Content->telefone}}</p>
                        </div>
                    @endif
                     @if(strlen($p_Content->whatsapp) > 0)
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p>WhatsApp.: {{$p_Content->whatsapp}}</p>
                        </div>
                    @endif
                   
                    @if(!empty($p_Content->site))
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p><a target="_blank" href="{{$p_Content->site}}">{{$p_Content->site}}</a></p>
                        </div>
                    @endif
                    @if(!empty($p_Content->site))
                        <?php $v_HasContact = true; ?>
                        <div class="col-lg-12" style="margin:0.2rem 0;">
                            <p><a href="mailto:{{$p_Content->email}}">{{$p_Content->email}}</a></p>
                        </div>
                    @endif
                    <div class="col-lg-12" style="display:flex;margin:0.5rem 0;">
                        <ul class="canais">
                            @if(!empty($p_Content->facebook))
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->facebook}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-fb-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(!empty($p_Content->instagram))
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->instagram}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-insta-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(!empty($p_Content->twitter))
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->twitter}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-tw-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(!empty($p_Content->youtube))
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->youtube}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-youtube-gray.png')}}" alt=""></a></li>
                            @endif
                            @if(!empty($p_Content->flickr))
                                <?php $v_HasContact = true; ?>
                                <li style="float: left;margin-right: 5px;"><a href="{{$p_Content->flickr}}" target="_blank"><img src="{{url('/portal/assets/imgs/icon-flicker-gray.png')}}" alt=""></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
                @if(!$v_HasContact)
                    <style>
                        #contactArea{
                            display:none!important;
                        }
                    </style>
                @endif

                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4" style="padding:2% 0;">
                    <p>{{$p_Content->logradouro . ', ' . $p_Content->numero . (empty($p_Content->complemento) ? '' : (', ' . $p_Content->complemento)) . ' - ' . $p_Content->bairro}}</p>
                    <p>CEP {{$p_Content->cep . ' - ' . $p_Content->cidade}} - MG</p>
                </div>
            </div>
        </div>
    </div>

    @if($p_Content != null && $p_Content->latitude != null && $p_Content->longitude != null)
        <div class="row-fluid" id="map_canvas"></div>
    @endif

    <div class="row-fluid" id="useful-information">

        <div class="container">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <h2><img src="{{url('/portal/assets/imgs/icon-4.png')}}" alt="" style="margin-right:15px;">{{trans('attraction.useful_information')}}</h2>

                @if($p_Content->equipamento_fechado)
                    <?php
                        $v_EquipmentClosingReasons = [
                            'Fechado para reforma/obras' => trans('attraction.place_closed_works'),
                            'Interditado por órgão regulador' => trans('attraction.place_closed_restricted'),
                            'Conjuntura econômica' => trans('attraction.place_closed_economic_conjuncture'),
                            'Problemas de manutenção' => trans('attraction.place_closed_maintenance'),
                            'Fenômeno meteorológico' => trans('attraction.place_closed_meteorological_phenomena'),
                            'Fenômeno climatólogico' => trans('attraction.place_closed_climatic_phenomena'),
                            'Infraestrutura de acesso danificada' => trans('attraction.place_closed_access_infrastructure_damaged'),
                            'Outros' => trans('attraction.place_closed_other')
                        ];
                    ?>
                    <div class="alert alert-danger" role="alert">
                      <h4><strong>{{trans('attraction.place_closed')}}</strong></h4>
                      <p>{{trans('attraction.place_closed_reason') . $v_EquipmentClosingReasons[$p_Content->equipamento_fechado_motivo]}}</p>
                    </div>
                @endif
            </div>


            <div class="cards col-lg-12"> 
            @if(!empty($p_Content->localizacao))
                <?php
                    $v_Locations = [
                        'Urbana'=>trans('attraction.location_urban'),
                        'Rururbana'=>trans('attraction.location_rururban'),
                        'Rural'=>trans('attraction.location_rural')
                    ];
                ?>
                <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.location')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$v_Locations[$p_Content->localizacao]}}</p>
                        </div>
                    </div>
                </div>
            @endif

            <?php
                $v_Items = [
                    'descricao_arredores_distancia_pontos' => 'surroundings_description',
                    'ponto_referencia' => 'reference_points'
                ];
            ?>
            @foreach($v_Items as $c_Key => $c_TranslationKey)
                @if(!empty($p_Content[$c_Key]))
                    <?php $v_Text = json_decode($p_Content[$c_Key],1)[$p_Language]; ?>
                    @if(!empty($v_Text))
                    <div class="col-lg-4" id="services">
                        <div class="bg-service">
                            <div class="col-lg-12 name-service">
                                <h4>{{trans('attraction.' . $c_TranslationKey)}}</h4>
                            </div>
                            <div class="col-lg-12 resume-service">
                                <p>{!! nl2br($v_Text) !!}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                @endif
            @endforeach

            <div class="col-lg-4 funcionamento_1" id="services">
                <div class="bg-service">
                    <div class="col-lg-12 name-service">
                        <h4>{{trans('attraction.functioning_period')}}</h4>
                    </div>
                    <div class="col-lg-12 resume-service">
                        <input type="hidden" id="funcionamento_1" value="{{$p_Content->funcionamento_1}}">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 funcionamento_2" id="services">
                <div class="bg-service">
                    <div class="col-lg-12 name-service">
                        <h4>{{trans('attraction.functioning_period_2')}}</h4>
                    </div>
                    <div class="col-lg-12 resume-service">
                        <input type="hidden" id="funcionamento_2" value="{{$p_Content->funcionamento_2}}">
                    </div>
                </div>
            </div>
            @if(!empty($p_Content->funcionamento_observacao))
                <?php $v_Text = json_decode($p_Content->funcionamento_observacao,1)[$p_Language]; ?>
                @if(!empty($v_Text))
                <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.functioning_period_observation')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{!! nl2br($v_Text) !!}</p>
                        </div>
                    </div>
                </div>
                @endif
            @endif

            @if(!empty($p_Content->visita_tipo))
                <?php
                    $v_VisitationTypes = [
                        'Não guiada' => trans('attraction.visitation_type_not_guided'),
                        'Auto-guiada' => trans('attraction.visitation_type_self_guided'),
                        'Guiada' => trans('attraction.visitation_type_guided')
                    ];
                    $v_VisitationTypesArray = explode(';', $p_Content->visita_tipo);
                    $v_VisitationTypesText = '';
                    foreach($v_VisitationTypesArray as $c_Index => $c_Type){
                        if(!empty($c_Type))
                            $v_VisitationTypesText .= ($c_Index > 0 ? ', ' : '') . $v_VisitationTypes[$c_Type];
                    }
                ?>
                <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.visitation_type')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$v_VisitationTypesText}}</p>
                        </div>
                    </div>
                </div>
            @endif

            @if(!empty($p_Content->entrada_tipo))
                <?php
                    $v_EntranceTypes = [
                        'Franca' => trans('attraction.entrance_type_free'),
                        'Paga' => trans('attraction.entrance_type_paid')
                    ];
                ?>
                <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.entrance_type')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$v_EntranceTypes[$p_Content->entrada_tipo]}}</p>
                            @if(!empty($p_Content->entrada_valor))
                                <p>{{trans('attraction.value')}}: R${{$p_Content->entrada_valor}}</p>
                            @endif
                        </div>

                    @if(!empty($p_Content->formas_pagamento))
                        <?php $v_Items = json_decode($p_Content->formas_pagamento,1); ?>
                        @if(!empty($v_Items))
                            <div class="col-lg-12 name-service">
                                <h4>{{trans('attraction.payment_methods')}}</h4>
                            </div>
                            <div class="col-lg-12 resume-service">
                                <p>
                                    @foreach($v_Items as $c_Index => $c_Item)
                                        {{($c_Index == 0 ? '' : ', ') . $p_PaymentMethods[$c_Item['id']]}}
                                    @endforeach
                                </p>
                            </div>

                        @endif
                    @endif
                    </div>
                </div>
            @endif

            @if(!empty($p_Content->atividades_realizadas))
                <?php $v_Text = json_decode($p_Content->atividades_realizadas,1)[$p_Language]; ?>
                @if(!empty($v_Text))
                <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.activities')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{!! nl2br($v_Text) !!}</p>
                        </div>
                    </div>
                </div>
                @endif
            @endif

            @if($p_Content->necessaria_autorizacao_previa == 1)
                <?php
                    $v_Text = empty($p_Content->autorizacao_previa_tipo) ? '' : json_decode($p_Content->autorizacao_previa_tipo,1)[$p_Language];
                ?>
                @if(!empty($v_Text))
                <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.requires_authorization')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{!! nl2br($v_Text) !!}</p>
                        </div>
                    </div>
                </div>
                @endif
            @endif

            @if(!empty($p_Content->formas_pagamento))
                <?php $v_Items = json_decode($p_Content->formas_pagamento,1); ?>
                @if(!empty($v_Items))
                <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.payment_methods')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>
                                @foreach($v_Items as $c_Index => $c_Item)
                                    {{($c_Index == 0 ? '' : ', ') . $p_PaymentMethods[$c_Item['id']]}}
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
                @endif
            @endif

            @if(!empty($p_Content->observacoes_complementares))
                <?php $v_Text = json_decode($p_Content->observacoes_complementares,1)[$p_Language]; ?>
                @if(!empty($v_Text))
                    <div class="col-lg-4" id="services">
                        <div class="bg-service">
                            <div class="col-lg-12 name-service">
                                <h4>{{trans('attraction.complementary_observations')}}</h4>
                            </div>
                            <div class="col-lg-12 resume-service">
                                <p>{!! nl2br($v_Text) !!}</p>
                            </div>
                        </div>
                    </div>
                @endif
            @endif

            <?php
                $v_Items = [
                    'aceita_animais' => 'accepts_animals',
                    'gay_friendly' => 'gay_friendly',
                    'nao_aceita_criancas' => 'doesnt_accept_kids'
                ];
            ?>
            @foreach($v_Items as $c_Key => $c_TranslationKey)
                @if($p_Content[$c_Key] == 1)
                    <div class="col-lg-4" id="services">
                        <div class="bg-service">
                            <div class="col-lg-12 name-service">
                                 <h4>{{trans('attraction.' . $c_TranslationKey)}}</h4>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach

            @if(!empty($p_Content->unidades_habitacionais_facilidades) || !empty($p_Content->unidades_habitacionais_voltagem))
                <?php
                    $v_Items = json_decode($p_Content->unidades_habitacionais_facilidades,1);
                    $v_Voltages = json_decode($p_Content->unidades_habitacionais_voltagem,1);
                    $v_Facilities = '';
                    foreach($v_Items as $c_Item)
                    {
                        if($c_Item['valor'])
                        {
                            if($c_Item['nome'] == 'tv_assinatura')
                                $v_Facilities .= (empty($v_Facilities) ? '' : ', ') . trans('attraction.cable_tv');
                            if($c_Item['nome'] == 'internet')
                                $v_Facilities .= (empty($v_Facilities) ? '' : ', ') . 'Internet';
                        }
                    }
                ?>
                @if(!empty($v_Facilities) || !empty($v_Voltages))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.facilities')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            @if(!empty($v_Facilities))
                                <p>{{$v_Facilities}}</p>
                            @endif
                            @if(!empty($v_Voltages))
                                <p>{{trans('attraction.voltage')}}:
                                    @foreach($v_Voltages as $c_Index => $c_Item)
                                        {{($c_Index == 0 ? '' : ', ') . $c_Item['nome']}}V
                                    @endforeach
                                </p>
                            @endif
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            <?php
                $v_Text = empty($p_Content->tipo_diaria_outros) ? '' : json_decode($p_Content->tipo_diaria_outros,1)[$p_Language];
            ?>
            @if(!empty($p_Content->tipo_diaria) || !empty($v_Text))
                <?php
                    $v_DailyRateTypes = [
                        'Sem café' => trans('attraction.daily_rate_type_no_breakfest'),
                        'Com café' => trans('attraction.daily_rate_type_breakfest'),
                        'Meia pensão' => trans('attraction.daily_rate_type_half_pension'),
                        'Pensão completa' => trans('attraction.daily_rate_type_full_pension')
                    ];
                ?>
                <div class="col-lg-4" id="services">
                <div class="bg-service">
                    <div class="col-lg-12 name-service">
                        <h4>{{trans('attraction.daily_rate_type')}}</h4>
                    </div>
                    <div class="col-lg-12 resume-service">
                        @if(!empty($p_Content->tipo_diaria))
                            <?php
                                $v_TiposDiariaJSON = json_decode($p_Content->tipo_diaria,1);
                                $v_TiposDiaria = '';
                                foreach($v_TiposDiariaJSON as $c_Index => $c_TipoDiaria)
                                    $v_TiposDiaria .= ($c_Index > 0 ? ', ' : '') . $v_DailyRateTypes[$c_TipoDiaria];
                            ?>
                            @if(!empty($v_TiposDiaria))
                            <p>{{$v_TiposDiaria}}</p>
                            @endif
                        @endif
                        @if(!empty($v_Text))
                            <p>{!! nl2br($v_Text) !!}</p>
                        @endif
                    </div>
                    </div>
                </div>
            @endif

            @if(!empty($p_Content->servicos_equipamentos_recreacao_lazer))
                <?php
                    $v_ItemNames = [
                        'Piscina' => trans('attraction.pool'),
                        'Piscina aquecida' => trans('attraction.heated_pool'),
                        'Sauna seca' => trans('attraction.dry_sauna'),
                        'Sauna a vapor' => trans('attraction.sauna'),
                        'Quadra de esportes' => trans('attraction.court'),
                        'Playground' => trans('attraction.playground'),
                        'Hidromassagem' => trans('attraction.hottub'),
                        'Spa' => trans('attraction.spa'),
                        'Day Use' => trans('attraction.day_use'),
                        'Guia turístico' => trans('attraction.touristic_guide'),
                        'Guia de pesca' => trans('attraction.fishing_guide'),
                        'Monitor de recreação/passeios' => trans('attraction.recreation_monitor'),
                        'Sala de jogos' => trans('attraction.game_room'),
                        'Animais para montaria' => trans('attraction.mounting_animals'),
                        'Campo de futebol' => trans('attraction.soccer_court'),
                        'Boliche' => trans('attraction.bowling'),
                        'Barcos/Embarcações' => trans('attraction.boats'),
                        'Equipamentos para pesca' => trans('attraction.fishing'),
                        'Serviço de animação' => trans('attraction.cheering_service'),
                        'Pista de cooper' => trans('attraction.cooper_track'),
                        'Charretes' => trans('attraction.carriages'),
                        'Golfe' => trans('attraction.golf'),
                        'Equipamento de mergulho' => trans('attraction.diving_equipment'),
                        'Pingue-pongue' => trans('attraction.ping_pong'),
                        'Jogos eletrônicos' => trans('attraction.eletronic_games')
                    ];
                    $v_Items = json_decode($p_Content->servicos_equipamentos_recreacao_lazer,1);
                    $v_Equipments = '';
                    foreach($v_Items as $c_Item)
                    {
                        if(array_key_exists($c_Item['nome'], $v_ItemNames)) {
                            if($c_Item['valor'])
                                $v_Equipments .= (empty($v_Equipments) ? '' : ', ') . $v_ItemNames[$c_Item['nome']];
                        }
                    }
                ?>
                @if(!empty($v_Equipments))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.recreation')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$v_Equipments}}</p>
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->servicos_equipamentos))
                <?php
                    $v_ItemNames = [
                        'Local para eventos' => trans('attraction.event_place'),
                        'Música ao vivo' => trans('attraction.live_music'),
                        'Música mecânica' => trans('attraction.mechanic_music'),
                        'Estacionamento' => trans('attraction.parking_lot'),
                        'Ar condicionado' => trans('attraction.air_conditioning'),
                        'Recreação/Área de lazer' => trans('attraction.recreation_area'),
                        'Espaço para fumantes' => trans('attraction.smoking_lounge'),
                        'Sanitários adaptados' => trans('attraction.adapted_toilets'),
                        'Cardápio em braille' => trans('attraction.braille_menu'),
                        'Manobristas' => trans('attraction.vallets'),
                        'Internet' => trans('attraction.internet')
                    ];
                    $v_Items = json_decode($p_Content->servicos_equipamentos,1);
                    $v_Equipments = '';
                    $v_EventLocation = false;
                    foreach($v_Items as $c_Item)
                    {
                        if(array_key_exists($c_Item['nome'], $v_ItemNames)) {
                            if($c_Item['valor']) {
                                if($c_Item['nome'] == 'Local para eventos')
                                    $v_EventLocation = true;
                                if($c_Item['nome'] == 'Local para eventos - Capacidade' && $v_EventLocation)
                                    $v_Equipments .= ', ' . trans('attraction.capacity') . ': ' . $c_Item['valor'];
                                else
                                    $v_Equipments .= (empty($v_Equipments) ? '' : ', ') . $v_ItemNames[$c_Item['nome']];
                            }
                        }
                    }
                ?>
                @if(!empty($v_Equipments))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.services_equipments')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$v_Equipments}}</p>
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->culinaria_nacionalidades) || !empty($p_Content->culinaria_nacionalidade_outras))
                <?php
                    $v_Items = json_decode($p_Content->culinaria_nacionalidades,1);
                    $v_Text = empty($p_Content->culinaria_nacionalidade_outras) ? '' : json_decode($p_Content->culinaria_nacionalidade_outras,1)[$p_Language];
                ?>
                @if(!empty($v_Items))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.cuisine_nationality')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>
                                @foreach($v_Items as $c_Index => $c_Item)
                                    {{($c_Index == 0 ? '' : ', ') . $p_CuisineNationalities[$c_Item['id']]}}
                                @endforeach
                            </p>
                            @if(!empty($v_Text))
                                <p>{!! nl2br($v_Text) !!}</p>
                            @endif
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->culinaria_servicos))
                <?php
                    $v_Items = json_decode($p_Content->culinaria_servicos,1);
                ?>
                @if(!empty($v_Items))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.cuisine_service')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>
                                @foreach($v_Items as $c_Index => $c_Item)
                                    {{($c_Index == 0 ? '' : ', ') . $p_CuisineServices[$c_Item['id']]}}
                                @endforeach
                            </p>
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->culinaria_temas) || !empty($p_Content->culinaria_temas_outros))
                <?php
                    $v_Items = json_decode($p_Content->culinaria_temas,1);
                    $v_Text = empty($p_Content->culinaria_temas_outros) ? '' : json_decode($p_Content->culinaria_temas_outros,1)[$p_Language];
                ?>
                @if(!empty($v_Items))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.cuisine_theme')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>
                                @foreach($v_Items as $c_Index => $c_Item)
                                    {{($c_Index == 0 ? '' : ', ') . $p_CuisineThemes[$c_Item['id']]}}
                                @endforeach
                            </p>
                            @if(!empty($v_Text))
                                <p>{!! nl2br($v_Text) !!}</p>
                            @endif
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->culinaria_regioes) || !empty($p_Content->culinaria_regiao_outras))
                <?php
                    $v_Items = json_decode($p_Content->culinaria_regioes,1);
                    $v_Text = empty($p_Content->culinaria_regiao_outras) ? '' : json_decode($p_Content->culinaria_regiao_outras,1)[$p_Language];
                ?>
                @if(!empty($v_Items))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.cuisine_region')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>
                                @foreach($v_Items as $c_Index => $c_Item)
                                    {{($c_Index == 0 ? '' : ', ') . $p_CuisineRegions[$c_Item['id']]}}
                                @endforeach
                            </p>
                            @if(!empty($v_Text))
                                <p>{!! nl2br($v_Text) !!}</p>
                            @endif
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->restricoes))
                <?php $v_Text = json_decode($p_Content->restricoes,1)[$p_Language]; ?>
                @if(!empty($v_Text))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.restrictions')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{!! nl2br($v_Text) !!}</p>
                        </div>
                    </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->segmentos_agencia))
                <?php
                    $v_TourismSegments = [
                        'Aventura' => trans('attraction.specialization_adventure'),
                        'Ecoturismo' => trans('attraction.specialization_ecoturism'),
                        'Rural' => trans('attraction.specialization_rural'),
                        'Sol e praia' => trans('attraction.specialization_sun_beach'),
                        'Estudos e intercâmbio' => trans('attraction.specialization_studies'),
                        'Negócios e eventos' => trans('attraction.specialization_business'),
                        'Cultural' => trans('attraction.specialization_cultural'),
                        'Náutico' => trans('attraction.specialization_nautic'),
                        'Saúde (bem-estar e médico)' => trans('attraction.specialization_health'),
                        'Pesca' => trans('attraction.specialization_fishing'),
                        'Não é especializado em nenhum segmento' => ''
                    ];
                    $v_Items = json_decode($p_Content->segmentos_agencia,1);
                    $v_Segments = '';
                    foreach($v_Items as $c_Item)
                        $v_Segments .= (empty($v_Segments) ? '' : ', ') . $v_TourismSegments[$c_Item];
                ?>
                @if(!empty($v_Segments))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.tourism_specialization')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$v_Segments}}</p>
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->tipos_servico))
                <?php
                    $v_ServiceTypes = [
                        'Excursão' => trans('attraction.service_type_excursion'),
                        'Passeio local' => trans('attraction.service_type_local'),
                        'Especial' => trans('attraction.service_type_special'),
                        'Traslado' => trans('attraction.service_type_transfer')
                    ];
                    $v_Items = json_decode($p_Content->tipos_servico,1);
                ?>
                @if(!empty($v_Segments))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.service_types')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>
                                @foreach($v_Items as $c_Index => $c_Item)
                                    {{($c_Index == 0 ? '' : ', ') . $v_ServiceTypes[$c_Item['id']]}}
                                @endforeach
                            </p>
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->quantidade_veiculos_adaptados))
                <div class="col-lg-4" id="services">
                <div class="bg-service">
                    <div class="col-lg-12 name-service">
                        <h4>{{trans('attraction.adapted_vehicles_quantity')}}</h4>
                    </div>
                    <div class="col-lg-12 resume-service">
                        <p>{{$p_Content->quantidade_veiculos_adaptados}}</p>
                    </div>
                    </div>
                </div>
            @endif

            @if(!empty($p_Content->servicos_equipamentos_apoio))
                <?php
                    $v_ItemNames = [
                        'Serviço de segurança' => trans('attraction.security_service'),
                        'Serviços médicos' => trans('attraction.medical_services'),
                        'Serviços ambulatoriais' => trans('attraction.ambulatorial_services'),
                        'Serviços de café/buffet' => trans('attraction.buffet'),
                        'Informações turísticas' => trans('attraction.touristic_info'),
                        'Lojas' => trans('attraction.stores'),
                        'Banco 24 horas' => trans('attraction.bank24hours'),
                        'Internet' => trans('attraction.internet'),
                        'Estacionamento - Manobrista' => trans('attraction.vallets'),
                    ];
                    $v_Items = json_decode($p_Content->servicos_equipamentos_apoio,1);
                    $v_Equipments = '';
                    foreach($v_Items as $c_Item)
                    {
                        if(array_key_exists($c_Item['nome'], $v_ItemNames)) {
                            if($c_Item['valor']) {
                                if($c_Item['nome'] == 'Estacionamento - Vagas')
                                    $v_Equipments = (empty($v_Equipments) ? '' : ', ') . trans('attraction.parking_lot') . ': ' . $c_Item['valor'] . ' ' . trans('attraction.parking_lot_spots');
                                else
                                    $v_Equipments .= (empty($v_Equipments) ? '' : ', ') . $v_ItemNames[$c_Item['nome']];
                            }
                        }
                    }
                ?>
                @if(!empty($v_Equipments))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.support_services_equipments')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{{$v_Equipments}}</p>
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            @if(!empty($p_Content->principais_atividades))
                <?php $v_Text = json_decode($p_Content->principais_atividades,1)[$p_Language]; ?>
                @if(!empty($v_Text))
                    <div class="col-lg-4" id="services">
                    <div class="bg-service">
                        <div class="col-lg-12 name-service">
                            <h4>{{trans('attraction.main_activities')}}</h4>
                        </div>
                        <div class="col-lg-12 resume-service">
                            <p>{!! nl2br($v_Text) !!}</p>
                        </div>
                        </div>
                    </div>
                @endif
            @endif

            <div class="col-lg-4 acessibilidade" id="services">
            <div class="bg-service">
                <div class="col-lg-12 name-service">
                    <h4>{{trans('attraction.accessibility')}}</h4>
                </div>
                <div class="col-lg-12 resume-service">
                    <input type="hidden" id="acessibilidade" value="{{$p_Content->acessibilidade}}">
                </div>
                </div>
            </div>
        </div><!--cards-->
    </div>
</div>   

    </div>
    
@stop

@section('pageScript')
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a0debd79357d1c" async="async"></script>
<script src="{{url('/vendor/lightbox/js/lightbox.min.js')}}"></script>
<script>
    $(document).ready(function()
    {
        carregarDadosFuncionamento(1);
        carregarDadosFuncionamento(2);
        carregarDadosAcessibilidade();

        $('#myTabs a').click(function(){
            $('#myTabContent .tab-pane').removeClass('active in');
            $('#myTabContent .tab-pane' + $(this).attr('href')).addClass('active in');
        });

        @if($p_Content != null && $p_Content->latitude != null && $p_Content->longitude != null)
            var v_MapFrame = $('#map_canvas');

            v_MapFrame.gmap3({
                marker: {
                    values:[
                        {latLng:[{{ $p_Content->latitude }}, {{ $p_Content->longitude }}]}
                    ],
                    options:{
                        draggable: false
                    }
                },
                map:{
                    options:{
                        zoom:15,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        navigationControl: false,
                        scrollwheel: false,
                        streetViewControl: true
                    }
                }
            });
        @endif
    });


    var m_Meses = {
        Janeiro:'{!!trans('string.january')!!}',
        Fevereiro:'{!!trans('string.february')!!}',
        Março:'{!!trans('string.march')!!}',
        Abril:'{!!trans('string.april')!!}',
        Maio:'{!!trans('string.may')!!}',
        Junho:'{!!trans('string.june')!!}',
        Julho:'{!!trans('string.july')!!}',
        Agosto:'{!!trans('string.august')!!}',
        Setembro:'{!!trans('string.september')!!}',
        Outubro:'{!!trans('string.october')!!}',
        Novembro:'{!!trans('string.november')!!}',
        Dezembro:'{!!trans('string.december')!!}'
    };
    var m_Dias = {
        Segunda:'{!!trans('string.monday')!!}',
        Terça:'{!!trans('string.tuesday')!!}',
        Quarta:'{!!trans('string.wednesday')!!}',
        Quinta:'{!!trans('string.thursday')!!}',
        Sexta:'{!!trans('string.friday')!!}',
        Sábado:'{!!trans('string.saturday')!!}',
        Domingo:'{!!trans('string.sunday')!!}',
        Feriados:'{!!trans('string.holiday')!!}'
    };

    function carregarDadosFuncionamento(p_Index)
    {
        var v_Funcionamento = $('#funcionamento_'+p_Index).val();
        var $v_Container = $('.funcionamento_'+p_Index + ' .resume-service');
        var v_MesesString = '';
        if(v_Funcionamento.length > 0)
        {
            v_Funcionamento = JSON.parse(v_Funcionamento);
            if(v_Funcionamento.meses.length == 12)
                v_MesesString = ' ';
            else {
                $(v_Funcionamento.meses).each(function(){
                    if(v_MesesString.length > 0)
                        v_MesesString += ', ';
                    v_MesesString += m_Meses[this];
                });
                $v_Container.append('<p>'+v_MesesString+'</p>');
            }
            var v_DiasStrings = [];
            $(v_Funcionamento.dias).each(function(){
                var v_Period = '';
                var v_Dia = this;
                if(v_Dia.fechado)
                    v_Period = '{!!trans('string.closed')!!}';
                else if(v_Dia.funciona24horas)
                    v_Period = '{!!trans('string.24hours')!!}';
                else if(v_Dia.horarioDe.length && v_Dia.horarioAte.length) {
                    v_Period = v_Dia.horarioDe + ' - ' + v_Dia.horarioAte;
                }
                if(v_Period.length > 0){
                    var v_HasPeriod = false;
                    $(v_DiasStrings).each(function(c_Index){
                        if(this.indexOf(v_Period) > -1){
                            var v_Strings = this.split(': ');
                            v_DiasStrings[c_Index] = v_Strings[0] + ', ' + m_Dias[v_Dia.dia] + ': ' + v_Strings[1];
                            v_HasPeriod = true;
                            return false;
                        }
                    });
                    if(!v_HasPeriod)
                        v_DiasStrings.push('<p>' + m_Dias[v_Dia.dia] + ': ' + v_Period + '</p>')
                }
            });
            $v_Container.append(v_DiasStrings.join(''));
        }
        if(v_MesesString.length == 0)
            $('.funcionamento_'+p_Index).remove();
    }

    function carregarDadosAcessibilidade()
    {
        var v_Acessibilidade = $('#acessibilidade').val();
        var $v_Container = $('.acessibilidade .resume-service');
        var v_AcessibilidadeString = '';
        if(v_Acessibilidade.length > 0)
        {
            v_Acessibilidade = JSON.parse(v_Acessibilidade);
            @if($p_Content->item_inventario == 'B3')
                var v_Fields = {
                    'Possui acessibilidade na estrutura do espaço físico':'{!!trans('attraction.accessibility_agency_structure')!!}',
                    'Roteiros Adaptados':'{!!trans('attraction.accessibility_adapted_routes')!!}',
                    'Meios de hospedagem':'{!!trans('attraction.accessibility_accomodation')!!}',
                    'Transportes':'{!!trans('attraction.accessibility_transport')!!}',
                    'Guias preparados para público com deficiência ou mobilidade reduzida':'{!!trans('attraction.accessibility_prepared_guides')!!}',
                    'Atrativos':'{!!trans('attraction.accessibility_attractions')!!}',
                    'Material acessivo':'{!!trans('attraction.accessibility_accessible_material')!!}'
                };
                var v_Options = {
                    'Corrimão':'{!!trans('attraction.accessibility_handrail')!!}',
                    'Vagas de estacionamento especiais':'{!!trans('attraction.accessibility_special_parking_spots')!!}',
                    'Rampas de acesso':'{!!trans('attraction.accessibility_access_ramps')!!}',
                    'Funcionários capacitados':'{!!trans('attraction.accessibility_capacitated_workers')!!}',
                    'Sinalização tátil (deficiência visual)':'{!!trans('attraction.accessibility_tactile_signaling')!!}',
                    'Sanitário acessível':'{!!trans('attraction.accessibility_accessible_toilet')!!}',
                    'Espaço para deslocamento (cadeirantes)':'{!!trans('attraction.accessibility_displacement_space')!!}',
                    'Física ou motora':'{!!trans('attraction.accessibility_physical')!!}',
                    'Visual':'{!!trans('attraction.accessibility_visual')!!}',
                    'Auditiva':'{!!trans('attraction.accessibility_hearing')!!}',
                    'Mental':'{!!trans('attraction.accessibility_mental')!!}',
                    'Mobilidade reduzida (idosos, gestantes, obesos entre outros)':'{!!trans('attraction.accessibility_reduced_mobility')!!}',
                    'Assento especial para obesos':'{!!trans('attraction.acessibility_special_seat_obese')!!}',
                    'Porta larga o suficiente para entrada de obesos':'{!!trans('attraction.accessibility_large_door_obese')!!}',
                    'Sanitário com assento especial para obesos':'{!!trans('attraction.accessibility_toilet_seat_obese')!!}'
                };

                $(v_Acessibilidade).each(function(){
                    if(this.tipo == 'text' && this.valor.length) {
                        if(this.nome == 'Observações complementares - {{$p_Language}}')
                            v_AcessibilidadeString += '<p>{!!trans('attraction.complementary_observations')!!}: ' + this.valor + '</p>';
                    }
                    else {
                        var v_Campo = this;
                        var v_Valores = '';
                        $(v_Campo.valor).each(function(){
                            if(this != 'Não')
                                v_Valores += (v_Valores.length ? ', ' : '') + v_Options[this];
                        });
                        if(v_Valores.length)
                            v_AcessibilidadeString += '<p>' + v_Fields[v_Campo.nome] + ': ' + v_Valores + '</p>';
                    }
                });
            @else
                var v_Fields = {
                    'Rota externa acessível':'{!!trans('attraction.accessibility_accessible_external_route')!!}',
                    'Local de embarque e desembarque':'{!!trans('attraction.accessibility_boarding_area')!!}',
                    'Vaga em estacionamento':'{!!trans('attraction.accessibility_parking_spot')!!}',
                    'Área de circulação/acesso interno para cadeiras de rodas/Lazer e UHs':'{!!trans('attraction.accessibility_access_area')!!}',
                    'Escada':'{!!trans('attraction.accessibility_stairs')!!}',
                    'Elevador':'{!!trans('attraction.accessibility_elevator')!!}',
                    'Comunicação':'{!!trans('attraction.accessibility_communication')!!}',
                    'Assento especial para obesos':'{!!trans('attraction.acessibility_special_seat_obese')!!}',
                    'Sanitário':'{!!trans('attraction.accessibility_toilet')!!}'
                };
                var v_Options = {
                    'Estacionamento':'{!!trans('attraction.parking_lot')!!}',
                    'Calçada rebaixada':'{!!trans('attraction.accessibility_low_sidewalk')!!}',
                    'Faixa de pedestre':'{!!trans('attraction.accessibility_walking_stripes')!!}',
                    'Rampa':'{!!trans('attraction.accessibility_ramp')!!}',
                    'Semáforo sonoro':'{!!trans('attraction.accessibility_sound_semaphore')!!}',
                    'Piso tátil de alerta':'{!!trans('attraction.accessibility_floor_tactile_alert')!!}',
                    'Piso regular e antiderrapante':'{!!trans('attraction.accessibility_regular_floor')!!}',
                    'Livre de obstáculos':'{!!trans('attraction.accessibility_obstacle_free')!!}',
                    'Sinalizado':'{!!trans('attraction.accessibility_signalized')!!}',
                    'Com acesso em nível':'{!!trans('attraction.accessibility_leveled_access')!!}',
                    'Sinalizada':'{!!trans('attraction.accessibility_signalized_a')!!}',
                    'Alargada para cadeira de rodas':'{!!trans('attraction.accessibility_large_wheelchair')!!}',
                    'Rampa de acesso à calçada':'{!!trans('attraction.accessibility_sidewalk_ramp')!!}',
                    'Elevador':'{!!trans('attraction.accessibility_elevator')!!}',
                    'Plataforma elevatória':'{!!trans('attraction.accessibility_elevator_platform')!!}',
                    'Com circulação entre mobiliário':'{!!trans('attraction.accessibility_circulation_furniture')!!}',
                    'Porta larga':'{!!trans('attraction.accessibility_large_door')!!}',
                    'Piso regular/antiderrapante':'{!!trans('attraction.accessibility_regular_floor')!!}',
                    'Corrimão':'{!!trans('attraction.accessibility_handrail')!!}',
                    'Patamar para descanso':'{!!trans('attraction.accessibility_resting_platform')!!}',
                    'Sinalização tátil de alerta':'{!!trans('attraction.accessibility_tactile_alert_signaling')!!}',
                    'Piso antiderrapante':'{!!trans('attraction.accessibility_non_skid_floor')!!}',
                    'Sinalizado em Braille':'{!!trans('attraction.accessibility_braille_signaling')!!}',
                    'Dispositivo sonoro':'{!!trans('attraction.accessibility_sound_mechanism')!!}',
                    'Dispositivo luminoso':'{!!trans('attraction.accessibility_light_mechanism')!!}',
                    'Sensor eletrônico (porta)':'{!!trans('attraction.accessibility_eletronic_sensor')!!}',
                    'Texto informativo em Braille':'{!!trans('attraction.accessibility_braille_informative_text')!!}',
                    'Texto informativo em fonte ampliada':'{!!trans('attraction.accessibility_large_informative_text')!!}',
                    'Intérprete em Libras (língua brasileira de sinais)':'{!!trans('attraction.accessibility_signaling_interpreter')!!}',
                    'Barra de apoio':'{!!trans('attraction.accessibility_support_bar')!!}',
                    'Porta larga suficiente para entrada de cadeira de rodas':'{!!trans('attraction.accessibility_large_door_wheelchair')!!}',
                    'Giro para cadeira de rodas':'{!!trans('attraction.accessibility_wheelchair_spin')!!}',
                    'Acesso para cadeira de rodas':'{!!trans('attraction.accessibility_wheelchair_access')!!}',
                    'Pia rebaixada':'{!!trans('attraction.accessibility_low_sink')!!}',
                    'Espelho rebaixado ou com ângulo de alcance visual':'{!!trans('attraction.accessibility_low_mirror')!!}',
                    'Boxe ou banheira adaptada':'{!!trans('attraction.accessibility_adapted_bath')!!}',
                    'Assento especial para obesos':'{!!trans('attraction.acessibility_special_seat_obese')!!}',
                    'Porta larga o suficiente para entrada de obesos':'{!!trans('attraction.accessibility_large_door_obese')!!}',
                    'Sanitário com assento especial para obesos':'{!!trans('attraction.accessibility_toilet_seat_obese')!!}'
                };

                if(v_Acessibilidade.possui) {
                    $(v_Acessibilidade.campos).each(function(){
                        if(this.tipo == 'text' && this.valor.length) {
                            if(this.nome == 'Rota externa acessível - Outras - {{$p_Language}}')
                                v_AcessibilidadeString += '<p>{!!trans('attraction.accessibility_accessible_external_route_others')!!}: ' + this.valor + '</p>';
                            else if(this.nome == 'Observações complementares - {{$p_Language}}')
                                v_AcessibilidadeString += '<p>{!!trans('attraction.complementary_observations')!!}: ' + this.valor + '</p>';
                        }
                        else {
                            var v_Campo = this;
                            var v_Valores = '';
                            $(v_Campo.valor).each(function(){
                                if(this != 'Não')
                                    v_Valores += (v_Valores.length ? ', ' : '') + v_Options[this];
                            });
                            if(v_Valores.length)
                                v_AcessibilidadeString += '<p>' + v_Fields[v_Campo.nome] + ': ' + v_Valores + '</p>';
                        }
                    });
                }
            @endif

            $v_Container.append(v_AcessibilidadeString);
        }
        if(v_AcessibilidadeString.length == 0)
            $('.acessibilidade').remove();
    }
</script>
@stop