@extends('public.base')
@section('pageCSS')
@stop
@section('main-content')
<div class="row-fluid" id="testimonials" style="padding:0"></div>
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12">
                <h2 class="laranja margint">{{trans('menu.what_to_do')}}</h2>
            </div>
        </div>
    </div>
    <div class="row-fluid " id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12">
                        <p>{!! $p_Content == null ? '' : nl2br($p_Content->descricao) !!}</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 line no-padding">
                @foreach($p_Categories as $c_Index => $c_Category)
                    <?php
                        $v_Name = json_decode($c_Category->nome,1)[$p_Language];
                        $v_Description = json_decode($c_Category->descricao_curta,1)[$p_Language];
                    ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 list-thumbs">
                        <a href="{{url($p_Language . '/o-que-fazer/' . $c_Category->slug)}}">
                            <div class="hoverzoom">
                                <div class="thumbs-mini-three">
                                    <!--<img src="{{$c_Category->foto_capa_url}}">-->
                                    <div class="thumbs-mini-recorte" style="background: url('{{$c_Category->foto_capa_url}}') no-repeat;"></div>
                                </div>
                                <div class="retina-hover">
                                    <div class="col-lg-12 title">
                                        <p>{{ $v_Name }}</p>
                                    </div>
                                    <div class="col-lg-12 no-padding">
                                        <hr>
                                    </div>
                                    <div class="col-lg-12 text">
                                        <p>{{ $v_Description }}</p>
                                    </div>
                                </div>
                                <div class="retina">
                                    <p>{{ $v_Name }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>

            @if(count($p_Types))
                <div class="col-lg-12 no-padding">
                    <div class="col-lg-12">
                        <h2 style="color:#b4b4b4;">
                            {{trans('whatToDo.ideal_trip')}}
                        </h2>
                        <p style="text-align:center;margin-bottom:30px;">
                            {!! $p_Content == null ? '' : nl2br($p_Content->descricao_outra) !!}
                        </p>
                    </div>
                    <div class="col-lg-12 text-center" id="choices_what">
                        @foreach($p_Types as $c_Index => $c_Type)
                            <?php $v_Name = json_decode($c_Type->nome,1)[$p_Language]; ?>
                            <div  style="display: inline-table; margin: 1% 1% 0 1%;">
                                <a style="padding:9px 30px 9px 9px; border-radius: 0px;" class="options label" href="{{url($p_Language . '/o-que-fazer/' . $c_Type->category_slug . '/' . $c_Type->slug)}}">{{$v_Name}}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
        </div>
    </div>
@stop

@section('pageScript')
@stop