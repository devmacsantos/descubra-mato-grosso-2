<section  class="fixed-bottom container-fluid text-light d-block d-md-none menu-mobile seach-form">
<form method="GET" action="{{url('/pt/busca')}}"  accept-charset="UTF-8" class="form-inline justify-content-center py-2">
            <div class="input-group w-100">
                <input type="text" name="q" class="form-control" placeholder="{{trans('menu.search_in_site')}}" aria-label="" aria-describedby="btnGroupAddon2">
                <div class="input-group-append">
                    <button type="submit" class="input-group-text" id="btnGroupAddon2"><span class="oi oi-magnifying-glass"></span></button>
                </div>
            </div>                                                                                        
        </form>        
</section>
