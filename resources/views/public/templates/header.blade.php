<header class=" container-fluid bg shadow-sm-1" style="background-color:white;">
    <nav class="navbar navbar-expand-lg bg-white navbar-light"> 
        <a class="navbar-brand mb-0" href="{{url($p_Language)}}">
            <img class="img-fluid" src="{{url('/portal/assets/imgs/descubraMatoGrosso.png')}}" alt="Descubra Mato Grosso" title="Descubra Mato Grosso">
        </a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarPortal">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarPortal" style="overflow:auto;">
            <ul class="navbar-nav small mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="http://www.mt.gov.br/" target="_blank" style="font-size: 18px; color: #0000FF; font-family: Arial, Helvetica, sans-serif;"><strong>Mato Grosso</strong></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url($p_Language . '/para-onde-ir')}}" style="font-size: 18px; color: #0000FF; font-family: Arial, Helvetica, sans-serif;""><strong>{{trans('menu.where_to_go')}}</strong></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url($p_Language . '/o-que-fazer')}}" style="font-size: 18px; color: #0000FF; font-family: Arial, Helvetica, sans-serif;""><strong>{{trans('menu.what_to_do')}}</strong></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/admin/login')}}" target="_blank" style="font-size: 18px; color: #0000FF; font-family: Arial, Helvetica, sans-serif;""><strong>{{trans('menu.register_your_business', [],'messages', $p_Language)}}</strong></a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item pl-3 d-none d-md-flex w-100">
                    <form method="GET" action="{{url('/pt/busca')}}"  accept-charset="UTF-8" class="form-inline py-1">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="{{trans('menu.search_in_site')}}" aria-label="" aria-describedby="btnGroupAddon2">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-outline-secondary" id="btnGroupAddon2"><span class="oi oi-magnifying-glass"></span></button>
                                </div>
                        </div>                                                                                        
                    </form>
                </li>
            </ul>
            <ul class="navbar-nav small my-2 my-lg-0 pl-5">
                <li class="nav-item">
                    <a class="nav-link" href="https://www.facebook.com/descubraMatoGrosso/" target="_blank">
                        <img src="{{url('/portal/assets/imgs/facebookGray.png')}}" alt="Facebook" title="Facebook">
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.instagram.com/descubramatogrosso/" target="_blank">
                        <img src="{{url('/portal/assets/imgs/instagramGray.png')}}" alt="Instagram" title="Instagram">
                    </a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="{{url('/pt')}}"><img src="{{url('/portal/assets/imgs/flag-' . "pt" . '.png')}}"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/fr')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "fr" . '.png')}}"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/en')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "en" . '.png')}}"></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/es')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "es" . '.png')}}"></a>
                </li>-->
            </ul>
            <!-- Futuramente quando precisar
                <li class="nav-item">
                    <a class="nav-link" href="{{url($p_Language . '/roteiros-duracao')}}">{{trans('menu.plan_your_trip')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url($p_Language . '/explore-o-mapa')}}">{{trans('menu.explore_the_map')}}</a>
                </li>
                <li class="nav-item">
                        <a class="nav-link strings px-1" href="{{url($p_Language . '/blog')}}">Blog</a>
                </li> 
            <ul class="navbar-nav ">
                <li class="nav-item">
                    <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . $p_Language . '.png')}}">
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a class="dropdown-item" href="{{url('/pt')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "pt" . '.png')}}"> Português</a>
                            <a class="dropdown-item disabled" href="{{url('/fr')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "fr" . '.png')}}"> Français</a>
                            <a class="dropdown-item disabled" href="{{url('/en')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "en" . '.png')}}"> English</a>
                            <a class="dropdown-item disabled" href="{{url('/es')}}"><img class="lang-flag" src="{{url('/portal/assets/imgs/flag-' . "es" . '.png')}}"> Español</a>
                        </div>
                    </div>
                </li>
            </ul>-->
        </div>
    </nav>
</header>