@extends('public.base')

@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style>
        .row.maps{
            margin-right: 0;
            margin-left: 0;
        }
        .select2-container--default .select2-selection--single {
            background: #e6e6e6;
            border-radius: 0;
            border: none;
            text-align: center;
            color: #818181;
            font-size: 1rem;
            line-height: 1.5;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 39px;
        }
        .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow{
            min-height: 39px;
        }
        .content-checkbox input[type="radio"] + label {
            font-family: 'Montserrat', sans-serif;
            font-size: 21px;
        }
        .content-checkbox input[type="radio"] + label span {
            background-color: #ffffff;
            border: 3px solid;
        }
        .content-checkbox input[type="radio"]:checked + label span {
             background-color: #488dc8;
        }
    </style>
@stop

@section('main-content')
    <div class="row-fluid" id="useful-information">
        <div class="container">
            <div class="col-lg-12" id="list-title">
                <h2>{{trans('menu.explore_the_map')}}</h2>
            </div>
        </div>
    </div>

    <div class="row-fluid" id="destinos" style="padding:1% 0;">
        <div class="container">
            <div class="col-lg-12">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-12">
                        <p>{{$p_Content == null ? '' : $p_Content}}</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <h3 class="text-center sub-titulo">{{trans('exploreMap.mark_attractions_to_show_or_select_destination')}}.</h3>
            </div>

            <div class="col-lg-12 no-padding">
                

                <div class="col-lg-12 no-padding" style="margin:1rem 0 2rem;">
                    <div class="col-lg-4 col-md-6">
                        <h4 class="laranja text-left">{{trans('exploreMap.nature')}}</h4>
                        <ul class="list-checkbox map-filters">
                            <?php
                                $v_Items = [
                                    'parks' => 'menu.parks',
                                    'waterfalls' => 'exploreMap.waterfalls',
                                    'lakes' => 'exploreMap.lakes',
                                    'caves' => 'exploreMap.caves',
                                    'mountains' => 'exploreMap.mountains',
                                    'hidromineral' => 'exploreMap.hidromineral',
                                ];
                            ?>
                            @foreach($v_Items as $c_Key => $c_String)
                            <li>
                                <div class="content-checkbox">
                                    <input id="{{$c_Key}}" type="checkbox" value="{{$c_Key}}" checked>
                                    <span></span><label for="{{$c_Key}}">{{trans($c_String)}}</label>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="col-lg-4 col-md-6">
                        <h4 class="laranja text-left">{{trans('exploreMap.culture')}}</h4>
                        <ul class="list-checkbox map-filters">
                            <?php
                                $v_Items = [
                                    'historic_centers' => 'exploreMap.historic_centers',
                                    'buildings_monuments' => 'exploreMap.buildings_monuments',
                                    'churches' => 'exploreMap.churches',
                                    'museums' => 'exploreMap.museums',
                                    'zoos' => 'exploreMap.zoos',
                                    'markets' => 'exploreMap.markets',
                                ];
                            ?>
                            @foreach($v_Items as $c_Key => $c_String)
                            <li>
                                <div class="content-checkbox">
                                    <input id="{{$c_Key}}" type="checkbox" value="{{$c_Key}}" checked>
                                    <span></span><label for="{{$c_Key}}">{{trans($c_String)}}</label>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>


                <div class="col-lg-12">
                    <div class="col-lg-8 col-lg-offset-3">
                        <div class="col-lg-9">
                            <form action="#" method="post">
                                <div class="input-group" style="width: 100%;">
                                    {!! Form::select('cidade', [], null, ['id' => 'selectCities', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%; padding: 9.5px; border-radius: 0; border:none; background:#e6e6e6']) !!}
                                    {{--<input type="text" class="form-control" placeholder="{{trans('exploreMap.select_destination')}}" style="padding: 9.5px;border-radius: 0;border:none;background:#e6e6e6;" title="{{trans('exploreMap.select_destination')}}" required>--}}
                                    <span class="input-group-btn" style="width: 15%;">
                                        <button class="btn btn-secondary" type="button" style="padding:0;border-radius: 0;border:none;background:transparent;">
                                            <img src="{{url('/portal/assets/imgs/filtro.png')}}" style="height:40px" alt="">
                                        </button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-row-fluid" id="map_canvas" style="height:600px;">
    </div>
@stop

@section('pageScript')
    <script type="text/javascript" src="{{url('/portal/src/scripts/convexHull.js')}}"></script>
    <script type="text/javascript" src="{{url('/portal/src/scripts/epolys.js')}}"></script>
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>

    <script>
        // SELECT & Filters
        var g_FilterCategories = [];
        var g_FilterDestinationIndex = '';
        var v_Destinations = {!! json_encode($p_Destinations) !!};
        var v_Attractions = {!! json_encode($p_Attractions) !!};

        // MAP
        var g_Map;
        var g_MapCenter = new google.maps.LatLng(-18.926058, -45.385104);
        var g_LastValidCenter = g_MapCenter;
        var g_FeaturesMarkerSets = [];
        var g_FeaturesSets = [];
        var g_IconBase = '{{url('/portal/assets/imgs/geopins')}}/';
        var g_InfoWindow = new google.maps.InfoWindow;


        function keepInBounds(p_AllowedBounds){
            v_CurrentBounds = g_Map.getBounds();
            if (p_AllowedBounds.contains(v_CurrentBounds.getNorthEast()) && p_AllowedBounds.contains(v_CurrentBounds.getSouthWest()) )
                g_LastValidCenter = g_Map.getCenter();
            else
                g_Map.panTo(g_LastValidCenter);
        }

        function fixBoundsIfNeeded(p_AllowedBounds){
            v_CurrentBounds = g_Map.getBounds();
            if (!p_AllowedBounds.contains(v_CurrentBounds.getNorthEast()) || !p_AllowedBounds.contains(v_CurrentBounds.getSouthWest()) )
            {
                g_LastValidCenter = g_MapCenter;
                g_Map.panTo(g_LastValidCenter);
            }
        }

        function showMarkers(p_Markers){
            p_Markers.forEach(function(c_Marker){
                c_Marker.setMap(g_Map);
            });
        }

        function hideMarkers(p_Markers){
            p_Markers.forEach(function(c_Marker){
                c_Marker.setMap(null);
            });
        }

        function toggleMarkers(p_Markers){
            if(p_Markers[0].getMap() == g_Map)
                hideMarkers(p_Markers);
            else
                showMarkers(p_Markers);
        }

        function addMarker(p_Feature) {
            var v_Marker = new google.maps.Marker({
                position: p_Feature.position,
                icon: g_IconBase + p_Feature.type + '.png',
                map: g_Map,
                cityId: p_Feature.cityId,
                type: p_Feature.type
            });

            v_Marker.addListener('click', function() {
                g_InfoWindow.setContent(p_Feature.infoHTML);
                g_InfoWindow.open(g_Map, v_Marker);
            });

            return v_Marker;
        }

        function addMarkers(p_Features){
            var v_Markers = [];
            p_Features.forEach(function(c_Feature){
                var v_Marker = addMarker(c_Feature);
                v_Markers.push(v_Marker);
            });
            return v_Markers;
        }

        function initializeMap(){
            var v_MapCanvas = document.getElementById('map_canvas');
            var v_MapOptions = {
                center: g_MapCenter,
                language: 'pt-BR',
                zoom: 6,
                minZoom: 6,
                mapTypeControlOptions: {
                    mapTypeIds: []
                },
                navigationControl: false,
                scrollwheel: false,
                streetViewControl: true
            };
            g_Map = new google.maps.Map(v_MapCanvas, v_MapOptions);

            var v_CustomMapType = new google.maps.StyledMapType([
//                    {
//                        featureType: 'all',
//                        elementType: 'all',
//                        stylers: [
//                            { color: "#F0F0F0" }
//                        ]
//                    }
                ],{
                    name: 'Custom Style'
                }
            );
            var v_CustomMapTypeId = 'custom_style';

            g_Map.mapTypes.set(v_CustomMapTypeId, v_CustomMapType);
            g_Map.setMapTypeId(v_CustomMapTypeId);

            {{--v_MgLayer = new google.maps.KmlLayer({--}}
                {{--url: 'https://www.dropbox.com/s/w8tv08a555u2dih/mgborder.zip?dl=1',--}}
                {{--url: "{{url('/portal/assets/mgborder.zip')}}",--}}
                {{--suppressInfoWindows: true,--}}
                {{--map: g_Map--}}
            {{--});--}}

            // IMAGE OVERLAY
            {{--var imageBounds = {--}}
                {{--north: -14.233189,--}}
                {{--south: -22.922758,--}}
                {{--east: -39.856824,--}}
                {{--west: -51.046075--}}
            {{--};--}}
            {{--v_MapOverlay = new google.maps.GroundOverlay(--}}
                    {{--"{{url('/exploreMapOverlay.png')}}",--}}
                    {{--imageBounds);--}}
            {{--v_MapOverlay.setOpacity(0.2);--}}
            {{--v_MapOverlay.setMap(g_Map);--}}

            g_FeaturesSets.forEach(function(p_AttractionSet){
                var v_Markers = addMarkers(p_AttractionSet);
                g_FeaturesMarkerSets.push(v_Markers);
                g_FilterCategories.push(v_Markers[0].type);
            });

            v_AllowedBounds = new google.maps.LatLngBounds(
                    new google.maps.LatLng(-23.626012, -52.859094),  //SW point
                    new google.maps.LatLng(-13.247002, -37.770634)   //NE point
            );

            google.maps.event.addListener(g_Map, 'center_changed', function(){
                keepInBounds(v_AllowedBounds);
            });
            google.maps.event.addListener(g_Map, 'zoom_changed', function(){
                fixBoundsIfNeeded(v_AllowedBounds)
            });
            google.maps.event.addListener(g_Map, 'click', function(){
                g_InfoWindow.close();
            });
            google.maps.event.addListener(g_Map, 'click', function(){
                g_InfoWindow.close();
            });
        }


        function getInfoWindowHTML(p_Data){
            return  '<a href="{{url($p_Language . '/atracoes')}}/' + p_Data.slug + '">' +
                        '<div id="iw-container">' +
                            '<div class="iw-content">' +
                                '<img src="' + p_Data.url + '" alt="" style="max-width:200px">' +
                            '</div>' +
                            '<div class="iw-bottom-gradient">'+
                                '<h1 class="mapTitle">' + p_Data.nome + '</h1>' +
                                '<h3 class="mapSubTitle">' + p_Data.cidade + '</h3>' +
                            '</div>' +
                        '</div>' +
                    '</a>';
        }

        $(document).ready(function()
        {
            $.each(v_Attractions, function(c_Key, c_Value){
                var v_FeaturesSet = [];
                var v_Type = c_Key;
                if(c_Value.length) {
                    $(c_Value).each(function(){
                        this.nome = JSON.parse(this.nome).{{$p_Language}};
                        v_FeaturesSet.push({
                            position: new google.maps.LatLng(this.latitude, this.longitude),
                            type: v_Type,
                            infoImageUrl: this.url,
                            infoTitle: this.nome,
                            infoHTML: getInfoWindowHTML(this),
                            infoUrl: this.slug,
                            cityId: this.city_id
                        });
                    });
                    g_FeaturesSets.push(v_FeaturesSet);
                }
            });

            initializeMap();

            //SELECT
            var v_DataString = '<option value="">{{trans('exploreMap.select_destination')}}</option>';
            $.each(v_Destinations, function (c_Key, c_Destination)
            {
                v_DataString += '<option value="' + c_Key + '">' + c_Destination.nome + '</option>';
            });

            $('#selectCities').html(v_DataString).select2(
                @if($p_Language == 'pt')
                    {language:'pt-BR'}
                @endif
            );

            $('#selectCities.select2').on('change', function(){
                g_FilterDestinationIndex = this.value;
                filterAttractions(g_FilterDestinationIndex, g_FilterCategories);
            });


            $('.map-filters input[type="checkbox"]').change(function(){
                g_FilterCategories = [];
                $('.map-filters input[type="checkbox"]:checked').each(function(){
                    g_FilterCategories.push(this.value);
                });
                filterAttractions(g_FilterDestinationIndex, g_FilterCategories);
            });
        });


        function filterAttractions(p_DestinationIndex, p_CategoriesIndexes){
            hideAllMarkers();
            var v_CityId = p_DestinationIndex != '' ? v_Destinations[p_DestinationIndex].city_id : -1;
            var v_Markers = [];
            $(g_FeaturesMarkerSets).each(function(){
                if($.inArray(this[0].type, p_CategoriesIndexes) > -1) {
                    var v_FilteredMarkers = this.filter(function (p_Marker) {
                        return (p_DestinationIndex == '' || p_Marker.cityId.toString() == v_CityId);
                    });
                    v_Markers = v_Markers.concat(v_FilteredMarkers);
                }
            });
            showMarkers(v_Markers);
        }

        function hideAllMarkers(){
            g_FeaturesMarkerSets.forEach(function(p_MarkersSet){
                hideMarkers(p_MarkersSet);
            });
        }
    </script>

@stop