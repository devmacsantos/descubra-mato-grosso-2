<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Dashboard do Descubra Mato Grosso (powered by GAFIT - Soluções em Automação e TI)">
        <meta name="author" content="GAFIT - Soluções em Automação e TI">
        <link rel="icon" href="{{url('/favicon.ico')}}"/>

        <title>Descubra Mato Grosso - Dashboard - Esqueci a senha</title>

        <!-- Font CSS (Via CDN) -->
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800">
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">

        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('/assets/skin/default_skin/css/theme.css')}}">

        <!-- Admin Forms CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('/assets/admin-tools/admin-forms/css/admin-forms.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
       <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
       <![endif]-->
    </head>

<body class="external-page sb-l-c sb-r-c onload-check">

    <!-- Start: Settings Scripts -->
    <script>
        var boxtest = localStorage.getItem('boxed');

        if (boxtest === 'true') {
            document.body.className += ' boxed-layout';
        }
    </script>
    <!-- End: Settings Scripts -->

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content -->
        <section id="content_wrapper">

            <!-- begin canvas animation bg -->
            <div id="canvas-wrapper">
                <canvas id="demo-canvas" width="1364" height="707"></canvas>
            </div>

            <!-- Begin: Content -->
            <section id="content">

                <div class="admin-form theme-info mw500" style="margin-top: 10%;" id="login1">
                    <div class="row mb15 table-layout">
                        <div class="col-xs-6 center-block text-center va-m pln">
                            <a href="{{ url('/') }}">
                                <img src="{{url('/assets/img/descubraMatoGrosso.png')}}" title="Descubra Mato Grosso" class="img-responsive">
                            </a>
                        </div>

                        <div class="col-xs-6 text-right va-b pr5">
                            <div class="login-links">
                                <a style="color: #0000FF">Esqueci a senha</a>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-info mv10 heading-border br-n">
                        @if(session('message'))
                            <div class="alert alert-system alert-block alert-dismissable fade in mbn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <p>{!! session('message') !!}</p>
                            </div>
                        @endif
                        @if(session('error_message'))
                            <div class="alert alert-danger alert-block alert-dismissable fade in mbn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <p>{!! session('error_message') !!}</p>
                            </div>
                        @endif
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger alert-block alert-dismissable fade in mbn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <p>{{ $error }}</p>
                            </div>
                        @endforeach
                        <!-- end .form-header section -->
                        {!! Form::open(array('url'=> url('/admin/resetar-senha'))) !!}
                            <div class="panel-body bg-white p15 pt25">
                                <div class="alert alert-micro alert-border-left alert-info pastel alert-dismissable mn">
                                    <i class="fa fa-info pr10"></i> Insira seu <b>Email</b> e uma nova senha será enviada para você!
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer p25 pv15">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="section mn">
                                            <label for="email" class="field-label text-muted fs18 mb10 hidden">Esqueci a Senha</label>
                                            <div class="smart-widget sm-right smr-80">
                                                <label for="email" class="field prepend-icon">
                                                    <input type="email" name="email" class="gui-input" id="email" placeholder="Endereço de email" required>
                                                    <label for="email" class="field-icon"><i class="fa fa-envelope-o"></i>
                                                    </label>
                                                </label>
                                                {!! Form::submit('Resetar', ['class'=>'button']) !!}
                                            </div>
                                            <!-- end .smart-widget section -->
                                        </div>
                                        <!-- end section -->
                                    </div>

                                </div>

                            </div>
                            <!-- end .form-footer section -->
                        {!! Form::close() !!}

                    </div>

                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->

    <!-- BEGIN: PAGE SCRIPTS -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{url('/vendor/jquery/jquery-1.11.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/jquery/jquery_ui/jquery-ui.min.js')}}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{url('/assets/js/bootstrap/bootstrap.min.js')}}"></script>

    <!-- Page Plugins -->
    <script type="text/javascript" src="{{url('/assets/js/pages/login/EasePack.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/pages/login/rAF.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/pages/login/TweenLite.min.js')}}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{url('/assets/js/utility/utility.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/main.js')}}"></script>

    <!-- Page Javascript -->
    <script type="text/javascript">
        jQuery(document).ready(function() {

            "use strict";

            // Init Theme Core      
            Core.init();

        });

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'REPLACEME', 'auto');
        ga('send', 'pageview');
    </script>

    <!-- END: PAGE SCRIPTS -->

<div class="jvectormap-label"></div></body></html>