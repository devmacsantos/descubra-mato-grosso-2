@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Editar perfil
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/editar-perfil'))) !!}
        <input type="hidden" name="id" value="{{$p_User->id}}">
        <div class="form-group col-sm-6">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="usuario[name]" class="form-control" id="name" placeholder="Digite Aqui" value="{{$p_User->name}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="email">Email<span class="mandatory-field">*</span></label>
            <input type="email" name="usuario[email]" class="form-control" id="email" placeholder="Digite Aqui" value="{{$p_User->email}}" required>
        </div>
        @if($p_User->user_type_id != 6)
        <div class="form-group col-sm-6">
            <label for="user_type_id">Tipo de usuário</label>
            <p>{{$p_User->user_type}}</p>
        </div>
        @endif

        @if($p_User->user_type_id == 4)
            <div class="form-group col-sm-6">
                <label for="city_id">Município</label>
                <p>{{$p_City->name}}</p>
            </div>
        @elseif($p_User->user_type_id == 5)
        <div class="form-group col-sm-6">
            <label for="touristic_circuit_id">Região Turística</label>
            <p>{{$p_Circuit->nome}}</p>
        </div>
        @endif
        {{--<div class="form-group col-sm-6">--}}
            {{--<label for="attraction_id">Trade</label>--}}
            {{--{!! Form::select('usuario[attraction_id]', $p_Trades, $p_User == null ? '' : $p_User->attraction_id, array('id' => 'attraction_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required')) !!}--}}
        {{--</div>--}}
        <div class="col-sm-12"></div>
        @if($p_User == null)
            <div class="form-group col-sm-6">
                <label for="senha">Senha (mínimo de 6 caracteres)<span class="mandatory-field">*</span></label>
                <input type="password" name="senha" class="form-control" id="senha" placeholder="Digite a senha" required>
            </div>
            <div class="form-group col-sm-6">
                <label for="senha_confirmation">Confirmar senha (mínimo de 6 caracteres)<span class="mandatory-field">*</span></label>
                <input type="password" name="senha_confirmation" class="form-control" id="senha_confirmation" placeholder="Digite a senha" required>
            </div>
        @else
            <div class="form-group col-sm-6">
                <label for="senha">Nova senha (opcional - mínimo de 6 caracteres)</label>
                <input type="password" name="senha" class="form-control" id="senha" placeholder="Digite a senha ">
            </div>
            <div class="form-group col-sm-6">
                <label for="senha_confirmation">Confirmar nova senha (mínimo de 6 caracteres)</label>
                <input type="password" name="senha_confirmation" class="form-control" id="senha_confirmation" placeholder="Digite a senha">
            </div>
        @endif
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
        });
    </script>
@stop