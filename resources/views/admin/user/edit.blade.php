@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_User == null ? 'Cadastro' : 'Edição' }} de usuário
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/usuarios'))) !!}
        @if($p_User != null)
            <input type="hidden" name="id" value="{{$p_User->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="usuario[name]" class="form-control" id="name" placeholder="Digite Aqui" value="{{$p_User == null ? '' : $p_User->name}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="email">Email<span class="mandatory-field">*</span></label>
            <input type="email" name="usuario[email]" class="form-control" id="email" placeholder="Digite Aqui" value="{{$p_User == null ? '' : $p_User->email}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="user_type_id">Tipo de usuário<span class="mandatory-field">*</span></label>
            {!! Form::select('usuario[user_type_id]', $p_UserTypes, $p_User == null ? '' : $p_User->user_type_id, ['id' => 'user_type_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6 circuit-user-fields" style="display: none;">
            <label for="touristic_circuit_id">Região Turística<span class="mandatory-field">*</span></label>
            {!! Form::select('usuario[touristic_circuit_id]', $p_Circuits, $p_User == null ? '' : $p_User->touristic_circuit_id, ['id' => 'touristic_circuit_id', 'class' => 'form-control select2 mandatory', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-6 city-user-fields" style="display: none;">
            <label for="city_id">Município<span class="mandatory-field">*</span></label>
            <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
            {!! Form::select('usuario[city_id]', $v_CityOptions, $p_User == null ? '' : $p_User->city_id, ['id' => 'city_id', 'class' => 'form-control select2 mandatory', 'style' => 'width: 100%']) !!}
        </div>
        {{--<div class="form-group col-sm-6">--}}
            {{--<label for="attraction_id">Trade</label>--}}
            {{--{!! Form::select('usuario[attraction_id]', $p_Trades, $p_User == null ? '' : $p_User->attraction_id, array('id' => 'attraction_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required')) !!}--}}
        {{--</div>--}}
        <div class="col-sm-12"></div>
        @if($p_User == null)
            <div class="form-group col-sm-6">
                <label for="senha">Senha (mínimo de 6 caracteres)<span class="mandatory-field">*</span></label>
                <input type="password" name="senha" class="form-control" id="senha" placeholder="Digite a senha" required>
            </div>
            <div class="form-group col-sm-6">
                <label for="senha_confirmation">Confirmar senha (mínimo de 6 caracteres)<span class="mandatory-field">*</span></label>
                <input type="password" name="senha_confirmation" class="form-control" id="senha_confirmation" placeholder="Digite a senha" required>
            </div>
        @else
            <div class="form-group col-sm-6">
                <label for="senha">Nova senha (opcional - mínimo de 6 caracteres)</label>
                <input type="password" name="senha" class="form-control" id="senha" placeholder="Digite a senha ">
            </div>
            <div class="form-group col-sm-6">
                <label for="senha_confirmation">Confirmar nova senha (mínimo de 6 caracteres)</label>
                <input type="password" name="senha_confirmation" class="form-control" id="senha_confirmation" placeholder="Digite a senha">
            </div>
        @endif
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $('#user_type_id').change(function(){
                $('.city-user-fields, .circuit-user-fields').hide();
                $('.city-user-fields .mandatory, .circuit-user-fields .mandatory').removeAttr('required');
                if(this.value == '4') {
                    $('.city-user-fields').show();
                    $('.city-user-fields .mandatory').attr('required', true);
                    $('.circuit-user-fields select').val('').change();
                }
                else if(this.value == '5') {
                    $('.circuit-user-fields').show();
                    $('.circuit-user-fields .mandatory').attr('required', true);
                    $('.city-user-fields select').val('').change();
                }
                else {
                    $('.city-user-fields select, .circuit-user-fields select').val('').change();
                }
            }).change();
        });
    </script>
@stop