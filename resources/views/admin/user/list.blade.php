@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
@stop
@section('panel-header')
    Usuários
    <a href="{{ url('admin/usuarios/editar') }}">
        <button class="btn btn-success pull-right" title="Novo usuário">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Região Turística</th>
        <th>Município</th>
        <th>Nome</th>
        <th>Email</th>
        <th>Tipo</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td>{!! Form::select('', ['' => 'Selecione'] + $p_UserTypes, null, ['class' => 'form-control']) !!}</td>
        <td>{!! Form::select('', ['' => 'Selecione', 1 => 'Ativo', 0 => 'Desativado'], null, ['class' => 'form-control']) !!}</td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/usuarios')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 2, "desc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop