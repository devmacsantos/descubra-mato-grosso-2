@extends('admin.mainTabs')
@section('pageCSS')
    {{--<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />--}}
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css')}}">
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Attraction == null ? 'Cadastro' : 'Edição' }} - {{ \App\Http\Controllers\AttractionController::$m_InventoryItemDescriptions[$p_InventoryItem] }}
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('admin/cidades/' . $p_City->id . '/atracoes/' . $p_InventoryItem), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
    <div class="tab-content pn br-n">
        @if($p_Attraction != null)
            <input type="hidden" name="id" value="{{$p_Attraction->id}}">
        @endif
        <input type="hidden" name="atracao[trade]" id="trade" value="1">

        <?php $v_Languages = [$p_Portuguese, $p_English, $p_Spanish, $p_French] ?>

        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                @if($c_Index == 0)
                    <div class="row">
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="district_id">Distrito</label>
                            {!! Form::select('atracao[district_id]', $p_Districts, $p_Attraction == null ? '' : $p_Attraction->district_id, array('id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%')) !!}
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="type_id">Tipo<span class="mandatory-field">*</span></label>
                            {!! Form::select('atracao[type_id]', $p_Types, $p_Attraction == null ? '' : $p_Attraction->type_id, array('id' => 'type_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%')) !!}
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="sub_type_id">Subtipo</label>
                            {!! Form::select('atracao[sub_type_id]', [($p_Attraction == null ? '' : $p_Attraction->sub_type_id) => ''], $p_Attraction == null ? '' : $p_Attraction->sub_type_id, array('id' => 'sub_type_id', 'class' => 'form-control select2', 'style' => 'width: 100%')) !!}
                        </div>
                        <div class="form-group col-sm-6 hidden-xs"></div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="nome">Nome<span class="mandatory-field">*</span></label>
                            <input type="text" name="atracao[nome]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->nome}}" required>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="cnpj">CNPJ</label>
                            <input type="text" class="form-control cnpj-field" name="cnpj-aux" id="cnpj" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->cnpj}}">
                            <input type="hidden" name="atracao[cnpj]" value="">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="endereco">Endereço<span class="mandatory-field">*</span></label>
                            <input type="text" name="atracao[endereco]" class="form-control" id="endereco" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->endereco}}" required>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="cep">CEP<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->cep}}" required>
                            <input type="hidden" name="atracao[cep]" value="">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="latitude" >Latitude <i>(formato decimal)</i></label>
                            <input type="number" name="atracao[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->latitude}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="longitude">Longitude <i>(formato decimal)</i></label>
                            <input type="number" name="atracao[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->longitude}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="site">Site</label>
                            <input type="url"  name="atracao[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->site}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
                            <input type="text" name="atracao[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->telefone}}" required>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="email">Email</label>
                            <input type="email" name="atracao[email]" class="form-control" id="email" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->email}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="facebook">Facebook</label>
                            <input type="url"  name="atracao[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->facebook}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="instagram">Instagram</label>
                            <input type="url"  name="atracao[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->instagram}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="twitter">Twitter</label>
                            <input type="url"  name="atracao[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->twitter}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="youtube">Youtube</label>
                            <input type="url"  name="atracao[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->youtube}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="flickr">Flickr</label>
                            <input type="url"  name="atracao[flickr]" class="form-control" id="flickr" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->flickr}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="tripadvisor_url">Url do Tripadvisor</label>
                            <input type="url"  name="atracao[tripadvisor_url]" class="form-control" id="tripadvisor_url" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->tripadvisor_url}}">
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="preco">Preço</label>
                            {!! Form::select('atracao[preco]', \App\Http\Controllers\BaseController::$m_Prices, $p_Attraction == null ? '' : $p_Attraction->preco, array('id' => 'sub_type_id', 'class' => 'form-control', 'style' => 'width: 100%')) !!}
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="atracao">Atração do site?</label>
                            <p><input type="checkbox" value="1" name="atracao[atracao]" id="atracao" class="ml5 mt10" {{($p_Attraction == null || $p_Attraction->atracao == 0) ? '' : 'checked'}}></p>
                        </div>
                    </div>

                    <div class="row attraction-fields hidden">
                        <div class="col-sm-12 col-xs-12 fixed-photo-div">
                            <div class="camera" style="{{$p_CoverPhoto == null ? '' : 'background-image:url(' . $p_CoverPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}">Foto de<br>capa</h4>
                                <input class="photo-upload-input {{$p_CoverPhoto == null ? 'mandatory' : ''}}" name="photo[file][]" type="file" accept="image/*" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="cover">
                                <input type="hidden" name="photo[id][]" value="{{$p_CoverPhoto == null ? '' : $p_CoverPhoto->id}}">
                            </div>
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                            <label for="destaque">Destaque?</label>
                            <p><input type="checkbox" value="1" name="atracao[destaque]" id="destaque" class="ml5 mt10" {{($p_Attraction == null || $p_Attraction->destaque == 0) ? '' : 'checked'}}></p>
                        </div>
                    </div>
                @endif

                <div class="row attraction-fields hidden">
                    <input type="hidden" name="idioma[id][]" value="{{$c_Language == null ? '' : $c_Language->id}}">
                    <div class="form-group col-sm-12 col-xs-12">
                        <label for="descricao_curta{{$c_Index}}">Descrição curta</label>
                        <textarea name="idioma[descricao_curta][]" id="descricao_curta{{$c_Index}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language->descricao_curta}}</textarea>
                    </div>
                    <div class="form-group col-sm-12 col-xs-12">
                        <label for="descricao{{$c_Index}}">Descrição<span class="mandatory-field">*</span></label>
                        <textarea name="idioma[descricao][]" id="descricao{{$c_Index}}" class="form-control {{ $c_Index == 0 ? 'mandatory' : '' }}" rows="4" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language->descricao}}</textarea>
                    </div>
                    @for($c_Counter = 1; $c_Counter <= 4; $c_Counter++)
                        <div class="form-group col-sm-12 col-xs-12">
                            <label for="dado_relevante_{{ $c_Counter . $c_Index}}">Dado relevante {{ $c_Counter }}</label>
                            <input type="text" name="idioma[dado_relevante_{{ $c_Counter }}][]" class="form-control" id="dado_relevante_{{ $c_Counter . $c_Index}}" placeholder="Digite Aqui" value="{{$c_Language == null ? '' : $c_Language['dado_relevante_' . $c_Counter]}}">
                        </div>
                        <div class="form-group col-sm-12 col-xs-12">
                            <label for="{{'dado_relevante_descricao_' . $c_Counter . $c_Index}}">Dado relevante {{ $c_Counter }} - Descrição</label>
                            <textarea name="idioma[dado_relevante_descricao_{{ $c_Counter}}][]" id="dado_relevante_descricao_{{ $c_Counter . $c_Index}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$c_Language == null ? '' : $c_Language['dado_relevante_descricao_' . $c_Counter]}}</textarea>
                        </div>
                    @endfor
                    <input type="hidden" name="idioma[language][]" value="{{ \App\Http\Controllers\PagesConfigController::$m_Languages[$c_Index] }}">

                    @if($c_Index == 0)
                        <div class="form-group col-sm-12 col-xs-12">
                            <label for="tipos_viagem">Tipo de polo <i>(permite mais de uma opção)</i></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('tipos_viagem[]', $p_TripTypes, $p_SelectedTripTypes, ['id' => 'tipos_viagem', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>

                        <div class="form-group col-sm-12 col-xs-12">
                            <label for="hashtags">Palavras-chave<span class="mandatory-field">*</span></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('hashtags[]', $p_Hashtags, array_keys($p_Hashtags), ['id' => 'hashtags', 'class' => 'form-control select2-custom mandatory', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>

                        <div class="col-sm-12 col-xs-12"></div>
                        @include('admin.util.photos', array('p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => false))
                    @endif
                </div>
            </div>
        @endforeach

        <div class="form-group col-sm-12 col-xs-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.headerIdentificationScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        var v_DateIndex = 2;
        var v_DateHeight = 0;
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });

            $('.cep-field').mask('99.999-999');

            $('#trade').change(function(){
                if($(this).val() == 1)
                {
                    $('.attraction-fields').removeClass('hidden');
                    $('.attraction-fields .mandatory').attr('required', true);
                    v_PhotoHeight = $('.photo-div:last').height();
                    $('#add-photo-column').height(v_PhotoHeight);
                }
                else
                {
                    $('.attraction-fields').addClass('hidden');
                    $('.attraction-fields .mandatory').removeAttr('required');
                }
            }).trigger('change');
        });

        function submitForm()
        {
            $('.cep-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            return true;
        }

    </script>
@stop