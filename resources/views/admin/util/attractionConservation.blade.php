<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Estado de conservação/preservação do atrativo</h3>
</div>
<input type="hidden" id="conservacao" name="formulario[conservacao]" value="{{$p_Form == null ? '' : $p_Form->conservacao}}">
<div class="conservacao-fields">
    <?php
        $v_ConservationStates = [
                '' => '',
                'Deteriorado' => 'Deteriorado',
                'Ruim' => 'Ruim',
                'Regular' => 'Regular',
                'Bom' => 'Bom',
                'Muito bom' => 'Muito bom'
        ];
        $v_OptionalConservationStates = [
                '' => '',
                'Não se aplica' => 'Não se aplica',
                'Deteriorado' => 'Deteriorado',
                'Ruim' => 'Ruim',
                'Regular' => 'Regular',
                'Bom' => 'Bom',
                'Muito bom' => 'Muito bom'
        ];
        $v_SinalizationStates = [
                '' => '',
                'Não sinalizado' => 'Não sinalizado',
                'Mal sinalizado' => 'Mal sinalizado',
                'Bem sinalizado' => 'Bem sinalizado'
        ];
        $v_ConservationFields = [
                'Geral',
                'Cobertura',
                'Interior',
                'Condição higiênica',
                'Estrutura'
        ];
    ?>
    @foreach($v_ConservationFields as $c_Index => $c_Field)
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}
            @if(!isset($p_NaturalAttraction) || $c_Index == 0)
                <span class="mandatory-field">*</span>
            @endif
        </label>
        @if(isset($p_CulturalAttraction) && ($c_Index >= 1 && $c_Index < 3))
        {!! Form::select('', $v_OptionalConservationStates, null, ['rel' => $c_Field, 'class' => 'form-control', 'required', 'style' => 'width: 100%']) !!}
        @else
        {!! Form::select('', $v_ConservationStates, null, ['rel' => $c_Field, 'class' => 'form-control', (!isset($p_NaturalAttraction) || $c_Index == 0) ? 'required' : '', 'style' => 'width: 100%']) !!}
        @endif
    </div>
    @endforeach
    <div class="form-group col-sm-6">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Sinalização turística<span class="mandatory-field">*</span></label>
        {!! Form::select('', $v_SinalizationStates, null, ['rel' => 'Sinalização turística', 'class' => 'form-control field', 'required' => 'required', 'style' => 'width: 100%']) !!}
    </div>
    <div class="form-group col-sm-12">
        <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Observação</label>
        <textarea rel="Observação" class="form-control" rows="4" placeholder="Digite Aqui"></textarea>
    </div>
</div>