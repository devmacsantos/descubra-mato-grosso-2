<link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css')}}">
<style>
    .search-cnpj-cadastur{
        vertical-align: top;
        float: right;
    }
    .cnpj-field-with-cadastur {
        width: calc(100% - 45px);
        display:inline-block;
    }
</style>

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
</div>
<div class="form-group col-sm-6">
    <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
    <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
    {!! Form::select('formulario[city_id]', $v_CityOptions, $p_Form == null ? '' : $p_Form->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
</div>
<div class="form-group col-sm-6">
    <label for="district_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distrito</label>
    {!! Form::select('formulario[district_id]', [($p_Form == null ? '' : $p_Form->district_id) => ''], $p_Form == null ? '' : $p_Form->district_id, ['id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
</div>
<div class="form-group col-sm-6">
    <label for="type_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo<span class="mandatory-field">*</span></label>
    {!! Form::select('formulario[type_id]', $p_Types, $p_Form == null ? '' : $p_Form->type_id, array('id' => 'type_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%')) !!}
</div>
@if(!isset($p_WithoutSubtype))
<div class="form-group col-sm-6">
    <label for="sub_type_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Subtipo</label>
    {!! Form::select('formulario[sub_type_id]', [($p_Form == null ? '' : $p_Form->sub_type_id) => ''], $p_Form == null ? '' : $p_Form->sub_type_id, array('id' => 'sub_type_id', 'class' => 'form-control select2', 'style' => 'width: 100%')) !!}
</div>
@endif
<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Nome/Entidade</h3>
</div>
@if(isset($p_CadasturActivityOptions))
    <input type="hidden" name="formulario[tipo_atividade_cadastur]" id="tipo_atividade_cadastur" value="{{$p_Form == null ? '' : $p_Form->tipo_atividade_cadastur}}">
@else
    <input type="hidden" name="formulario[tipo_atividade_cadastur]" id="tipo_atividade_cadastur" value="{{$p_CadasturActivity}}">
@endif
<div class="form-group col-sm-6">
    <label for="cnpj" style="width: 100%">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CNPJ</label>
    <input type="text" class="form-control cnpj-field cnpj-field-with-cadastur" id="cnpj" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->cnpj}}">
    <input type="hidden" name="formulario[cnpj]" value="">

    <a title="Pesquisar na base de dados da CADASTUR" type="button" id="searchCnpjCadasturBtn" class="btn btn-success search-cnpj-cadastur" onclick="loadCadasturData()">
        <i class="fa fa-search"></i>
        <img style="display:none;margin:auto;height:13px" src="{{url('/assets/img/loading.gif')}}">
    </a>
</div>
<div class="form-group col-sm-6">
    <label for="nome_fantasia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome fantasia/comercial<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[nome_fantasia]" class="form-control" id="nome_fantasia" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->nome_fantasia}}" required>
</div>
<div class="form-group col-sm-6">
    <label for="nome_juridico">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome jurídico</label>
    <input type="text" name="formulario[nome_juridico]" class="form-control" id="nome_juridico" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->nome_juridico}}">
</div>
@if(!isset($p_WithoutNetwork))
<div class="form-group col-sm-6">
    <label for="nome_rede">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome da rede{{isset($p_Holding) ? '/Holding' : ''}}</label>
    <input type="text" name="formulario[nome_rede]" class="form-control" id="nome_rede" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->nome_rede}}">
</div>
@endif
<div class="form-group col-sm-6">
    <label for="inicio_atividade">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Início da atividade</label>
    <input type="text" name="formulario[inicio_atividade]" class="form-control" id="inicio_atividade" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->inicio_atividade}}">
</div>
<div class="form-group col-sm-6">
    <label for="registro_cadastur">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de registro do CADASTUR</label>
    <input type="text" name="formulario[registro_cadastur]" class="form-control" id="registro_cadastur" value="{{$p_Form == null ? '' : $p_Form->registro_cadastur}}" readonly>
</div>