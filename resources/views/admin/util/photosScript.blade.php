<script type="text/javascript" src="{{url('/assets/js/jquery.popconfirm.js')}}"></script>
<script>
    var MAX_FILE_SIZE = 1024*1024*5; //5MB

    var v_PhotoIndex = parseInt($('.photo-div:last').find('input[name="photo[id][]"]').val()) + 1;
    if(!v_PhotoIndex)
        v_PhotoIndex = 1;

    $(document).ready(function()
    {
        @if($p_MaxPhotoCount != -1)
            if($('.photo-div').length >= '{{ $p_MaxPhotoCount }}')
                $('#add-photo-column').addClass('hidden');
        @endif

        $('.remove-photo').popConfirm({
            title: "Confirmar exclusão",
            content: "Tem certeza que deseja remover essa imagem?",
            placement: "left",
            yesBtn: "Sim",
            noBtn: "Não"
        });
    });

    function onChangeImage(p_ImageInput)
    {
        v_PhotoExists = $(p_ImageInput).parent().find('input[name="photo[id][]"]').val() != '';
        v_CancelClicked = (p_ImageInput.value == '');

        if(v_CancelClicked) {
            if(!v_PhotoExists)
                setPhotoBackground(p_ImageInput, '');
        }
        else if(p_ImageInput.files[0].size > MAX_FILE_SIZE) {
            alert('Tamanho máximo de 5MB excedido.');
            if(!v_PhotoExists)
                setPhotoBackground(p_ImageInput, '');
            $(p_ImageInput).val('');
            return false;
        }
        else {
            if($(p_ImageInput).parent().is($('#add-photo-column')))
                addPhoto(p_ImageInput);

            var reader = new FileReader();
            reader.onload = function(e) {
                $(p_ImageInput).parent().find('.download-photo-btn').remove();
                setPhotoBackground(p_ImageInput, 'url(' + e.target.result + ')');
            };
            reader.readAsDataURL(p_ImageInput.files[0]);
            v_PhotoIndex ++;
        }
    }

    function setPhotoBackground(p_ImageInput, p_Background)
    {
        $(p_ImageInput).parent().css('background-image', p_Background);
        $(p_ImageInput).parent().find('.upload-btn-icon').css('display', (p_Background == '' ? 'block' : 'none'));
        $(p_ImageInput).parent().find('.upload-btn-text').css('display', (p_Background == '' ? 'block' : 'none'));
    }

    var v_PhotoHeight = $('.photo-div:last').height();
    $('#add-photo-column').height(v_PhotoHeight);

    function addPhoto(p_ImageInput)
    {
        var v_Div = ('<div class="col-sm-4 photo-div">' +
                '<div class="camera">' +
                '<img class="upload-btn-icon" src="{{url('/assets/img/camera.png')}}" alt="">' +
                '<h4 class="upload-btn-text">Foto de<br>divulgação</h4>' +
                '<input type="hidden" name="photo[type][]" value="gallery">' +
                '<input type="hidden" name="photo[id][]" value="">' +
                '<a title="Excluir" type="button" class="btn btn-success remove-photo delete-btn" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>' +
                '</div>' +
                '</div>'
        );
        $('.photo-div:last').after(v_Div);
        $('.photo-div:last').find('.upload-btn-text').after(p_ImageInput);
        $('#newPhotoBtn').after('<input class="photo-upload-input" name="photo[file][]" id="" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">');

        @if($p_MaxPhotoCount != -1)
            if($('.photo-div').length >= '{{ $p_MaxPhotoCount }}')
                $('#add-photo-column').addClass('hidden');
        @endif

        $('.delete-btn').removeClass('delete-btn').popConfirm({
            title: "Confirmar exclusão",
            content: "Tem certeza que deseja remover essa imagem?",
            placement: "left",
            yesBtn: "Sim",
            noBtn: "Não"
        });
    }

    function removePhoto(p_DeleteBtn)
    {
        var v_DeleteDiv = $(p_DeleteBtn).closest('div[class*="photo-div"]');
        var v_PhotoId = v_DeleteDiv.find('input[name="photo[id][]"]').val();
        if (v_PhotoId != '')
            $('div[class*="photo-div"]:first').before('<input name="delete_photo[]" type="hidden" value="' + v_PhotoId + '">');
        if (!(($('.photo-div:first').is(v_DeleteDiv) && $('.photo-div:last').is(v_DeleteDiv)) || $(v_DeleteDiv).hasClass('fixed-photo-div')))
        {
            var v_PopConfirmModalId = $(p_DeleteBtn).attr('aria-describedby');
            $('#'+v_PopConfirmModalId).remove();
            v_DeleteDiv.remove();
        }
        else
        {
            v_DeleteDiv.find('input[name="photo[id][]"]').val('');
            v_DeleteDiv.find('.download-photo-btn').remove();
//                v_DeleteDiv.find('input[name="photo[credits][]"]').val('');
            var v_ImageInput = v_DeleteDiv.find('.photo-upload-input');
            $(v_ImageInput).replaceWith(v_ImageInput = $(v_ImageInput).clone(true));
            setPhotoBackground(v_ImageInput, '');
        }

        @if($p_MaxPhotoCount != -1)
            if($('.photo-div').length < '{{ $p_MaxPhotoCount }}')
                $('#add-photo-column').removeClass('hidden');
        @endif
    }

</script>

<script> 
        $("button[title='Visualizar']").on('click', function(event){
            event.preventDefault();
            $('#imgModal').css('display', 'block');
            $("#img01").attr("src", $(this).closest('.camera').css('background-image').replace(/^url\(\"/,'').replace(/\"\)/,''));
            $('#imgCaption').html('');
        });
         
        $("span.close").on('click', function(){
           $('#imgModal').css('display', 'none'); 
        });
</script>    