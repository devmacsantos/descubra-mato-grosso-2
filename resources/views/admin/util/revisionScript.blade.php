<script>
    $(document).ready(function()
    {
        @if(\App\UserType::isMunicipio())
        $('#city_id').change(function(){
            $.get("{{url('/admin/circuitoTuristicoMunicipio?city_id=')}}" + $(this).val(), function(){
            }).done(function(data){
                if (data.error == 'ok' && data.data != '')
                    $('#revisionStatusCityUser').val('2');
                else
                    $('#revisionStatusCityUser').val('3');
            }).error(function(){
            });
        }).change();
        @endif

        $('.revision-radiobuttons input').change(function(){
            if(this.value == 5)
                $('.revision-denied-comment').show();
            else
                $('.revision-denied-comment').hide();

            @if((\App\UserType::isMaster() || \App\UserType::isAdmin()))

            $('input[name="formulario[publicar]"]').prop('checked', false);
            if(this.value == 4) {
                $('#publicar-default').prop('checked', true);
                $('.revision-approved-actions').show();
            }
            else {
                $('.revision-approved-actions').hide();
            }
            @endif
        });

        if($('#revision_status_default_approved').val() == 4)
            $('.revision-approved-actions').show();
    });
</script>