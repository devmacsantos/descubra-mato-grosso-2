@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css')}}">
@endif

@if(!isset($p_NoTitle))
<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Funcionamento</h3>
</div>
@endif
@if(isset($p_ClosedFields) && (!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt')))
    <div class="form-group col-sm-6">
        <label for="equipamento_fechado">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Equipamento fechado?</label>
        <p><input type="checkbox" value="1" name="formulario[equipamento_fechado]" id="equipamento_fechado" class="ml5 mt10" {{($p_Form == null || $p_Form->equipamento_fechado == 0) ? '' : 'checked'}}></p>
    </div>
    <div class="form-group col-sm-6 mandatory closed-equipment-fields">
        <?php
        $v_EquipmentClosingReasons = [
            '' => '',
            'Fechado para reforma/obras' => 'Fechado para reforma/obras',
            'Interditado por órgão regulador' => 'Interditado por órgão regulador',
            'Conjuntura econômica' => 'Conjuntura econômica',
            'Problemas de manutenção' => 'Problemas de manutenção',
            'Fenômeno meteorológico' => 'Fenômeno meteorológico',
            'Fenômeno climatólogico' => 'Fenômeno climatólogico',
            'Infraestrutura de acesso danificada' => 'Infraestrutura de acesso danificada',
            'Outros' => 'Outros'
        ];
        ?>
        <label for="equipamento_fechado_motivo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Motivo do fechamento<span class="mandatory-field">*</span></label>
        {!! Form::select('formulario[equipamento_fechado_motivo]', $v_EquipmentClosingReasons, $p_Form == null ? '' : $p_Form->equipamento_fechado_motivo, ['id' => 'equipamento_fechado_motivo', 'class' => 'form-control field', 'style' => 'width: 100%']) !!}
    </div>
@endif
@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<div class="col-sm-12">
    <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Horário de funcionamento</h4>
</div>
<div class="col-sm-12 mb10">
    <input type="hidden" id="funcionamento_1" name="formulario[funcionamento_1]" value="{{$p_Form == null ? '' : $p_Form->funcionamento_1}}">
    <label>Período do ano<span class="mandatory-field">*</span></label>
    <div>
        <?php $v_Months = array_slice(\App\Http\Controllers\BaseController::$m_Months, 1); ?>
        @foreach($v_Months as $c_Month)
            <div class="pull-left mr10">
                <input type="checkbox" value="{{$c_Month}}" class="ml5 mt10 mes-funcionamento-1" {{$p_Form == null ? 'checked' : ''}}>
                <label>{{$c_Month}}</label>
            </div>
        @endforeach
    </div>
</div>
<div class="row"></div>
@foreach(\App\Http\Controllers\BaseController::$m_Days as $c_Day)
    <div class="dia-funcionamento-1" rel="{{$c_Day}}">
        <div class="form-group col-sm-1">
            <label class="mt25">{{$c_Day}}</label>
        </div>
        <div class="form-group col-sm-2">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}De<span class="mandatory-field">*</span></label>
            <input type="text" class="form-control time-field dia-funcionamento-de" placeholder="Digite Aqui" value="{{$p_Form == null ? '08:00' : ''}}" required>
        </div>
        <div class="form-group col-sm-2">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Até<span class="mandatory-field">*</span></label>
            <input type="text" class="form-control time-field dia-funcionamento-ate" placeholder="Digite Aqui" value="{{$p_Form == null ? '12:00' : ''}}" required>
        </div>
        <div class="form-group col-sm-2">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}De</label>
            <input type="text" class="form-control time-field dia-funcionamento-almoco-de" placeholder="Digite Aqui">
        </div>
        <div class="form-group col-sm-2">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Até</label>
            <input type="text" class="form-control time-field dia-funcionamento-almoco-ate" placeholder="Digite Aqui">
        </div>
        <div class="form-group col-sm-3">
            <input type="checkbox" value="1" class="dia-funcionamento-24-horas mr5 mt25">24 horas
            <input type="checkbox" value="1" class="dia-funcionamento-fechado m5 ml10 mt25">Fechado
            @if($c_Day == 'Segunda')
                <p class="mbn">
                    <input type="checkbox" value="1" class="funcionamento-1-aplicar-todos-dias mr5 mt10">Aplicar horário a todos os dias
                </p>
            @endif
        </div>
    </div>
    @if($c_Day != 'Feriados')
    <div class="col-sm-12"><hr class="mn mb5"></div>
    @endif
@endforeach

<div class="form-group col-sm-12">
    <label for="horario_funcionamento_adicional">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Preencher horário de funcionamento adicional?</label>
    <p><input type="checkbox" value="1" id="horario_funcionamento_adicional" class="ml5 mt10"></p>
</div>
<div class="horario-funcionamento-adicional" style="display: none">
    <div class="col-sm-12">
        <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Horário de funcionamento</h4>
    </div>
    <div class="col-sm-12 mb10">
        <input type="hidden" id="funcionamento_2" name="formulario[funcionamento_2]" value="{{$p_Form == null ? '' : $p_Form->funcionamento_2}}">
        <label>Período do ano</label>
        <div>
            <?php $v_Months = array_slice(\App\Http\Controllers\BaseController::$m_Months, 1); ?>
            @foreach($v_Months as $c_Month)
                <div class="pull-left mr10">
                    <input type="checkbox" value="{{$c_Month}}" class="ml5 mt10 mes-funcionamento-2">
                    <label>{{$c_Month}}</label>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row"></div>
    @foreach(\App\Http\Controllers\BaseController::$m_Days as $c_Day)
        <div class="dia-funcionamento-2" rel="{{$c_Day}}">
            <div class="form-group col-sm-1">
                <label class="mt25">{{$c_Day}}</label>
            </div>
            <div class="form-group col-sm-2">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}De</label>
                <input type="text" class="form-control time-field dia-funcionamento-de" placeholder="Digite Aqui">
            </div>
            <div class="form-group col-sm-2">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}Até</label>
                <input type="text" class="form-control time-field dia-funcionamento-ate" placeholder="Digite Aqui">
            </div>
            <div class="form-group col-sm-2">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}De</label>
                <input type="text" class="form-control time-field dia-funcionamento-almoco-de" placeholder="Digite Aqui">
            </div>
            <div class="form-group col-sm-2">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}Até</label>
                <input type="text" class="form-control time-field dia-funcionamento-almoco-ate" placeholder="Digite Aqui">
            </div>
            <div class="form-group col-sm-3">
                <input type="checkbox" value="1" class="dia-funcionamento-24-horas mr5 mt25">24 horas
                <input type="checkbox" value="1" class="dia-funcionamento-fechado m5 ml10 mt25">Fechado
                @if($c_Day == 'Segunda')
                    <p class="mbn">
                        <input type="checkbox" value="1" class="funcionamento-2-aplicar-todos-dias mr5 mt10">Aplicar horário a todos os dias
                    </p>
                @endif
            </div>
        </div>
        @if($c_Day != 'Feriados')
        <div class="col-sm-12"><hr class="mn mb5"></div>
        @endif
    @endforeach
</div>
@endif

@if(!isset($p_Translation))
    <div class="form-group col-sm-12">
        <label for="funcionamento_observacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Observação</label>
        <textarea name="formulario[funcionamento_observacao]" id="funcionamento_observacao" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $p_Form->funcionamento_observacao}}</textarea>
    </div>
@else
    <?php
        if($p_Form != null){
            $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
            $v_WorkingPeriodObs = $p_Form->funcionamento_observacao != null ? json_decode($p_Form->funcionamento_observacao,1) : $v_EmptyData;
        }
    ?>
    @if($p_TabLanguage == 'pt')
    <input type="hidden" id="funcionamento_observacao" name="formulario[funcionamento_observacao]">
    @endif
    <div class="form-group col-sm-12">
        <label for="funcionamento_observacao_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Observação</label>
        <textarea id="funcionamento_observacao_{{$p_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $v_WorkingPeriodObs[$p_TabLanguage]}}</textarea>
    </div>
@endif