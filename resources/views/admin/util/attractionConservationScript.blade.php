<script>
    $(document).ready(function()
    {
        carregaDadosConservacao();
    });

    function carregaDadosConservacao()
    {
        var v_Conservacao = $('#conservacao').val();
        if(v_Conservacao.length > 0)
        {
            v_Conservacao = JSON.parse(v_Conservacao);
            $(v_Conservacao).each(function(){
                $('.conservacao-fields .form-control[rel="'+this.nome+'"]').val(this.valor);
            });
        }
    }

    function processaDadosConservacao()
    {
        var v_Conservacao = [];
        $('.conservacao-fields .form-control').each(function(){
            v_Conservacao.push({
                nome:$(this).attr('rel'),
                valor:$(this).val()
            });
        });
        $('#conservacao').val(JSON.stringify(v_Conservacao));
    }
</script>