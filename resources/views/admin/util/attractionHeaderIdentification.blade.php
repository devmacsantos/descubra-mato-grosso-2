@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
    <div class="form-group col-sm-12">
        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
    </div>

    @if($p_Form != null)
        <input type="hidden" name="id" value="{{$p_Form->id}}">
    @endif
    <div class="form-group col-sm-6">
        <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
        <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
        {!! Form::select('formulario[city_id]', $v_CityOptions, $p_Form == null ? '' : $p_Form->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label for="district_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distrito</label>
        {!! Form::select('formulario[district_id]', [($p_Form == null ? '' : $p_Form->district_id) => ''], $p_Form == null ? '' : $p_Form->district_id, ['id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
    </div>
    <div class="form-group col-sm-6">
        <label for="type_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo<span class="mandatory-field">*</span></label>
        {!! Form::select('formulario[type_id]', $p_Types, $p_Form == null ? '' : $p_Form->type_id, array('id' => 'type_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%')) !!}
    </div>
    @if(!isset($p_WithoutSubtype))
    <div class="form-group col-sm-6">
        <label for="sub_type_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Subtipo</label>
        {!! Form::select('formulario[sub_type_id]', [($p_Form == null ? '' : $p_Form->sub_type_id) => ''], $p_Form == null ? '' : $p_Form->sub_type_id, array('id' => 'sub_type_id', 'class' => 'form-control select2', 'style' => 'width: 100%')) !!}
    </div>
    @endif
@endif
<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Nome/Entidade</h3>
</div>
@if(!isset($p_Translation))
    <div class="form-group col-sm-6">
        <label for="nome_popular">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome popular<span class="mandatory-field">*</span></label>
        <input type="text" name="formulario[nome_popular]" class="form-control" id="nome_popular" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->nome_popular}}" required>
    </div>
@else
    <?php
        if($p_Form != null){
            $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
            $v_Names = $p_Form->nome_popular != null ? json_decode($p_Form->nome_popular,1) : $v_EmptyData;
            $v_ReferencePoints = $p_Form->ponto_referencia != null ? json_decode($p_Form->ponto_referencia,1) : $v_EmptyData;
        }
    ?>
    @if($p_TabLanguage == 'pt')
    <input type="hidden" id="nome_popular" name="formulario[nome_popular]">
    @endif
    <div class="form-group col-sm-6">
        <label for="nome_popular_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome popular
            @if($p_TabLanguage == 'pt')
                <span class="mandatory-field">*</span>
            @endif
        </label>
        <input type="text" class="form-control" id="nome_popular_{{$p_TabLanguage}}" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $v_Names[$p_TabLanguage]}}"  {{$p_TabLanguage == 'pt' ? 'required' : ''}}>
    </div>
@endif

@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<div class="form-group col-sm-6">
    <label for="nome">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome oficial<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[nome_oficial]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->nome_oficial}}" required>
</div>
<div class="form-group col-sm-6">
    <label for="cnpj">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CNPJ</label>
    <input type="text" class="form-control cnpj-field" id="cnpj" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->cnpj}}">
    <input type="hidden" name="formulario[cnpj]" value="">
</div>
<div class="form-group col-sm-6">
    <label for="nome_organizacao_mantedora">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome da organização mantedora/gestora
        @if(!isset($p_OrganizationNotRequired))
        <span class="mandatory-field">*</span>
        @endif
    </label>
    <input type="text" name="formulario[nome_organizacao_mantedora]" class="form-control" id="nome_organizacao_mantedora" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->nome_organizacao_mantedora}}" {{isset($p_OrganizationNotRequired) ? '' : 'required'}}>
</div>
<div class="form-group col-sm-6">
    <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->telefone}}" required>
</div>
<div class="form-group col-sm-6">
    <label for="whatsapp">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Whatsapp</label>
    <input type="text" name="formulario[whatsapp]" class="form-control phone-field" id="whatsapp" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->whatsapp}}">
</div>
<div class="form-group col-sm-6">
    <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
    <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->site}}">
</div>
<div class="form-group col-sm-6">
    <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
    <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->email}}">
</div>

<div class="form-group col-sm-6">
    <label for="facebook">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Facebook</label>
    <input type="url"  name="formulario[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->facebook}}">
</div>
<div class="form-group col-sm-6">
    <label for="instagram">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instagram</label>
    <input type="url"  name="formulario[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->instagram}}">
</div>
<div class="form-group col-sm-6">
    <label for="twitter">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Twitter</label>
    <input type="url"  name="formulario[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->twitter}}">
</div>
<div class="form-group col-sm-6">
    <label for="youtube">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Youtube</label>
    <input type="url"  name="formulario[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->youtube}}">
</div>
<div class="form-group col-sm-6">
    <label for="tripadvisor">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tripadvisor</label>
    <input type="url"  name="formulario[tripadvisor]" class="form-control" id="tripadvisor" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->tripadvisor}}">
</div>
@endif

@if(!isset($p_Translation))
    <div class="form-group col-sm-12">
        <label for="descricao_atrativo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
        <input name="formulario[descricao_curta]" id="descricao_curta" type="text" class="form-control" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->descricao_curta}}">
    </div>
    <div class="form-group col-sm-12">
        <label for="descricao_atrativo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição do atrativo<span class="mandatory-field">*</span></label>
        <textarea name="formulario[descricao_atrativo]" id="descricao_atrativo" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_Form == null ? '' : $p_Form->descricao_atrativo}}</textarea>
    </div>
@else
    <?php
        if($p_Form != null){
            $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
            $v_ShortDescriptions = $p_Form->descricao_curta != null ? json_decode($p_Form->descricao_curta,1) : $v_EmptyData;
            $v_Descriptions = $p_Form->descricao_atrativo != null ? json_decode($p_Form->descricao_atrativo,1) : $v_EmptyData;
        }
    ?>
    @if($p_TabLanguage == 'pt')
    <input type="hidden" id="descricao_curta" name="formulario[descricao_curta]">
    @endif
    <div class="form-group col-sm-6">
        <label for="descricao_curta_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta
            @if($p_TabLanguage == 'pt')
                <span class="mandatory-field">*</span>
            @endif
        </label>
        <input type="text" class="form-control" id="descricao_curta_{{$p_TabLanguage}}" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $v_ShortDescriptions[$p_TabLanguage]}}" {{$p_TabLanguage == 'pt' ? 'required' : ''}}>
    </div>

    @if($p_TabLanguage == 'pt')
    <input type="hidden" id="descricao_atrativo" name="formulario[descricao_atrativo]">
    @endif
    <div class="form-group col-sm-12">
        <label for="descricao_atrativo_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição do atrativo
            @if($p_TabLanguage == 'pt')
                <span class="mandatory-field">*</span>
            @endif
        </label>
        <textarea id="descricao_atrativo_{{$p_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$p_TabLanguage == 'pt' ? 'required' : ''}}>{{$p_Form == null ? '' : $v_Descriptions[$p_TabLanguage]}}</textarea>
    </div>
@endif

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização e ambiência</h3>
</div>
@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
<div class="form-group col-sm-6">
    <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP
        @if(!isset($p_CulturalAttraction))
        <span class="mandatory-field">*</span>
        @endif
    </label>
    <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->cep}}" {{!isset($p_CulturalAttraction) ? 'required' : ''}}>
</div>
<div class="form-group col-sm-6">
    <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro
        @if(!isset($p_CulturalAttraction))
            <span class="mandatory-field">*</span>
        @endif
    </label>
    <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->bairro}}" {{!isset($p_CulturalAttraction) ? 'required' : ''}}>
</div>
<div class="form-group col-sm-6">
    <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
    <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->logradouro}}" required>
</div>
<div class="form-group col-sm-6">
    <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número
        @if(!isset($p_CulturalAttraction))
            <span class="mandatory-field">*</span>
        @endif
    </label>
    <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->numero}}" {{!isset($p_CulturalAttraction) ? 'required' : ''}}>
</div>
<div class="form-group col-sm-12">
    <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
    <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->complemento}}">
    <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
        <i class="fa fa-map-marker"></i>
    </a>
</div>
@endif

@if(!isset($p_Translation))
    <div class="form-group col-sm-12">
        <label for="ponto_referencia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Como Chegar/Referência de acesso<span class="mandatory-field">*</span></label>
        <textarea name="formulario[ponto_referencia]" id="ponto_referencia" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_Form == null ? '' : $p_Form->ponto_referencia}}</textarea>
    </div>
@else
    @if($p_TabLanguage == 'pt')
    <input type="hidden" id="ponto_referencia" name="formulario[ponto_referencia]">
    @endif
    <div class="form-group col-sm-12">
        <label for="ponto_referencia_{{$p_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Como Chegar/Referência de acesso
            @if($p_TabLanguage == 'pt')
                <span class="mandatory-field">*</span>
            @endif
        </label>
        <textarea id="ponto_referencia_{{$p_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$p_TabLanguage == 'pt' ? 'required' : ''}}>{{$p_Form == null ? '' : $v_ReferencePoints[$p_TabLanguage]}}</textarea>
    </div>
@endif

@if(!isset($p_Translation) || (isset($p_TabLanguage) && $p_TabLanguage == 'pt'))
    <div class="form-group col-sm-12">
        <?php
            $v_Locations = [
                    ''=>'',
                    'Urbana'=>'Urbana',
                    'Rururbana'=>'Rururbana',
                    'Rural'=>'Rural'
            ];
        ?>
        <label for="localizacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Localização<span class="mandatory-field">*</span></label>
        {!! Form::select('formulario[localizacao]', $v_Locations, $p_Form == null ? '' : $p_Form->localizacao, ['id' => 'localizacao', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
    <div class="form-group col-sm-12">
        <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
        <div id="map_canvas" style="height:320px;"></div>
    </div>
    <div class="form-group col-sm-6">
        <label for="latitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
        <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->latitude}}">
    </div>
    <div class="form-group col-sm-6">
        <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
        <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_Form == null ? '' : $p_Form->longitude}}">
    </div>
    <div class="form-group col-sm-12">
        <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
        <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
    </div>
    
@endif