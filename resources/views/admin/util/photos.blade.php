<style type="text/css">
    .add-photo{
        height: 39px;
        width: 39px;
    }
    #add-photo-column{
        display: flex;
        align-items: center;
        justify-content: center;
    }
</style>
<style type="text/css">

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 9000; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }
        
        /* Modal Content (Image) */
        .modal-content {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
        }
        
        /* Caption of Modal Image (Image Text) - Same Width as the Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }
        
        /* Add Animation - Zoom in the Modal */
        .modal-content, #caption { 
            animation-name: zoom;
            animation-duration: 0.6s;
        }
        
        @keyframes zoom {
            from {transform:scale(0)} 
            to {transform:scale(1)}
        }
        
        /* The Close Button */
        .close {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }
        
        .close:hover,
        .close:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }
        
        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
            .modal-content {
                width: 100%;
            }
        }        
</style>

                        
<!-- The Modal -->
<div id="imgModal" class="modal">
    
    <!-- The Close Button -->
    <span class="close">&times;</span>
    
    <!-- Modal Content (The Image) -->
    <img class="modal-content" id="img01">
    
    <!-- Modal Caption (Image Text) -->
    <div id="imgCaption"></div>
</div>  

<div class="col-sm-12">
    <h3>{{isset($p_WithoutFieldIndex) ? '' : \App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Fotos promocionais</h3>
</div>
@if($p_Cover == true)
<div class="form-group col-sm-12">
    <h4>{{isset($p_WithoutFieldIndex) ? '' : \App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Foto de capa</h4>
</div>
<div class="col-sm-12 fixed-photo-div">
    <div class="camera" style="{{$p_CoverPhoto == null ? '' : 'background-image:url(' . $p_CoverPhoto->url . ')!important;'}}">
        <img class="upload-btn-icon" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
        <h4 class="upload-btn-text" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}">Foto de<br>capa</h4>
        <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
        <input type="hidden" name="photo[type][]" value="cover">
        <input type="hidden" name="photo[id][]" value="{{$p_CoverPhoto == null ? '' : $p_CoverPhoto->id}}">
        @if($p_CoverPhoto)
            <a title="Baixar" href="{{$p_CoverPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
            <button title="Visualizar" class="btn btn-success" download style="position: absolute; bottom: 3px; right: 3px;"><i class="fa fa-eye"></i></button>
        @endif
        <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
    </div>
    {{--<div class="form-group photo-credits-div">--}}
        {{--<label>Créditos</label>--}}
        {{--<input type="text" name="photo[credits][]" class="photo-credits form-control" placeholder="Digite Aqui" value="{{$p_CoverPhoto == null ? '' : $p_CoverPhoto->creditos}}">--}}
    {{--</div>--}}
</div>
@endif

@if(!isset($p_OnlyCover))
    <div class="form-group col-sm-12">
        <h4>{{isset($p_WithoutFieldIndex) ? '' : \App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Fotos de divugação</h4>
    </div>
    @if(count($p_PhotoGallery) == 0)
        <div class="col-sm-4 photo-div">
            <div class="camera">
                <img class="upload-btn-icon" src="{{url('/assets/img/camera.png')}}" alt="">
                <h4 class="upload-btn-text">Foto de<br>divulgação</h4>
                <input class="photo-upload-input" name="photo[file][]" id="" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                <input type="hidden" name="photo[type][]" value="gallery">
                <input type="hidden" name="photo[id][]" value="">
                <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
                <button title="Visualizar" class="btn btn-success" download style="position: absolute; bottom: 3px; right: 3px;"><i class="fa fa-eye"></i></button>
            </div>
            {{--<div class="form-group photo-credits-div">--}}
                {{--<label>Créditos</label>--}}
                {{--<input type="text" name="photo[credits][]" class="photo-credits form-control" placeholder="Digite Aqui" >--}}
            {{--</div>--}}
        </div>
    @else
        @foreach($p_PhotoGallery as $c_Index => $c_Photo)
            <div class="col-sm-4 photo-div">
                <div class="camera" style="{{'background-image:url(' . $c_Photo->url . ')!important;'}}">
                    <img class="upload-btn-icon" style="display:none" src="{{url('/assets/img/camera.png')}}" alt="">
                    <h4 class="upload-btn-text" style="display:none">Foto de<br>divulgação</h4>
                    <input class="photo-upload-input" name="photo[file][]" id="{{'photo' . $c_Index}}" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                    <input type="hidden" name="photo[type][]" value="gallery">
                    <input type="hidden" name="photo[id][]" value="{{$c_Photo->id}}">
                    @if($c_Photo)
                        <a title="Baixar" href="{{$c_Photo->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                        <button title="Visualizar" class="btn btn-success" download style="position: absolute; bottom: 3px; right: 3px;"><i class="fa fa-eye"></i></button>
                    @endif
                    <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhoto(this)"><i class="fa fa-trash-o"></i></a>
                </div>
                {{--<div class="form-group photo-credits-div">--}}
                    {{--<label>Créditos</label>--}}
                    {{--<input type="text" name="photo[credits][]" class="photo-credits form-control" placeholder="Digite Aqui" value="{{$c_Photo->creditos}}">--}}
                {{--</div>--}}
            </div>
        @endforeach
    @endif

    <div id="add-photo-column" class="col-sm-4 form-group">
        <a title="Adicionar foto" type="button" id="newPhotoBtn" class="btn btn-success add-photo" onclick="addPhoto(this)"><i class="fa fa-plus"></i></a>
        <input class="photo-upload-input" name="photo[file][]" id="" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
    </div> 
@endif