@extends('admin.util.listStatic', ['p_HasDateFilter' => false, 'p_HasDateTimeFilter' => true])
@section('list-css')
    <style>
        table .value-columns
        {
            max-width: 200px;
            overflow-wrap: break-word;
        }
    </style>
@stop
@section('panel-header')
    {{$p_Title}}
    @if(isset($p_Subtitle))
        <br>
        {{$p_Subtitle}}
    @endif
@stop
@section('list-table-head')
    <tr>
        <th>Data da alteração</th>
        <th>Campo</th>
        <th class="value-columns">Alterado de</th>
        <th class="value-columns">Alterado para</th>
        <th>Responsável</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_History as $c_History)
        <tr>
            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_History->created_at)->format('d/m/y - H:i')}}</td>
            <td>{{$c_History->fieldName()}}</td>
            <td class="value-columns">{{$c_History->oldValue()}}</td>
            <td class="value-columns">{{$c_History->newValue()}}</td>
            <td>{{$c_History->userResponsible()->name}}</td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "desc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true}
@stop
@section('list-js')
@stop