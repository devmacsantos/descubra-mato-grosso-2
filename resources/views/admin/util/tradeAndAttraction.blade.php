@if(\App\UserType::isMaster() || \App\UserType::isAdmin() || \App\UserType::isComunicacao())
    <div class="col-sm-12"></div>
    <div class="form-group col-sm-6">
        <label for="destaque">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Atração do site?</label>
        <p><input type="checkbox" value="1" name="formulario[atracao]" id="destaque" class="ml5 mt10" {{($p_Form == null || $p_Form->atracao == 0) ? '' : 'checked'}}></p>
    </div>
    <div class="form-group col-sm-6">
        <label for="destaque">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Atração de destaque?</label>
        <p><input type="checkbox" value="1" name="formulario[destaque]" id="destaque" class="ml5 mt10" {{($p_Form == null || $p_Form->destaque == 0) ? '' : 'checked'}}></p>
    </div>
@endif