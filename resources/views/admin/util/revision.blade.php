@if($p_Form != null && $p_Form->revision_status_id == 5)
    <div class="form-group col-sm-12">
        <label for="comentario_revisao">Alterações rejeitadas - Motivo:</label>
        <textarea class="form-control" rows="4" placeholder="Digite Aqui" readonly>{{$p_Form->comentario_revisao}}</textarea>
    </div>
@endif

<div class="col-sm-12 mt5"></div>

@if(\App\UserType::isTrade() || \App\UserType::isFiscalizador())
    <input type="hidden" name="formulario[revision_status_id]" id="revisionStatusCityUser" value="1">
@else
    @if($p_Form == null)
        @if(\App\UserType::isMunicipio())
            <input type="hidden" name="formulario[revision_status_id]" id="revisionStatusCityUser" value="2">
        @elseif(\App\UserType::isCircuito() || \App\UserType::isComunicacao())
            <input type="hidden" name="formulario[revision_status_id]" value="3">
        @elseif((\App\UserType::isMaster() || \App\UserType::isAdmin()))
            <input type="hidden" name="formulario[revision_status_id]" value="4">
            @if(isset($p_Publish))
                <div class="form-group col-sm-12">
                    <label for="publicar">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Publicar no portal?</label>
                    <p><input name="formulario[publicar]" type="checkbox" value="1" class="ml5 mt10"></p>
                </div>
            @endif
        @endif
    @else
        @if(\App\UserType::isMunicipio())
            @if($p_Form->revision_status_id == 1)
                <div class="col-sm-12 revision-radiobuttons mb15">
                    <p><label for="revisao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Revisão</label></p>
                    <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" id="revisionStatusCityUser" value="2" required>Aprovar alterações</label>
                    <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="5">Rejeitar alterações</label>
                </div>
            @else
                <input type="hidden" name="formulario[revision_status_id]" id="revisionStatusCityUser" value="2">
            @endif
        @elseif(\App\UserType::isCircuito())
            @if($p_Form->revision_status_id != 4)
                <div class="col-sm-12 revision-radiobuttons mb15">
                    <p><label for="revisao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Revisão</label></p>
                    <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="3" required>Aprovar alterações</label>
                    <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="5">Rejeitar alterações</label>
                </div>
            @else
                <input type="hidden" name="formulario[revision_status_id]" value="3">
            @endif
        @elseif(\App\UserType::isComunicacao() && $p_Form->revision_status_id == 4)
            <input type="hidden" name="formulario[revision_status_id]" value="3">
        @elseif(\App\UserType::isMaster() || \App\UserType::isAdmin())
            @if($p_Form->revision_status_id != 4)
                <div class="col-sm-12 revision-radiobuttons mb15">
                    <p><label for="revisao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Revisão</label></p>
                    <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="4" required>Aprovar alterações</label>
                    <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="5">Rejeitar alterações</label>
                </div>
            @else
                <input type="hidden" id="revision_status_default_approved" name="formulario[revision_status_id]" value="4">
            @endif
            @if(isset($p_Publish))
                @if($p_Form->data_publicacao == null)
                    <div class="col-sm-12 mb15 mt5 revision-approved-actions" style="display:none">
                        <label for="publicar">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Publicar no portal?</label>
                        <p><input name="formulario[publicar]" type="checkbox" value="1" class="ml5 mt10"></p>
                    </div>
                @else
                    <div class="col-sm-12 mb15 mt5 revision-approved-actions" style="display:none">
                        <p><label for="revisao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Publicação</label></p>
                        <label class="radio-inline"><input type="radio" id="publicar-default" name="formulario[publicar]" value="0" checked>Não alterar dados do portal</label>
                        <label class="radio-inline"><input type="radio" name="formulario[publicar]" value="1">Atualizar publicação do portal</label>
                        <label class="radio-inline"><input type="radio" name="formulario[publicar]" value="2">Remover publicação do portal</label>
                    </div>
                @endif
            @endif
        @endif
        <div class="form-group col-sm-12 revision-denied-comment" style="display:none">
            <label for="comentario_revisao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Motivo</label>
            <textarea name="formulario[comentario_revisao]" class="form-control" rows="4" placeholder="Digite Aqui"></textarea>
        </div>
        @if((\App\UserType::isMaster() || \App\UserType::isAdmin()) && isset($p_Publish) && $p_Form->data_publicacao != null)
            <div class="form-group col-sm-12 revision-denied-comment" style="display:none">
                <label for="formulario[publicar]">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Remover publicação do portal?</label>
                <p><input name="formulario[publicar]" type="checkbox" value="2" class="ml5 mt10"></p>
            </div>
        @endif
    @endif
@endif
