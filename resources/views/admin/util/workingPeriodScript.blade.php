<script type="text/javascript" src="{{url('/vendor/plugins/datepicker/js/bootstrap-datetimepicker.js')}}"></script>
<script>
    $(document).ready(function()
    {
        $('.time-field').mask('99:99')
        .datetimepicker({
            pickDate: false,
            format: 'HH:mm',
            icons: {
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down'
            }
        });

        carregaDadosFuncionamentos();

        if($('.mes-funcionamento-2:checked').length)
            $('#horario_funcionamento_adicional').prop("checked", true);

        $('#horario_funcionamento_adicional').change(function(){
            if($(this).is(':checked'))
                $('.horario-funcionamento-adicional').show();
            else{
                $('.horario-funcionamento-adicional').hide();
                $('.mes-funcionamento-2').prop("checked", false);
            }
        }).change();

        $('.dia-funcionamento-24-horas').change(function(){
            if($(this).is(':checked'))
            {
                var v_Parent = $(this).closest('div[class^="dia-funcionamento-"]');
                v_Parent.find('.dia-funcionamento-de').val('00:00').change();
                v_Parent.find('.dia-funcionamento-ate').val('23:59').change();
            }
        }).change();
        $('.dia-funcionamento-fechado').change(function(){
            if($(this).is(':checked'))
            {
                var v_Parent = $(this).closest('div[class^="dia-funcionamento-"]');
                v_Parent.find('.dia-funcionamento-de').val('00:00').change();
                v_Parent.find('.dia-funcionamento-ate').val('00:00').change();
                v_Parent.find('.dia-funcionamento-almoco-de').val('').change();
                v_Parent.find('.dia-funcionamento-almoco-ate').val('').change();
            }
        }).change();

        $('.funcionamento-1-aplicar-todos-dias, .funcionamento-2-aplicar-todos-dias').change(function(){
            if($(this).is(':checked'))
            {
                var v_Parent = $(this).closest('div[class^="dia-funcionamento-"]');
                var v_OtherDays = v_Parent.siblings('div[class^="dia-funcionamento-"]');

                v_OtherDays.find('.dia-funcionamento-24-horas').prop("checked", v_Parent.find('.dia-funcionamento-24-horas').is(':checked'));
                v_OtherDays.find('.dia-funcionamento-fechado').prop("checked", v_Parent.find('.dia-funcionamento-fechado').is(':checked'));
                v_OtherDays.find('.dia-funcionamento-de').val(v_Parent.find('.dia-funcionamento-de').val()).change();
                v_OtherDays.find('.dia-funcionamento-ate').val(v_Parent.find('.dia-funcionamento-ate').val()).change();
                v_OtherDays.find('.dia-funcionamento-almoco-de').val(v_Parent.find('.dia-funcionamento-almoco-de').val()).change();
                v_OtherDays.find('.dia-funcionamento-almoco-ate').val(v_Parent.find('.dia-funcionamento-almoco-ate').val()).change();
            }
        });
    });

    function carregaDadosFuncionamentos()
    {
        carregarDadosFuncionamento(1);
        carregarDadosFuncionamento(2);
    }

    function carregarDadosFuncionamento(p_Index)
    {
        var v_Funcionamento = $('#funcionamento_'+p_Index).val();
        if(v_Funcionamento.length > 0)
        {
            v_Funcionamento = JSON.parse(v_Funcionamento);
            $(v_Funcionamento.meses).each(function(){
                $('.mes-funcionamento-'+p_Index+'[value="'+this+'"]').prop("checked", true);
            });
            $(v_Funcionamento.dias).each(function(){
                var v_DiaDiv = $('.dia-funcionamento-'+p_Index+'[rel="'+this.dia+'"]');
                if(this.horarioDe.length)
                    v_DiaDiv.find('.dia-funcionamento-de').val(this.horarioDe).trigger('blur');
                if(this.horarioAte.length)
                    v_DiaDiv.find('.dia-funcionamento-ate').val(this.horarioAte).trigger('blur');
                if(this.almocoDe.length)
                    v_DiaDiv.find('.dia-funcionamento-almoco-de').val(this.almocoDe).trigger('blur');
                if(this.almocoAte.length)
                    v_DiaDiv.find('.dia-funcionamento-almoco-ate').val(this.almocoAte).trigger('blur');
                v_DiaDiv.find('.dia-funcionamento-24-horas').prop("checked", this.funciona24horas);
                v_DiaDiv.find('.dia-funcionamento-fechado').prop("checked", this.fechado);
            });
        }
    }

    function processaDadosFuncionamentos()
    {
        $('#funcionamento_1').val(JSON.stringify(coletarDadosFuncionamento(1)));
        $('#funcionamento_2').val(JSON.stringify(coletarDadosFuncionamento(2)));


        @if(isset($p_Translation))
            <?php $v_Languages = ['pt', 'en', 'es', 'fr']; ?>

            var v_WorkingPeriodObsJson = {};
            @foreach($v_Languages as $c_Language)
                v_WorkingPeriodObsJson.{{$c_Language}} = $('#funcionamento_observacao_{{$c_Language}}').val();
            @endforeach
            $('#funcionamento_observacao').val(JSON.stringify(v_WorkingPeriodObsJson));
        @endif
    }

    function coletarDadosFuncionamento(p_Index)
    {
        var v_Funcionamento = {meses:[],dias:[]};
        $('.mes-funcionamento-'+p_Index).each(function(){
            if($(this).is(':checked'))
                v_Funcionamento.meses.push($(this).val())
        });
        $('.dia-funcionamento-'+p_Index).each(function(){
            var v_Data = {
                dia:$(this).attr('rel'),
                horarioDe:processaStringHorario($(this).find('.dia-funcionamento-de').val()),
                horarioAte:processaStringHorario($(this).find('.dia-funcionamento-ate').val()),
                almocoDe:processaStringHorario($(this).find('.dia-funcionamento-almoco-de').val()),
                almocoAte:processaStringHorario($(this).find('.dia-funcionamento-almoco-ate').val()),
                funciona24horas:$(this).find('.dia-funcionamento-24-horas').is(':checked'),
                fechado:$(this).find('.dia-funcionamento-fechado').is(':checked')
            };
            v_Funcionamento.dias.push(v_Data)
        });
        return v_Funcionamento;
    }

    function processaStringHorario(p_String)
    {
        return p_String == '__:__' ? '' : p_String;
    }
</script>