<script>
    //Script integração CADASTUR


    var v_CadasturToken = '';

    var v_CadasturUrl = "{{\App\Parameter::getParameterByKey('cadastur_url')}}";
    var v_CadasturPwd = "{{\App\Parameter::getParameterByKey('cadastur_senha')}}";
    var v_CadasturTrAtiv = new Array();


    /*
     1-Acampamento Turístico
     2-Agência de Turismo
     3-Meio de Hospedagem
     4-Organizadora de Eventos
     5-Parque Temático
     6-Transportadora Turística
     7-Empreendimento de Apoio ao Turismo Náutico ou à Pesca Desportiva
     8-Casa de Espetáculos e Equipamento de Animação Turística
     9-Centro de Convenções
     10-Prestador Especializado em Segmentos Turísticos
     11-Prestador de Infraestrutura de Apoio para Eventos
     12-Locadora de Veículos para Turistas
     13-Parque Aquático e Empreendimento de Lazer
     14-Restaurante, Cafeteria, Bar e Similares
     */

    //Array para converter tipos de atividades cadastradas no BD para o cadastur 
    v_CadasturTrAtiv[10] = 2;
    v_CadasturTrAtiv[15] = 14;
    v_CadasturTrAtiv[20] = 3;
    v_CadasturTrAtiv[25] = 9;
    v_CadasturTrAtiv[30] = 6;
    v_CadasturTrAtiv[35] = 1;
    v_CadasturTrAtiv[40] = 11;
    v_CadasturTrAtiv[45] = 7;
    v_CadasturTrAtiv[55] = 8;
    v_CadasturTrAtiv[60] = 5;
    v_CadasturTrAtiv[65] = 13;
    v_CadasturTrAtiv[70] = 13;
    v_CadasturTrAtiv[75] = 10;
    v_CadasturTrAtiv[80] = 4;
    v_CadasturTrAtiv[85] = 12;



    function indicaVenctoCADASTUR() {
        try {

        //Espera-se que a data esteja no formado yyyy-mm-dd hh:mm:ss
        if ($("[name='formulario[validade_cadastur]']").val() != '') {

            //console.log($("[name='formulario[validade_cadastur]']").val());
                dataCadastur = $("[name='formulario[validade_cadastur]']").val().split(' ')[0].split('-');


            if (dataCadastur.length != 3) {
                dataCadastur = $("[name='formulario[validade_cadastur]']").val().split('/');
                data = new Date(dataCadastur[2], dataCadastur[1] - 1, dataCadastur[0]);
            } else {
                data = new Date(dataCadastur[0], dataCadastur[1] - 1, dataCadastur[2]);
            }

            if (data < new Date()) {
                $("[for='registro_cadastur']").html('2.6. Número de registro do CADASTUR | <small style="color:  red;"> Vencído em: ' + data.toLocaleDateString('pt-BR') + '</small>');
            } else {
                $("[for='registro_cadastur']").html('2.6. Número de registro do CADASTUR | <small style="color:  green;"> Válido até: ' + data.toLocaleDateString('pt-BR') + '</small>');
            }

        } else {
            $("[for='registro_cadastur']").html("2.6. Número de registro do CADASTUR");
        }

        } catch(err) {
            $("[for='registro_cadastur']").html("2.6. Número de registro do CADASTUR");
        }




        //
        //
//$("[for='registro_cadastur']").html();
        //"2.6. Número de registro do CADASTUR"
    }

    function loadCadasturData() {
        var v_CNPJ = $('#cnpj').val().replace(/[^\d]+/g, '');
        var v_CadasturTipoAtividade = $('#tipo_atividade_cadastur').val();

        //Pega valor padrão para o fomulário caso o crusamento dos tipos falhe
        if (!v_CadasturTipoAtividade) {
            v_CadasturTipoAtividade = $('#default_tipo_atividade_cadastur').val();
        }

        if (v_CNPJ.length && v_CadasturTipoAtividade.length) {
            $('#searchCnpjCadasturBtn i').hide();
            $('#searchCnpjCadasturBtn img').show();

            v_url = v_CadasturUrl.replace(/\s/g, '') + v_CadasturPwd.replace(/\s/g, '') + '/cnpj/' + v_CNPJ + '/tipoAtividade/' + v_CadasturTrAtiv[v_CadasturTipoAtividade];

            $.ajax({
                url: v_url,
                type: 'GET',
                success: function (data) {
                    $('#searchCnpjCadasturBtn i').show();
                    $('#searchCnpjCadasturBtn img').hide();

                    if (typeof data['cod'] == "undefined") {

                        //transforma data em array sando como referência /
                        sDate = data['validadeCertificado'].split("/");
                        //Cria data usando como parâmetro os dados do array
                        certDate = new Date(sDate[2], sDate[1] - 1, sDate[0]);

                        if ((data['situacaoAtividadeTuristica'] && data['situacaoAtividadeTuristica'] == 'Vencido') || (certDate < new Date())) {
                            alert('CNPJ possui um cadastro VENCIDO no CADASTUR. Para prosseguir preencha os dados manualmente. \n\nSaiba mais: www.cadastur.turismo.mg.gov.br');
                            $('input[name="formulario[tem_cadastur]"]').val(0);
                            $('input[name="formulario[validade_cadastur]"]').val('');
                            $('input[name="formulario[registro_cadastur]"]').val('');
                            indicaVenctoCADASTUR();
                            return 0;
                        }

                        if (data['nuAtividadePj'])
                            $('input[name="formulario[tem_cadastur]"]').val(1);
                        if (data['validadeCertificado'])
                            $('input[name="formulario[validade_cadastur]"]').val(data['validadeCertificado'].split('/')[2] + '-' + data['validadeCertificado'].split('/')[1] + '-' + data['validadeCertificado'].split('/')[0]);
                        if (data['nuAtividadePj'])
                            $('input[name="formulario[registro_cadastur]"]').val(data['nuAtividadePj']);
                        if (data['noFantasia'])
                            $('input[name="formulario[nome_fantasia]"]').val(data['noFantasia'].toLowerCase().trim());
                        if (data['noRazaoSocial'])
                            $('input[name="formulario[nome_juridico]"]').val(data['noRazaoSocial'].toLowerCase().trim());

                        //Trata data de abertura
                        if (data['dtAbertura'].indexOf('.') >= 0) {
                            data['dtAbertura'] = data['dtAbertura'].split('.')[2] + '/' + data['dtAbertura'].split('.')[1] + '/' + data['dtAbertura'].split('.')[0];
                        }

                        if (data['dtAbertura'])
                            $('input[name="formulario[inicio_atividade]"]').val(data['dtAbertura']);
                        if (data['noWebsite'])
                            $('input[name="formulario[site]"]').val(data['noWebsite'].toLowerCase().trim());
                        if (data['telefoneInstitucional'])
                            $('input[name="formulario[telefone]"]').val(data['telefoneInstitucional']).blur();
                        if (data['noEmail'])
                            $('input[name="formulario[email]"]').val(data['noEmail'].toLowerCase().trim());
                        if (data['coCep'])
                            $('input[name="formulario[cep]"]').val(data['coCep']).blur();
                        if (data['noBairro'])
                            $('input[name="formulario[bairro]"]').val(data['noBairro'].toLowerCase().trim());
                        if (data['deLogradouro'])
                            $('input[name="formulario[logradouro]"]').val(data['deLogradouro'].toLowerCase().trim());
                        //if (data['nuLogradouro']) $('input[name="formulario[numero]"]').val(data['nuLogradouro']);
                        if (data['nuQtdUnidHabitacionais'])
                            $('input[rel="Número de UHs - Total"]').val(data['nuQtdUnidHabitacionais']);
                        if (data['nuQtdLeitos'])
                            $('input[rel="Número de Leitos - Total"]').val(data['nuQtdLeitos']);
                        if (data['noFantasia'])
                            $('input[name="formulario[nome_fantasia]"]').val(data['noFantasia'].toLowerCase().trim());
                        if (data['nuUhsAcessiveis'])
                            $('input[rel="Número de UHs - UHs adaptadas para pessoas com deficiência"]').val(data['nuUhsAcessiveis']);

                    } else {
                        if (data['msg'] == "Prestador nao encontrado") {
                            alert('CNPJ não encontrado no CADASTUR. Preencha os dados manualmente para prosseguir. \n\nSaiba mais: www.cadastur.turismo.mg.gov.br');
                            $('input[name="formulario[tem_cadastur]"]').val(0);
                            $('input[name="formulario[validade_cadastur]"]').val('');
                            $('input[name="formulario[registro_cadastur]"]').val('');
                        } else {
                            alert("Não foi possível realizar essa operação. Preencha os dados manualmente, ou tente novamente mais tarde.");
                            $('input[name="formulario[tem_cadastur]"]').val(0);
                            $('input[name="formulario[validade_cadastur]"]').val('');
                            $('input[name="formulario[registro_cadastur]"]').val('');
                        }
                        console.log(v_url);
                        console.log(data['msg']);
                    }
                    indicaVenctoCADASTUR();
                },
                erro: function () {
                    alert("Não foi possível realizar essa operação. Preencha os dados manualmente, ou tente novamente mais tarde.");
                    $('#searchCnpjCadasturBtn i').show();
                    $('#searchCnpjCadasturBtn img').hide();
                    indicaVenctoCADASTUR();
                }
            });
        } else {
            $('input[name="formulario[tem_cadastur]"]').val(0);
            $('input[name="formulario[validade_cadastur]"]').val('');
            $('input[name="formulario[registro_cadastur]"]').val('');

            console.log(v_url);
            console.log(data['msg']);

        }
    }

    $('#cnpj').on('change', function () {
        var v_CNPJ = $('#cnpj').val().replace(/[^\d]+/g, '');
        if (v_CNPJ == '') {
            $('input[name="formulario[tem_cadastur]"]').val(0);
            $('input[name="formulario[validade_cadastur]"]').val('');
            $('input[name="formulario[registro_cadastur]"]').val('');
            indicaVenctoCADASTUR();
        } else {
            loadCadasturData();
        }

    });


    $(document).ready(function () {

        indicaVenctoCADASTUR();
        

    });
</script>