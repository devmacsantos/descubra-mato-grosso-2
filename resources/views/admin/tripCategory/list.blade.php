@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Tipo de polos
    <a href="{{ url('admin/categorias-viagem/editar') }}">
        <button class="btn btn-success pull-right" title="Novo clima">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_Categories as $c_Category)
        <tr>
            <td>{{$c_Category->nome_pt}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/categorias-viagem/editar/' . $c_Category->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop