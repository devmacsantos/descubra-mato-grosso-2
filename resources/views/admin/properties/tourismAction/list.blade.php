@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Ações relativas ao turismo
    <a href="{{ url('admin/acoes-turismo/editar') }}">
        <button class="btn btn-success pull-right" title="Nova ação">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_TourismActions as $c_Action)
        <tr>
            <td>{{$c_Action->name}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/acoes-turismo/editar/' . $c_Action->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop