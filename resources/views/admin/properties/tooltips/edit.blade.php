@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }

        .window-div{
            padding: 21px 5px 5px;
            margin-left: 0;
            margin-right: 0;
            margin-bottom: 10px;
            border: 1px solid #eee;
            position: relative;
        }

        .remove-window{
            font-size:15px;
            padding:10px;
            position: absolute;
            top:0;
            right:0;
            color: #bbb;
            cursor: pointer;
        }
    </style>
@stop
@section('panel-header')
    Tooltips - {{$p_Type}}
@stop
@section('content')
    <div class="row pt10">
        <div class="col-sm-12 mb10">
            <p><strong>Campo:</strong> nome do campo em que se deseja colocar tooltip (deve ser igual ao texto que acompanha o campo no formulário).</p>
            <p><strong>Tooltip:</strong> texto do tooltip.</p>
        </div>
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/tooltips/' . $p_Type), 'onsubmit' => 'return processTooltips()']) !!}
        <input type="hidden" name="tooltips[tooltips-{{$p_Type}}]" id="tooltips">

        <div id="tooltips-container">
            <?php
                $v_Fields = [];
                if($p_Tooltips != null)
                    $v_Fields = json_decode($p_Tooltips,1);
            ?>
            @foreach($v_Fields as $c_Field)
                <div class="col-sm-12">
                    <div class="window-div row">
                        <div class="form-group col-sm-12">
                            <label>Campo<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control name"  placeholder="Digite Aqui" value="{{$c_Field['name']}}" required>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Tooltip<span class="mandatory-field">*</span></label>
                            <textarea class="form-control text"  placeholder="Digite Aqui" required>{!! $c_Field['text'] !!}</textarea>
                        </div>
                        <a onclick="$(this).closest('.col-sm-12').remove()" title="Excluir" type="button" class="remove-window"><i class="fa fa-times-circle"></i></a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="form-group col-sm-12 mt5">
            <button type="button" class="btn btn-success" onclick="addTooltip()">Adicionar campo</button>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}

        <div class="tooltip-template" style="display: none">
            <div class="col-sm-12">
                <div class="window-div row">
                    <div class="form-group col-sm-12">
                        <label>Campo<span class="mandatory-field">*</span></label>
                        <input type="text" class="form-control name"  placeholder="Digite Aqui" required>
                    </div>
                    <div class="form-group col-sm-12">
                        <label>Tooltip<span class="mandatory-field">*</span></label>
                        <textarea class="form-control text"  placeholder="Digite Aqui" required></textarea>
                    </div>
                    <a onclick="$(this).closest('.col-sm-12').remove()" title="Excluir" type="button" class="remove-window"><i class="fa fa-times-circle"></i></a>
                </div>
            </div>
        </div>
    </div>
@stop
@section('pageScript')
    <script>
        var m_TooltipTemplate;
        $(document).ready(function(){
            m_TooltipTemplate = $('.tooltip-template').html();
        });


        function addTooltip(){
            $('#tooltips-container').append(m_TooltipTemplate);
        }

        function processTooltips(){
            var v_Fields = [];
            $('#mainForm .window-div').each(function(){
                v_Fields.push({name:$(this).find('.name').val().trim(), text:$(this).find('.text').val().trim()});
            });

            $('#tooltips').val(JSON.stringify(v_Fields));

            return true;
        }
    </script>
@stop