@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Rodovias - {{$p_Type}}
    <a href="{{ url('admin/rodovias/' . $p_Type . '/editar') }}">
        <button class="btn btn-success pull-right" title="Nova rodovia">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_Roads as $c_Road)
        <tr>
            <td>{{$c_Road->nome}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/rodovias/' . $p_Type . '/editar/' . $c_Road->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ url('admin/rodovias/' . $p_Type . '/excluir/' . $c_Road->id) }}" title="Excluir" type="button" class="btn btn-success delete-btn">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop