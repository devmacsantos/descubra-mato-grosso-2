@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Cadastur
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/cadastur'))) !!}
        <div class="form-group col-sm-6">
            <label for="url">URL<span class="mandatory-field">*</span></label>
            <input type="text" name="data[cadastur_url]" class="form-control" id="url" placeholder="Digite Aqui" value="{{$p_Url == null ? '' : $p_Url}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="senha">Senha (criptografada)<span class="mandatory-field">*</span></label>
            <input type="password" name="data[cadastur_senha]" class="form-control" id="senha" placeholder="Digite Aqui" value="{{$p_Pwd == null ? '' : $p_Pwd}}" required>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script>
        $(document).ready(function()
        {
        });
    </script>
@stop