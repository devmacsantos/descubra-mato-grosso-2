@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Capas - Calendário de Eventos
    <a href="{{ url('admin/capas-relatorios/criar') }}">
        <button class="btn btn-success pull-right" title="Nova atividade">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($reportcovers as $reportcover)
        <tr>
            <td>{{$reportcover->label}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/capas-relatorios/' . $reportcover->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                    @if (count($reportcovers) > 1)
                    <a href="{{ url('admin/capas-relatorios/delete/' . $reportcover->id) }}" title="Excluir" type="button" class="btn btn-success delete-btn">
                        <i class="fa fa-trash-o"></i>
                    </a>
                    @endif
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop