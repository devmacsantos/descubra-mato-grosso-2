@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Configurar relatório do calendário de eventos
@stop
@section('content')
    <div class="row pt15">
        @if (isset($r_reportcover))
        {!! Form::open(['id' => 'mainForm', 'url'=> url("/admin/capas-relatorios/$r_reportcover->id"), 'files' => true]) !!}
        @else
        {!! Form::open(['id' => 'mainForm', 'url'=> url("/admin/capas-relatorios"), 'files' => true]) !!}
        @endif
        <div class="form-group col-sm-12">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input maxlength="40" type="text" name="label" class="form-control" id="nome" placeholder="Digite Aqui" value="{{isset($r_reportcover) ? $r_reportcover->label : '' }}"  required>
        </div>
                 @if(isset($r_reportcover))
                 {{ method_field('PUT') }}
                 <input type="hidden" name="id" value="{{$r_reportcover->id}}">
                @endif       

            <div class="col-sm-6 fixed-photo-div text-center">
                <h5>Foto de Capa</h5>
                <div class="camera" style="@if (isset($r_reportcover) && ($r_reportcover->pcover != '')) {{'background-image:url(' . url('/imagens/calendario-eventos/' . $r_reportcover->pcover) . ');'}} @endif">
                    <img class="upload-btn-icon" style="@if (isset($r_reportcover) && ($r_reportcover->pcover != '')) {{'display:none'}} @endif" src="{{url('/assets/img/camera.png')}}" alt="">
                    <h4 class="upload-btn-text" style="@if (isset($r_reportcover) && ($r_reportcover->pcover != '')) {{'display:none'}} @endif">Capa</h4>
                    <input class="photo-upload-input" name="capa" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)" @if (!isset($r_reportcover)) {{'required'}} @endif>
                    <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhotoCalendarEvent(this, 'capa')"><i class="fa fa-trash-o"></i></a>
                </div>
            </div>
            <div class="col-sm-6 fixed-photo-div text-center">
                <h5>Imagem de Cabeçalho</h5>
                <div class="camera" style="@if (isset($r_reportcover) && ($r_reportcover->pheader != '')) {{'background-image:url(' . url('/imagens/calendario-eventos/' . $r_reportcover->pheader) . ');'}} @endif">
                    <img class="upload-btn-icon" style="@if (isset($r_reportcover) && ($r_reportcover->pheader != '')) {{'display:none'}} @endif" src="{{url('/assets/img/camera.png')}}" alt="">
                    <h4 class="upload-btn-text" style="@if (isset($r_reportcover) && ($r_reportcover->pheader != '')) {{'display:none'}} @endif">Cabeçalho</h4>
                    <input class="photo-upload-input" name="cabecalho" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                    <a title="Excluir" type="button" class="btn btn-success remove-photo" onclick="removePhotoCalendarEvent(this, 'cabecalho')"><i class="fa fa-trash-o"></i></a>
                </div>
            </div>
            <div class="form-group col-sm-12 mt15">
                <label for="last_page">Última página</label>
                <textarea name="event_calendar_last_page" class="form-control" rows="4" id="last_page" placeholder="Digite Aqui">@if (isset($r_reportcover)) {{$r_reportcover->message}} @endif</textarea>
            </div>
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script src="{{url('/vendor/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            CKEDITOR.replace('last_page',{
                allowedContent: true,
                filebrowserImageBrowseUrl: null,
                filebrowserFlashBrowseUrl: null,
                filebrowserUploadUrl: null,
                filebrowserImageUploadUrl: null,
                filebrowserFlashUploadUrl: null,
                height:420
            });
        });

        function removePhotoCalendarEvent(p_DeleteBtn, p_Type)
        {
            var v_DeleteDiv = $(p_DeleteBtn).closest('div[class*="photo-div"]');

            $('div[class*="photo-div"]:first').before('<input name="delete_photo[]" type="hidden" value="' + p_Type + '">');
            var v_ImageInput = v_DeleteDiv.find('.photo-upload-input');
            $(v_ImageInput).replaceWith(v_ImageInput = $(v_ImageInput).clone(true));
            setPhotoBackground(v_ImageInput, '');
        }
    </script>
@stop