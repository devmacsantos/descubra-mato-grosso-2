@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Serviços telefônicos
    <a href="{{ url('admin/servicos-telefonicos/editar') }}">
        <button class="btn btn-success pull-right" title="Novo clima">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_Services as $c_Service)
        <tr>
            <td>{{$c_Service->name}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/servicos-telefonicos/editar/' . $c_Service->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ url('admin/servicos-telefonicos/excluir/' . $c_Service->id) }}" title="Excluir" type="button" class="btn btn-success delete-btn">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop