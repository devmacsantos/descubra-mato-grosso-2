@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    <?php
        $v_Types = [
            'agua' => 'tipo de abastecimento de água',
            'esgotamento' => 'tipo de esgotamento',
            'energia' => 'tipo de abastecimento de energia',
            'coleta-lixo' => 'tipo de destinação do lixo',
        ];
    ?>
    {{ ($p_SupplyType == null ? 'Cadastro de ' : 'Edição de ') . $v_Types[$p_Type] }}
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/servicos-abastecimento/' . $p_Type))) !!}
        @if($p_SupplyType != null)
            <input type="hidden" name="id" value="{{$p_SupplyType->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_SupplyType == null ? '' : $p_SupplyType->name}}" required>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script>
        $(document).ready(function()
        {
        });
    </script>
@stop