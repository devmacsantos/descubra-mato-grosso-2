@extends('admin.main')
@section('pageCSS')
<link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css')}}">
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ isset($eventclassification) ? 'Edição' : 'Cadastro' }} de classificação de evento
@stop
@section('content')
@if (isset($messageErro))
<div id="error">
        <div class="alert alert-danger alert-block alert-dismissable fade in">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <p>{{$messageErro}}</p>
        </div>
</div>
@endif

<div class="row pt15">
        

        {!! Form::open(array('id' => 'mainForm', 'url'=> isset($eventclassification) ? url("/admin/eventos/classificacao/$eventclassification->id") : url("/admin/eventos/classificacao"))) !!}
        @if( isset($eventclassification))
            {{ method_field('PUT') }}
            <input type="hidden" name="id" value="{{$eventclassification->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite Aqui" value="{{isset($eventclassification) ? $eventclassification->nome : '' }}{{isset($old) ? $old['nome'] : '' }}" required>
        </div>
        <div class="form-group col-sm-2">
            <label for="nome">Publicar de</label>
            <input type="text" name="initial_date" class="form-control datetimepicker" id="initial_date" placeholder="Digite Aqui" value="{{isset($eventclassification) ? $eventclassification->initial_date : '' }}{{isset($old) ? $old['initial_date'] : '' }}">
        </div>
        <div class="form-group col-sm-2">
            <label for="nome">Até</label>
            <input type="text" name="final_date" class="form-control datetimepicker" id="final_date" placeholder="Digite Aqui" value="{{isset($eventclassification) ? $eventclassification->final_date : '' }}{{isset($old) ? $old['final_date'] : '' }}">
        </div>
        <div class="form-group col-sm-2">
            <label for="nome">Publicar</label>
            <select class="form-control" name="public">
                    <option value="0" @if (isset($eventclassification) && $eventclassification->public == 0 || isset($old) && $old['public'] == 0){{'selected'}}@endif>Não</option>
                    <option value="1" @if (isset($eventclassification) && $eventclassification->public == 1 || isset($old) && $old['public'] == 1){{'selected'}}@endif>Sim</option>
            </select> 
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
<script type="text/javascript" src="{{url('/vendor/plugins/datepicker/js/bootstrap-datetimepicker.js')}}"></script>
    <script>
        $(document).ready(function()
        {
           $('.datetimepicker').datetimepicker({
                        pickTime: false,
                        format: "DD-MM-YYYY"
                    }).mask('99-99-9999');
        });
    </script>
@stop