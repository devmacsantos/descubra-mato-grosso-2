@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Inventário - {{ $p_Type == null ? 'Cadastro' : 'Edição' }} de tipo
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/tipos'))) !!}
        @if($p_Type != null)
            <input type="hidden" name="id" value="{{$p_Type->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="inventory_item_id">Inventário<span class="mandatory-field">*</span></label>
            {!! Form::select('inventory_item_id', $p_InventoryItems, $p_Type == null ? '' : $p_Type->inventory_item_id, array('id' => 'inventory_item_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%')) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="super_type_id">Pai</label>
            {!! Form::select('super_type_id', $p_SuperTypes, $p_Type == null ? '' : $p_Type->super_type_id, array('id' => 'super_type_id', 'class' => 'form-control select2')) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">Nome - Português<span class="mandatory-field">*</span></label>
            <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Type == null ? '' : $p_Type->name}}" required>
        </div>

        <?php $v_Languages = ['en' => 'Inglês', 'es' => 'Espanhol', 'fr' => 'Francês']; ?>
        @foreach($v_Languages as $c_LanguageIndex => $c_Language)
            <div class="form-group col-sm-6">
                <label for="nome_{{$c_LanguageIndex}}">Nome - {{$c_Language}}</label>
                <input type="text" name="nome_{{$c_LanguageIndex}}" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Type == null ? '' : $p_Type['name_' . $c_LanguageIndex]}}">
            </div>
        @endforeach
        <div class="form-group col-sm-6">
            <label for="nome">Slug<span class="mandatory-field">*</span></label>
            <input type="text" name="slug" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Type == null ? '' : $p_Type->slug}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">Nome para busca/visualização<span class="mandatory-field">*</span></label>
            <input type="text" name="trade" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Type == null ? '' : $p_Type->trade}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">Sinônimos</label>
            <textarea name="sinonimos" class="form-control" id="nome" placeholder="Digite Aqui">{{$p_Type == null ? '' : $p_Type->sinonimos}} </textarea>
        </div>    
        <div class="form-group col-sm-6">
            <label for="nome">Excluir</label>
            <textarea name="excluir" class="form-control" id="nome" placeholder="Digite Aqui">{{$p_Type == null ? '' : $p_Type->excluir}} </textarea>
        </div>        
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('#inventory_item_id').change(function(){
                $.get("{{url('/admin/tipos-inventario?' . ($p_Type != null ? ('id=' . $p_Type->id) : ''))}}&inventory_id=" + $(this).val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                    {
                        var v_LastVal = $('#super_type_id').val();
                        var v_DataString = '';
                        $.each(data.data, function (c_Key, c_Field)
                        {
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                        });

                        $('#super_type_id').html('<option value=""></option>' + v_DataString);
                        if(data.data.length == 0)
                            $('#super_type_id').select2("val", "");
                        else
                            $('#super_type_id').select2("val", v_LastVal);
                    }
                    else
                        $('#super_type_id').html('<option value=""></option>' + v_DataString).select2("val", "");
                }).error(function(){
                });
            }).change();
        });
    </script>
@stop