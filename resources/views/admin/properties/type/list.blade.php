@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Inventário - Tipos e subtipos
    <a href="{{ url('admin/tipos/editar') }}">
        <button class="btn btn-success pull-right" title="Novo clima">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Inventário</th>
        <th>Pai</th>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_Types as $c_Type)
        <tr>
            <td>{{$c_Type->inventory}}</td>
            <td>{{$c_Type->super_name == null ? '' : $c_Type->super_name}}</td>
            <td>{{$c_Type->name}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/tipos/editar/' . $c_Type->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop