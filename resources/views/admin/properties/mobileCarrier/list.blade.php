@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Operadoras telefônicas
    <a href="{{ url('admin/operadoras-telefonicas/editar') }}">
        <button class="btn btn-success pull-right" title="Novo clima">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_Carriers as $c_Carrier)
        <tr>
            <td>{{$c_Carrier->name}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/operadoras-telefonicas/editar/' . $c_Carrier->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ url('admin/operadoras-telefonicas/excluir/' . $c_Carrier->id) }}" title="Excluir" type="button" class="btn btn-success delete-btn">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop