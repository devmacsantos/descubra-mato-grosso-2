@extends('admin.util.listStatic', ['p_HasDateFilter' => false])
@section('list-css')
@stop
@section('panel-header')
    Formas de pagamento
    <a href="{{ url('admin/formas-pagamento/editar') }}">
        <button class="btn btn-success pull-right" title="Nova forma de pagamento">
            <i class="fa fa-plus"></i>
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Ações</th>
    </tr>
@stop
@section('list-table-rows')
    @foreach($p_PaymentMethods as $c_PaymentMethod)
        <tr>
            <td>{{$c_PaymentMethod->nome_pt}}</td>
            <td>
                <div class="actions-div">
                    <a href="{{ url('admin/formas-pagamento/editar/' . $c_PaymentMethod->id) }}" title="Editar" type="button" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{ url('admin/formas-pagamento/excluir/' . $c_PaymentMethod->id) }}" title="Excluir" type="button" class="btn btn-success delete-btn">
                        <i class="fa fa-trash-o"></i>
                    </a>
                </div>
            </td>
        </tr>
    @endforeach
@stop
@section('list-table-initial-sorting')
    "order": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop