@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .remove-video, .add-video{
            height: 39px;
            width: 39px;
            min-width: 39px;
        }
        .remove-video-column, #add-video-column{
            display: flex;
            align-items: flex-start;
        }
        .remove-video-column{
            justify-content: center;
        }
    </style>
@stop
@section('panel-header')
    Galeria de Vídeos
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/paginas/galeria-videos'), 'onsubmit' => 'return submitForm()')) !!}

        <div class="col-sm-12 col-xs-12">
            <label for="video_url1">Urls dos vídeos<span class="mandatory-field">*</span></label>
        </div>
        <div class="form-group col-sm-6 col-xs-8 video-div">
            <input type="url" name="video_url[]" class="form-control" id="video_url1" placeholder="Digite Aqui" value="" required>
        </div>
        <div id="add-video-column" class="col-xs-2 form-group">
            <label for="newVideoBtn">&nbsp;</label>
            <a title="Adicionar vídeo" type="button" id="newVideoBtn" class="btn btn-success add-video" onclick="addVideo('')"><i class="fa fa-plus"></i></a>
        </div>

        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script>
        var v_VideoIndex = 2;
        var v_VideoHeight = 0;
        $(document).ready(function()
        {
            v_VideoHeight = $('.video-div:last').height();
            $('#add-video-column').height(v_VideoHeight);
            @if(count($p_Videos) > 0)
                @foreach($p_Videos as $c_Key => $c_Video)
                    @if($c_Key == 0)
                        $('#video_url1').val('{{$c_Video->value}}')
                    @else
                        addVideo("{{ $c_Video->value }}");
                    @endif
                @endforeach
            @endif
        });

        function addVideo(p_Value)
        {
            var v_Div = ('<div class="video-div" id="video-div' + v_VideoIndex.toString() + '" >' +
                    '<div class="col-xs-1 video-filler-column form-group"> </div>' +
                    '<div class="form-group col-sm-6 col-xs-8 video-picker">' +
                    '<input id="video_url' + v_VideoIndex.toString() + '" class="form-control" name="video_url[]" type="url" value="' + p_Value + '">' +
                    '</div>' +
                    '<div class="col-xs-1 remove-video-column">' +
                    '<a title="Excluir vídeo" type="button" class="btn btn-success remove-date" onclick="removeVideo(this)"><i class="fa fa-remove"></i></a>' +
                    '</div>' +
                    '</div>'
            );
            $('.video-div:last').after(v_Div);
            $('.video-filler-column:last').height(v_VideoHeight);
            v_VideoHeight = $('.video-picker:last').height();
            $('.remove-video-column').height(v_VideoHeight);
            $('#add-video-column').height(v_VideoHeight);
            v_VideoIndex ++;
        }
        function removeVideo(p_DeleteBtn)
        {
            $(p_DeleteBtn).parent().parent().remove();
            v_VideoHeight = $('.video-div:last').height();
            if(v_VideoHeight > 0)
                $('#add-video-column').height(v_VideoHeight);
        }

        function submitForm()
        {
            $('#mainForm input[name*="video_url"]').each(function (c_Key, c_Field)
            {
                var v_Url = $(c_Field).val();
                if(v_Url.indexOf('/watch') > -1)
                {
                    var v_VideoId = v_Url.split('v=')[1];
                    var v_AmpersandPosition = v_VideoId.indexOf('&');
                    if(v_AmpersandPosition != -1)
                        v_VideoId = v_VideoId.substring(0, v_AmpersandPosition);

                    $(c_Field).val('https://www.youtube.com/embed/' + v_VideoId);
                }
            });

            return true;
        }
    </script>
@stop