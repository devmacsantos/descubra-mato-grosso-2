<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Dashboard do Descubra Mato Grosso (powered by GAFIT - Soluções em Automação e TI)">
        <meta name="author" content="GAFIT - Soluções em Automação e TI">
        <link rel="icon" href="{{url('/favicon.ico')}}"/>

        <title>Descubra Mato Grosso</title>

        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('/assets/skin/default_skin/css/theme.css')}}">
        <!-- Admin Forms CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('/assets/admin-tools/admin-forms/css/admin-forms.css')}}">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="external-page sb-l-c sb-r-c">

    <!-- Start: Settings Scripts -->
    <script>
        var boxtest = localStorage.getItem('boxed');

        if (boxtest === 'true') {
            document.body.className += ' boxed-layout';
        }
    </script>
    <!-- End: Settings Scripts -->

    <!-- Start: Main -->
    <div id="main" class="animated fadeIn">

        <!-- Start: Content -->
        <section id="content_wrapper">

            <!-- begin canvas animation bg -->
            <div id="canvas-wrapper">
                <canvas id="demo-canvas"></canvas>
            </div>

            <!-- Begin: Content -->
            <section id="content">

                <div class="admin-form theme-info" id="login1">

                    <div class="row mb15 table-layout">
                        <div class="col-xs-12 va-m pln">
                            <a href="{{ url('/') }}">
                                <img src="{{url('/assets/img/descubraMatoGrosso.png')}}" title="Descubra Mato Grosso" class="img-responsive">
                            </a>
                            <!--
                            @if (App::environment('local')) <h1 class="text-danger">Ambiente de Treinamento</h1> @endif
                            -->
                        </div>
                    </div>

                    <div class="panel panel-info mt10 br-n">
                        @if(session('message'))
                            <div class="alert alert-system alert-block alert-dismissable fade in mbn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <p>{!! session('message') !!}</p>
                            </div>
                        @endif
                        @if(session('error_message'))
                            <div class="alert alert-danger alert-block alert-dismissable fade in mbn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <p>{!! session('error_message') !!}</p>
                            </div>
                        @endif
                        @foreach($errors->all() as $error)
                            <div class="alert alert-danger alert-block alert-dismissable fade in mbn">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <p>{{ $error }}</p>
                            </div>
                        @endforeach
                        {!! Form::open(array('url'=> url('/admin/login'))) !!}
                            @if(Session::has('redirected_from'))
                                <input type="hidden" name="redirected_from" value="{{ Session::get('redirected_from') }}">
                            @endif
                            <div class="panel-body bg-light p30">
                                <div class="row">
                                    <div class="col-sm-12 pr30">
                                        <div class="section">
                                            <label for="email" class="field-label text-muted fs18 mb10">Email</label>
                                            <label for="email" class="field prepend-icon">
                                                <input type="email" name="email" class="gui-input" id="email" placeholder="Digite o email" required>
                                                <label for="email" class="field-icon"><i class="fa fa-user"></i></label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                        <div class="section">
                                            <label for="password" class="field-label text-muted fs18 mb10">Senha</label>
                                            <label for="password" class="field prepend-icon">
                                                <input type="password" name="password" class="gui-input" id="password" placeholder="Digite a senha" required>
                                                <label for="password" class="field-icon"><i class="fa fa-lock"></i>
                                                </label>
                                            </label>
                                        </div>
                                        <!-- end section -->

                                        <div class="mt10">
                                            Não tem usuário? <a href="{{ url('/admin/trade/cadastrar') }}">Cadastre-se aqui</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end .form-body section -->
                            <div class="panel-footer clearfix p10 ph15">
                                <div class="switch block switch-primary pull-left input-align mt10">
                                    <a href="{{ url('/admin/resetar-senha') }}">Esqueci a senha</a>
                                </div>
                                {!! Form::submit('Entrar', array('class'=>'button btn-info mr10 pull-right')) !!}
                            </div>
                            <!-- end .form-footer section -->
                        {!! Form::close() !!}
                        <!-- end .form-header section -->
                    </div>
                </div>

            </section>
            <!-- End: Content -->

        </section>
        <!-- End: Content-Wrapper -->

    </div>
    <!-- End: Main -->
    <!-- jQuery -->
    <script type="text/javascript" src="{{url('/vendor/jquery/jquery-1.11.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/jquery/jquery_ui/jquery-ui.min.js')}}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{url('/assets/js/bootstrap/bootstrap.min.js')}}"></script>

    <!-- Page Plugins -->
    <script type="text/javascript" src="{{url('/assets/js/pages/login/EasePack.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/pages/login/rAF.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/pages/login/TweenLite.min.js')}}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{url('/assets/js/utility/utility.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/main.js')}}"></script>

    <!-- Page Javascript -->
    <script type="text/javascript">
        $(document).ready(function() {

            "use strict";

            // Init Theme Core
            Core.init();

        });

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'REPLACEME', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- END: PAGE SCRIPTS -->
    </body>
</html>
