@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    @if(\App\UserType::isTrade())
        Gastronomia
    @else
        B2 - Serviço e equipamento de alimentos e bebidas
    @endif
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
        <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
            <i class="fa fa-print"></i>
        </a>
    </ul>
@stop
@section('content')
    <div class="row visible-print">
        <div class="mb20 col-sm-12">
            <h2>B2 - Serviço e equipamento de alimentos e bebidas</h2>
        </div>
    </div>
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    @if(!\App\UserType::isParceiro())
    {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/servicos-alimentos'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
    @else
    <div id="mainForm">
    @endif

    <div class="tab-content pn br-n">
        <?php
            $v_TabLanguages = ['pt', 'en', 'es', 'fr'];
            if($p_FoodDrinksService != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_SurroundingsDescriptions = $p_FoodDrinksService->descricao_arredores_distancia_pontos != null ? json_decode($p_FoodDrinksService->descricao_arredores_distancia_pontos,1) : $v_EmptyData;
                $v_Restrictions = $p_FoodDrinksService->restricoes != null ? json_decode($p_FoodDrinksService->restricoes,1) : $v_EmptyData;
                $v_Descriptions = $p_FoodDrinksService->descricoes_observacoes_complementares != null ? json_decode($p_FoodDrinksService->descricoes_observacoes_complementares,1) : $v_EmptyData;
                $v_CuisineOtherNationalities = $p_FoodDrinksService->culinaria_nacionalidade_outras != null ? json_decode($p_FoodDrinksService->culinaria_nacionalidade_outras,1) : $v_EmptyData;
                $v_CuisineOtherThemes = $p_FoodDrinksService->culinaria_tema_outros != null ? json_decode($p_FoodDrinksService->culinaria_tema_outros,1) : $v_EmptyData;
                $v_CuisineOtherRegions = $p_FoodDrinksService->culinaria_regiao_outras != null ? json_decode($p_FoodDrinksService->culinaria_regiao_outras,1) : $v_EmptyData;
                $v_ShortDescriptions = $p_FoodDrinksService->descricao_curta != null ? json_decode($p_FoodDrinksService->descricao_curta,1) : $v_EmptyData;
            }
        ?>
        @foreach($v_TabLanguages as $c_TabIndex => $c_TabLanguage)
        <div id="{{'tab' . $c_TabIndex}}" class="tab-pane {{ $c_TabIndex == 0 ? 'active' : '' }}">
            <div class="row">
                @if($c_TabLanguage == 'pt')
                    @if($p_FoodDrinksService != null)
                        <input type="hidden" name="id" value="{{$p_FoodDrinksService->id}}">
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="{{$p_FoodDrinksService->tem_cadastur}}">
                    @else
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="0">
                    @endif
                    @include('admin.util.headerIdentification', ['p_Form' => $p_FoodDrinksService, 'p_Types' => $p_Types, 'p_WithoutSubtype' => true, 'p_CadasturActivity' => 15])

                    <input type="hidden" id="default_tipo_atividade_cadastur" value="15">
                    
                    <div class="form-group col-sm-6">
                        <label for="data_tombamento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Data do tombamento</label>
                        <input type="text" name="formulario[data_tombamento]" class="form-control date-field" id="data_tombamento" placeholder="Digite Aqui" value="{{($p_FoodDrinksService == null || $p_FoodDrinksService->data_tombamento == null || $p_FoodDrinksService->data_tombamento == '') ? '' : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_FoodDrinksService->data_tombamento)->format('d/m/Y')}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="organizacao_responsavel">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Organização responsável pelo tombamento</label>
                        <input type="text" name="formulario[organizacao_responsavel]" class="form-control" id="organizacao_responsavel" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->organizacao_responsavel}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 0000-0000)<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->telefone}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="whatsapp">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Whatsapp</label>
                        <input type="text" name="formulario[whatsapp]" class="form-control phone-field" id="whatsapp" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->whatsapp}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
                        <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->site}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
                        <input type="email" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->email}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="facebook">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Facebook</label>
                        <input type="url"  name="formulario[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->facebook}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="instagram">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instagram</label>
                        <input type="text"  name="formulario[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->instagram}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="twitter">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Twitter</label>
                        <input type="text"  name="formulario[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->twitter}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="youtube">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Youtube</label>
                        <input type="url"  name="formulario[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->youtube}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="tripadvisor">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tripadvisor</label>
                        <input type="url"  name="formulario[tripadvisor]" class="form-control" id="tripadvisor" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->tripadvisor}}">
                    </div>

                    <input type="hidden" id="descricao_curta" name="formulario[descricao_curta]">
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_FoodDrinksService == null ? '' : $v_ShortDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                @endif

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-6">
                        <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP<span class="mandatory-field">*</span></label>
                        <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->cep}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->bairro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->logradouro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->numero}}" required>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
                        <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->complemento}}">
                        <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                            <i class="fa fa-map-marker"></i>
                        </a>
                    </div>

                    <input type="hidden" id="descricao_arredores_distancia_pontos" name="formulario[descricao_arredores_distancia_pontos]">
                    <div class="form-group col-sm-12">
                        <label for="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição dos arredores e distância dos principais pontos turísticos<span class="mandatory-field">*</span></label>
                        <textarea id="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_FoodDrinksService == null ? '' : $v_SurroundingsDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <?php
                            $v_Locations = [
                                ''=>'',
                                'Urbana'=>'Urbana',
                                'Rururbana'=>'Rururbana',
                                'Rural'=>'Rural'
                            ];
                        ?>
                        <label for="localizacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Localização<span class="mandatory-field">*</span></label>
                        {!! Form::select('formulario[localizacao]', $v_Locations, $p_FoodDrinksService == null ? '' : $p_FoodDrinksService->localizacao, ['id' => 'localizacao', 'class' => 'form-control', 'required' => 'required']) !!}
                    </div>
                    <div class="form-group col-sm-12">
                        <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
                        <div id="map_canvas" style="height:320px;"></div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="latitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->latitude}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->longitude}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
                        <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
                    </div>

                    @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])
                    <div class="col-sm-12">
                        <p>* É necessário ter ao menos uma foto de capa para que possa aparecer no portal.</p>
                    </div>
                @endif

                @if($c_TabLanguage != 'pt')
                    <div class="form-group col-sm-12">
                        <label for="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição dos arredores e distância dos principais pontos turísticos</label>
                        <textarea id="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_FoodDrinksService == null ? '' : $v_SurroundingsDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                @endif

                @include('admin.util.workingPeriod', ['p_Form' => $p_FoodDrinksService, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage, 'p_ClosedFields' => true])

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Capacidade do empreendimento</h3>
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="capacidade_pessoas_em_pe">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de pessoas em pé</label>
                        <input type="text" name="formulario[capacidade_pessoas_em_pe]" class="form-control integer-field" id="capacidade_pessoas_em_pe" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->capacidade_pessoas_em_pe}}">
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="capacidade_pessoas_sentadas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de pessoas sentadas</label>
                        <input type="text" name="formulario[capacidade_pessoas_sentadas]" class="form-control integer-field" id="capacidade_pessoas_sentadas" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->capacidade_pessoas_sentadas}}">
                    </div>
                    <div class="form-group col-sm-4">
                        <label for="capacidade_mesas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de mesas<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[capacidade_mesas]" class="form-control integer-field" id="capacidade_mesas" placeholder="Digite Aqui" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->capacidade_mesas}}" required>
                    </div>

                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Serviços e equipamentos</h3>
                    </div>
                    <input type="hidden" id="servicos_equipamentos" name="formulario[servicos_equipamentos]" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->servicos_equipamentos}}">
                    <div class="servicos-equipamentos-fields">
                        <?php
                            $v_ServiceEquipmentsFields = [
                                    'Música ao vivo',
                                    'Música mecânica/ambiente',
                                    'Estacionamento',
                                    'Ar condicionado',
                                    'Recreação/Área de lazer',
                                    'Espaço para fumantes',
                                    'Cardápio em braille',
                                    'Manobristas',
                                    'Wi Fi'
                            ];
                        ?>
                        @foreach($v_ServiceEquipmentsFields as $c_Field)
                            <div class="form-group col-sm-3">
                                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
                                <p><input type="checkbox" rel="{{$c_Field}}" class="ml5 mt10 checkbox field"></p>
                            </div>
                        @endforeach
                        <div class="form-group col-sm-12">
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Atendimento bilingue?</label>
                            <p><input type="checkbox" rel="Atendimento bilingue" class="ml5 mt10 checkbox field" id="atendimento_bilingue"></p>
                        </div>
                        <div class="form-group col-sm-12 atendimento-bilingue-fields">
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Atendimento - Idiomas <i>(permite mais de uma opção)</i></label>
                            {!! Form::select('', $p_LanguageNames, null, ['rel' => 'Atendimento - Idiomas', 'class' => 'form-control field select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Cardápio em outros idiomas?</label>
                            <p><input type="checkbox" rel="Cardápio em outros idiomas" class="ml5 mt10 checkbox field" id="cardapio_outros_idiomas"></p>
                        </div>
                        <div class="form-group col-sm-12 cardapio-outros-idiomas-fields">
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Cardápio - Idiomas <i>(permite mais de uma opção)</i></label>
                            {!! Form::select('', $p_LanguageNames, null, ['rel' => 'Cardápio - Idiomas', 'class' => 'form-control field select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
                        </div>
                    </div>
                @endif

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Tipos de culinária</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <label for="culinaria_nacionalidades">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nacionalidade <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="culinaria_nacionalidades" name="formulario[culinaria_nacionalidades]" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->culinaria_nacionalidades}}">
                        {!! Form::select('', $p_CuisineNationalities, null, ['id' => 'culinaria_nacionalidades_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    <input type="hidden" id="culinaria_nacionalidade_outras" name="formulario[culinaria_nacionalidade_outras]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="culinaria_nacionalidade_outras_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nacionalidade - Outras</label>
                    <textarea id="culinaria_nacionalidade_outras_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_FoodDrinksService == null ? '' : $v_CuisineOtherNationalities[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <label for="culinaria_servicos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviço <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="culinaria_servicos" name="formulario[culinaria_servicos]" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->culinaria_servicos}}">
                        {!! Form::select('', $p_CuisineServices, null, ['id' => 'culinaria_servicos_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="culinaria_temas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tema <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="culinaria_temas" name="formulario[culinaria_temas]" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->culinaria_temas}}">
                        {!! Form::select('', $p_CuisineThemes, null, ['id' => 'culinaria_temas_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    <input type="hidden" id="culinaria_tema_outros" name="formulario[culinaria_tema_outros]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="culinaria_tema_outros_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tema - Outros</label>
                    <textarea id="culinaria_tema_outros_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_FoodDrinksService == null ? '' : $v_CuisineOtherThemes[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <label for="culinaria_regioes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Região <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="culinaria_regioes" name="formulario[culinaria_regioes]" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->culinaria_regioes}}">
                        {!! Form::select('', $p_CuisineRegions, null, ['id' => 'culinaria_regioes_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    <input type="hidden" id="culinaria_regiao_outras" name="formulario[culinaria_regiao_outras]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="culinaria_regiao_outras_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Região - Outras</label>
                    <textarea id="culinaria_regiao_outras_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_FoodDrinksService == null ? '' : $v_CuisineOtherRegions[$c_TabLanguage]}}</textarea>
                </div>

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Modalidades complementares</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <?php
                        $v_DescriptionFields = [
                                'aceita_animais'=>'Aceita animais - PET',
                                'nao_aceita_criancas'=>'Adequado para crianças'
                        ];
                    ?>
                    @foreach($v_DescriptionFields as $c_Id => $c_Field)
                        <div class="form-group col-sm-3">
                            <label for="{{$c_Id}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
                            <p><input type="checkbox" value="1" name="formulario[{{$c_Id}}]" id="{{$c_Id}}" class="ml5 mt10" {{($p_FoodDrinksService == null || $p_FoodDrinksService[$c_Id] == 0) ? '' : 'checked'}}></p>
                        </div>
                    @endforeach

                    <input type="hidden" id="restricoes" name="formulario[restricoes]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="restricoes_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Restrições</label>
                    <textarea id="restricoes_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_FoodDrinksService == null ? '' : $v_Restrictions[$c_TabLanguage]}}</textarea>
                </div>

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <label for="formas_pagamento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Formas de pagamento <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="formas_pagamento" name="formulario[formas_pagamento]" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->formas_pagamento}}">
                        {!! Form::select('', $p_PaymentMethods, null, ['id' => 'formas_pagamento_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    <input type="hidden" id="descricoes_observacoes_complementares" name="formulario[descricoes_observacoes_complementares]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="descricoes_observacoes_complementares_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
                    <textarea id="descricoes_observacoes_complementares_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_FoodDrinksService == null ? '' : $v_Descriptions[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    @include('admin.util.hashtags', ['p_Form' => $p_FoodDrinksService])

                    @if(!\App\UserType::isTrade())
                        <div class="form-group col-sm-12">
                            <label for="possui_espacos_eventos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui espaços de eventos?</label>
                            <p><input type="checkbox" value="1" name="formulario[possui_espacos_eventos]" id="possui_espacos_eventos" class="ml5 mt10" {{($p_FoodDrinksService == null || $p_FoodDrinksService->possui_espacos_eventos == 0) ? '' : 'checked'}}></p>
                        </div>
                    @endif
                @endif


                @include('admin.util.accessibility', ['p_Form' => $p_FoodDrinksService, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage])

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
                    </div>

                    <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_FoodDrinksService == null ? '' : $p_FoodDrinksService->tipos_viagem}}">
                    <div class="form-group col-sm-12">
                        <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
                        {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>


                    @include('admin.util.tradeAndAttraction', ['p_Form' => $p_FoodDrinksService])

                    @include('admin.util.revision', ['p_Form' => $p_FoodDrinksService, 'p_Publish' => true])

                    @include('admin.util.responsibleTeam', ['p_Form' => $p_FoodDrinksService])
                @endif
            </div>
        </div>
        @endforeach

        @if(!\App\UserType::isParceiro())
            <div class="row">
                <div class="form-group col-sm-12">
                    <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
                </div>
            </div>
        @endif
    </div>
    @if(!\App\UserType::isParceiro())
    {!! Form::close() !!}
    @else
    </div>
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.workingPeriodScript', ['p_Translation' => true])
    @include('admin.util.headerIdentificationScript', ['p_WithoutSubtype' => true])
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            carregaDadosServicosEquipamentos();

            carregaDadosCulinaria();

            var v_PaymentMethods = $('#formas_pagamento').val();
            if(v_PaymentMethods.length > 0)
            {
                v_PaymentMethods = JSON.parse(v_PaymentMethods);
                var v_SelectedItems = [];
                $(v_PaymentMethods).each(function(){
                    v_SelectedItems.push(this.id);
                });
                $('#formas_pagamento_select').select2("val", v_SelectedItems);
            }

            @if(\App\UserType::isTrade())
                $('#cnpj').attr('disabled', true);
                @if($p_FoodDrinksService == null)
                    var v_Data = localStorage.getItem("newTradeItem");
                    if(v_Data != null) {
                        localStorage.removeItem("newTradeItem");
                        v_Data = JSON.parse(v_Data);
                        $('#cnpj').val(v_Data.cnpj).blur();
                        $('#tipo_atividade_cadastur').val(v_Data.tipoAtividade);
                        if(v_Data.token.length > 0)
                            v_CadasturToken = v_Data.token;
                        if(v_CadasturToken.length > 0)
                            loadCadasturData();
                    }
                    else
                        location.href = '{{ url('admin/inventario/servicos-alimentos') }}';
                @endif
            @endif

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }
        });


        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosFuncionamentos();

            processaDadosServicosEquipamentos();

            processaDadosCulinaria();

            processaDadosAcessibilidade();

            var v_PaymentMethods = [];
            $('#formas_pagamento_select option:selected').each(function(){
                v_PaymentMethods.push({
                    id:$(this).attr('value'),
                    nome:$(this).text()
                });
            });
            $('#formas_pagamento').val(JSON.stringify(v_PaymentMethods));


            <?php $v_Languages = ['pt', 'en', 'es', 'fr']; ?>

            var v_SurroundingsJson = {};
            var v_RestrictionsJson = {};
            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            var v_CuisineOtherNationalitiesJson = {};
            var v_CuisineOtherThemesJson = {};
            var v_CuisineOtherRegionsJson = {};
            @foreach($v_Languages as $c_Language)
                v_SurroundingsJson.{{$c_Language}} = $('#descricao_arredores_distancia_pontos_{{$c_Language}}').val();
                v_RestrictionsJson.{{$c_Language}} = $('#restricoes_{{$c_Language}}').val();
                v_DescriptionsJson.{{$c_Language}} = $('#descricoes_observacoes_complementares_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
                v_CuisineOtherNationalitiesJson.{{$c_Language}} = $('#culinaria_nacionalidade_outras_{{$c_Language}}').val();
                v_CuisineOtherThemesJson.{{$c_Language}} = $('#culinaria_tema_outros_{{$c_Language}}').val();
                v_CuisineOtherRegionsJson.{{$c_Language}} = $('#culinaria_regiao_outras_{{$c_Language}}').val();
            @endforeach
            $('#descricao_arredores_distancia_pontos').val(JSON.stringify(v_SurroundingsJson));
            $('#restricoes').val(JSON.stringify(v_RestrictionsJson));
            $('#descricoes_observacoes_complementares').val(JSON.stringify(v_DescriptionsJson));
            $('#descricao_curta').val(JSON.stringify(v_ShortDescriptionsJson));
            $('#culinaria_nacionalidade_outras').val(JSON.stringify(v_CuisineOtherNationalitiesJson));
            $('#culinaria_tema_outros').val(JSON.stringify(v_CuisineOtherThemesJson));
            $('#culinaria_regiao_outras').val(JSON.stringify(v_CuisineOtherRegionsJson));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            var v_Hashtags = $('#hashtags_select').val();
            $('#hashtags').val(v_Hashtags == null ? '' : v_Hashtags.join(';'));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

        function carregaDadosServicosEquipamentos()
        {
            var v_Dados = $('#servicos_equipamentos').val();
            if(v_Dados.length > 0)
            {
                v_Dados = JSON.parse(v_Dados);
                $(v_Dados).each(function(){
                    if(this.tipo == 'checkbox')
                        $('.servicos-equipamentos-fields .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                    else if(this.tipo == 'select2')
                        $('.servicos-equipamentos-fields .field[rel="'+this.nome+'"]').select2("val", this.valor);
                    else
                        $('.servicos-equipamentos-fields .field[rel="'+this.nome+'"]').val(this.valor);
                });
            }

            $('#atendimento_bilingue').change(function(){
                if($(this).is(':checked'))
                    $('.atendimento-bilingue-fields').show();
                else
                    $('.atendimento-bilingue-fields').hide();
            }).change();

            $('#cardapio_outros_idiomas').change(function(){
                if($(this).is(':checked'))
                    $('.cardapio-outros-idiomas-fields').show();
                else
                    $('.cardapio-outros-idiomas-fields').hide();
            }).change();
        }

        function carregaDadosCulinaria()
        {
            carregaSelectCulinaria('culinaria_nacionalidades');
            carregaSelectCulinaria('culinaria_servicos');
            carregaSelectCulinaria('culinaria_temas');
            carregaSelectCulinaria('culinaria_regioes');
        }

        function carregaSelectCulinaria(p_Campo)
        {
            var v_Dados = $('#'+p_Campo).val();
            if(v_Dados.length > 0)
            {
                v_Dados = JSON.parse(v_Dados);
                var v_SelectedItems = [];
                $(v_Dados).each(function(){
                    v_SelectedItems.push(this.id);
                });
                $('#'+p_Campo+'_select').select2("val", v_SelectedItems);
            }
        }

        function processaDadosServicosEquipamentos()
        {
            var v_Dados = [];
            $('.servicos-equipamentos-fields .field').each(function(){
                v_Dados.push({
                    nome:$(this).attr('rel'),
                    tipo:$(this).hasClass('checkbox') ? 'checkbox' : ($(this).hasClass('select2') ? 'select2' : 'text-or-select'),
                    valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
                });
            });
            $('#servicos_equipamentos').val(JSON.stringify(v_Dados));
        }

        function processaDadosCulinaria()
        {
            processaSelectCulinaria('culinaria_nacionalidades');
            processaSelectCulinaria('culinaria_servicos');
            processaSelectCulinaria('culinaria_temas');
            processaSelectCulinaria('culinaria_regioes');
        }

        function processaSelectCulinaria(p_Campo)
        {
            var v_Dados = [];
            $('#'+p_Campo+'_select option:selected').each(function(){
                v_Dados.push({
                    id:$(this).attr('value'),
                    nome:$(this).text()
                });
            });
            $('#'+p_Campo).val(JSON.stringify(v_Dados));
        }






    </script>
    @include('admin.util.mapScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.cadastur30')
    @include('admin.util.tooltipScript', ['p_Type' => 'B2'])
@stop