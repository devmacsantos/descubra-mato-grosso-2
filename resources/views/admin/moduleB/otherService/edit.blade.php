@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    B7 - Outros serviços e equipamentos turísticos
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <div class="row">
        <div class="mb20 col-sm-12 visible-print">
            <h2>B7 - Outros serviços e equipamentos turísticos</h2>
        </div>
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/outros-servicos-turisticos'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
        @else
        <div id="mainForm">
        @endif

        @if($p_OtherService != null)
            <input type="hidden" name="id" value="{{$p_OtherService->id}}">
        @endif
        @include('admin.util.headerIdentification', ['p_Form' => $p_OtherService, 'p_Types' => $p_Types, 'p_CadasturActivityOptions' => [''=>'',45=>'Empreendimento de Apoio ao Turismo Náutico ou à Pesca Desportiva',75=>'Prestador Especializado em Segmentos Turísticos'], 'p_WithoutNetwork' => true])

        <div class="form-group col-sm-6">
            <?php
                $v_Naturezas = [
                        ''=>'',
                        'Pública'=>'Pública',
                        'Privada'=>'Privada',
                        'Sindicatos'=>'Sindicatos',
                        'Associações'=>'Associações'
                ];
            ?>
            <label for="natureza_entidade">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Natureza da entidade<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[natureza_entidade]', $v_Naturezas, $p_OtherService == null ? '' : $p_OtherService->natureza_entidade, ['id' => 'natureza_entidade', 'class' => 'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="natureza_entidade_outras">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outras</label>
            <textarea name="formulario[natureza_entidade_outras]" id="natureza_entidade_outras" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_OtherService == null ? '' : $p_OtherService->natureza_entidade_outras}}</textarea>
        </div>

        <div class="form-group col-sm-6">
            <label for="registro_filiacao_entidade">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Registro ou filiações - Entidade</label>
            <input type="text" name="formulario[registro_filiacao_entidade]" class="form-control" id="registro_filiacao_entidade" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->registro_filiacao_entidade}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="registro_filiacao_numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Registro ou filiações - Número</label>
            <input type="number" name="formulario[registro_filiacao_numero]" class="form-control" id="registro_filiacao_numero" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->registro_filiacao_numero}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->telefone}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="whatsapp">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Whatsapp</label>
            <input type="text" name="formulario[whatsapp]" class="form-control phone-field" id="whatsapp" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->whatsapp}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
            <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->site}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
            <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->email}}">
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização e ambiência</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP<span class="mandatory-field">*</span></label>
            <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->cep}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->bairro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->logradouro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->numero}}" required>
        </div>
        <div class="form-group col-sm-12">
            <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
            <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->complemento}}">
            <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                <i class="fa fa-map-marker"></i>
            </a>
        </div>
        <div class="form-group col-sm-12">
            <label for="descricao_arredores_distancia_pontos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição dos arredores e distância dos principais pontos turísticos<span class="mandatory-field">*</span></label>
            <textarea name="formulario[descricao_arredores_distancia_pontos]" id="descricao_arredores_distancia_pontos" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_OtherService == null ? '' : $p_OtherService->descricao_arredores_distancia_pontos}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <?php
                $v_Locations = [
                    ''=>'',
                    'Urbana'=>'Urbana',
                    'Rururbana'=>'Rururbana',
                    'Rural'=>'Rural'
                ];
            ?>
            <label for="localizacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Localização<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[localizacao]', $v_Locations, $p_OtherService == null ? '' : $p_OtherService->localizacao, ['id' => 'localizacao', 'class' => 'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
            <div id="map_canvas" style="height:320px;"></div>
        </div>
        <div class="form-group col-sm-6">
            <label for="latitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->latitude}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->longitude}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
            <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
        </div>
        

        @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])

        @include('admin.util.workingPeriod', ['p_Form' => $p_OtherService, 'p_ClosedFields' => true])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Atendimento</h3>
        </div>
        <div class="col-sm-12 mt30"></div>
        <div class="form-group col-sm-6">
            <label for="taxa_ocupacao_media_mensal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Taxa de visitação media mensal</label>
            <input type="number" min="0" step="0.01" name="formulario[taxa_ocupacao_media_mensal]" class="form-control" id="taxa_ocupacao_media_mensal" placeholder="Digite Aqui" value="{{$p_OtherService == null ? '' : $p_OtherService->taxa_ocupacao_media_mensal}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="mes_maior_atendimento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Mês de maior atendimento <i>(permite mais de uma opção)</i></label>
            <?php $v_MonthRange = $p_OtherService == null ? [] : explode(';', $p_OtherService->mes_maior_atendimento); ?>
            {!! Form::select('mes_maior_atendimento[]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, ['id' => 'mes_maior_atendimento', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="mes_menor_atendimento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Mês de menor atendimento <i>(permite mais de uma opção)</i></label>
            <?php $v_MonthRange = $p_OtherService == null ? [] : explode(';', $p_OtherService->mes_menor_atendimento); ?>
            {!! Form::select('mes_menor_atendimento[]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, ['id' => 'mes_menor_atendimento', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>

        <div class="form-group col-sm-12 mt20">
            <label for="principais_atividades">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Principais atividades<span class="mandatory-field">*</span></label>
            <textarea name="formulario[principais_atividades]" id="principais_atividades" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_OtherService == null ? '' : $p_OtherService->principais_atividades}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <label for="atendimento_bilingue">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Atendimento bilingue?</label>
            <p><input type="checkbox" value="1" name="formulario[atendimento_bilingue]" id="atendimento_bilingue" class="ml5 mt10" {{($p_OtherService == null || $p_OtherService->atendimento_bilingue == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-12 atendimento-bilingue-fields">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Atendimento - Idiomas <i>(permite mais de uma opção)</i></label>
            <input type="hidden" id="atendimento_bilingue_idiomas" name="formulario[atendimento_bilingue_idiomas]" value="{{$p_OtherService == null ? '' : $p_OtherService->atendimento_bilingue_idiomas}}">
            {!! Form::select('', $p_LanguageNames, null, ['id' => 'atendimento_bilingue_idiomas_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>


        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados complementares</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="descricoes_observacoes_complementares">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
            <textarea name="formulario[descricoes_observacoes_complementares]" id="descricoes_observacoes_complementares" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_OtherService == null ? '' : $p_OtherService->descricoes_observacoes_complementares}}</textarea>
        </div>

        <div class="form-group col-sm-12">
            <label for="possui_espacos_eventos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui espaços de eventos?</label>
            <p><input type="checkbox" value="1" name="formulario[possui_espacos_eventos]" id="possui_espacos_eventos" class="ml5 mt10" {{($p_OtherService == null || $p_OtherService->possui_espacos_eventos == 0) ? '' : 'checked'}}></p>
        </div>

        @include('admin.util.accessibility', ['p_Form' => $p_OtherService])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
        </div>

        <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_OtherService == null ? '' : $p_OtherService->tipos_viagem}}">
        <div class="form-group col-sm-12">
            <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>

        @include('admin.util.revision', ['p_Form' => $p_OtherService])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_OtherService])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.workingPeriodScript')
    @include('admin.util.headerIdentificationScript', ['p_CadasturActivities' => $p_CadasturActivities])
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('#mes_maior_atendimento, #mes_menor_atendimento').change(function(){
                var v_Field = this;
                var v_Selection = $(v_Field).val();
                if($.inArray('Todos', v_Selection) > -1)
                {
                    if($(v_Field).find('option:selected').length == 13)
                        $(v_Field).select2("val", "");
                    else
                        $(v_Field).select2("val", ['1','2','3','4','5','6','7','8','9','10','11','12']);
                }
            });

            $('#atendimento_bilingue').change(function(){
                if($(this).is(':checked'))
                    $('.atendimento-bilingue-fields').show();
                else
                    $('.atendimento-bilingue-fields').hide();
            }).change();

            var v_AtendimentoIdiomas = $('#atendimento_bilingue_idiomas').val();
            if(v_AtendimentoIdiomas.length > 0)
            {
                v_AtendimentoIdiomas = JSON.parse(v_AtendimentoIdiomas);
                $('#atendimento_bilingue_idiomas_select').select2("val", v_AtendimentoIdiomas);
            }

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }
        });

        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosFuncionamentos();

            processaDadosAcessibilidade();

            $('#atendimento_bilingue_idiomas').val(JSON.stringify(getSelectValue($('#atendimento_bilingue_idiomas_select').val())));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }


 
    </script>
    @include('admin.util.mapScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'B7'])
@stop