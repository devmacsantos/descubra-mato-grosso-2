@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    @if(\App\UserType::isTrade())
        Transporte
    @else
        B4 - Serviço e equipamento de transporte turístico
    @endif
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
        <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
            <i class="fa fa-print"></i>
        </a>
    </ul>
@stop
@section('content')
    <div class="row visible-print">
        <div class="mb20 col-sm-12">
            <h2>B4 - Serviço e equipamento de transporte turístico</h2>
        </div>
    </div>
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    @if(!\App\UserType::isParceiro())
    {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/servicos-transporte-turistico'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
    @else
    <div id="mainForm">
    @endif

    <div class="tab-content pn br-n">
        <?php
            $v_TabLanguages = ['pt', 'en', 'es', 'fr'];
            if($p_TransportationService != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_Descriptions = $p_TransportationService->descricoes_observacoes_complementares != null ? json_decode($p_TransportationService->descricoes_observacoes_complementares,1) : $v_EmptyData;
                $v_ShortDescriptions = $p_TransportationService->descricao_curta != null ? json_decode($p_TransportationService->descricao_curta,1) : $v_EmptyData;
            }
        ?>
        @foreach($v_TabLanguages as $c_TabIndex => $c_TabLanguage)
        <div id="{{'tab' . $c_TabIndex}}" class="tab-pane {{ $c_TabIndex == 0 ? 'active' : '' }}">
            <div class="row">
                @if($c_TabLanguage == 'pt')
                    @if($p_TransportationService != null)
                        <input type="hidden" name="id" value="{{$p_TransportationService->id}}">
                        <input type="hidden" name="formulario[validade_cadastur]" value="{{$p_TransportationService->validade_cadastur}}">
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="{{$p_TransportationService->tem_cadastur}}">
                    @else
                        <input type="hidden" id="validade_cadastur" name="formulario[validade_cadastur]">
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="0">
                    @endif
                    @include('admin.util.headerIdentification', ['p_Form' => $p_TransportationService, 'p_Types' => $p_Types, 'p_WithoutSubtype' => true, 'p_CadasturActivity' => 15])

                    <input type="hidden" id="default_tipo_atividade_cadastur" value="85">
                    
                    <div class="form-group col-sm-6">
                        <label for="numero_registro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nº de registro (órgão oficial turismo/outra entidade regulação)</label>
                        <input type="number" name="formulario[numero_registro]" class="form-control" id="numero_registro" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->numero_registro}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->telefone}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="whatsapp">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Whatsapp</label>
                        <input type="text" name="formulario[whatsapp]" class="form-control phone-field" id="whatsapp" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->whatsapp}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
                        <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->site}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
                        <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->email}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="facebook">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Facebook</label>
                        <input type="url"  name="formulario[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->facebook}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="instagram">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instagram</label>
                        <input type="url"  name="formulario[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->instagram}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="twitter">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Twitter</label>
                        <input type="url"  name="formulario[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->twitter}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="youtube">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Youtube</label>
                        <input type="url"  name="formulario[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->youtube}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="tripadvisor">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tripadvisor</label>
                        <input type="url"  name="formulario[tripadvisor]" class="form-control" id="tripadvisor" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->tripadvisor}}">
                    </div>
                    <input type="hidden" id="descricao_curta" name="formulario[descricao_curta]">
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TransportationService == null ? '' : $v_ShortDescriptions[$c_TabLanguage]}}</textarea>
                    </div>

                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização e ambiência</h3>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP<span class="mandatory-field">*</span></label>
                        <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->cep}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->bairro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->logradouro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->numero}}" required>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
                        <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->complemento}}">
                        <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                            <i class="fa fa-map-marker"></i>
                        </a>
                    </div>
                    <div class="form-group col-sm-12">
                        <?php
                            $v_Locations = [
                                ''=>'',
                                'Urbana'=>'Urbana',
                                'Rururbana'=>'Rururbana',
                                'Rural'=>'Rural'
                            ];
                        ?>
                        <label for="localizacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Localização<span class="mandatory-field">*</span></label>
                        {!! Form::select('formulario[localizacao]', $v_Locations, $p_TransportationService == null ? '' : $p_TransportationService->localizacao, ['id' => 'localizacao', 'class' => 'form-control', 'required' => 'required']) !!}
                    </div>
                    <div class="form-group col-sm-12">
                        <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
                        <div id="map_canvas" style="height:320px;"></div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="latitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->latitude}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->longitude}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
                        <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
                    </div>
                    

                    @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])
                    <div class="col-sm-12">
                        <p>* É necessário ter ao menos uma foto de capa para que possa aparecer no portal.</p>
                    </div>
                @else
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Nome/Entidade</h3>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TransportationService == null ? '' : $v_ShortDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                @endif

                @include('admin.util.workingPeriod', ['p_Form' => $p_TransportationService, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage, 'p_ClosedFields' => true])

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Especificações da Frota</h3>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="quantidade_veiculos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Quantidade de veículos<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[quantidade_veiculos]" class="form-control integer-field" id="quantidade_veiculos" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->quantidade_veiculos}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="quantidade_veiculos_adaptados">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Quantidade de veículos adaptados<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[quantidade_veiculos_adaptados]" class="form-control integer-field" id="quantidade_veiculos_adaptados" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->quantidade_veiculos_adaptados}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="oferta_total_lugares_sentados">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Oferta total de lugares sentados</label>
                        <input type="text" name="formulario[oferta_total_lugares_sentados]" class="form-control integer-field" id="oferta_total_lugares_sentados" placeholder="Digite Aqui" value="{{$p_TransportationService == null ? '' : $p_TransportationService->oferta_total_lugares_sentados}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="categoria_veiculos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Categoria dos veículos</label>
                        <textarea name="formulario[categoria_veiculos]" id="categoria_veiculos" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TransportationService == null ? '' : $p_TransportationService->categoria_veiculos}}</textarea>
                    </div>

                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Serviços oferecidos</h3>
                    </div>

                    <div class="form-group col-sm-12">
                        <?php
                            $v_Coverages = [
                                    'Municipal'=>'Municipal',
                                    'Intermunicipal metropolitano'=>'Intermunicipal metropolitano',
                                    'Intermunicipal estadual'=>'Intermunicipal estadual',
                                    'Interestadual'=>'Interestadual',
                                    'Internacional continental'=>'Internacional continental',
                                    'Internacional intercontinental'=>'Internacional intercontinental'
                            ];
                        ?>
                        <label for="abrangencia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Abrangência <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                        <input type="hidden" id="abrangencia" name="formulario[abrangencia]" value="{{$p_TransportationService == null ? '' : $p_TransportationService->abrangencia}}">
                        {!! Form::select('', $v_Coverages, null, ['id' => 'abrangencia_select', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                    <div class="form-group col-sm-12">
                        <?php
                            $v_TransportationTypes = [
                                    'Excursão'=>'Excursão',
                                    'Passeio local'=>'Passeio local',
                                    'Especial'=>'Especial',
                                    'Traslado'=>'Traslado'
                            ];
                        ?>
                        <label for="tipos_servico">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipos de serviço <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="tipos_servico" name="formulario[tipos_servico]" value="{{$p_TransportationService == null ? '' : $p_TransportationService->tipos_servico}}">
                        {!! Form::select('', $v_TransportationTypes, null, ['id' => 'tipos_servico_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                @endif

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados complementares</h3>
                </div>

                @if($c_TabLanguage == 'pt')
                    <input type="hidden" id="descricoes_observacoes_complementares" name="formulario[descricoes_observacoes_complementares]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="descricoes_observacoes_complementares_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
                    <textarea id="descricoes_observacoes_complementares_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_TransportationService == null ? '' : $v_Descriptions[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    @include('admin.util.hashtags', ['p_Form' => $p_TransportationService])
                @endif

                @include('admin.util.accessibility', ['p_Form' => $p_TransportationService, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage])

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
                    </div>

                    <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_TransportationService == null ? '' : $p_TransportationService->tipos_viagem}}">
                    <div class="form-group col-sm-12">
                        <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
                        {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    @include('admin.util.tradeAndAttraction', ['p_Form' => $p_TransportationService])

                    @if($p_TransportationService == null || $p_TransportationService->revision_status_id != 6)
                        @include('admin.util.revision', ['p_Form' => $p_TransportationService, 'p_Publish' => true])
                    @endif

                    @include('admin.util.responsibleTeam', ['p_Form' => $p_TransportationService])
                @endif
            </div>
        </div>
        @endforeach

        @if(!\App\UserType::isParceiro())
            <div class="row">
                <div class="form-group col-sm-12">
                    <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
                </div>
            </div>
        @endif
    </div>
    @if(!\App\UserType::isParceiro())
    {!! Form::close() !!}
    @else
    </div>
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.workingPeriodScript', ['p_Translation' => true])
    @include('admin.util.headerIdentificationScript', ['p_WithoutSubtype' => true])
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            carregaDadosLocalizacao();

            @if(\App\UserType::isTrade())
                $('#cnpj').attr('disabled', true);
                @if($p_TransportationService == null)
                    var v_Data = localStorage.getItem("newTradeItem");
                    if(v_Data != null) {
                        localStorage.removeItem("newTradeItem");
                        v_Data = JSON.parse(v_Data);
                        $('#cnpj').val(v_Data.cnpj).blur();
                        $('#tipo_atividade_cadastur').val(v_Data.tipoAtividade);
                        $('#validade_cadastur').val(v_Data.validade + ' 00:00:00');
                        if(v_Data.token.length > 0)
                            v_CadasturToken = v_Data.token;
                        if(v_CadasturToken.length > 0)
                            loadCadasturData();
                    }
                    else
                        location.href = '{{ url('admin/inventario/servicos-transporte-turistico') }}';
                @endif
            @endif

            @if($p_TransportationService != null && $p_TransportationService->revision_status_id == 6)
                $.confirm({
                    text: 'Este cadastro está vencido no CADASTUR. Atualize-o para reativar seu cadastro no Descubra Mato Grosso. Saiba mais: www.cadastur.turismo.mg.gov.br',
                    title: 'Atenção',
                    confirmButton: 'OK',
                    cancelButton: 'Não',
                    cancelButtonClass:'hidden'
                });
            @endif

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }
        });

        function carregaDadosLocalizacao()
        {
            carregaSelect('abrangencia');
            carregaSelect('tipos_servico');
        }

        function carregaSelect(p_Campo)
        {
            var v_Dados = $('#'+p_Campo).val();
            if(v_Dados.length > 0)
            {
                v_Dados = JSON.parse(v_Dados);
                $('#'+p_Campo+'_select').select2("val", v_Dados);
            }
        }

        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosLocalizacao();

            processaDadosFuncionamentos();

            processaDadosAcessibilidade();


            <?php $v_Languages = ['pt', 'en', 'es', 'fr']; ?>

            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            @foreach($v_Languages as $c_Language)
                v_DescriptionsJson.{{$c_Language}} = $('#descricoes_observacoes_complementares_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
            @endforeach
            $('#descricoes_observacoes_complementares').val(JSON.stringify(v_DescriptionsJson));
            $('#descricao_curta').val(JSON.stringify(v_ShortDescriptionsJson));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            var v_Hashtags = $('#hashtags_select').val();
            $('#hashtags').val(v_Hashtags == null ? '' : v_Hashtags.join(';'));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

        function processaDadosLocalizacao()
        {
            processaSelect('abrangencia');
            processaSelect('tipos_servico');
        }

        function processaSelect(p_Campo)
        {
            var v_Dados = $('#'+p_Campo+'_select').val();
            $('#'+p_Campo).val(JSON.stringify(v_Dados == null ? [] : v_Dados));
        }

    </script>
    @include('admin.util.mapScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @if($p_TransportationService == null || $p_TransportationService->revision_status_id != 6)
    @include('admin.util.revisionScript')
    @endif
    @include('admin.util.cadastur30')
    @include('admin.util.tooltipScript', ['p_Type' => 'B4'])
@stop