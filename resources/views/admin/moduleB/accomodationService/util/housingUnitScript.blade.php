<script>
    $(document).ready(function()
    {
        carregaDadosUHs();
    });

    function carregaDadosUHs()
    {
        carregaCamposUHs('unidades_habitacionais', 'uh-fields');
        carregaCamposUHs('unidades_habitacionais_facilidades', 'uh-facilities-fields');

        var v_UHVoltages = $('#unidades_habitacionais_voltagem').val();
        if(v_UHVoltages.length > 0)
        {
            v_UHVoltages = JSON.parse(v_UHVoltages);
            var v_SelectedItems = [];
            $(v_UHVoltages).each(function(){
                v_SelectedItems.push(this.id);
            });
            $('#unidades_habitacionais_voltagem_select').select2("val", v_SelectedItems);
        }
    }

    function carregaCamposUHs(p_Id, p_DivClass)
    {
        var v_Dados = $('#'+p_Id).val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                if(this.tipo == 'checkbox')
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                else
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').val(this.valor);
            });
        }
    }

    function processaDadosUHs()
    {
        processaCamposUHs('unidades_habitacionais', 'uh-fields');
        processaCamposUHs('unidades_habitacionais_facilidades', 'uh-facilities-fields');

        var v_UHVoltages = [];
        $('#unidades_habitacionais_voltagem_select option:selected').each(function(){
            v_UHVoltages.push({
                id:$(this).attr('value'),
                nome:$(this).text()
            });
        });
        $('#unidades_habitacionais_voltagem').val(JSON.stringify(v_UHVoltages));
    }

    function processaCamposUHs(p_Id, p_DivClass)
    {
        var v_Dados = [];
        $('.'+p_DivClass+' .field').each(function(){
            v_Dados.push({
                nome:$(this).attr('rel'),
                tipo:$(this).hasClass('checkbox') ? 'checkbox' : 'text-or-select',
                valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
            });
        });
        $('#'+p_Id).val(JSON.stringify(v_Dados));
    }
</script>