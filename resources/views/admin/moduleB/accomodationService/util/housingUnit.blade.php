<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Unidades habitacionais</h3>
</div>

<?php
    $v_UHFields = [
        'total'=>'Total',
        'suites'=>'Suítes (com sala de estar)',
        'apartamentos'=>'Apartamentos com banheiro privativo',
        'quartos'=>'Quartos (sem banheiro privativo)',
        'chales'=>'Chalés',
        'camas_extras'=>'Total de camas extras (móveis em depósito)',
        'uhs_adaptadas'=>'UHs adaptadas para pessoas com deficiência',
        'uhs_adaptadas_pessoas_obesas'=>'UHs adaptadas para pessoas obesas'
    ];
?>
<input type="hidden" id="unidades_habitacionais" name="formulario[unidades_habitacionais]" value="{{$p_Form == null ? '' : $p_Form->unidades_habitacionais}}">
<div class="uh-fields">
    @foreach($v_UHFields as $c_Id => $c_Field)
        <div class="uh-field" rel="{{$c_Id}}">
            <div class="col-sm-12">
                <h4>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}</h4>
            </div>
            <div class="form-group col-sm-6">
                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Número de UHs
                    @if($c_Id == 'total')
                        <span class="mandatory-field">*</span>
                    @endif
                </label>
                <input type="number" min="0" rel="Número de UHs - {{$c_Field}}" class="form-control field" placeholder="Digite Aqui" {{$c_Id == 'total' ? 'required' : ''}}>
            </div>
            <div class="form-group col-sm-6">
                <label for="total_leitos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Número de Leitos (Total de vagas disponíveis)
                    @if($c_Id == 'total')
                        <span class="mandatory-field">*</span>
                    @endif
                </label>
                <input type="number" min="0" rel="Número de Leitos - {{$c_Field}}" class="form-control field" placeholder="Digite Aqui" {{$c_Id == 'total' ? 'required' : ''}}>
            </div>
        </div>
    @endforeach
</div>

<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Facilidades nas UHs</h3>
</div>
<?php
    $v_UHFacilitiesFields = [
        'tv'=>'TV',
        'tv_assinatura'=>'TV por assinatura',
        'radio'=>'Rádio',
        'internet'=>'Internet',
        'wifi'=>'Wi Fi',
        'wifi_liberada'=>'Wi Fi liberada (free zone)',
        'telefone'=>'Telefone',
        'frigobar'=>'Frigobar',
        'ar_condicionado'=>'Ar condicionado',
        'calefacao'=>'Calefação',
        'ventilador'=>'Ventilador',
        'lareira'=>'Lareira',
        'cofre'=>'Cofre',
        'secador_cabelo'=>'Secador de cabelo',
        'shampoo_condicionador'=>'Shampoo/Condicionador',
        'rede_protecao'=>'Rede de proteção'
    ];
?>
<input type="hidden" id="unidades_habitacionais_facilidades" name="formulario[unidades_habitacionais_facilidades]" value="{{$p_Form == null ? '' : $p_Form->unidades_habitacionais_facilidades}}">
<div class="uh-facilities-fields">
    @foreach($v_UHFacilitiesFields as $c_Id => $c_Field)
        <div class="form-group col-sm-3">
            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
            <p><input type="checkbox" rel="{{$c_Id}}" class="ml5 mt10 checkbox field"></p>
        </div>
    @endforeach
</div>
<div class="form-group col-sm-12">
    <label for="unidades_habitacionais_facilidades_outras">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outras</label>
    <textarea name="formulario[unidades_habitacionais_facilidades_outras]" id="unidades_habitacionais_facilidades_outras" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Form == null ? '' : $p_Form->unidades_habitacionais_facilidades_outras}}</textarea>
</div>

<div class="form-group col-sm-12">
    <label for="unidades_habitacionais_voltagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Voltagem <i>(permite mais de uma opção)</i></label>
    <input type="hidden" id="unidades_habitacionais_voltagem" name="formulario[unidades_habitacionais_voltagem]" value="{{$p_Form == null ? '' : $p_Form->unidades_habitacionais_voltagem}}">
    {!! Form::select('', $p_Voltages, null, ['id' => 'unidades_habitacionais_voltagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
</div>