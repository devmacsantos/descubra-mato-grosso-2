<script>
    $(document).ready(function()
    {
        carregaDadosServicosEquipamentos();
        $('#restaurante_aberto_publico').change(function(){
            if($(this).is(':checked'))
                $('.restaurant-fields').hide();
            else
                $('.restaurant-fields').show();
        }).change();
    });

    function carregaDadosServicosEquipamentos()
    {
        carregaCamposServicosEquipamentos('servicos_equipamentos_area_social', 'social-area-fields');
        carregaCamposServicosEquipamentos('servicos_equipamentos_recreacao_lazer', 'recreation-fields');
    }

    function carregaCamposServicosEquipamentos(p_Id, p_DivClass)
    {
        var v_Dados = $('#'+p_Id).val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                if(this.tipo == 'checkbox')
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                else
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').val(this.valor);
            });
        }
    }

    function processaDadosServicosEquipamentos()
    {
        processaCamposServicosEquipamentos('servicos_equipamentos_area_social', 'social-area-fields');
        processaCamposServicosEquipamentos('servicos_equipamentos_recreacao_lazer', 'recreation-fields');
    }

    function processaCamposServicosEquipamentos(p_Id, p_DivClass)
    {
        var v_Dados = [];
        $('.'+p_DivClass+' .field').each(function(){
            v_Dados.push({
                nome:$(this).attr('rel'),
                tipo:$(this).hasClass('checkbox') ? 'checkbox' : 'text-or-select',
                valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
            });
        });
        $('#'+p_Id).val(JSON.stringify(v_Dados));
    }
</script>