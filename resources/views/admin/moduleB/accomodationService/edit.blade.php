@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    @if(\App\UserType::isTrade())
        Hospedagem
    @else
        B1 - Serviço e equipamento de hospedagem
    @endif
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
        <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
            <i class="fa fa-print"></i>
        </a>
    </ul>
@stop
@section('content')
    <div class="row visible-print">
        <div class="mb20 col-sm-12">
            <h2>B1 - Serviço e equipamento de hospedagem</h2>
        </div>
    </div>
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    @if(!\App\UserType::isParceiro())
    {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/servicos-hospedagem'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
    @else
    <div id="mainForm">
    @endif

    <div class="tab-content pn br-n">
        <?php
            $v_TabLanguages = ['pt', 'en', 'es', 'fr'];
            if($p_AccomodationService != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_SurroundingsDescriptions = $p_AccomodationService->descricao_arredores_distancia_pontos != null ? json_decode($p_AccomodationService->descricao_arredores_distancia_pontos,1) : $v_EmptyData;
                $v_ShortDescriptions = $p_AccomodationService->descricao_curta != null ? json_decode($p_AccomodationService->descricao_curta,1) : $v_EmptyData;
                $v_DiariaTypes = $p_AccomodationService->tipo_diaria_outros != null ? json_decode($p_AccomodationService->tipo_diaria_outros,1) : $v_EmptyData;
                $v_Descriptions = $p_AccomodationService->descricoes_observacoes_complementares != null ? json_decode($p_AccomodationService->descricoes_observacoes_complementares,1) : $v_EmptyData;
            }
        ?>
        @foreach($v_TabLanguages as $c_TabIndex => $c_TabLanguage)
        <div id="{{'tab' . $c_TabIndex}}" class="tab-pane {{ $c_TabIndex == 0 ? 'active' : '' }}">
            <div class="row">
                @if($c_TabLanguage == 'pt')
                    @if($p_AccomodationService != null)
                        <input type="hidden" name="id" value="{{$p_AccomodationService->id}}">
                        <input type="hidden" name="formulario[validade_cadastur]" value="{{$p_AccomodationService->validade_cadastur}}">
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="{{$p_AccomodationService->tem_cadastur}}">
                    @else
                        <input type="hidden" id="validade_cadastur" name="formulario[validade_cadastur]">
                        <input type="hidden" id="tem_cadastur" name="formulario[tem_cadastur]" value="0">
                    @endif
                    @include('admin.util.headerIdentification', ['p_Form' => $p_AccomodationService, 'p_Types' => $p_Types, 'p_CadasturActivityOptions' => [''=>'',20=>'Meio de Hospedagem',35=>'Acampamento Turístico']])
                    
                    <input type="hidden" id="default_tipo_atividade_cadastur" value="20">
                    
                    <div class="form-group col-sm-6">
                        <label for="data_tombamento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Data do tombamento</label>
                        <input type="text" name="formulario[data_tombamento]" class="form-control date-field" id="data_tombamento" placeholder="Digite Aqui" value="{{($p_AccomodationService == null || $p_AccomodationService->data_tombamento == null || $p_AccomodationService->data_tombamento == '') ? '' : \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_AccomodationService->data_tombamento)->format('d/m/Y')}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="organizacao_responsavel">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Organização responsável pelo tombamento</label>
                        <input type="text" name="formulario[organizacao_responsavel]" class="form-control" id="organizacao_responsavel" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->organizacao_responsavel}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->telefone}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="whatsapp">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Whatsapp</label>
                        <input type="text" name="formulario[whatsapp]" class="form-control phone-field" id="whatsapp" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->whatsapp}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
                        <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->site}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
                        <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->email}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="facebook">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Facebook</label>
                        <input type="url"  name="formulario[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->facebook}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="instagram">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Instagram</label>
                        <input type="url"  name="formulario[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->instagram}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="twitter">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Twitter</label>
                        <input type="url"  name="formulario[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->twitter}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="youtube">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Youtube</label>
                        <input type="url"  name="formulario[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->youtube}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="tripadvisor">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tripadvisor</label>
                        <input type="url"  name="formulario[tripadvisor]" class="form-control" id="tripadvisor" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->tripadvisor}}">
                    </div>

                    <input type="hidden" id="descricao_curta" name="formulario[descricao_curta]">
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_AccomodationService == null ? '' : $v_ShortDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                @else
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Nome/Entidade</h3>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição curta</label>
                        <textarea id="descricao_curta_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_AccomodationService == null ? '' : $v_ShortDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                @endif


                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização e ambiência</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-6">
                        <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP<span class="mandatory-field">*</span></label>
                        <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->cep}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->bairro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->logradouro}}" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
                        <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->numero}}" required>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
                        <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->complemento}}">
                        <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                            <i class="fa fa-map-marker"></i>
                        </a>
                    </div>

                    <input type="hidden" id="descricao_arredores_distancia_pontos" name="formulario[descricao_arredores_distancia_pontos]">
                    <div class="form-group col-sm-12">
                        <label for="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição dos arredores e distância dos principais pontos turísticos<span class="mandatory-field">*</span>
                        </label>
                        <textarea id="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_AccomodationService == null ? '' : $v_SurroundingsDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <?php
                            $v_Locations = [
                                ''=>'',
                                'Urbana'=>'Urbana',
                                'Rururbana'=>'Rururbana',
                                'Rural'=>'Rural'
                            ];
                        ?>
                        <label for="localizacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Localização<span class="mandatory-field">*</span></label>
                        {!! Form::select('formulario[localizacao]', $v_Locations, $p_AccomodationService == null ? '' : $p_AccomodationService->localizacao, ['id' => 'localizacao', 'class' => 'form-control', 'required' => 'required']) !!}
                    </div>
                    <div class="form-group col-sm-12">
                        <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
                        <div id="map_canvas" style="height:320px;"></div>
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="latitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->latitude}}">
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
                        <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->longitude}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
                        <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
                    </div>

                    @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])
                    <div class="col-sm-12">
                        <p>* É necessário ter ao menos uma foto de capa para que possa aparecer no portal.</p>
                    </div>
                @endif

                @if($c_TabLanguage != 'pt')
                    <div class="form-group col-sm-12">
                        <label for="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrição dos arredores e distância dos principais pontos turísticos</label>
                        <textarea id="descricao_arredores_distancia_pontos_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_AccomodationService == null ? '' : $v_SurroundingsDescriptions[$c_TabLanguage]}}</textarea>
                    </div>
                @endif

                @include('admin.util.workingPeriod', ['p_Form' => $p_AccomodationService, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage, 'p_ClosedFields' => true])


                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <label for="meses_maior_ocupacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Meses de maior ocupação <i>(permite mais de uma opção)</i></label>
                        <?php $v_MonthRange = $p_AccomodationService == null ? [] : explode(';', $p_AccomodationService->meses_maior_ocupacao); ?>
                        {!! Form::select('meses_maior_ocupacao[]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, ['id' => 'meses_maior_ocupacao', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
                    </div>

                    @include('admin.moduleB.accomodationService.util.housingUnit', ['p_Form' => $p_AccomodationService, 'p_Voltages' => $p_Voltages])

                    @include('admin.moduleB.accomodationService.util.extraHotelAccomodation', ['p_Form' => $p_AccomodationService, 'p_Voltages' => $p_Voltages])
                @endif

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Tipo de diária</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <?php
                        $v_DiariaOptions = [
                                'Sem café'=>'Sem café',
                                'Com café'=>'Com café',
                                'Meia pensão'=>'Meia pensão',
                                'Pensão completa'=>'Pensão completa'
                        ];
                    ?>
                    <div class="form-group col-sm-12">
                        <label for="tipo_diaria">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="tipo_diaria" name="formulario[tipo_diaria]" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->tipo_diaria}}">
                        {!! Form::select('', $v_DiariaOptions, null, ['id' => 'tipo_diaria_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="valor_medio_diaria">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Valor médio das diárias - R$</label>
                        <input type="text" name="formulario[valor_medio_diaria]" class="form-control currency-field" id="valor_medio_diaria" placeholder="Digite Aqui" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->valor_medio_diaria}}" required>
                    </div>
                    <div class="col-sm-12"></div>
                    <input type="hidden" id="tipo_diaria_outros" name="formulario[tipo_diaria_outros]">
                @endif

                <div class="form-group col-sm-12">
                    <label for="tipo_diaria_outros_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outros</label>
                    <textarea id="tipo_diaria_outros_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_AccomodationService == null ? '' : $v_DiariaTypes[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <label for="formas_pagamento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Formas de pagamento <i>(permite mais de uma opção)</i></label>
                        <input type="hidden" id="formas_pagamento" name="formulario[formas_pagamento]" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->formas_pagamento}}">
                        {!! Form::select('', $p_PaymentMethods, null, ['id' => 'formas_pagamento_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    @include('admin.moduleB.accomodationService.util.accomodationServicesEquipments', ['p_Form' => $p_AccomodationService])
                @endif

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Modalidades complementares</h3>
                    </div>
                    <?php
                        $v_DescriptionFields = [
                                'aceita_animais'=>'Aceita animais - PET',
                                'nao_aceita_criancas'=>'Adequado para crianças'
                        ];
                    ?>
                    @foreach($v_DescriptionFields as $c_Id => $c_Field)
                        <div class="form-group col-sm-3">
                            <label for="{{$c_Id}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}{{$c_Field}}?</label>
                            <p><input type="checkbox" value="1" name="formulario[{{$c_Id}}]" id="{{$c_Id}}" class="ml5 mt10" {{($p_AccomodationService == null || $p_AccomodationService[$c_Id] == 0) ? '' : 'checked'}}></p>
                        </div>
                    @endforeach
                    <div class="form-group col-sm-12">
                        <label for="restricoes_hospedes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Restrições aos hóspedes</label>
                        <textarea name="formulario[restricoes_hospedes]" id="restricoes_hospedes" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_AccomodationService == null ? '' : $p_AccomodationService->restricoes_hospedes}}</textarea>
                    </div>
                @endif

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
                </div>
                @if($c_TabLanguage == 'pt')
                    <input type="hidden" id="descricoes_observacoes_complementares" name="formulario[descricoes_observacoes_complementares]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="descricoes_observacoes_complementares_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
                    <textarea id="descricoes_observacoes_complementares_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_AccomodationService == null ? '' : $v_Descriptions[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    @include('admin.util.hashtags', ['p_Form' => $p_AccomodationService])

                    @if(!\App\UserType::isTrade())
                        <div class="form-group col-sm-12">
                            <label for="possui_espacos_eventos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui espaços de eventos?</label>
                            <p><input type="checkbox" value="1" name="formulario[possui_espacos_eventos]" id="possui_espacos_eventos" class="ml5 mt10" {{($p_AccomodationService == null || $p_AccomodationService->possui_espacos_eventos == 0) ? '' : 'checked'}}></p>
                        </div>
                    @endif
                @endif

                @include('admin.util.accessibility', ['p_Form' => $p_AccomodationService, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage])

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
                    </div>

                    <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_AccomodationService == null ? '' : $p_AccomodationService->tipos_viagem}}">
                    <div class="form-group col-sm-12">
                        <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo</label>
                        {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    @include('admin.util.tradeAndAttraction', ['p_Form' => $p_AccomodationService])

                    @if($p_AccomodationService == null || $p_AccomodationService->revision_status_id != 6)
                        @include('admin.util.revision', ['p_Form' => $p_AccomodationService, 'p_Publish' => true])
                    @endif

                    @include('admin.util.responsibleTeam', ['p_Form' => $p_AccomodationService])
                @endif
            </div>
        </div>
        @endforeach

        @if(!\App\UserType::isParceiro())
        <div class="row">
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
            </div>
        </div>
        @endif
    </div>
    @if(!\App\UserType::isParceiro())
    {!! Form::close() !!}
    @else
    </div>
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.workingPeriodScript', ['p_Translation' => true])
    @include('admin.util.headerIdentificationScript', ['p_CadasturActivities' => $p_CadasturActivities])
    @include('admin.moduleB.accomodationService.util.accomodationServicesEquipmentsScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            @if($p_AccomodationService == null)
            $('.dia-funcionamento-24-horas').prop("checked", true).trigger('change');
            @endif

            $('#meses_maior_ocupacao').change(function(){
                var v_Field = this;
                var v_Selection = $(v_Field).val();
                if($.inArray('Todos', v_Selection) > -1)
                {
                    if($(v_Field).find('option:selected').length == 13)
                        $(v_Field).select2("val", "");
                    else
                        $(v_Field).select2("val", ['1','2','3','4','5','6','7','8','9','10','11','12']);
                }
            });

            var v_TiposDiaria = $('#tipo_diaria').val();
            if(v_TiposDiaria.length > 0)
            {
                v_TiposDiaria = JSON.parse(v_TiposDiaria);
                $('#tipo_diaria_select').select2("val", v_TiposDiaria);
            }

            var v_PaymentMethods = $('#formas_pagamento').val();
            if(v_PaymentMethods.length > 0)
            {
                v_PaymentMethods = JSON.parse(v_PaymentMethods);
                var v_SelectedItems = [];
                $(v_PaymentMethods).each(function(){
                    v_SelectedItems.push(this.id);
                });
                $('#formas_pagamento_select').select2("val", v_SelectedItems);
            }

            @if(\App\UserType::isTrade())
                $('#cnpj').attr('disabled', true);
                @if($p_AccomodationService == null)
                    var v_Data = localStorage.getItem("newTradeItem");
                    if(v_Data != null) {
                        localStorage.removeItem("newTradeItem");
                        v_Data = JSON.parse(v_Data);
                        $('#cnpj').val(v_Data.cnpj).blur();
                        $('#tipo_atividade_cadastur').val(v_Data.tipoAtividade);
                        $('#validade_cadastur').val(v_Data.validade + ' 00:00:00');
                        if(v_Data.token.length > 0)
                            v_CadasturToken = v_Data.token;
                        if(v_CadasturToken.length > 0)
                            loadCadasturData();
                    }
                    else
                        location.href = '{{ url('admin/inventario/servicos-hospedagem') }}';
                @endif
            @endif

            @if($p_AccomodationService != null && $p_AccomodationService->revision_status_id == 6)
                $.confirm({
                    text: 'Este cadastro está vencido no CADASTUR. Atualize-o para reativar seu cadastro no Descubra Mato Grosso. Saiba mais: www.cadastur.turismo.mg.gov.br',
                    title: 'Atenção',
                    confirmButton: 'OK',
                    cancelButton: 'Não',
                    cancelButtonClass:'hidden'
                });
            @endif

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }
        });

        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

//            $('.emails-field').each(function(c_Key, c_Field)
//            {
//                var v_Values = $(c_Field).val();
//                var v_Value = '';
//                $(v_Values).each(function(){
//                    v_Value += this + ';';
//                });
//                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
//            });

            processaDadosFuncionamentos();

            processaDadosUHs();

            processaDadosAcessibilidade();

            processaDadosServicosEquipamentos();

            processaDadosMeiosHospedagemExtraHoteleiros();

            var v_PaymentMethods = [];
            $('#formas_pagamento_select option:selected').each(function(){
                v_PaymentMethods.push({
                    id:$(this).attr('value'),
                    nome:$(this).text()
                });
            });
            $('#formas_pagamento').val(JSON.stringify(v_PaymentMethods));


            <?php $v_Languages = ['pt', 'en', 'es', 'fr']; ?>

            var v_SurroundingsJson = {};
            var v_DiariaTypesJson = {};
            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            @foreach($v_Languages as $c_Language)
                v_SurroundingsJson.{{$c_Language}} = $('#descricao_arredores_distancia_pontos_{{$c_Language}}').val();
                v_DiariaTypesJson.{{$c_Language}} = $('#tipo_diaria_outros_{{$c_Language}}').val();
                v_DescriptionsJson.{{$c_Language}} = $('#descricoes_observacoes_complementares_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
            @endforeach
            $('#descricao_arredores_distancia_pontos').val(JSON.stringify(v_SurroundingsJson));
            $('#tipo_diaria_outros').val(JSON.stringify(v_DiariaTypesJson));
            $('#descricoes_observacoes_complementares').val(JSON.stringify(v_DescriptionsJson));
            $('#descricao_curta').val(JSON.stringify(v_ShortDescriptionsJson));

            $('#tipo_diaria').val(JSON.stringify(getSelectValue($('#tipo_diaria_select').val())));
            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            var v_Hashtags = $('#hashtags_select').val();
            $('#hashtags').val(v_Hashtags == null ? '' : v_Hashtags.join(';'));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

    </script>
    @include('admin.util.mapScript')
    @include('admin.moduleB.accomodationService.util.housingUnitScript')
    @include('admin.moduleB.accomodationService.util.extraHotelAccomodationScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.cadastur30')
    @if($p_AccomodationService == null || $p_AccomodationService->revision_status_id != 6)
    @include('admin.util.revisionScript')
    @endif
    @include('admin.util.tooltipScript', ['p_Type' => 'B1'])
@stop