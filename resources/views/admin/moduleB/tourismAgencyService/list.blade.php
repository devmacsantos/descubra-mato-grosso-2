@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
@stop
@section('panel-header')
    @if(\App\UserType::isTrade())
        Agenciamento
    @else
        B3 - Serviços e equipamentos de agências de turismo
    @endif
    @if(!\App\UserType::isParceiro() && !\App\UserType::isTrade())
    <a href="{{ url('admin/inventario/servicos-agencias-turismo/editar') }}">
        <button class="btn btn-success pull-right" title="Nova entrada">
            <i class="fa fa-plus"></i>
        </button>
    </a>
    @endif
@stop

@if(\App\UserType::isTrade())
@section('list-above-table-content')
    <div class="mb15 pv5 ph15 new-trade">
        <h4>Novo cadastro</h4>
        <div class="row">
            <input type="hidden" id="tipo_atividade_cadastur" value="10">
            <div class="form-group col-sm-6">
                <label for="cnpj" style="width: 100%">CNPJ</label>
                <input type="text" class="form-control cnpj-field cnpj-field-with-cadastur" id="search-cnpj" placeholder="Digite Aqui">

                <a title="Pesquisar" type="button" class="btn btn-success search-cnpj-cadastur" onclick="searchData()">
                    <i class="fa fa-search"></i>
                    <img style="display:none;margin:auto -1px;height:14px" src="{{url('/assets/img/loading.gif')}}">
                </a>
            </div>
        </div>
    </div>
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/solicitar-posse-cnpj'))) !!}
        <input type="hidden" name="tipo" value="B3">
        <input type="hidden" id="cnpj" name="cnpj">
    {!! Form::close() !!}
@stop
@endif

@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Município</th>
        <th>Data de cadastro</th>
        <th>Atualizado em</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td>{!! Form::select('', ['' => 'Selecione'] + $p_Status, null, ['class' => 'form-control']) !!}</td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/inventario/servicos-agencias-turismo')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
    @include('admin.util.tradeListScript', [
        'p_Type'=>'B3',
        'p_EditUrl' => url('admin/inventario/servicos-agencias-turismo/editar'),
        'p_RequiresCadastur' => true
    ])
@stop