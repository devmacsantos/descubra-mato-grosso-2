<script>
    $(document).ready(function()
    {
        carregaDadosServicosEquipamentos();

        $('#modalContainer').append($('#physicalSpaceModal'));
        $('#espaco_fisico_formatos').change(function(){
            $('#physicalSpaceModal .format-capacity-fields').hide();
            $('#physicalSpaceModal .format-capacity-fields input').removeAttr('required');
            var v_Selection = $(this).select2('data');
            $(v_Selection).each(function(c_Index){
                $('#physicalSpaceModal .format-capacity-fields[rel="' + this.id  + '"]').show();
                $('#physicalSpaceModal .format-capacity-fields[rel="' + this.id  + '"] input').attr('required', true);
            });
            setTimeout(function(){
                $('#physicalSpaceModal .format-capacity-fields:hidden input').val('');
            },500);
        });

        $('.new-delete-btn').removeClass('new-delete-btn').popConfirm({
            title: "Confirmar exclusão",
            content: "Tem certeza de que deseja excluir esta entrada?",
            placement: "left",
            yesBtn: "Sim",
            noBtn: "Não",
            container: false
        });
    });

    function carregaDadosServicosEquipamentos()
    {
        carregaCamposServicosEquipamentosEspacoFisico();
        carregaCamposServicosEquipamentos('servicos_equipamentos_apoio','servicos-equipamentos-apoio-fields');
        $('.servicos-equipamentos-apoio-fields .sanitario-fields input').change(function(){
            var v_Field = $(this).val();
            if($(this).is(':checked'))
                $('.servicos-equipamentos-apoio-fields .' + v_Field).show();
            else{
                $('.servicos-equipamentos-apoio-fields .' + v_Field).hide();
                $('.servicos-equipamentos-apoio-fields .' + v_Field + ' input').val('');
            }
        }).change();
    }

    function carregaCamposServicosEquipamentosEspacoFisico()
    {
        var v_Dados = $('#espaco_fisico').val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                addPhysicalSpaceItem(this, false);
            });
        }
    }

    function carregaCamposServicosEquipamentos(p_Id, p_DivClass)
    {
        var v_Dados = $('#'+p_Id).val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                if(this.tipo == 'checkbox')
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                else
                    $('.'+p_DivClass+' .field[rel="'+this.nome+'"]').val(this.valor);
            });
        }
    }

    function processaDadosServicosEquipamentos()
    {
        processaCamposServicosEquipamentosEspacoFisico();
        processaCamposServicosEquipamentos('servicos_equipamentos_apoio','servicos-equipamentos-apoio-fields');
    }

    function processaCamposServicosEquipamentosEspacoFisico()
    {
        var v_Dados = [];
        $('.espaco-fisico-fields .field').each(function(){
            v_Dados.push(JSON.parse($(this).val()));
        });
        $('#espaco_fisico').val(JSON.stringify(v_Dados));
    }

    function processaCamposServicosEquipamentos(p_Id, p_DivClass)
    {
        var v_Dados = [];
        $('.'+p_DivClass+' .field').each(function(){
            v_Dados.push({
                nome:$(this).attr('rel'),
                tipo:$(this).hasClass('checkbox') ? 'checkbox' : 'text-or-select',
                valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
            });
        });
        $('#'+p_Id).val(JSON.stringify(v_Dados));
    }

    function editPhysicalSpaceItem(p_Button)
    {
        $('#tablePhysicalSpace .btn.editing').removeClass('editing');
        $('#physicalSpaceModal').modal();
        if(p_Button == '')
            $('#physicalSpaceModal input, #physicalSpaceModal select').val('').change();
        else {
            $(p_Button).addClass('editing');
            var v_Item = $(p_Button).siblings('.field').val();
            v_Item = JSON.parse(v_Item);

            $('#espaco_fisico_nome').val(v_Item.nome);
            $('#espaco_fisico_tipo').val(v_Item.tipo);
            $('#espaco_fisico_area').val(v_Item.area);
            $('#espaco_fisico_pe_direito').val(v_Item.peDireito);
            $('#espaco_fisico_dimensoes_c').val(v_Item.dimensoesC);
            $('#espaco_fisico_dimensoes_l').val(v_Item.dimensoesL);
            $('#espaco_fisico_capacidade').val(v_Item.capacidade);
            $('#espaco_fisico_espaco').val(v_Item.espaco);

            var v_Formatos = [];
            $(v_Item.formatos).each(function(){
                v_Formatos.push(this.nome);
                $('#espaco_fisico_capacidade_' + this.id).val(this.capacidade).change();
            });
            $('#espaco_fisico_formatos').val(v_Formatos).change();
        }
    }

    function submitPhysicalSpace()
    {
        if($('#physicalSpaceModalForm')[0].checkValidity()){
            var v_Item = {
                nome:$('#espaco_fisico_nome').val(),
                tipo:$('#espaco_fisico_tipo').val(),
                area:$('#espaco_fisico_area').val(),
                peDireito:$('#espaco_fisico_pe_direito').val(),
                dimensoesC:$('#espaco_fisico_dimensoes_c').val(),
                dimensoesL:$('#espaco_fisico_dimensoes_l').val(),
                capacidade:$('#espaco_fisico_capacidade').val(),
                espaco:$('#espaco_fisico_espaco').val(),
                formatos:[]
            };

            var v_FormatSelection = $('#espaco_fisico_formatos').select2('data');
            $(v_FormatSelection).each(function(){
                var $v_Input = $('#physicalSpaceModal .format-capacity-fields[rel="' + this.id  + '"] input');
                var v_Id = $v_Input.attr('id').split('espaco_fisico_capacidade_')[1];
                v_Item.formatos.push({id:v_Id, nome:this.id, capacidade:$v_Input.val()})
            });

            addPhysicalSpaceItem(v_Item, ($('#tablePhysicalSpace .btn.editing').length > 0));
            $('#physicalSpaceModal').modal('hide');
        }
        return false;
    }

    function addPhysicalSpaceItem(p_Item, p_IsEditing)
    {
        var v_Formatos = '';
        $(p_Item.formatos).each(function(){
            v_Formatos += '<p>' + this.nome + ' - Capacidade: ' + this.capacidade + '</p>';
        });
        var v_TableHTML =
                '<tr class="item-line new-line">' +
                    '<td>' + p_Item.nome + '</td>' +
                    '<td>' + p_Item.tipo + '</td>' +
                    '<td>' + p_Item.area + '</td>' +
                    '<td>' + p_Item.peDireito + '</td>' +
                    '<td>' + p_Item.dimensoesC + 'x' + p_Item.dimensoesL + '</td>' +
                    '<td>' + p_Item.capacidade + '</td>' +
                    '<td>' + p_Item.espaco + '</td>' +
                    '<td>' + v_Formatos + '</td>' +
                    '<td class="table-actions">' +
                        '<input type="hidden" class="field">' +
                        '<button type="button" title="Editar" class="btn btn-success" onclick="editPhysicalSpaceItem(this)"><i class="fa fa-edit"></i></button>' +
                        '<button type="button" title="Excluir" class="btn btn-success delete-btn new-delete-btn" onclick="$(this).closest(\'tr\').remove()"><i class="fa fa-trash-o"></i></button>' +
                    '</td>' +
                '</tr>';

        if(p_IsEditing){
            var $v_TR = $('#tablePhysicalSpace .btn.editing').closest('tr');
            $v_TR.replaceWith(v_TableHTML)
        }
        else
            $('#tablePhysicalSpaceNewItem').before(v_TableHTML);

        $('#tablePhysicalSpace .item-line.new-line .field').val(JSON.stringify(p_Item));
        $('#tablePhysicalSpace .item-line.new-line').removeClass('new-line');

        $('.new-delete-btn').removeClass('new-delete-btn').popConfirm({
            title: "Confirmar exclusão",
            content: "Tem certeza de que deseja excluir esta entrada?",
            placement: "left",
            yesBtn: "Sim",
            noBtn: "Não",
            container: false
        });
    }
</script>