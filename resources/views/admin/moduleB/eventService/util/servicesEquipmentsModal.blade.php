<!-- Modal -->
<style>
    .select2-dropdown{
        z-index: 9000 !important;
    }
</style>
<div class="modal fade" id="physicalSpaceModal" tabindex="-1" role="dialog" aria-labelledby="physicalSpaceModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="physicalSpaceModalLabel">Espaço fisico</h4>
            </div>
            {!! Form::open(['id' => 'physicalSpaceModalForm', 'onsubmit' => 'return submitPhysicalSpace()']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="espaco_fisico_nome">Nome<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control" id="espaco_fisico_nome" placeholder="Digite Aqui" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <?php
                                $v_SpaceTypes = [
                                    ''=>'',
                                    'Área livre'=>'Área livre',
                                    'Auditório'=>'Auditório',
                                    'Sala'=>'Sala'
                                ];
                            ?>
                            <label for="espaco_fisico_tipo">Tipo<span class="mandatory-field">*</span></label>
                            {!! Form::select('', $v_SpaceTypes, null, ['id' => 'espaco_fisico_tipo', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="espaco_fisico_area">Área (m<sup>2</sup>)<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control currency-field" id="espaco_fisico_area" placeholder="Digite Aqui" required>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="espaco_fisico_pe_direito">Pé-direito (m)<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control currency-field" id="espaco_fisico_pe_direito" placeholder="Digite Aqui" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="espaco_fisico_dimensoes">Dimensões (Comprimento x Largura)<span class="mandatory-field">*</span></label>
                            <div class="row">
                                <div class="col-xs-6">
                                    <input type="text" class="form-control currency-field" id="espaco_fisico_dimensoes_c" placeholder="Digite Aqui" required>
                                </div>
                                <div class="col-xs-6">
                                    <input type="text" class="form-control currency-field" id="espaco_fisico_dimensoes_l" placeholder="Digite Aqui" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-sm-3">
                            <label for="espaco_fisico_capacidade">Capacidade<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control integer-field" id="espaco_fisico_capacidade" placeholder="Digite Aqui" required>
                        </div>
                        <div class="form-group col-sm-3">
                            <?php
                                $v_Space = [
                                    ''=>'',
                                    'Coberto'=>'Coberto',
                                    'Descoberto'=>'Descoberto'
                                ];
                            ?>
                            <label for="espaco_fisico_espaco">Espaço<span class="mandatory-field">*</span></label>
                            {!! Form::select('', $v_Space, null, ['id' => 'espaco_fisico_espaco', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <?php

                                $v_SpaceFormats = [
                                    'Auditório'=>'Auditório',
                                    'Banquete'=>'Banquete',
                                    'Buffet'=>'Buffet',
                                    'Classroom'=>'Classroom',
                                    'Cocktail'=>'Cocktail',
                                    'Espinha de peixe'=>'Espinha de peixe',
                                    'Formato U'=>'Formato U'
                                ];
                            ?>
                            <label for="espaco_fisico_formatos">Formato <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                            {!! Form::select('', $v_SpaceFormats, null, ['id' => 'espaco_fisico_formatos', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple', 'required']) !!}
                        </div>

                        <?php
                            $v_SpaceFormatFields = [
                                    'auditorio'=>'Auditório',
                                    'banquete'=>'Banquete',
                                    'buffet'=>'Buffet',
                                    'classroom'=>'Classroom',
                                    'cocktail'=>'Cocktail',
                                    'espinha_peixe'=>'Espinha de peixe',
                                    'formato_u'=>'Formato U'
                            ];
                        ?>
                        @foreach($v_SpaceFormatFields as $c_Index => $c_Field)
                            <div class="form-group col-sm-4 format-capacity-fields" rel="{{$c_Field}}" style="display: none;">
                                <label>{{$c_Field}} - Capacidade<span class="mandatory-field">*</span></label>
                                <input type="text" id="espaco_fisico_capacidade_{{$c_Index}}" class="form-control integer-field" placeholder="Digite Aqui">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" id="submitBtn">Continuar</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>