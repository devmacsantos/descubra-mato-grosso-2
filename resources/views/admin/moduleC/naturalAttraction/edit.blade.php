@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    C1 - Atrativo natural
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
        <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
            <i class="fa fa-print"></i>
        </a>
    </ul>
@stop
@section('content')
    <div class="row visible-print">
        <div class="mb20 col-sm-12">
            <h2>C1 - Atrativo natural</h2>
        </div>
    </div>
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    @if(!\App\UserType::isParceiro())
    {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/atrativos-naturais'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
    @else
    <div id="mainForm">
    @endif

    <div class="tab-content pn br-n">

        <?php
            $v_TabLanguages = ['pt', 'en', 'es', 'fr'];
            if($p_Attraction != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_Observations = $p_Attraction->observacoes_complementares != null ? json_decode($p_Attraction->observacoes_complementares,1) : $v_EmptyData;
            }
        ?>
        @foreach($v_TabLanguages as $c_TabIndex => $c_TabLanguage)
        <div id="{{'tab' . $c_TabIndex}}" class="tab-pane {{ $c_TabIndex == 0 ? 'active' : '' }}">
            <div class="row">
                @include('admin.moduleC.naturalAttraction.util.attractionHeaderIdentification', ['p_Form' => $p_Attraction, 'p_Types' => $p_Types, 'p_TabLanguage' => $c_TabLanguage])

                @if($c_TabLanguage == 'pt')
                    @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])
                    <div class="col-sm-12">
                        <p>* É necessário ter ao menos uma foto de capa para que possa aparecer no portal.</p>
                    </div>

                    @include('admin.util.attractionAccess', ['p_Form' => $p_Attraction, 'p_NaturalAttraction' => true])

                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Proteção</h3>
                    </div>
                    <input type="hidden" id="protecao" name="formulario[protecao]" value="{{$p_Attraction == null ? '' : $p_Attraction->protecao}}">
                    <div class="protecao-fields">
                        <div class="form-group col-sm-12">
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Existe dispositivo legal (lei, decreto, norma, etc) que trate da criação, proteção ou restrição referentes ao atrativo?</label>
                            <p><input id="existe_dispositivo_legal" type="checkbox" value="1" rel="Existe dispositivo legal" class="ml5 mt10 checkbox field"></p>
                        </div>
                        <div class="form-group col-sm-12 mandatory dispositivo-legal-fields">
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Especifique<span class="mandatory-field">*</span></label>
                            <textarea class="form-control field" rel="Existe dispositivo legal - Especifique" rows="4" placeholder="Digite Aqui"></textarea>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}O atrativo é ou está localizado em uma unidade de conservação?</label>
                            <p><input id="atrativo_localizado_unidade_conservacao" type="checkbox" value="1" rel="Localizado em unidade de conservação" class="ml5 mt10 checkbox field"></p>
                        </div>
                        <div class="form-group col-sm-6 mandatory conservation-unit-fields">
                            <?php
                                $v_ProtectedAreaTypes = [
                                    ''=>'',
                                    'Unidade de proteção integral' => 'Unidade de proteção integral',
                                    'Unidade de uso sustentável' => 'Unidade de uso sustentável',
                                    'Parque municipal' => 'Parque municipal',
                                    'Parque estadual' => 'Parque estadual'
                                ];
                            ?>
                            <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Tipo<span class="mandatory-field">*</span></label>
                            {!! Form::select('', $v_ProtectedAreaTypes, null, ['rel' => 'Tipo', 'id' => 'tipo_unidade_conservacao', 'class' => 'form-control field', 'style' => 'width: 100%']) !!}
                        </div>
                        <div class="conservation-unit-fields">
                            <div class="form-group col-sm-6 conservation-unit-integral-fields">
                                <?php
                                    $v_ProtectedAreaCategories = [
                                        '' => '',
                                        'Estação ecológica' => 'Estação ecológica',
                                        'Reserva biológica' => 'Reserva biológica',
                                        'Parque nacional' => 'Parque nacional',
                                        'Monumento natural' => 'Monumento natural',
                                        'Refúgio de vida silvestre' => 'Refúgio de vida silvestre'
                                    ];
                                ?>
                                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}Categoria</label>
                                {!! Form::select('', $v_ProtectedAreaCategories, null, ['rel' => 'Categoria (unidade de proteção integral)', 'class' => 'form-control field', 'style' => 'width: 100%']) !!}
                            </div>
                            <div class="form-group col-sm-6 conservation-unit-sustentavel-fields">
                                <?php
                                    $v_ProtectedAreaCategories = [
                                        '' => '',
                                        'Área de proteção ambiental' => 'Área de proteção ambiental',
                                        'Área de relevante interesse ecológico' => 'Área de relevante interesse ecológico',
                                        'Floresta nacional' => 'Floresta nacional',
                                        'Reserva extrativista' => 'Reserva extrativista',
                                        'Reserva de fauna' => 'Reserva de fauna',
                                        'Reserva de desenvolvimento sustentável' => 'Reserva de desenvolvimento sustentável',
                                        'Reserva particular do patrimônio natural' => 'Reserva particular do patrimônio natural'
                                    ];
                                ?>
                                <label>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(3)}}Categoria</label>
                                {!! Form::select('', $v_ProtectedAreaCategories, null, ['rel' => 'Categoria (unidade de uso sustentável)', 'class' => 'form-control field', 'style' => 'width: 100%']) !!}
                            </div>
                        </div>
                    </div>

                    @include('admin.util.attractionConservation', ['p_Form' => $p_Attraction, 'p_NaturalAttraction' => true])
                @endif

                @include('admin.moduleC.naturalAttraction.util.attractionFacilities', ['p_Form' => $p_Attraction, 'p_Languages' => $p_Languages, 'p_LanguageNames' => $p_LanguageNames, 'p_TabLanguage' => $c_TabLanguage])

                <div class="form-group col-sm-12">
                    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
                </div>

                @if($c_TabLanguage == 'pt')
                    <input type="hidden" id="observacoes_complementares" name="formulario[observacoes_complementares]">
                @endif
                <div class="form-group col-sm-12">
                    <label for="observacoes_complementares_{{$c_TabLanguage}}">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e observações complementares</label>
                    <textarea id="observacoes_complementares_{{$c_TabLanguage}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Attraction == null ? '' : $v_Observations[$c_TabLanguage]}}</textarea>
                </div>

                @if($c_TabLanguage == 'pt')
                    <input type="hidden" id="hashtags" name="formulario[hashtags]">
                    <div class="form-group col-sm-12">
                        <label for="hashtags">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Palavras-chave</label>
                        <?php
                            $v_Hashtags = $p_Attraction != null && $p_Attraction->hashtags != null ? explode(';', $p_Attraction->hashtags) : [];
                            $v_HashtagOptions = [];
                            foreach($v_Hashtags as $c_Key)
                                $v_HashtagOptions[$c_Key] = $c_Key;
                        ?>
                        {!! Form::select('', $v_HashtagOptions, $v_Hashtags, ['id' => 'hashtags_select', 'class' => 'form-control select2-custom', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    <div class="form-group col-sm-12">
                        <label for="possui_espacos_eventos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui espaços de eventos?</label>
                        <p><input type="checkbox" value="1" name="formulario[possui_espacos_eventos]" id="possui_espacos_eventos" class="ml5 mt10" {{($p_Attraction == null || $p_Attraction->possui_espacos_eventos == 0) ? '' : 'checked'}}></p>
                    </div>
                @endif

                @include('admin.util.accessibility', ['p_Form' => $p_Attraction, 'p_Translation' => true, 'p_TabLanguage' => $c_TabLanguage])

                @if($c_TabLanguage == 'pt')
                    <div class="form-group col-sm-12">
                        <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
                    </div>
                    <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_Attraction == null ? '' : $p_Attraction->tipos_viagem}}">
                    <div class="form-group col-sm-12">
                        <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
                        {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>

                    @if(\App\UserType::isMaster() || \App\UserType::isAdmin() || \App\UserType::isComunicacao())
                        <div class="form-group col-sm-12">
                            <label for="destaque">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Destaque?</label>
                            <p><input type="checkbox" value="1" name="formulario[destaque]" id="destaque" class="ml5 mt10" {{($p_Attraction == null || $p_Attraction->destaque == 0) ? '' : 'checked'}}></p>
                        </div>
                    @endif

                    @include('admin.util.revision', ['p_Form' => $p_Attraction, 'p_Publish' => true])

                    @include('admin.util.responsibleTeam', ['p_Form' => $p_Attraction])
                @endif
            </div>
        </div>
        @endforeach

        @if(!\App\UserType::isParceiro())
            <div class="row">
                <div class="form-group col-sm-12">
                    <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
                </div>
            </div>
        @endif
    </div>
    @if(!\App\UserType::isParceiro())
    {!! Form::close() !!}
    @else
    </div>
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.headerIdentificationScript')
    @include('admin.util.attractionConservationScript')
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            carregaDadosProtecao();

            $('#existe_dispositivo_legal').change(function(){
                if($(this).is(":checked")) {
                    $('.dispositivo-legal-fields').show();
                    $('.dispositivo-legal-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.dispositivo-legal-fields').hide();
                    $('.dispositivo-legal-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            $('#atrativo_localizado_unidade_conservacao').change(function(){
                if($(this).is(":checked")) {
                    $('.conservation-unit-fields').show();
                    $('.conservation-unit-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.conservation-unit-fields').hide();
                    $('.conservation-unit-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            $('#tipo_unidade_conservacao').change(function(){
                $('.conservation-unit-integral-fields, .conservation-unit-sustentavel-fields').hide();
                if($(this).val() == 'Unidade de proteção integral'){
                    $('.conservation-unit-integral-fields').show();
                }
                else if($(this).val() == 'Unidade de uso sustentável'){
                    $('.conservation-unit-sustentavel-fields').show();
                }
            }).change();

            $('#equipamento_fechado').change(function(){
                if($(this).is(":checked")) {
                    $('.closed-equipment-fields').show();
                    $('.closed-equipment-fields.mandatory .form-control').attr('required', true);
                }
                else {
                    $('.closed-equipment-fields').hide();
                    $('.closed-equipment-fields.mandatory .form-control').removeAttr('required');
                }
            }).change();

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }
        });

        function carregaDadosProtecao()
        {
            var v_Protecao = $('#protecao').val();
            if(v_Protecao.length > 0)
            {
                v_Protecao = JSON.parse(v_Protecao);
                $(v_Protecao).each(function(){
                    if(this.tipo == 'checkbox')
                        $('.protecao-fields .field[rel="'+this.nome+'"]').prop('checked',this.valor);
                    else
                        $('.protecao-fields .field[rel="'+this.nome+'"]').val(this.valor);
                });
            }
        }

        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosAcesso();

            processaDadosProtecao();

            processaDadosConservacao();

            processaDadosFacilidades();

            processaDadosAcessibilidade();

            <?php $v_Languages = ['pt', 'en', 'es', 'fr']; ?>

            var v_NamesJson = {};
            var v_ReferencePointsJson = {};
            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            var v_ObservationsJson = {};
            @foreach($v_Languages as $c_Language)
                v_NamesJson.{{$c_Language}} = $('#nome_popular_{{$c_Language}}').val();
                v_ReferencePointsJson.{{$c_Language}} = $('#ponto_referencia_{{$c_Language}}').val();
                v_DescriptionsJson.{{$c_Language}} = $('#descricao_atrativo_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
                v_ObservationsJson.{{$c_Language}} = $('#observacoes_complementares_{{$c_Language}}').val();
            @endforeach
            $('#nome_popular').val(JSON.stringify(v_NamesJson));
            $('#ponto_referencia').val(JSON.stringify(v_ReferencePointsJson));
            $('#descricao_atrativo').val(JSON.stringify(v_DescriptionsJson));
            $('#descricao_curta').val(JSON.stringify(v_ShortDescriptionsJson));
            $('#observacoes_complementares').val(JSON.stringify(v_ObservationsJson));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            var v_Hashtags = $('#hashtags_select').val();
            $('#hashtags').val(v_Hashtags == null ? '' : v_Hashtags.join(';'));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

        function processaDadosProtecao()
        {
            var v_Protecao = [];
            $('.protecao-fields .field').each(function(){
                v_Protecao.push({
                    nome:$(this).attr('rel'),
                    tipo:$(this).hasClass('checkbox') ? 'checkbox' : 'text-or-select',
                    valor:$(this).hasClass('checkbox') ? $(this).is(':checked') : $(this).val()
                });
            });
            $('#protecao').val(JSON.stringify(v_Protecao));
        }
    </script>
    @include('admin.util.mapScript')
    @include('admin.util.attractionAccessScript')
    @include('admin.util.attractionFacilitiesScript', ['p_Translation' => true])
    @include('admin.util.attractionGeneralInfoScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'C1'])
@stop