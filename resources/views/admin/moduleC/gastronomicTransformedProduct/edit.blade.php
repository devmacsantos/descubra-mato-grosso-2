@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        #myModal .modal-dialog{
            z-index:1051;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    C6.2 - Gastronomia -  Produto transformado
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <div class="row">
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/produto-transformado'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
        @else
        <div id="mainForm">
        @endif
        <div class="form-group col-sm-12 visible-print">
            <h2>C6.2 - Gastronomia -  Produto transformado</h2>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
        </div>
        @if($p_Attraction != null)
            <input type="hidden" name="id" value="{{$p_Attraction->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
            <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
            {!! Form::select('formulario[city_id]', $v_CityOptions, $p_Attraction == null ? '' : $p_Attraction->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="district_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distrito</label>
            {!! Form::select('formulario[district_id]', [($p_Attraction == null ? '' : $p_Attraction->district_id) => ''], $p_Attraction == null ? '' : $p_Attraction->district_id, ['id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Sobre o produto transformado <i class="fs12">(elaborados a partir do produto primário. Exemplo: pó de café, azeite, bananada, geleia de morango)</i></h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Produto transformado<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[nome]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->nome}}" required>
        </div>
        <div class="form-group col-sm-12">
            <label for="receita">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Informe a receita</label>
            <textarea id="receita" name="formulario[receita]" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Attraction == null ? '' : $p_Attraction->receita}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <label for="reconhecido_como_patrimonio">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}É reconhecido por algum órgão enquanto patrimônio?</label>
            <p><input type="checkbox" name="formulario[reconhecido_como_patrimonio]" value="1" id="reconhecido_como_patrimonio" class="ml5 mt10 checkbox" onchange="showOrHideFields($(this).is(':checked'),'.orgaos-reconhecem-field');" {{($p_Attraction == null || $p_Attraction->reconhecido_como_patrimonio == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-12 orgaos-reconhecem-field">
            <?php
                $v_EntitiesOptions = [
                    'Municipal' => 'Municipal',
                    'Estadual' => 'Estadual',
                    'Nacional' => 'Nacional',
                    'Mundial' => 'Mundial'
                ];
            ?>
            <label for="orgaos_que_reconhecem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Órgãos <i>(permite mais de uma opção)</i></label>
            <input type="hidden" id="orgaos_que_reconhecem" name="formulario[orgaos_que_reconhecem]" value="{{$p_Attraction == null ? '' : $p_Attraction->orgaos_que_reconhecem}}">
            {!! Form::select('', $v_EntitiesOptions, null, ['id' => 'orgaos_que_reconhecem_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>
        <div class="col-sm-12 mb20"></div>

        @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
        </div>

        <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_Attraction == null ? '' : $p_Attraction->tipos_viagem}}">
        <div class="form-group col-sm-12">
            <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>

        @include('admin.util.revision', ['p_Form' => $p_Attraction])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_Attraction])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <button type="button" class="btn btn-default mt15 mb25 pull-right" onclick="checkForm()">Salvar</button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Informação adicional</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="orgaos_que_reconhecem">Existem empreendimentos no município que permitem a visitação do turista ao processo de produção, podendo contemplar também degustação ou oficinas que envolvam a produção deste produto?</label>
                                {!! Form::select('existe_c3', [0 => 'Não', 1 => 'Sim'], null, ['id' => '', 'class' => 'form-control']) !!}
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="submitBtn" class="btn btn-primary" value="Continuar">
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>

@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });

            CKEDITOR.replace('receita',{
                allowedContent: true,
                filebrowserImageBrowseUrl: null,
                filebrowserFlashBrowseUrl: null,
                filebrowserUploadUrl: "{{url('/admin/upload-arquivo')}}",
                filebrowserImageUploadUrl: "{{url('/admin/upload-arquivo')}}",
                filebrowserFlashUploadUrl: "{{url('/admin/upload-arquivo')}}",
                height:350
            });

            $('#city_id').change(function(){
                $.get("{{url('/admin/distritosMunicipio?city_id=')}}" + $(this).val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                    {
                        var v_LastVal = $('#district_id').val();
                        var v_DataString = '';
                        $.each(data.data, function (c_Key, c_Field)
                        {
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                        });

                        $('#district_id').html('<option value=""></option>' + v_DataString);
                        if(data.data.length == 0)
                            $('#district_id').select2("val", "");
                        else
                            $('#district_id').select2("val", v_LastVal);
                    }
                    else
                        $('#district_id').html('<option value=""></option>').select2("val", "");
                }).error(function(){
                });
            }).change();

            carregaSelect2Simples('orgaos_que_reconhecem');

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }

            $('input[type="checkbox"]').trigger('change');
        });

        function carregaSelect2Simples(p_Id)
        {
            var v_Dados = $('#'+p_Id).val();
            if(v_Dados.length > 0)
            {
                v_Dados = JSON.parse(v_Dados);
                $('#'+p_Id+'_select').select2("val", v_Dados);
            }
        }

        function checkForm()
        {
            if($('#mainForm')[0].checkValidity())
                $('#myModal').modal();
            else
                $('#submitBtn').click();
        }

        function submitForm()
        {
            $('#orgaos_que_reconhecem').val(JSON.stringify(getSelectValue($('#orgaos_que_reconhecem_select').val())));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

        function showOrHideFields(p_Show, p_Class)
        {
            if(p_Show)
                $(p_Class).show();
            else
                $(p_Class).hide();
        }
    </script>
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'C6.2'])
@stop