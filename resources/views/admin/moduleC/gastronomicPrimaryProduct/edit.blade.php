@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        #myModal .modal-dialog{
            z-index:1051;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    C6.1 - Gastronomia -  Produto primário
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <div class="row">
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/produto-primario'), 'onsubmit' => 'return submitForm()', 'files' => true]) !!}
        @else
        <div id="mainForm">
        @endif
        <div class="form-group col-sm-12 visible-print">
            <h2>C6.1 - Gastronomia -  Produto primário</h2>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
        </div>
        @if($p_Attraction != null)
            <input type="hidden" name="id" value="{{$p_Attraction->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
            <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
            {!! Form::select('formulario[city_id]', $v_CityOptions, $p_Attraction == null ? '' : $p_Attraction->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="district_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distrito</label>
            {!! Form::select('formulario[district_id]', [($p_Attraction == null ? '' : $p_Attraction->district_id) => ''], $p_Attraction == null ? '' : $p_Attraction->district_id, ['id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Sobre o produto primário <i class="fs12">(aqueles produtos que estão em seu estado natural, ou seja, da forma como foi retirado da natureza. Exemplo: grão de café, oliva, banana, morango)</i></h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Produto primário<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[nome]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->nome}}" required>
        </div>
        <div class="form-group col-sm-6">
            <?php
                $v_RelevanceOptions = [
                    'Alta' => 'Alta',
                    'Média' => 'Média',
                    'Baixa' => 'Baixa'
                ];
            ?>
            <label for="relevancia_economia_cidade">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Qual a relevância para a economia da cidade?</label>
            {!! Form::select('formulario[relevancia_economia_cidade]', $v_RelevanceOptions, $p_Attraction == null ? '' : $p_Attraction->relevancia_economia_cidade, ['id' => 'relevancia_economia_cidade', 'class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="periodo_colheita">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Período de colheita <i>(permite mais de uma opção)</i></label>
            <?php $v_MonthRange = $p_Attraction == null ? [] : explode(';', $p_Attraction->periodo_colheita); ?>
            {!! Form::select('periodo_colheita[]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, ['id' => 'periodo_colheita', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="produto_agricultura_familiar">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Produto da Agricultura Familiar?</label>
            <p><input type="checkbox" name="formulario[produto_agricultura_familiar]" value="1" id="produto_agricultura_familiar" class="ml5 mt10 checkbox" onchange="showOrHideFields($(this).is(':checked'),'.agricultura-familiar-fields');" {{($p_Attraction == null || $p_Attraction->produto_agricultura_familiar == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="agricultura-familiar-fields">
            <div class="form-group col-sm-6">
                <label for="nome_associacao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Nome da Associação/Cooperativa </label>
                <input type="text" name="formulario[nome_associacao]" class="form-control" id="nome_associacao" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->nome_associacao}}">
            </div>
            <div class="form-group col-sm-6">
                <label for="responsavel">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Responsável</label>
                <input type="text" name="formulario[responsavel]" class="form-control" id="responsavel" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->responsavel}}">
            </div>
            <div class="form-group col-sm-6">
                <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
                <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_Attraction == null ? '' : $p_Attraction->telefone}}">
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label for="reconhecido_como_patrimonio">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}É reconhecido por algum órgão enquanto patrimônio?</label>
            <p><input type="checkbox" name="formulario[reconhecido_como_patrimonio]" value="1" id="reconhecido_como_patrimonio" class="ml5 mt10 checkbox" onchange="showOrHideFields($(this).is(':checked'),'.orgaos-reconhecem-field');" {{($p_Attraction == null || $p_Attraction->reconhecido_como_patrimonio == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-12 orgaos-reconhecem-field">
            <?php
                $v_EntitiesOptions = [
                    'Municipal' => 'Municipal',
                    'Estadual' => 'Estadual',
                    'Nacional' => 'Nacional',
                    'Mundial' => 'Mundial'
                ];
            ?>
            <label for="orgaos_que_reconhecem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Órgãos <i>(permite mais de uma opção)</i></label>
            <input type="hidden" id="orgaos_que_reconhecem" name="formulario[orgaos_que_reconhecem]" value="{{$p_Attraction == null ? '' : $p_Attraction->orgaos_que_reconhecem}}">
            {!! Form::select('', $v_EntitiesOptions, null, ['id' => 'orgaos_que_reconhecem_select', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>
        <div class="col-sm-12 mb20"></div>

        @include('admin.util.photos', ['p_CoverPhoto' => $p_CoverPhoto, 'p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => true])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
        </div>

        <input type="hidden" id="tipos_viagem" name="formulario[tipos_viagem]" value="{{$p_Attraction == null ? '' : $p_Attraction->tipos_viagem}}">
        <div class="form-group col-sm-12">
            <label for="tipos_viagem">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo de polo <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_TripTypes, null, ['id' => 'tipos_viagem_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>

        @include('admin.util.revision', ['p_Form' => $p_Attraction])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_Attraction])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <button type="button" class="btn btn-default mt15 mb25 pull-right" onclick="checkForm()">Salvar</button>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Informação adicional</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="orgaos_que_reconhecem">Existem empreendimentos no município que permitem a visitação do turista ao processo de produção, podendo contemplar também degustação ou oficinas que envolvam a produção deste produto?</label>
                                {!! Form::select('existe_c3', [0 => 'Não', 1 => 'Sim'], null, ['id' => '', 'class' => 'form-control']) !!}
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="submitBtn" class="btn btn-primary" value="Continuar">
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>

@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });

            $('#periodo_colheita').change(function(){
                var v_Field = this;
                var v_Selection = $(v_Field).val();
                if($.inArray('Todos', v_Selection) > -1)
                {
                    if($(v_Field).find('option:selected').length == 13)
                        $(v_Field).select2("val", "");
                    else
                        $(v_Field).select2("val", ['1','2','3','4','5','6','7','8','9','10','11','12']);
                }
            });

            $('#city_id').change(function(){
                $.get("{{url('/admin/distritosMunicipio?city_id=')}}" + $(this).val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                    {
                        var v_LastVal = $('#district_id').val();
                        var v_DataString = '';
                        $.each(data.data, function (c_Key, c_Field)
                        {
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                        });

                        $('#district_id').html('<option value=""></option>' + v_DataString);
                        if(data.data.length == 0)
                            $('#district_id').select2("val", "");
                        else
                            $('#district_id').select2("val", v_LastVal);
                    }
                    else
                        $('#district_id').html('<option value=""></option>').select2("val", "");
                }).error(function(){
                });
            }).change();

            carregaSelect2Simples('orgaos_que_reconhecem');

            var v_TripTypes = $('#tipos_viagem').val();
            if(v_TripTypes.length > 0)
            {
                v_TripTypes = JSON.parse(v_TripTypes);
                $('#tipos_viagem_select').select2("val", v_TripTypes);
            }

            $('input[type="checkbox"]').trigger('change');
        });

        function carregaSelect2Simples(p_Id)
        {
            var v_Dados = $('#'+p_Id).val();
            if(v_Dados.length > 0)
            {
                v_Dados = JSON.parse(v_Dados);
                $('#'+p_Id+'_select').select2("val", v_Dados);
            }
        }

        function checkForm()
        {
            if($('#mainForm')[0].checkValidity())
                $('#myModal').modal();
            else
                $('#submitBtn').click();
        }

        function submitForm()
        {
            $('#orgaos_que_reconhecem').val(JSON.stringify(getSelectValue($('#orgaos_que_reconhecem_select').val())));

            $('#tipos_viagem').val(JSON.stringify(getSelectValue($('#tipos_viagem_select').val())));

            return true;
        }

        function getSelectValue(p_SelectValue){
            if(p_SelectValue == null || p_SelectValue == undefined)
                return [];
            else return p_SelectValue;
        }

        function showOrHideFields(p_Show, p_Class)
        {
            if(p_Show)
                $(p_Class).show();
            else
                $(p_Class).hide();
        }
    </script>
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'C6.1'])
@stop