@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Type == null ? 'Cadastro' : 'Edição' }} de tipo de segmento
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/tipos-viagem'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
        <div class="tab-content pn br-n">
            @if($p_Type != null)
                <input type="hidden" name="id" value="{{$p_Type->id}}">
            @endif
            <input type="hidden" id="form_nome_pt" name="formulario[nome_pt]">
            <input type="hidden" id="form_nomes" name="formulario[nome]">
            <input type="hidden" id="form_descricoes_curtas" name="formulario[descricao_curta]">
            <input type="hidden" id="form_descricoes" name="formulario[descricao]">

            <?php
                if($p_Type != null){
                    $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                    $v_Names = $p_Type->nome != null ? json_decode($p_Type->nome,1) : $v_EmptyData;
                    $v_ShortDescriptions = $p_Type->descricao_curta != null ? json_decode($p_Type->descricao_curta,1) : $v_EmptyData;
                    $v_Descriptions = $p_Type->descricao != null ? json_decode($p_Type->descricao,1) : $v_EmptyData;
                }
                $v_Languages = ['pt', 'en', 'es', 'fr'];
            ?>

            @foreach($v_Languages as $c_Index => $c_Language)
                <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                    @if($c_Index == 0)
                        <div class="row">
                            <div class="col-sm-12 fixed-photo-div">
                                <div class="camera" style="{{($p_Type == null || $p_Type->foto_capa_url == null) ? '' : 'background-image:url(' . $p_Type->foto_capa_url . ');'}}">
                                    <img class="upload-btn-icon" style="{{($p_Type == null || $p_Type->foto_capa_url == null) ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                    <h4 class="upload-btn-text" style="{{($p_Type == null || $p_Type->foto_capa_url == null) ? '' : 'display:none'}}">Foto de<br>capa</h4>
                                    <input class="photo-upload-input" name="foto_capa" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)" {{$p_Type == null ? 'required' : ''}}>
                                    <input type="hidden" name="photo[id][]" value="{{$p_Type == null ? '' : $p_Type->id}}">
                                </div>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="category_id">Categoria<span class="mandatory-field">*</span></label>
                                {!! Form::select('formulario[trip_category_id]', $p_Categories, $p_Type == null ? '' : $p_Type->trip_category_id, array('id' => 'category_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%')) !!}
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="publico">Público?</label>
                                <p><input type="checkbox" value="1" name="formulario[publico]" id="publico" class="ml5 mt10" {{($p_Type == null || $p_Type->publico == 0) ? '' : 'checked'}}></p>
                            </div>
                            <div class="form-group col-sm-3">
                                <label for="destaque">Destaque?</label>
                                <p><input type="checkbox" value="1" name="formulario[destaque]" id="destaque" class="ml5 mt10" {{($p_Type == null || $p_Type->destaque == 0) ? '' : 'checked'}}></p>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="nome_{{$c_Language}}">Nome<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control" id="nome_{{$c_Language}}" placeholder="Digite Aqui" value="{{$p_Type == null ? '' : $v_Names[$c_Language]}}" {{$c_Index == 0 ? 'required' : ''}}>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="descricao_curta_{{$c_Language}}">Descrição curta</label>
                            <textarea id="descricao_curta_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Type == null ? '' : $v_ShortDescriptions[$c_Language]}}</textarea>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="descricao_{{$c_Language}}">Descrição<span class="mandatory-field">*</span></label>
                            <textarea id="descricao_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$c_Index == 0 ? 'required' : ''}}>{{$p_Type == null ? '' : $v_Descriptions[$c_Language]}}</textarea>
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="form-group col-sm-12">
                    <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
                </div>
            </div>
        </div>

    {!! Form::close() !!}
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language: 'pt-BR'});
        });

        function submitForm()
        {
            var v_NamesJson = {};
            var v_DescriptionsJson = {};
            var v_ShortDescriptionsJson = {};
            @foreach($v_Languages as $c_Language)
                v_NamesJson.{{$c_Language}} = $('#nome_{{$c_Language}}').val();
                v_DescriptionsJson.{{$c_Language}} = $('#descricao_{{$c_Language}}').val();
                v_ShortDescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
            @endforeach

            $('#form_nome_pt').val(v_NamesJson.pt);
            $('#form_nomes').val(JSON.stringify(v_NamesJson));
            $('#form_descricoes').val(JSON.stringify(v_DescriptionsJson));
            $('#form_descricoes_curtas').val(JSON.stringify(v_ShortDescriptionsJson));

            return true;
        }
    </script>
@stop