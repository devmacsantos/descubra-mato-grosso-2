<?php
    if(array_key_exists('search', $p_Data))
    {
        $v_CreatedAtSearch = '';
        $v_UpdatedAtSearch = '';
        $v_NameSearch = '';
        $v_CitySearch = '';

        if($p_Data['created_at'] != ''){

            $v_StartDate = \Carbon\Carbon::createFromFormat('d/m/Y', substr($p_Data['created_at'], 0, 10));
            $v_EndDate = \Carbon\Carbon::createFromFormat('d/m/Y', substr($p_Data['created_at'], 13, 23));

            $v_CreatedAtSearch = 'created_at >= "'. $v_StartDate->startOfDay()->format('Y-m-d H:i:s'). '" and created_at <= "'. $v_EndDate->endOfDay()->format('Y-m-d H:i:s') . '"';
        }

        if($p_Data['updated_at'] != ''){

            $v_StartDate = \Carbon\Carbon::createFromFormat('d/m/Y', substr($p_Data['updated_at'], 0, 10));
            $v_EndDate = \Carbon\Carbon::createFromFormat('d/m/Y', substr($p_Data['updated_at'], 13, 23));

            $v_UpdatedAtSearch = 'updated_at >= "'. $v_StartDate->startOfDay()->format('Y-m-d H:i:s'). '" and updated_at <= "'. $v_EndDate->endOfDay()->format('Y-m-d H:i:s') . '"';
        }

        if($p_Data['city_id'] != '')
            $v_CitySearch = 'city_id = '. $p_Data['city_id'];

        if($p_Data['name'] != '')
            $v_NameSearch = ' LIKE "%'. $p_Data['name'] . '%"';

        $v_WhereString = $v_CreatedAtSearch;
        $v_WhereString .= (($v_WhereString != '' && $v_UpdatedAtSearch != '') ? ' and ' : '') . $v_UpdatedAtSearch;
        $v_WhereString .= (($v_WhereString != '' && $v_CitySearch != '') ? ' and ' : '') . $v_CitySearch;
        $v_WhereString .= (($v_WhereString != '' && $v_NameSearch != '') ? ' and ' : '');

        
        
        $v_WhereStringType1 = $v_WhereString . ($v_NameSearch != '' ? ('city.name' . $v_NameSearch) : '');
        $v_WhereStringType2 = $v_WhereString . ($v_NameSearch != '' ? ('nome_fantasia' . $v_NameSearch) : '');
        $v_WhereStringType3 = $v_WhereString . ($v_NameSearch != '' ? ('nome_oficial' . $v_NameSearch) : '');
        $v_WhereStringType4 = $v_WhereString . ($v_NameSearch != '' ? ('nome' . $v_NameSearch) : '');
        $v_WhereStringType5 = $v_WhereString . ($v_NameSearch != '' ? ('nome_popular' . $v_NameSearch) : '');
        $v_WhereStringType6 = $v_WhereString . ($v_NameSearch != '' ? ('nome_juridico' . $v_NameSearch) : '');
        //dd($v_WhereStringType1, $v_WhereStringType2, $v_WhereStringType3, $v_WhereStringType4);
       
        if($v_WhereStringType1 != ''){
            $v_Inventory = [
                [
                    'name' => 'A1 - Informações básicas do município',
                    'path' => 'informacoes',
                    'items' => \App\CityInfo::join('city', 'city.id', '=', 'city_info.city_id')->whereRaw($v_WhereStringType1)
                            ->select('city_info.id as item_id', 'city.name as nome', 'city.name as cidade')->get()
                ],
                [
                    'name' => 'A2.1 - Meios de acesso ao município - Geral',
                    'path' => 'meios-acesso/geral',
                    'items' => \App\CityAccessGeneral::join('city', 'city.id', '=', 'city_access_general.city_id')->whereRaw($v_WhereStringType1)
                                            ->select('city_access_general.id as item_id', 'city.name as nome', 'city.name as cidade')->get()
                ],
                [
                    'name' => 'A2.2 - Meios de acesso ao município',
                    'path' => 'meios-acesso',
                    'items' => \App\CityAccess::join('city', 'city.id', '=', 'city_access.city_id')->whereRaw($v_WhereStringType4)
                            ->select('city_access.id as item_id', 'city_access.nome', 'city.name as cidade')->get()
                ],
                [
                    'name' => 'A4 - Sistemas de segurança',
                    'path' => 'sistemas-seguranca',
                    'items' => \App\CitySecuritySystem::join('city', 'city.id', '=', 'city_security_system.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('city_security_system.id as item_id', 'city_security_system.nome_juridico as nome', 'city.name as cidade', 'city_security_system.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'A5 - Sistemas hospitalares',
                    'path' => 'sistemas-hospitalares',
                    'items' => \App\CityHospitalSystem::join('city', 'city.id', '=', 'city_hospital_system.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('city_hospital_system.id as item_id', 'city_hospital_system.nome_juridico as nome', 'city.name as cidade', 'city_hospital_system.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'A7 - Outros serviços',
                    'path' => 'outros-servicos',
                    'items' => \App\CityOtherService::join('city', 'city.id', '=', 'city_other_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('city_other_service.id as item_id', 'city_other_service.nome_juridico as nome', 'city.name as cidade', 'city_other_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'B1 - Serviços e equipamentos de hospedagem',
                    'path' => 'servicos-hospedagem',
                    'items' => \App\AccomodationService::join('city', 'city.id', '=', 'accomodation_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('accomodation_service.id as item_id', 'accomodation_service.nome_juridico as nome', 'city.name as cidade', 'accomodation_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'B2 - Serviços e equipamentos de alimentos e bebidas',
                    'path' => 'servicos-alimentos',
                    'items' => \App\FoodDrinksService::join('city', 'city.id', '=', 'food_drinks_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('food_drinks_service.id as item_id', 'food_drinks_service.nome_juridico as nome', 'city.name as cidade', 'food_drinks_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'B3 - Serviços e equipamentos de agências de turismo',
                    'path' => 'servicos-agencias-turismo',
                    'items' => \App\TourismAgencyService::join('city', 'city.id', '=', 'tourism_agency_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('tourism_agency_service.id as item_id', 'tourism_agency_service.nome_juridico as nome', 'city.name as cidade', 'tourism_agency_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'B4 - Serviços e equipamentos de transporte turístico',
                    'path' => 'servicos-transporte-turistico',
                    'items' => \App\TourismTransportationService::join('city', 'city.id', '=', 'tourism_transportation_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('tourism_transportation_service.id as item_id', 'tourism_transportation_service.nome_juridico as nome', 'city.name as cidade', 'tourism_transportation_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'B5 - Serviços e equipamentos para eventos',
                    'path' => 'servicos-eventos',
                    'items' => \App\EventService::join('city', 'city.id', '=', 'event_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('event_service.id as item_id', 'event_service.nome_juridico as nome', 'city.name as cidade', 'event_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'B6 - Serviços e equipamentos de lazer',
                    'path' => 'servicos-lazer',
                    'items' => \App\RecreationService::join('city', 'city.id', '=', 'recreation_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)->orWhereRaw($v_WhereStringType6)
                            ->select('recreation_service.id as item_id', 'recreation_service.nome_juridico as nome', 'city.name as cidade', 'recreation_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'B7 - Outros serviços e equipamentos turísticos',
                    'path' => 'outros-servicos-turisticos',
                    'items' => \App\OtherTourismService::join('city', 'city.id', '=', 'other_service.city_id')->whereRaw($v_WhereStringType2)->orWhereRaw($v_WhereStringType6)
                            ->select('other_service.id as item_id', 'other_service.nome_juridico as nome', 'city.name as cidade', 'other_service.nome_fantasia as nome_fantasia')->get()
                ],
                [
                    'name' => 'C1 - Atrativos naturais',
                    'path' => 'atrativos-naturais',
                    'items' => \App\NaturalAttraction::join('city', 'city.id', '=', 'natural_attraction.city_id')->whereRaw($v_WhereStringType3)->orWhereRaw($v_WhereStringType5)
                            ->select('natural_attraction.id as item_id', 'natural_attraction.nome_oficial as nome', 'city.name as cidade', 'natural_attraction.nome_popular as nome_popular')->get()
                ],
                [
                    'name' => 'C2 - Atrativos culturais',
                    'path' => 'atrativos-culturais',
                    'items' => \App\CulturalAttraction::join('city', 'city.id', '=', 'cultural_attraction.city_id')->whereRaw($v_WhereStringType3)->orWhereRaw($v_WhereStringType5)
                            ->select('cultural_attraction.id as item_id', 'cultural_attraction.nome_oficial as nome', 'city.name as cidade', 'cultural_attraction.nome_popular as nome_popular')->get()
                ],
                [
                    'name' => 'C3 - Atividades econômicas',
                    'path' => 'atrativos-atividades-economicas',
                    'items' => \App\EconomicActivityAttraction::join('city', 'city.id', '=', 'economic_activity_attraction.city_id')->whereRaw($v_WhereStringType3)->orWhereRaw($v_WhereStringType5)
                            ->select('economic_activity_attraction.id as item_id', 'economic_activity_attraction.nome_oficial as nome', 'city.name as cidade', 'economic_activity_attraction.nome_popular as nome_popular')->get()
                ],
                [
                    'name' => 'C4 - Realizações técnicas e científicas contemporâneas',
                    'path' => 'realizacoes-contemporaneas',
                    'items' => \App\ContemporaryRealization::join('city', 'city.id', '=', 'contemporary_realization.city_id')->whereRaw($v_WhereStringType3)->orWhereRaw($v_WhereStringType5)
                            ->select('contemporary_realization.id as item_id', 'contemporary_realization.nome_oficial as nome', 'city.name as cidade', 'contemporary_realization.nome_popular as nome_popular')->get()
                ],
                [
                    'name' => 'C6.1 - Gastronomia - Produto primário',
                    'path' => 'produto-primario',
                    'items' => \App\GastronomicPrimaryProduct::join('city', 'city.id', '=', 'gastronomic_primary_product.city_id')->whereRaw($v_WhereStringType4)
                        ->select('gastronomic_primary_product.id as item_id', 'nome', 'city.name as cidade')->get()
                ],
                [
                    'name' => 'C6.2 - Gastronomia - Produto transformado',
                    'path' => 'produto-transformado',
                    'items' => \App\GastronomicTransformedProduct::join('city', 'city.id', '=', 'gastronomic_transformed_product.city_id')->whereRaw($v_WhereStringType4)
                        ->select('gastronomic_transformed_product.id as item_id', 'nome', 'city.name as cidade')->get()
                ],
                [
                    'name' => 'C6.3 - Gastronomia - Prato típico',
                    'path' => 'prato-tipico',
                    'items' => \App\GastronomicTypicalDish::join('city', 'city.id', '=', 'gastronomic_typical_dish.city_id')->whereRaw($v_WhereStringType4)
                        ->select('gastronomic_typical_dish.id as item_id', 'nome', 'city.name as cidade')->get()
                ]
            ];
            $v_InvalidSearch = false;
        }
        else
        {
            $v_Inventory = [];
            $v_InvalidSearch = true;
        }
    }
    else
    {
        $v_Inventory = [];
        $v_InvalidSearch = false;
    }
    $v_EmptySearch = true;
    foreach($v_Inventory as $c_Inventory){
        if(count($c_Inventory['items'])){
            $v_EmptySearch = false;
            break;
        }
    }
?>
@if(count($v_Inventory))
    @if($v_EmptySearch)
        <div class="form-group col-sm-12 text-center">
            <p>Nenhum registro encontrado</p>
        </div>
    @else
        @include('admin.inventory.partials.inventory', ['p_Inventory' => $v_Inventory])
    @endif
@elseif($v_InvalidSearch)
    <div class="form-group col-sm-12 text-center">
        <p>Busca inválida, preencha pelo menos um dos filtros</p>
    </div>
@endif