@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/daterange/daterangepicker.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTablesTemplate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datatables/media/css/dataTables.responsive.css')}}">
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }

        div ul li.paginate_button.active a
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button.active a:hover
        {
            background-color: #C30F1B;
            border-color: #C30F1B;
            color: #FFF;
        }
        div ul li.paginate_button a, div ul li.paginate_button a:hover
        {
            color: #000;
        }
        td
        {
            text-align: center;
        }
        .responsive-table {
            width: 99.9999% !important;
        }
        thead tr th.select-status {
            width: 100px !important;
        }
        .dtr-data
        {
            display: inline-block;
            word-wrap: break-word;
            word-break: break-all;
        }
        table.dataTable tr.child ul li
        {
            white-space: normal;
        }
        table div.actions-div a
        {
            width: 39px;
            margin-right: 3px;
            margin-top: 1px;
            margin-bottom: 1px;
        }
        .maximize-panel, .minimize-panel {
            cursor: pointer;
        }
        @media (min-width: 500px)
        {
            td
            {
                text-align: center;
            }
        }
        @media (max-width: 320px)
        {
            table div.actions-div
            {
                text-align: center;
            }
            table div.actions-div a
            {
                margin-bottom: 3px;
            }
        }
    </style>
@stop
@section('panel-header')
    Pesquisar no Inventário
@stop
@section('content')
    <div class="row pt15">
        <div class="col-sm-12">
            <h3 class="mtn mb20">Filtros</h3>
        </div>
        {!! Form::open(['id' => 'mainForm', 'method' => 'GET', 'url'=> url('/admin/inventario/pesquisar')]) !!}
            <input name="search" type="hidden" value="1">
            <div class="form-group col-sm-6">
                <label for="name">Nome</label>
                <input name="name" class="form-control" type="text" value="{{array_key_exists('search', $p_Data) ? $p_Data['name'] : ''}}">
            </div>
            <div class="form-group col-sm-6">
                <label for="city_id">Município</label>
                {!! Form::select('city_id', $p_Cities, array_key_exists('search', $p_Data) ? $p_Data['city_id'] : '', ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
            </div>
            <div class="form-group col-sm-6">
                <label for="created_at">Data de cadastro</label>
                <input name="created_at" class="form-control dateInput" type="text" value="{{array_key_exists('search', $p_Data) ? $p_Data['created_at'] : ''}}">
            </div>
            <div class="form-group col-sm-6">
                <label for="updated_at">Atualizado em</label>
                <input name="updated_at" class="form-control dateInput" type="text" value="{{array_key_exists('search', $p_Data) ? $p_Data['updated_at'] : ''}}">
            </div>
            <div class="form-group col-sm-12">
                <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Pesquisar">
            </div>
        {!! Form::close() !!}

        @include('admin.inventory.partials.searchData')
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/datetime-moment.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/daterangepicker.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/media/js/dataTables.responsive.js')}}"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.dateInput').daterangepicker({
                    "autoApply": true,
                    "locale": {
                        "format": "DD/MM/YYYY",
                        "separator": " - ",
                        "applyLabel": "Selecionar",
                        "cancelLabel": "Cancelar",
                        "fromLabel": "De",
                        "toLabel": "Até",
                        "daysOfWeek": [
                            "D",
                            "S",
                            "T",
                            "Q",
                            "Q",
                            "S",
                            "S"
                        ],
                        "customRangeLabel": "Intervalo",
                        "monthNames": [
                            "Janeiro",
                            "Fevereiro",
                            "Março",
                            "Abril",
                            "Maio",
                            "Junho",
                            "Julho",
                            "Agosto",
                            "Setembro",
                            "Outubro",
                            "Novembro",
                            "Dezembro"
                        ],
                        "firstDay": 1
                    },
                    ranges: {
                        'Hoje': [moment(), moment()],
                        'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                        'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                        'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                        'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    "linkedCalendars": false,
                    "opens": "center",
                    "autoUpdateInput": false
                }).on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY')).blur();
                }).mask('99/99/9999 - 99/99/9999');

            initDatatable('responsive-table');
        });

        function initDatatable(p_Id)
        {
            var v_Columns = [
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true},
                    {"bSortable": true}
                ];
            /* DataTables */
            var v_Table = $('#'+p_Id).DataTable({
                responsive: true,
                "aLengthMenu": [[25, 50, 100, 250, 500, -1], [25, 50, 100, 250, 500, "Todos"]],
                "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                tableTools: {
                    "aButtons": []
                },
                "order": [[ 0, "asc" ]],
                "bAutoWidth": true,
                "sScrollX": "100%",
                "bScrollCollapse": true,
                "oLanguage":
                    {
                        "sEmptyTable": "Nenhum registro encontrado",
                        "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                        "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "_MENU_ resultados por página",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar",
                        "oPaginate":
                            {
                                "sNext": "",
                                "sPrevious": "",
                                "sFirst": "Primeiro",
                                "sLast": "Último"
                            },
                        "oAria":
                            {
                                "sSortAscending": ": Ordenar colunas de forma ascendente",
                                "sSortDescending": ": Ordenar colunas de forma descendente"
                            }
                    },
                "aoColumns": v_Columns
            });

            var v_FilterInput = $('.adv-table input[aria-controls="'+p_Id+'"]').appendTo('.adv-table #'+p_Id+'_filter');
            v_FilterInput.attr('placeholder', 'Buscar');
            $('.adv-table #'+p_Id+'_filter label').text('');
            v_FilterInput.appendTo('.adv-table #'+p_Id+'_filter label');
        }
    </script>
@stop