@extends('admin.mainTabs')
                        
@section('pageCSS')
    {{--<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />--}}
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/datepicker/css/bootstrap-datetimepicker.css')}}">
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .remove-date, .add-date{
            height: 39px;
            width: 39px;
            min-width: 39px;
        }
        .remove-date-column, #add-date-column{
            display: flex;
            align-items: flex-start;
        }
        .remove-date-column{
            justify-content: center;
        }
    </style>
    
@stop
@section('panel-header')
    {{ $p_Event == null ? 'Cadastro' : 'Edição' }} de evento
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop
@section('content')
    @if(!\App\UserType::isParceiro())
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/eventos'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
    @endif
    <div class="tab-content pn br-n">
        @if($p_Event != null)
            <input type="hidden" name="id" value="{{$p_Event->id}}">
        @endif
        <input type="hidden" id="descricao_curta" name="formulario[descricao_curta]">
        <input type="hidden" id="sobre" name="formulario[sobre]">
        <input type="hidden" id="dados_relevantes" name="formulario[dados_relevantes]">
        <input type="hidden" id="informacoes_uteis" name="formulario[informacoes_uteis]">

        <?php
            if($p_Event != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_Descriptions = $p_Event->descricao_curta != null ? json_decode($p_Event->descricao_curta,1) : $v_EmptyData;
                $v_About = $p_Event->sobre != null ? json_decode($p_Event->sobre,1) : $v_EmptyData;
                $v_RelevantData = $p_Event->dados_relevantes != null ? json_decode($p_Event->dados_relevantes,1) : ['pt'=>[], 'en'=>[], 'es'=>[], 'fr'=>[]];
                $v_UsefulInformation = $p_Event->informacoes_uteis != null ? json_decode($p_Event->informacoes_uteis,1) : $v_EmptyData;
            }
            $v_Languages = ['pt', 'en', 'es', 'fr'];
        ?>

        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                @if($c_Index == 0)
                    <div class="row">
                        <div class="col-sm-12 fixed-photo-div">
                            <div class="camera" style="{{$p_CoverPhoto == null ? '' : 'background-image:url(' . $p_CoverPhoto->url . ');'}}">
                                <img class="upload-btn-icon" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{$p_CoverPhoto == null ? '' : 'display:none'}}">Foto de<br>capa</h4>
                                <input class="photo-upload-input" name="photo[file][]" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)">
                                <input type="hidden" name="photo[type][]" value="cover">
                                <input type="hidden" name="photo[id][]" value="{{$p_CoverPhoto == null ? '' : $p_CoverPhoto->id}}">
                                @if($p_CoverPhoto)
                                    <a title="Baixar" href="{{$p_CoverPhoto->url}}" class="btn btn-success download-photo-btn" download><i class="fa fa-download"></i></a>
                                    <button title="Visualizar" class="btn btn-success" download style="position: absolute; top: 3px; right: 3px;"><i class="fa fa-eye"></i></button>
                                @endif
                            </div>
                        </div>
                    </div>

                @endif
                <div class="row">
                    @if($c_Index == 0)
                        <div class="form-group col-sm-6">
                            <label for="nome">Nome do evento<span class="mandatory-field">*</span></label>
                            <input type="text" name="formulario[nome]" class="form-control" id="nome" placeholder="Digite Aqui" style="text-transform:uppercase" value="{{$p_Event == null ? '' : $p_Event->nome}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="destaque">Destaque?</label>
                            <p><input type="checkbox" value="1" name="formulario[destaque]" id="destaque" class="ml5 mt10" {{($p_Event == null || $p_Event->destaque == 0) ? '' : 'checked'}}></p>
                        </div>
                    @endif
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_Language}}">Descrição curta (máximo de 10 palavras)</label>
                        <input type="text" class="form-control" id="descricao_curta_{{$c_Language}}" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $v_Descriptions[$c_Language]}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="sobre_{{$c_Language}}">Descrição do evento
                            @if($c_Index == 0)
                                <span class="mandatory-field">*</span>
                            @endif
                        </label>
                        <textarea id="sobre_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$c_Index == 0 ? 'required' : ''}}>{{$p_Event == null ? '' : $v_About[$c_Language]}}</textarea>
                    </div>
                    @if($c_Index == 0)
                        <div class="col-sm-12">
                            <label for="data1">Período do Evento<span class="mandatory-field">*</span></label>
                        </div>
                        <div class="form-group col-sm-6 col-xs-8 date-div">
                            {!! Form::text('datas[]', '', ['id' => 'data1', 'class'=>'form-control', 'required' => 'required']) !!}
                        </div>
                        <div id="add-date-column" class="col-xs-2 form-group">
                            <label for="newDateBtn">&nbsp;</label>
                            <a title="Adicionar data" type="button" id="newDateBtn" class="btn btn-success add-date" onclick="addDate('')"><i class="fa fa-plus"></i></a>
                        </div>
                        <div class="form-group col-sm-6">
                            <?php
                                $v_TypeOptions = [
                                    'Turismo de Lazer' => 'Turismo de Lazer',
                                    'Turismo de Negócio' => 'Turismo de Negócio'
                                ];
                                $v_SelectedTypes = $p_Event == null ? [] : explode(';', $p_Event->tipo_evento);
                            ?>
                            <label for="tipo_evento">Tipo de Evento <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                            {!! Form::select('tipo_evento[]', $v_TypeOptions, $v_SelectedTypes, ['id' => 'tipo_evento', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="categorias">Categorias <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('categorias[]', $p_Categories, $p_SelectedCategories, ['id' => 'categorias', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="city_id">Município<span class="mandatory-field">*</span></label>
                            <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
                            {!! Form::select('formulario[city_id]', $v_CityOptions, $p_Event == null ? '' : $p_Event->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="touristic_circuit_id">Região Turística</label>
                            
                            {!! Form::select('formulario[touristic_circuit_id]', $p_TouristicCircuits, $p_Event == null ? '' : get_circuito($p_Event->city_id) , ['id' => 'touristic_circuit_id', 'class' => 'form-control select2', 'disabled']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="municipios_participantes">Municípios participantes <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                            {{-- $p_EventCities é um array com as chaves dos municipios selecionados --}}
                            {!! Form::select('municipios_participantes[]', $p_AllCities, $p_EventCities, ['id' => 'municipios_participantes', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="espaco_evento">Espaço para o Evento</label>
                            {!! Form::select('formulario[espaco_evento]', $p_EventPlaces, $p_Event == null ? '' : $p_Event->espaco_evento, ['id' => 'espaco_evento', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="local_evento">Nome do local do evento<span class="mandatory-field">*</span></label>
                            <input type="text" name="formulario[local_evento]" class="form-control" id="local_evento" placeholder="Digite Aqui" maxlength="140" value="{{$p_Event == null ? '' : $p_Event->local_evento}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="cep">CEP<span class="mandatory-field">*</span></label>
                            <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->cep}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="bairro">Bairro<span class="mandatory-field">*</span></label>
                            <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->bairro}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="logradouro">Logradouro<span class="mandatory-field">*</span></label>
                            <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->logradouro}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="numero">Número<span class="mandatory-field">*</span></label>
                            <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->numero}}" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="complemento">Complemento</label>
                            <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->complemento}}">
                            <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                                <i class="fa fa-map-marker"></i>
                            </a>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="latitude_longitude_decimal">Latitude e Longitude em decimal</label>
                            <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="latitude" >Latitude <i>(formato decimal)</i></label>
                            <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->latitude}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="longitude">Longitude <i>(formato decimal)</i></label>
                            <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->longitude}}">
                        </div>
                        <div class="form-group col-sm-12">
                            <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
                            <div id="map_canvas" style="height:320px;"></div>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="site">Site</label>
                            <input type="url"  name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->site}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
                            <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->telefone}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="email">Email</label>
                            <input type="email" name="formulario[email]" class="form-control" id="email" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->email}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="facebook">Facebook</label>
                            <input type="url"  name="formulario[facebook]" class="form-control" id="facebook" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->facebook}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="instagram">Instagram</label>
                            <input type="url"  name="formulario[instagram]" class="form-control" id="instagram" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->instagram}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="twitter">Twitter</label>
                            <input type="url"  name="formulario[twitter]" class="form-control" id="twitter" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->twitter}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="youtube">Youtube</label>
                            <input type="url"  name="formulario[youtube]" class="form-control" id="youtube" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->youtube}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="flickr">Flickr</label>
                            <input type="url"  name="formulario[flickr]" class="form-control" id="flickr" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : $p_Event->flickr}}">
                        </div>
                    @endif
                    <div class="form-group col-sm-12">
                        <label for="informacoes_uteis_{{$c_Language}}">Informações úteis</label>
                        <textarea id="informacoes_uteis_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Event == null ? '' : $v_UsefulInformation[$c_Language]}}</textarea>
                    </div>

                    @for($c_Counter = 1; $c_Counter <= 3; $c_Counter++)
                        <div class="form-group col-sm-6">
                            <label for="dado_relevante_{{ $c_Language . $c_Counter}}">Dado relevante {{ $c_Counter }}</label>
                            <input type="text" class="form-control" id="dado_relevante_{{ $c_Language . $c_Counter}}" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : (array_key_exists($c_Counter-1, $v_RelevantData[$c_Language]) ? $v_RelevantData[$c_Language][$c_Counter-1]['dado'] : '')}}">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="{{'dado_relevante_descricao_' . $c_Language . $c_Counter}}">Dado relevante {{ $c_Counter }} - Descrição</label>
                            <input type="text" class="form-control" id="dado_relevante_descricao_{{ $c_Language . $c_Counter}}" placeholder="Digite Aqui" value="{{$p_Event == null ? '' : (array_key_exists($c_Counter-1, $v_RelevantData[$c_Language]) ? $v_RelevantData[$c_Language][$c_Counter-1]['descricao'] : '')}}">
                        </div>
                    @endfor
                    <input type="hidden" name="language[]" value="{{ \App\Http\Controllers\PagesConfigController::$m_Languages[$c_Index] }}">

                    @if($c_Index == 0)
                        <div class="form-group col-sm-12">
                            <label for="tipos_viagem">Tipo de polo <i>(permite mais de uma opção)</i></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('tipos_viagem[]', $p_TripTypes, $p_SelectedTripTypes, ['id' => 'tipos_viagem', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="hashtags">Palavras-chave<span class="mandatory-field">*</span></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('hashtags[]', $p_Hashtags, array_keys($p_Hashtags), ['id' => 'hashtags', 'class' => 'form-control select2-custom', 'style' => 'width: 100%', 'required' => 'required', 'multiple' => 'multiple']) !!}
                        </div>

                        <div class="col-sm-12"></div>
                        @include('admin.util.photos', ['p_PhotoGallery' => $p_PhotoGallery, 'p_Cover' => false, 'p_WithoutFieldIndex' => true])
                    @endif
                </div>
            </div>
        @endforeach

        @if($p_Event != null && $p_Event->revision_status_id == 5)
            <div class="form-group col-sm-12">
                <label for="comentario_revisao">Alterações rejeitadas - Motivo:</label>
                <textarea class="form-control" rows="4" placeholder="Digite Aqui" readonly>{{$p_Event->comentario_revisao}}</textarea>
            </div>
        @endif

       
        <div class="col-sm-12 mt20"></div>

        @if(\App\UserType::isTrade() || \App\UserType::isFiscalizador())
            <input type="hidden" name="formulario[revision_status_id]" id="revisionStatusCityUser" value="1">
        @else
            @if($p_Event == null)
                @if(\App\UserType::isMunicipio())
                    <input type="hidden" name="formulario[revision_status_id]" id="revisionStatusCityUser" value="2">
                @elseif(\App\UserType::isCircuito() || \App\UserType::isComunicacao())
                    <input type="hidden" name="formulario[revision_status_id]" value="3">
                @elseif((\App\UserType::isMaster() || \App\UserType::isAdmin()))
                    <div class="form-group col-sm-12">
                        <label for="classificacao_evento">Classificação do Evento <i>(permite mais de uma opção)</i></label>
                        {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                        {!! Form::select('classificacao_evento[]', $p_EventClassification, $p_SelectedEventClassification, ['id' => 'classificacao_evento', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                    <div class="col-sm-12 mb15">
                        <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="4" required>Publicar evento</label>
                        <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="3">Não publicar</label>
                    </div>
                @endif
            @else
                @if(\App\UserType::isMunicipio())
                    @if($p_Event->revision_status_id == 1)
                        <div class="col-sm-12 revision-radiobuttons mb15">
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="2" required>Aprovar alterações</label>
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="5">Rejeitar alterações</label>
                        </div>
                    @else
                        <input type="hidden" name="formulario[revision_status_id]" id="revisionStatusCityUser" value="2">
                    @endif
                @elseif(\App\UserType::isCircuito())
                    @if($p_Event->revision_status_id != 4)
                        <div class="col-sm-12 revision-radiobuttons mb15">
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="3" required>Aprovar alterações</label>
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="5">Rejeitar alterações</label>
                        </div>
                    @else
                        <input type="hidden" name="formulario[revision_status_id]" value="3">
                    @endif
                @elseif(\App\UserType::isComunicacao() && $p_Event->revision_status_id == 4)
                    <input type="hidden" name="formulario[revision_status_id]" value="3">
                @elseif((\App\UserType::isMaster() || \App\UserType::isAdmin()))
                    <div class="form-group col-sm-12">
                        <label for="classificacao_evento">Classificação do Evento <i>(permite mais de uma opção)</i></label>
                        {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                        {!! Form::select('classificacao_evento[]', $p_EventClassification, $p_SelectedEventClassification, ['id' => 'classificacao_evento', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                    @if($p_Event->revision_status_id != 4)
                        <div class="col-sm-12 revision-radiobuttons mb15">
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="4" required>Publicar evento</label>
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="5">Rejeitar alterações</label>
                        </div>
                    @else
                        <div class="col-sm-12 mb15">
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="4" checked="checked" required>Publicar evento</label>
                            <label class="radio-inline"><input type="radio" name="formulario[revision_status_id]" value="3">Ocultar do site público</label>
                        </div>
                    @endif
                @endif
                <div class="form-group col-sm-12 revision-denied-comment" style="display:none">
                    <label for="comentario_revisao">Motivo</label>
                    <textarea name="formulario[comentario_revisao]" class="form-control" rows="4" placeholder="Digite Aqui"></textarea>
                </div>
            @endif
        @endif


        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        @endif
    </div>
    @if(!\App\UserType::isParceiro())
    {!! Form::close() !!}
    @endif
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datepicker/js/bootstrap-datetimepicker.js')}}"></script>
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => 6])
    @include('admin.util.mapScript')
    <script>
        var v_DateIndex = 2;
        var v_DateHeight = 0;
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('.cep-field').mask('99.999-999');

            $('#city_id').change(function(){
                if(this.value != '')
                {
                    $.get("{{url('/admin/cidade/circuitos')}}/" + $(this).val(), function(){
                    }).done(function(data){
                        if (data.length > 0) {
                            $("#touristic_circuit_id").select2("val", data[0]);
                        }
                        else
                            $("#touristic_circuit_id").select2("val", '');
                    }).error(function(){
                    });

                    var v_Participants = $('#municipios_participantes').val();
                    if(v_Participants != null && v_Participants.length > 1)
                        var v_Data = v_Participants.concat(this.value);
                    else
                        v_Data = this.value;
                    $("#municipios_participantes").select2("val", v_Data);

                    $.get("{{url('/admin/cidade/espacos-evento/')}}/" + $(this).val(), function(){
                    }).done(function(data){
                        if (data.error == 'ok'){
                            var v_LastVal = $('#espaco_evento').val();
                            var v_DataString = '<option value=""></option>';
                            $.each(data.data, function (c_Key, c_Field)
                            {
                                v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                            });

                            $('#espaco_evento').html(v_DataString);
                            if(data.data.length == 0)
                                $('#espaco_evento').val('').change();
                            else
                                $('#espaco_evento').val(v_LastVal).change();
                        }
                        else
                            $("#espaco_evento").html('<option value=""></option>').val('').change();
                    }).error(function(){
                    });
                }
                else{
                    $("#touristic_circuit_id").select2("val", '');
                    $("#espaco_evento").val('').change();
                }
            });

            $('#touristic_circuit_id').change(function(){
                if(this.value != '') {
                    @if(\App\UserType::isMunicipio())
                    $('#revisionStatusCityUser').val('2');
                    @endif
                }
                @if(\App\UserType::isMunicipio())
                else
                    $('#revisionStatusCityUser').val('3');
                @endif
            }).change();

            $('#espaco_evento').change(function(){
                if(this.value != ''){
                    $('#local_evento').val(this.value);
                }
            });

            @if(count($p_Dates) > 0)
                addDatePicker('#data1', '{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_Dates[0]->date)->format('d/m/Y') }}');
            @else
                addDatePicker('#data1', '');
            @endif

            v_DateHeight = $('.date-div:last').height();
            $('#add-date-column').height(v_DateHeight);
            @foreach($p_Dates as $c_Key => $c_Date)
                @if($c_Key > 0)
                    addDate("{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Date->date)->format('d/m/Y') }}");
                @endif
            @endforeach

            $('.revision-radiobuttons input').change(function(){
                if(this.value == 5)
                    $('.revision-denied-comment').show();
                else
                    $('.revision-denied-comment').hide();
            });

            $('#latitude_longitude_decimal').keyup(function(){
                var v_Value = this.value;
                if(v_Value.length){
                    v_Value = v_Value.split(',');
                    var v_Latitude = v_Value[0].trim();
                    var v_Longitude = v_Value[1].trim();
                    if(v_Latitude.length && v_Longitude.length){
                        $('#latitude').val(v_Latitude);
                        $('#longitude').val(v_Longitude).change();
                    }
                }
            });

            $('#latitude, #longitude').focus(function() {
                $(this).on('mousewheel.disableScroll', function(e) {
                    e.preventDefault();
                })
            }).blur(function() {
                $(this).off('mousewheel.disableScroll');
            }).keydown(function(e) {
                if (e.keyCode === 38 || e.keyCode === 40)
                    e.preventDefault();
            });
        });

        function submitForm()
        {
            $('#nome').val($('#nome').val().toUpperCase());

            var v_DescriptionsJson = {};
            var v_AboutJson = {};
            var v_RelevantDataJson = {};
            var v_UsefulInformationJson = {};
            @foreach($v_Languages as $c_Language)
                v_DescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
                v_AboutJson.{{$c_Language}} = $('#sobre_{{$c_Language}}').val();
                v_UsefulInformationJson.{{$c_Language}} = $('#informacoes_uteis_{{$c_Language}}').val();
                var v_Info = [];
                for(c_Index = 1; c_Index <= 3; c_Index++)
                {
                    var v_Data = {};
                    v_Data.dado = $('#dado_relevante_{{$c_Language}}'+c_Index).val();
                    v_Data.descricao = $('#dado_relevante_descricao_{{$c_Language}}'+c_Index).val();
                    v_Info.push(v_Data);
                }
                v_RelevantDataJson.{{$c_Language}} = v_Info;
            @endforeach

            $('#descricao_curta').val(JSON.stringify(v_DescriptionsJson));
            $('#sobre').val(JSON.stringify(v_AboutJson));
            $('#dados_relevantes').val(JSON.stringify(v_RelevantDataJson));
            $('#informacoes_uteis').val(JSON.stringify(v_UsefulInformationJson));

            $('#touristic_circuit_id').removeAttr('disabled');

            return true;
        }

        function addDate(p_Value)
        {
            var v_Div = ('<div class="date-div" id="date-div' + v_DateIndex.toString() + '" >' +
                '<div class="col-xs-1 date-filler-column form-group"> </div>' +
                '<div class="form-group col-sm-6 col-xs-8 date-picker">' +
                '<input id="data' + v_DateIndex.toString() + '" class="form-control" name="datas[]" type="text" value="">' +
                '</div>' +
                '<div class="col-xs-1 remove-date-column">' +
                '<a title="Excluir data" type="button" class="btn btn-success remove-date" onclick="removeDate(this)"><i class="fa fa-remove"></i></a>' +
                '</div>' +
                '</div>'
            );
            //Limita o máximo de datas em duas
            if ($('.date-div').length >= 2) return 0;
    
            $('.date-div:last').after(v_Div);
            $('.date-filler-column:last').height(v_DateHeight);
            v_DateHeight = $('.date-picker:last').height();
            $('.remove-date-column').height(v_DateHeight);
            $('#add-date-column').height(v_DateHeight);
            addDatePicker('#data' + v_DateIndex.toString(), p_Value);
            v_DateIndex ++;
        }
        function removeDate(p_DeleteBtn)
        {
            $(p_DeleteBtn).parent().parent().remove();
            v_DateHeight = $('.date-div:last').height();
            if(v_DateHeight > 0)
                $('#add-date-column').height(v_DateHeight);
        }


        function addDatePicker(p_Identifier, p_Value){
            $(p_Identifier).val(p_Value)
                    .datetimepicker({
                        pickTime: false,
                        format: "DD/MM/YYYY"
                    }).mask('99/99/9999')
                    .val(p_Value);
        }
    </script>
   
    @include('admin.util.tooltipScript', ['p_Type' => 'eventos'])
@stop