@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/daterange/daterangepicker.css')}}">
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Calendário de eventos
@stop
@section('content')

    <div class="row pt15">
        <div class="col-sm-12">
            <h3 class="mtn mb20">Filtros</h3>
        </div>
        {!! Form::open(['id' => 'mainForm', 'method' => 'GET', 'target' => '_blank', 'url'=> url('/admin/relatorios/calendario-eventos/gerar')]) !!}
        <div class="form-group col-sm-12">
            <label for="id">Município <i>(permite mais de uma opção)</i></label>
            {!! Form::select('id[]', $p_Cities, '', ['id' => 'id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="touristic_circuit_id">Região Turística</label>
            {!! Form::select('touristic_circuit_id', $p_Circuits, '', ['id' => 'touristic_circuit_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="region_id">Região</label>
            {!! Form::select('region_id', $p_Regions, '', ['id' => 'region_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="category_id">Categoria <i>(permite mais de uma opção)</i></label>
            {!! Form::select('category_id[]', $p_Categories, '', ['id' => 'category_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="category_id">Classificação <i>(permite mais de uma opção)</i></label>
            {!! Form::select('classification_id[]', $p_Classification, '', ['id' => 'classification_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <input type="checkbox" class="form-check-input" id="period-ex" name="period-ex">
            <label class="form-check-label" for="period-ex">Periodo Exclusivo</label>
            <input name="period" class="form-control dateInput" type="text">
        </div>

        <div class="form-group col-sm-6">
            <?php
                $v_TypeOptions = [
                    'Turismo de Lazer' => 'Turismo de Lazer',
                    'Turismo de Negócio' => 'Turismo de Negócio'
                ];
            ?>
            <label for="event_type_id">Tipo de Evento <i>(permite mais de uma opção)</i></label>
            {!! Form::select('event_type_id[]', $v_TypeOptions, '', ['id' => 'touristic_circuit_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        
        <div class="form-group col-sm-6">
            <label for="report_cover">Capa do Relatório</label>
            {!! Form::select('report_cover', $p_ReportCovers, '', ['id' => 'report_cover', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        
        
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Gerar relatório">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/datetime-moment.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/daterangepicker.js')}}"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.dateInput').daterangepicker({
                "autoApply": true,
                "locale": {
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "applyLabel": "Selecionar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "De",
                    "toLabel": "Até",
                    "daysOfWeek": [
                        "D",
                        "S",
                        "T",
                        "Q",
                        "Q",
                        "S",
                        "S"
                    ],
                    "customRangeLabel": "Intervalo",
                    "monthNames": [
                        "Janeiro",
                        "Fevereiro",
                        "Março",
                        "Abril",
                        "Maio",
                        "Junho",
                        "Julho",
                        "Agosto",
                        "Setembro",
                        "Outubro",
                        "Novembro",
                        "Dezembro"
                    ],
                    "firstDay": 1
                },
                ranges: {
                    'Hoje': [moment(), moment()],
                    'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                    'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                    'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                    'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                "linkedCalendars": false,
                "opens": "center",
                "autoUpdateInput": false
            }).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY')).blur();
            }).mask('99/99/9999 - 99/99/9999');
        });
    </script>
@stop