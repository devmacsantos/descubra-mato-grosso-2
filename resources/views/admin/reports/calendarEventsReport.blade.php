<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
    body {
        margin:0;
    }
    h3{
        text-transform: uppercase!important;
        margin: 5px 0;
        font-size: 17px;
    }
    p{
        margin: 9px 0;
    }
    .event {
        margin-bottom: 25px;
        page-break-inside: avoid;
    }
    .event .event-name{
        border-top: 1px solid;
        margin:0;
        padding:15px 0 8px;
    }
    .event p span{
        width: 130px;
        display: inline-block;
    }
    .month{
        text-transform: uppercase;
        color: #c63f43;
        margin: 8px 0 15px;
    }

    .last-page{
        font-weight: bold;
        page-break-inside: avoid;
    }
    .last-page p{
        line-height: 21px;
        font-size: 17px;
    }
</style>

</head>
<body>
    <div class="report-content">
        <?php
            $v_LastMonth = 0;
            $v_LastYear = 0;
            $v_ShowYear = false;
            if(count($p_Events)){
                $v_FirstEventYear = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_Events[0]->eventDates[0]->date)->year;
                $v_LastEventYear = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $p_Events[count($p_Events) - 1]->eventDates[0]->date)->year;
                if($v_FirstEventYear != $v_LastEventYear)
                    $v_ShowYear = true;
            }
        ?>
        @foreach($p_Events as $c_Event)
            <?php
                $v_Month = '';
                $v_Date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Event->eventDates[0]->date);
                $v_EventMonth = $v_Date->month;
                $v_EventYear = $v_Date->year;
                if($v_EventMonth != $v_LastMonth || $v_EventYear != $v_LastYear){
                    $v_LastMonth = $v_EventMonth;
                    $v_LastYear = $v_EventYear;
                    $v_Month = \App\Http\Controllers\BaseController::$m_Months[$v_EventMonth] . ($v_ShowYear ? ('/' . $v_EventYear) : '');
                }
            ?>
            <div class="event">
                @if(!empty($v_Month))
                    <h3 class="month">{{$v_Month}}</h3>
                @endif
                <h3 class="event-name">{{$c_Event->nome}}</h3>

                <p><span><strong>Município:</strong></span>{{$c_Event->cidade}}</p>

                <?php
                    $v_Period = '';
                    foreach($c_Event->eventDates as $c_Index => $c_Date)
                        $v_Period .= ($c_Index > 0 ? ', ' : '') . \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $c_Date->date)->format('d/m');
                ?>
                <p><span><strong>Data:</strong></span>{{$v_Period}}</p>

                <?php
                    $v_Categories = '';
                    foreach($c_Event->categories as $c_Index => $c_Category)
                        $v_Categories .= ($c_Index > 0 ? ', ' : '') . $c_Category->nome_pt;
                ?>
                <p><span><strong>Categoria:</strong></span>{{$v_Categories}}</p>

                <p><span><strong>Endereço:</strong></span>{{$c_Event->local_evento . ' - ' . $c_Event->bairro . ', ' . $c_Event->logradouro . ', ' . $c_Event->numero . (empty($c_Event->complemento) ? '' : (', ' . $c_Event->complemento))}}</p>
                
                @if(!empty($c_Event->circuito))
                    <p><span><strong>Região Turística:</strong></span>{{$c_Event->circuito}}</p>
                @endif

                @if(!empty($c_Event->telefone) || !empty($c_Event->site) || !empty($c_Event->facebook) || !empty(json_decode($c_Event->sobre,1)['pt']))
                    <p><strong>Organização/Informações:</strong></p>
                    @if(!empty($c_Event->telefone))
                        <p><span><strong>Telefone:</strong></span>{{$c_Event->telefone}}</p>
                    @endif
                    @if(!empty($c_Event->facebook))
                        <p><span><strong>Facebook:</strong></span>{{$c_Event->facebook}}</p>
                    @endif
                    @if(!empty($c_Event->site))
                        <p><span><strong>Site:</strong></span>{{$c_Event->site}}</p>
                    @endif
                    @if(!empty(json_decode($c_Event->sobre,1)['pt']))
                        <p><strong>Descrição:</strong></p>
                        <p>{!! json_decode($c_Event->sobre,1)['pt'] !!}</p>
                    @endif
                @endif
            </div>
        @endforeach
        <div class="last-page">
            {!! $p_EventCalendarLastPage !!}
        </div>
    </div>

</body>
</html>