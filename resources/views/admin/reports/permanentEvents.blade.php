@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/daterange/daterangepicker.css')}}">
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Relatório de eventos permanentes
@stop
@section('content')
    <div class="row pt15">
        <div class="col-sm-12">
            <h3 class="mtn mb20">Filtros</h3>
        </div>
        {!! Form::open(['id' => 'mainForm', 'method' => 'GET', 'target' => '_blank', 'url'=> url('/admin/relatorios/eventos-permanentes/gerar')]) !!}
        <div class="form-group col-sm-12">
            <label for="id">Município <i>(permite mais de uma opção)</i></label>
            {!! Form::select('id[]', $p_Cities, '', ['id' => 'id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <?php
                $v_PeriodOptions = [
                    'Mensal'=>'Mensal',
                    'Bimestral'=>'Bimestral',
                    'Trimestral'=>'Trimestral',
                    'Quadrimestral'=>'Quadrimestral',
                    'Semestral'=>'Semestral',
                    'Anual'=>'Anual'
                ];
            ?>
            <label for="periodicidade">Periodicidade <i>(permite mais de uma opção)</i></label>
            {!! Form::select('periodicidade[]', $v_PeriodOptions, null, ['id' => 'periodicidade', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <?php
                $v_RealizationOptions = [
                    'Público'=>'Público',
                    'Privado'=>'Privado',
                    'PPP'=>'PPP'
                ];
            ?>
            <label for="realizacao">Realização <i>(permite mais de uma opção)</i></label>
            {!! Form::select('realizacao[]', $v_RealizationOptions, null, ['id' => 'realizacao', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="meses">Período que ocorre <i>(permite mais de uma opção)</i></label>
            {!! Form::select('meses[]', \App\Http\Controllers\BaseController::$m_Months, [], ['id' => 'meses', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="category_id">Categoria <i>(permite mais de uma opção)</i></label>
            {!! Form::select('category_id[]', $p_Categories, '', ['id' => 'category_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Gerar relatório">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/datatables/extensions/TableTools/js/datetime-moment.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/daterangepicker.js')}}"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('#meses').change(function(){
                var v_Field = this;
                var v_Selection = $(v_Field).val();
                if($.inArray('Todos', v_Selection) > -1)
                {
                    if($(v_Field).find('option:selected').length == 13)
                        $(v_Field).select2("val", "");
                    else
                        $(v_Field).select2("val", ['1','2','3','4','5','6','7','8','9','10','11','12']);
                }
            });
        });
    </script>
@stop