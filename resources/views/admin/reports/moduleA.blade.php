@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Relatórios - Inventário - Módulo A
    <?php $v_Items = ['A1', 'A2.1', 'A2.2', 'A4', 'A5', 'A7']; ?>
    <ul class="nav panel-tabs-border panel-tabs">
        @foreach($v_Items as $c_Index => $c_Item)
        <li class="{{$c_Index == 0 ? 'active' : ''}}">
            <a href="#tab{{$c_Index}}" data-toggle="tab" aria-expanded="true">{{$c_Item}}</a>
        </li>
        @endforeach
    </ul>
@stop
@section('content')
    <div class="tab-content pn br-n">
        @foreach($v_Items as $c_Index => $c_Item)
            <div id="tab{{$c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                {!! Form::open(['url' => url('/admin/relatorios/inventario/modulo-a/' . $c_Item . '/gerar'), 'method' => 'GET', 'target' => '_blank']) !!}
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="city_id{{$c_Index}}">Município <i>(permite mais de uma opção)</i></label>
                        {!! Form::select('city_id[]', $p_Cities, '', ['id' => 'city_id'.$c_Index, 'class' => 'form-control select2' . ($c_Item == 'A2.2' ? ' cities' : ''), 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="touristic_circuit_id{{$c_Index}}">Região Turística</label>
                        {!! Form::select('touristic_circuit_id', $p_Circuits, '', ['id' => 'touristic_circuit_id'.$c_Index, 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="region_id{{$c_Index}}">Região</label>
                        {!! Form::select('region_id', $p_Regions, '', ['id' => 'region_id'.$c_Index, 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                    </div>
                    @if($c_Item == 'A1')
                        <div class="form-group col-sm-6">
                            <label for="regionalizado">Município regionalizado?</label>
                            {!! Form::select('regionalizado', ['' => '', 0 => 'Não', 1 => 'Sim'], '', ['id' => 'regionalizado', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                        </div>
                    @elseif($c_Item == 'A2.2')
                        <div class="form-group col-sm-6">
                            <label for="district_id{{$c_Index}}">Distrito</label>
                            {!! Form::select('district_id', ['' => ''], '', ['id' => 'district_id'.$c_Index, 'class' => 'form-control select2 district', 'style' => 'width: 100%']) !!}
                        </div>
                    @endif

                    <div class="form-group col-sm-12">
                        <button class="btn btn-default mt15 mb25 report-btn pull-right">Gerar relatório</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        @endforeach
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.cities').change(function(){
                var $v_DistrictField = $(this).closest('.tab-pane').find('.district');
                if($v_DistrictField.length) {
                    $.get("{{url('/admin/distritosMunicipios')}}", {city_ids:$(this).val()}, function(){
                    }).done(function(data){
                        if (data.error == 'ok')
                        {
                            var v_LastVal = $v_DistrictField.val();
                            var v_DataString = '<option value=""></option>';
                            $.each(data.data, function (c_Key, c_Field)
                            {
                                v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                            });

                            $v_DistrictField.html('<option value=""></option>' + v_DataString);
                            if(data.data.length == 0)
                                $v_DistrictField.select2("val", "");
                            else
                                $v_DistrictField.select2("val", v_LastVal);
                        }
                        else
                            $v_DistrictField.html('<option value=""></option>' + v_DataString).select2("val", "");
                    }).error(function(){
                    });
                }
            });
        });
    </script>
@stop