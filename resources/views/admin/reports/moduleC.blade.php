@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .full-width{
            width: 100%;
        }
        .align-center{
            text-align: center;
        }
        .margin-auto{
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    Relatórios - Inventário - Módulo C
    <?php $v_Items = ['C1', 'C2', 'C3', 'C4', 'C6.1', 'C6.2', 'C6.3']; ?>
    <ul class="nav panel-tabs-border panel-tabs">
        @foreach($v_Items as $c_Index => $c_Item)
        <li class="{{$c_Index == 0 ? 'active' : ''}}">
            <a href="#tab{{$c_Index}}" data-toggle="tab" aria-expanded="true">{{$c_Item}}</a>
        </li>
        @endforeach
    </ul>
@stop
@section('content')
    <?php
        $v_ProductorOptions = [
            'Indústrias' => 'Indústrias',
            'Restaurantes' => 'Restaurantes',
            'Artesãos' => 'Artesãos',
            'Associações' => 'Associações',
            'Donas de casa' => 'Donas de casa',
            'Agricultor familiar' => 'Agricultor familiar',
            'Convento/Freiras' => 'Convento/Freiras'
        ];
    ?>
    <div class="tab-content pn br-n">
        @foreach($v_Items as $c_Index => $c_Item)
            <div id="tab{{$c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                {!! Form::open(['url' => url('/admin/relatorios/inventario/modulo-c/' . $c_Item . '/gerar'), 'method' => 'GET', 'target' => '_blank']) !!}
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="city_id{{$c_Index}}">Município <i>(permite mais de uma opção)</i></label>
                        {!! Form::select('city_id[]', $p_Cities, '', ['id' => 'city_id'.$c_Index, 'class' => 'form-control select2 cities', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="touristic_circuit_id{{$c_Index}}">Região Turística</label>
                        {!! Form::select('touristic_circuit_id', $p_Circuits, '', ['id' => 'touristic_circuit_id'.$c_Index, 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="region_id{{$c_Index}}">Região</label>
                        {!! Form::select('region_id', $p_Regions, '', ['id' => 'region_id'.$c_Index, 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
                    </div>
                    <div class="form-group col-sm-6">
                        <label for="district_id{{$c_Index}}">Distrito</label>
                        {!! Form::select('district_id', ['' => ''], '', ['id' => 'district_id'.$c_Index, 'class' => 'form-control select2 district', 'style' => 'width: 100%']) !!}
                    </div>
                    @if(strpos($c_Item,'C6') === false)
                        <div class="form-group col-sm-6">
                            <label for="type_id{{$c_Index}}">Tipo</label>
                            {!! Form::select('type_id', ['' => ''] + $p_Types[$c_Item], '', ['id' => 'type_id'.$c_Index, 'class' => 'form-control select2 type_id', 'style' => 'width: 100%']) !!}
                        </div>
                        @if($c_Item != 'C4')
                            <div class="form-group col-sm-6">
                                <label for="sub_type_id{{$c_Index}}">Subtipo</label>
                                {!! Form::select('sub_type_id', ['' => ''], '', ['id' => 'sub_type_id'.$c_Index, 'class' => 'form-control select2 sub_type_id', 'style' => 'width: 100%']) !!}
                            </div>
                        @endif
                    @else
                        <div class="form-group col-sm-6">
                            <label for="nome{{$c_Index}}">{{$c_Item == 'C6.1' ? 'Produto primário' : ($c_Item == 'C6.1' ? 'Produto transformado' : 'Prato típico')}}</label>
                            {!! Form::text('nome', '', ['id' => 'nome'.$c_Index, 'class' => 'form-control', 'style' => 'width: 100%']) !!}
                        </div>
                    @endif

                    <div class="form-group col-sm-12">
                        <button class="btn btn-default mt15 mb25 report-btn pull-right">Gerar relatório</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        @endforeach
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.cities').change(function(){
                var $v_DistrictField = $(this).closest('.tab-pane').find('.district');
                if($v_DistrictField.length) {
                    $.get("{{url('/admin/distritosMunicipios')}}", {city_ids:$(this).val()}, function(){
                    }).done(function(data){
                        if (data.error == 'ok')
                        {
                            var v_LastVal = $v_DistrictField.val();
                            var v_DataString = '<option value=""></option>';
                            $.each(data.data, function (c_Key, c_Field)
                            {
                                v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                            });

                            $v_DistrictField.html(v_DataString);
                            if(data.data.length == 0)
                                $v_DistrictField.select2("val", "");
                            else
                                $v_DistrictField.select2("val", v_LastVal);
                        }
                        else
                            $v_DistrictField.html('<option value=""></option>' + v_DataString).select2("val", "");
                    }).error(function(){
                    });
                }
            });

            $('.type_id').change(function(){
                var $v_SubtypeField = $(this).closest('.tab-pane').find('.sub_type_id');
                if($v_SubtypeField.length) {
                    $.get("{{url('/admin/subtipos?super_type_id=')}}" + $(this).val(), function(){
                    }).done(function(data){
                        if (data.error == 'ok')
                        {
                            var v_LastVal = $v_SubtypeField.val();
                            var v_DataString = '';
                            $.each(data.data, function (c_Key, c_Field)
                            {
                                v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                            });

                            $v_SubtypeField.html('<option value=""></option>' + v_DataString);
                            if(data.data.length == 0)
                                $v_SubtypeField.select2("val", "");
                            else
                                $v_SubtypeField.select2("val", v_LastVal);
                        }
                        else
                            $v_SubtypeField.html('<option value=""></option>' + v_DataString).select2("val", "");
                    }).error(function(){
                    });
                }
            });
        });
    </script>
@stop