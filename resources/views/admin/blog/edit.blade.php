@extends('admin.mainTabs')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Article == null ? 'Cadastro' : 'Edição' }} de artigo
    <ul class="nav panel-tabs-border panel-tabs">
        <li class="active">
            <a href="#tab0" data-toggle="tab" aria-expanded="true">Português</a>
        </li>
        <li class="">
            <a href="#tab1" data-toggle="tab" aria-expanded="false">Inglês</a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="false">Espanhol</a>
        </li>
        <li class="">
            <a href="#tab3" data-toggle="tab" aria-expanded="false">Francês</a>
        </li>
    </ul>
@stop
@section('content')
    {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/blog'), 'onsubmit' => 'return submitForm()', 'files' => true)) !!}
    <div class="tab-content pn br-n">
        @if($p_Article != null)
            <input type="hidden" name="id" value="{{$p_Article->id}}">
        @endif
            <input type="hidden" id="form_titulo_pt" name="formulario[titulo_pt]">
            <input type="hidden" id="form_titulos" name="formulario[titulo]">
            <input type="hidden" id="form_descricoes" name="formulario[descricao_curta]">
            <input type="hidden" id="form_conteudos" name="formulario[conteudo]">

        <?php
            if($p_Article != null){
                $v_EmptyData = ['pt'=>'', 'en'=>'', 'es'=>'', 'fr'=>''];
                $v_Titles = $p_Article->titulo != null ? json_decode($p_Article->titulo,1) : $v_EmptyData;
                $v_Descriptions = $p_Article->descricao_curta != null ? json_decode($p_Article->descricao_curta,1) : $v_EmptyData;
                $v_Contents = $p_Article->conteudo != null ? json_decode($p_Article->conteudo,1) : $v_EmptyData;
            }
            $v_Languages = ['pt', 'en', 'es', 'fr'];
        ?>

        @foreach($v_Languages as $c_Index => $c_Language)
            <div id="{{'tab' . $c_Index}}" class="tab-pane {{ $c_Index == 0 ? 'active' : '' }}">
                @if($c_Index == 0)
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="camera" style="{{($p_Article == null || $p_Article->foto_capa_url == null) ? '' : 'background-image:url(' . $p_Article->foto_capa_url . ');'}}">
                                <img class="upload-btn-icon" style="{{($p_Article == null || $p_Article->foto_capa_url == null) ? '' : 'display:none'}}" src="{{url('/assets/img/camera.png')}}" alt="">
                                <h4 class="upload-btn-text" style="{{($p_Article == null || $p_Article->foto_capa_url == null) ? '' : 'display:none'}}">Foto de<br>capa</h4>
                                <input class="photo-upload-input" name="foto_capa" type="file" accept="image/gif, image/jpg, image/jpeg, image/png" onchange="onChangeImage(this)" {{ $p_Article == null ? 'required' : '' }}>
                                <input type="hidden" class="foto-capa-status" value="{{($p_Article == null || $p_Article->foto_capa_url == null) ? 'none' : 'old'}}">
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="form-group col-sm-12">
                        <label for="titulo_{{$c_Language}}">Título<span class="mandatory-field">*</span></label>
                        <input type="text" class="form-control" id="titulo_{{$c_Language}}" placeholder="Digite Aqui" value="{{$p_Article == null ? '' : $v_Titles[$c_Language]}}" {{$c_Index == 0 ? 'required' : ''}}>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="descricao_curta_{{$c_Language}}">Descrição curta<span class="mandatory-field">*</span></label>
                        <textarea id="descricao_curta_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$c_Index == 0 ? 'required' : ''}}>{{$p_Article == null ? '' : $v_Descriptions[$c_Language]}}</textarea>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="conteudo_{{$c_Language}}">Conteúdo<span class="mandatory-field">*</span></label>
                        <textarea id="conteudo_{{$c_Language}}" class="form-control" rows="4" placeholder="Digite Aqui" {{$c_Index == 0 ? 'required' : ''}}>{{$p_Article == null ? '' : $v_Contents[$c_Language]}}</textarea>
                    </div>

                    @if($c_Index == 0)
                        <div class="form-group col-sm-12">
                            <label for="hashtags">Tags<span class="mandatory-field">*</span></label>
                            {{-- $p_SelectedCategories é um array com as chaves das categorias selecionadas --}}
                            {!! Form::select('hashtags[]', $p_Hashtags, array_keys($p_Hashtags), ['id' => 'hashtags', 'class' => 'form-control select2-custom', 'style' => 'width: 100%', 'required' => 'required', 'multiple' => 'multiple']) !!}
                        </div>
                    @endif
                </div>
            </div>
        @endforeach

        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
    </div>
    {!! Form::close() !!}
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/ckeditor/ckeditor.js')}}" type="text/javascript"></script>
    @include('admin.util.photosScript', ['p_MaxPhotoCount' => -1])
    <script>
        $(document).ready(function()
        {
            $(".select2-custom").select2({language: 'pt-BR', tags: true});
            var v_Languages = {!! json_encode($v_Languages) !!}
            $(v_Languages).each(function(){
                CKEDITOR.replace('conteudo_' + this,{
                    allowedContent: true,
                    filebrowserImageBrowseUrl: null,
                    filebrowserFlashBrowseUrl: null,
                    filebrowserUploadUrl: "{{url('/admin/upload-arquivo')}}",
                    filebrowserImageUploadUrl: "{{url('/admin/upload-arquivo')}}",
                    filebrowserFlashUploadUrl: "{{url('/admin/upload-arquivo')}}"
                });
            });

//            {
//                //TODO: arrumar as urls
//
//            }
        });

        function submitForm()
        {
            var v_TitlesJson = {};
            var v_DescriptionsJson = {};
            var v_ContentsJson = {};
            @foreach($v_Languages as $c_Language)
                v_TitlesJson.{{$c_Language}} = $('#titulo_{{$c_Language}}').val();
                v_DescriptionsJson.{{$c_Language}} = $('#descricao_curta_{{$c_Language}}').val();
                v_ContentsJson.{{$c_Language}} = CKEDITOR.instances['conteudo_{{$c_Language}}'].getData();
            @endforeach

            $('#form_titulo_pt').val(v_TitlesJson.pt);
            $('#form_titulos').val(JSON.stringify(v_TitlesJson));
            $('#form_descricoes').val(JSON.stringify(v_DescriptionsJson));
            $('#form_conteudos').val(JSON.stringify(v_ContentsJson));
        }
    </script>
@stop