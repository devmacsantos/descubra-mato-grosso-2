@extends('admin.main')
@section('pageCSS')
    {{--<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />--}}
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/daterange/daterangepicker.css')}}">
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .remove-period, .add-period{
            height: 39px;
            width: 39px;
        }
        .remove-period-column, #add-period-column{
            display: flex;
            align-items: flex-end;
        }
        .remove-period-column{
            justify-content: center;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Circuit == null ? 'Cadastro' : 'Edição' }} de região turística
@stop
@section('content')
    <div class="row">
        @if(!\App\UserType::isParceiro())
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/circuitos'), 'onsubmit' => 'return submitForm()')) !!}
        @endif
        @if($p_Circuit != null)
            <input type="hidden" name="id" value="{{$p_Circuit->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="nome" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->nome}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome_oficial">Nome oficial<span class="mandatory-field">*</span></label>
            <input type="text" name="nome_oficial" class="form-control" id="nome_oficial" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->nome_oficial}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="status">Status<span class="mandatory-field">*</span></label>
            {!! Form::select('status', [''=>''] + \App\Http\Controllers\CircuitController::$m_CircuitStatus, $p_Circuit == null ? '' : $p_Circuit->status, ['id' => 'status', 'class' => 'form-control', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="situacao_certificacao">Situação da certificação</label>
            <textarea name="situacao_certificacao" id="situacao_certificacao" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Circuit == null ? '' : $p_Circuit->situacao_certificacao}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <label for="observacao">Observação</label>
            <textarea name="observacao" id="observacao" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Circuit == null ? '' : $p_Circuit->observacao}}</textarea>
        </div>
        <div class="form-group col-sm-6">
            <label for="city_id">Município<span class="mandatory-field">*</span></label>
            {!! Form::select('city_id', $p_Cities, $p_Circuit == null ? '' : $p_Circuit->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="cep">CEP<span class="mandatory-field">*</span></label>
            <input name="cep" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->cep}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="bairro">Bairro<span class="mandatory-field">*</span></label>
            <input type="text" name="bairro" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->bairro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="logradouro">Logradouro<span class="mandatory-field">*</span></label>
            <input type="text" name="logradouro" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->logradouro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="numero">Número<span class="mandatory-field">*</span></label>
            <input type="text" name="numero" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->numero}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="complemento">Complemento</label>
            <input type="text" name="complemento" class="form-control" id="complemento" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->complemento}}">
        </div>

        <div class="form-group col-sm-6">
            <label for="telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
            <input type="text" name="telefone" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->telefone}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="email">Email<span class="mandatory-field">*</span></label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->email}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="site">Site</label>
            <input type="url"  name="site" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->site}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="municipios_participantes">Municípios participantes <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
            {!! Form::select('municipios_participantes[]', $p_Cities, $p_CircuitCities, ['id' => 'municipios_participantes', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-12">
            <h3>Contato</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="contato_presidente">Presidente?</label>
            <p><input type="checkbox" value="1" name="contato_presidente" id="contato_presidente" class="ml5 mt10" {{($p_Circuit == null || $p_Circuit->contato_presidente == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-6 mandatory president-fields hidden">
            <label for="contato_presidente_nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="contato_presidente_nome" class="form-control" id="contato_presidente_nome" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_presidente_nome}}">
        </div>
        <div class="form-group col-sm-6 president-fields hidden">
            <label for="contato_presidente_telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
            <input type="text" name="contato_presidente_telefone" class="form-control phone-field" id="contato_presidente_telefone" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_presidente_telefone}}">
        </div>
        <div class="form-group col-sm-6 president-fields hidden">
            <label for="contato_presidente_email">Email</label>
            <input type="email" name="contato_presidente_email" class="form-control" id="contato_presidente_email" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_presidente_email}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="contato_gestor">Gestor?</label>
            <p><input type="checkbox" value="1" name="contato_gestor" id="contato_gestor" class="ml5 mt10" {{($p_Circuit == null || $p_Circuit->contato_gestor == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-6 mandatory manager-fields hidden">
            <label for="contato_gestor_nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="contato_gestor_nome" class="form-control" id="contato_gestor_nome" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_gestor_nome}}">
        </div>
        <div class="form-group col-sm-6 manager-fields hidden">
            <label for="contato_gestor_telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
            <input type="text" name="contato_gestor_telefone" class="form-control phone-field" id="contato_gestor_telefone" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_gestor_telefone}}">
        </div>
        <div class="form-group col-sm-6 manager-fields hidden">
            <label for="contato_gestor_email">Email</label>
            <input type="email" name="contato_gestor_email" class="form-control" id="contato_gestor_email" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_gestor_email}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="contato_assessor">Assessor?</label>
            <p><input type="checkbox" value="1" name="contato_assessor" id="contato_assessor" class="ml5 mt10" {{($p_Circuit == null || $p_Circuit->contato_assessor == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-6 mandatory advisor-fields hidden">
            <label for="contato_assessor_nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="contato_assessor_nome" class="form-control" id="contato_assessor_nome" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_assessor_nome}}">
        </div>
        <div class="form-group col-sm-6 advisor-fields hidden">
            <label for="contato_assessor_telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
            <input type="text" name="contato_assessor_telefone" class="form-control phone-field" id="contato_assessor_telefone" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_assessor_telefone}}">
        </div>
        <div class="form-group col-sm-6 advisor-fields hidden">
            <label for="contato_assessor_email">Email</label>
            <input type="email" name="contato_assessor_email" class="form-control" id="contato_assessor_email" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_assessor_email}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="contato_secretaria">Secretária?</label>
            <p><input type="checkbox" value="1" name="contato_secretaria" id="contato_secretaria" class="ml5 mt10" {{($p_Circuit == null || $p_Circuit->contato_secretaria == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-6 mandatory secretary-fields hidden">
            <label for="contato_secretaria_nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="contato_secretaria_nome" class="form-control" id="contato_secretaria_nome" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_secretaria_nome}}">
        </div>
        <div class="form-group col-sm-6 secretary-fields hidden">
            <label for="contato_secretaria_telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
            <input type="text" name="contato_secretaria_telefone" class="form-control phone-field" id="contato_secretaria_telefone" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_secretaria_telefone}}">
        </div>
        <div class="form-group col-sm-6 secretary-fields hidden">
            <label for="contato_secretaria_email">Email</label>
            <input type="email" name="contato_secretaria_email" class="form-control" id="contato_secretaria_email" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_secretaria_email}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="contato_tesoureiro">Tesoureiro?</label>
            <p><input type="checkbox" value="1" name="contato_tesoureiro" id="contato_tesoureiro" class="ml5 mt10" {{($p_Circuit == null || $p_Circuit->contato_tesoureiro == 0) ? '' : 'checked'}}></p>
        </div>
        <div class="form-group col-sm-6 mandatory treasurer-fields hidden">
            <label for="contato_tesoureiro_nome">Nome<span class="mandatory-field">*</span></label>
            <input type="text" name="contato_tesoureiro_nome" class="form-control" id="contato_tesoureiro_nome" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_tesoureiro_nome}}">
        </div>
        <div class="form-group col-sm-6 treasurer-fields hidden">
            <label for="contato_tesoureiro_telefone">Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)</label>
            <input type="text" name="contato_tesoureiro_telefone" class="form-control phone-field" id="contato_tesoureiro_telefone" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_tesoureiro_telefone}}">
        </div>
        <div class="form-group col-sm-6 treasurer-fields hidden">
            <label for="contato_tesoureiro_email">Email</label>
            <input type="email" name="contato_tesoureiro_email" class="form-control" id="contato_tesoureiro_email" placeholder="Digite Aqui" value="{{$p_Circuit == null ? '' : $p_Circuit->contato_tesoureiro_email}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="historico">Histórico</label>
            <textarea name="historico" id="historico" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_Circuit == null ? '' : $p_Circuit->historico}}</textarea>
        </div>
        <div class="form-group col-sm-6">
            <label for="responsavel_id">Técnico responsável<span class="mandatory-field">*</span></label>
            {!! Form::select('responsavel_id', $p_Users, $p_Circuit == null ? '' : $p_Circuit->responsavel_id, ['id' => 'responsavel_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @endif
    </div>
@stop
@section('pageScript')
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>--}}
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/plugins/daterange/daterangepicker.js')}}"></script>
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});
            $(".select2-custom").select2({language:'pt-BR', tags: true});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('.cep-field').mask('99.999-999');
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            toggleFields('contato_presidente', 'president-fields');
            toggleFields('contato_gestor', 'manager-fields');
            toggleFields('contato_assessor', 'advisor-fields');
            toggleFields('contato_secretaria', 'secretary-fields');
            toggleFields('contato_tesoureiro', 'treasurer-fields');
        });

        function toggleFields(p_Switch, p_Fields)
        {
            $('#' + p_Switch).change(function(){
                if($(this).is(":checked"))
                {
                    $('.' + p_Fields).removeClass('hidden');
                    $('.' + p_Fields + '.mandatory .form-control').attr('required', true);
                }
                else
                {
                    $('.' + p_Fields).addClass('hidden');
                    $('.' + p_Fields + '.mandatory .form-control').removeAttr('required');
                }
            }).trigger('change');
        }

        function submitForm()
        {
            if($('#contato_presidente:checked, #contato_gestor:checked, #contato_assessor:checked, #contato_secretaria:checked, #contato_tesoureiro:checked').length == 0)
            {
                alert('Ao menos um contato deve ser preenchido!');
                return false;
            }
            return true;
        }
    </script>
@stop