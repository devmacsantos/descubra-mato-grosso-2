@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .supply-title h5{
            color:#000;
            text-decoration: underline;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
        #permanentEventModal .modal-dialog{
            z-index:1051;
        }
        #tablePermanentEvent tbody{
            max-height:400px;
            overflow-y: scroll;
        }
        #tablePermanentEvent tbody p{
            margin-bottom: 1px;
        }
        #tablePermanentEvent tbody .table-actions > .btn{
            width: 36px;
            margin-right: 3px;
            margin-top: 1px;
            margin-bottom: 1px;
        }
        #tablePermanentEvent tbody .event-description {
            max-width: 200px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden;
        }
        @media (min-width: 768px){
            .modal-dialog {
                width: 760px;
            }
        }
    </style>
@stop
@section('panel-header')
    A1 - Informações básicas do município
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <div class="row">
        <div class="col-sm-12 visible-print">
            <h2>A1 - Informações básicas do município</h2>
        </div>
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/informacoes'), 'onsubmit' => 'return submitForm()']) !!}
        @else
        <div id="mainForm">
        @endif

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
        </div>
        @if($p_CityInfo != null)
            <input type="hidden" name="id" value="{{$p_CityInfo->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
            <?php $v_CityOptions = count($p_CityOptions) == 1 ? $p_CityOptions : ([''=>''] + $p_CityOptions); ?>
            {!! Form::select('formulario[city_id]', $v_CityOptions, $p_CityInfo == null ? '' : $p_CityInfo->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="circuito_turistico">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Região Turística</label>
            <input type="text" id="circuito_turistico" class="form-control" disabled>
        </div>
        <div class="form-group col-sm-6">
            <label for="regiao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Região</label>
            <input type="text" id="regiao" class="form-control" disabled>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Apresentação do município</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP da prefeitura<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_endereco_cep]" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_endereco_cep}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_endereco_bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_endereco_bairro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_endereco_logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_endereco_logradouro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_endereco_numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_endereco_numero}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
            <input type="text" name="formulario[prefeitura_endereco_complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_endereco_complemento}}">
            <!--<a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                <i class="fa fa-map-marker"></i>
            </a>-->
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeitura_telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_telefone]" class="form-control phone-field" id="prefeitura_telefone" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_telefone}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeitura_site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site<span class="mandatory-field">*</span></label>
            <input type="url" name="formulario[prefeitura_site]" class="form-control" id="prefeitura_site" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_site}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeitura_email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_email]" class="email-field  form-control" id="prefeitura_email" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_email}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="registro_estadual">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Registro estadual</label>
            <input type="text" name="formulario[registro_estadual]" class="form-control" id="registro_estadual" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->registro_estadual}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
            <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
        </div>
        <div class="form-group col-sm-6">
            <label for="latitude" >{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->latitude}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->longitude}}">
        </div>
        <div class="form-group col-sm-12">
            <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
            <div id="map_canvas" style="height:320px;"></div>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Informações gerais</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="populacao_total">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}População total (Hab.)</label>
            <input type="text" name="formulario[populacao_total]" class="form-control" id="populacao_total" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? 0 : $p_CityInfo->populacao_total}}" readonly>
        </div>
        <div class="form-group col-sm-6">
            <label for="populacao_urbana">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}População urbana (Hab.)</label>
            <input type="text" name="formulario[populacao_urbana]" class="form-control integer-field" id="populacao_urbana" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->populacao_urbana}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="populacao_rural">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}População rural (Hab.)</label>
            <input type="text" name="formulario[populacao_rural]" class="form-control integer-field" id="populacao_rural" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->populacao_rural}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="area_total">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Área total do município (Km<sup>2</sup>)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[area_total]" class="form-control currency-field" id="area_total" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->area_total}}" required>
        </div>
        <div class="form-group col-sm-12">
            <label for="municipios_limitrofes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Municípios limítrofes <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
            @if($p_CityInfo == null)
            {!! Form::select('', [], null, ['id' => 'municipios_limitrofes', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
            @else
            {!! Form::select('', $p_Cities, null, ['id' => 'municipios_limitrofes', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
            @endif
            <input type="hidden" name="formulario[municipios_limitrofes]" class="form-control" id="municipios_limitrofes_hidden">
        </div>
        <div class="form-group col-sm-6">
            <label for="temperatura_media">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Temperatura média anual (<sup>o</sup>C)<span class="mandatory-field">*</span></label>
            <input type="number" step="0.1" name="formulario[temperatura_media]" class="form-control" id="temperatura_media" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->temperatura_media}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="temperatura_min">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Temperatura mínima (<sup>o</sup>C)</label>
            <input type="number" step="0.1" name="formulario[temperatura_min]" class="form-control" id="temperatura_min" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->temperatura_min}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="temperatura_max">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Temperatura máxima (<sup>o</sup>C)</label>
            <input type="number" step="0.1" name="formulario[temperatura_max]" class="form-control" id="temperatura_max" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->temperatura_max}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="meses_secos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Período de seca <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
            <?php $v_MonthRange = $p_CityInfo == null ? [] : explode(';', $p_CityInfo->meses_secos); ?>
            {!! Form::select('meses_secos[]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, array('id' => 'meses_secos', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%')) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="meses_chuvosos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Período de chuva <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
            <?php $v_MonthRange = $p_CityInfo == null ? [] : explode(';', $p_CityInfo->meses_chuvosos); ?>
            {!! Form::select('meses_chuvosos[]', \App\Http\Controllers\BaseController::$m_Months, $v_MonthRange, array('id' => 'meses_chuvosos', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%')) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="clima">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Clima <i>(permite mais de uma opção)</i></label>
            <input type="hidden" id="clima" name="formulario[clima]" value="{{$p_CityInfo == null ? '' : $p_CityInfo->clima}}">
            {!! Form::select('', $p_Climates, null, ['id' => 'clima_select', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="altitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Altitude</label>
            <input type="text" name="formulario[altitude]" class="form-control currency-field" id="altitude" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->altitude}}">
        </div>

        <div class="form-group col-sm-12">
            <label for="principais_atividades_economicas">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Principais atividades econômicas <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
            {!! Form::select('', $p_EconomicActivities, [], ['id' => 'principais_atividades_economicas', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
            <input type="hidden" name="formulario[principais_atividades_economicas]" class="form-control" id="principais_atividades_economicas_hidden">
        </div>

        <div class="form-group col-sm-12">
            <label for="servicos_telefonia_movel">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviços de telefonia e internet móvel <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
            <?php $v_SelectedServices = $p_CityInfo == null ? [] : explode(';', $p_CityInfo->servicos_telefonia_movel); ?>
            {!! Form::select('servicos_telefonia_movel[]', $p_MobileCarrierServices, $v_SelectedServices, ['id' => 'servicos_telefonia_movel', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="servicos_telefonia_movel_outros">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviços de telefonia e internet móvel - Outros</label>
            <textarea name="formulario[servicos_telefonia_movel_outros]" id="servicos_telefonia_movel_outros" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_CityInfo == null ? '' : $p_CityInfo->servicos_telefonia_movel_outros}}</textarea>
        </div>
        <div class="form-group col-sm-6">
            <label for="postos_telefonicos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Postos telefônicos público?<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[postos_telefonicos]', [''=>'', '0' => 'Não', '1' => 'Sim'], $p_CityInfo == null ? '' : $p_CityInfo->postos_telefonicos , array('id' => 'postos_telefonicos', 'class'=>'form-control', 'required' => 'required')) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="emissora_radio">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Emissora de rádio?<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[emissora_radio]', [''=>'', '0' => 'Não', '1' => 'Sim'], $p_CityInfo == null ? '' : $p_CityInfo->emissora_radio , array('id' => 'emissora_radio', 'class'=>'form-control', 'required' => 'required')) !!}
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Administração municipal</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeito_nome">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome do prefeito<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeito_nome]" class="form-control" id="prefeito_nome" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeito_nome}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeito_telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeito_telefone]" class="form-control phone-field" id="prefeito_telefone" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeito_telefone}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeito_email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeito_email]" class="email-field form-control" id="prefeito_email" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeito_email}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeito_partido_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Partido<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[prefeito_partido_id]', $p_PoliticalParties, $p_CityInfo == null ? '' : $p_CityInfo->prefeito_partido_id, array('id' => 'prefeito_partido_id', 'class' => 'form-control', 'required' => 'required')) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeitura_funcionarios_fixos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de funcionários permanentes<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_funcionarios_fixos]" class="form-control integer-field" id="prefeitura_funcionarios_fixos" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_funcionarios_fixos}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeitura_funcionarios_temporarios">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de funcionários temporários<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_funcionarios_temporarios]" class="form-control integer-field" id="prefeitura_funcionarios_temporarios" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_funcionarios_temporarios}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeitura_funcionarios_deficiencia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de funcionários com deficiência<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_funcionarios_deficiencia]" class="form-control integer-field" id="prefeitura_funcionarios_deficiencia" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_funcionarios_deficiencia}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="prefeitura_funcionarios_total">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Total de funcionários da prefeitura<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[prefeitura_funcionarios_total]" class="form-control" id="prefeitura_funcionarios_total" value="{{$p_CityInfo == null ? '0' : $p_CityInfo->prefeitura_funcionarios_total}}" readonly>
        </div>
        <div class="form-group col-sm-12">
            <label for="prefeitura_nomes_departamentos">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome das secretarias, departamentos e outros<span class="mandatory-field">*</span></label>
            <textarea name="formulario[prefeitura_nomes_departamentos]" id="prefeitura_nomes_departamentos" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_CityInfo == null ? '' : $p_CityInfo->prefeitura_nomes_departamentos}}</textarea>
        </div>

        <div class="form-group col-sm-6">
            <label for="possui_orgao_turismo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui órgão responsável pelo turismo?</label>
            <p><input type="checkbox" value="1" name="formulario[possui_orgao_turismo]" id="possui_orgao_turismo" class="ml5 mt10" {{($p_CityInfo == null || $p_CityInfo->possui_orgao_turismo == 0) ? '' : 'checked'}}></p>
        </div>

        <div class="orgao-turismo-fields">
            <div class="form-group col-sm-6">
                <?php
                    $v_OrganTypes = [
                        ''=>'',
                        'Coordenação'=>'Coordenação',
                        'Departamento'=>'Departamento',
                        'Diretoria'=>'Diretoria',
                        'Gerência'=>'Gerência',
                        'Secretaria'=>'Secretaria',
                        'Setor'=>'Setor',
                        'Superintendência'=>'Superintendência'
                    ];
                ?>
                <label for="orgao_turismo_tipo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}De qual tipo?<span class="mandatory-field">*</span></label>
                {!! Form::select('formulario[orgao_turismo_tipo]', $v_OrganTypes, $p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_tipo , ['id' => 'orgao_turismo_tipo', 'class'=>'form-control']) !!}
            </div>

            <div class="form-group col-sm-6">
                <label for="orgao_turismo_nome">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Nome do órgão oficial de Turismo<span class="mandatory-field">*</span></label>
                <input type="text" name="formulario[orgao_turismo_nome]" class="form-control" id="orgao_turismo_nome" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_nome}}">
            </div>
            <div class="form-group col-sm-6">
                <label for="orgao_turismo_titular">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Títular do órgão<span class="mandatory-field">*</span></label>
                <input type="text" name="formulario[orgao_turismo_titular]" class="form-control" id="orgao_turismo_titular" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_titular}}">
            </div>
        </div>

        <div class="form-group col-sm-6">
            <label for="orgao_turismo_orcamento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Possui dotação orçamentária destinada ao turismo?<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[orgao_turismo_orcamento]', [''=>'', '0' => 'Não', '1' => 'Sim'], $p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_orcamento , array('id' => 'orgao_turismo_orcamento', 'class'=>'form-control', 'required' => 'required')) !!}
        </div>
        <div class="budget-fields">
            <div class="form-group col-sm-6">
                <label for="orgao_turismo_orcamento_ultimo_ano">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Valor da dotação no último ano - R$</label>
                <input type="text" name="formulario[orgao_turismo_orcamento_ultimo_ano]" class="form-control currency-field" id="orgao_turismo_orcamento_ultimo_ano" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_orcamento_ultimo_ano}}">
            </div>
            <div class="form-group col-sm-6">
                <label for="orgao_turismo_orcamento_ano_referencia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(2)}}Ano de referência</label>
                <input type="number" min="1800" max="2500" name="formulario[orgao_turismo_orcamento_ano_referencia]" class="form-control" id="orgao_turismo_orcamento_ano_referencia" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_orcamento_ano_referencia}}">
            </div>
        </div>
        <div class="form-group col-sm-12">
            <label for="orgao_turismo_acoes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Ações executadas pelo município relativas ao turismo (direta ou indiretamente) - (últimos 5 anos) <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_TourismActions, null, ['id' => 'orgao_turismo_acoes', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
            <input type="hidden" name="formulario[orgao_turismo_acoes]" class="form-control" id="orgao_turismo_acoes_hidden">
        </div>
        <div class="form-group col-sm-12">
            <label for="orgao_turismo_acoes_outras">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outras ações</label>
            <textarea name="formulario[orgao_turismo_acoes_outras]" id="orgao_turismo_acoes_outras" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_acoes_outras}}</textarea>
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_endereco_cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP do órgão de turismo<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[orgao_turismo_endereco_cep]" class="form-control cep-field" id="orgao_turismo_endereco_cep" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_endereco_cep}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_endereco_bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[orgao_turismo_endereco_bairro]" class="form-control" id="orgao_turismo_endereco_bairro" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_endereco_bairro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_endereco_logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[orgao_turismo_endereco_logradouro]" class="form-control" id="orgao_turismo_endereco_logradouro" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_endereco_logradouro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_endereco_numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[orgao_turismo_endereco_numero]" class="form-control" id="orgao_turismo_endereco_numero" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_endereco_numero}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_endereco_complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
            <input type="text" name="formulario[orgao_turismo_endereco_complemento]" class="form-control" id="orgao_turismo_endereco_complemento" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_endereco_complemento}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
            <input type="url" name="formulario[orgao_turismo_site]" class="form-control" id="orgao_turismo_site" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_site}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[orgao_turismo_email]" class="email-field form-control" id="orgao_turismo_email" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_email}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="orgao_turismo_telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[orgao_turismo_telefone]" class="form-control phone-field" id="orgao_turismo_telefone" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->orgao_turismo_telefone}}" required>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Legislação municipal</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_lei_organica">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Lei orgânica do município<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_lei_organica]" class="form-control" id="legislacao_lei_organica" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_lei_organica}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_criacao_conselho_turismo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Lei de criação do Conselho Municipal de Turismo<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_criacao_conselho_turismo]" class="form-control" id="legislacao_criacao_conselho_turismo" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_criacao_conselho_turismo}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_ocupacao_solo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Legislação de ocupação do solo<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_ocupacao_solo]" class="form-control" id="legislacao_ocupacao_solo" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_ocupacao_solo}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_protecao_ambiental">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Legislação de proteção ambiental<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_protecao_ambiental]" class="form-control" id="legislacao_protecao_ambiental" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_protecao_ambiental}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_apoio_cultura">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Legislação de apoio à cultura<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_apoio_cultura]" class="form-control" id="legislacao_apoio_cultura" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_apoio_cultura}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_incentivo_fiscal_turismo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Legislação de incentivos fiscais ao turismo<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_incentivo_fiscal_turismo]" class="form-control" id="legislacao_incentivo_fiscal_turismo" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_incentivo_fiscal_turismo}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_regulamentacao_turismo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Regulamentação específica do turismo<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_regulamentacao_turismo]" class="form-control" id="legislacao_regulamentacao_turismo" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_regulamentacao_turismo}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_plano_desenvolvimento_turismo">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Plano de desenvolvimento do turismo<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[legislacao_plano_desenvolvimento_turismo]" class="form-control" id="legislacao_plano_desenvolvimento_turismo" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_plano_desenvolvimento_turismo}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="legislacao_plano_diretor">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Plano diretor<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[legislacao_plano_diretor]', [''=>'', '0' => 'Não', '1' => 'Sim'], $p_CityInfo == null ? '' : $p_CityInfo->legislacao_plano_diretor , ['id' => 'legislacao_plano_diretor', 'class'=>'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="legislacao_outras">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Outras</label>
            <textarea name="formulario[legislacao_outras]" id="legislacao_outras" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_CityInfo == null ? '' : $p_CityInfo->legislacao_outras}}</textarea>
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Equipamentos, instalações e serviços públicos</h3>
        </div>
        <div class="col-sm-12">
            <h4 id="agua_abastecimento_index">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Abastecimento de água</h4>
        </div>
        <div class="form-group col-sm-12">
            <label>Tipos de abastecimento <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_WaterSupply, [], ['id' => 'agua_abastecimento', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <input id="agua_abastecimento_hidden" type="hidden" name="formulario[agua_abastecimento]">

        <div class="col-sm-12">
            <hr>
            <h4 id="esgotamento_index">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviços de esgoto</h4>
        </div>
        <div class="form-group col-sm-12">
            <label>Tipos de esgotamento <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_SewerSupply, [], ['id' => 'esgotamento', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <input id="esgotamento_hidden" type="hidden" name="formulario[esgotamento]">

        <div class="col-sm-12">
            <hr>
            <h4 id="energia_abastecimento_index">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviços de energia</h4>
        </div>
        <div class="form-group col-sm-12">
            <label>Tipos de abastecimento <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_EnergySupply, [], ['id' => 'energia_abastecimento', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <input id="energia_abastecimento_hidden" type="hidden" name="formulario[energia_abastecimento]">

        <div class="col-sm-12">
            <hr>
            <h4 id="destinacao_lixo_index">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviços de coleta de lixo</h4>
        </div>
        <div class="form-group col-sm-12">
            <label>Destinação do lixo <i>(permite mais de uma opção)</i></label>
            {!! Form::select('', $p_GarbageSupply, [], ['id' => 'destinacao_lixo', 'class' => 'form-control select2', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
        </div>
        <input id="destinacao_lixo_hidden" type="hidden" name="formulario[destinacao_lixo]">

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Demanda turística</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="ano_base">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Ano base da informação</label>
            <input type="number" name="formulario[ano_base]" class="form-control" id="ano_base" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->ano_base}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="visitantes_total">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número total de visitantes</label>
            <input type="text" name="formulario[visitantes_total]" class="form-control integer-field" id="visitantes_total" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->visitantes_total}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="visitantes_alta_temporada">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de visitantes nos meses de alta temporada</label>
            <input type="text" name="formulario[visitantes_alta_temporada]" class="form-control integer-field" id="visitantes_alta_temporada" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->visitantes_alta_temporada}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="visitantes_baixa_temporada">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de visitantes nos meses de baixa temporada</label>
            <input type="text" name="formulario[visitantes_baixa_temporada]" class="form-control integer-field" id="visitantes_baixa_temporada" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->visitantes_baixa_temporada}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="visitantes_regionais">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de visitantes de procedência regional</label>
            <input type="text" name="formulario[visitantes_regionais]" class="form-control integer-field" id="visitantes_regionais" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->visitantes_regionais}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="visitantes_estaduais">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de visitantes de procedência estadual</label>
            <input type="text" name="formulario[visitantes_estaduais]" class="form-control integer-field" id="visitantes_estaduais" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->visitantes_estaduais}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="visitantes_nacionais">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de visitantes de procedência nacional</label>
            <input type="text" name="formulario[visitantes_nacionais]" class="form-control integer-field" id="visitantes_nacionais" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->visitantes_nacionais}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="visitantes_internacionais">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número de visitantes de procedência internacional</label>
            <input type="text" name="formulario[visitantes_internacionais]" class="form-control integer-field" id="visitantes_internacionais" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->visitantes_internacionais}}">
        </div>

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Fontes de dados e informações sobre o município</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="fontes_dados_instituicao">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome da instituição<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[fontes_dados_instituicao]" class="form-control" id="fontes_dados_instituicao" placeholder="Digite Aqui" value="{{$p_CityInfo == null ? '' : $p_CityInfo->fontes_dados_instituicao}}" required>
        </div>
        <div class="form-group col-sm-12">
            <label for="fontes_dados_site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site<span class="mandatory-field">*</span></label>
            <textarea name="formulario[fontes_dados_site]" id="fontes_dados_site" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_CityInfo == null ? '' : $p_CityInfo->fontes_dados_site}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <label for="fontes_dados_publicacoes">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Publicações</label>
            <textarea name="formulario[fontes_dados_publicacoes]" id="fontes_dados_publicacoes" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_CityInfo == null ? '' : $p_CityInfo->fontes_dados_publicacoes}}</textarea>
        </div>

        @include('admin.moduleA.cityInfo.util.permanentEvents', ['p_Events' => $p_Events])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="historico_municipio">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Histórico do município<span class="mandatory-field">*</span></label>
            <textarea name="formulario[historico_municipio]" id="historico_municipio" class="form-control" rows="4" placeholder="Digite Aqui" required>{{$p_CityInfo == null ? '' : $p_CityInfo->historico_municipio}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <label for="parceirias_cooperacoes_intercambios_interfaces">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Principais parcerias, rede de cooperação, intercâmbios e interfaces com o município</label>
            <textarea name="formulario[parceirias_cooperacoes_intercambios_interfaces]" id="parceirias_cooperacoes_intercambios_interfaces" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_CityInfo == null ? '' : $p_CityInfo->parceirias_cooperacoes_intercambios_interfaces}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <label for="descricao_informacoes_complementares">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e informações complementares</label>
            <textarea name="formulario[descricao_informacoes_complementares]" id="descricao_informacoes_complementares" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_CityInfo == null ? '' : $p_CityInfo->descricao_informacoes_complementares}}</textarea>
        </div>

        <?php
            $v_ShowInternalUsage = false;
            if($p_CityInfo != null){
                $v_RevisionStatus = $p_CityInfo->revision_status_id;
                $v_ShowInternalUsage = ($v_RevisionStatus == 5
                                        || (\App\UserType::isMunicipio() && $v_RevisionStatus == 1)
                                        || (\App\UserType::isCircuito() && $v_RevisionStatus != 4)
                                        || ((\App\UserType::isMaster() || \App\UserType::isAdmin()) && $v_RevisionStatus != 4));
            }
        ?>
        @if($v_ShowInternalUsage)
            <div class="form-group col-sm-12">
                <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
            </div>
        @endif
        @include('admin.util.revision', ['p_Form' => $p_CityInfo])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_CityInfo])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>
    <div id="modalContainer">
        @include('admin.moduleA.cityInfo.util.permanentEventsModal', ['p_EventCategories' => $p_EventCategories, 'p_AllCities' => $p_AllCities])
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.moduleA.cityInfo.util.permanentEventsScript', ['p_EventCategories' => $p_EventCategories])
    <script>
        @if($p_CityInfo == null)
        var v_AllCities = '{!! json_encode($p_Cities) !!}';
        v_AllCities = v_AllCities.length > 0 ? JSON.parse(v_AllCities) : [];
        @endif
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('.cep-field').inputmask("99.999-999");
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('#prefeitura_funcionarios_fixos, #prefeitura_funcionarios_temporarios').on('keyup change', function(){
                var v_Fixos = parseInt($('#prefeitura_funcionarios_fixos').val().replace(/\./g,''));
                v_Fixos = isNaN(v_Fixos) ? 0 : parseInt(v_Fixos);
                var v_Temporarios = parseInt($('#prefeitura_funcionarios_temporarios').val().replace(/\./g,''));
                v_Temporarios = isNaN(v_Temporarios) ? 0 : parseInt(v_Temporarios);
                var v_Total = v_Fixos + v_Temporarios;
                $('#prefeitura_funcionarios_total').val(v_Total.formatMoney(0, ',', '.')).change();
            }).change();

            $('#populacao_urbana, #populacao_rural').on('keyup change', function(){
                var v_Urbana = parseInt($('#populacao_urbana').val().replace(/\./g,''));
                v_Urbana = isNaN(v_Urbana) ? 0 : parseInt(v_Urbana);
                var v_Rural = parseInt($('#populacao_rural').val().replace(/\./g,''));
                v_Rural = isNaN(v_Rural) ? 0 : parseInt(v_Rural);
                var v_Total = v_Urbana + v_Rural;
                $('#populacao_total').val(v_Total.formatMoney(0, ',', '.')).change();
            }).change();

            $('#orgao_turismo_orcamento').change(function(){
                if($(this).val() == '1')
                    $('.budget-fields').show();
                else
                    $('.budget-fields').hide();
            }).change();

//            $.validator.messages.required = 'Campo Obrigatório';
//            $('#mainForm').validate({
//                rules:{
//                    "nome" :{required: true}
//                }
//            });

            var v_Climates = $('#clima').val();
            if(v_Climates.length > 0)
            {
                v_Climates = JSON.parse(v_Climates);
                var v_SelectedItems = [];
                $(v_Climates).each(function(){
                    v_SelectedItems.push(this.id);
                });
                $('#clima_select').select2("val", v_SelectedItems);
            }

            $('#meses_secos, #meses_chuvosos').change(function(){
                var v_Field = this;
                var v_Selection = $(v_Field).val();
                if($.inArray('Todos', v_Selection) > -1)
                {
                    if($(v_Field).find('option:selected').length == 13)
                        $(v_Field).select2("val", "");
                    else
                        $(v_Field).select2("val", ['1','2','3','4','5','6','7','8','9','10','11','12']);
                }
            });

            $('#possui_orgao_turismo').change(function(){
                if($(this).is(":checked")){
                    $('.orgao-turismo-fields').show();
                    $('.orgao-turismo-fields .form-control').attr('required', true);
                }
                else {
                    $('.orgao-turismo-fields').hide();
                    $('.orgao-turismo-fields .form-control').removeAttr('required');
                }
            }).change();

            @if($p_CityInfo != null)
                $('#city_id').prop('disabled', true);
                $.get("{{url('/admin/circuitoTuristicoMunicipio?city_id=')}}" + $('#city_id').val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                        $('#circuito_turistico').val(data.data);
                    else
                        $('#circuito_turistico').val('');
                }).error(function(){
                });
                $.get("{{url('/admin/regiaoMunicipio?city_id=')}}" + $('#city_id').val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                        $('#regiao').val(data.data);
                    else
                        $('#regiao').val('');
                }).error(function(){
                });

                var v_SelectedCities = JSON.parse('{!! $p_CityInfo->municipios_limitrofes !!}');
                var v_Cities = [];
                $(v_SelectedCities).each(function(){
                    v_Cities.push(this.id);
                });
                if(v_Cities.length > 0)
                    $('#municipios_limitrofes').select2("val", v_Cities);

                var v_SelectedEconomicActivities = JSON.parse('{!! $p_CityInfo->principais_atividades_economicas !!}');
                var v_Activities = [];
                $(v_SelectedEconomicActivities).each(function(){
                    v_Activities.push(this.id);
                });
                if(v_Activities.length > 0)
                    $('#principais_atividades_economicas').select2("val", v_Activities);

                var v_SelectedActions = JSON.parse('{!! $p_CityInfo->orgao_turismo_acoes !!}');
                var v_Actions = [];
                $(v_SelectedActions).each(function(){
                    v_Actions.push(this.id);
                });
                if(v_Actions.length > 0)
                    $('#orgao_turismo_acoes').select2("val", v_Actions);

                var v_SupplyData = '{!! $p_CityInfo->agua_abastecimento !!}';
                v_SupplyData = v_SupplyData.length > 0 ? JSON.parse(v_SupplyData) : [];
                var v_SelectedItems = [];
                $(v_SupplyData).each(function(c_Index){
                    var v_Fields = getFieldsString('agua_abastecimento', this, c_Index);
                    $('#agua_abastecimento_hidden').before(v_Fields);
                    v_SelectedItems.push(this.id);
                });
                $('#agua_abastecimento').select2("val", v_SelectedItems);

                v_SupplyData = '{!! $p_CityInfo->esgotamento !!}';
                v_SupplyData = v_SupplyData.length > 0 ? JSON.parse(v_SupplyData) : [];
                var v_SelectedItems = [];
                $(v_SupplyData).each(function(c_Index){
                    var v_Fields = getFieldsString('esgotamento', this, c_Index);
                    $('#esgotamento_hidden').before(v_Fields);
                    v_SelectedItems.push(this.id);
                });
                $('#esgotamento').select2("val", v_SelectedItems);


                v_SupplyData = '{!! $p_CityInfo->energia_abastecimento !!}';
                v_SupplyData = v_SupplyData.length > 0 ? JSON.parse(v_SupplyData) : [];
                var v_SelectedItems = [];
                $(v_SupplyData).each(function(c_Index){
                    var v_Fields = getFieldsString('energia_abastecimento', this, c_Index);
                    $('#energia_abastecimento_hidden').before(v_Fields);
                    v_SelectedItems.push(this.id);
                });
                $('#energia_abastecimento').select2("val", v_SelectedItems);

                v_SupplyData = '{!! $p_CityInfo->destinacao_lixo !!}';
                v_SupplyData = v_SupplyData.length > 0 ? JSON.parse(v_SupplyData) : [];
                var v_SelectedItems = [];
                $(v_SupplyData).each(function(c_Index){
                    var v_Fields = getFieldsString('destinacao_lixo', this, c_Index);
                    $('#destinacao_lixo_hidden').before(v_Fields);
                    v_SelectedItems.push(this.id);
                });
                $('#destinacao_lixo').select2("val", v_SelectedItems);
            @else
                $('#city_id').change(function(){
                    var v_SelectedCity = $(this).val();
                    var v_LastVal = $('#municipios_limitrofes').val();
                    var v_DataString = '';
                    $.each(v_AllCities, function (c_Key, c_Field)
                    {
                        if(c_Key != v_SelectedCity)
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                    });

                    $('#municipios_limitrofes').html(v_DataString);
                    if(v_AllCities.length == 0)
                        $('#municipios_limitrofes').select2("val", "");
                    else
                        $('#municipios_limitrofes').select2("val", v_LastVal);


                    $.get("{{url('/admin/circuitoTuristicoMunicipio?city_id=')}}" + $(this).val(), function(){
                    }).done(function(data){
                        if (data.error == 'ok')
                            $('#circuito_turistico').val(data.data);
                        else
                            $('#circuito_turistico').val('');
                    }).error(function(){
                    });

                    $.get("{{url('/admin/regiaoMunicipio?city_id=')}}" + $(this).val(), function(){
                    }).done(function(data){
                        if (data.error == 'ok')
                            $('#regiao').val(data.data);
                        else
                            $('#regiao').val('');
                    }).error(function(){
                    });
                }).change();
            @endif

            $('#agua_abastecimento').change(function(){
                var v_Data = updateJSONData('agua_abastecimento');
                $('.agua_abastecimento').remove();
                var v_Selection = $(this).select2('data');
                $(v_Selection).each(function(c_Index){
                    var v_Supply = findObjectById(v_Data, this.id);
                    if(v_Supply == undefined)
                        v_Supply = {id:this.id, nome:this.text, empresa:'', percentual_atendimento:''};
                    var v_Fields = getFieldsString('agua_abastecimento', v_Supply, c_Index);
                    $('#agua_abastecimento_hidden').before(v_Fields);
                });
            });

            $('#esgotamento').change(function(){
                var v_Data = updateJSONData('esgotamento');
                $('.esgotamento').remove();
                var v_Selection = $(this).select2('data');
                $(v_Selection).each(function(c_Index){
                    var v_Supply = findObjectById(v_Data, this.id);
                    if(v_Supply == undefined)
                        v_Supply = {id:this.id, nome:this.text, empresa:'', percentual_atendimento:''};
                    var v_Fields = getFieldsString('esgotamento', v_Supply, c_Index);
                    $('#esgotamento_hidden').before(v_Fields);
                });
            });

            $('#energia_abastecimento').change(function(){
                var v_Data = updateJSONData('energia_abastecimento');
                $('.energia_abastecimento').remove();
                var v_Selection = $(this).select2('data');
                $(v_Selection).each(function(c_Index){
                    var v_Supply = findObjectById(v_Data, this.id);
                    if(v_Supply == undefined)
                        v_Supply = {id:this.id, nome:this.text, empresa:'', percentual_atendimento:''};
                    var v_Fields = getFieldsString('energia_abastecimento', v_Supply, c_Index);
                    $('#energia_abastecimento_hidden').before(v_Fields);
                });
            });

            $('#destinacao_lixo').change(function(){
                var v_Data = updateJSONData('destinacao_lixo');
                $('.destinacao_lixo').remove();
                var v_Selection = $(this).select2('data');
                $(v_Selection).each(function(c_Index){
                    var v_Supply = findObjectById(v_Data, this.id);
                    if(v_Supply == undefined)
                        v_Supply = {id:this.id, nome:this.text, empresa:'', percentual_atendimento:'', reciclagem: ''};
                    var v_Fields = getFieldsString('destinacao_lixo', v_Supply, c_Index);
                    $('#destinacao_lixo_hidden').before(v_Fields);
                });
            });

            $('#latitude_longitude_decimal').keyup(function(){
                var v_Value = this.value;
                if(v_Value.length){
                    v_Value = v_Value.split(',');
                    var v_Latitude = v_Value[0].trim();
                    var v_Longitude = v_Value[1].trim();
                    if(v_Latitude.length && v_Longitude.length){
                        $('#latitude').val(v_Latitude);
                        $('#longitude').val(v_Longitude).change();
                    }
                }
            });

            $('#latitude, #longitude').focus(function() {
                $(this).on('mousewheel.disableScroll', function(e) {
                    e.preventDefault();
                })
            }).blur(function() {
                $(this).off('mousewheel.disableScroll');
            }).keydown(function(e) {
                if (e.keyCode === 38 || e.keyCode === 40)
                    e.preventDefault();
            });
        });

        function submitForm()
        {
            var v_Cities = [];
            $('#municipios_limitrofes option:selected').each(function(){
                v_Cities.push({id: $(this).attr('value'), nome: $(this).text()});
            });

            $('#municipios_limitrofes_hidden').val(JSON.stringify(v_Cities));


            updateJSONData('agua_abastecimento');
            updateJSONData('esgotamento');
            updateJSONData('energia_abastecimento');
            updateJSONData('destinacao_lixo');

            var v_Climates = [];
            $('#clima_select option:selected').each(function(){
                v_Climates.push({id:$(this).attr('value'), nome:$(this).text()});
            });
            $('#clima').val(JSON.stringify(v_Climates));

            var v_Activities = [];
            $('#principais_atividades_economicas option:selected').each(function(){
                v_Activities.push({id: $(this).attr('value'), nome: $(this).text()});
            });
            $('#principais_atividades_economicas_hidden').val(JSON.stringify(v_Activities));
            var v_Actions = [];
            $('#orgao_turismo_acoes option:selected').each(function(){
                v_Actions.push({id: $(this).attr('value'), nome: $(this).text()});
            });
            $('#orgao_turismo_acoes_hidden').val(JSON.stringify(v_Actions));

            processaDadosEventosPermanentes();

            return true;
        }

        function updateJSONData(p_Type)
        {
            var v_SupplyDataArray = [];
            $('.' + p_Type).each(function(){
                var v_SupplyData = {};
                v_SupplyData.id = $(this).find('.supply-id').val();
                v_SupplyData.nome = $(this).find('.supply-name').val();
                v_SupplyData.empresa = $(this).find('.supply-empresa').val();
                v_SupplyData.percentual_atendimento = $(this).find('.supply-percentual-atendimento').val();
                if(p_Type == 'destinacao_lixo')
                    v_SupplyData.reciclagem = $(this).find('.supply-reciclagem').val();
                v_SupplyDataArray.push(v_SupplyData);
            });
            $('#' + p_Type + '_hidden').val(JSON.stringify(v_SupplyDataArray));
            return v_SupplyDataArray;
        }

        function getFieldsString(p_Type, p_Supply, p_Index){
            var v_ParentIndex = $('#' + p_Type + '_index').text().split('. ')[0];
            var v_BaseIndex = v_ParentIndex + '.' + (p_Index + 1) + '.';
            var v_String = '<div class="' + p_Type + '">' +
                    '<div class="col-sm-12 supply-title">' +
                        '<h5>' + v_BaseIndex + ' Tipo - ' + p_Supply.nome + '</h5>' +
                    '</div>' +
                    '<input type="hidden" class="supply-id" value="' + p_Supply.id + '">' +
                    '<input type="hidden" class="supply-name" value="' + p_Supply.nome + '">' +
                    '<div class="form-group col-sm-6">'+
                        '<label>' + v_BaseIndex + '1. Empresa responsável</label>'+
                        '<input type="text" class="form-control supply-empresa" placeholder="Digite Aqui" value="' + p_Supply.empresa + '">'+
                    '</div>'+
                    '<div class="form-group col-sm-6">'+
                        '<label>' + v_BaseIndex + '2. Percentual de domicílios atendidos</label>'+
                        '<input type="number" step="0.01" min="0" max="100" class="form-control supply-percentual-atendimento" placeholder="Digite Aqui" value="' + p_Supply.percentual_atendimento + '">'+
                    '</div>';
            if(p_Type == 'destinacao_lixo')
            {
                v_String += '<div class="form-group col-sm-6">'+
                        '<label>' + v_BaseIndex + '3. Existe tratamento de reciclagem para os resíduos?</label>'+
                        '<select class="form-control supply-reciclagem" required="required">' +
                        '<option value="" ' + (p_Supply.reciclagem == '' ? 'selected' : '') + '></option>' +
                        '<option value="Não" ' + (p_Supply.reciclagem == 'Não' ? 'selected' : '') + '>Não</option>' +
                        '<option value="Sim" ' + (p_Supply.reciclagem == 'Sim' ? 'selected' : '') + '>Sim</option>' +
                        '</select>' +
                        '</div>';
            }
            v_String += '</div>';
            return v_String;
        }

        function findObjectById(p_Array, p_Id)
        {
            return $.grep(p_Array, function(e){ return e.id == p_Id; })[0];
        }

        Number.prototype.formatMoney = function(c, d, t){
            var n = this,
                    c = isNaN(c = Math.abs(c)) ? 2 : c,
                    d = d == undefined ? "." : d,
                    t = t == undefined ? "," : t,
                    s = n < 0 ? "-" : "",
                    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                    j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    </script>
    @include('admin.util.mapScript')
    @include('admin.util.responsibleTeamScript') <!--Equipe responsável pelo preenchimento de dados-->
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'A1'])
@stop