<script type="text/javascript" src="{{url('/assets/js/jquery.popconfirm.js')}}"></script>
<script>
    var v_EventCategories = {!! json_encode($p_EventCategories) !!};
    $(document).ready(function()
    {
        carregaDadosEventosPermanentes();

        $('#evento_permanente_meses').change(function(){
            var v_Field = this;
            var v_Selection = $(v_Field).val();
            if($.inArray('Todos', v_Selection) > -1)
            {
                if($(v_Field).find('option:selected').length == 13)
                    $(v_Field).select2("val", "");
                else
                    $(v_Field).select2("val", ['1','2','3','4','5','6','7','8','9','10','11','12']);
            }
        });

        $('.new-delete-btn').removeClass('new-delete-btn').popConfirm({
            title: "Confirmar exclusão",
            content: "Tem certeza de que deseja excluir esta entrada?",
            placement: "left",
            yesBtn: "Sim",
            noBtn: "Não",
            container: false
        });
    });

    function carregaDadosEventosPermanentes()
    {
        var v_Dados = $('#eventos_permanentes').val();
        if(v_Dados.length > 0)
        {
            v_Dados = JSON.parse(v_Dados);
            $(v_Dados).each(function(){
                this.periodo = JSON.parse(this.periodo);
                this.municipios_participantes = JSON.parse(this.municipios_participantes);
                addEventItem(this, false);
            });
        }
    }

    function processaDadosEventosPermanentes()
    {
        var v_Dados = [];
        $('.eventos-permanentes-fields .field').each(function(){
            v_Dados.push(JSON.parse($(this).val()));
        });
        $('#eventos_permanentes').val(JSON.stringify(v_Dados));
    }

    function editEventItem(p_Button)
    {
        $('#tablePermanentEvent .btn.editing').removeClass('editing');
        $('#permanentEventModal').modal();
        if(p_Button == '')
            $('#permanentEventModal input, #permanentEventModal select, #permanentEventModal textarea').val('').change();
        else {
            $(p_Button).addClass('editing');
            var v_Item = $(p_Button).siblings('.field').val();
            v_Item = JSON.parse(v_Item);
            var v_Meses = (typeof v_Item.periodo) == 'string' ? JSON.parse(v_Item.periodo) : v_Item.periodo;
            var v_Municipios = (typeof v_Item.municipios_participantes) == 'string' ? JSON.parse(v_Item.municipios_participantes) : v_Item.municipios_participantes;

            $('#evento_permanente_nome').val(v_Item.nome);
            $('#evento_permanente_descricao').val(v_Item.descricao);
            $('#evento_permanente_periodicidade').val(v_Item.periodicidade);
            $('#evento_permanente_meses').val(v_Meses).change();
            $('#evento_permanente_realizacao').val(v_Item.realizacao);
            $('#evento_permanente_entidade_responsavel').val(v_Item.responsavel_realizacao);
            $('#evento_permanente_tema').val(v_Item.tema);
            $('#evento_permanente_tipo').val(v_Item.tipo);
            $('#evento_permanente_municipios_participantes').val(v_Municipios).change();
            $('#evento_permanente_observacao').val(v_Item.observacao);
        }
    }

    function submitEventItem()
    {
        if($('#permanentEventModalForm')[0].checkValidity()){
            var v_Item = {
                nome:$('#evento_permanente_nome').val(),
                descricao:$('#evento_permanente_descricao').val(),
                periodicidade:$('#evento_permanente_periodicidade').val(),
                periodo:$('#evento_permanente_meses').val(),
                realizacao:$('#evento_permanente_realizacao').val(),
                responsavel_realizacao:$('#evento_permanente_entidade_responsavel').val(),
                tema:$('#evento_permanente_tema').val(),
                tipo:$('#evento_permanente_tipo').val(),
                municipios_participantes:$('#evento_permanente_municipios_participantes').val(),
                observacao:$('#evento_permanente_observacao').val()
            };

            addEventItem(v_Item, ($('#tablePermanentEvent .btn.editing').length > 0));
            $('#permanentEventModal').modal('hide');
        }
        return false;
    }

    function addEventItem(p_Item, p_IsEditing)
    {
        var v_Category = p_Item.tipo ? v_EventCategories[p_Item.tipo] : '';
        var v_TableHTML =
                '<tr class="item-line new-line">' +
                    '<td>' + p_Item.nome + '</td>' +
                    '<td class="event-description">' + p_Item.descricao + '</td>' +
                    '<td>' + p_Item.periodicidade + '</td>' +
                    '<td>' + p_Item.realizacao + '</td>' +
                    '<td>' + p_Item.tema + '</td>' +
                    '<td>' + v_Category + '</td>' +
                    '<td class="table-actions">' +
                        '<input name="evento_permanente[]" type="hidden" class="field">' +
                        '<button type="button" title="Editar" class="btn btn-success" onclick="editEventItem(this)"><i class="fa fa-edit"></i></button>' +
                        '<button type="button" title="Excluir" class="btn btn-success delete-btn new-delete-btn" onclick="$(this).closest(\'tr\').remove()"><i class="fa fa-trash-o"></i></button>' +
                    '</td>' +
                '</tr>';

        if(p_IsEditing){
            var $v_TR = $('#tablePermanentEvent .btn.editing').closest('tr');
            $v_TR.replaceWith(v_TableHTML)
        }
        else
            $('#tablePermanentEventNewItem').before(v_TableHTML);

        $('#tablePermanentEvent .item-line.new-line .field').val(JSON.stringify(p_Item));
        $('#tablePermanentEvent .item-line.new-line').removeClass('new-line');

        $('.new-delete-btn').removeClass('new-delete-btn').popConfirm({
            title: "Confirmar exclusão",
            content: "Tem certeza de que deseja excluir esta entrada?",
            placement: "left",
            yesBtn: "Sim",
            noBtn: "Não",
            container: false
        });
    }
</script>