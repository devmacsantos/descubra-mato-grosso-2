<!-- Modal -->
<div class="modal fade" id="permanentEventModal" tabindex="-1" role="dialog" aria-labelledby="permanentEventModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="permanentEventModalLabel">Evento permanente</h4>
            </div>
            {!! Form::open(['id' => 'permanentEventModalForm', 'onsubmit' => 'return submitEventItem()']) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <label for="evento_permanente_nome">Nome do evento<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control" id="evento_permanente_nome" placeholder="Digite Aqui" maxlength="150" required>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="evento_permanente_descricao">Breve descrição do evento<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control" id="evento_permanente_descricao" placeholder="Digite Aqui" maxlength="300" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <?php
                                $v_PeriodOptions = [
                                    ''=>'',
                                    'Mensal'=>'Mensal',
                                    'Bimestral'=>'Bimestral',
                                    'Trimestral'=>'Trimestral',
                                    'Quadrimestral'=>'Quadrimestral',
                                    'Semestral'=>'Semestral',
                                    'Anual'=>'Anual'
                                ];
                            ?>
                            <label for="evento_permanente_periodicidade">Periodicidade<span class="mandatory-field">*</span></label>
                            {!! Form::select('', $v_PeriodOptions, null, ['id' => 'evento_permanente_periodicidade', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="evento_permanente_meses">Período que ocorre <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                            {!! Form::select('', \App\Http\Controllers\BaseController::$m_Months, [], ['id' => 'evento_permanente_meses', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <?php
                                $v_RealizationOptions = [
                                    ''=>'',
                                    'Público'=>'Público',
                                    'Privado'=>'Privado',
                                    'PPP'=>'PPP'
                                ];
                            ?>
                            <label for="evento_permanente_realizacao">Realização<span class="mandatory-field">*</span></label>
                            {!! Form::select('', $v_RealizationOptions, null, ['id' => 'evento_permanente_realizacao', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="evento_permanente_entidade_responsavel">Nome da entidade responsável pela realização<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control" id="evento_permanente_entidade_responsavel" placeholder="Digite Aqui" maxlength="150" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="evento_permanente_tema">Tema<span class="mandatory-field">*</span></label>
                            <input type="text" class="form-control" id="evento_permanente_tema" placeholder="Digite Aqui" maxlength="100" required>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="evento_permanente_tipo">Tipo de evento<span class="mandatory-field">*</span></label>
                            {!! Form::select('', $p_EventCategories, null, ['id' => 'evento_permanente_tipo', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="evento_permanente_municipios_participantes">Municípios participantes <i>(permite mais de uma opção)</i><span class="mandatory-field">*</span></label>
                            {!! Form::select('', $p_AllCities, null, ['id' => 'evento_permanente_municipios_participantes', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%', 'multiple' => 'multiple']) !!}
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="evento_permanente_observacao">Observação</label>
                            <textarea id="evento_permanente_observacao" class="form-control" rows="3" placeholder="Digite Aqui"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary" id="submitBtn">Continuar</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


