<div class="form-group col-sm-12">
    <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Eventos Permanentes</h3>
</div>
<input type="hidden" id="eventos_permanentes" name="eventos_permanentes" value="{{json_encode($p_Events)}}">
<div class="eventos-permanentes-fields col-sm-12">
    <table class="table table-striped" id="tablePermanentEvent">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Breve descrição</th>
                <th>Periodicidade</th>
                <th>Realização</th>
                <th>Tema</th>
                <th>Tipo de evento</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr id="tablePermanentEventNewItem" style="text-align: center">
                <td colspan="9">
                    <button type="button" class="btn btn-success" onclick="editEventItem('')">
                        Adicionar evento
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>