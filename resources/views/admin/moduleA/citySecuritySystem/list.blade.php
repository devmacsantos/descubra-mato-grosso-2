@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
@stop
@section('panel-header')
    A4 - Sistemas de segurança
    @if(!\App\UserType::isParceiro())
    <a href="{{ url('admin/inventario/sistemas-seguranca/editar') }}">
        <button class="btn btn-success pull-right" title="Nova entrada">
            <i class="fa fa-plus"></i>
        </button>
    </a>
    @endif
@stop
@section('list-table-head')
    <tr>
        <th>Nome</th>
        <th>Município</th>
        <th>Data de cadastro</th>
        <th>Atualizado em</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td>{!! Form::select('', ['' => 'Selecione'] + $p_Status, null, ['class' => 'form-control']) !!}</td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/inventario/sistemas-seguranca')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 0, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop