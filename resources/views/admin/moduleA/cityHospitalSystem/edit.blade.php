@extends('admin.main')
@section('pageCSS')
    <link href="{{url('/vendor/select2/select2.min.css')}}" rel="stylesheet" />
    <style type="text/css">
        .align-center
        {
            text-align: center;
        }
        .margin-auto
        {
            margin: auto;
        }
        .panel-title > a.btn-success{
            color: white!important;
        }
    </style>
@stop
@section('panel-header')
    A5 - Sistema hospitalar
    <a title="Imprimir" type="button" class="btn btn-success pull-right" onclick="print()">
        <i class="fa fa-print"></i>
    </a>
@stop
@section('content')
    <?php \App\BaseInventoryModel::startFormFieldIndexing(); ?>
    <?php
        $v_CareTypes = ['SUS' => 'SUS', 'Planos particulares' => 'Planos particulares', 'Atendimento particular' => 'Atendimento particular'];
    ?>
    <div class="row">
        <div class="mb20 col-sm-12 visible-print">
            <h2>A5 - Sistema hospitalar</h2>
        </div>
        @if(!\App\UserType::isParceiro())
        {!! Form::open(['id' => 'mainForm', 'url'=> url('/admin/inventario/sistemas-hospitalares'), 'onsubmit' => 'return submitForm()']) !!}
        @else
        <div id="mainForm">
        @endif


        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Identificação</h3>
        </div>
        @if($p_HospitalSystem != null)
            <input type="hidden" name="id" value="{{$p_HospitalSystem->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="city_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Município<span class="mandatory-field">*</span></label>
            <?php $v_CityOptions = count($p_Cities) == 1 ? $p_Cities : ([''=>''] + $p_Cities); ?>
            {!! Form::select('formulario[city_id]', $v_CityOptions, $p_HospitalSystem == null ? '' : $p_HospitalSystem->city_id, ['id' => 'city_id', 'class' => 'form-control select2', 'style' => 'width: 100%', 'required' => 'required']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="district_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Distrito</label>
            {!! Form::select('formulario[district_id]', [($p_HospitalSystem == null ? '' : $p_HospitalSystem->district_id) => ''], $p_HospitalSystem == null ? '' : $p_HospitalSystem->district_id, ['id' => 'district_id', 'class' => 'form-control select2', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-6">
            <label for="type_id">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipo<span class="mandatory-field">*</span></label>
            {!! Form::select('formulario[type_id]', $p_Types, $p_HospitalSystem == null ? '' : $p_HospitalSystem->type_id, ['id' => 'type_id', 'class' => 'form-control select2', 'required' => 'required', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Nome/Entidade</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="cnpj">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CNPJ</label>
            <input type="text" class="form-control cnpj-field" id="cnpj" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->cnpj}}">
            <input type="hidden" name="formulario[cnpj]" value="">
        </div>
        <div class="form-group col-sm-6">
            <label for="nome_fantasia">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome fantasia/comercial<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[nome_fantasia]" class="form-control" id="nome_fantasia" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->nome_fantasia}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome_juridico">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Nome jurídico</label>
            <input type="text" name="formulario[nome_juridico]" class="form-control" id="nome_juridico" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->nome_juridico}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="telefone">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Telefone (exemplo: (DDD) 9 9999-9999 / (DDD) 9999-9999)<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[telefone]" class="form-control phone-field" id="telefone" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->telefone}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="site">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Site</label>
            <input type="url" name="formulario[site]" class="form-control" id="site" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->site}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="email">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Email</label>
            <input type="text" name="formulario[email]" class="email-field form-control" id="email" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->email}}">
        </div>


        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Localização e ambiência</h3>
        </div>
        <div class="form-group col-sm-6">
            <label for="cep">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}CEP<span class="mandatory-field">*</span></label>
            <input name="formulario[cep]" type="text" class="form-control cep-field" id="cep" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->cep}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="bairro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Bairro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[bairro]" class="form-control" id="bairro" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->bairro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="logradouro">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Logradouro<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[logradouro]" class="form-control" id="logradouro" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->logradouro}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="numero">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Número<span class="mandatory-field">*</span></label>
            <input type="text" name="formulario[numero]" class="form-control" id="numero" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->numero}}" required>
        </div>
        <div class="form-group col-sm-12">
            <label for="complemento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Complemento</label>
            <input type="text" name="formulario[complemento]" class="form-control geolocalizacao" id="complemento" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->complemento}}">
            <a title="Pesquisar geolocalização" type="button" class="btn btn-success btn-search-geolocation" onclick="updateMarkerPosition(1)">
                <i class="fa fa-map-marker"></i>
            </a>
        </div>
        <div class="form-group col-sm-12">
            <p>* Movimente o marcador no mapa para ajustar a latitude e longitude.</p>
            <div id="map_canvas" style="height:320px;"></div>
        </div>
        <div class="form-group col-sm-6">
            <label for="latitude" >{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[latitude]" step="0.0000001" class="form-control" id="latitude" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->latitude}}">
        </div>
        <div class="form-group col-sm-6">
            <label for="longitude">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Longitude <i>(formato decimal)</i></label>
            <input type="number" name="formulario[longitude]" step="0.0000001" class="form-control" id="longitude" placeholder="Digite Aqui" value="{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->longitude}}">
        </div>
        <div class="form-group col-sm-12">
            <label for="latitude_longitude_decimal">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Latitude e Longitude em decimal</label>
            <input type="text" class="form-control" id="latitude_longitude_decimal" placeholder="Digite Aqui">
        </div>
        
        
        @include('admin.util.workingPeriod', ['p_Form' => $p_HospitalSystem])

        <div class="form-group col-sm-12">
            <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Dados Complementares</h3>
        </div>
        <div class="form-group col-sm-12">
            <label for="tipo_atendimento">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Tipos de atendimento <i>(permite mais de uma opção)</i></label>
            <?php $v_Types = $p_HospitalSystem == null ? [] : explode(';', $p_HospitalSystem->tipo_atendimento); ?>
            {!! Form::select('tipo_atendimento[]', $v_CareTypes, $v_Types, ['id' => 'tipo_atendimento', 'class' => 'form-control select2', 'multiple' => 'multiple', 'style' => 'width: 100%']) !!}
        </div>
        <div class="form-group col-sm-12">
            <label for="servicos_prestados">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Serviços prestados</label>
            <textarea name="formulario[servicos_prestados]" id="servicos_prestados" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->servicos_prestados}}</textarea>
        </div>
        <div class="form-group col-sm-12">
            <label for="informacoes_observacoes_complementares">{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement(1)}}Descrições e informações complementares</label>
            <textarea name="formulario[informacoes_observacoes_complementares]" id="informacoes_observacoes_complementares" class="form-control" rows="4" placeholder="Digite Aqui">{{$p_HospitalSystem == null ? '' : $p_HospitalSystem->informacoes_observacoes_complementares}}</textarea>
        </div>

        @include('admin.util.accessibility', ['p_Form' => $p_HospitalSystem])

        <?php
            $v_ShowInternalUsage = false;
            if($p_HospitalSystem != null){
                $v_RevisionStatus = $p_HospitalSystem->revision_status_id;
                $v_ShowInternalUsage = ($v_RevisionStatus == 5
                                        || (\App\UserType::isMunicipio() && $v_RevisionStatus == 1)
                                        || (\App\UserType::isCircuito() && $v_RevisionStatus != 4)
                                        || ((\App\UserType::isMaster() || \App\UserType::isAdmin()) && $v_RevisionStatus != 4));
            }
        ?>
        @if($v_ShowInternalUsage)
            <div class="form-group col-sm-12">
                <h3>{{\App\BaseInventoryModel::getFormFieldIndexAndIncrement()}}Uso interno da SEDEC/TUR</h3>
            </div>
        @endif
        @include('admin.util.revision', ['p_Form' => $p_HospitalSystem])

        @include('admin.util.responsibleTeam', ['p_Form' => $p_HospitalSystem])

        @if(!\App\UserType::isParceiro())
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
        @else
        </div>
        @endif
    </div>
@stop
@section('pageScript')
    <script src="{{url('/vendor/select2/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/vendor/select2/i18n/pt-BR.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/inputmask.js')}}" type="text/javascript"></script>
    <script src="{{url('/assets/js/jquery.inputmask.js')}}" type="text/javascript"></script>
    @include('admin.util.workingPeriodScript')
    <script>
        $(document).ready(function()
        {
            $(".select2").select2({language:'pt-BR'});

            $('.cep-field').mask('99.999-999');
            $('.phone-field').keypress(function (e) {
                var key = e.keyCode || e.charCode;
                if( key == 8 || key == 37 || key == 39 || key == 46 )
                    return true;

                var regex = new RegExp(/^[\d\s\(\)\-\+\.\,\;\\\/]+$/);
                var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
                if (regex.test(str))
                    return true;

                e.preventDefault();
                return false;
            });
            $('.cnpj-field').mask('99.999.999/9999-99');
            $('input[type="url"]').focus(function(){
                if(this.value == '')
                    this.value = 'http://';
            }).focusout(function(){
                if(this.value == 'http://')
                    this.value = '';
            });

            $('#city_id').change(function(){
                $.get("{{url('/admin/distritosMunicipio?city_id=')}}" + $(this).val(), function(){
                }).done(function(data){
                    if (data.error == 'ok')
                    {
                        var v_LastVal = $('#district_id').val();
                        var v_DataString = '';
                        $.each(data.data, function (c_Key, c_Field)
                        {
                            v_DataString += '<option value="' + c_Key + '">' + c_Field + '</option>';
                        });

                        $('#district_id').html('<option value=""></option>' + v_DataString);
                        if(data.data.length == 0)
                            $('#district_id').select2("val", "");
                        else
                            $('#district_id').select2("val", v_LastVal);
                    }
                    else
                        $('#sub_type_id').html('<option value=""></option>').select2("val", "");
                }).error(function(){
                });
            }).change();

            $('#latitude_longitude_decimal').keyup(function(){
                var v_Value = this.value;
                if(v_Value.length){
                    v_Value = v_Value.split(',');
                    var v_Latitude = v_Value[0].trim();
                    var v_Longitude = v_Value[1].trim();
                    if(v_Latitude.length && v_Longitude.length){
                        $('#latitude').val(v_Latitude);
                        $('#longitude').val(v_Longitude).change();
                    }
                }
            });

            $('#latitude, #longitude').focus(function() {
                $(this).on('mousewheel.disableScroll', function(e) {
                    e.preventDefault();
                })
            }).blur(function() {
                $(this).off('mousewheel.disableScroll');
            }).keydown(function(e) {
                if (e.keyCode === 38 || e.keyCode === 40)
                    e.preventDefault();
            });
        });

        function validateCNPJ(p_Value)
        {
            var p_Cnpj = p_Value.replace(/[^\d]+/g,'');
            if(p_Cnpj == '')
                return true; //not mandatory

            if (p_Cnpj.length != 14)
                return false;

            if (p_Cnpj == "00000000000000" || p_Cnpj == "11111111111111" || p_Cnpj == "22222222222222" || p_Cnpj == "33333333333333" || p_Cnpj == "44444444444444" ||
                    p_Cnpj == "55555555555555" || p_Cnpj == "66666666666666" || p_Cnpj == "77777777777777" || p_Cnpj == "88888888888888" || p_Cnpj == "99999999999999")
                return false;

            var v_Size = p_Cnpj.length - 2;
            var v_Numbers = p_Cnpj.substring(0,v_Size);
            var v_Digits = p_Cnpj.substring(v_Size);
            var v_Sum = 0;
            var v_Pos = v_Size - 7;
            for (i = v_Size; i >= 1; i--)
            {
                v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
                if (v_Pos < 2)
                    v_Pos = 9;
            }
            var v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
            if (v_Result != v_Digits.charAt(0))
                return false;

            v_Size = v_Size + 1;
            v_Numbers = p_Cnpj.substring(0,v_Size);
            v_Sum = 0;
            v_Pos = v_Size - 7;
            for (i = v_Size; i >= 1; i--)
            {
                v_Sum += v_Numbers.charAt(v_Size - i) * v_Pos--;
                if (v_Pos < 2)
                    v_Pos = 9;
            }
            v_Result = v_Sum % 11 < 2 ? 0 : 11 - v_Sum % 11;
            return v_Result == v_Digits.charAt(1);
        }

        function submitForm()
        {
            if(!validateCNPJ($('.cnpj-field').val()))
            {
                alert('CNPJ inválido!');
                return false;
            }

            $('.cnpj-field').each(function (c_Key, c_Field)
            {
                var v_Value = $(c_Field).val().replace(/[^\d]+/g,'');
                $(c_Field).parent().find('input[type="hidden"]').val(v_Value);
            });

            processaDadosFuncionamentos();

            processaDadosAcessibilidade();

            return true;
        }
    </script>
    @include('admin.util.mapScript')
    @include('admin.util.accessibilityScript')
    @include('admin.util.responsibleTeamScript')
    @include('admin.util.revisionScript')
    @include('admin.util.tooltipScript', ['p_Type' => 'A5'])
@stop