@extends('admin.util.listDT', ['p_HasDateFilter' => true])
@section('list-css')
@stop
@section('panel-header')
    Destinos
    <a href="{{ url('admin/destinos/editar/distrito/') }}">
        <button class="btn btn-success pull-right" title="Novo destino">
            <i class="fa fa-plus"></i> Distrito
        </button>
    </a>
    <a href="{{ url('admin/destinos/editar/municipio/') }}">
        <button class="btn btn-success pull-right mr10" title="Novo destino">
            <i class="fa fa-plus"></i> Município
        </button>
    </a>
@stop
@section('list-table-head')
    <tr>
        <th>Tipo</th>
        <th>Região Turística</th>
        <th>Município</th>
        <th>Distrito</th>
        <th>Atualizado em</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    <tr>
        <td>{!! Form::select('', ['' => 'Selecione', 'Distrito' => 'Distrito', 'Município' => 'Município'], null, ['class' => 'form-control']) !!}</td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input type="text" placeholder="Buscar" class="form-control" style="font-weight:normal"/></td>
        <td><input class="form-control dateInput" type="text" placeholder="Buscar"></td>
        <td>{!! Form::select('', ['' => 'Selecione', 1 => 'Ativo', 0 => 'Desativado'], null, ['class' => 'form-control']) !!}</td>
        <td></td>
    </tr>
@stop
@section('list-table-dt-url')
    url: "{{ url('/admin/dt/destinos')}}"
@stop
@section('list-table-initial-sorting')
    "aaSorting": [[ 1, "asc" ]],
@stop
@section('list-table-sortable-columns')
    {"bSortable": false},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": true},
    {"bSortable": false}
@stop
@section('list-js')
@stop