<!DOCTYPE html>
<html lang="en">
    <head>
        
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=REPLACEME"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'REPLACEME');
</script>

        
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Dashboard do Descubra Mato Grosso (powered by GAFIT - Soluções em Automação e TI)">
        <meta name="author" content="GAFIT - Soluções em Automação e TI">
        <link rel="icon" href="{{url('/favicon.ico')}}"/>

        <title>Descubra Mato Grosso - Dashboard</title>
        <!-- Font CSS (Via CDN) -->
        <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

        <!-- Theme CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('/assets/skin/default_skin/css/theme.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('/assets/fonts/icomoon/icomoon.css')}}">
        {{--<!-- Bootstrao CSS -->--}}
        {{--<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap/bootstrap.min.css')}}">--}}
        <!-- Admin Forms CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('/assets/admin-tools/admin-forms/css/admin-forms.css')}}">

        <link rel="stylesheet" type="text/css" href="{{url('/assets/css/admin-style.css')}}">

        <!-- Tooltipster CSS -->
        <link rel="stylesheet" type="text/css" href="{{url('/vendor/plugins/tooltipster/tooltipster.bundle.min.css')}}">
        <style>
            #latitude::-webkit-outer-spin-button,
            #longitude::-webkit-outer-spin-button,
            #latitude::-webkit-inner-spin-button,
            #longitude::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }
            .form-control.geolocalizacao#complemento {
                width: calc(100% - 38px);
                display:inline-block;
            }
            .btn-search-geolocation{
                padding:8px 12px;
            }
        </style>
        @yield('pageCSS')

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <div id="main">
        <!-- Start: Header -->
        <header class="navbar navbar-fixed-top bg-light">
            <div class="navbar-branding">
                <a class="navbar-brand" href="{{url('/')}}" style="width:73%;">
                    <img style="width: 100%;padding-left: 5px;" title="Home" alt="Descubra Mato Grosso"  src="{{url('/assets/img/descubraMatoGrosso.png')}}">
                </a>
                
                <span id="toggle_sidemenu_l" class="fa fa-bars"></span>
                
            </div>

            <ul class="nav navbar-nav navbar-right">
                @if (App::environment('local')) <li><h3 class="text-center text-danger"></h3> </li>@endif 
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span>{{ Auth::User()['name'] }}</span>
                        <span class="caret caret-tp"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-persist pn w250 bg-white" role="menu">
                        <li class="br-t of-h">
                            <a href="{{url('/admin/editar-perfil')}}">
                                Editar perfil
                            </a>
                        </li>
                        <li class="br-t of-h">
                            {!! \HTML::link('/admin/logout', 'Sair') !!}
                        </li>
                    </ul>
                </li>
            </ul>
        </header>

        <!-- Start: Sidebar -->
        <aside id="sidebar_left" class="nano nano-primary affix">
            <div class="nano-content">
                <!-- sidebar menu -->
                <ul class="nav sidebar-menu">
                    <li class="sidebar-label pt20">Menu</li>
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isMunicipio() || \App\UserType::isCircuito() || \App\UserType::isTrade())
                    <li>
                        <a href="{{url('/admin/painel-geral')}}">
                            <span class="fa fa-home"></span>
                            <span class="sidebar-title">Painel geral</span>
                        </a>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isComunicacao())
                    <li>
                        <a href="{{url('/admin/destinos')}}">
                            <span class="fa fa-map-marker"></span>
                            <span class="sidebar-title">Destinos</span>
                        </a>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isComunicacao() || \App\UserType::isParceiro())
                    <li>
                        <a href="{{url('/admin/circuitos')}}">
                            <span class="fa fa-road"></span>
                            <span class="sidebar-title">Regiões Turísticas</span>
                        </a>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isComunicacao() || \App\UserType::isMunicipio() || \App\UserType::isCircuito() || \App\UserType::isFiscalizador() || \App\UserType::isParceiro())
                    <li>
                        <a class="accordion-toggle" href="#">
                            <span class="fa fa-archive"></span>
                            <span class="sidebar-title">Inventário</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li>
                                <a href="{{url('/admin/inventario/pesquisar')}}">
                                    <span class="menu-inventory-name">Pesquisar</span>
                                </a>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Módulo A</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/inventario/informacoes')}}">
                                            <span class="menu-inventory-name">A1 - Informações básicas do município</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/meios-acesso/geral')}}">
                                            <span class="menu-inventory-name">A2.1 - Meios de acesso ao município - Geral</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/meios-acesso')}}">
                                            <span class="menu-inventory-name">A2.2 - Meios de acesso ao município</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/sistemas-seguranca')}}">
                                            <span class="menu-inventory-name">A4 - Sistemas de segurança</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/sistemas-hospitalares')}}">
                                            <span class="menu-inventory-name">A5 - Sistemas hospitalares</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/outros-servicos')}}">
                                            <span class="menu-inventory-name">A7 - Outros serviços</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Módulo B</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/inventario/servicos-hospedagem')}}">
                                            <span class="menu-inventory-name">B1 - Serviços e equipamentos de hospedagem</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/servicos-alimentos')}}">
                                            <span class="menu-inventory-name">B2 - Serviços e equipamentos de alimentos e bebidas</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/servicos-agencias-turismo')}}">
                                            <span class="menu-inventory-name">B3 - Serviços e equipamentos de agências de turismo</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/servicos-transporte-turistico')}}">
                                            <span class="menu-inventory-name">B4 - Serviços e equipamentos de transporte turístico</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/servicos-eventos')}}">
                                            <span class="menu-inventory-name">B5 - Serviços e equipamentos de eventos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/servicos-lazer')}}">
                                            <span class="menu-inventory-name">B6 - Serviços e equipamentos de lazer</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/outros-servicos-turisticos')}}">
                                            <span class="menu-inventory-name">B7 - Outros serviços e equipamentos turísticos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Módulo C</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/inventario/atrativos-naturais')}}">
                                            <span class="menu-inventory-name">C1 - Atrativos naturais</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/atrativos-culturais')}}">
                                            <span class="menu-inventory-name">C2 - Atrativos culturais</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/atrativos-atividades-economicas')}}">
                                            <span class="menu-inventory-name">C3 - Atividades econômicas/produção associada ao turismo</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/realizacoes-contemporaneas')}}">
                                            <span class="menu-inventory-name">C4 - Realizações técnicas e científicas contemporâneas</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/produto-primario')}}">
                                            <span class="menu-inventory-name">C6.1 - Gastronomia - Produto primário</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/produto-transformado')}}">
                                            <span class="menu-inventory-name">C6.2 - Gastronomia - Produto transformado</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/inventario/prato-tipico')}}">
                                            <span class="menu-inventory-name">C6.3 - Gastronomia - Prato típico</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isMunicipio() || \App\UserType::isCircuito())
                            <li>
                                <a href="{{url('/admin/equipes-responsaveis')}}">
                                    <span>Equipes responsáveis por coleta de dados</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isComunicacao() || \App\UserType::isParceiro())
                    <li>
                        <a href="{{url('/admin/roteiros')}}">
                            <span class="imoon imoon-map2"></span>
                            <span class="sidebar-title">Roteiros</span>
                        </a>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin())
                    <li>
                        <a class="accordion-toggle" href="#">
                            <span class="fa fa-tags"></span>
                            <span class="sidebar-title">O que fazer</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li>
                                <a href="{{url('/admin/categorias-viagem')}}">
                                    <span>Tipo de polo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/tipos-viagem')}}">
                                    <span>Tipo de segmento</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isComunicacao() || \App\UserType::isMunicipio() || \App\UserType::isCircuito() || \App\UserType::isParceiro())
                    <li>
                        <a href="{{url('/admin/eventos')}}">
                            <span class="fa fa-calendar"></span>
                            <span class="sidebar-title">Eventos</span>
                        </a>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isComunicacao())
                    <li>
                        <a href="{{url('/admin/blog')}}">
                            <span class="fa fa-file-text-o"></span>
                            <span class="sidebar-title">Blog</span>
                        </a>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin())
                    <li>
                        <a class="accordion-toggle" href="#">
                            <span class="fa fa-th"></span>
                            <span class="sidebar-title">Propriedades</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Tooltips Módulo A</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/tooltips/A1')}}">
                                            <span class="menu-inventory-name">A1 - Informações básicas do município</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/A2.1')}}">
                                            <span class="menu-inventory-name">A2.1 - Meios de acesso ao município - Geral</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/A2.2')}}">
                                            <span class="menu-inventory-name">A2.2 - Meios de acesso ao município</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/A4')}}">
                                            <span class="menu-inventory-name">A4 - Sistemas de segurança</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/A5')}}">
                                            <span class="menu-inventory-name">A5 - Sistemas hospitalares</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/A7')}}">
                                            <span class="menu-inventory-name">A7 - Outros serviços</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Tooltips Módulo B</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/tooltips/B1')}}">
                                            <span class="menu-inventory-name">B1 - Serviços e equipamentos de hospedagem</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/B2')}}">
                                            <span class="menu-inventory-name">B2 - Serviços e equipamentos de alimentos e bebidas</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/B3')}}">
                                            <span class="menu-inventory-name">B3 - Serviços e equipamentos de agências de turismo</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/B4')}}">
                                            <span class="menu-inventory-name">B4 - Serviços e equipamentos de transporte turístico</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/B5')}}">
                                            <span class="menu-inventory-name">B5 - Serviços e equipamentos de eventos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/B6')}}">
                                            <span class="menu-inventory-name">B6 - Serviços e equipamentos de lazer</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/B7')}}">
                                            <span class="menu-inventory-name">B7 - Outros serviços e equipamentos turísticos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Tooltips Módulo C</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/tooltips/C1')}}">
                                            <span class="menu-inventory-name">C1 - Atrativos naturais</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/C2')}}">
                                            <span class="menu-inventory-name">C2 - Atrativos culturais</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/C3')}}">
                                            <span class="menu-inventory-name">C3 - Atividades econômicas/produção associada ao turismo</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/C4')}}">
                                            <span class="menu-inventory-name">C4 - Realizações técnicas e científicas contemporâneas</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/C6.1')}}">
                                            <span class="menu-inventory-name">C6.1 - Gastronomia - Produto primário</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/C6.2')}}">
                                            <span class="menu-inventory-name">C6.2 - Gastronomia - Produto transformado</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/tooltips/C6.3')}}">
                                            <span class="menu-inventory-name">C6.3 - Gastronomia - Prato típico</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{url('/admin/tooltips/eventos')}}">
                                    <span>Tooltips Eventos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/cadastur/editar')}}">
                                    <span>Cadastur</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/atividades-economicas')}}">
                                    <span>Atividades econômicas</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/municipios')}}">
                                    <span>Municípios</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/climas')}}">
                                    <span>Climas</span>
                                </a>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Telefonia móvel</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/operadoras-telefonicas')}}">
                                            <span>Operadoras</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/servicos-telefonicos')}}">
                                            <span>Serviços</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Tipos de abastecimento</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <?php
                                        $v_Types = [
                                                'agua' => 'Água',
                                                'esgotamento' => 'Esgotamento',
                                                'energia' => 'Energia',
                                                'coleta-lixo' => 'Destinação do lixo',
                                        ];
                                    ?>
                                    @foreach($v_Types as $c_Type => $c_Name)
                                    <li>
                                        <a href="{{url('/admin/servicos-abastecimento/' . $c_Type)}}">
                                            <span>{{$c_Name}}</span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Rodovias</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/rodovias/estadual')}}">
                                            <span>Estadual</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/rodovias/federal')}}">
                                            <span>Federal</span>
                                        </a>
                                    </li>
                                    {{--<li>--}}
                                        {{--<a href="{{url('/admin/rodovias/municipal')}}">--}}
                                            {{--<span>Municipal</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                </ul>
                            </li>
                            <li>
                                <a href="{{url('/admin/eventos/categorias')}}">
                                    <span>Categorias de eventos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/eventos/classificacao')}}">
                                    <span>Classificações de eventos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/partidos')}}">
                                    <span>Partidos políticos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/acoes-turismo')}}">
                                    <span>Ações relativas ao turismo</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/formas-pagamento')}}">
                                    <span>Formas de pagamento</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/tipos')}}">
                                    <span>Inventário - Tipos e subtipos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/regioes')}}">
                                    <span>Regiões</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/paises')}}">
                                    <span>Países</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/idiomas')}}">
                                    <span>Idiomas</span>
                                </a>
                            </li>
                            <!--
                            <li>
                                <a href="{{url('/admin/configurar-calendario-eventos')}}">
                                    <span>Calendário de eventos</span>
                                </a>
                            </li>
                            -->
                            <li>
                                <a href="{{url('/admin/capas-relatorios')}}">
                                    <span>Capas - Calendário de Eventos</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isComunicacao())
                    <li>
                        <a class="accordion-toggle" href="#">
                            <span class="fa fa-files-o"></span>
                            <span class="sidebar-title">Páginas</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            <li>
                                <a href="{{url('/admin/paginas/home/editar')}}">
                                    <span>Home</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/paginas/conheca/editar')}}">
                                    <span>Conheça</span>
                                </a>
                            </li>
                            {{--<li>--}}
                                {{--<a class="accordion-toggle" href="#">--}}
                                    {{--<span>Conheça</span>--}}
                                    {{--<span class="caret"></span>--}}
                                {{--</a>--}}
                                {{--<ul class="nav sub-nav" style="">--}}
                                    {{--<li>--}}
                                        {{--<a href="{{url('/admin/paginas/conheca/editar')}}">--}}
                                            {{--<span>Conheça</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                    {{--<li>--}}
                                        {{--<a href="{{url('/admin/paginas/galeria-videos/editar')}}">--}}
                                            {{--<span>Galeria de vídeos</span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{url('/admin/paginas/descricoes/o-que-fazer/editar')}}">
                                    <span>O que fazer</span>
                                </a>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Para onde ir</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/paginas/descricoes/para-onde-ir/editar')}}">
                                            <span>Para onde ir</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/paginas/descricoes/destinos/editar')}}">
                                            <span>Destinos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/paginas/descricoes/parques/editar')}}">
                                            <span>Parques</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/paginas/descricoes/roteiros/editar')}}">
                                            <span>Roteiros</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{url('/admin/paginas/descricao/explore-o-mapa/editar')}}">
                                    <span>Explore o mapa</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/paginas/planeje-sua-viagem/editar')}}">
                                    <span>Planeje sua viagem</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/paginas/descricoes/doacao-de-midias/editar')}}">
                                    <span>Doação de Mídias</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/paginas/descricoes/eventos/editar')}}">
                                    <span>Eventos</span>
                                </a>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Fale conosco</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/paginas/descricao/fale-conosco/editar')}}">
                                            <span>Fale conosco</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/assuntos-fale-conosco')}}">
                                            <span>Assuntos</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    @endif
                    @if(\App\UserType::isAdmin() || \App\UserType::isMaster() || \App\UserType::isComunicacao())
                        <li>
                            <a href="{{url('/admin/doacoes')}}">
                                <span class="fa fa-cloud-upload"></span>
                                <span class="sidebar-title">Mídias doadas</span>
                            </a>
                        </li>
                    @endif
                    @if(\App\UserType::isAdmin())
                        <li>
                            <a href="{{url('/admin/usuarios')}}">
                                <span class="fa fa-users"></span>
                                <span class="sidebar-title">Usuários</span>
                            </a>
                        </li>
                    @endif

                    <li>
                        <a class="accordion-toggle" href="#">
                            <span class="fa fa-bar-chart-o"></span>
                            <span class="sidebar-title">Relatórios</span>
                            <span class="caret"></span>
                        </a>
                        <ul class="nav sub-nav" style="">
                            @if(!\App\UserType::isTrade())
                            <li>
                                <a href="{{url('/admin/relatorios/circuitos')}}">
                                    <span>Regiões Turísticas</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/relatorios/destinos')}}">
                                    <span>Destinos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/relatorios/eventos')}}">
                                    <span>Eventos</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/admin/relatorios/calendario-eventos')}}">
                                    <span>Calendário de eventos</span>
                                </a>
                            </li>
                            <li>
                                <a class="accordion-toggle" href="#">
                                    <span>Inventário</span>
                                    <span class="caret"></span>
                                </a>
                                <ul class="nav sub-nav" style="">
                                    <li>
                                        <a href="{{url('/admin/relatorios/inventario/modulo-a')}}">
                                            <span class="menu-inventory-name">Módulo A</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/relatorios/inventario/modulo-b')}}">
                                            <span class="menu-inventory-name">Módulo B</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/relatorios/inventario/modulo-c')}}">
                                            <span class="menu-inventory-name">Módulo C</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="{{url('/admin/relatorios/eventos-permanentes')}}">
                                            <span>Eventos permanentes</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/relatorios/inventario/mapa-numerico')}}">
                                            <span>Mapa numérico</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('/admin/relatorios/inventario/planejamento-georreferenciado')}}">
                                            <span>Planejamento georreferenciado</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @else
                            <li>
                                <a href="{{url('/admin/relatorios/inventario/planejamento-georreferenciado')}}">
                                    <span>Planejamento georreferenciado</span>
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>

                    @if(\App\UserType::isTrade())
                        <li>
                            <a href="{{url('/admin/inventario/servicos-hospedagem')}}">
                                <span class="fa fa-h-square"></span>
                                <span class="sidebar-title">Hospedagem</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/admin/inventario/servicos-alimentos')}}">
                                <span class="fa fa-cutlery"></span>
                                <span class="sidebar-title">Gastronomia</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/admin/inventario/servicos-agencias-turismo')}}">
                                <span class="fa fa-plane"></span>
                                <span class="sidebar-title">Agenciamento</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/admin/inventario/servicos-transporte-turistico')}}">
                                <span class="fa fa-bus"></span>
                                <span class="sidebar-title">Transporte</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/admin/inventario/servicos-lazer')}}">
                                <span class="fa fa-futbol-o"></span>
                                <span class="sidebar-title">Lazer e entretenimento</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{url('/admin/eventos')}}">
                                <span class="fa fa-calendar"></span>
                                <span class="sidebar-title">Eventos</span>
                            </a>
                        </li>
                    @endif
                </ul>
                <div class="sidebar-toggle-mini">
                    <a href="#">
                        <span class="fa fa-sign-out"></span>
                    </a>
                </div>
            </div>
        </aside>
        <!--main content start-->
        <section id="content_wrapper">
            <section id="content" class="container-fluid">
                <!-- page start-->
                <div id="error">
                    @include('errors.error-list')
                </div>
                @yield('main-content')
                <!-- page end-->
            </section>
        </section>
    </div>
    <!-- jQuery -->
    <script type="text/javascript" src="{{url('/vendor/jquery/jquery-1.11.1.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/vendor/jquery/jquery_ui/jquery-ui.min.js')}}"></script>

    <!-- Bootstrap -->
    <script type="text/javascript" src="{{url('/assets/js/bootstrap/bootstrap.min.js')}}"></script>

    <!-- Page Plugins via CDN -->
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/globalize/0.1.1/globalize.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.js"></script>

    <!-- Sparklines CDN -->
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery-sparklines/2.1.2/jquery.sparkline.min.js"></script>

    <!-- Holder js  -->
    <script type="text/javascript" src="{{url('/assets/js/bootstrap/holder.min.js')}}"></script>

    <!-- Theme Javascript -->
    <script type="text/javascript" src="{{url('/assets/js/utility/utility.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/main.js')}}"></script>

    <!-- Admin Panels  -->
    <script type="text/javascript" src="{{url('/assets/admin-tools/admin-plugins/admin-panels/json2.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/admin-tools/admin-plugins/admin-panels/jquery.ui.touch-punch.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/admin-tools/admin-plugins/admin-panels/adminpanels.js')}}"></script>

    <!-- Page Javascript -->
    <script type="text/javascript" src="{{url('/assets/js/pages/widgets.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/jquery.maskedinput.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/jquery.maskmoney.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{url('/assets/js/jquery.confirm.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            "use strict";

            // Init Theme Core
            Core.init({
                sbm: "sb-l-c"
            });

            // Init Admin Panels on widgets inside the ".admin-panels" container
            $('.admin-panels').adminpanel({
                grid: '.admin-grid',
                draggable: true,
                mobile: true,
                callback: function() {
                    bootbox.confirm('<h3>A Custom Callback!</h3>', function() {});
                },
                onFinish: function() {
                    $('.admin-panels').addClass('animated fadeIn').removeClass('fade-onLoad');
                    // We also refresh any "in-view" waypoints to ensure
                    // the correct position is being calculated after the
                    // Admin Panels plugin moved everything
                    Waypoint.refreshAll();

                },
                onSave: function() {
                    $(window).trigger('resize');
                }
            });

            $('.integer-field').maskMoney({thousands:'.',precision:0,allowZero:true});
            $('.currency-field').maskMoney({thousands:'.',decimal:',',allowZero:true});
        });
        Date.prototype.getSQLFormat = function()
        {
            var yyyy = this.getFullYear().toString();
            var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
            var dd  = this.getDate().toString();
            return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0] + ' 00:00:00'); // padding
        };

         (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          ga('create', 'REPLACEME', 'auto');
          ga('send', 'pageview');
    </script>
    @yield('pageScript')
    </body>
</html>
