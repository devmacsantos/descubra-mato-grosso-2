@extends('admin.main')
@section('pageCSS')
    <style type="text/css">
        .full-width
        {
            width: 100%;
        }
        .align-center
        {
            text-align: center;
        }

        .margin-auto
        {
            margin: auto;
        }
    </style>
@stop
@section('panel-header')
    {{ $p_Subject == null ? 'Cadastro' : 'Edição' }} de assunto
@stop
@section('content')
    <div class="row pt15">
        {!! Form::open(array('id' => 'mainForm', 'url'=> url('/admin/assuntos-fale-conosco'))) !!}
        @if($p_Subject != null)
            <input type="hidden" name="id" value="{{$p_Subject->id}}">
        @endif
        <div class="form-group col-sm-6">
            <label for="nome">Nome - Português<span class="mandatory-field">*</span></label>
            <input type="text" name="idioma[nome_pt]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Subject == null ? '' : $p_Subject->nome_pt}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">Nome - Inglês<span class="mandatory-field">*</span></label>
            <input type="text" name="idioma[nome_en]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Subject == null ? '' : $p_Subject->nome_en}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">Nome - Espanhol<span class="mandatory-field">*</span></label>
            <input type="text" name="idioma[nome_es]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Subject == null ? '' : $p_Subject->nome_es}}" required>
        </div>
        <div class="form-group col-sm-6">
            <label for="nome">Nome - Francês<span class="mandatory-field">*</span></label>
            <input type="text" name="idioma[nome_fr]" class="form-control" id="nome" placeholder="Digite Aqui" value="{{$p_Subject == null ? '' : $p_Subject->nome_fr}}" required>
        </div>
        <div class="form-group col-sm-12">
            <input type="submit" class="btn btn-default mt15 mb25 pull-right" value="Salvar">
        </div>
        {!! Form::close() !!}
    </div>
@stop
@section('pageScript')
    <script>
        $(document).ready(function()
        {
        });
    </script>
@stop