<?php
    $v_WaitingItems = [];
    $v_WaitingItems['Eventos'] = \App\Event::whereIn('id', \App\UserTradeItem::getUserPendingItems('EV'))
                                                        ->select('nome')->get()->toArray();
    $v_WaitingItems['Hospedagem'] = \App\AccomodationService::whereIn('id', \App\UserTradeItem::getUserPendingItems('B1'))
                                                        ->select('nome_fantasia', 'cnpj')->get()->toArray();
    $v_WaitingItems['Gastronomia'] = \App\FoodDrinksService::whereIn('id', \App\UserTradeItem::getUserPendingItems('B2'))
                                                        ->select('nome_fantasia', 'cnpj')->get()->toArray();
    $v_WaitingItems['Agenciamento'] = \App\TourismAgencyService::whereIn('id', \App\UserTradeItem::getUserPendingItems('B3'))
                                                        ->select('nome_fantasia', 'cnpj')->get()->toArray();
    $v_WaitingItems['Transporte'] = \App\TourismTransportationService::whereIn('id', \App\UserTradeItem::getUserPendingItems('B4'))
                                                        ->select('nome_fantasia', 'cnpj')->get()->toArray();
    $v_WaitingItems['Lazer e entretenimento'] = \App\RecreationService::whereIn('id', \App\UserTradeItem::getUserPendingItems('B6'))
                                                        ->select('nome_fantasia', 'cnpj')->get()->toArray();

    $v_ItemCount = 0;
    foreach($v_WaitingItems as $c_Type => $c_Items)
        $v_ItemCount += count($c_Items);
?>

@if($v_ItemCount)
    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-visible">
                <header class="panel-heading">
                    <div class="panel-title">
                        {{$v_ItemCount}} item(s) aguardando resposta da solicitação de posse
                        <div class="minimize-panel pull-right mr10" title="Minimizar">
                            <i class="fa fa-minus"></i>
                        </div>
                        <div class="maximize-panel pull-right mr10" title="Expandir" style="display: none">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 mt15">
                            <div class="panel panel-visible">
                                <div class="panel-body pn">
                                    <div class="adv-table">
                                        <table class="table table-striped table-bordered table-hover responsive-table" id="responsive-table-expired" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Nome</th>
                                                <th>CNPJ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($v_WaitingItems as $c_Type => $c_Items)
                                                @foreach($c_Items as $c_Item)
                                                    <tr>
                                                        <td>{{$c_Type}}</td>
                                                        <td>{{($c_Type == 'Eventos') ? $c_Item['nome']: $c_Item['nome_fantasia']}}</td>
                                                        <td>{{($c_Type == 'Eventos') ? '00.000.000/0000-00': substr($c_Item['cnpj'], 0, 2) . '.' . substr($c_Item['cnpj'], 2, 3) . '.' . substr($c_Item['cnpj'], 5, 3) . '/' . substr($c_Item['cnpj'], 8, 4) . '-' . substr($c_Item['cnpj'], 12, 2)}}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endif
