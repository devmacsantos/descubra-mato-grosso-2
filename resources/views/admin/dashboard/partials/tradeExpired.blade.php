<?php
    $v_ExpiredItems = [];
    $v_ExpiredItems['Hospedagem'] = [];
    $v_ExpiredItems['Gastronomia'] = [];
    $v_ExpiredItems['Agenciamento'] = [];
    $v_ExpiredItems['Transporte'] = [];
    $v_ExpiredItems['Lazer e entretenimento'] = [];

    $v_Items = \App\AccomodationService::whereIn('id', \App\UserTradeItem::getUserTradeItems('B1'))->where('revision_status_id', 6)
            ->select('id', 'nome_fantasia', 'cnpj')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-hospedagem';
        array_push($v_ExpiredItems['Hospedagem'], $c_Item);
    }

    $v_Items = \App\AccomodationService::whereIn('id', \App\UserTradeItem::getUserTradeItems('B2'))->where('revision_status_id', 6)
            ->select('id', 'nome_fantasia', 'cnpj')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-alimentos';
        array_push($v_ExpiredItems['Gastronomia'], $c_Item);
    }

    $v_Items = \App\AccomodationService::whereIn('id', \App\UserTradeItem::getUserTradeItems('B3'))->where('revision_status_id', 6)
            ->select('id', 'nome_fantasia', 'cnpj')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-agencias-turismo';
        array_push($v_ExpiredItems['Agenciamento'], $c_Item);
    }

    $v_Items = \App\AccomodationService::whereIn('id', \App\UserTradeItem::getUserTradeItems('B4'))->where('revision_status_id', 6)
            ->select('id', 'nome_fantasia', 'cnpj')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-transporte-turistico';
        array_push($v_ExpiredItems['Transporte'], $c_Item);
    }

    $v_Items = \App\AccomodationService::whereIn('id', \App\UserTradeItem::getUserTradeItems('B6'))->where('revision_status_id', 6)
            ->select('id', 'nome_fantasia', 'cnpj')->get()->toArray();
    foreach($v_Items as $c_Item) {
        $c_Item['path'] = 'servicos-lazer';
        array_push($v_ExpiredItems['Lazer e entretenimento'], $c_Item);
    }

    $v_ItemCount = 0;
    foreach($v_ExpiredItems as $c_Type => $c_Items)
        $v_ItemCount += count($c_Items);
?>

@if($v_ItemCount)
    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-visible">
                <header class="panel-heading">
                    <div class="panel-title">
                        {{$v_ItemCount}} cadastro(s) vencido(s)
                        <div class="minimize-panel pull-right mr10" title="Minimizar">
                            <i class="fa fa-minus"></i>
                        </div>
                        <div class="maximize-panel pull-right mr10" title="Expandir" style="display: none">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 mt15">
                            <div class="panel panel-visible">
                                <div class="panel-body pn">
                                    <div class="adv-table">
                                        <table class="table table-striped table-bordered table-hover responsive-table" id="responsive-table-expired" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Nome</th>
                                                <th>CNPJ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($v_ExpiredItems as $c_Type => $c_Items)
                                                @foreach($c_Items as $c_Item)
                                                    <tr>
                                                        <td>{{$c_Type}}</td>
                                                        <td>
                                                            <a href="{{url('admin/inventario/' . $c_Item['path'] . '/editar/' . $c_Item['id'])}}" title="Editar">
                                                                {{$c_Item['nome_fantasia']}}
                                                            </a>
                                                        </td>
                                                        <td>{{substr($c_Item['cnpj'], 0, 2) . '.' . substr($c_Item['cnpj'], 2, 3) . '.' . substr($c_Item['cnpj'], 5, 3) . '/' . substr($c_Item['cnpj'], 8, 4) . '-' . substr($c_Item['cnpj'], 12, 2)}}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endif
