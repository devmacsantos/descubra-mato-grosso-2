<?php
if($p_Query != null)
{
    $v_Events = \App\Event::join('revision_status', 'revision_status.id', '=', 'event.revision_status_id')
            ->whereRaw($p_Query)->select('event.id', 'event.nome', 'revision_status.name as status')->get();
}
else
{
    $v_Events = [];
}
?>

@if(count($v_Events))
    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-visible">
                <header class="panel-heading">
                    <div class="panel-title">
                        @if(\App\UserType::isAdmin() || \App\UserType::isMaster())
                        {{count($v_Events)}} evento(s) aguardando aprovação
                        @elseif(\App\UserType::isCircuito())
                        {{count($v_Events)}} evento(s) aguardando aprovação ou rejeitado(s)
                        @else
                        {{count($v_Events)}} evento(s) rejeitado(s)
                        @endif
                        <div class="minimize-panel pull-right mr10" title="Minimizar">
                            <i class="fa fa-minus"></i>
                        </div>
                        <div class="maximize-panel pull-right mr10" title="Expandir" style="display: none">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 mt15">
                            <div class="panel panel-visible">
                                <div class="panel-body pn">
                                    <div class="adv-table">
                                        <table class="table table-striped table-bordered table-hover responsive-table" id="responsive-table" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Nome</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($v_Events as $c_Event)
                                                    <tr>
                                                        <td>
                                                            <a href="{{url('admin/eventos/editar/' . $c_Event->id)}}" title="Editar">
                                                                {{$c_Event->nome}}
                                                            </a>
                                                        </td>
                                                        <td>{{$c_Event->status}}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endif