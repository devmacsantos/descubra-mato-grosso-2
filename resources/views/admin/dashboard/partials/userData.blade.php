<?php
    if($p_Query != null)
    {
        $v_Inventory = [
            [
                'name' => 'A1 - Informações básicas do município',
                'path' => 'informacoes',
                'name_column' => 'Município',
                'items' => \App\CityInfo::join('city', 'city.id', '=', 'city_info.city_id')->join('revision_status', 'revision_status.id', '=', 'city_info.revision_status_id')->whereRaw($p_Query)
                        ->select('city_info.id as item_id', 'city.name as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'A2.1 - Meios de acesso ao município - Geral',
                'path' => 'meios-acesso/geral',
                'name_column' => 'Município',
                'items' => \App\CityAccessGeneral::join('city', 'city.id', '=', 'city_access_general.city_id')->join('revision_status', 'revision_status.id', '=', 'city_access_general.revision_status_id')->whereRaw($p_Query)
                                        ->select('city_access_general.id as item_id', 'city.name as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'A2.2 - Meios de acesso ao município',
                'path' => 'meios-acesso',
                'items' => \App\CityAccess::join('revision_status', 'revision_status.id', '=', 'city_access.revision_status_id')->whereRaw($p_Query)
                        ->select('city_access.id as item_id', 'city_access.nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'A4 - Sistemas de segurança',
                'path' => 'sistemas-seguranca',
                'items' => \App\CitySecuritySystem::join('revision_status', 'revision_status.id', '=', 'city_security_system.revision_status_id')->whereRaw($p_Query)
                        ->select('city_security_system.id as item_id', 'city_security_system.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'A5 - Sistemas hospitalares',
                'path' => 'sistemas-hospitalares',
                'items' => \App\CityHospitalSystem::join('revision_status', 'revision_status.id', '=', 'city_hospital_system.revision_status_id')->whereRaw($p_Query)
                        ->select('city_hospital_system.id as item_id', 'city_hospital_system.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'A7 - Outros serviços',
                'path' => 'outros-servicos',
                'items' => \App\CityOtherService::join('revision_status', 'revision_status.id', '=', 'city_other_service.revision_status_id')->whereRaw($p_Query)
                        ->select('city_other_service.id as item_id', 'city_other_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'B1 - Serviços e equipamentos de hospedagem',
                'path' => 'servicos-hospedagem',
                'items' => \App\AccomodationService::join('revision_status', 'revision_status.id', '=', 'accomodation_service.revision_status_id')->whereRaw($p_Query)
                        ->select('accomodation_service.id as item_id', 'accomodation_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'B2 - Serviços e equipamentos de alimentos e bebidas',
                'path' => 'servicos-alimentos',
                'items' => \App\FoodDrinksService::join('revision_status', 'revision_status.id', '=', 'food_drinks_service.revision_status_id')->whereRaw($p_Query)
                        ->select('food_drinks_service.id as item_id', 'food_drinks_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'B3 - Serviços e equipamentos de agências de turismo',
                'path' => 'servicos-agencias-turismo',
                'items' => \App\TourismAgencyService::join('revision_status', 'revision_status.id', '=', 'tourism_agency_service.revision_status_id')->whereRaw($p_Query)
                        ->select('tourism_agency_service.id as item_id', 'tourism_agency_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'B4 - Serviços e equipamentos de transporte turístico',
                'path' => 'servicos-transporte-turistico',
                'items' => \App\TourismTransportationService::join('revision_status', 'revision_status.id', '=', 'tourism_transportation_service.revision_status_id')->whereRaw($p_Query)
                        ->select('tourism_transportation_service.id as item_id', 'tourism_transportation_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'B5 - Serviços e equipamentos para eventos',
                'path' => 'servicos-eventos',
                'items' => \App\EventService::join('revision_status', 'revision_status.id', '=', 'event_service.revision_status_id')->whereRaw($p_Query)
                        ->select('event_service.id as item_id', 'event_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'B6 - Serviços e equipamentos de lazer',
                'path' => 'servicos-lazer',
                'items' => \App\RecreationService::join('revision_status', 'revision_status.id', '=', 'recreation_service.revision_status_id')->whereRaw($p_Query)
                        ->select('recreation_service.id as item_id', 'recreation_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'B7 - Outros serviços e equipamentos turísticos',
                'path' => 'outros-servicos-turisticos',
                'items' => \App\OtherTourismService::join('revision_status', 'revision_status.id', '=', 'other_service.revision_status_id')->whereRaw($p_Query)
                        ->select('other_service.id as item_id', 'other_service.nome_fantasia as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'C1 - Atrativos naturais',
                'path' => 'atrativos-naturais',
                'items' => \App\NaturalAttraction::join('revision_status', 'revision_status.id', '=', 'natural_attraction.revision_status_id')->whereRaw($p_Query)
                        ->select('natural_attraction.id as item_id', 'natural_attraction.nome_oficial as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'C2 - Atrativos culturais',
                'path' => 'atrativos-culturais',
                'items' => \App\CulturalAttraction::join('revision_status', 'revision_status.id', '=', 'cultural_attraction.revision_status_id')->whereRaw($p_Query)
                        ->select('cultural_attraction.id as item_id', 'cultural_attraction.nome_oficial as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'C3 - Atividades econômicas',
                'path' => 'atrativos-atividades-economicas',
                'items' => \App\EconomicActivityAttraction::join('revision_status', 'revision_status.id', '=', 'economic_activity_attraction.revision_status_id')->whereRaw($p_Query)
                        ->select('economic_activity_attraction.id as item_id', 'economic_activity_attraction.nome_oficial as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'C4 - Realizações técnicas e científicas contemporâneas',
                'path' => 'realizacoes-contemporaneas',
                'items' => \App\ContemporaryRealization::join('revision_status', 'revision_status.id', '=', 'contemporary_realization.revision_status_id')->whereRaw($p_Query)
                        ->select('contemporary_realization.id as item_id', 'contemporary_realization.nome_oficial as nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'C6.1 - Gastronomia - Produto primário',
                'path' => 'produto-primario',
                'items' => \App\GastronomicPrimaryProduct::join('revision_status', 'revision_status.id', '=', 'gastronomic_primary_product.revision_status_id')->whereRaw($p_Query)
                        ->select('gastronomic_primary_product.id as item_id', 'nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'C6.2 - Gastronomia - Produto transformado',
                'path' => 'produto-transformado',
                'items' => \App\GastronomicTransformedProduct::join('revision_status', 'revision_status.id', '=', 'gastronomic_transformed_product.revision_status_id')->whereRaw($p_Query)
                        ->select('gastronomic_transformed_product.id as item_id', 'nome', 'revision_status.name as status')->get()
            ],
            [
                'name' => 'C6.3 - Gastronomia - Prato típico',
                'path' => 'prato-tipico',
                'items' => \App\GastronomicTypicalDish::join('revision_status', 'revision_status.id', '=', 'gastronomic_typical_dish.revision_status_id')->whereRaw($p_Query)
                        ->select('gastronomic_typical_dish.id as item_id', 'nome', 'revision_status.name as status')->get()
            ]
        ];
    }
    else if(\App\UserType::isTrade())
    {
        $v_Inventory = [
                [
                        'name' => 'Hospedagem',
                        'path' => 'servicos-hospedagem',
                        'items' => \App\AccomodationService::join('revision_status', 'revision_status.id', '=', 'accomodation_service.revision_status_id')->where('revision_status_id', 5)->whereIn('accomodation_service.id', \App\UserTradeItem::getUserTradeItems('B1'))
                                                           ->select('accomodation_service.id as item_id', 'accomodation_service.nome_fantasia as nome', 'revision_status.name as status')->get()
                ],
                [
                        'name' => 'Gastronomia',
                        'path' => 'servicos-alimentos',
                        'items' => \App\FoodDrinksService::join('revision_status', 'revision_status.id', '=', 'food_drinks_service.revision_status_id')->where('revision_status_id', 5)->whereIn('food_drinks_service.id', \App\UserTradeItem::getUserTradeItems('B2'))
                                                         ->select('food_drinks_service.id as item_id', 'food_drinks_service.nome_fantasia as nome', 'revision_status.name as status')->get()
                ],
                [
                        'name' => 'Agenciamento',
                        'path' => 'servicos-agencias-turismo',
                        'items' => \App\TourismAgencyService::join('revision_status', 'revision_status.id', '=', 'tourism_agency_service.revision_status_id')->where('revision_status_id', 5)->whereIn('tourism_agency_service.id', \App\UserTradeItem::getUserTradeItems('B3'))
                                                            ->select('tourism_agency_service.id as item_id', 'tourism_agency_service.nome_fantasia as nome', 'revision_status.name as status')->get()
                ],
                [
                        'name' => 'Transporte',
                        'path' => 'servicos-transporte-turistico',
                        'items' => \App\TourismTransportationService::join('revision_status', 'revision_status.id', '=', 'tourism_transportation_service.revision_status_id')->where('revision_status_id', 5)->whereIn('tourism_transportation_service.id', \App\UserTradeItem::getUserTradeItems('B4'))
                                                                    ->select('tourism_transportation_service.id as item_id', 'tourism_transportation_service.nome_fantasia as nome', 'revision_status.name as status')->get()
                ],
                [
                        'name' => 'Lazer e entretenimento',
                        'path' => 'servicos-lazer',
                        'items' => \App\RecreationService::join('revision_status', 'revision_status.id', '=', 'recreation_service.revision_status_id')->where('revision_status_id', 5)->whereIn('recreation_service.id', \App\UserTradeItem::getUserTradeItems('B6'))
                                                         ->select('recreation_service.id as item_id', 'recreation_service.nome_fantasia as nome', 'revision_status.name as status')->get()
                ]
        ];
    }
    else
    {
        $v_Inventory = [];
    }
?>

@foreach($v_Inventory as $c_Index => $c_Inventory)
    @include('admin.dashboard.partials.inventory', ['p_Inventory' => $c_Inventory])
@endforeach
