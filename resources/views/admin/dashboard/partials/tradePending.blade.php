<?php
    $v_WaitingItems = [];
    $v_WaitingItems['Eventos'] = [];
    $v_WaitingItems['Hospedagem'] = [];
    $v_WaitingItems['Gastronomia'] = [];
    $v_WaitingItems['Agenciamento'] = [];
    $v_WaitingItems['Transporte'] = [];
    $v_WaitingItems['Lazer e entretenimento'] = [];

    foreach(\App\UserTradeItem::getPendingItems('B1') as $c_Pending){
        $v_Item = \App\AccomodationService::where('id', $c_Pending->id)->select('id', 'nome_fantasia', 'cnpj')->first()->toArray();
        $v_Item['data'] = $c_Pending;
        $v_Item['path'] = 'servicos-hospedagem';
        array_push($v_WaitingItems['Hospedagem'], $v_Item);
    }
    foreach(\App\UserTradeItem::getPendingItems('B2') as $c_Pending){
        $v_Item = \App\FoodDrinksService::where('id', $c_Pending->id)->select('id', 'nome_fantasia', 'cnpj')->first()->toArray();
        $v_Item['data'] = $c_Pending;
        $v_Item['path'] = 'servicos-alimentos';
        array_push($v_WaitingItems['Gastronomia'], $v_Item);
    }
    foreach(\App\UserTradeItem::getPendingItems('B3') as $c_Pending){
        $v_Item = \App\TourismAgencyService::where('id', $c_Pending->id)->select('id', 'nome_fantasia', 'cnpj')->first()->toArray();
        $v_Item['data'] = $c_Pending;
        $v_Item['path'] = 'servicos-agencias-turismo';
        array_push($v_WaitingItems['Agenciamento'], $v_Item);
    }
    foreach(\App\UserTradeItem::getPendingItems('B4') as $c_Pending){
        $v_Item = \App\TourismTransportationService::where('id', $c_Pending->id)->select('id', 'nome_fantasia', 'cnpj')->first()->toArray();
        $v_Item['data'] = $c_Pending;
        $v_Item['path'] = 'servicos-transporte-turistico';
        array_push($v_WaitingItems['Transporte'], $v_Item);
    }
    foreach(\App\UserTradeItem::getPendingItems('B6') as $c_Pending){
        $v_Item = \App\RecreationService::where('id', $c_Pending->id)->select('id', 'nome_fantasia', 'cnpj')->first()->toArray();
        $v_Item['data'] = $c_Pending;
        $v_Item['path'] = 'servicos-lazer';
        array_push($v_WaitingItems['Lazer e entretenimento'], $v_Item);
    }
    foreach(\App\UserTradeItem::getPendingItems('EV') as $c_Pending){
        $v_Item = \App\Event::where('id', $c_Pending->id)->select('id', 'nome')->first()->toArray();
        $v_Item['data'] = $c_Pending;
        $v_Item['nome_fantasia'] = $v_Item['nome'];
        $v_Item['cnpj'] = '00000000000000';
        $v_Item['path'] = 'eventos';
        array_push($v_WaitingItems['Eventos'], $v_Item);
    }


//dd($v_WaitingItems);

    $v_ItemCount = 0;
    foreach($v_WaitingItems as $c_Type => $c_Items)
        $v_ItemCount += count($c_Items);

    
?>

@if($v_ItemCount)
    <div class="row">
        <div class="col-sm-12">
            <section class="panel panel-visible">
                <header class="panel-heading">
                    <div class="panel-title">
                        Cadastre seu negócio/Eventos - {{$v_ItemCount}} pedido(s) de posse de cadastro
                        <div class="minimize-panel pull-right mr10" title="Minimizar">
                            <i class="fa fa-minus"></i>
                        </div>
                        <div class="maximize-panel pull-right mr10" title="Expandir" style="display: none">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 mt15">
                            <div class="panel panel-visible">
                                <div class="panel-body pn">
                                    <div class="adv-table">
                                        <table class="table table-striped table-bordered table-hover responsive-table" id="trade-responsive-table" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Tipo</th>
                                                <th>Nome</th>
                                                <th>CNPJ</th>
                                                <th>Nome do solicitante</th>
                                                <th>Email</th>
                                                <th>Ações</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($v_WaitingItems as $c_Type => $c_Items)
                                                @foreach($c_Items as $c_Item)
                                                    <tr>
                                                        <td>{{$c_Type}}</td>
                                                        <td>
                                                            @if ($c_Type == "Eventos")
                                                            <a href="{{url('admin/' . $c_Item['path'] . '/editar/' . $c_Item['id'])}}" title="Editar">
                                                                {{$c_Item['nome_fantasia']}}
                                                            </a>
                                                            @else
                                                            <a href="{{url('admin/inventario/' . $c_Item['path'] . '/editar/' . $c_Item['id'])}}" title="Editar">
                                                                {{$c_Item['nome_fantasia']}}
                                                            </a>
                                                            @endif

                                                        </td>
                                                        <td>{{substr($c_Item['cnpj'], 0, 2) . '.' . substr($c_Item['cnpj'], 2, 3) . '.' . substr($c_Item['cnpj'], 5, 3) . '/' . substr($c_Item['cnpj'], 8, 4) . '-' . substr($c_Item['cnpj'], 12, 2)}}</td>
                                                        <td>{{$c_Item['data']['name']}}</td>
                                                        <td>{{$c_Item['data']['email']}}</td>
                                                        <td>
                                                            <div class="actions-div">
                                                                <a href="{!! url('admin/tratar-solicitacao-cnpj?aprovar=1&tipo=' . $c_Item['data']['type'] . '&id=' . $c_Item['data']['id'] . '&user_id=' . $c_Item['data']['user_id']) !!}" title="Aprovar" type="button" class="btn btn-success">
                                                                    <i class="fa fa-check"></i>
                                                                </a>
                                                                <a href="{!! url('admin/tratar-solicitacao-cnpj?aprovar=0&tipo=' . $c_Item['data']['type'] . '&id=' . $c_Item['data']['id'] . '&user_id=' . $c_Item['data']['user_id']) !!}" title="Rejeitar" type="button" class="btn btn-success delete-btn">
                                                                    <i class="fa fa-times"></i>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endif
