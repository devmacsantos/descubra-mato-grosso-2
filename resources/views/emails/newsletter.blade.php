@extends('emails.base')
@section('mail-title')
	Newsletter
@stop
@section('mail-content')
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">{!! $p_Msg !!}</p>
	<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">
		Se você não quiser receber nossos emails, cancele sua inscrição aqui: <a href="{{ url('/pt/newsletter/cancelar-assinatura/' . $p_Email) }}" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>
	</p>
@stop
