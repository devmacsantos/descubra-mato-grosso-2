@extends('emails.base')
@section('mail-title')
	Vencimento de cadastro
@stop
@section('mail-content')
	<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">{!! $p_Msg !!}</p>
	<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">
		{!! $p_CallToAction !!}
	</p>
@stop
