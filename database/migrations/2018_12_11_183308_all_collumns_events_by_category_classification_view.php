<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllCollumnsEventsByCategoryClassificationView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW events_by_date_category_classification");

        DB::statement("CREATE VIEW  `events_by_date_category_classification` AS
        SELECT 
            `event_by_date`.`start` AS `start`,
            `event_by_date`.`end` AS `end`,
            `event_by_date`.`event_id` AS `event_id`,
            `event_by_date`.`name` AS `name`,
            `event_by_date`.`short_description` AS `short_description`,
            `event_by_date`.`events_place` AS `events_place`,
            `event_by_date`.`featured` AS `featured`,
            `event_by_date`.`city` AS `city`,
            `event_by_date`.`id` AS `id`,
            `event_by_date`.`city_id` AS `city_id`,
            `event_by_date`.`touristic_circuit_id` AS `touristic_circuit_id`,
            `event_by_date`.`revision_status_id` AS `revision_status_id`,
            `event_by_date`.`active` AS `active`,
            `event_by_date`.`nome` AS `nome`,
            `event_by_date`.`slug` AS `slug`,
            `event_by_date`.`destaque` AS `destaque`,
            `event_by_date`.`cep` AS `cep`,
            `event_by_date`.`bairro` AS `bairro`,
            `event_by_date`.`logradouro` AS `logradouro`,
            `event_by_date`.`numero` AS `numero`,
            `event_by_date`.`complemento` AS `complemento`,
            `event_by_date`.`latitude` AS `latitude`,
            `event_by_date`.`longitude` AS `longitude`,
            `event_by_date`.`site` AS `site`,
            `event_by_date`.`telefone` AS `telefone`,
            `event_by_date`.`email` AS `email`,
            `event_by_date`.`facebook` AS `facebook`,
            `event_by_date`.`instagram` AS `instagram`,
            `event_by_date`.`twitter` AS `twitter`,
            `event_by_date`.`youtube` AS `youtube`,
            `event_by_date`.`flickr` AS `flickr`,
            `event_by_date`.`descricao_curta` AS `descricao_curta`,
            `event_by_date`.`local_evento` AS `local_evento`,
            `event_by_date`.`espaco_evento` AS `espaco_evento`,
            `event_by_date`.`tipo_evento` AS `tipo_evento`,
            `event_by_date`.`sobre` AS `sobre`,
            `event_by_date`.`dados_relevantes` AS `dados_relevantes`,
            `event_by_date`.`informacoes_uteis` AS `informacoes_uteis`,
            `event_by_date`.`created_at` AS `created_at`,
            `event_by_date`.`created_by` AS `created_by`,
            `event_by_date`.`updated_at` AS `updated_at`,
            `event_by_date`.`updated_by` AS `updated_by`,
            `event_by_date`.`comentario_revisao` AS `comentario_revisao`,
            `event_selected_categories`.`event_category_id` AS `event_category_id`,
            `event_selected_classification`.`event_classification_id` AS `event_classification_id`
        FROM
            ((`event_by_date`
            LEFT JOIN `event_selected_categories` ON ((`event_by_date`.`event_id` = `event_selected_categories`.`event_id`)))
            LEFT JOIN `event_selected_classification` ON ((`event_by_date`.`event_id` = `event_selected_classification`.`event_id`)))");


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
