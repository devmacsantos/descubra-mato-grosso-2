<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventsByDateCategoryClassificationView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW events_by_date_category_classification AS
                        SELECT minasgerais.event_by_date.event_id, 
                            minasgerais.event_by_date.end,
                            minasgerais.event_by_date.start,
                            minasgerais.event_by_date.featured,
                            minasgerais.event_by_date.name,
                            minasgerais.event_by_date.short_description,
							minasgerais.event_by_date.city,
                            minasgerais.event_by_date.city_id,
                            minasgerais.event_by_date.events_place,
                            minasgerais.event_by_date.revision_status_id,
                            minasgerais.event_by_date.slug,
                            minasgerais.event_selected_categories.event_category_id,
                            minasgerais.event_selected_classification.event_classification_id
                            from minasgerais.event_by_date
                        LEFT OUTER JOIN minasgerais.event_selected_categories
                        on minasgerais.event_by_date.event_id = minasgerais.event_selected_categories.event_id
                        LEFT OUTER JOIN minasgerais.event_selected_classification
                        on minasgerais.event_by_date.event_id = minasgerais.event_selected_classification.event_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW events_by_date_category_classification");
    }
}
