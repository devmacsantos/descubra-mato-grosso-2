<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventsByDateView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW event_by_date AS
                        SELECT min(minasgerais.event_date.date) as start, max(minasgerais.event_date.date) as end, minasgerais.event_date.event_id, minasgerais.event.nome as name, minasgerais.event.slug as slug, minasgerais.event.descricao_curta as short_description, minasgerais.event.local_evento events_place, minasgerais.event.destaque as featured, minasgerais.event.revision_status_id as revision_status_id, minasgerais.city.name as city, minasgerais.event.city_id as city_id
                        FROM minasgerais.event_date, minasgerais.event, minasgerais.city 
                        WHERE minasgerais.event_date.event_id = minasgerais.event.id and minasgerais.event.city_id = minasgerais.city.id and minasgerais.event_date.date > DATE_SUB(CURDATE(),INTERVAL 180 DAY)
                        GROUP BY minasgerais.event_date.event_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW event_by_date");
    }
}
