<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewEventsByDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("DROP VIEW event_by_date");

        DB::statement("CREATE VIEW event_by_date AS
                        SELECT min(minasgerais.event_date.date) as start,
                        max(minasgerais.event_date.date) as end, 
                        minasgerais.event_date.event_id, 
                        minasgerais.event.nome as name, 
                        minasgerais.event.descricao_curta as short_description, 
                        minasgerais.event.local_evento events_place, 
                        minasgerais.event.destaque as featured, 
                        minasgerais.city.name as city, 

                        minasgerais.event.id as id,
                        minasgerais.event.city_id as city_id,
                        minasgerais.event.touristic_circuit_id as touristic_circuit_id,
                        minasgerais.event.revision_status_id as revision_status_id,
                        minasgerais.event.active as active,
                        minasgerais.event.nome as nome,
                        minasgerais.event.slug as slug,
                        minasgerais.event.destaque as destaque,
                        minasgerais.event.cep as cep,
                        minasgerais.event.bairro as bairro,
                        minasgerais.event.logradouro as logradouro,
                        minasgerais.event.numero as numero,
                        minasgerais.event.complemento as complemento,
                        minasgerais.event.latitude as latitude,
                        minasgerais.event.longitude as longitude,
                        minasgerais.event.site as site,
                        minasgerais.event.telefone as telefone,
                        minasgerais.event.email as email,
                        minasgerais.event.facebook as facebook,
                        minasgerais.event.instagram as instagram,
                        minasgerais.event.twitter as twitter,
                        minasgerais.event.youtube as youtube,
                        minasgerais.event.flickr as flickr,
                        minasgerais.event.descricao_curta as descricao_curta,
                        minasgerais.event.local_evento as local_evento,
                        minasgerais.event.espaco_evento as espaco_evento,
                        minasgerais.event.tipo_evento as tipo_evento,
                        minasgerais.event.sobre as sobre,
                        minasgerais.event.dados_relevantes as dados_relevantes,
                        minasgerais.event.informacoes_uteis as informacoes_uteis,
                        minasgerais.event.created_at as created_at,
                        minasgerais.event.created_by as created_by,
                        minasgerais.event.updated_at as updated_at,
                        minasgerais.event.updated_by as updated_by,
                        minasgerais.event.comentario_revisao as comentario_revisao

                        FROM minasgerais.event_date, minasgerais.event, minasgerais.city 
                        WHERE minasgerais.event_date.event_id = minasgerais.event.id and minasgerais.event.city_id = minasgerais.city.id
                        GROUP BY minasgerais.event_date.event_id");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
