-- alter table attraction add column nome_oficial varchar(250);

alter table attraction drop index nome_pt;
alter table attraction add fulltext(nome_pt,nome_oficial);

-- b1
update attraction a
inner join accomodation_service ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_juridico
where a.item_inventario = 'B1';
-- b2
update attraction a
inner join food_drinks_service ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_juridico
where a.item_inventario = 'B2';
-- forms['B3'] = 'tourism_agency_service'
update attraction a
inner join tourism_agency_service ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_juridico
where a.item_inventario = 'B3';

-- forms['B4'] = 'tourism_transportation_service'
update attraction a
inner join tourism_transportation_service ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_juridico
where a.item_inventario = 'B4';
-- forms['B5'] = 'event_service'
update attraction a
inner join event_service ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_juridico
where a.item_inventario = 'B5';
-- forms['B6'] = 'recreation_service'
update attraction a
inner join recreation_service ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_juridico
where a.item_inventario = 'B6';
-- forms['B7'] = 'other_service'
update attraction a
inner join other_service ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_juridico
where a.item_inventario = 'B7';
-- forms['C1'] = 'natural_attraction'
update attraction a
inner join natural_attraction ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_oficial
where a.item_inventario = 'C1';
-- forms['C2'] = 'cultural_attraction'
update attraction a
inner join cultural_attraction ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_oficial
where a.item_inventario = 'C2';
-- forms['C3'] = 'economic_activity_attraction'
update attraction a
inner join economic_activity_attraction ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_oficial
where a.item_inventario = 'C3';
-- forms['C4'] = 'contemporary_realization'
update attraction a
inner join contemporary_realization ca on a.item_inventario_id = ca.id
set a.nome_oficial=ca.nome_oficial
where a.item_inventario = 'C4';
-- forms['C6.1'] = 'gastronomic_primary_product'
-- forms['C6.2'] = 'gastronomic_transformed_product'
-- forms['C6.3'] = 'gastronomic_typical_dish'


-- executar ao colocar em produção ou quando for fazer testes 
update attraction a inner join type t on a.type_id = t.id
set a.type_slug=t.slug, a.slug_trade=t.trade, a.sinonimos=t.sinonimos, a.antonimos=t.excluir
where sub_type_id is null;

update attraction a inner join type t on a.sub_type_id = t.id
set a.type_slug=t.slug, a.slug_trade=t.trade, a.sinonimos=t.sinonimos, a.antonimos=t.excluir
where sub_type_id is not null;

update attraction a inner join city c on a.city_id = c.id
set a.city_name = c.name 


CREATE FULLTEXT INDEX fts
ON region(name)

CREATE FULLTEXT INDEX fts
ON blog_article(titulo_pt, conteudo)

-- to document
-- CREATE FULLTEXT INDEX fts_exclude
-- ON attraction(antonimos)