<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsThemeControlToTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_classification', function (Blueprint $table) {
            $table->date('initial_date');
            $table->date('final_date');
            $table->boolean('public');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_classification', function (Blueprint $table) {
            $table->dropColumn('initial_date');
            $table->dropColumn('final_date');
            $table->dropColumn('public');
        });
    }
}
