<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class RevisionStatus extends Model
{
    public $timestamps = false;
    protected $table = 'revision_status';

    public static function getList()
    {
        return RevisionStatus::whereIn('id',[2,3,4,5])->lists('name', 'id')->toArray();
    }

    public static function getCompleteStatusList()
    {
        return RevisionStatus::whereIn('id',[1,2,3,4,5])->lists('name', 'id')->toArray();
    }
}