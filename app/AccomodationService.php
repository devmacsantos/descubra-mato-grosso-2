<?php

namespace App;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class AccomodationService extends BaseInventoryModel {

    protected $table = 'accomodation_service';
    //Indicação dos campos que podem ser salvos atravé do comando create/update
    protected $fillable = [
        'city_id',
        'tipo_atividade_cadastur',
        'revision_status_id',
        'district_id',
        'type_id',
        'sub_type_id',
        'nome_fantasia',
        'nome_juridico',
        'nome_rede',
        'cnpj',
        'registro_cadastur',
        'inicio_atividade',
        'data_tombamento',
        'organizacao_responsavel',
        'cep',
        'bairro',
        'logradouro',
        'numero',
        'complemento',
        'telefone',
        'whatsapp',
        'site',
        'email',
        'facebook',
        'instagram',
        'twitter',
        'youtube',
        'tripadvisor',
        'minas_360',
        'descricao_arredores_distancia_pontos',
        'latitude',
        'longitude',
        'localizacao',
        'funcionamento_1',
        'funcionamento_2',
        'funcionamento_observacao',
        'meses_maior_ocupacao',
        'valor_medio_diaria',
        'unidades_habitacionais',
        'unidades_habitacionais_facilidades',
        'unidades_habitacionais_voltagem',
        'unidades_habitacionais_facilidades_outras',
        'meios_hospedagem_extra_hoteleiros',
        'tipo_diaria',
        'tipo_diaria_outros',
        'servicos_equipamentos_area_social',
        'servicos_equipamentos_recreacao_lazer',
        'aceita_animais',
        'gay_friendly',
        'nao_aceita_criancas',
        'restricoes_hospedes',
        'formas_pagamento',
        'descricoes_observacoes_complementares',
        'acessibilidade',
        'equipe_responsavel_responsavel',
        'equipe_responsavel_instituicao',
        'equipe_responsavel_telefone',
        'equipe_responsavel_email',
        'equipe_responsavel_observacao',
        'equipamento_fechado',
        'equipamento_fechado_motivo',
        'comentario_revisao',
        'tem_cadastur',
        'descricao_curta',
        'destaque',
        'hashtags',
        'data_publicacao',
        'tipos_viagem',
        'possui_espacos_eventos',
        'validade_cadastur',
        'atracao',
    ];

    public static function getDT($p_Name, $p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw) {
        $v_Query = AccomodationService::join('city', 'city.id', '=', 'accomodation_service.city_id')->join('revision_status', 'revision_status.id', '=', 'accomodation_service.revision_status_id')
                ->select(DB::raw('SQL_CALC_FOUND_ROWS accomodation_service.id, accomodation_service.city_id, accomodation_service.nome_fantasia, city.name as municipio, accomodation_service.created_at, accomodation_service.updated_at, revision_status.name as status'));

        if (UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if (UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());
        else if (UserType::isTrade())
            $v_Query->whereIn('accomodation_service.id', UserTradeItem::getUserTradeItems('B1'));

        if ($p_Name != '')
            $v_Query->where('accomodation_service.nome_fantasia', 'like', '%' . $p_Name . '%');

        if ($p_City != '')
            $v_Query->where('city.name', 'like', '%' . $p_City . '%');

        if ($p_CreatedAt != '') {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('accomodation_service.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('accomodation_service.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if ($p_UpdatedAt != '') {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('accomodation_service.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('accomodation_service.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if ($p_Status != '')
            $v_Query->where('accomodation_service.revision_status_id', $p_Status);

        if ($p_Order != null) {
            if ($p_Order["column"] == 0)
                $v_Query->orderBy('accomodation_service.nome_fantasia', $p_Order["dir"]);
            if ($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if ($p_Order["column"] == 2)
                $v_Query->orderBy('accomodation_service.created_at', $p_Order["dir"]);
            if ($p_Order["column"] == 3)
                $v_Query->orderBy('accomodation_service.updated_at', $p_Order["dir"]);
            if ($p_Order["column"] == 4)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if ($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for ($c_Index = 0; $c_Index < sizeof($v_QueryRes); $c_Index++) {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['nome_fantasia'],
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                '<a href="' . url('admin/inventario/servicos-hospedagem/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                '<a href="' . url('admin/inventario/servicos-hospedagem/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/servicos-hospedagem/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = BaseInventoryModel::getTotalRows();
        if (UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = AccomodationService::where('city_id', Auth::user()->city_id)->count();
        else if (UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = AccomodationService::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else if (UserType::isTrade())
            $v_DataTableAjax->recordsTotal = AccomodationService::whereIn('id', UserTradeItem::getUserTradeItems('B1'))->count();
        else
            $v_DataTableAjax->recordsTotal = AccomodationService::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function post($p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data) {
        $v_Data = $p_Data['formulario'];

        if (array_key_exists('data_tombamento', $v_Data) && $v_Data['data_tombamento'] != '')
            $v_Data['data_tombamento'] = Carbon::createFromFormat('d/m/Y', $v_Data['data_tombamento'])->format('Y-m-d H:i:s');

        if (array_key_exists('publicar', $v_Data)) {
            $v_Publicar = $v_Data['publicar'];
            unset($v_Data['publicar']);
        } else
            $v_Publicar = 0;

        $v_Data['possui_espacos_eventos'] = array_key_exists('possui_espacos_eventos', $v_Data) ? 1 : 0;
        $v_Data['equipamento_fechado'] = array_key_exists('equipamento_fechado', $v_Data) ? 1 : 0;
        $v_Data['destaque'] = array_key_exists('destaque', $v_Data) ? 1 : 0;
        $v_Data['atracao'] = array_key_exists('atracao', $v_Data) ? 1 : 0;
        $v_Data['aceita_animais'] = array_key_exists('aceita_animais', $v_Data) ? 1 : 0;
        $v_Data['nao_aceita_criancas'] = array_key_exists('nao_aceita_criancas', $v_Data) ? 1 : 0;

        if (array_key_exists('meses_maior_ocupacao', $p_Data) && $p_Data['meses_maior_ocupacao'] != '') {
            $v_BusyMonths = $p_Data['meses_maior_ocupacao'];
            $v_Months = '';
            foreach ($v_BusyMonths as $c_Month)
                $v_Months .= $c_Month . ';';
            $v_Data['meses_maior_ocupacao'] = $v_Months;
        }

        array_walk($v_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_PreviousStatus = 0;
        if ($p_Id != null)
            $v_PreviousStatus = AccomodationService::find($p_Id)->revision_status_id;

        $v_Item = AccomodationService::updateOrCreate(['id' => $p_Id], $v_Data);
        InventoryPhoto::updatePhotos('B1', $v_Item->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);

        if ($p_Id == null && UserType::isTrade())
            UserTradeItem::postUserTradeItem('B1', $v_Item->id);

        if ($v_Item->registro_cadastur == null) {
            if ($v_Item->type_id == 37 || $v_Item->sub_type_id == 51)
                $v_Publicar = 2;
        }

        if (UserType::isAdmin() || UserType::isMaster()) {
            if ($v_Item->revision_status_id == 4 && $v_Publicar == 1) {
                Attraction::publish($v_Item, 'B1', 1);
                $v_Item->data_publicacao = Carbon::now()->format('Y-m-d H:i:s');
                $v_Item->save();
            } else if ($v_Item->data_publicacao != null && $v_Publicar == 2) {
                Attraction::unpublish($v_Item->id, 'B1');
                $v_Item->data_publicacao = null;
                $v_Item->save();
            }
        }

        if ($p_Id != null && $v_PreviousStatus != 4) {
            $v_Subject = null;
            $v_Message = null;
            if ($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_fantasia . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            } else if ($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_fantasia . ' foram rejeitadas.</p>';
                if (!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if ($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'B1', $v_Item->id);
                foreach ($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message], function ($message) use ($c_User, $v_Subject) {
                        $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                    });
                }
            }
        }

        return $v_Item;
    }

    public static function deleteAccomodationService($p_Id) {
        $v_Query = AccomodationService::where('id', $p_Id);
        if (UserType::isMunicipio())
            $v_Query->where('city_id', Auth::user()->city_id);
        else if (UserType::isCircuito())
            $v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
        else if (UserType::isTrade())
            $v_Query->whereIn('id', UserTradeItem::getUserTradeItems('B1'));
        $v_Item = $v_Query->firstOrFail();
        InventoryPhoto::deletePhotos('B1', $p_Id);

        if ($v_Item->data_publicacao != null)
            Attraction::unpublish($p_Id, 'B1');

        UserTradeItem::deleteUserTradeItem('B1', $p_Id);

        $v_Item->equipe_responsavel_observacao = 'DELETED';
        $v_Item->save();
        $v_Item->delete();
    }

    public static function checkCNPJAvailability($p_CNPJ) {
        $v_Item = AccomodationService::where('cnpj', $p_CNPJ)->first();
        if ($v_Item == null)
            return -1;
        else if (UserTradeItem::userAlreadyRequestedItem('B1', $v_Item->id))
            return -2;
        else
            return UserTradeItem::itemHasUser('B1', $v_Item->id);
    }

    public static function findIdByCNPJ($p_CNPJ) {
        return AccomodationService::where('cnpj', $p_CNPJ)->first()->id;
    }

    public static function getReport($p_Data) {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_DistrictId = $p_Data['district_id'];
        $v_TypeId = $p_Data['type_id'];
        $v_SubTypeId = $p_Data['sub_type_id'];

        $v_Query = AccomodationService::leftJoin('city', 'city.id', '=', 'accomodation_service.city_id')
                ->leftJoin('district', 'accomodation_service.district_id', '=', 'district.id')
                ->leftJoin('touristic_circuit_cities', 'accomodation_service.city_id', '=', 'touristic_circuit_cities.city_id')
                ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                ->leftJoin('destination', 'destination.city_id', '=', 'accomodation_service.city_id')
                ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                ->leftJoin('type', 'accomodation_service.type_id', '=', 'type.id')
                ->leftJoin('type as sub_type', 'accomodation_service.sub_type_id', '=', 'sub_type.id')
                ->selectRaw('accomodation_service.*, city.name as cidade, district.name as distrito, touristic_circuit.nome as circuito, region.name as regiao, type.name as type, sub_type.name as subtype')
                ->groupBy('accomodation_service.id');

        if (array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('accomodation_service.city_id', $p_Data['city_id']);

        if ($v_CircuitId != '')
            $v_Query->where('touristic_circuit_cities.touristic_circuit_id', $v_CircuitId);

        if ($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        if ($v_DistrictId != '')
            $v_Query->where('accomodation_service.district_id', $v_DistrictId);

        if ($v_TypeId != '')
            $v_Query->where('accomodation_service.type_id', $v_TypeId);

        if ($v_SubTypeId != '')
            $v_Query->where('accomodation_service.sub_type_id', $v_SubTypeId);

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioB1.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results) {
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                $v_Languages = ['pt', 'en', 'es', 'fr'];
                foreach ($v_Results as $c_Result) {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->distrito);
                        array_push($v_Data, $c_Result->type);
                        array_push($v_Data, $c_Result->subtype);
                        $v_CNPJ = empty($c_Result->cnpj) ? '' : substr($c_Result->cnpj, 0, 2) . '.' . substr($c_Result->cnpj, 2, 3) . '.' . substr($c_Result->cnpj, 5, 3) . '/' . substr($c_Result->cnpj, 8, 4) . '-' . substr($c_Result->cnpj, 12, 2);
                        array_push($v_Data, $v_CNPJ);
                        array_push($v_Data, $c_Result->nome_fantasia);
                        array_push($v_Data, $c_Result->nome_juridico);
                        array_push($v_Data, $c_Result->nome_rede);
                        array_push($v_Data, $c_Result->inicio_atividade);
                        array_push($v_Data, $c_Result->registro_cadastur);
                        array_push($v_Data, $c_Result->data_tombamento);
                        array_push($v_Data, $c_Result->organizacao_responsavel);
                        array_push($v_Data, $c_Result->cep);
                        array_push($v_Data, $c_Result->bairro);
                        array_push($v_Data, $c_Result->logradouro);
                        array_push($v_Data, $c_Result->numero);
                        array_push($v_Data, $c_Result->complemento);
                        array_push($v_Data, $c_Result->telefone);
                        array_push($v_Data, $c_Result->whatsapp);
                        array_push($v_Data, $c_Result->site);
                        array_push($v_Data, $c_Result->email);
                        array_push($v_Data, $c_Result->latitude);
                        array_push($v_Data, $c_Result->longitude);
                        array_push($v_Data, $c_Result->localizacao);
                        $v_DescricaoArredoresJSON = json_decode($c_Result->descricao_arredores_distancia_pontos, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_DescricaoArredoresJSON[$c_Lang]);

                        array_push($v_Data, $c_Result->facebook);
                        array_push($v_Data, $c_Result->instagram);
                        array_push($v_Data, $c_Result->twitter);
                        array_push($v_Data, $c_Result->youtube);
                        array_push($v_Data, $c_Result->tripadvisor);
                        array_push($v_Data, $c_Result->minas_360);

                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_1, 1);
                        $v_Funcionamento = '';
                        if ($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach ($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach ($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if ($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if ($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if (!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_2, 1);
                        $v_Funcionamento = '';
                        if ($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach ($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach ($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if ($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if ($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if (!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        $v_FuncionamentoObsJSON = json_decode($c_Result->funcionamento_observacao, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_FuncionamentoObsJSON[$c_Lang]);

                        $v_BusyMonths = explode(';', $c_Result->meses_maior_ocupacao);
                        $v_Months = '';
                        foreach ($v_BusyMonths as $c_Index => $c_Month) {
                            if (!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);

                        $v_UHsJSON = json_decode($c_Result->unidades_habitacionais, 1);
                        foreach ($v_UHsJSON as $c_Field)
                            array_push($v_Data, $c_Field['valor']);

                        $v_FacilidadesUHsJSON = json_decode($c_Result->unidades_habitacionais_facilidades, 1);
                        foreach ($v_FacilidadesUHsJSON as $c_Field)
                            array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->unidades_habitacionais_facilidades_outras);

                        $v_VoltagensJSON = json_decode($c_Result->unidades_habitacionais_voltagem, 1);
                        $v_String = '';
                        foreach ($v_VoltagensJSON as $c_Index => $c_Voltagem)
                            $v_String .= ($c_Index > 0 ? ', ' : '') . $c_Voltagem['nome'];
                        array_push($v_Data, $v_String);
                        $v_MeiosExtraHoteleirosJSON = json_decode($c_Result->meios_hospedagem_extra_hoteleiros, 1);
                        foreach ($v_MeiosExtraHoteleirosJSON as $c_Field) {
                            if ($c_Field['tipo'] == 'checkbox')
                                array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');
                            else if ($c_Field['tipo'] == 'select2') {
                                $v_Valor = '';
                                $c_Field['valor'] = $c_Field['valor'] == null ? [] : $c_Field['valor'];
                                foreach ($c_Field['valor'] as $c_Index => $c_Value)
                                    $v_Valor .= ($c_Index > 0 ? ', ' : '') . $c_Value['nome'];
                                array_push($v_Data, $v_Valor);
                            } else
                                array_push($v_Data, $c_Field['valor']);
                        }

                        $v_TiposDiariaJSON = json_decode($c_Result->tipo_diaria, 1);
                        $v_TiposDiaria = '';
                        foreach ($v_TiposDiariaJSON as $c_Index => $c_TipoDiaria)
                            $v_TiposDiaria .= ($c_Index > 0 ? ', ' : '') . $c_TipoDiaria;
                        array_push($v_Data, $v_TiposDiaria);

                        array_push($v_Data, $c_Result->valor_medio_diaria);

                        $v_TipoDiariaOutrosJSON = json_decode($c_Result->tipo_diaria_outros, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_TipoDiariaOutrosJSON[$c_Lang]);

                        $v_ServicosEquipamentosJSON = json_decode($c_Result->servicos_equipamentos_area_social, 1);
                        foreach ($v_ServicosEquipamentosJSON as $c_Field) {
                            if ($c_Field['tipo'] == 'checkbox')
                                array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');
                            else
                                array_push($v_Data, $c_Field['valor']);
                        }

                        $v_ServicosEquipamentosJSON = json_decode($c_Result->servicos_equipamentos_recreacao_lazer, 1);
                        foreach ($v_ServicosEquipamentosJSON as $c_Field)
                            array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');

                        array_push($v_Data, $c_Result->aceita_animais ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->gay_friendly ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->nao_aceita_criancas ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->restricoes_hospedes);

                        $v_PagamentoJSON = json_decode($c_Result->formas_pagamento, 1);
                        $v_Pagamento = '';
                        foreach ($v_PagamentoJSON as $c_Index => $c_Pagamento)
                            $v_Pagamento .= ($c_Index > 0 ? ', ' : '') . $c_Pagamento['nome'];
                        array_push($v_Data, $v_Pagamento);

                        $v_DescricaoCurtaJSON = json_decode($c_Result->descricao_curta, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_DescricaoCurtaJSON[$c_Lang]);
                        $v_DescricaoJSON = json_decode($c_Result->descricoes_observacoes_complementares, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_DescricaoJSON[$c_Lang]);

                        array_push($v_Data, $c_Result->atracao ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->destaque ? 'Sim' : 'Não');
                        $v_HashtagsArray = explode(';', $c_Result->hashtags);
                        $v_Hashtags = '';
                        foreach ($v_HashtagsArray as $c_Index => $c_Hashtag) {
                            if (!empty($c_Hashtag))
                                $v_Hashtags .= ($c_Index > 0 ? ', ' : '') . $c_Hashtag;
                        }
                        array_push($v_Data, $v_Hashtags);

                        array_push($v_Data, $c_Result->possui_espacos_eventos ? 'Sim' : 'Não');

                        $v_AcessibilidadeJSON = json_decode($c_Result->acessibilidade, 1);
                        if ($v_AcessibilidadeJSON != null && $v_AcessibilidadeJSON['possui']) {
                            array_push($v_Data, 'Sim');
                            $v_FieldsArray = [];
                            $v_ObsArray = [];
                            foreach ($v_AcessibilidadeJSON['campos'] as $c_Field) {
                                if (stripos($c_Field['nome'], 'Rota externa acessível - Outras - ') > -1)
                                    array_push($v_Data, $c_Field['valor']);
                                else if ($c_Field['nome'] == 'Rota externa acessível - Outras')
                                    array_push($v_Data, $c_Field['valor'], '', '', '');
                                else if (stripos($c_Field['nome'], 'Observações complementares - ') > -1)
                                    array_push($v_ObsArray, $c_Field['valor']);
                                else if ($c_Field['nome'] == 'Observações complementares')
                                    array_push($v_ObsArray, $c_Field['valor'], '', '', '');
                                else {
                                    $v_Valor = '';
                                    $c_Field['valor'] = $c_Field['valor'] == null ? [] : $c_Field['valor'];
                                    foreach ($c_Field['valor'] as $c_Index => $c_Value)
                                        $v_Valor .= ($c_Index > 0 ? ', ' : '') . $c_Value;
                                    array_push($v_FieldsArray, $v_Valor);
                                }
                            }
                            $v_Data = array_merge($v_Data, $v_FieldsArray, $v_ObsArray);
                        } else
                            array_push($v_Data, 'Não', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        array_push($v_Data, $c_Result->equipamento_fechado ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->equipamento_fechado ? $c_Result->equipamento_fechado_motivo : '');

                        $sheet->row($v_CurrentRow, $v_Data);
                        $v_CurrentRow++;
                    } catch (\Exception $e) {
                        
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:GW' . ($v_CurrentRow - 1), 'thin');
            });
        })->download('xlsx');
    }

    public function getEventBasicFields() {
        $v_Event = new EventService();
        $v_Event->city_id = $this->city_id;
        $v_Event->district_id = $this->district_id;
        $v_Event->cnpj = $this->cnpj;
        $v_Event->nome_fantasia = $this->nome_fantasia;
        $v_Event->nome_juridico = $this->nome_juridico;
        $v_Event->nome_rede = $this->nome_rede;
        $v_Event->inicio_atividade = $this->inicio_atividade;
        $v_Event->cep = $this->cep;
        $v_Event->bairro = $this->bairro;
        $v_Event->logradouro = $this->logradouro;
        $v_Event->numero = $this->numero;
        $v_Event->complemento = $this->complemento;
        $v_Event->telefone = $this->telefone;
        $v_Event->whatsapp = $this->whatsapp;
        $v_Event->site = $this->site;
        $v_Event->email = $this->email;
        $v_Event->latitude = $this->latitude;
        $v_Event->longitude = $this->longitude;
        $v_Event->localizacao = $this->localizacao;
        $v_DescricaoArredoresJSON = json_decode($this->descricao_arredores_distancia_pontos, 1);
        $v_Event->descricao_arredores_distancia_pontos = $v_DescricaoArredoresJSON['pt'];

        return $v_Event;
    }

    public static function getGeoReport($p_Data) {
        $v_Query = AccomodationService::leftJoin('city', 'city.id', '=', 'accomodation_service.city_id')
                ->leftJoin('touristic_circuit_cities', 'accomodation_service.city_id', '=', 'touristic_circuit_cities.city_id')
                ->leftJoin('destination', 'destination.city_id', '=', 'accomodation_service.city_id')
                ->join('type', 'accomodation_service.type_id', '=', 'type.id')
                ->whereNotNull('accomodation_service.latitude')
                ->whereNotNull('accomodation_service.longitude')
                ->selectRaw('accomodation_service.nome_fantasia as nome, accomodation_service.latitude, accomodation_service.longitude, accomodation_service.type_id, accomodation_service.sub_type_id, city.name as cidade')
                ->groupBy('accomodation_service.id');

        if (array_key_exists('id', $p_Data))
            $v_Query->whereIn('accomodation_service.city_id', $p_Data['id']);

        if (array_key_exists('touristic_circuit_id', $p_Data))
            $v_Query->whereIn('touristic_circuit_cities.touristic_circuit_id', $p_Data['touristic_circuit_id']);

        if (array_key_exists('region_id', $p_Data))
            $v_Query->whereIn('destination.region_id', $p_Data['region_id']);

        if (array_key_exists('revision_status_id', $p_Data))
            $v_Query->whereIn('accomodation_service.revision_status_id', $p_Data['revision_status_id']);

        if (array_key_exists('published', $p_Data) && $p_Data['published'] == 1) {
            if ($p_Data['published'] == 1)
                $v_Query->whereNotNull('accomodation_service.data_publicacao');
            else
                $v_Query->whereNull('accomodation_service.data_publicacao');
        }

        return $v_Query->get();
    }

}
