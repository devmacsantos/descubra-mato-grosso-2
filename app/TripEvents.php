<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TripEvents extends Model
{
    public $timestamps = false;
    protected $table = 'trip_type_events';

    public static function updateTripTypes($p_EventId, $p_TripTypes)
    {
        TripEvents::where('event_id', $p_EventId)->delete();
        foreach($p_TripTypes as $c_Type)
        {
            $v_Item = new TripEvents();
            $v_Item->event_id = $p_EventId;
            $v_Item->trip_type_id = $c_Type;
            $v_Item->save();
        }
    }

    public static function getSelectedTypes($p_EventId)
    {
        return TripEvents::where('event_id', $p_EventId)->lists('trip_type_id')->toArray();
    }

    public static function getTripEvents($p_TripTypeId, $p_Quantity)
    {
        return TripEvents::join('event', 'event.id', '=', 'trip_type_events.event_id')
                         ->join('event_photo', 'event_photo.event_id', '=', 'event.id')
                         ->join('event_date', 'event_date.event_id', '=', 'event.id')
                         ->where('trip_type_events.trip_type_id', $p_TripTypeId)
                         ->where('event.revision_status_id', 4)
                         ->where('event_date.date', '>=', Carbon::now()->startOfDay()->format('Y-m-d H:i:s'))
                         ->where('event_photo.is_cover', 1)
                         ->select(['event.nome', 'event.slug', 'event.descricao_curta', 'event_photo.url'])
                         ->orderBy('event.destaque', 'desc')
                         ->orderBy('event_date.date')
                         ->groupBy('event_date.event_id')
                         ->take($p_Quantity)->get();
    }

//    public static function getTripAndCityDestinations($p_TripTypeId, $p_CityId, $p_Quantity)
//    {
//        return TripEvents::join('destination', 'destination.id', '=', 'trip_type_events.event_id')
//            ->join('destination_photo', 'destination_photo.event_id', '=', 'destination.id')
//            ->where('trip_type_events.trip_type_id', $p_TripTypeId)
//            ->where('destination.publico', 1)
//            ->where('destination.city_id', $p_CityId)
//            ->where('destination_photo.is_cover', 1)
//            ->select(['destination.nome', 'destination.slug', 'destination_photo.url', 'destination.descricao_curta'])
//            ->orderBy('destination.destaque', 'desc')
//            ->take($p_Quantity)->get();
//    }
}