<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
class EventByDates extends Model
{
    protected $table = 'event_by_date';

    //Relacionamento de 1 para 1
    public function Event(){
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function filter(Array $data, $paginate = 10) {
        return $this->where(function($query) use ($data) {

                            if (isset($data['id']))
                                $query->where('id', '=', $data['id']);
                            
                            if (isset($data['event']))
                                $query->where('name', 'like', "%$data[event]%");

                            if (isset($data['city']) && $data['city'] != 0)
                                $query->where('city_id', $data['city']);


                            if (isset($data['initialDate'])){
                                $query->where('start', '>=', $data['initialDate']);
                            }

                            if (isset($data['finallDate'])){
                                $query->where('start', '<=', $data['finalDate']);
                                $query->where('end', '<=', $data['finalDate']);
                            }
                        })
                        ->orderBy('start', 'asc')
                        ->paginate($paginate);
    }

    public static function getCalendarReportData($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_Date = $p_Data['period'];

        $v_Query = EventByDates::with('eventDates')
            ->with('categories')
            ->with('classification')
            ->join('city', 'event_by_date.city_id', '=', 'city.id')
            ->leftJoin('touristic_circuit', 'event_by_date.touristic_circuit_id', '=', 'touristic_circuit.id')
            ->join('event_date as start_date', 'start_date.event_id', '=', 'event_by_date.id')
            ->join('event_date as end_date', 'end_date.event_id', '=', 'event_by_date.id')
            ->leftJoin('event_selected_categories', 'event_selected_categories.event_id', '=', 'event_by_date.id')
            ->leftJoin('event_selected_classification', 'event_selected_classification.event_id', '=', 'event_by_date.id')
            ->leftJoin('event_cities', 'event_cities.event_id', '=', 'event_by_date.id')
            ->select('event_by_date.*', 'city.name as cidade', 'touristic_circuit.nome as circuito')
            ->groupBy('event_by_date.id')
            ->orderBy('start_date.date')
            ->orderBy('event_by_date.nome');

        if(array_key_exists('id', $p_Data)){
            $v_Query->where(function ($p_Query) use($p_Data){
                $p_Query->whereIn('event_cities.city_id', $p_Data['id'])
                    ->orWhereIn('event_by_date.city_id', $p_Data['id']);
            });
        }

        if($v_CircuitId != '')
            $v_Query->where('event_by_date.touristic_circuit_id', $v_CircuitId);

        if($v_RegionId != '') {
            $v_Query->join('destination', 'destination.city_id', '=', 'event_by_date.city_id')
                ->where('destination.region_id', $v_RegionId);
        }

        if(array_key_exists('category_id', $p_Data))
            $v_Query->whereIn('event_selected_categories.event_category_id', $p_Data['category_id']);
        
        if(array_key_exists('classification_id', $p_Data))
            $v_Query->whereIn('event_selected_classification.event_classification_id', $p_Data['classification_id']);
        

        if(array_key_exists('event_type_id', $p_Data)){
            $v_Query->where(function ($p_Query) use($p_Data){
                foreach ($p_Data['event_type_id'] as $c_EventType){
                    $p_Query->orWhere('event_by_date.tipo_evento', "LIKE", '%' . $c_EventType . '%');
                }
            });
        }

        if($v_Date != '') {
            $v_Query->where(function ($p_Query) use($v_Date, $p_Data){
                $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($v_Date, 0, 10))->startOfDay()->format('Y-m-d H:i:s');
                $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($v_Date, 13, 23))->endOfDay()->format('Y-m-d H:i:s');

                if (array_key_exists('period-ex', $p_Data) && $p_Data['period-ex'] == 'on'){
                    $p_Query->where('start', '>=', $v_StartDate)->where('end', '<=', $v_EndDate);
                } else {
                    $p_Query->whereRaw('start_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
                                    end_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
                                    "' . $v_StartDate . '" BETWEEN start_date.date AND end_date.date OR
                                    "' . $v_EndDate . '" BETWEEN start_date.date AND end_date.date');    
                }

            });
        }
        
        //Somente regitros aprovados
        $v_Query->where('revision_status_id', 4);


        //dd($v_Query->paginate(8));


        return $v_Query->distinct()->get();
    }

    public function eventDates()
    {
        return $this->hasMany('App\EventDate', 'event_id', 'id')->orderBy('date');
    }

    public function classification()
    {
         return $this->hasMany('App\EventSelectedClassification', 'event_id', 'id')
            ->join('event_classification', 'event_classification.id', '=', 'event_selected_classification.event_classification_id')
            ->select('event_selected_classification.event_id', 'event_classification.nome')
            ->orderBy('nome');       
    }

    public function categories()
    {
        return $this->hasMany('App\EventSelectedCategories', 'event_id', 'id')
            ->join('event_category', 'event_category.id', '=', 'event_selected_categories.event_category_id')
            ->select('event_selected_categories.event_id', 'event_category.nome_pt')
            ->orderBy('nome_pt');
    }

    public function cities()
    {
        return $this->hasMany('App\EventCities', 'event_id', 'id')
                    ->join('city', 'city.id', '=', 'event_cities.city_id')
                    ->select('event_cities.event_id', 'city.name')
                    ->orderBy('name');
    }

    public function hashtags()
    {
        return $this->hasMany('App\EventHashtag', 'event_id', 'id')
                    ->join('hashtag', 'hashtag.id', '=', 'event_hashtag.hashtag_id')
                    ->select('event_hashtag.event_id', 'hashtag.name')
                    ->orderBy('name');
    }
}
