<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

abstract class BaseInventoryModel extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    protected $revisionCreationsEnabled = true;
    protected $revisionEnabled = true;
    protected $revisionCleanup = true; // Remove revisões antigas (funciona apenas quando usado com $historyLimit)
    protected $historyLimit = 500; // Mantém um máximo de 500 alterações a qualquer momento, enquanto limpa as revisões antigas.

    protected $revisionFormattedFieldNames = array(
        'city_id' => 'municipio',
        'district_id' => 'distrito',
        'type_id' => 'tipo',
        'sub_type_id' => 'subtipo',
        'economic_activity_id' => 'atividade_economica',
    );

    protected $guarded = [];

    public static $m_Rules = array
    (
    );

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function startFormFieldIndexing($p_Level = 0)
    {
        Session::put('form_field_index_level_' . $p_Level, 1);
    }

    public static function incrementFormFieldIndex($p_Level = 0)
    {
        Session::put('form_field_index_level_' . $p_Level, Session::get('form_field_index_level_' . $p_Level) + 1);
        for($c_Level = $p_Level + 1; $c_Level < 7; $c_Level++){
            Session::forget('form_field_index_level_' . $c_Level);
        }
    }

    public static function getFormFieldIndex($p_Level = 0)
    {
        $v_Index = '';
        for($c_Level = 0; $c_Level <= $p_Level; $c_Level++){
            $v_LevelIndex = Session::get('form_field_index_level_' . $c_Level);
            if($v_LevelIndex == null){
                BaseInventoryModel::startFormFieldIndexing($c_Level);
                $v_LevelIndex = 1;
            }
            if($c_Level < $p_Level)
                $v_LevelIndex--;
            $v_Index .= $v_LevelIndex . '.';
        }
        return $v_Index . ' ';
    }

    public static function getFormFieldIndexAndIncrement($p_Level = 0)
    {
        $v_Index = BaseInventoryModel::getFormFieldIndex($p_Level);
        BaseInventoryModel::incrementFormFieldIndex($p_Level);
        return $v_Index;
    }
}