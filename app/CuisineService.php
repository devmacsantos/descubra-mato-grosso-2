<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class CuisineService extends Model
{
    public $timestamps = false;
    protected $table = 'cuisine_service';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:200'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:200'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_CuisineService = CuisineService::findOrNew($p_Id);
        $v_CuisineService->nome_pt = $p_Name;
        $v_CuisineService->save();
    }

    public static function getList()
    {
        return CuisineService::orderBy('nome_pt')->lists('nome_pt', 'id')->toArray();
    }

    public static function getLanguageList($p_Lang)
    {
        return CuisineService::orderBy('nome_' . $p_Lang)->lists('nome_' . $p_Lang, 'id')->toArray();
    }
}