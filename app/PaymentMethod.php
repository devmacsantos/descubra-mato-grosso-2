<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    public $timestamps = false;
    protected $table = 'payment_method';
    protected $guarded = [];

    public static function post($p_Id, $p_Data)
    {
        PaymentMethod::updateOrCreate(['id' => $p_Id], $p_Data['idioma']);
    }

    public static function getList()
    {
        return PaymentMethod::orderBy('nome_pt')->lists('nome_pt', 'id')->toArray();
    }

    public static function getLanguageList($p_Lang)
    {
        return PaymentMethod::orderBy('nome_' . $p_Lang)->lists('nome_' . $p_Lang, 'id')->toArray();
    }
}