<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Road extends Model
{
    public $timestamps = false;
    protected $table = 'road';
    protected $guarded = [];

    public static function post($p_Id, $p_Data)
    {
        Road::updateOrCreate(['id' => $p_Id], $p_Data);
    }

    public static function getListByType($p_Type)
    {
        return Road::where('tipo', $p_Type)->orderBy('nome')->lists('nome', 'id')->toArray();
    }
}