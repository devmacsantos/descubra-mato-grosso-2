<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserType extends Model
{
    public $timestamps = false;

    protected $table = 'user_type';

	protected $guarded = [];

    public static $m_Rules = array
    (
        'senha' => 'required|min:6|confirmed'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric',
        'senha' => 'min:6|confirmed'
    );

    public static function getList()
    {
        return UserType::orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function isAdmin()
    {
        return Auth::check() && Auth::user()->user_type_id == 1;
    }

    public static function isMaster()
    {
        return Auth::check() && Auth::user()->user_type_id == 2;
    }

    public static function isComunicacao()
    {
        return Auth::check() && Auth::user()->user_type_id == 3;
    }

    public static function isMunicipio()
    {
        return Auth::check() && Auth::user()->user_type_id == 4;
    }

    public static function isCircuito()
    {
        return Auth::check() && Auth::user()->user_type_id == 5;
    }

    public static function isTrade()
    {
        return Auth::check() && Auth::user()->user_type_id == 6;
    }

    public static function isParceiro()
    {
        return Auth::check() && Auth::user()->user_type_id == 7;
    }

    public static function isFiscalizador()
    {
        return Auth::check() && Auth::user()->user_type_id == 8;
    }
}
