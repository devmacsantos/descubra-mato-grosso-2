<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserTradeItem extends Model
{
    public $timestamps = false;

    protected $table = 'user_trade_item';

	protected $guarded = [];

    public static function getTradeItems($p_Type)
    {
        return UserTradeItem::where('type',$p_Type)
            ->where('authorized',1)
            ->lists('id')->toArray();
    }

    public static function getUserTradeItems($p_Type)
    {
        return UserTradeItem::where('user_id',Auth::id())->where('type',$p_Type)
            ->where('authorized',1)
            ->lists('id')->toArray();
    }

    public static function getUserPendingItems($p_Type)
    {
        return UserTradeItem::where('user_id',Auth::id())->where('type',$p_Type)
            ->where('authorized',0)
            ->lists('id')->toArray();
    }

    public static function getPendingItems($p_Type)
    {
        return UserTradeItem::join('user', 'user.id', '=', 'user_trade_item.user_id')
            ->where('type',$p_Type)
            ->where('authorized',0)
            ->select('user_trade_item.*', 'user.name', 'user.email')
            ->get();
    }

    public static function postUserTradeItem($p_Type, $p_Id)
    {
        UserTradeItem::create([
            'user_id' => Auth::id(),
            'type' => $p_Type,
            'id' => $p_Id
        ]);
    }

    public static function requestTradeItemOwnership($p_Type, $p_Id)
    {
        UserTradeItem::create([
            'user_id' => Auth::id(),
            'type' => $p_Type,
            'id' => $p_Id,
            'authorized' => 0
        ]);
    }

    public static function approveOwnershipRequest($p_Type, $p_Id, $p_UserId)
    {
        UserTradeItem::where('type',$p_Type)->where('id',$p_Id)->where('user_id',$p_UserId)
            ->where('authorized',0)->update(['authorized' => 1]);

        if($p_Type == 'B1')
            $v_Item = AccomodationService::find($p_Id);
        else if($p_Type == 'B2')
            $v_Item = FoodDrinksService::find($p_Id);
        else if($p_Type == 'B3')
            $v_Item = TourismAgencyService::find($p_Id);
        else if($p_Type == 'B4')
            $v_Item = TourismTransportationService::find($p_Id);
        else if($p_Type == 'B6')
            $v_Item = RecreationService::find($p_Id);
        else if($p_Type == 'EV'){
            $v_Item = Event::find($p_Id);
            $v_Item->update(['created_by'=>$p_UserId]); 
        }
        $v_Subject = 'Pedido de posse de item aprovado';
        $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">O seu pedido de posse do item ' . ($p_Type == 'EV') ? $v_Item->nome : $v_Item->nome_fantasia . ' foi aprovado.</p>';
        $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';

        $v_User = User::find($p_UserId);
        if($v_User->active){
            Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                function ($message) use ($v_User, $v_Subject)
                {
                    $message->to($v_User->email, $v_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                });
        }
    }

    public static function rejectOwnershipRequest($p_Type, $p_Id, $p_UserId)
    {
        UserTradeItem::where('type',$p_Type)->where('id',$p_Id)->where('user_id',$p_UserId)
            ->where('authorized',0)->delete();

        if($p_Type == 'B1')
            $v_Item = AccomodationService::find($p_Id);
        else if($p_Type == 'B2')
            $v_Item = FoodDrinksService::find($p_Id);
        else if($p_Type == 'B3')
            $v_Item = TourismAgencyService::find($p_Id);
        else if($p_Type == 'B4')
            $v_Item = TourismTransportationService::find($p_Id);
        else if($p_Type == 'B6')
            $v_Item = RecreationService::find($p_Id);
        else if($p_Type == 'EV')
            $v_Item = Event::find($p_Id);

        $v_Subject = 'Pedido de posse de item rejeitado';
        $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">O seu pedido de posse do item ' . ($p_Type == 'EV') ? $v_Item->nome : $v_Item->nome_fantasia . ' foi rejeitado.</p>';

        $v_User = User::find($p_UserId);
        if($v_User->active){
            Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                function ($message) use ($v_User, $v_Subject)
                {
                    $message->to($v_User->email, $v_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                });
        }
    }

    public static function deleteUserTradeItem($p_Type, $p_Id)
    {
        UserTradeItem::where('type',$p_Type)->where('id',$p_Id)->delete();
    }

    public static function userAlreadyRequestedItem($p_Type, $p_Id)
    {
        return UserTradeItem::where('type',$p_Type)->where('id',$p_Id)->where('user_id',Auth::id())->where('authorized',0)->count();
    }

    public static function itemHasUser($p_Type, $p_Id)
    {
        return UserTradeItem::where('type',$p_Type)->where('id',$p_Id)->where('authorized',1)->count();
    }

    public static function itemUsers($p_Type, $p_Id)
    {
        return UserTradeItem::where('type',$p_Type)->where('id',$p_Id)->lists('user_id')->toArray();
    }

    public static function itemApprovedUsers($p_Type, $p_Id)
    {
        return UserTradeItem::where('type',$p_Type)->where('id',$p_Id)->where('authorized',1)->lists('user_id')->toArray();
    }







    public static function sendExpiringTradeNotifications()
    {
        $v_TokenUrl = 'http://www.cadastur.turismo.gov.br/cadastur/rest/autenticacao/obterToken?login=' . Parameter::getParameterByKey('cadastur_login') . '&' . Parameter::getParameterByKey('cadastur_senha') . '&nuPerfil=710';
        $v_TokenData = json_decode(file_get_contents($v_TokenUrl), true);
        $v_Token = $v_TokenData['token'];
        $v_Context = stream_context_create([
            'http' => ['header'  => "Authorization: " . $v_Token]
        ]);

        UserTradeItem::processExpiringCadasturItems(AccomodationService::where('user_trade_item.type', 'B1'), 'accomodation_service', $v_Context);
        UserTradeItem::processExpiringItems(FoodDrinksService::where('user_trade_item.type', 'B2'), 'food_drinks_service');
        UserTradeItem::processExpiringCadasturItems(TourismAgencyService::where('user_trade_item.type', 'B3'), 'tourism_agency_service', $v_Context);
        UserTradeItem::processExpiringCadasturItems(TourismTransportationService::where('user_trade_item.type', 'B4'), 'tourism_transportation_service', $v_Context);
        UserTradeItem::processExpiringItems(RecreationService::where('user_trade_item.type', 'B6'), 'recreation_service');
    }

    public static function processExpiringCadasturItems($p_InitialQuery, $p_TableName, $p_Context)
    {
        $v_Items = $p_InitialQuery->join('user_trade_item', 'user_trade_item.id', '=', $p_TableName . 'id')
                              ->join('user', 'user_trade_item.user_id', '=', 'user.id')
                              ->where('user_trade_item.authorized', 1)
                              ->where('user.active', 1)
                              ->where(function($q){
                                  $q->where('validade_cadastur', 'like', "%" . Carbon::now()->addDays(30)->format('Y-m-d ') . "%")
                                    ->orWhere('validade_cadastur', 'like', "%" . Carbon::now()->addDays(10)->format('Y-m-d ') . "%")
                                    ->orWhere('validade_cadastur', 'like', "%" . Carbon::now()->addDays(5)->format('Y-m-d ') . "%")
                                    ->orWhere('validade_cadastur', 'like', "%" . Carbon::now()->addDays(0)->format('Y-m-d ') . "%")
                                    ->orWhereNull('validade_cadastur');
                              })
                              ->orderBy('validade_cadastur')
                              ->select($p_TableName . '.*', 'user.name as user_name', 'user.email as user_email')->get();

        $v_ValidityBaseUrl = 'http://www.cadastur.turismo.gov.br/cadastur/rest/integracaoRegional/validadeCadastro?coCnpj=';
        $v_CadasturExpiresToday = Carbon::now()->addDays(0)->format('Y-m-d ');
        $v_CadasturExpires5Days = Carbon::now()->addDays(5)->format('Y-m-d ');
        $v_CadasturExpires10Days = Carbon::now()->addDays(10)->format('Y-m-d ');
        $v_CadasturExpires30Days = Carbon::now()->addDays(30)->format('Y-m-d ');

        foreach($v_Items as $c_Item) {
            $v_ValidityUrl = $v_ValidityBaseUrl . $c_Item->cnpj . '&nuTipoAtividade=' . $c_Item->tipo_atividade_cadastur;
            $v_ValidityData = json_decode(file_get_contents($v_ValidityUrl, false, $p_Context), true);

            if($v_ValidityData['status']) {
                if(!empty($v_ValidityData['dto'])) {
                    $v_Validade = $v_ValidityData['dto']['dtValidade'];
                    if(strpos($c_Item->validade_cadastur, $v_Validade) === false) {
                        $c_Item->validade_cadastur = $v_Validade . ' 00:00:00';
                        $c_Item->save();
                    }
                }
            }
            if($c_Item->validade_cadastur == null) {
                $c_Item->validade_cadastur = Carbon::yesterday()->format('Y-m-d H:i:s');
                $c_Item->save();
            }
            $v_Message = null;
            $v_CNPJ = substr($c_Item->cnpj, 0, 2) . '.' . substr($c_Item->cnpj, 2, 3) . '.' . substr($c_Item->cnpj, 5, 3) . '/' . substr($c_Item->cnpj, 8, 4) . '-' . substr($c_Item->cnpj, 12, 2);
            if(strpos($c_Item->validade_cadastur, $v_CadasturExpiresToday) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar hoje no CADASTUR!';
            else if(strpos($c_Item->updated_at, $v_CadasturExpires5Days) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar em 5 dias no CADASTUR!';
            else if(strpos($c_Item->updated_at, $v_CadasturExpires10Days) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar em 10 dias no CADASTUR!';
            else if(strpos($c_Item->updated_at, $v_CadasturExpires30Days) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar em 30 dias no CADASTUR!';

            if($v_Message != null) {
                $v_CallToAction = 'Atualize os seus dados para permanecer ativo no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.';
                Mail::send('emails.expiringTrade', ['p_Msg' => $v_Message, 'p_CallToAction' => $v_CallToAction],
                    function ($message) use ($c_Item)
                    {
                        $message->to($c_Item->user_email, $c_Item->user_name)->subject('Descubra Mato Grosso - Vencimento de cadastro');
                    });
            }
        }
    }

    public static function processExpiringItems($p_InitialQuery, $p_TableName)
    {
        $v_Items = $p_InitialQuery->join('user_trade_item', 'user_trade_item.id', '=', $p_TableName . 'id')
                              ->join('user', 'user_trade_item.user_id', '=', 'user.id')
                              ->where('user_trade_item.authorized', 1)
                              ->where('user.active', 1)
                              ->where(function($q) use($p_TableName){
                                  $q->where($p_TableName . '.updated_at', 'like', "%" . Carbon::now()->subDays(150 + 30)->format('Y-m-d ') . "%")
                                    ->orWhere($p_TableName . '.updated_at', 'like', "%" . Carbon::now()->subDays(150 + 10)->format('Y-m-d ') . "%")
                                    ->orWhere($p_TableName . '.updated_at', 'like', "%" . Carbon::now()->subDays(150 + 5)->format('Y-m-d ') . "%")
                                    ->orWhere($p_TableName . '.updated_at', 'like', "%" . Carbon::now()->subDays(150 + 0)->format('Y-m-d ') . "%");
                              })
                              ->orderBy($p_TableName . '.updated_at')
                              ->select($p_TableName . '.*', 'user.name as user_name', 'user.email as user_email')->get();

        $v_ExpiresToday = Carbon::now()->subDays(150 + 30)->format('Y-m-d ');
        $v_Expires5Days = Carbon::now()->subDays(150 + 10)->format('Y-m-d ');
        $v_Expires10Days = Carbon::now()->subDays(150 + 5)->format('Y-m-d ');
        $v_Expires30Days = Carbon::now()->subDays(150 + 0)->format('Y-m-d ');

        foreach($v_Items as $c_Item) {
            $v_Message = null;
            $v_CNPJ = substr($c_Item->cnpj, 0, 2) . '.' . substr($c_Item->cnpj, 2, 3) . '.' . substr($c_Item->cnpj, 5, 3) . '/' . substr($c_Item->cnpj, 8, 4) . '-' . substr($c_Item->cnpj, 12, 2);
            if(strpos($c_Item->updated_at, $v_ExpiresToday) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar hoje!';
            else if(strpos($c_Item->updated_at, $v_Expires5Days) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar em 5 dias!';
            else if(strpos($c_Item->updated_at, $v_Expires10Days) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar em 10 dias!';
            else if(strpos($c_Item->updated_at, $v_Expires30Days) !== false)
                $v_Message = 'O cadastro do CNPJ ' . $v_CNPJ . ' vai expirar em 30 dias!';

            if($v_Message != null) {
                $v_CallToAction = 'Atualize os seus dados para permanecer ativo no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.';
                Mail::send('emails.expiringTrade', ['p_Msg' => $v_Message, 'p_CallToAction' => $v_CallToAction],
                    function ($message) use ($c_Item)
                    {
                        $message->to($c_Item->user_email, $c_Item->user_name)->subject('Descubra Mato Grosso - Vencimento de cadastro');
                    });
            }
        }
    }


    public static function processExpiredTrade()
    {
        $v_TokenUrl = 'http://www.cadastur.turismo.gov.br/cadastur/rest/autenticacao/obterToken?login=' . Parameter::getParameterByKey('cadastur_login') . '&' . Parameter::getParameterByKey('cadastur_senha') . '&nuPerfil=710';
        $v_TokenData = json_decode(file_get_contents($v_TokenUrl), true);
        $v_Token = $v_TokenData['token'];
        $v_Context = stream_context_create([
            'http' => ['header'  => "Authorization: " . $v_Token]
        ]);

        UserTradeItem::processExpiredCadasturItems(AccomodationService::where('user_trade_item.type', 'B1'), 'accomodation_service', $v_Context, 'B1');
        UserTradeItem::processExpiredItems(FoodDrinksService::where('user_trade_item.type', 'B2'), 'food_drinks_service', 'B2');
        UserTradeItem::processExpiredCadasturItems(TourismAgencyService::where('user_trade_item.type', 'B3'), 'tourism_agency_service', $v_Context, 'B3');
        UserTradeItem::processExpiredCadasturItems(TourismTransportationService::where('user_trade_item.type', 'B4'), 'tourism_transportation_service', $v_Context, 'B4');
        UserTradeItem::processExpiredItems(RecreationService::where('user_trade_item.type', 'B6'), 'recreation_service', 'B6');
    }

    public static function processExpiredCadasturItems($p_InitialQuery, $p_TableName, $p_Context, $p_Type)
    {
        $v_Today = Carbon::now()->startOfDay();
        $v_Items = $p_InitialQuery->join('user_trade_item', 'user_trade_item.id', '=', $p_TableName . 'id')
                                  ->where('user_trade_item.authorized', 1)
                                  ->where('validade_cadastur', '<', $v_Today->format('Y-m-d H:i:s'))
                                  ->select($p_TableName . '.*')->get();

        $v_ValidityBaseUrl = 'http://www.cadastur.turismo.gov.br/cadastur/rest/integracaoRegional/validadeCadastro?coCnpj=';

        foreach($v_Items as $c_Item) {
            $v_ValidityUrl = $v_ValidityBaseUrl . $c_Item->cnpj . '&nuTipoAtividade=' . $c_Item->tipo_atividade_cadastur;
            $v_ValidityData = json_decode(file_get_contents($v_ValidityUrl, false, $p_Context), true);

            if($v_ValidityData['status']) {
                if(!empty($v_ValidityData['dto'])) {
                    $v_Validade = $v_ValidityData['dto']['dtValidade'];
                    if(strpos($c_Item->validade_cadastur, $v_Validade) === false) {
                        $c_Item->validade_cadastur = $v_Validade . ' 00:00:00';
                    }
                }
            }
            $v_Validade = Carbon::createFromFormat('Y-m-d H:i:s', $c_Item->validade_cadastur);
            if($v_Validade->lt($v_Today)) {
                $c_Item->revision_status_id = 6;
                if($c_Item->data_publicacao != null) {
                    Attraction::unpublish($c_Item->id, $p_Type);
                    $c_Item->data_publicacao = null;
                }
            }
            else if($c_Item->revision_status_id == 6) {
                $c_Item->revision_status_id = 1;
            }
            $c_Item->save();
        }
    }

    public static function processExpiredItems($p_InitialQuery, $p_TableName, $p_Type)
    {
        $v_Items = $p_InitialQuery->join('user_trade_item', 'user_trade_item.id', '=', $p_TableName . 'id')
                                  ->where('user_trade_item.authorized', 1)
                                  ->where('revision_status_id', '!=', 6)
                                  ->where($p_TableName . '.updated_at', '<', Carbon::now()->startOfDay()->subDays(180)->format('Y-m-d H:i:s'))
                                  ->select($p_TableName . '.*')->get();

        foreach($v_Items as $c_Item) {
            $c_Item->revision_status_id = 6;
            if($c_Item->data_publicacao != null) {
                Attraction::unpublish($c_Item->id, $p_Type);
                $c_Item->data_publicacao = null;
            }
            $c_Item->save();
        }
    }
}
