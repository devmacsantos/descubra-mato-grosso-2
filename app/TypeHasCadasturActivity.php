<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeHasCadasturActivity extends Model
{
    public $timestamps = false;
    protected $table = 'type_has_cadastur_activity';
}