<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsHome extends Model
{
    public $timestamps = false;
    protected $table = 'items_home';

    public static $m_Rules = array
    (
        'foto_capa' => 'required|image',
        'perfil_instagram' => 'required|min:1|max:100'
    );

    public static $m_RulesEdit = array
    (
        'foto_capa' => 'image',
        'perfil_instagram' => 'required|min:1|max:100'
    );

    public static function post($p_CoverPhoto, $p_Data)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/paginas/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/home/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_PhotoUrl = null;
        if($p_CoverPhoto != null)
        {
            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $p_CoverPhoto->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $p_CoverPhoto->encode('jpg')->save($v_Path . $v_PhotoName);
            $v_PhotoUrl = url('/imagens/paginas/home/' . $v_PhotoName);
        }

        foreach($p_Data['language'] as $c_Index => $p_Language)
        {
            $v_ItemsHome = ItemsHome::findOrNew($p_Data['id'][$c_Index]);
            $v_ItemsHome->language = $p_Language;
            $v_ItemsHome->titulo_o_que_fazer = $p_Data['titulo_o_que_fazer'][$c_Index];
            $v_ItemsHome->titulo_destinos = $p_Data['titulo_destinos'][$c_Index];
            $v_ItemsHome->titulo_instagram = $p_Data['titulo_instagram'][$c_Index];
            $v_ItemsHome->texto_instagram = $p_Data['texto_instagram'][$c_Index];
            $v_ItemsHome->perfil_instagram = $p_Data['perfil_instagram'];
            $v_ItemsHome->titulo_aplicativos = $p_Data['titulo_aplicativos'][$c_Index];

            if($v_PhotoUrl != null)
            {
                if($c_Index == 0 && $v_ItemsHome != null)
                {
                    $v_OldFileName = explode('/', $v_ItemsHome->foto_capa_url);
                    $v_OldFileName = array_pop($v_OldFileName);
                    \File::delete($v_Path . $v_OldFileName);
                }
                $v_ItemsHome->foto_capa_url = $v_PhotoUrl;
            }

            $v_ItemsHome->save();
        }
    }

    public static function getByLanguage($p_Language)
    {
        return ItemsHome::where('language', $p_Language)->first();
    }
}