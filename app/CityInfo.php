<?php
namespace App;

use App\Http\Controllers\BaseController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CityInfo extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    protected $revisionCreationsEnabled = true;
    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.

//    protected $revisionFormattedFields = array(
//        'title'  => 'string:<strong>%s</strong>',
//        'public' => 'boolean:No|Yes',
//        'modified' => 'datetime:m/d/Y g:i A',
//        'deleted_at' => 'isEmpty:Active|Deleted'
//    );

    protected $table = 'city_info';
    protected $guarded = [];

    public static function getDT($p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = CityInfo::join('city', 'city.id', '=', 'city_info.city_id')->join('revision_status', 'revision_status.id', '=', 'city_info.revision_status_id')
            ->select(DB::raw('SQL_CALC_FOUND_ROWS city_info.id, city.name as municipio, city_info.created_at, city_info.updated_at, revision_status.name as status'));

        if(UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());

        if($p_City != '')
            $v_Query->where('city.name', 'LIKE', '%' . $p_City . '%');

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('city_info.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('city_info.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('city_info.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('city_info.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Status != '')
            $v_Query->where('city_info.revision_status_id',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('city_info.created_at', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('city_info.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                    '<a href="' . url('admin/inventario/informacoes/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/inventario/informacoes/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                    ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/informacoes/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = CityInfo::getTotalRows();

        if(UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = CityInfo::where('city_id', Auth::user()->city_id)->count();
        else if(UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = CityInfo::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else
            $v_DataTableAjax->recordsTotal = CityInfo::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function getCityInfoCityIds()
    {
        return CityInfo::lists('city_id')->toArray();
    }

    public static function post($p_Id, $p_Data)
    {
        $v_CityData = $p_Data['formulario'];

        if(!array_key_exists('possui_orgao_turismo', $v_CityData))
            $v_CityData['possui_orgao_turismo'] = 0;

        if(array_key_exists('meses_secos', $p_Data) && $p_Data['meses_secos'] != '')
        {
            $v_DryMonths = $p_Data['meses_secos'];
            $v_Months = '';
            foreach($v_DryMonths as $c_Month)
                $v_Months .= $c_Month . ';';
            $v_CityData['meses_secos'] = $v_Months;
        }

        if(array_key_exists('meses_chuvosos', $p_Data) && $p_Data['meses_chuvosos'] != '')
        {
            $v_RainyMonths = $p_Data['meses_chuvosos'];
            $v_Months = '';
            foreach($v_RainyMonths as $c_Month)
                $v_Months .= $c_Month . ';';
            $v_CityData['meses_chuvosos'] = $v_Months;
        }

        $v_CarrierServices = $p_Data['servicos_telefonia_movel'];
        $v_Services = '';
        foreach($v_CarrierServices as $c_CarrierService)
            $v_Services .= $c_CarrierService . ';';
        $v_CityData['servicos_telefonia_movel'] = $v_Services;

        array_walk($v_CityData, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_PreviousStatus = 0;
        if($p_Id != null)
            $v_PreviousStatus = CityInfo::find($p_Id)->revision_status_id;

        $v_Item = CityInfo::updateOrCreate(['id' => $p_Id], $v_CityData);

        PermanentEvent::post($v_Item->id, $p_Data['eventos_permanentes']);

        if($p_Id != null && $v_PreviousStatus != 4){
            $v_City = City::find($v_Item->city_id)->name;
            $v_Subject = null;
            $v_Message = null;
            if($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item A1 - ' . $v_City . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            else if($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item A1 - ' . $v_City . ' foram rejeitadas.</p>';
                if(!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'A1', $v_Item->id);
                foreach($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                        function ($message) use ($c_User, $v_Subject)
                        {
                            $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                        });
                }
            }
        }
    }

    public static function getReport($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_Regionalized = $p_Data['regionalizado'];

        $v_Query = CityInfo::leftJoin('city', 'city.id', '=', 'city_info.city_id')
                           ->leftJoin('touristic_circuit_cities', 'city_info.city_id', '=', 'touristic_circuit_cities.city_id')
                           ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                           ->leftJoin('destination', 'destination.city_id', '=', 'city_info.city_id')
                           ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                           ->leftJoin('political_party', 'city_info.prefeito_partido_id', '=', 'political_party.id')
                           ->selectRaw('city_info.*, city.name as cidade, touristic_circuit.nome as circuito, region.name as regiao, political_party.abbreviation as prefeito_partido')
                           ->groupBy('city_info.id');

        if(array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('city_info.city_id', $p_Data['city_id']);

        if($v_CircuitId != '')
            $v_Query->where('touristic_circuit.id', $v_CircuitId);

        if($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        if($v_Regionalized != '') {
            if($v_Regionalized == 1)
                $v_Query->whereNotNull('touristic_circuit.id');
            else
                $v_Query->whereNull('touristic_circuit.id');
        }

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioA1.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->prefeitura_endereco_cep);
                        array_push($v_Data, $c_Result->prefeitura_endereco_bairro);
                        array_push($v_Data, $c_Result->prefeitura_endereco_logradouro);
                        array_push($v_Data, $c_Result->prefeitura_endereco_numero);
                        array_push($v_Data, $c_Result->prefeitura_endereco_complemento);
                        array_push($v_Data, $c_Result->prefeitura_telefone);
                        array_push($v_Data, $c_Result->prefeitura_site);
                        array_push($v_Data, $c_Result->prefeitura_email);
                        array_push($v_Data, $c_Result->registro_estadual);
                        array_push($v_Data, $c_Result->latitude);
                        array_push($v_Data, $c_Result->longitude);
                        array_push($v_Data, $c_Result->populacao_total);
                        array_push($v_Data, $c_Result->populacao_urbana);
                        array_push($v_Data, $c_Result->populacao_rural);
                        array_push($v_Data, $c_Result->area_total);
                        $v_CitiesJSON = json_decode($c_Result->municipios_limitrofes,1);
                        $v_Cities = '';
                        foreach($v_CitiesJSON as $c_Index => $c_City)
                            $v_Cities .= ($c_Index > 0 ? ', ' : '') . $c_City['nome'];
                        array_push($v_Data, $v_Cities);
                        array_push($v_Data, $c_Result->temperatura_media);
                        array_push($v_Data, $c_Result->temperatura_min);
                        array_push($v_Data, $c_Result->temperatura_max);
                        $v_DryMonths = explode(';', $c_Result->meses_secos);
                        $v_Months = '';
                        foreach($v_DryMonths as $c_Index => $c_Month){
                            if(!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);
                        $v_WetMonths = explode(';', $c_Result->meses_chuvosos);
                        $v_Months = '';
                        foreach($v_WetMonths as $c_Index => $c_Month) {
                            if(!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);

                        $v_ClimaJSON = json_decode($c_Result->clima,1);
                        $v_ClimaJSON = is_array($v_ClimaJSON) ? $v_ClimaJSON : [];
                        $v_Clima = '';
                        foreach($v_ClimaJSON as $c_Index => $c_Clima)
                            $v_Clima .= ($c_Index > 0 ? ', ' : '') . $c_Clima['nome'];
                        array_push($v_Data, $v_Clima);

                        array_push($v_Data, $c_Result->altitude);
                        $v_ActivitiesJSON = json_decode($c_Result->principais_atividades_economicas,1);
                        $v_ActivitiesJSON = $v_ActivitiesJSON == null ? [] : $v_ActivitiesJSON;
                        $v_Activities = '';
                        foreach($v_ActivitiesJSON as $c_Index => $c_Activity)
                            $v_Activities .= ($c_Index > 0 ? ', ' : '') . $c_Activity['nome'];
                        array_push($v_Data, $v_Activities);
                        $c_Result->servicos_telefonia_movel = preg_replace('/;$/', '', $c_Result->servicos_telefonia_movel);
                        array_push($v_Data, str_replace(';', ', ', $c_Result->servicos_telefonia_movel));
                        array_push($v_Data, $c_Result->servicos_telefonia_movel_outros);
                        array_push($v_Data, $c_Result->postos_telefonicos == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->emissora_radio == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->prefeito_nome);
                        array_push($v_Data, $c_Result->prefeito_telefone);
                        array_push($v_Data, $c_Result->prefeito_email);
                        array_push($v_Data, $c_Result->prefeito_partido);
                        array_push($v_Data, $c_Result->prefeitura_funcionarios_fixos);
                        array_push($v_Data, $c_Result->prefeitura_funcionarios_temporarios);
                        array_push($v_Data, $c_Result->prefeitura_funcionarios_deficiencia);
                        array_push($v_Data, $c_Result->prefeitura_funcionarios_total);
                        array_push($v_Data, $c_Result->prefeitura_nomes_departamentos);
                        array_push($v_Data, $c_Result->possui_orgao_turismo == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->orgao_turismo_tipo);
                        array_push($v_Data, $c_Result->orgao_turismo_nome);
                        array_push($v_Data, $c_Result->orgao_turismo_titular);
                        array_push($v_Data, $c_Result->orgao_turismo_orcamento == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->orgao_turismo_orcamento_ultimo_ano);
                        array_push($v_Data, $c_Result->orgao_turismo_orcamento_ano_referencia);
                        $v_ActionsJSON = empty($c_Result->orgao_turismo_acoes) ? [] : json_decode($c_Result->orgao_turismo_acoes,1);
                        $v_Actions = '';
                        foreach($v_ActionsJSON as $c_Index => $c_Action)
                            $v_Actions .= ($c_Index > 0 ? ', ' : '') . $c_Action['nome'];
                        array_push($v_Data, $v_Actions);
                        array_push($v_Data, $c_Result->orgao_turismo_acoes_outras);
                        array_push($v_Data, $c_Result->orgao_turismo_endereco_cep);
                        array_push($v_Data, $c_Result->orgao_turismo_endereco_bairro);
                        array_push($v_Data, $c_Result->orgao_turismo_endereco_logradouro);
                        array_push($v_Data, $c_Result->orgao_turismo_endereco_numero);
                        array_push($v_Data, $c_Result->orgao_turismo_endereco_complemento);
                        array_push($v_Data, $c_Result->orgao_turismo_site);
                        array_push($v_Data, $c_Result->orgao_turismo_email);
                        array_push($v_Data, $c_Result->orgao_turismo_telefone);
                        array_push($v_Data, $c_Result->legislacao_lei_organica);
                        array_push($v_Data, $c_Result->legislacao_criacao_conselho_turismo);
                        array_push($v_Data, $c_Result->legislacao_ocupacao_solo);
                        array_push($v_Data, $c_Result->legislacao_protecao_ambiental);
                        array_push($v_Data, $c_Result->legislacao_apoio_cultura);
                        array_push($v_Data, $c_Result->legislacao_incentivo_fiscal_turismo);
                        array_push($v_Data, $c_Result->legislacao_regulamentacao_turismo);
                        array_push($v_Data, $c_Result->legislacao_plano_desenvolvimento_turismo);
                        array_push($v_Data, $c_Result->legislacao_plano_diretor == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->legislacao_outras);
                        $v_AbastecimentoJSON = empty($c_Result->agua_abastecimento) ? [] : json_decode($c_Result->agua_abastecimento,1);
                        $v_Abastecimentos = '';
                        foreach($v_AbastecimentoJSON as $c_Index => $c_Abastecimento)
                            $v_Abastecimentos .= ($c_Index > 0 ? ';\n' : '') . $c_Abastecimento['nome'] . ' - Empresa: ' . $c_Abastecimento['empresa'] . ' - Domicílios atendidos: ' . str_replace('.', ',', $c_Abastecimento['percentual_atendimento']) . '%';
                        array_push($v_Data, stripcslashes($v_Abastecimentos));

                        $v_AbastecimentoJSON = empty($c_Result->esgotamento) ? [] : json_decode($c_Result->esgotamento,1);
                        $v_Abastecimentos = '';
                        foreach($v_AbastecimentoJSON as $c_Index => $c_Abastecimento)
                            $v_Abastecimentos .= ($c_Index > 0 ? ';\n' : '') . $c_Abastecimento['nome'] . ' - Empresa: ' . $c_Abastecimento['empresa'] . ' - Domicílios atendidos: ' . str_replace('.', ',', $c_Abastecimento['percentual_atendimento']) . '%';
                        array_push($v_Data, stripcslashes($v_Abastecimentos));

                        $v_AbastecimentoJSON = empty($c_Result->energia_abastecimento) ? [] : json_decode($c_Result->energia_abastecimento,1);
                        $v_Abastecimentos = '';
                        foreach($v_AbastecimentoJSON as $c_Index => $c_Abastecimento)
                            $v_Abastecimentos .= ($c_Index > 0 ? ';\n' : '') . $c_Abastecimento['nome'] . ' - Empresa: ' . $c_Abastecimento['empresa'] . ' - Domicílios atendidos: ' . str_replace('.', ',', $c_Abastecimento['percentual_atendimento']) . '%';
                        array_push($v_Data, stripcslashes($v_Abastecimentos));

                        $v_AbastecimentoJSON = empty($c_Result->destinacao_lixo) ? [] : json_decode($c_Result->destinacao_lixo,1);
                        $v_Abastecimentos = '';
                        foreach($v_AbastecimentoJSON as $c_Index => $c_Abastecimento)
                            $v_Abastecimentos .= ($c_Index > 0 ? ';\n' : '') . $c_Abastecimento['nome'] . ' - Empresa: ' . $c_Abastecimento['empresa'] . ' - Domicílios atendidos: ' . str_replace('.', ',', $c_Abastecimento['percentual_atendimento']) . '%' . ' - Tratamento de reciclagem: ' . $c_Abastecimento['reciclagem'];
                        array_push($v_Data, stripcslashes($v_Abastecimentos));

                        array_push($v_Data, $c_Result->ano_base);
                        array_push($v_Data, $c_Result->visitantes_total);
                        array_push($v_Data, $c_Result->visitantes_alta_temporada);
                        array_push($v_Data, $c_Result->visitantes_baixa_temporada);
                        array_push($v_Data, $c_Result->visitantes_regionais);
                        array_push($v_Data, $c_Result->visitantes_estaduais);
                        array_push($v_Data, $c_Result->visitantes_nacionais);
                        array_push($v_Data, $c_Result->visitantes_internacionais);
                        array_push($v_Data, $c_Result->historico_municipio);
                        array_push($v_Data, $c_Result->descricao_informacoes_complementares);
                        array_push($v_Data, $c_Result->parceirias_cooperacoes_intercambios_interfaces);
                        array_push($v_Data, $c_Result->fontes_dados_instituicao);
                        array_push($v_Data, $c_Result->fontes_dados_site);
                        array_push($v_Data, $c_Result->fontes_dados_publicacoes);
                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        $sheet->row($v_CurrentRow,$v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:CL'.($v_CurrentRow-1), 'thin');
            });
        })->download('xlsx');
    }
}