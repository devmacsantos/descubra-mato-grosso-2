<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSelectedCategories extends Model
{
    public $timestamps = false;
    protected $table = 'event_selected_categories';

    public static function updateEventCategories($p_EventId, $p_Categories)
    {
        EventSelectedCategories::where('event_id', $p_EventId)->delete();
        foreach($p_Categories as $c_Category)
        {
            $v_EventCity = new EventSelectedCategories();
            $v_EventCity->event_id = $p_EventId;
            $v_EventCity->event_category_id = $c_Category;
            $v_EventCity->save();
        }
    }

    public static function getSelectedCategories($p_EventId)
    {
        return EventSelectedCategories::where('event_id', $p_EventId)->lists('event_category_id')->toArray();
    }

    public function Event(){
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }

    public function vEventDates(){
        return $this->belongsTo(vEventDates::class, 'event_id', 'event_id');
    }


    public function EventCategory(){
        return $this->belongsTo(EventCategory::class, 'event_category_id', 'id');
    }



}