<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemsInstitutional extends Model
{
    public $timestamps = false;
    protected $table = 'items_institutional';

    public static function post($p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data)
    {
        foreach($p_Data['language'] as $c_Index => $p_Language)
        {
            $v_ItemsInstitutional = ItemsInstitutional::findOrNew($p_Data['id'][$c_Index]);
            $v_ItemsInstitutional->language = $p_Language;
            $v_ItemsInstitutional->depoimento = $p_Data['depoimento'][$c_Index];
            $v_ItemsInstitutional->depoimento_autor = $p_Data['depoimento_autor'][$c_Index];
            $v_ItemsInstitutional->titulo = $p_Data['titulo'][$c_Index];
            $v_ItemsInstitutional->descricao = $p_Data['descricao'][$c_Index];
            for($c_Counter = 1; $c_Counter <=3; $c_Counter++)
            {
                $v_ItemsInstitutional['dado_relevante_' . $c_Counter] = $p_Data['dado_relevante_' . $c_Counter][$c_Index];
                $v_ItemsInstitutional['dado_relevante_descricao_' . $c_Counter] = $p_Data['dado_relevante_descricao_' . $c_Counter][$c_Index];
            }
            $v_ItemsInstitutional->video_url = $p_Data['video_url'];

            $v_ItemsInstitutional->save();
        }

        InstitutionalPhoto::updatePhotos($p_Photos, $p_PhotoInfo, $p_DeletedPhotos);
    }

    public static function getByLanguage($p_Language)
    {
        return ItemsInstitutional::where('language', $p_Language)->first();
    }
}