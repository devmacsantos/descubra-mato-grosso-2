<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TripCategory extends Model
{
    public $timestamps = false;
    protected $table = 'trip_category';
    protected $guarded = [];

    public static function getCategoryList()
    {
        return TripCategory::select(['id', 'nome_pt'])->get();
    }

    public static function getList($p_Language = null)
    {
        if($p_Language == null)
            return TripCategory::lists('nome_pt', 'id')->toArray();
        else
            return TripCategory::lists('nome', 'id')->toArray();
    }

    public static function post($p_Id, $p_CoverPhoto, $p_Data)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/tipo-viagem/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_TripData = $p_Data['formulario'];
        $v_PhotoUrl = null;
        if($p_CoverPhoto != null)
        {
            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $p_CoverPhoto->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $p_CoverPhoto->encode('jpg')->save($v_Path . $v_PhotoName);
            $v_PhotoUrl = url('/imagens/tipo-viagem/' . $v_PhotoName);
        }
        $v_TripData['destaque'] = array_key_exists('destaque', $v_TripData) ? 1 : 0;

        $v_TripCategory = TripCategory::findOrNew($p_Id);

        if($v_PhotoUrl != null)
        {
            if($v_TripCategory->foto_capa_url != null)
            {
                $v_OldFileName = explode('/', $v_TripCategory->foto_capa_url);
                $v_OldFileName = array_pop($v_OldFileName);
                \File::delete($v_Path . $v_OldFileName);
            }
            $v_TripData['foto_capa_url'] = $v_PhotoUrl;
        }

        if($p_Id == null)
            $v_TripData['slug'] = TripCategory::generateSlug($v_TripData['nome_pt']);
        TripCategory::updateOrCreate(['id' => $p_Id], $v_TripData);
    }

    public static function generateSlug($p_String)
    {
        $v_Slug = Str::slug($p_String);
        if(TripCategory::where('slug', $v_Slug)->count() == 0)
            return $v_Slug;

        $v_UniqueSlug = false;
        $v_Index = 0;
        while(!$v_UniqueSlug){
            if(TripCategory::where('slug', $v_Slug . '-' . $v_Index)->count() == 0)
                $v_UniqueSlug = true;
            else
                $v_Index++;
        }

        return $v_Slug . '-' . $v_Index;
    }

    public static function getCategories($p_Quantity)
    {
        return TripCategory::orderBy('destaque', 'desc')->where('destaque','1')->orderByRaw('RAND()')->take($p_Quantity)->get();
    }

    public static function getCategory($p_Slug)
    {
        return TripCategory::where('slug', $p_Slug)
                           ->select(['id', 'slug', 'nome', 'foto_capa_url', 'descricao'])
                           ->firstOrFail();
    }
}