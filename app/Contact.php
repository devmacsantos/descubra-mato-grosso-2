<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class Contact extends Model
{
    public $timestamps = false;
    protected $table = 'contact';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:250',
        'email' => 'required|email',
        'assunto' => 'required|numeric|min:1',
        'mensagem' => 'required|min:1',
        'cidade' => 'required|numeric|min:1',
        'g-recaptcha-response' => 'required|recaptcha'
    );

    public static function post($p_Name, $p_Email, $p_SubjectId, $p_Message, $p_City, $p_Ip)
    {
        $v_Contact = new Contact();
        $v_Contact->name = $p_Name;
        $v_Contact->email = $p_Email;
        $v_Contact->contact_subject_id = $p_SubjectId;
        $v_Contact->message = $p_Message;
        $v_Contact->contact_city_id = $p_City;
        $v_Contact->date = Carbon::now()->format('Y-m-d H:i:s');
        $v_Contact->ip = $p_Ip;
        $v_Contact->save();

        $v_ContactCity = ContactCity::find($p_City);
	    $v_Subject = ContactSubject::find($p_SubjectId)->nome_pt;

	    Mail::send('emails.contact', ['p_Name' => $p_Name, 'p_Email' => $p_Email, 'p_Subject' => $v_Subject, 'p_City' => $v_ContactCity, 'p_Message' => $p_Message],
	    function ($message)
	    {
		    $message->to('informasetur@turismo.mg.gov.br', 'Portal Setur')->subject('Fale conosco - Descubra Mato Grosso');
	    });

	    //Delete old messages
	    Contact::where('date', '<', Carbon::yesterday()->startOfDay())->delete();
        return true;
    }

    public static function invalid($p_Ip)
    {
        $v_ContactCount = Contact::where('ip', '=', $p_Ip)
                               ->where('date', '>=', Carbon::now()->subMinute())
                               ->count();
        return ($v_ContactCount >= 5);
    }
}