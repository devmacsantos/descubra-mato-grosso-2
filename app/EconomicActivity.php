<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EconomicActivity extends Model
{
    public $timestamps = false;
    protected $table = 'economic_activity';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:300'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:300'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_Activity = EconomicActivity::findOrNew($p_Id);
        $v_Activity->name = $p_Name;
        $v_Activity->save();
    }

    public static function getList()
    {
        return EconomicActivity::orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getOptionalList()
    {
        return ['' => ''] + EconomicActivity::orderBy('name')->lists('name', 'id')->toArray();
    }
}