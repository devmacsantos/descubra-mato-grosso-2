<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class City extends Model
{
    public $timestamps = false;
    protected $table = 'city';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:250'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:250'
    );



    //Relacionamento de 1 para muitos
    public function Destination(){
        return $this->hasMany(Destination::class, 'city_id', 'id');
    }



    public static function getList()
    {
        return City::orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getListExceptIds($p_Ids, $p_LimitByUserType = false)
    {
        $v_Query = City::whereNotIn('id', $p_Ids);
        if($p_LimitByUserType)
        {
            if(UserType::isMunicipio())
                $v_Query->where('id',Auth::user()->city_id);
            else if(UserType::isCircuito())
                $v_Query->whereIn('id',TouristicCircuitCities::getUserCircuitCities());
        }
        return $v_Query->orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getListExcept($p_Id)
    {
        return City::where('id', '!=', $p_Id)->orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getDTCities($p_Name, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = City::select(DB::raw('SQL_CALC_FOUND_ROWS city.id, city.name'));

        if($p_Name != '')
            $v_Query->where('city.name', 'LIKE', '%' . $p_Name . '%');

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['name'],
                '<div class="actions-div">' .
                    '<a href="' . url('admin/municipios/' . $v_QueryRes[$c_Index]['id'] . '/distritos') . '" title="Distritos" type="button" class="btn btn-success districts-btn">Distritos</a>' .
                    '<a href="' . url('admin/municipios/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = City::getTotalRows();
        $v_DataTableAjax->recordsTotal = City::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function post($p_Id, $p_Name)
    {
        $v_City = City::findOrNew($p_Id);
        $v_City->name = $p_Name;
        $v_City->save();
    }

    public static function getCityTouristicCircuit($p_CityId)
    {
        $v_Response['error'] = 'ok';
        $v_Circuit = TouristicCircuit::join('touristic_circuit_cities', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
            ->where('touristic_circuit_cities.city_id', $p_CityId)->select('touristic_circuit.nome')->groupBy('touristic_circuit.id')->first();
        $v_Response['data'] = $v_Circuit == null ? '' : $v_Circuit->nome;
        return $v_Response;
    }


    public static function getCitiesInventoryReport($p_Filters)
    {
        $v_Query = City::orderBy('city.name');
        if(isset($p_Filters['id']) && $p_Filters['id'] != '')
            $v_Query->whereIn('city.id', $p_Filters['id']);
        if(isset($p_Filters['touristic_circuit_id']) && $p_Filters['touristic_circuit_id'] != '') {
            $v_CityIds = TouristicCircuitCities::getCircuitCities($p_Filters['touristic_circuit_id']);
            $v_Query->whereIn('city.id', $v_CityIds['data']);
        }
        if(isset($p_Filters['region_id']) && $p_Filters['region_id'] != '') {
            $v_Query->join('destination', 'destination.city_id', '=', 'city.id')
                    ->where('destination.region_id', $p_Filters['region_id']);
        }
        return $v_Query->get();
    }

    public function accomodations($p_Filters, $p_TemCadastur)
    {
        $v_Relation = $this->hasMany('App\AccomodationService')->select('city_id');
        if($p_TemCadastur != null)
            $v_Relation->where('tem_cadastur', $p_TemCadastur);
        return $this->publishedInventoryFilter($v_Relation, $p_Filters);
    }

    public function foodDrinkServices($p_Filters)
    {
        $v_Relation = $this->hasMany('App\FoodDrinksService')->select('city_id');
        return $this->publishedInventoryFilter($v_Relation, $p_Filters);
    }

    public function tourismAgencyServices($p_Filters, $p_TemCadastur)
    {
        $v_Relation = $this->hasMany('App\TourismAgencyService')->select('city_id');
        if($p_TemCadastur != null)
            $v_Relation->where('tem_cadastur', $p_TemCadastur);
        return $this->publishedInventoryFilter($v_Relation, $p_Filters);
    }

    public function tourismTransportationServices($p_Filters, $p_TemCadastur)
    {
        $v_Relation = $this->hasMany('App\TourismTransportationService')->select('city_id');
        if($p_TemCadastur != null)
            $v_Relation->where('tem_cadastur', $p_TemCadastur);
        return $this->publishedInventoryFilter($v_Relation, $p_Filters);
    }

    public function eventServices($p_Filters)
    {
        return $this->hasMany('App\EventService')->select('city_id');
    }

    public function recreationServices($p_Filters)
    {
        $v_Relation = $this->hasMany('App\RecreationService')->select('city_id');
        return $this->publishedInventoryFilter($v_Relation, $p_Filters);
    }

    public function otherTourismServices($p_Filters)
    {
        return $this->hasMany('App\OtherTourismService')->select('city_id');
    }

    public function naturalAttractions($p_Filters)
    {
        $v_Relation = $this->hasMany('App\NaturalAttraction')->select('city_id');
        return $this->publishedInventoryFilter($v_Relation, $p_Filters);
    }

    public function culturalAttractions($p_Filters)
    {
        $v_Relation = $this->hasMany('App\CulturalAttraction')->select('city_id');
        return $this->publishedInventoryFilter($v_Relation, $p_Filters);
    }

    public function publishedInventoryFilter($p_Relation, $p_Filters) {
        if(isset($p_Filters['published']) && $p_Filters['published'] != '') {
            if($p_Filters['published'])
                $p_Relation->whereNotNull('data_publicacao');
            else
                $p_Relation->whereNull('data_publicacao');
        }
        return $p_Relation;
    }
}