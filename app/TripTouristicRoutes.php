<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TripTouristicRoutes extends Model
{
    public $timestamps = false;
    protected $table = 'trip_touristic_routes';

    public static function updateTripTypes($p_TouristicRouteId, $p_TripTypes)
    {
        TripTouristicRoutes::where('touristic_route_id', $p_TouristicRouteId)->delete();
        foreach($p_TripTypes as $c_Type)
        {
            $v_Item = new TripTouristicRoutes();
            $v_Item->touristic_route_id = $p_TouristicRouteId;
            $v_Item->trip_type_id = $c_Type;
            $v_Item->save();
        }
    }

    public static function getSelectedTypes($p_TouristicRouteId)
    {
        return TripTouristicRoutes::where('touristic_route_id', $p_TouristicRouteId)->lists('trip_type_id')->toArray();
    }

    public static function getTripTouristicRoutes($p_TripTypeId, $p_Quantity)
    {
        return TripTouristicRoutes::join('touristic_route', 'touristic_route.id', '=', 'trip_touristic_routes.touristic_route_id')
            ->join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
            ->where('trip_touristic_routes.trip_type_id', $p_TripTypeId)
            ->where('touristic_route.active', 1)
            ->where('touristic_route_photo.is_cover', 1)
            ->select(['touristic_route.nome', 'touristic_route.slug', 'touristic_route_photo.url', 'touristic_route.descricao_curta'])
            ->orderBy('touristic_route.destaque', 'desc')
            ->groupBy('touristic_route.id')
            ->take($p_Quantity)->get();
    }

    public static function getTripTouristicRoutesWithDuration($p_TripTypeId, $p_Duration, $p_Quantity)
    {
        $v_Query = TripTouristicRoutes::join('touristic_route', 'touristic_route.id', '=', 'trip_touristic_routes.touristic_route_id')
                                      ->join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
                                      ->where('trip_touristic_routes.trip_type_id', $p_TripTypeId)
                                      ->where('touristic_route.active', 1)
                                      ->where('touristic_route_photo.is_cover', 1);
        if(!empty($p_Duration)) {
            if($p_Duration == 1)
                $v_Query->where('touristic_route.dias_duracao', '<=', 3);
            else if($p_Duration == 2)
                $v_Query->where('touristic_route.dias_duracao', '>', 3)->where('touristic_route.dias_duracao', '<=', 7);
            else
                $v_Query->where('touristic_route.dias_duracao', '>', 7);
        }
        return $v_Query->select(['touristic_route.nome', 'touristic_route.slug', 'touristic_route_photo.url', 'touristic_route.descricao_curta'])
            ->orderBy('touristic_route.destaque', 'desc')
            ->groupBy('touristic_route.id')
            ->take($p_Quantity)->get();
    }

    public static function getTripAndDestinationTouristicRoutes($p_TripTypeId, $p_DestinationId, $p_Quantity)
    {
        return TripTouristicRoutes::join('touristic_route', 'touristic_route.id', '=', 'trip_touristic_routes.touristic_route_id')
            ->join('touristic_route_photo', 'touristic_route_photo.touristic_route_id', '=', 'touristic_route.id')
            ->join('touristic_route_destinations', 'touristic_route_destinations.touristic_route_id', '=', 'touristic_route.id')
            ->where('trip_touristic_routes.trip_type_id', $p_TripTypeId)
            ->where('touristic_route.active', 1)
            ->where('touristic_route_destinations.destination_id', $p_DestinationId)
            ->where('touristic_route_photo.is_cover', 1)
            ->select(['touristic_route.nome', 'touristic_route.slug', 'touristic_route_photo.url', 'touristic_route.descricao_curta'])
            ->orderBy('touristic_route.destaque', 'desc')
            ->groupBy('touristic_route.id')
            ->take($p_Quantity)->get();
    }
}