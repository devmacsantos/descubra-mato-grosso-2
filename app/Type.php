<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Attraction;
class Type extends Model
{
    public $timestamps = false;
    protected $table = 'type';

    public static $m_Rules = array
    (
        'inventory_item_id' => 'required|numeric|min:1',
        'super_type_id' => 'numeric|min:1',
        'nome' => 'required|min:1|max:300',
        'slug' => 'required|min:1|max:300',
        'trade' => 'required|min:1|max:300'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'inventory_item_id' => 'required|numeric|min:1',
        'super_type_id' => 'numeric|min:1',
        'nome' => 'required|min:1|max:300',
        'slug' => 'required|min:1|max:300',
        'trade' => 'required|min:1|max:300'
    );

    public static function post($p_Id, 
                                $p_InventoryItemId, 
                                $p_SuperTypeId, 
                                $p_Name, 
                                $p_NameEn, 
                                $p_NameEs, 
                                $p_NameFr,
                                $slug,
                                $trade,
                                $sinonimos,
                                $excluir
                                )
    {
        $v_Type = Type::findOrNew($p_Id);
        $v_Type->inventory_item_id = $p_InventoryItemId;
        $v_Type->super_type_id = $p_SuperTypeId;
        $v_Type->name = $p_Name;
        $v_Type->name_en = $p_NameEn;
        $v_Type->name_es = $p_NameEs;
        $v_Type->name_fr = $p_NameFr;
        $v_Type->slug= $slug;
        $v_Type->trade = $trade;
        $v_Type->sinonimos= $sinonimos;
        $v_Type->excluir = $excluir;
        $v_Type->save();
        Type::where('super_type_id', $v_Type->id)->update(['inventory_item_id' => $p_InventoryItemId]);
        //teste

        DB::statement("update attraction
                set type_slug='$slug',
                    slug_trade='$trade',
                    sinonimos='$sinonimos',
                    antonimos='$excluir'
                where type_id='$v_Type->id'
                and sub_type_id is  NULL
                ");
        DB::statement("update attraction
                set type_slug='$slug',
                    slug_trade='$trade',
                    sinonimos='$sinonimos',
                    antonimos='$excluir'
                where sub_type_id='$v_Type->id'
                and sub_type_id is not NULL
                ");

    }

    public static function getList()
    {
        return Type::orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getInventoryTypeListById($p_Id, $p_InventoryItemId)
    {
        $p_Id = $p_Id == null ? 0 : $p_Id;
        $v_Response['error'] = 'ok';
        $v_Response['data'] = Type::where('type.inventory_item_id', $p_InventoryItemId)
            ->where('type.id', '!=', $p_Id)->whereNull('type.super_type_id')
            ->orderBy('name')->lists('name', 'id')->toArray();
        return $v_Response;
    }

    public static function getInventoryTypeListByName($p_InventoryItemName)
    {
        if(UserType::isTrade())
            return Type::join('inventory_item', 'inventory_item.id', '=', 'type.inventory_item_id')
                       ->where('inventory_item.name', $p_InventoryItemName)->whereNull('type.super_type_id')
                       ->whereIn('type.id', [37,50,55,56,57,58,60,61,62,63,64,256,66,74,76,78,79,80,83,84,85,86,87,88,89])
                       ->select(['type.name', 'type.id'])
                       ->orderBy('name')->lists('name', 'id')->toArray();
        else
            return Type::join('inventory_item', 'inventory_item.id', '=', 'type.inventory_item_id')
                       ->where('inventory_item.name', $p_InventoryItemName)->whereNull('type.super_type_id')
                       ->select(['type.name', 'type.id'])
                       ->orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getPublicInventoryTypeServiceList($p_Language)
    {
        $p_Language = $p_Language == 'pt' ? '' : ('_' . $p_Language);
        return Type::whereIn('id', [64, 66, 256])
                   ->orderBy('name' . $p_Language)->lists('name' . $p_Language, 'id')->toArray();
    }

    public static function getPublicInventoryTypeListByName($p_InventoryItemName, $p_Language)
    {
        $p_Language = $p_Language == 'pt' ? '' : ('_' . $p_Language);
        return Type::join('inventory_item', 'inventory_item.id', '=', 'type.inventory_item_id')
                   ->where('inventory_item.name', $p_InventoryItemName)
                   ->select(['type.name' . $p_Language, 'type.id'])
                   ->orderBy('name' . $p_Language)->lists('name' . $p_Language, 'id')->toArray();
    }

    public static function getInventorySubtypeList($p_SuperTypeId)
    {
        $v_Response['error'] = 'ok';
        if(UserType::isTrade())
            $v_Response['data'] = Type::where('type.super_type_id', $p_SuperTypeId)
                ->whereIn('type.id', [38,40,41,43,51,52,53])->orderBy('name')->lists('name', 'id')->toArray();
        else
            $v_Response['data'] = Type::where('type.super_type_id', $p_SuperTypeId)->orderBy('name')->lists('name', 'id')->toArray();
        return $v_Response;
    }

    public static function getSuper()
    {
        return ['' => ''] + Type::whereNull('super_type_id')->orderBy('name')->lists('name', 'id')->toArray();
    }

    public static function getAll()
    {
        return Type::join('inventory_item', 'inventory_item.id', '=', 'type.inventory_item_id')
            ->leftJoin('type as super_type', 'super_type.id', '=', 'type.super_type_id')
            ->select(['inventory_item.name as inventory', 'super_type.name as super_name', 'type.name', 'type.id'])->orderBy('name')->get();
    }
}