<?php
namespace App;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class GastronomicTypicalDish extends BaseInventoryModel
{
    protected $table = 'gastronomic_typical_dish';

    public static function getDT($p_Name, $p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = GastronomicTypicalDish::join('city', 'city.id', '=', 'gastronomic_typical_dish.city_id')->join('revision_status', 'revision_status.id', '=', 'gastronomic_typical_dish.revision_status_id')
                                         ->select(DB::raw('SQL_CALC_FOUND_ROWS gastronomic_typical_dish.id, gastronomic_typical_dish.city_id, gastronomic_typical_dish.nome, city.name as municipio, gastronomic_typical_dish.created_at, gastronomic_typical_dish.updated_at, revision_status.name as status'));

        if(UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());

        if($p_Name != '')
            $v_Query->where('gastronomic_typical_dish.nome', 'like', '%' . $p_Name . '%');

        if($p_City != '')
            $v_Query->where('city.name', 'like', '%' . $p_City . '%');

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('gastronomic_typical_dish.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('gastronomic_typical_dish.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('gastronomic_typical_dish.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('gastronomic_typical_dish.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Status != '')
            $v_Query->where('gastronomic_typical_dish.revision_status_id',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('gastronomic_typical_dish.nome', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('gastronomic_typical_dish.created_at', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('gastronomic_typical_dish.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['nome'],
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                    '<a href="' . url('admin/inventario/prato-tipico/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/inventario/prato-tipico/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                    ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/prato-tipico/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = BaseInventoryModel::getTotalRows();

        if(UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = GastronomicTypicalDish::where('city_id', Auth::user()->city_id)->count();
        else if(UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = GastronomicTypicalDish::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else
            $v_DataTableAjax->recordsTotal = GastronomicTypicalDish::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function post($p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data)
    {
        $v_Data = $p_Data['formulario'];
        $v_Data['reconhecido_como_patrimonio'] = array_key_exists('reconhecido_como_patrimonio', $v_Data) ? 1 : 0;

        array_walk($v_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_PreviousStatus = 0;
        if($p_Id != null)
            $v_PreviousStatus = GastronomicTypicalDish::find($p_Id)->revision_status_id;

        $v_Item = GastronomicTypicalDish::updateOrCreate(['id' => $p_Id], $v_Data);

        InventoryPhoto::updatePhotos('C6.3', $v_Item->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);

        if($p_Id != null && $v_PreviousStatus != 4){
            $v_Subject = null;
            $v_Message = null;
            if($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            else if($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome . ' foram rejeitadas.</p>';
                if(!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'C6.2', $v_Item->id);
                foreach($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                        function ($message) use ($c_User, $v_Subject)
                        {
                            $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                        });
                }
            }
        }
    }

    public static function deleteGastronomicTypicalDish($p_Id)
    {
        $v_Query = GastronomicTypicalDish::where('id',$p_Id);
        if(UserType::isMunicipio())
            $v_Query->where('city_id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
        $v_Query->delete();
        $v_Item = $v_Query->firstOrFail();

        InventoryPhoto::deletePhotos('C6.3', $p_Id);

        $v_Item->equipe_responsavel_observacao = 'DELETED';
        $v_Item->save();
        $v_Item->delete();
    }

    public static function getReport($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_DistrictId = $p_Data['district_id'];
	    $v_Name = $p_Data['nome'];

        $v_Query = GastronomicTypicalDish::leftJoin('city', 'city.id', '=', 'gastronomic_typical_dish.city_id')
                                        ->leftJoin('district', 'gastronomic_typical_dish.district_id', '=', 'district.id')
                                        ->leftJoin('touristic_circuit_cities', 'gastronomic_typical_dish.city_id', '=', 'touristic_circuit_cities.city_id')
                                        ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                                        ->leftJoin('destination', 'destination.city_id', '=', 'gastronomic_typical_dish.city_id')
                                        ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                                        ->selectRaw('gastronomic_typical_dish.*, city.name as cidade, district.name as distrito, touristic_circuit.nome as circuito, region.name as regiao')
                                        ->groupBy('gastronomic_typical_dish.id');

        if(array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('gastronomic_typical_dish.city_id', $p_Data['city_id']);

        if($v_CircuitId != '')
            $v_Query->where('touristic_circuit_cities.touristic_circuit_id', $v_CircuitId);

        if($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        if($v_DistrictId != '')
            $v_Query->where('gastronomic_typical_dish.district_id', $v_DistrictId);

        if($v_Name != '')
            $v_Query->where('gastronomic_typical_dish.nome', 'LIKE', '%' . $v_Name . '%');

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioC6-2.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->distrito);

                        array_push($v_Data, $c_Result->nome);
                        array_push($v_Data, html_entity_decode(strip_tags($c_Result->receita)));
                        array_push($v_Data, $c_Result->reconhecido_como_patrimonio ? 'Sim' : 'Não');
                        $v_OrgaosJSON = json_decode($c_Result->orgaos_que_reconhecem,1);
                        $v_Orgaos = '';
                        foreach($v_OrgaosJSON as $c_Index => $c_Orgao)
                            $v_Orgaos .= ($c_Index > 0 ? ', ' : '') . $c_Orgao;
                        array_push($v_Data, $v_Orgaos);

                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        $sheet->row($v_CurrentRow,$v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:M'.($v_CurrentRow-1), 'thin');
            });
        })->download('xlsx');
    }
}