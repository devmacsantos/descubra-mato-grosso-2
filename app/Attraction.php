<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class Attraction extends Model
{
    protected $table = 'attraction';
    protected $guarded = [];

    public static $m_ParkTypeId = 129; //Unidades de conservação

    public static $m_Fields = [
        'city_id' => 'city_id',
        'district_id' => 'district_id',
        'type_id' => 'type_id',
        'sub_type_id' => 'sub_type_id',
        'destaque' => 'destaque',
        'telefone' => 'telefone',
        'whatsapp' => 'whatsapp',
        'site' => 'site',
        'email' => 'email',
        'facebook' => 'facebook',
        'instagram' => 'instagram',
        'twitter' => 'twitter',
        'youtube' => 'youtube',
        'tripadvisor' => 'tripadvisor',
        'cep' => 'cep',
        'bairro' => 'bairro',
        'logradouro' => 'logradouro',
        'numero' => 'numero',
        'complemento' => 'complemento',
        'latitude' => 'latitude',
        'longitude' => 'longitude',
        'localizacao' => 'localizacao',
        'funcionamento_1' => 'funcionamento_1',
        'funcionamento_2' => 'funcionamento_2',
        'funcionamento_observacao' => 'funcionamento_observacao',
        'descricao_curta' => 'descricao_curta',
        'acessibilidade' => 'acessibilidade',
        'formas_pagamento' => 'formas_pagamento',
        'minas_360' => 'minas_360',
        'equipamento_fechado' => 'equipamento_fechado',
        'equipamento_fechado_motivo' => 'equipamento_fechado_motivo',
        'nome_oficial'  => 'nome_oficial',

        //module B specific fields
        'descricao_arredores_distancia_pontos' => 'descricao_arredores_distancia_pontos',
        'aceita_animais' => 'aceita_animais',
        'gay_friendly' => 'gay_friendly',
        'nao_aceita_criancas' => 'nao_aceita_criancas',
        'restricoes' => 'restricoes',
        'unidades_habitacionais_facilidades' => 'unidades_habitacionais_facilidades',
        'unidades_habitacionais_voltagem' => 'unidades_habitacionais_voltagem',
        'tipo_diaria' => 'tipo_diaria',
        'tipo_diaria_outros' => 'tipo_diaria_outros',
        'servicos_equipamentos_recreacao_lazer' => 'servicos_equipamentos_recreacao_lazer',
        'culinaria_nacionalidades' => 'culinaria_nacionalidades',
        'culinaria_nacionalidade_outras' => 'culinaria_nacionalidade_outras',
        'culinaria_servicos' => 'culinaria_servicos',
        'culinaria_temas' => 'culinaria_temas',
        'culinaria_tema_outros' => 'culinaria_tema_outros',
        'culinaria_regioes' => 'culinaria_regioes',
        'culinaria_regiao_outras' => 'culinaria_regiao_outras',
        'segmentos_agencia' => 'segmentos_agencia',
        'tipos_servico' => 'tipos_servico',
        'quantidade_veiculos_adaptados' => 'quantidade_veiculos_adaptados',
        'servicos_equipamentos_apoio' => 'servicos_equipamentos_apoio',
        'principais_atividades' => 'principais_atividades',
        'municipios_participantes' => 'municipios_participantes',
        

        //module C specific fields
        'observacoes_complementares' => 'observacoes_complementares',
        'visita_tipo' => 'visita_tipo',
        'entrada_tipo' => 'entrada_tipo',
        'entrada_valor' => 'entrada_valor',
        'atividades_realizadas' => 'atividades_realizadas',
        'necessaria_autorizacao_previa' => 'necessaria_autorizacao_previa',
        'autorizacao_previa_tipo' => 'autorizacao_previa_tipo',
    ];

    public static $m_ModuleBFields = [
        'nome' => 'nome_fantasia',
        'descricao' => 'descricoes_observacoes_complementares',
        'servicos_equipamentos' => 'servicos_equipamentos',
        'atracao' => 'atracao'
    ];

    public static $m_ModuleCFields = [
        'nome' => 'nome_popular',
        'ponto_referencia' => 'ponto_referencia',
        'descricao' => 'descricao_atrativo'
    ];

    public static function publish($p_Item, $p_Type, $p_IsTrade)
    {
        $p_Item = $p_Item->toArray();
        $v_Attraction = Attraction::firstOrNew(['item_inventario_id' => $p_Item['id'],
                                                'item_inventario' => $p_Type]);
        if($p_IsTrade) {
            $v_Fields = array_merge(Attraction::$m_Fields, Attraction::$m_ModuleBFields);
        }
        else {
            $v_Fields = array_merge(Attraction::$m_Fields, Attraction::$m_ModuleCFields);
            $v_Attraction->atracao = 1;
        }

        $v_Attraction->trade = $p_IsTrade;
        $v_Attraction->parque = ($p_Type == 'C1' && $p_Item['type_id'] == Attraction::$m_ParkTypeId);
        $v_Attraction->integrante_minas_recebe = $p_Type == 'B3' ? $p_Item['integrante_minas_recebe'] : 0;
        foreach($v_Fields as $c_Field => $c_Key)
        {
            if(array_key_exists($c_Key, $p_Item))
                $v_Attraction[$c_Field] = $p_Item[$c_Key];
            else if($c_Key != 'destaque')
                $v_Attraction[$c_Field] = null;
        }
        if(in_array($p_Type,array('B1','B2','B3','B4','B5','B6'))){
            
            $v_Attraction->nome_oficial = $p_Item['nome_juridico'];
        }
        //atualiza campos que permitem busca

        if ($v_Attraction->sub_type == NULL){
            $tipo = $v_Attraction->type_id;
        }else {
            $tipo = $v_Attraction->sub_type_id;
        }
            

        $type = DB::select("select t.* from type t
        where t.id=$tipo
        ");


        $v_Attraction->type_slug = $type[0]->slug;
        $v_Attraction->slug_trade = $type[0]->trade;
        $v_Attraction->sinonimos = $type[0]->sinonimos;
        $v_Attraction->antonimos = $type[0]->excluir;

        $cidade = DB::select("select c.* from city c, attraction a
        where c.id=a.city_id
        ");

        $v_Attraction->city_name = $cidade[0]->name;

        if($p_IsTrade)
            $v_Attraction->nome_pt = $v_Attraction->nome;
        else
            $v_Attraction->nome_pt = json_decode($v_Attraction->nome,1)['pt'];

        if($v_Attraction->slug == null)
            $v_Attraction->slug = Attraction::generateSlug($v_Attraction, $v_Attraction->id);

        $v_Attraction->save();

        $v_TripTypes = $p_Item['tipos_viagem'] == null ? [] : json_decode($p_Item['tipos_viagem'],1);
        TripAttractions::updateTripTypes($v_Attraction->id, $v_TripTypes);

        $v_Hashtags = $p_Item['hashtags'] == null ? [] : explode(';', $p_Item['hashtags']);
        Hashtag::updateHashtag('attraction', $v_Attraction->id, $v_Hashtags);

        //dd(InventoryPhoto::getItemPhotos($p_Type, $p_Item['id']), $v_Attraction->id);

        AttractionPhoto::updatePhotos(InventoryPhoto::getItemPhotos($p_Type, $p_Item['id']), $v_Attraction->id);
    }

    public static function generateSlug($p_Attraction, $p_Id)
    {
        $v_Slug = Str::slug($p_Attraction->nome_pt);

        $v_Query = Attraction::where('slug', $v_Slug);
        if($p_Id != null)
            $v_Query->where('id', '!=', $p_Id);
        if($v_Query->count() == 0)
            return $v_Slug;

        $v_UniqueSlug = false;
        $v_Index = 0;
        while(!$v_UniqueSlug){
            $v_Query = Attraction::where('slug', $v_Slug . '-' . $v_Index);
            if($p_Id != null)
                $v_Query->where('id', '!=', $p_Id);

            if($v_Query->count() == 0)
                $v_UniqueSlug = true;
            else
                $v_Index++;
        }

        return $v_Slug . '-' . $v_Index;
    }

    public static function unpublish($p_ItemId, $p_Type)
    {
        $v_Attraction = Attraction::where('item_inventario_id', $p_ItemId)
                                  ->where('item_inventario', $p_Type)
                                  ->first();

        AttractionPhoto::updatePhotos([], $v_Attraction->id);
        $v_Attraction->delete();
    }
    
    public static function getList()
    {
        return Attraction::where('atracao', 1)->orderBy('nome_pt')->lists('nome_pt', 'id')->toArray();
    }

    public static function getParks($p_Quantity)
    {
        return Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                         ->where('attraction.parque', 1)
                         ->where('attraction.atracao', 1)
                         ->where('attraction_photo.is_cover', 1)
                         ->select(['attraction.nome', 'attraction.slug', 'attraction.descricao_curta', 
                         'attraction_photo.url', 'attraction.city_id'])
                         ->orderBy('attraction.destaque', 'desc')
                         ->orderByRaw('RAND()')
                         ->groupBy('attraction.id')
                         ->take($p_Quantity)->get();
    }

    public static function getBySlug($p_Slug, $p_IsTrade = null)
    {

        $v_Query = Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                             ->join('city', 'city.id', '=', 'attraction.city_id')
                             ->where('attraction.slug', $p_Slug)
                             ->where('attraction_photo.is_cover', 1)
                             ->select('attraction.*', 'attraction_photo.url', 'city.name as cidade');

        if($p_IsTrade)
            $v_Query->where('attraction.trade', 1);
        else
            $v_Query->where('attraction.atracao', 1);
            
        return $v_Query->first();
    }

    public static function getCityMainAttractions($p_CityId, $p_Quantity, $p_ExceptId = null)
    {
        $v_Query = Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                             ->where('attraction.atracao', 1)->where('attraction_photo.is_cover', 1)
                             ->where('attraction.city_id', $p_CityId)
                             ->select(['attraction.nome', 'attraction.slug', 'attraction.descricao_curta', 
                             'attraction.trade', 'attraction_photo.url', 'attraction.city_id', 'attraction.type_id',
                             'attraction.item_inventario']);

        if($p_ExceptId != null)
            $v_Query->where('attraction.id', '!=', $p_ExceptId);

        return $v_Query->orderBy('destaque', 'desc')
                       ->orderByRaw('RAND()')
                       ->groupBy('attraction.id')
                       ->take($p_Quantity)->get();
    }

    public static function getOtherCitiesAttractions($p_CityId, $p_Latitude, $p_Longitude, $p_Quantity)
    {
        if($p_Latitude != null && $p_Longitude != null)
        {
            $v_Query = Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                                 ->where('attraction.atracao', 1)->where('attraction_photo.is_cover', 1)
                                 ->where('attraction.city_id', '!=', $p_CityId)
                                 ->select(['attraction.nome', 'attraction.slug', 
                                 'attraction.descricao_curta', 'attraction.trade', 
                                 'attraction.city_id',
                                 'attraction_photo.url']);

            //Find attractions from other cities in a 100km radius
            $v_Query->whereNotNull('attraction.latitude')->whereNotNull('attraction.longitude');
            $v_Query->where(function ($p_Query) use($p_Latitude, $p_Longitude){
                $p_Query->whereRaw('(6370.6934842*ACOS(SIN(RADIANS(' . $p_Latitude . '))*SIN(RADIANS(attraction.latitude))+COS(RADIANS(' . $p_Latitude . '))*COS(RADIANS(attraction.latitude))*COS(RADIANS(' . $p_Longitude . ' - attraction.longitude)))) <= 100');
            });

            return $v_Query->orderBy('destaque', 'desc')->orderByRaw('RAND()')->groupBy('attraction.id')->take($p_Quantity)->get();
        }
        else
            return [];
    }

    public static function getTouristicRouteAttractions($p_Destinations, $p_Quantity)
    {
        $v_CityIds = [];
        foreach($p_Destinations as $c_Destination)
            $v_CityIds = array_merge($v_CityIds, [$c_Destination->city_id]);

        return Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                         ->where('attraction.atracao', 1)->where('attraction_photo.is_cover', 1)
                         ->whereIn('attraction.city_id', $v_CityIds)
                         ->select(['attraction.nome', 'attraction.descricao_curta', 'attraction.slug', 
                         'attraction.trade', 'attraction_photo.url','attraction.city_id'])
                         ->orderBy('destaque', 'desc')
                         ->orderByRaw('RAND()')
                         ->groupBy('attraction.id')
                         ->take($p_Quantity)->get();
    }


//    public static function getTradeList()
//    {
//        return Attraction::where('trade', 1)->orderBy('nome')->lists('nome', 'id')->toArray();
//    }

//    public static function getCityTrade($p_CityId, $p_InventoryItem, $p_Quantity)
//    {
//	    $v_TypeIds = array_keys(Type::getInventoryTypeListByName($p_InventoryItem));
//	    //TODO pegar tambem o tipo?? o que vai precisar mostrar na listagem? precisa da cidade?
//
//	    return Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
//                         ->join('city', 'city.id', '=', 'attraction.city_id')
//                         ->whereIn('type_id', $v_TypeIds)->where('attraction.trade', 1)->where('attraction_photo.is_cover', 1)
//                         ->where('attraction.city_id', $p_CityId)
//                         ->select(['attraction.nome', 'attraction.slug', 'attraction.preco', 'attraction.tripadvisor_url',
//		        'attraction_photo.url', 'attraction.descricao_curta', 'city.name as cidade'])
//                         ->orderBy('destaque', 'desc')
//                         ->orderByRaw('RAND()')
//                         ->take($p_Quantity)->get();
//    }
//
//

    public static function getMapNaturalAttractions()
    {
        return [
            'parks' => Attraction::getMapAttractions([129],null),
            'waterfalls' => Attraction::getMapAttractions([122],null),
            'lakes' => Attraction::getMapAttractions(null,[119,120,121]),
            'caves' => Attraction::getMapAttractions([134],null),
            'mountains' => Attraction::getMapAttractions([94],null),
            'hidromineral' => Attraction::getMapAttractions([128],null)
        ];
    }

    public static function getMapCulturalAttractions()
    {
        return [
            'historic_centers' => Attraction::getMapAttractions([139],null),
            'buildings_monuments' => Attraction::getMapAttractions(null,[151,152,154,155,156,157,159]),
            'churches' => Attraction::getMapAttractions(null,[153]),
            'museums' => Attraction::getMapAttractions([165,240],null),
            'zoos' => Attraction::getMapAttractions([244,245,249,250],null),
            'markets' => Attraction::getMapAttractions(null,[202,203,204,207,211,212])
        ];
    }

    public static function getMapAttractions($p_TypeIds, $p_SubTypeIds)
    {
        $v_Query = Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                             ->join('city', 'city.id', '=', 'attraction.city_id')
                             ->where('attraction.atracao', 1)
                             ->where('attraction_photo.is_cover', 1)
                             ->whereNotNull('attraction.latitude')
                             ->whereNotNull('attraction.longitude')
                             ->select(['attraction.nome', 'attraction.slug', 'attraction.city_id', 'attraction.latitude', 'attraction.longitude', 'attraction_photo.url', 'city.name as cidade']);

        if($p_TypeIds != null)
            $v_Query->whereIn('type_id', $p_TypeIds);
        else
            $v_Query->whereIn('sub_type_id', $p_SubTypeIds);
        return $v_Query->groupBy('attraction.id')->get();
    }


    public static function getDestinationTrade($p_CityId, $p_Types)
    {
        return Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                         ->join('city', 'city.id', '=', 'attraction.city_id')
                         ->where(function($q) use($p_CityId) {
                             $q->where('attraction.city_id', $p_CityId)
                               ->orWhere('attraction.municipios_participantes', 'LIKE', '%"id":"' . $p_CityId . '"%')  ;
                         })
                         ->whereIn('attraction.item_inventario', $p_Types)
                         ->whereNotIn('type_id', [63, 65, 67, 68])
                         ->where('attraction.trade', 1)
                         ->where('attraction_photo.is_cover', 1)
                         ->select(['attraction.*', 'attraction_photo.url', 'city.name as cidade'])
                         ->orderBy('integrante_minas_recebe', 'DESC')
                         ->orderBy('nome')
                         ->groupBy('attraction.id')
                         ->take(4)->get();
    }


    public static function getTradeByCityAndType($p_CityId, $p_Types, $p_TypeIds)
    {
        $v_Quantity = (!is_null($p_TypeIds)) ? 100 : 20;
        $v_Types = ($p_TypeIds != null) ? Type::whereIn('id', $p_TypeIds)->get() : null;
        $v_Query = Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                         ->join('city', 'city.id', '=', 'attraction.city_id')
                         ->where(function($q) use($p_CityId) {
                             $q->where('attraction.city_id', $p_CityId)
                               ->orWhere('attraction.municipios_participantes', 'LIKE', '%"id":"' . $p_CityId . '"%')  ;
                         })
                         ->whereIn('attraction.item_inventario', $p_Types)
                         ->whereNotIn('type_id', [63, 65, 67, 68])
                         ->where('attraction.trade', 1)
                         ->where('attraction_photo.is_cover', 1);
//                         ->whereIn('attraction.type_id', [37,50,55,56,57,58,60,61,62,63,64,256,66,74,76,78,79,80,83,84,85,86,87,88,89,
//                             38,40,41,43,51,52,53]);

        if($v_Types && count($v_Types)> 0){
            $v_Query->where(function($q) use($v_Types) {
                    $v_FirstType = true;
                    foreach($v_Types as $c_Type){
                        $v_Field = $c_Type->super_type_id == null ? 'attraction.type_id' : 'attraction.sub_type_id';
                        if($v_FirstType){
                            $q->where($v_Field, $c_Type->id);
                            $v_FirstType = false;
                        }
                        else
                            $q->orWhere($v_Field, $c_Type->id);
                    }
                });
        }


        return $v_Query->select(['attraction.*', 'attraction_photo.url', 'city.name as cidade'])
                       ->orderBy('integrante_minas_recebe', 'DESC')
                       ->orderBy('nome')
                       ->groupBy('attraction.id')
                       ->take($v_Quantity)->get();
    }


    public static function getAttractionByCityAndType($p_CityId, $p_Type)
    {
        $v_Query = Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                             ->where('attraction.city_id', $p_CityId)
                             ->where('attraction.atracao', 1)
                             ->where('attraction_photo.is_cover', 1);

        $v_Type = null;
        if($p_Type != null) {
            $v_Quantity = 1000;
            if($p_Type == 'natureza')
                $v_Type = 'C1';
            else if($p_Type == 'cultura')
                $v_Type = 'C2';
            else if($p_Type == 'experiencia')
                $v_Type = 'C4';
        }
        else
            $v_Quantity = 100;

        if($v_Type != null)
            $v_Query->where('attraction.item_inventario', $v_Type);
        else
            $v_Query->whereIn('attraction.item_inventario', ['C1', 'C2', 'C4']);

        return $v_Query->select(['attraction.*', 'attraction_photo.url'])
                       ->orderBy('attraction.destaque', 'desc')
                       ->orderBy('nome')
                       ->groupBy('attraction.id')
                       ->take($v_Quantity)->get();
    }

    public static function getFeaturedAttractions($p_Quantity)
    {
        return Attraction::join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                        ->where('attraction.atracao', 1)->where('attraction_photo.is_cover', 1)
                        ->where('attraction.destaque', 1)
                        ->select(['attraction.nome', 'attraction.slug', 'attraction.descricao_curta', 'attraction.trade', 'attraction_photo.url'])
                        ->orderByRaw('RAND()')
                        ->groupBy('attraction.id')
                        ->take($p_Quantity)->get();
    }

    public static function get360Attractions($p_Filter)
    {
        $v_Query = Attraction::with('tripCategories')
                             ->join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                             ->where('attraction_photo.is_cover', 1)
                             ->whereNotNull('attraction.minas_360')
                             ->select(['attraction.id', 'attraction.nome', 'attraction.minas_360', 'attraction_photo.url']);

        if($p_Filter != null){
            $v_Query->leftJoin('trip_type_attractions', 'trip_type_attractions.attraction_id', '=', 'attraction.id')
                    ->whereIn('trip_type_attractions.trip_type_id', $p_Filter);
        }

        return $v_Query->groupBy('attraction.id')->get();
    }

    public function tripCategories()
    {
        return $this->hasMany('App\TripAttractions')
                    ->join('trip_type', 'trip_type.id', '=', 'trip_type_attractions.trip_type_id')
                    ->join('trip_category', 'trip_category.id', '=', 'trip_type.trip_category_id')
                    ->where('trip_type.publico', 1)
                    ->select('trip_type_attractions.attraction_id', 'trip_category.nome')
                    ->groupBy('trip_category.id');
    }
/**
 * Retornda registros que são obrigatório cadastro do cadastur
 */    
   public function filterRequiredCadastur(){
      return $this->whereIn('attraction.type_id', [37,50,63,64,65,66,67,68,72,74,90,253,256])
              ->orderBy('attraction.type_id', 'desc')
              ->get();
   }
}