<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class Event extends Model
{

    use \Venturecraft\Revisionable\RevisionableTrait;
    protected $revisionCreationsEnabled = true;
    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.

    protected $table = 'event';
    protected $guarded = [];

    public function EventSelectedClassification(){
        return $this->hasMany(EventSelectedClassification::class, 'event_id', 'id');
    }

    public function EventSelectedCategories(){
        return $this->hasMany(EventSelectedCategories::class, 'event_id', 'id');
    }

    //Relacionamento de 1 para muitos
    public function EventDate(){
        return $this->hasMany(EventDate::class, 'event_id', 'id');
    }

    public function EventByDates(){
        return $this->hasOne(EventByDates::class, 'event_id', 'id');
    }

    //Relacionamento de 1 para 1
    public function City(){
        return $this->hasOne(City::class, 'id', 'city_id');
    }
     //Relacionamento de 1 para 1
     public function CityInfo(){
        return $this->hasOne(CityInfo::class, 'city_id', 'city_id');
    }   
    //Relacionamento de 1 para 1
    public function Destination(){
        return $this->hasOne(Destination::class, 'city_id', 'city_id')
                ->whereNull('district_id');
    }    
    //Relacionamento de 1 para 1    
    public function RevisionStatus(){
        return $this->hasOne(RevisionStatus::class, 'id', 'revision_status_id');
    } 
    //Relacionamento de 1 para 1
    public function TouristicCircuit(){
        return $this->hasOne(TouristicCircuit::class, 'id', 'touristic_circuit_id');
    } 
    //Relacionamento de 1 para 1
    public function CreatedBy(){
        return $this->hasOne(User::class, 'id', 'created_by');
    }
    //Relacionamento de 1 para 1
    public function UpdatedBy(){
        return $this->hasOne(User::class, 'id', 'updated_by');
    }

    public function filter(Array $data, $paginate = 1000000){

        return $this->where(function($query) use ($data) {
            
            //Verifica se foi passado filtro de categoria
            if (isset($data['category'])){
                //Caso o filtro de categoria não seja um array o ítem é convertido
                if (!is_array($data['category'])){
                    $data['category'] = [$data['category']];
                }

                //Verifica se foi passado id para filtro
                if (isset($data['id'])){
                    //Verifica se id é array se não converte o ítem
                    if (!is_array($data['id'])){
                        $data['id'] = [$data['id']];
                    }
                    //Realiza consulta sando id de eventos como parâmetro alêm do id de categorias
                    $eventsId= EventSelectedCategories::whereIn('event_selected_categories.event_category_id', $data['category'])->whereIn('event_id', $data['id'])->select(DB::raw('event_selected_categories.event_id as id'))->get()->toArray();
                } else {
                    //Realiza consulta somente com id de categoria    
                    $eventsId= EventSelectedCategories::whereIn('event_selected_categories.event_category_id', $data['category'])->select(DB::raw('event_selected_categories.event_id as id'))->get()->toArray();    
                }

                //Verifica resultado
                if (count($eventsId) > 0){
                    $data['id'] = array_column($eventsId, 'id');
                } else {
                    //Não é possível interromper o processo :(
                    $data['id'] = ['0'];
                }
            }

            //Verifica se foi passado filtro de classificação
            if (isset($data['classification'])){
                //Caso o filtro de categoria não seja um array o ítem é convertido
                if (!is_array($data['classification'])){
                    $data['classification'] = [$data['classification']];
                }

                //Verifica se foi passado id para filtro
                if (isset($data['id'])){
                    //Verifica se id é array se não converte o ítem
                    if (!is_array($data['id'])){
                        $data['id'] = [$data['id']];
                    }
                    //Realiza consulta sando id de eventos como parâmetro alêm do id de categorias
                    $eventsId= EventSelectedClassification::whereIn('event_selected_classification.event_classification_id', $data['classification'])->whereIn('event_id', $data['id'])->select(DB::raw('event_selected_classification.event_id as id'))->get()->toArray();
                } else {
                    //Realiza consulta somente com id de categoria    
                    $eventsId= EventSelectedClassification::whereIn('event_selected_classification.event_classification_id', $data['classification'])->select(DB::raw('event_selected_classification.event_id as id'))->get()->toArray();    
                }

                //Verifica resultado
                if (count($eventsId) > 0){
                    $data['id'] = array_column($eventsId, 'id');
                } else {
                    //Não é possível interromper o processo :(
                    $data['id'] = ['0'];
                }
            }
            

            if (isset($data['city'])){
                //Verifica se id é array se não converte o ítem
                if (!is_array($data['city'])){
                    $data['city'] = [$data['city']];
                }

                $eventsId = City::whereIn('name', $data['city'])->select(DB::raw('city.id as id'))->get()->toArray();

                //Verifica resultado
                if (count($eventsId) > 0){
                    $data['city_id'] = array_column($eventsId, 'id');
                } else {
                    //Não é possível interromper o processo :(
                    $data['city_id'] = ['0'];
                }
            }


            if (isset($data['id'])){
                if (!is_array($data['id'])){
                    $data['id'] = [$data['id']];
                } 
                $query->whereIn('event.id', $data['id']);
            }

            if (isset($data['slug'])){
                if (!is_array($data['slug'])){
                    $data['slug'] = [$data['slug']];
                } 
                $query->whereIn('event.slug', $data['slug']);
            }

            if (isset($data['destaque'])){
                $query->where('event.destaque', '=', $data['destaque']);
            }
            
            if (isset($data['nome'])){
                $query->where('event.nome', 'like', "%$data[name]%");                            
            }

            if (isset($data['city_id'])){
                if (!is_array($data['city_id'])){
                    $data['city_id'] = [$data['city_id']];
                }
                $query->whereIn('event.city_id', $data['city_id']);                            
            }
            //print($data['revision_status_id']);exit();
            if (isset($data['revision_status_id'])){
                if (!is_array($data['revision_status_id'])){
                    $data['revision_status_id'] = [$data['revision_status_id']];
                }
                $query->whereIn('event.revision_status_id', $data['revision_status_id']);                            
            }

            if (!isset($data['inicio']) || !isset($data['fim'])) {
                $data['exclusive'] = true;
            }

            if (isset($data['exclusive']) && $data['exclusive'] == true){

                if (isset($data['inicio'])){
                    $query->where('start_date.date', '>=', $data['inicio']);  
                }
    
                if (isset($data['fim'])){
                    $query->where('end_date.date', '<=', $data['fim']);  
                }
            } else {
                if (isset($data['inicio'])){
                    $query->where('start_date.date', '<=', $data['fim']);  
                }
    
                if (isset($data['fim'])){
                    $query->where('end_date.date', '>=', $data['inicio']);  
                }
            }

        })
        ->join('event_date as start_date', 'start_date.event_id', '=', 'event.id')
        ->join('event_date as end_date', 'end_date.event_id', '=', 'event.id')
        ->select(DB::raw('SQL_CALC_FOUND_ROWS event.*, min(start_date.date) as inicio, max(end_date.date) as fim'))
        ->groupBy('event.id')
        ->orderBy('start_date.date', 'asc')
        //->toSql();

        ->paginate($paginate);

    }

    //Relacionamento de 1 para muitos
    /**
     * Retorna Fotos
     *
     * @param boolean $is_cover caso verdadeiro usa fitro para retornar apenas foto de capa
     * @return void
     */
    public function EventPhotos($is_cover=false){
        if(!$is_cover){
            return $this->hasMany(EventPhoto::class, 'event_id', 'id');
        } else {
            return $this->hasMany(EventPhoto::class, 'event_id', 'id')->where('is_cover', '1');    
        }     
    }


    public static $m_PhotoRule = array
    (
        'foto' => 'image'
    );

    public static $m_Rules = array
    (
        'datas' => 'required',
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'datas' => 'required',
    );

    public static function getDT($p_Date, $p_UpdatedAt, $p_City, $p_Name, $p_Circuit, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = Event::join('event_date as start_date', 'start_date.event_id', '=', 'event.id')
	                    ->join('event_date as end_date', 'end_date.event_id', '=', 'event.id')
	                    ->join('event_selected_categories', 'event_selected_categories.event_id', '=', 'event.id')
                        ->join('event_category', 'event_category.id', '=', 'event_selected_categories.event_category_id')
	                    ->join('city', 'city.id', '=', 'event.city_id')
                        ->join('revision_status', 'revision_status.id', '=', 'event.revision_status_id')
                        ->leftJoin('touristic_circuit','event.touristic_circuit_id',  '=',  'touristic_circuit.id')
                        ->select(DB::raw('SQL_CALC_FOUND_ROWS event.id, event.nome, event.updated_at,
                            min(start_date.date) as start, max(end_date.date) as end,
                            GROUP_CONCAT(DISTINCT event_category.nome_pt SEPARATOR \', \') as categories,
                            revision_status.name as status, city.name as cidade, touristic_circuit.nome as circuito'))
                        ->groupBy('event.id');

        if(UserType::isMunicipio())
            $v_Query->where('event.city_id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('event.city_id', TouristicCircuitCities::getUserCircuitCities());
        else if (UserType::isTrade())
            $v_Query->where('event.created_by', Auth::user()->id);

        if($p_Date != '') {
            $v_Query->where(function ($p_Query) use($p_Date){
                $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_Date, 0, 10))->startOfDay()->format('Y-m-d H:i:s');
                $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_Date, 13, 23))->endOfDay()->format('Y-m-d H:i:s');
                $p_Query->whereRaw('start_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								end_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								"' . $v_StartDate . '" BETWEEN start_date.date AND end_date.date OR
								"' . $v_EndDate . '" BETWEEN start_date.date AND end_date.date');
            });
        }

        if($p_UpdatedAt != '') {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('event.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('event.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_City != '')
            $v_Query->where('city.name', 'LIKE', '%' . $p_City . '%');

        if($p_Name != '')
            $v_Query->where('event.nome', 'LIKE', '%' . $p_Name . '%');

        if($p_Circuit != '')
            $v_Query->where('touristic_circuit.id', $p_Circuit);

        if($p_Status != '')
            $v_Query->where('event.revision_status_id',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('end', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('event.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('touristic_circuit.nome', $p_Order["dir"]);                
            if($p_Order["column"] == 3)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('event.nome', $p_Order["dir"]);
            if($p_Order["column"] == 5)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        //print($v_QueryRes);
        //exit();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            $v_Date = Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['start'])->format('d/m/Y');
            if($v_QueryRes[$c_Index]['start'] != $v_QueryRes[$c_Index]['end'])
                $v_Date .=  ' - ' . Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['end'])->format('d/m/Y');
            array_push($v_Data, [
                $v_Date,
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y H:i'),
                $v_QueryRes[$c_Index]['circuito'],  
                $v_QueryRes[$c_Index]['cidade'],
                $v_QueryRes[$c_Index]['nome'],
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                    '<a href="' . url('admin/eventos/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success" style="padding:0px 0px; width:24px;"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/eventos/editar/' . $v_QueryRes[$c_Index]['id'] . '/duplicar') . '" title="Duplicar" type="button" class="btn btn-success" style="padding:0px 0px; width:24px;"><i class="fa fa-tags"></i></a>' .
                    '<a href="' . url('admin/eventos/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success" style="padding:0px 0px; width:24px;"><i class="fa fa-history"></i></a>' .
                    //                    ($v_IsParceiro ? '' : '<a href="' . url('admin/eventos/desativar/' . $v_QueryRes[$c_Index]['id']) . '" title="' . ($v_QueryRes[$c_Index]['active']==1? 'Desativar' : 'Ativar') . '" type="button" class="btn btn-success"><i class="fa fa-' . ($v_QueryRes[$c_Index]['active']==1? 'times' : 'check') . '"></i></a>') .
                    ($v_IsParceiro ? '' : '<a href="' . url('admin/eventos/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn" style="padding:0px 0px; width:24px;"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        
        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = Event::getTotalRows();

        if(UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = Event::where('city_id', Auth::user()->city_id)->count();
        else if(UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = Event::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else
            $v_DataTableAjax->recordsTotal = Event::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function post($p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data)
    {
        $v_EventData = $p_Data['formulario'];
        
        $v_EventData['updated_by'] = Auth::id();
        if ($p_Id == null){
            $v_EventData['created_by'] = Auth::id();
            $v_EventData['slug'] = Event::generateSlug($v_EventData['nome']);
        }

        $v_DatesData = $p_Data['datas'];
        $v_EventCities = $p_Data['municipios_participantes'];
        $v_EventCategories = $p_Data['categorias'];
        $v_Hashtags = $p_Data['hashtags'];

        if(array_key_exists('tipo_evento', $p_Data)) {
            $v_EventTypes = $p_Data['tipo_evento'];
            $v_Types = '';
            foreach($v_EventTypes as $c_Type)
                $v_Types .= $c_Type . ';';
            $v_EventData['tipo_evento'] = $v_Types;
        }
        $v_EventData['destaque'] = array_key_exists('destaque', $v_EventData) ? 1 : 0;

        array_walk($v_EventData, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        if (UserType::isTrade()){
            //Define revisão para Município
            $v_EventData['revision_status_id'] = 1;
        }
        
        $v_PreviousStatus = 0;
        if($p_Id != null)
            $v_PreviousStatus = Event::find($p_Id)->revision_status_id;

        $v_Event = Event::updateOrCreate(['id' => $p_Id], $v_EventData);

        EventCities::updateEventCities($v_Event->id, $v_EventCities);
        EventSelectedCategories::updateEventCategories($v_Event->id, $v_EventCategories);
        Hashtag::updateHashtag('event', $v_Event->id, $v_Hashtags);
        EventDate::updateEventDate($v_Event->id, $v_DatesData);

        if(!array_key_exists('tipos_viagem', $p_Data))
            $p_Data['tipos_viagem'] = [];
        TripEvents::updateTripTypes($v_Event->id, $p_Data['tipos_viagem']);

        EventPhoto::updatePhotos($v_Event->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);

        if($p_Id != null && $v_PreviousStatus != 4){
            $v_Item = $v_Event;
            $v_Subject = null;
            $v_Message = null;
            if($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do evento ' . $v_Item->nome . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            else if($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do evento - ' . $v_Item->nome . ' foram rejeitadas.</p>';
                if(!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'Evento', $v_Item->id);
                foreach($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                        function ($message) use ($c_User, $v_Subject)
                        {
                            $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                        });
                }
            }
        }
        return $v_Event;
    }

    public static function generateSlug($p_String)
    {
        $v_Slug = Str::slug($p_String);
        if(Event::where('slug', $v_Slug)->count() == 0)
            return $v_Slug;

        $v_UniqueSlug = false;
        $v_Index = 0;
        while(!$v_UniqueSlug){
            if(Event::where('slug', $v_Slug . '-' . $v_Index)->count() == 0)
                $v_UniqueSlug = true;
            else
                $v_Index++;
        }

        return $v_Slug . '-' . $v_Index;
    }

    public static function getMainEvents($p_Quantity)
    {
        return Event::join('event_date', 'event_date.event_id', '=', 'event.id')
                    ->leftJoin(DB::raw('(SELECT event_id, is_cover, url FROM `event_photo` WHERE is_cover = 1) photo'), function($join) {
                        $join->on('event.id', '=', 'photo.event_id');
                    })
                    ->where('event.revision_status_id', 4)
                    ->where('event_date.date', '>=', Carbon::now()->startOfDay()->format('Y-m-d H:i:s'))
                    ->select(['event.nome', 'event.slug', 'event.descricao_curta', 'photo.url'])
                    ->orderBy('destaque', 'DESC')
                    ->orderBy('event_date.date')
                    ->orderBy('photo.is_cover','DESC')
                    ->orderByRaw('RAND()')
                    ->groupBy('event_date.event_id')
                    ->take($p_Quantity)
                    ->get();
    }

    public static function getEvent($p_Slug)
    {
        return Event::join('city', 'city.id', '=', 'event.city_id')
                    ->leftJoin(DB::raw('(SELECT event_id, is_cover, url FROM `event_photo` WHERE is_cover = 1) photo'), function($join) {
                        $join->on('event.id', '=', 'photo.event_id');
                    })
                    ->where('event.revision_status_id', 4)
                    ->where('event.slug', $p_Slug)
                    ->select(['event.*', 'photo.url', 'city.name as cidade'])
                    ->firstOrFail();
    }

    public static function getFilteredEvents($p_CityId, $p_Date, $p_Quantity)
    {
        $v_Query = Event::join('event_date as start_date', 'start_date.event_id', '=', 'event.id')
                        ->join('event_date as end_date', 'end_date.event_id', '=', 'event.id')
                        ->leftJoin(DB::raw('(SELECT event_id, is_cover, url FROM `event_photo` WHERE is_cover = 1) photo'), function($join) {
                            $join->on('event.id', '=', 'photo.event_id');
                        })
                        ->where('event.revision_status_id', 4);

        if(isset($p_CityId) && strlen($p_CityId) > 0)
        {
            $v_Query->leftJoin('event_cities', 'event_cities.event_id', '=', 'event.id')
                ->where(function ($p_Query) use($p_CityId){
                    $p_Query->where('event_cities.city_id', $p_CityId)
                        ->orWhere('event.city_id', $p_CityId);
                });
        }

        if(strlen($p_Date) > 0) {
            $v_Query->where(function ($p_Query) use($p_Date){
                $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_Date, 0, 10))->startOfDay()->format('Y-m-d H:i:s');
                $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_Date, 13, 23))->endOfDay()->format('Y-m-d H:i:s');
                $p_Query->whereRaw('(start_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								end_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								"' . $v_StartDate . '" BETWEEN start_date.date AND end_date.date OR
								"' . $v_EndDate . '" BETWEEN start_date.date AND end_date.date)');
            });
        }
        else
            $v_Query->where('start_date.date', '>=', Carbon::now()->startOfDay()->format('Y-m-d H:i:s'));

        return $v_Query->select(['event.nome', 'event.slug', 'event.city_id', 'event.descricao_curta', 'photo.url'])
                       ->orderBy('destaque', 'desc')
                       ->orderBy('start_date.date')
                       ->orderBy('photo.is_cover','DESC')
                       ->groupBy('event.slug')
                       ->take($p_Quantity)
                       ->get();
    }

    public static function deleteEvent($p_Id)
    {
        $v_Query = Event::where('id',$p_Id);
        if(UserType::isMunicipio())
            $v_Query->where('event.city_id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('event.city_id', TouristicCircuitCities::getUserCircuitCities());
        $v_Event = $v_Query->firstOrFail();
        EventPhoto::deletePhotos($p_Id);
        $v_Event->delete();
    }

    public static function getReport($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_CategoryId = $p_Data['category_id'];
        $v_ClassificationId = $p_Data['classification_id'];
        $v_Date = $p_Data['period'];
        $v_Hashtag = $p_Data['hashtag'];
        

        $v_Query = Event::with('eventDates')
                        ->with('categories')
                        ->with('classification')
                        ->with('hashtags')
                        ->with('cities')
                        ->join('city', 'event.city_id', '=', 'city.id')
                        ->leftJoin('touristic_circuit', 'event.touristic_circuit_id', '=', 'touristic_circuit.id')
                        ->join('event_date as start_date', 'start_date.event_id', '=', 'event.id')
                        ->join('event_date as end_date', 'end_date.event_id', '=', 'event.id')
                        ->join('revision_status', 'revision_status.id', '=', 'event.revision_status_id')
                        ->leftJoin('event_selected_categories', 'event_selected_categories.event_id', '=', 'event.id')
                        ->leftJoin('event_selected_classification', 'event_selected_classification.event_id', '=', 'event.id')
                        ->leftJoin('event_hashtag', 'event_hashtag.event_id', '=', 'event.id')
                        ->leftJoin('hashtag', 'event_hashtag.hashtag_id', '=', 'hashtag.id')
                        ->leftJoin('event_cities', 'event_cities.event_id', '=', 'event.id')
                        ->select('event.*', 'city.name as cidade', 'touristic_circuit.nome as circuito', 'revision_status.name as status')
                        ->groupBy('event.id');

        if(array_key_exists('id', $p_Data)){
            $v_Query->where(function ($p_Query) use($p_Data){
                $p_Query->whereIn('event.city_id', $p_Data['id']);
            });
        }

        if($v_CircuitId != '')
            $v_Query->where('event.touristic_circuit_id', $v_CircuitId);

        if($v_RegionId != '') {
            $v_Query->join('destination', 'destination.city_id', '=', 'event.city_id')
                    ->where('destination.region_id', $v_RegionId);
        }

        if($v_CategoryId != '')
            $v_Query->where('event_selected_categories.event_category_id', $v_CategoryId);
        
        if($v_ClassificationId != '')
            $v_Query->where('event_selected_classification.event_classification_id', $v_ClassificationId);
        
        if($v_Date != '') {
            $v_Query->where(function ($p_Query) use($v_Date){
                $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($v_Date, 0, 10))->startOfDay()->format('Y-m-d H:i:s');
                $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($v_Date, 13, 23))->endOfDay()->format('Y-m-d H:i:s');
                $p_Query->whereRaw('start_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								end_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								"' . $v_StartDate . '" BETWEEN start_date.date AND end_date.date OR
								"' . $v_EndDate . '" BETWEEN start_date.date AND end_date.date');
            });
        }

        if($v_Hashtag != '')
            $v_Query->where('hashtag.name', 'LIKE', '%' . trim($v_Hashtag) . '%');

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioEventos.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->nome);
                        array_push($v_Data, $c_Result->destaque == 1 ? 'Sim' : 'Não');
                        array_push($v_Data, json_decode($c_Result->descricao_curta,1)['pt']);
                        array_push($v_Data, $c_Result->local_evento);
                        array_push($v_Data, json_decode($c_Result->sobre,1)['pt']);
                        $v_Period = '';
                        foreach($c_Result->eventDates as $c_Index => $c_Date)
                            $v_Period .= ($c_Index > 0 ? ', ' : '') . Carbon::createFromFormat('Y-m-d H:i:s', $c_Date->date)->format('d/m/Y');
                        array_push($v_Data, $v_Period);

                        $v_EventTypes = $c_Result->tipo_evento == null ? [] : explode(';', $c_Result->tipo_evento);
                        $v_Types = '';
                        foreach($v_EventTypes as $c_Index => $c_Type)
                            $v_Types .= ($c_Index > 0 ? ', ' : '') . $c_Type;
                        array_push($v_Data, $v_Types);

                        $v_Categories = '';
                        foreach($c_Result->categories as $c_Index => $c_Category)
                            $v_Categories .= ($c_Index > 0 ? ', ' : '') . $c_Category->nome_pt;
                        array_push($v_Data, $v_Categories);

                        $v_Classification = '';
                        foreach($c_Result->EventSelectedClassification as $c_Index => $c_Classification)
                            $v_Classification .=($c_Index > 0 ? ', ' : '') . $c_Classification->EventClassification->nome;
                        array_push($v_Data, $v_Classification);

                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        $v_Cities = '';
                        foreach($c_Result->cities as $c_Index => $c_City)
                            $v_Cities .= ($c_Index > 0 ? ', ' : '') . $c_City->name;
                        array_push($v_Data, $v_Cities);
                        array_push($v_Data, $c_Result->cep);
                        array_push($v_Data, $c_Result->bairro);
                        array_push($v_Data, $c_Result->logradouro);
                        array_push($v_Data, $c_Result->numero);
                        array_push($v_Data, $c_Result->complemento);
                        array_push($v_Data, $c_Result->latitude);
                        array_push($v_Data, $c_Result->longitude);
                        array_push($v_Data, $c_Result->site);
                        array_push($v_Data, $c_Result->telefone);
                        array_push($v_Data, $c_Result->email);
                        array_push($v_Data, $c_Result->facebook);
                        array_push($v_Data, $c_Result->instagram);
                        array_push($v_Data, $c_Result->twitter);
                        array_push($v_Data, $c_Result->youtube);
                        array_push($v_Data, $c_Result->flickr);
                        array_push($v_Data, json_decode($c_Result->informacoes_uteis,1)['pt']);

                        $v_RelevantData =  json_decode($c_Result->dados_relevantes,1)['pt'];
                        $v_RelevantDataString = '';
                        
                        foreach($v_RelevantData as $c_Index => $c_Data)
                        {
                            if(!empty($c_Data['dado']) && !empty($c_Data['descricao']))
                                $v_RelevantDataString .= ($c_Index > 0 ? '; ' : '') . $c_Data['dado'] . ' ' . $c_Data['descricao'];
                        }
                        array_push($v_Data, $v_RelevantDataString);

                        $v_Hashtags = '';
                        foreach($c_Result->hashtags as $c_Index => $c_Hashtag)
                            $v_Hashtags .= ($c_Index > 0 ? ', ' : '') . $c_Hashtag->name;
                        array_push($v_Data, $v_Hashtags);
                        array_push($v_Data, $c_Result->revision_status_id == 4 ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->status);

                        $sheet->row($v_CurrentRow, $v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                }
                $sheet->setBorder('A2:AF'.(count($v_Results)+1), 'thin');
            });
        })->download('xlsx');
    }

    public static function getCalendarReportData($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_Date = $p_Data['period'];

        $v_Query = Event::with('eventDates')
            ->with('categories')
            ->with('classification')
            ->join('city', 'event.city_id', '=', 'city.id')
            ->leftJoin('touristic_circuit', 'event.touristic_circuit_id', '=', 'touristic_circuit.id')
            ->join('event_date as start_date', 'start_date.event_id', '=', 'event.id')
            ->join('event_date as end_date', 'end_date.event_id', '=', 'event.id')
            ->leftJoin('event_selected_categories', 'event_selected_categories.event_id', '=', 'event.id')
            ->leftJoin('event_selected_classification', 'event_selected_classification.event_id', '=', 'event.id')
            ->leftJoin('event_cities', 'event_cities.event_id', '=', 'event.id')
            ->select('event.*', 'city.name as cidade', 'touristic_circuit.nome as circuito')
            ->groupBy('event.id')
            ->orderBy('start_date.date')
            ->orderBy('event.nome');

        if(array_key_exists('id', $p_Data)){
            $v_Query->where(function ($p_Query) use($p_Data){
                $p_Query->whereIn('event_cities.city_id', $p_Data['id'])
                    ->orWhereIn('event.city_id', $p_Data['id']);
            });
        }

        if($v_CircuitId != '')
            $v_Query->where('event.touristic_circuit_id', $v_CircuitId);

        if($v_RegionId != '') {
            $v_Query->join('destination', 'destination.city_id', '=', 'event.city_id')
                ->where('destination.region_id', $v_RegionId);
        }

        if(array_key_exists('category_id', $p_Data))
            $v_Query->whereIn('event_selected_categories.event_category_id', $p_Data['category_id']);
        
        if(array_key_exists('classification_id', $p_Data))
            $v_Query->whereIn('event_selected_classification.event_classification_id', $p_Data['classification_id']);
        

        if(array_key_exists('event_type_id', $p_Data)){
            $v_Query->where(function ($p_Query) use($p_Data){
                foreach ($p_Data['event_type_id'] as $c_EventType){
                    $p_Query->orWhere('event.tipo_evento', "LIKE", '%' . $c_EventType . '%');
                }
            });
        }

        if($v_Date != '') {
            $v_Query->where(function ($p_Query) use($v_Date){
                $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($v_Date, 0, 10))->startOfDay()->format('Y-m-d H:i:s');
                $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($v_Date, 13, 23))->endOfDay()->format('Y-m-d H:i:s');
                $p_Query->whereRaw('start_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								end_date.date BETWEEN "' . $v_StartDate . '" AND "' . $v_EndDate . '" OR
								"' . $v_StartDate . '" BETWEEN start_date.date AND end_date.date OR
								"' . $v_EndDate . '" BETWEEN start_date.date AND end_date.date');
            });
        }
        
        //Somente regitros aprovados
        $v_Query->where('revision_status_id', 4);

        return $v_Query->get();
    }

    public function eventDates()
    {
        return $this->hasMany('App\EventDate')->orderBy('date');
    }
    
    public function classification()
    {
         return $this->hasMany('App\EventSelectedClassification')
            ->join('event_classification', 'event_classification.id', '=', 'event_selected_classification.event_classification_id')
            ->select('event_selected_classification.event_id', 'event_classification.nome')
            ->orderBy('nome');       
    }

    public function categories()
    {
        return $this->hasMany('App\EventSelectedCategories')
            ->join('event_category', 'event_category.id', '=', 'event_selected_categories.event_category_id')
            ->select('event_selected_categories.event_id', 'event_category.nome_pt')
            ->orderBy('nome_pt');
    }

    public function hashtags()
    {
        return $this->hasMany('App\EventHashtag')
                    ->join('hashtag', 'hashtag.id', '=', 'event_hashtag.hashtag_id')
                    ->select('event_hashtag.event_id', 'hashtag.name')
                    ->orderBy('name');
    }

    public function cities()
    {
        return $this->hasMany('App\EventCities')
                    ->join('city', 'city.id', '=', 'event_cities.city_id')
                    ->select('event_cities.event_id', 'city.name')
                    ->orderBy('name');
    }

}
