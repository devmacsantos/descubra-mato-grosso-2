<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    public $timestamps = false;
    protected $table = 'event_category';
    protected $guarded = [];

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:300'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:300'
    );


    public function EventSelectedCategories(){
        return $this->hasMany(EventSelectedCategories::class, 'event_category_id', 'id');
    }

    public static function getFilterList($p_Language)
    {
        $v_List = EventCategory::orderBy('nome_' . $p_Language)->lists('nome_' . $p_Language, 'id')->toArray();
        return ['' => trans('string.category')] + $v_List;
    }

    public static function post($p_Id, $p_Data)
    {
        EventCategory::updateOrCreate(['id' => $p_Id], $p_Data['idioma']);
    }

    public static function getList()
    {
        return EventCategory::orderBy('nome_pt')->lists('nome_pt', 'id')->toArray();
    }
}