<?php
namespace App;

use App\Http\Controllers\BaseController;
use Illuminate\Database\Eloquent\Model;

class PermanentEvent extends Model
{
    public $timestamps = false;
    protected $table = 'permanent_event';
    protected $guarded = [];

    public static function post($p_CityInfoId, $p_Data)
    {
        PermanentEvent::where('city_info_id', $p_CityInfoId)->delete();
        $v_Events = json_decode($p_Data,1);
        foreach($v_Events as $c_Event){
            PermanentEvent::create([
                'city_info_id' => $p_CityInfoId,
                'nome' => $c_Event['nome'],
                'descricao' => $c_Event['descricao'],
                'periodicidade' => $c_Event['periodicidade'],
                'periodo' => json_encode($c_Event['periodo']),
                'realizacao' => $c_Event['realizacao'],
                'responsavel_realizacao' => $c_Event['responsavel_realizacao'],
                'tema' => $c_Event['tema'],
                'tipo' => $c_Event['tipo'],
                'municipios_participantes' => json_encode($c_Event['municipios_participantes']),
                'observacao' => $c_Event['observacao']
            ]);
        }
    }

    public static function getCityEvents($p_CityInfoId)
    {
        return PermanentEvent::where('city_info_id', $p_CityInfoId)->get();
    }

    public static function getReport($p_Data)
    {
        $v_EventCategories = EventCategory::getList();
        $v_AllCities = City::getList();

        $v_Query = PermanentEvent::join('city_info', 'permanent_event.city_info_id', '=', 'city_info.id')
            ->join('city', 'city_info.city_id', '=', 'city.id')
            ->select('permanent_event.*', 'city.name as cidade')
            ->groupBy('permanent_event.id');
        
        if(array_key_exists('id', $p_Data)){
            $v_Query->where(function ($p_Query) use($p_Data){
                $p_Query->whereIn('city.id', $p_Data['id']);
                foreach($p_Data['id'] as $c_CityId) {
                    $p_Query->orWhere('municipios_participantes', 'LIKE', '%"' . $c_CityId . '"%');
                }
            });
        }
        if(array_key_exists('periodicidade', $p_Data)){
            $v_Query->whereIn('periodicidade', $p_Data['periodicidade']);
        }
        if(array_key_exists('realizacao', $p_Data)){
            $v_Query->whereIn('realizacao', $p_Data['realizacao']);
        }
        if(array_key_exists('meses', $p_Data)){
            $v_Query->where(function ($p_Query) use($p_Data){
                foreach($p_Data['meses'] as $c_Month) {
                    $p_Query->orWhere('periodo', 'LIKE', '%"' . $c_Month . '"%');
                }
            });
        }
        if(array_key_exists('category_id', $p_Data)){
            $v_Query->whereIn('tipo', $p_Data['category_id']);
        }

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioEventosPermanentes.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results, $v_EventCategories, $v_AllCities){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results, $v_EventCategories, $v_AllCities) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->nome);
                        array_push($v_Data, $c_Result->descricao);
                        array_push($v_Data, $c_Result->periodicidade);

                        $v_EventMonths = json_decode($c_Result->periodo,1);
                        $v_Months = '';
                        foreach($v_EventMonths as $c_Index => $c_Month){
                            if(!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);

                        array_push($v_Data, $c_Result->realizacao);
                        array_push($v_Data, $c_Result->responsavel_realizacao);
                        array_push($v_Data, $c_Result->tema);
                        array_push($v_Data, $c_Result->tipo ? $v_EventCategories[$c_Result->tipo] : '');

                        $v_EventCities = json_decode($c_Result->municipios_participantes,1);
                        $v_Cities = '';
                        foreach($v_EventCities as $c_Index => $c_City){
                            $v_Cities .= ($c_Index > 0 ? ', ' : '') . $v_AllCities[$c_City];
                        }
                        array_push($v_Data, $v_Cities);

                        array_push($v_Data, $c_Result->observacao);

                        $sheet->row($v_CurrentRow, $v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:K'.($v_CurrentRow-1), 'thin');
            });
        })->download('xlsx');
    }
}