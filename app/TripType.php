<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class TripType extends Model
{
    public $timestamps = false;
    protected $table = 'trip_type';
    protected $guarded = [];

    public static function getList()
    {
        return TripType::orderBy('nome_pt')->lists('nome_pt', 'id')->toArray();
    }

    public static function getTypeList()
    {
        return TripType::select(['id', 'nome_pt'])->orderBy('nome_pt')->get();
    }

    public static function post($p_Id, $p_CoverPhoto, $p_Data)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/tipo-viagem/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_TripData = $p_Data['formulario'];
        $v_PhotoUrl = null;
        if($p_CoverPhoto != null)
        {
            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $p_CoverPhoto->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $p_CoverPhoto->encode('jpg')->save($v_Path . $v_PhotoName);
            $v_PhotoUrl = url('/imagens/tipo-viagem/' . $v_PhotoName);
        }

        $v_TripType = TripType::findOrNew($p_Id);
        if($v_PhotoUrl != null)
        {
            if($v_TripType->foto_capa_url != null)
            {
                $v_OldFileName = explode('/', $v_TripType->foto_capa_url);
                $v_OldFileName = array_pop($v_OldFileName);
                \File::delete($v_Path . $v_OldFileName);
            }
            $v_TripData['foto_capa_url'] = $v_PhotoUrl;
        }
        if($p_Id == null)
            $v_TripData['slug'] = TripType::generateSlug($v_TripData['nome_pt']);
        $v_TripData['publico'] = array_key_exists('publico', $v_TripData) ? 1 : 0;
        $v_TripData['destaque'] = array_key_exists('destaque', $v_TripData) ? 1 : 0;
        TripType::updateOrCreate(['id' => $p_Id], $v_TripData);
    }

    public static function generateSlug($p_String)
    {
        $v_Slug = Str::slug($p_String);
        if(TripType::where('slug', $v_Slug)->count() == 0)
            return $v_Slug;

        $v_UniqueSlug = false;
        $v_Index = 0;
        while(!$v_UniqueSlug){
            if(TripType::where('slug', $v_Slug . '-' . $v_Index)->count() == 0)
                $v_UniqueSlug = true;
            else
                $v_Index++;
        }

        return $v_Slug . '-' . $v_Index;
    }

    public static function getCategoryTypes($p_CategoryId, $p_Quantity)
    {
        return TripType::where('trip_type.trip_category_id', $p_CategoryId)
                           ->where('trip_type.publico', 1)
                           ->orderBy('destaque', 'desc')
                           ->orderByRaw('RAND()')->take($p_Quantity)->get();
    }

    public static function getHeaderTypes($p_Quantity)
    {
        return TripType::join('trip_category', 'trip_category.id', '=', 'trip_type.trip_category_id')
                           ->where('trip_type.publico', 1)
                           ->select(['trip_type.nome', 'trip_type.slug as slug', 'trip_category.slug as category_slug'])
                           ->orderBy('trip_type.destaque', 'desc')
                           ->orderByRaw('RAND()')->take($p_Quantity)->get();
    }

    public static function getType($p_Slug)
    {
        return TripType::where('slug', $p_Slug)
                           ->where('trip_type.publico', 1)
                           ->select(['id', 'nome', 'foto_capa_url', 'descricao'])
                           ->firstOrFail();
    }

    public static function getTripTypePlaces($p_TripTypeId, $p_Quantity, $p_IsPark = null)
    {
        $v_Destinations = TripDestinations::getTripDestinations($p_TripTypeId, $p_Quantity);
        foreach($v_Destinations as $c_Item)
            $c_Item->tipo = 'destinos';
        if($p_IsPark)
            $v_Attractions = TripAttractions::getTripParks($p_TripTypeId, $p_Quantity);
        else
            $v_Attractions = TripAttractions::getTripAttractions($p_TripTypeId, $p_Quantity);
        foreach($v_Attractions as $c_Item)
            $c_Item->tipo = 'atracoes';
        $v_TouristicRoutes = TripTouristicRoutes::getTripTouristicRoutes($p_TripTypeId, $p_Quantity);
        foreach($v_TouristicRoutes as $c_Item)
            $c_Item->tipo = 'roteiros';
        $v_Events = TripEvents::getTripEvents($p_TripTypeId, $p_Quantity);
        foreach($v_Events as $c_Item)
            $c_Item->tipo = 'eventos';

        $v_Places = array_merge($v_Destinations->toArray(), $v_Attractions->toArray(), $v_TouristicRoutes->toArray(), $v_Events->toArray());
        shuffle($v_Places);
        $v_Places = array_slice($v_Places, 0, $p_Quantity);
        return $v_Places;
    }

    public static function getWhereToGoPlacesFilter($p_TripTypeId, $p_DestinationId, $p_Quantity)
    {
        if(!empty($p_DestinationId)) {
            $p_CityId = Destination::getCityId($p_DestinationId);
            $v_Destinations = TripDestinations::getTripAndCityDestinations($p_TripTypeId, $p_CityId, $p_Quantity);
            $v_Attractions = TripAttractions::getTripAndCityParks($p_TripTypeId, $p_CityId, $p_Quantity);
            $v_TouristicRoutes = TripTouristicRoutes::getTripAndDestinationTouristicRoutes($p_TripTypeId, $p_DestinationId, $p_Quantity);
        }
        else {
            $v_Destinations = TripDestinations::getTripDestinations($p_TripTypeId, $p_Quantity);
            $v_Attractions = TripAttractions::getTripParks($p_TripTypeId, $p_Quantity);
            $v_TouristicRoutes = TripTouristicRoutes::getTripTouristicRoutes($p_TripTypeId, $p_Quantity);
        }
        foreach($v_Destinations as $c_Item)
            $c_Item->tipo = 'destinos';
        foreach($v_Attractions as $c_Item)
            $c_Item->tipo = 'atracoes';
        foreach($v_TouristicRoutes as $c_Item)
            $c_Item->tipo = 'roteiros';

        $v_Places = array_merge($v_Destinations->toArray(), $v_Attractions->toArray(), $v_TouristicRoutes->toArray());
        shuffle($v_Places);
        $v_Places = array_slice($v_Places, 0, $p_Quantity);
        return $v_Places;
    }

    public static function getFilterList()
    {
        return TripType::where('publico', 1)
                       ->select(['id', 'trip_category_id', 'nome'])
                       ->orderBy('trip_category_id')
                       ->get();
    }

    public static function getCategoryTypesBySlugs($p_CategorySlugs)
    {
        $v_Slugs = $p_CategorySlugs ? explode(',', $p_CategorySlugs) : [];
        return TripType::leftJoin('trip_category', 'trip_category.id', '=', 'trip_type.trip_category_id')
                       ->whereIn('trip_category.slug', $v_Slugs)
                       ->where('trip_type.publico', 1)
                       ->select('trip_type.id')
                       ->lists('id')->toArray();
    }
}