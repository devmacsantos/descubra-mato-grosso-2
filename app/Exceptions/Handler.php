<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Request;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
        ModelNotFoundException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        if(strpos(url('/'), '8000') === false && strpos(url('/'), 'minasgerais') !== false) {
            if (!$e instanceof ModelNotFoundException) {
                if ($e instanceof \Exception && $e->getMessage() != '') {
                    $v_Exception = 'Exceção: ' . get_class($e) . '<br>';
                    $v_Exception .= 'Mensagem: ' . $e->getMessage() . '<br>';
                    if(Auth::check()){
                        $v_User = Auth::user();
                        $v_Exception .= '- ID usuário: ' . $v_User->id . '<br>';
                        $v_Exception .= '- Nome: ' . $v_User->name . '<br>';
                        $v_Exception .= '- Tipo: ' . $v_User->user_type_id . '<br>';
                    }
                    $v_Exception .= 'Arquivo: ' . $e->getFile() . '<br>';
                    $v_Exception .= 'Linha: ' . $e->getLine() . '<br>';
                    $v_Exception .= 'URL: ' . Request::url() . '<br>';
                    $v_Exception .= 'Método: ' . Request::method() . '<br>';
                    $v_Exception .= 'Parâmetros: ' . json_encode(Request::input()) . '<br>';
                    $v_Exception .= 'Arquivos: ' . (isset($_FILES) ? json_encode($_FILES) : '[]') . '<br><br><br>';
                    $v_Exception .= 'Stack trace: ' . str_replace('#', '<br><br>#', $e->getTraceAsString());

                    Mail::send('emails.exception', ['p_Error' => $v_Exception], function ($m) {
                        $m->to('gbelisario@gmail.com', 'SETUR')->subject('Erro no sistema SETUR');
                    });
                }
            }
        }
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return redirect( '/' );
        }
        //TODO arrumar tratamento de erros
        if ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
        }

        return parent::render($request, $e);
    }
}
