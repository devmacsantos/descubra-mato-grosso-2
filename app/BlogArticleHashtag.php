<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogArticleHashtag extends Model
{
    public $timestamps = false;
    protected $table = 'blog_article_hashtag';

    public static function deleteArticleHashtags($p_ArticleId)
    {
        BlogArticleHashtag::where('blog_article_id', $p_ArticleId)->delete();
    }

    public static function addArticleHashtag($p_ArticleId, $p_HashtagId)
    {
        $v_ArticleHashtag = new BlogArticleHashtag();
        $v_ArticleHashtag->blog_article_id = $p_ArticleId;
        $v_ArticleHashtag->hashtag_id = $p_HashtagId;
        $v_ArticleHashtag->save();
    }

    public static function getList($p_ArticleId)
    {
        return BlogArticleHashtag::join('hashtag', 'hashtag.id', '=', 'blog_article_hashtag.hashtag_id')
            ->where('blog_article_id', $p_ArticleId)->lists('hashtag.name', 'hashtag.name')->toArray();
    }

    public static function getRandomHashtags($p_Quantity)
    {
        $v_Hashtags = BlogArticleHashtag::join('hashtag', 'hashtag.id', '=', 'blog_article_hashtag.hashtag_id')
            ->groupBy('hashtag.id')
            ->lists('hashtag.name', 'hashtag.name')
            ->toArray();

        shuffle($v_Hashtags);
        $v_Hashtags = array_slice($v_Hashtags, 0, $p_Quantity);
        return $v_Hashtags;
    }

    public static function getHashtagArticles($p_Hashtag, $p_Quantity)
    {
        return BlogArticleHashtag::join('blog_article', 'blog_article_hashtag.blog_article_id', '=', 'blog_article.id')
                                 ->join('hashtag', 'hashtag.id', '=', 'blog_article_hashtag.hashtag_id')
                                 ->where('hashtag.name', $p_Hashtag)
                                 ->select(['blog_article.id', 'blog_article.created_at', 'blog_article.foto_capa_url',
                                     'blog_article.slug', 'blog_article.titulo', 'blog_article.descricao_curta'])
                                 ->orderBy('blog_article.created_at', 'desc')
                                 ->groupBy('blog_article.id')->take($p_Quantity)
                                 ->get();
    }
}
