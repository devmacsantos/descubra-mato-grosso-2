<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Hashtag extends Model
{
    public $timestamps = false;
    protected $table = 'hashtag';

    public static function deleteHashtags($p_PhotoId)
    {
        foreach(Hashtag::where('photo_id', '=', $p_PhotoId)->get() as $_Hashtag)
            $_Hashtag->delete();
    }

    public static function updateHashtag($p_ObjectType, $p_ObjectId, $p_Hashtags)
    {
        if($p_ObjectType == 'blog')
            BlogArticleHashtag::deleteArticleHashtags($p_ObjectId);
        else if ($p_ObjectType == 'event')
            EventHashtag::deleteEventHashtags($p_ObjectId);
        else if ($p_ObjectType == 'destination')
            DestinationHashtag::deleteDestinationHashtags($p_ObjectId);
        else if ($p_ObjectType == 'attraction')
            AttractionHashtag::deleteAttractionHashtags($p_ObjectId);
        else if ($p_ObjectType == 'touristicRoute')
            TouristicRouteHashtag::deleteTouristicRouteHashtags($p_ObjectId);
        foreach($p_Hashtags as $c_Hashtag)
        {
            $v_Name = str_replace('#', '', $c_Hashtag);
            $v_Hashtag = Hashtag::where('name', $v_Name)->first();
            if($v_Hashtag == null)
            {
                $v_Hashtag = new Hashtag();
                $v_Hashtag->name = $v_Name;
                $v_Hashtag->save();
            }
            if($p_ObjectType == 'blog')
                BlogArticleHashtag::addArticleHashtag($p_ObjectId, $v_Hashtag->id);
            else if ($p_ObjectType == 'event')
                EventHashtag::addEventHashtag($p_ObjectId, $v_Hashtag->id);
            else if ($p_ObjectType == 'destination')
                DestinationHashtag::addDestinationHashtag($p_ObjectId, $v_Hashtag->id);
            else if ($p_ObjectType == 'attraction')
                AttractionHashtag::addAttractionHashtag($p_ObjectId, $v_Hashtag->id);
            else if ($p_ObjectType == 'touristicRoute')
                TouristicRouteHashtag::addTouristicRouteHashtag($p_ObjectId, $v_Hashtag->id);
        }
    }


    public static function getHashtagIds($p_Pattern)
    {
        $v_Exclusions = ['e', 'a', 'as', 'o', 'os', 'de', 'do', 'dos', 'da', 'das'];
        //Remove caracteres especiais da busca
        $v_LowerCasePattern = strtolower(preg_replace('/[-+"~]/ui', ' ', $p_Pattern));

        if(in_array($v_LowerCasePattern, $v_Exclusions)){
            return Hashtag::where('id', 0)->lists('id')->toArray();
        }

        //$v_LowerCasePattern = str_replace(' ','* *',$v_LowerCasePattern);
        //print_r(Hashtag::where('name', 'LIKE', "%{$p_Pattern}%")->lists('id')->toArray());
        //exit();
        //return Hashtag::where('name', 'LIKE', "%{$p_Pattern}%")->lists('id')->toArray();
        return Hashtag::whereRaw('MATCH (name) AGAINST ("*' . $v_LowerCasePattern . '*" IN BOOLEAN MODE)')->lists('id')->toArray();
    }

//
//    public static function getHashtagCount($p_Hashtag)
//    {
//        return Hashtag::where('hashtag', '=', $p_Hashtag)->count();
//    }
//
//    public static function getPhotos($p_Hashtag, $p_Offset, $p_Fetch)
//    {
//        $v_Query = Hashtag::where('hashtag', '=', $p_Hashtag);
//        $v_Query->join('photo', 'photo.id', '=', 'hashtag.photo_id');
//        $v_Query->join('model', 'model.id', '=', 'photo.model_id');
//        $v_Query->join('user', 'user.id', '=', 'model.user_id');
//        if($p_Offset != null)
//            $v_Query->skip($p_Offset);
//        if($p_Fetch != null)
//            $v_Query->take($p_Fetch);
//        $v_Query->select([
//            \DB::raw('user.id as user_id'),
//            'user.profile_photo',
//            'user.name',
//            'model.nickname',
//            'photo.id',
//            'photo.model_id',
//            'photo.description',
//            'photo.file_name',
//            'photo.date',
//            'photo.credits',
//            \DB::raw('(SELECT COUNT(*) FROM photo_like where photo_id = photo.id) AS likes'),
//            \DB::raw('if((SELECT COUNT(*) FROM photo_like where photo_id = photo.id and user_id = ' . \Auth::id() . ') > 0, 1, 0) AS liked')
//        ]);
//        $v_Query->orderBy('date', 'DESC');
//        $v_Return = $v_Query->get();
//        return $v_Return;
//    }
}
