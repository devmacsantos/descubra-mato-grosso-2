<?php
namespace App;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class EconomicActivityAttraction extends BaseInventoryModel
{
    protected $table = 'economic_activity_attraction';

    public static function getDT($p_Name, $p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = EconomicActivityAttraction::join('city', 'city.id', '=', 'economic_activity_attraction.city_id')->join('revision_status', 'revision_status.id', '=', 'economic_activity_attraction.revision_status_id')
                                     ->select(DB::raw('SQL_CALC_FOUND_ROWS economic_activity_attraction.id, economic_activity_attraction.city_id, economic_activity_attraction.nome_popular, city.name as municipio, economic_activity_attraction.created_at, economic_activity_attraction.updated_at, revision_status.name as status'));

        if(UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());

        if($p_Name != '')
            $v_Query->where('economic_activity_attraction.nome_popular', 'like', '%' . $p_Name . '%');

        if($p_City != '')
            $v_Query->where('city.name', 'like', '%' . $p_City . '%');

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('economic_activity_attraction.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('economic_activity_attraction.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('economic_activity_attraction.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('economic_activity_attraction.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Status != '')
            $v_Query->where('economic_activity_attraction.revision_status_id',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('economic_activity_attraction.nome_popular', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('economic_activity_attraction.created_at', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('economic_activity_attraction.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 4)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['nome_popular'],
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                    '<a href="' . url('admin/inventario/atrativos-atividades-economicas/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    '<a href="' . url('admin/inventario/atrativos-atividades-economicas/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                    ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/atrativos-atividades-economicas/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = BaseInventoryModel::getTotalRows();

        if(UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = EconomicActivityAttraction::where('city_id', Auth::user()->city_id)->count();
        else if(UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = EconomicActivityAttraction::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else
            $v_DataTableAjax->recordsTotal = EconomicActivityAttraction::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function post($p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data)
    {
        if(array_key_exists('limite_numero_visitantes', $p_Data) && $p_Data['limite_numero_visitantes'] != 1)
            $p_Data['limite_numero_visitantes'] = 0;

        if(array_key_exists('visitantes_epoca_maior_fluxo', $p_Data) && $p_Data['visitantes_epoca_maior_fluxo'] != '')
        {
            $v_HighSeasonMonths = $p_Data['visitantes_epoca_maior_fluxo'];
            $v_Months = '';
            foreach($v_HighSeasonMonths as $c_Month)
                $v_Months .= $c_Month . ';';
            $p_Data['visitantes_epoca_maior_fluxo'] = $v_Months;
        }

        if(array_key_exists('visitantes_epoca_menor_fluxo', $p_Data) && $p_Data['visitantes_epoca_menor_fluxo'] != '')
        {
            $v_LowSeasonMonths = $p_Data['visitantes_epoca_menor_fluxo'];
            $v_Months = '';
            foreach($v_LowSeasonMonths as $c_Month)
                $v_Months .= $c_Month . ';';
            $p_Data['visitantes_epoca_menor_fluxo'] = $v_Months;
        }

        if(array_key_exists('visita_tipo', $p_Data) && $p_Data['visita_tipo'] != '')
        {
            $v_VisitationTypes = $p_Data['visita_tipo'];
            $v_Types = '';
            foreach($v_VisitationTypes as $c_Type)
                $v_Types .= $c_Type . ';';
            $p_Data['visita_tipo'] = $v_Types;
        }

        $p_Data['possui_espacos_eventos'] = array_key_exists('possui_espacos_eventos', $p_Data) ? 1 : 0;
        $p_Data['equipamento_fechado'] = array_key_exists('equipamento_fechado', $p_Data) ? 1 : 0;

        array_walk($p_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_PreviousStatus = 0;
        if($p_Id != null)
            $v_PreviousStatus = EconomicActivityAttraction::find($p_Id)->revision_status_id;

        $v_Item = EconomicActivityAttraction::updateOrCreate(['id' => $p_Id], $p_Data);

        InventoryPhoto::updatePhotos('C3', $v_Item->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);

        if($p_Id != null && $v_PreviousStatus != 4){
            $v_Subject = null;
            $v_Message = null;
            if($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_popular . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            else if($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_popular . ' foram rejeitadas.</p>';
                if(!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'C3', $v_Item->id);
                foreach($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                        function ($message) use ($c_User, $v_Subject)
                        {
                            $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                        });
                }
            }
        }

        return $v_Item;
    }

    public static function deleteEconomicActivityAttraction($p_Id)
    {
        $v_Query = EconomicActivityAttraction::where('id',$p_Id);
        if(UserType::isMunicipio())
            $v_Query->where('city_id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
        $v_Item = $v_Query->firstOrFail();

        InventoryPhoto::deletePhotos('C3', $p_Id);

        $v_Item->equipe_responsavel_observacao = 'DELETED';
        $v_Item->save();
        $v_Item->delete();
    }

    public static function getReport($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_DistrictId = $p_Data['district_id'];
        $v_TypeId = $p_Data['type_id'];
        $v_SubTypeId = $p_Data['sub_type_id'];

        $v_Query = EconomicActivityAttraction::leftJoin('city', 'city.id', '=', 'economic_activity_attraction.city_id')
                                             ->leftJoin('district', 'economic_activity_attraction.district_id', '=', 'district.id')
                                             ->leftJoin('touristic_circuit_cities', 'economic_activity_attraction.city_id', '=', 'touristic_circuit_cities.city_id')
                                             ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                                             ->leftJoin('destination', 'destination.city_id', '=', 'economic_activity_attraction.city_id')
                                             ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                                             ->leftJoin('type', 'economic_activity_attraction.type_id', '=', 'type.id')
                                             ->leftJoin('type as sub_type', 'economic_activity_attraction.sub_type_id', '=', 'sub_type.id')
                                             ->selectRaw('economic_activity_attraction.*, city.name as cidade, district.name as distrito, touristic_circuit.nome as circuito, region.name as regiao, type.name as type, sub_type.name as subtype')
                                             ->groupBy('economic_activity_attraction.id');

        if(array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('economic_activity_attraction.city_id', $p_Data['city_id']);

        if($v_CircuitId != '')
            $v_Query->where('touristic_circuit_cities.touristic_circuit_id', $v_CircuitId);

        if($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        if($v_DistrictId != '')
            $v_Query->where('economic_activity_attraction.district_id', $v_DistrictId);

        if($v_TypeId != '')
            $v_Query->where('economic_activity_attraction.type_id', $v_TypeId);

        if($v_SubTypeId != '')
            $v_Query->where('economic_activity_attraction.sub_type_id', $v_SubTypeId);

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioC3.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->distrito);
                        array_push($v_Data, $c_Result->type);
                        array_push($v_Data, $c_Result->subtype);
                        array_push($v_Data, $c_Result->nome_popular);
                        array_push($v_Data, $c_Result->nome_popular);
                        $v_CNPJ = empty($c_Result->cnpj) ? '' : substr($c_Result->cnpj, 0, 2) . '.' . substr($c_Result->cnpj, 2, 3) . '.' . substr($c_Result->cnpj, 5, 3) . '/' . substr($c_Result->cnpj, 8, 4) . '-' . substr($c_Result->cnpj, 12, 2);
                        array_push($v_Data, $v_CNPJ);
                        array_push($v_Data, $c_Result->nome_organizacao_mantedora);
                        array_push($v_Data, $c_Result->telefone);
                        array_push($v_Data, $c_Result->whatsapp);
                        array_push($v_Data, $c_Result->site);
                        array_push($v_Data, $c_Result->email);
                        array_push($v_Data, $c_Result->facebook);
                        array_push($v_Data, $c_Result->instagram);
                        array_push($v_Data, $c_Result->twitter);
                        array_push($v_Data, $c_Result->youtube);
                        array_push($v_Data, $c_Result->tripadvisor);
                        array_push($v_Data, $c_Result->minas_360);
                        array_push($v_Data, $c_Result->cep);
                        array_push($v_Data, $c_Result->bairro);
                        array_push($v_Data, $c_Result->logradouro);
                        array_push($v_Data, $c_Result->numero);
                        array_push($v_Data, $c_Result->complemento);
                        array_push($v_Data, $c_Result->ponto_referencia);
                        array_push($v_Data, $c_Result->localizacao);
                        array_push($v_Data, $c_Result->latitude);
                        array_push($v_Data, $c_Result->longitude);
                        array_push($v_Data, $c_Result->setor_responsavel_visitacao);
                        $v_AcessoJSON = json_decode($c_Result->acesso,1);
                        foreach($v_AcessoJSON as $c_Field)
                            array_push($v_Data, $c_Field['valor']);
                        $v_ViaTerrestreJSON = json_decode($c_Result->via_terrestre,1);
                        foreach($v_ViaTerrestreJSON as $c_Field)
                            array_push($v_Data, $c_Field['valor']);
                        $v_TransporteJSON = json_decode($c_Result->transporte,1);
                        foreach($v_TransporteJSON as $c_Field) {
                            if($c_Field['tipo'] == 'select2'){
                                $v_Valor = '';
                                $c_Field['valor'] = $c_Field['valor'] == null ? [] : $c_Field['valor'];
                                foreach($c_Field['valor'] as $c_Index => $c_Value)
                                    $v_Valor .= ($c_Index > 0 ? ', ' : '') . $c_Value;
                                array_push($v_Data, $v_Valor);
                            }
                            else
                                array_push($v_Data, $c_Field['valor']);
                        }
                        $v_ProtecaoJSON = json_decode($c_Result->protecao,1);
                        foreach($v_ProtecaoJSON as $c_Field) {
                            if($c_Field['tipo'] == 'checkbox')
                                array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');
                            else
                                array_push($v_Data, $c_Field['valor']);
                        }
                        $v_ConservacaoJSON = json_decode($c_Result->conservacao,1);
                        foreach($v_ConservacaoJSON as $c_Field)
                            array_push($v_Data, $c_Field['valor']);
                        $v_EntradaJSON = json_decode($c_Result->entrada_atrativo,1);
                        foreach($v_EntradaJSON as $c_Field) {
                            if($c_Field['tipo'] == 'checkbox')
                                array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');
                            else
                                array_push($v_Data, $c_Field['valor']);
                        }
                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_1,1);
                        $v_Funcionamento = '';
                        if($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if(!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_2,1);
                        $v_Funcionamento = '';
                        if($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if(!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        array_push($v_Data, $c_Result->funcionamento_observacao);
                        array_push($v_Data, $c_Result->visita_duracao_media);
                        $v_VisitationTypesArray = explode(';', $c_Result->visita_tipo);
                        $v_VisitationTypes = '';
                        foreach($v_VisitationTypesArray as $c_Index => $c_Type){
                            if(!empty($c_Type))
                                $v_VisitationTypes .= ($c_Index > 0 ? ', ' : '') . $c_Type;
                        }
                        array_push($v_Data, $v_VisitationTypes);
                        array_push($v_Data, $c_Result->visita_guia);
                        $v_IdiomasJSON = json_decode($c_Result->visita_idiomas,1);
                        $v_Idiomas = '';
                        foreach($v_IdiomasJSON as $c_Index => $c_Idioma)
                            $v_Idiomas .= ($c_Index > 0 ? ', ' : '') . $c_Idioma['nome'];
                        array_push($v_Data, $v_Idiomas);
                        array_push($v_Data, $c_Result->visita_outros_idiomas);
                        array_push($v_Data, $c_Result->entrada_tipo);
                        array_push($v_Data, $c_Result->entrada_valor);
                        $v_PagamentoJSON = json_decode($c_Result->formas_pagamento,1);
                        $v_Pagamento = '';
                        foreach($v_PagamentoJSON as $c_Index => $c_Pagamento)
                            $v_Pagamento .= ($c_Index > 0 ? ', ' : '') . $c_Pagamento['nome'];
                        array_push($v_Data, $v_Pagamento);

                        array_push($v_Data, $c_Result->necessaria_autorizacao_previa ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->autorizacao_previa_tipo);
                        array_push($v_Data, $c_Result->limite_numero_visitantes ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->limite_numero_visitantes_quantidade);
                        $v_AcessoMaisUtilizadoJSON = json_decode($c_Result->acesso_mais_utilizado,1);
                        foreach($v_AcessoMaisUtilizadoJSON as $c_Field) {
                            if($c_Field['tipo'] == 'checkbox')
                                array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');
                            else
                                array_push($v_Data, $c_Field['valor']);
                        }
                        $v_ServicosEquipamentosJSON = json_decode($c_Result->servicos_equipamentos,1);
                        foreach($v_ServicosEquipamentosJSON as $c_Field) {
                            if($c_Field['tipo'] == 'checkbox')
                                array_push($v_Data, $c_Field['valor'] ? 'Sim' : 'Não');
                            else if($c_Field['tipo'] == 'select2'){
                                $v_Valor = '';
                                $c_Field['valor'] = $c_Field['valor'] == null ? [] : $c_Field['valor'];
                                foreach($c_Field['valor'] as $c_Index => $c_Value)
                                    $v_Valor .= ($c_Index > 0 ? ', ' : '') . $c_Value;
                                array_push($v_Data, $v_Valor);
                            }
                            else
                                array_push($v_Data, $c_Field['valor']);
                        }
                        array_push($v_Data, $c_Result->atividades_realizadas);
                        $v_MonthsArray = explode(';', $c_Result->visitantes_epoca_maior_fluxo);
                        $v_Months = '';
                        foreach($v_MonthsArray as $c_Index => $c_Month){
                            if(!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);
                        $v_MonthsArray = explode(';', $c_Result->visitantes_epoca_menor_fluxo);
                        $v_Months = '';
                        foreach($v_MonthsArray as $c_Index => $c_Month){
                            if(!empty($c_Month))
                                $v_Months .= ($c_Index > 0 ? ', ' : '') . BaseController::$m_Months[$c_Month];
                        }
                        array_push($v_Data, $v_Months);
                        array_push($v_Data, $c_Result->visitantes_numero_anual);
                        $v_OrigensJSON = json_decode($c_Result->visitantes_origem,1);
                        $v_Origens = '';
                        foreach($v_OrigensJSON as $c_Index => $c_Origem)
                            $v_Origens .= ($c_Index > 0 ? ', ' : '') . $c_Origem;
                        array_push($v_Data, $v_Origens);
                        $v_OrigemInternacionalJSON = json_decode($c_Result->visitantes_origem_internacionais,1);
                        $v_Origens = '';
                        foreach($v_OrigemInternacionalJSON as $c_Index => $c_Origem)
                            $v_Origens .= ($c_Index > 0 ? ', ' : '') . $c_Origem['nome'];
                        array_push($v_Data, $v_Origens);
                        array_push($v_Data, $c_Result->descricao_atrativo);
                        array_push($v_Data, $c_Result->descricao_curta);
                        array_push($v_Data, $c_Result->observacoes_complementares);

                        array_push($v_Data, $c_Result->possui_espacos_eventos ? 'Sim' : 'Não');

                        $v_AcessibilidadeJSON = json_decode($c_Result->acessibilidade,1);
                        if($v_AcessibilidadeJSON != null && $v_AcessibilidadeJSON['possui']) {
                            array_push($v_Data, 'Sim');
                            foreach($v_AcessibilidadeJSON['campos'] as $c_Field) {
                                if($c_Field['tipo'] == 'text')
                                    array_push($v_Data, $c_Field['valor']);
                                else {
                                    $v_Valor = '';
                                    $c_Field['valor'] = $c_Field['valor'] == null ? [] : $c_Field['valor'];
                                    foreach($c_Field['valor'] as $c_Index => $c_Value)
                                        $v_Valor .= ($c_Index > 0 ? ', ' : '') . $c_Value;
                                    array_push($v_Data, $v_Valor);
                                }
                            }
                        }
                        else
                            array_push($v_Data, 'Não', '', '', '', '', '', '', '', '', '', '', '');

                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        array_push($v_Data, $c_Result->equipamento_fechado ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->equipamento_fechado ? $c_Result->equipamento_fechado_motivo : '');

                        $sheet->row($v_CurrentRow,$v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:DE'.($v_CurrentRow-1), 'thin');
            });
        })->download('xlsx');
    }

    public function getEventBasicFields(){
        $v_Event = new EventService();
        $v_Event->city_id = $this->city_id;
        $v_Event->district_id = $this->district_id;
        $v_Event->cnpj = $this->cnpj;
        $v_Event->nome_fantasia = $this->nome_popular;
        $v_Event->nome_juridico = $this->nome_popular;
        $v_Event->cep = $this->cep;
        $v_Event->bairro = $this->bairro;
        $v_Event->logradouro = $this->logradouro;
        $v_Event->numero = $this->numero;
        $v_Event->complemento = $this->complemento;
        $v_Event->telefone = $this->telefone;
        $v_Event->whatsapp = $this->whatsapp;
        $v_Event->site = $this->site;
        $v_Event->email = $this->email;
        $v_Event->latitude = $this->latitude;
        $v_Event->longitude = $this->longitude;
        $v_Event->localizacao = $this->localizacao;

        return $v_Event;
    }

    public static function getGeoReport($p_Data)
    {
        $v_Query = EconomicActivityAttraction::leftJoin('city', 'city.id', '=', 'economic_activity_attraction.city_id')
                                    ->leftJoin('touristic_circuit_cities', 'economic_activity_attraction.city_id', '=', 'touristic_circuit_cities.city_id')
                                    ->leftJoin('destination', 'destination.city_id', '=', 'economic_activity_attraction.city_id')
                                    ->join('type', 'economic_activity_attraction.type_id', '=', 'type.id')
                                    ->whereNotNull('economic_activity_attraction.latitude')
                                    ->whereNotNull('economic_activity_attraction.longitude')
                                    ->selectRaw('economic_activity_attraction.nome_popular as nome, economic_activity_attraction.latitude, economic_activity_attraction.longitude, economic_activity_attraction.type_id, economic_activity_attraction.sub_type_id, city.name as cidade')
                                    ->groupBy('economic_activity_attraction.id');

        if(array_key_exists('id', $p_Data))
            $v_Query->whereIn('economic_activity_attraction.city_id', $p_Data['id']);

        if(array_key_exists('touristic_circuit_id', $p_Data))
            $v_Query->whereIn('touristic_circuit_cities.touristic_circuit_id', $p_Data['touristic_circuit_id']);

        if(array_key_exists('region_id', $p_Data))
            $v_Query->whereIn('destination.region_id', $p_Data['region_id']);

        if(array_key_exists('revision_status_id', $p_Data))
            $v_Query->whereIn('economic_activity_attraction.revision_status_id', $p_Data['revision_status_id']);

        if(array_key_exists('published', $p_Data) && $p_Data['published'] == 1)
            $v_Query->where('economic_activity_attraction.id', 0); //nao existem itens publicados

        return $v_Query->get();
    }
}