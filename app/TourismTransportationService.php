<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class TourismTransportationService extends BaseInventoryModel {

    protected $table = 'tourism_transportation_service';
           //Indica campos que podem ser salvos pelo função create/update
    protected $fillable = [
        'city_id',
        'tipo_atividade_cadastur',
        'revision_status_id',
        'district_id',
        'type_id',
        'nome_fantasia',
        'nome_juridico',
        'nome_rede',
        'cnpj',
        'inicio_atividade',
        'registro_cadastur',
        'numero_registro',
        'cep',
        'bairro',
        'logradouro',
        'numero',
        'complemento',
        'telefone',
        'whatsapp',
        'site',
        'email',
        'localizacao',
        'facebook',
        'instagram',
        'twitter',
        'youtube',
        'tripadvisor',
        'minas_360',
        'latitude',
        'longitude',
        'abrangencia',
        'tipos_servico',
        'funcionamento_1',
        'funcionamento_2',
        'funcionamento_observacao',
        'quantidade_veiculos',
        'quantidade_veiculos_adaptados',
        'oferta_total_lugares_sentados',
        'categoria_veiculos',
        'descricoes_observacoes_complementares',
        'acessibilidade',
        'equipe_responsavel_responsavel',
        'equipe_responsavel_instituicao',
        'equipe_responsavel_telefone',
        'equipe_responsavel_email',
        'equipe_responsavel_observacao',
        'equipamento_fechado',
        'equipamento_fechado_motivo',
        'created_at',
        'updated_at',
        'comentario_revisao',
        'tem_cadastur',
        'descricao_curta',
        'destaque',
        'hashtags',
        'data_publicacao',
        'tipos_viagem',
        'validade_cadastur',
        'atracao',
    ];

    public static function getDT($p_Name, $p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw) {
        $v_Query = TourismTransportationService::join('city', 'city.id', '=', 'tourism_transportation_service.city_id')->join('revision_status', 'revision_status.id', '=', 'tourism_transportation_service.revision_status_id')
                ->select(DB::raw('SQL_CALC_FOUND_ROWS tourism_transportation_service.id, tourism_transportation_service.city_id, tourism_transportation_service.nome_fantasia, city.name as municipio, tourism_transportation_service.created_at, tourism_transportation_service.updated_at, revision_status.name as status'));

        if (UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if (UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());
        else if (UserType::isTrade())
            $v_Query->whereIn('tourism_transportation_service.id', UserTradeItem::getUserTradeItems('B4'));

        if ($p_Name != '')
            $v_Query->where('tourism_transportation_service.nome_fantasia', 'like', '%' . $p_Name . '%');

        if ($p_City != '')
            $v_Query->where('city.name', 'like', '%' . $p_City . '%');

        if ($p_CreatedAt != '') {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('tourism_transportation_service.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('tourism_transportation_service.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if ($p_UpdatedAt != '') {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('tourism_transportation_service.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('tourism_transportation_service.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if ($p_Status != '')
            $v_Query->where('tourism_transportation_service.revision_status_id', $p_Status);

        if ($p_Order != null) {
            if ($p_Order["column"] == 0)
                $v_Query->orderBy('tourism_transportation_service.nome_fantasia', $p_Order["dir"]);
            if ($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if ($p_Order["column"] == 2)
                $v_Query->orderBy('tourism_transportation_service.created_at', $p_Order["dir"]);
            if ($p_Order["column"] == 3)
                $v_Query->orderBy('tourism_transportation_service.updated_at', $p_Order["dir"]);
            if ($p_Order["column"] == 4)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if ($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for ($c_Index = 0; $c_Index < sizeof($v_QueryRes); $c_Index++) {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['nome_fantasia'],
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                '<a href="' . url('admin/inventario/servicos-transporte-turistico/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                '<a href="' . url('admin/inventario/servicos-transporte-turistico/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/servicos-transporte-turistico/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = BaseInventoryModel::getTotalRows();

        if (UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = TourismTransportationService::where('city_id', Auth::user()->city_id)->count();
        else if (UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = TourismTransportationService::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else if (UserType::isTrade())
            $v_DataTableAjax->recordsTotal = TourismTransportationService::whereIn('id', UserTradeItem::getUserTradeItems('B4'))->count();
        else
            $v_DataTableAjax->recordsTotal = TourismTransportationService::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function post($p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos, $p_Data) {
        if (array_key_exists('publicar', $p_Data)) {
            $v_Publicar = $p_Data['publicar'];
            unset($p_Data['publicar']);
        } else
            $v_Publicar = 0;
        $p_Data['equipamento_fechado'] = array_key_exists('equipamento_fechado', $p_Data) ? 1 : 0;
        $p_Data['destaque'] = array_key_exists('destaque', $p_Data) ? 1 : 0;
        $p_Data['atracao'] = array_key_exists('atracao', $p_Data) ? 1 : 0;

        array_walk($p_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_PreviousStatus = 0;
        if ($p_Id != null)
            $v_PreviousStatus = TourismTransportationService::find($p_Id)->revision_status_id;

        $v_Item = TourismTransportationService::updateOrCreate(['id' => $p_Id], $p_Data);
        InventoryPhoto::updatePhotos('B4', $v_Item->id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos);

        if ($p_Id == null && UserType::isTrade())
            UserTradeItem::postUserTradeItem('B4', $v_Item->id);

        if ($v_Item->registro_cadastur == null)
            $v_Publicar = 0;

        if (UserType::isAdmin() || UserType::isMaster()) {
            if ($v_Item->revision_status_id == 4 && $v_Publicar == 1) {
                Attraction::publish($v_Item, 'B4', 1);
                $v_Item->data_publicacao = Carbon::now()->format('Y-m-d H:i:s');
                $v_Item->save();
            } else if ($v_Item->data_publicacao != null && $v_Publicar == 2) {
                Attraction::unpublish($v_Item->id, 'B4');
                $v_Item->data_publicacao = null;
                $v_Item->save();
            }
        }

        if ($p_Id != null && $v_PreviousStatus != 4) {
            $v_Subject = null;
            $v_Message = null;
            if ($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_fantasia . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            } else if ($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item ' . $v_Item->nome_fantasia . ' foram rejeitadas.</p>';
                if (!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if ($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'B4', $v_Item->id);
                foreach ($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message], function ($message) use ($c_User, $v_Subject) {
                        $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                    });
                }
            }
        }
    }

    public static function deleteTourismTransportationService($p_Id) {
        $v_Query = TourismTransportationService::where('id', $p_Id);
        if (UserType::isMunicipio())
            $v_Query->where('city_id', Auth::user()->city_id);
        else if (UserType::isCircuito())
            $v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
        else if (UserType::isTrade())
            $v_Query->whereIn('id', UserTradeItem::getUserTradeItems('B4'));
        $v_Item = $v_Query->firstOrFail();
        InventoryPhoto::deletePhotos('B4', $p_Id);

        if ($v_Item->data_publicacao != null)
            Attraction::unpublish($p_Id, 'B4');

        UserTradeItem::deleteUserTradeItem('B4', $p_Id);

        $v_Item->equipe_responsavel_observacao = 'DELETED';
        $v_Item->save();
        $v_Item->delete();
    }

    public static function checkCNPJAvailability($p_CNPJ) {
        $v_Item = TourismTransportationService::where('cnpj', $p_CNPJ)->first();
        if ($v_Item == null)
            return -1;
        else if (UserTradeItem::userAlreadyRequestedItem('B4', $v_Item->id))
            return -2;
        else
            return UserTradeItem::itemHasUser('B4', $v_Item->id);
    }

    public static function findIdByCNPJ($p_CNPJ) {
        return TourismTransportationService::where('cnpj', $p_CNPJ)->first()->id;
    }

    public static function getReport($p_Data) {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_DistrictId = $p_Data['district_id'];
        $v_TypeId = $p_Data['type_id'];

        $v_Query = TourismTransportationService::leftJoin('city', 'city.id', '=', 'tourism_transportation_service.city_id')
                ->leftJoin('district', 'tourism_transportation_service.district_id', '=', 'district.id')
                ->leftJoin('touristic_circuit_cities', 'tourism_transportation_service.city_id', '=', 'touristic_circuit_cities.city_id')
                ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                ->leftJoin('destination', 'destination.city_id', '=', 'tourism_transportation_service.city_id')
                ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                ->leftJoin('type', 'tourism_transportation_service.type_id', '=', 'type.id')
                ->selectRaw('tourism_transportation_service.*, city.name as cidade, district.name as distrito, touristic_circuit.nome as circuito, region.name as regiao, type.name as type')
                ->groupBy('tourism_transportation_service.id');

        if (array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('tourism_transportation_service.city_id', $p_Data['city_id']);

        if ($v_CircuitId != '')
            $v_Query->where('touristic_circuit_cities.touristic_circuit_id', $v_CircuitId);

        if ($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        if ($v_DistrictId != '')
            $v_Query->where('tourism_transportation_service.district_id', $v_DistrictId);

        if ($v_TypeId != '')
            $v_Query->where('tourism_transportation_service.type_id', $v_TypeId);

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioB4.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results) {
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                $v_Languages = ['pt', 'en', 'es', 'fr'];
                foreach ($v_Results as $c_Result) {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);
                        array_push($v_Data, $c_Result->distrito);
                        array_push($v_Data, $c_Result->type);
                        $v_CNPJ = empty($c_Result->cnpj) ? '' : substr($c_Result->cnpj, 0, 2) . '.' . substr($c_Result->cnpj, 2, 3) . '.' . substr($c_Result->cnpj, 5, 3) . '/' . substr($c_Result->cnpj, 8, 4) . '-' . substr($c_Result->cnpj, 12, 2);
                        array_push($v_Data, $v_CNPJ);
                        array_push($v_Data, $c_Result->nome_fantasia);
                        array_push($v_Data, $c_Result->nome_juridico);
                        array_push($v_Data, $c_Result->nome_rede);
                        array_push($v_Data, $c_Result->inicio_atividade);
                        array_push($v_Data, $c_Result->registro_cadastur);
                        array_push($v_Data, $c_Result->numero_registro);
                        array_push($v_Data, $c_Result->cep);
                        array_push($v_Data, $c_Result->bairro);
                        array_push($v_Data, $c_Result->logradouro);
                        array_push($v_Data, $c_Result->numero);
                        array_push($v_Data, $c_Result->complemento);
                        array_push($v_Data, $c_Result->telefone);
                        array_push($v_Data, $c_Result->whatsapp);
                        array_push($v_Data, $c_Result->site);
                        array_push($v_Data, $c_Result->email);
                        array_push($v_Data, $c_Result->latitude);
                        array_push($v_Data, $c_Result->longitude);
                        array_push($v_Data, $c_Result->facebook);
                        array_push($v_Data, $c_Result->instagram);
                        array_push($v_Data, $c_Result->twitter);
                        array_push($v_Data, $c_Result->youtube);
                        array_push($v_Data, $c_Result->tripadvisor);
                        array_push($v_Data, $c_Result->minas_360);

                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_1, 1);
                        $v_Funcionamento = '';
                        if ($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach ($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach ($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if ($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if ($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if (!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        $v_FuncionamentoJSON = json_decode($c_Result->funcionamento_2, 1);
                        $v_Funcionamento = '';
                        if ($v_FuncionamentoJSON != null && count($v_FuncionamentoJSON['meses'])) {
                            foreach ($v_FuncionamentoJSON['meses'] as $c_Index => $c_Month)
                                $v_Funcionamento .= ($c_Index > 0 ? ', ' : '') . $c_Month;
                            foreach ($v_FuncionamentoJSON['dias'] as $c_Index => $c_Day) {
                                if ($c_Day['fechado'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': Fechado';
                                else if ($c_Day['funciona24horas'])
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': 24 horas';
                                else if (!empty($c_Day['horarioDe']) && !empty($c_Day['horarioAte']))
                                    $v_Funcionamento .= '\n' . $c_Day['dia'] . ': ' . $c_Day['horarioDe'] . ' - ' . $c_Day['horarioAte'];
                            }
                        }
                        array_push($v_Data, stripcslashes($v_Funcionamento));
                        $v_FuncionamentoObsJSON = json_decode($c_Result->funcionamento_observacao, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_FuncionamentoObsJSON[$c_Lang]);

                        $v_AbrangenciaJSON = json_decode($c_Result->abrangencia, 1);
                        $v_Abrangencia = '';
                        foreach ($v_AbrangenciaJSON as $c_Index => $c_Abrangencia)
                            $v_Abrangencia .= ($c_Index > 0 ? ', ' : '') . $c_Abrangencia;
                        array_push($v_Data, $v_Abrangencia);
                        $v_TiposServicoJSON = json_decode($c_Result->tipos_servico, 1);
                        $v_TiposServico = '';
                        foreach ($v_TiposServicoJSON as $c_Index => $c_TipoServico)
                            $v_TiposServico .= ($c_Index > 0 ? ', ' : '') . $c_TipoServico;
                        array_push($v_Data, $v_TiposServico);

                        array_push($v_Data, $c_Result->quantidade_veiculos);
                        array_push($v_Data, $c_Result->quantidade_veiculos_adaptados);
                        array_push($v_Data, $c_Result->oferta_total_lugares_sentados);
                        array_push($v_Data, $c_Result->categoria_veiculos);

                        $v_DescricaoCurtaJSON = json_decode($c_Result->descricao_curta, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_DescricaoCurtaJSON[$c_Lang]);
                        $v_DescricaoJSON = json_decode($c_Result->descricoes_observacoes_complementares, 1);
                        foreach ($v_Languages as $c_Lang)
                            array_push($v_Data, $v_DescricaoJSON[$c_Lang]);

                        array_push($v_Data, $c_Result->atracao ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->destaque ? 'Sim' : 'Não');
                        $v_HashtagsArray = explode(';', $c_Result->hashtags);
                        $v_Hashtags = '';
                        foreach ($v_HashtagsArray as $c_Index => $c_Hashtag) {
                            if (!empty($c_Hashtag))
                                $v_Hashtags .= ($c_Index > 0 ? ', ' : '') . $c_Hashtag;
                        }
                        array_push($v_Data, $v_Hashtags);

                        $v_AcessibilidadeJSON = json_decode($c_Result->acessibilidade, 1);
                        if ($v_AcessibilidadeJSON != null && $v_AcessibilidadeJSON['possui']) {
                            array_push($v_Data, 'Sim');
                            $v_FieldsArray = [];
                            $v_ObsArray = [];
                            foreach ($v_AcessibilidadeJSON['campos'] as $c_Field) {
                                if (stripos($c_Field['nome'], 'Rota externa acessível - Outras - ') > -1)
                                    array_push($v_Data, $c_Field['valor']);
                                else if ($c_Field['nome'] == 'Rota externa acessível - Outras')
                                    array_push($v_Data, $c_Field['valor'], '', '', '');
                                else if (stripos($c_Field['nome'], 'Observações complementares - ') > -1)
                                    array_push($v_ObsArray, $c_Field['valor']);
                                else if ($c_Field['nome'] == 'Observações complementares')
                                    array_push($v_ObsArray, $c_Field['valor'], '', '', '');
                                else {
                                    $v_Valor = '';
                                    $c_Field['valor'] = $c_Field['valor'] == null ? [] : $c_Field['valor'];
                                    foreach ($c_Field['valor'] as $c_Index => $c_Value)
                                        $v_Valor .= ($c_Index > 0 ? ', ' : '') . $c_Value;
                                    array_push($v_FieldsArray, $v_Valor);
                                }
                            }
                            $v_Data = array_merge($v_Data, $v_FieldsArray, $v_ObsArray);
                        } else
                            array_push($v_Data, 'Não', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        array_push($v_Data, $c_Result->equipamento_fechado ? 'Sim' : 'Não');
                        array_push($v_Data, $c_Result->equipamento_fechado ? $c_Result->equipamento_fechado_motivo : '');

                        $sheet->row($v_CurrentRow, $v_Data);
                        $v_CurrentRow++;
                    } catch (\Exception $e) {
                        
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:BY' . ($v_CurrentRow - 1), 'thin');
            });
        })->download('xlsx');
    }

    public static function getGeoReport($p_Data) {
        $v_Query = TourismTransportationService::leftJoin('city', 'city.id', '=', 'tourism_transportation_service.city_id')
                ->leftJoin('touristic_circuit_cities', 'tourism_transportation_service.city_id', '=', 'touristic_circuit_cities.city_id')
                ->leftJoin('destination', 'destination.city_id', '=', 'tourism_transportation_service.city_id')
                ->join('type', 'tourism_transportation_service.type_id', '=', 'type.id')
                ->whereNotNull('tourism_transportation_service.latitude')
                ->whereNotNull('tourism_transportation_service.longitude')
                ->selectRaw('tourism_transportation_service.nome_fantasia as nome, tourism_transportation_service.latitude, tourism_transportation_service.longitude, tourism_transportation_service.type_id, city.name as cidade')
                ->groupBy('tourism_transportation_service.id');

        if (array_key_exists('id', $p_Data))
            $v_Query->whereIn('tourism_transportation_service.city_id', $p_Data['id']);

        if (array_key_exists('touristic_circuit_id', $p_Data))
            $v_Query->whereIn('touristic_circuit_cities.touristic_circuit_id', $p_Data['touristic_circuit_id']);

        if (array_key_exists('region_id', $p_Data))
            $v_Query->whereIn('destination.region_id', $p_Data['region_id']);

        if (array_key_exists('revision_status_id', $p_Data))
            $v_Query->whereIn('tourism_transportation_service.revision_status_id', $p_Data['revision_status_id']);

        if (array_key_exists('published', $p_Data) && $p_Data['published'] == 1) {
            if ($p_Data['published'] == 1)
                $v_Query->whereNotNull('tourism_transportation_service.data_publicacao');
            else
                $v_Query->whereNull('tourism_transportation_service.data_publicacao');
        }

        return $v_Query->get();
    }

}
