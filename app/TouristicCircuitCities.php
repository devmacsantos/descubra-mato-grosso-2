<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TouristicCircuitCities extends Model
{
    public $timestamps = false;
    protected $table = 'touristic_circuit_cities';

    public static function updateTouristicCircuitCities($p_TouristicCircuitId, $p_Cities)
    {
        TouristicCircuitCities::where('touristic_circuit_id', $p_TouristicCircuitId)->delete();
        foreach($p_Cities as $c_City)
        {
            $v_EventCity = new TouristicCircuitCities();
            $v_EventCity->touristic_circuit_id = $p_TouristicCircuitId;
            $v_EventCity->city_id = $c_City;
            $v_EventCity->save();
        }
    }

    public static function getSelectedCities($p_TouristicCircuitId)
    {
        return TouristicCircuitCities::where('touristic_circuit_id', $p_TouristicCircuitId)->lists('city_id')->toArray();
    }

    public static function getCircuitCities($p_TouristicCircuitId)
    {
        $v_Response['error'] = 'ok';
        $v_Response['data'] = TouristicCircuitCities::where('touristic_circuit_id', $p_TouristicCircuitId)
            ->lists('city_id')->toArray();

        return $v_Response;
    }

    public static function getUserCircuitCities()
    {
        return TouristicCircuitCities::where('touristic_circuit_id',Auth::user()->touristic_circuit_id)
            ->lists('city_id')->toArray();
    }

    public static function getCityCircuit($p_CityId)
    {
        return TouristicCircuitCities::where('city_id',$p_CityId)
            ->lists('touristic_circuit_id')->toArray();
    }
}