<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TripDestinations extends Model
{
    public $timestamps = false;
    protected $table = 'trip_destinations';

    public static function updateTripTypes($p_DestinationId, $p_TripTypes)
    {
        TripDestinations::where('destination_id', $p_DestinationId)->delete();
        foreach($p_TripTypes as $c_Type)
        {
            $v_Item = new TripDestinations();
            $v_Item->destination_id = $p_DestinationId;
            $v_Item->trip_type_id = $c_Type;
            $v_Item->save();
        }
    }

    public static function getSelectedTypes($p_DestinationId)
    {
        return TripDestinations::where('destination_id', $p_DestinationId)->lists('trip_type_id')->toArray();
    }

    public static function getTripDestinations($p_TripTypeId, $p_Quantity)
    {
        return TripDestinations::join('destination', 'destination.id', '=', 'trip_destinations.destination_id')
            ->join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
            ->where('trip_destinations.trip_type_id', $p_TripTypeId)
            ->where('destination.publico', 1)
            ->where('destination_photo.is_cover', 1)
            ->select(['destination.nome', 'destination.slug', 'destination_photo.url', 'destination.descricao_curta'])
            ->orderBy('destination.destaque', 'desc')
            ->groupBy('destination.id')
            ->take($p_Quantity)->get();
    }

    public static function getTripAndCityDestinations($p_TripTypeId, $p_CityId, $p_Quantity)
    {
        return TripDestinations::join('destination', 'destination.id', '=', 'trip_destinations.destination_id')
            ->join('destination_photo', 'destination_photo.destination_id', '=', 'destination.id')
            ->where('trip_destinations.trip_type_id', $p_TripTypeId)
            ->where('destination.publico', 1)
            ->where('destination.city_id', $p_CityId)
            ->where('destination_photo.is_cover', 1)
            ->select(['destination.nome', 'destination.slug', 'destination_photo.url', 'destination.descricao_curta'])
            ->orderBy('destination.destaque', 'desc')
            ->groupBy('destination.id')
            ->take($p_Quantity)->get();
    }
}