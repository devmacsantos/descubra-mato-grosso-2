<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class EventPhoto extends Model
{
    public $timestamps = false;
    protected $table = 'event_photo';

    public static function updatePhotos($p_EventId, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/eventos/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        foreach($p_DeletedPhotos as $c_PhotoId)
        {
            $v_Photo = EventPhoto::find($c_PhotoId);
            $v_OldFileName = explode('/', $v_Photo->url);
            $v_OldFileName = array_pop($v_OldFileName);
            $v_Photo->delete();
            \File::delete($v_Path . $v_OldFileName);
        }
        foreach($p_Photos as $c_Index => $c_File)
        {
            if($c_File != null)
            {
                $v_Photo = EventPhoto::findOrNew($p_PhotoInfo['id'][$c_Index]);
                if($p_PhotoInfo['type'][$c_Index] == 'cover')
                    $v_Photo->is_cover = 1;
                else
                    $v_Photo->is_cover = 0;

                $v_PhotoName =  time() . str_random(10) . '.jpg';
                $v_Image = Image::make($c_File);
                $v_Image->widen(1920, function ($constraint){
                    $constraint->upsize();
                });
                $v_Image->encode('jpg')->save($v_Path . $v_PhotoName);
                if ($v_Photo->url != null)
                {
                    $v_OldFileName = explode('/', $v_Photo->url);
                    $v_OldFileName = array_pop($v_OldFileName);
                    \File::delete($v_Path . $v_OldFileName);
                }
                $v_Photo->url = url('/imagens/eventos/' . $v_PhotoName);
                $v_Photo->event_id = $p_EventId;
                $v_Photo->save();
            }
        }
    }

    public static function getCoverPhoto($p_EventId)
    {
        return EventPhoto::where('is_cover', 1)->where('event_id', $p_EventId)->first();
    }

    public static function getPhotoGallery($p_EventId)
    {
        return EventPhoto::where('is_cover', 0)->where('event_id', $p_EventId)->get();
    }

    public static function deletePhotos($p_EventId)
    {
        $v_Path = public_path() . '/imagens/eventos/';
        $v_Photos = EventPhoto::where('event_id', $p_EventId)->get();
        foreach($v_Photos as $c_Photo)
        {
            $v_FileName = explode('/', $c_Photo->url);
            $v_FileName = array_pop($v_FileName);
            \File::delete($v_Path . $v_FileName);
        }
    }
}