<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AttractionHashtag extends Model
{
    public $timestamps = false;
    protected $table = 'attraction_hashtag';

    public static function deleteAttractionHashtags($p_AttractionId)
    {
        AttractionHashtag::where('attraction_id', $p_AttractionId)->delete();
    }

    public static function addAttractionHashtag($p_AttractionId, $p_HashtagId)
    {
        $v_AttractionHashtag = new AttractionHashtag();
        $v_AttractionHashtag->attraction_id = $p_AttractionId;
        $v_AttractionHashtag->hashtag_id = $p_HashtagId;
        $v_AttractionHashtag->save();
    }

    public static function getHashtagAttractions($p_HashtagIds, $p_Quantity)
    {
        $v_Query = AttractionHashtag::join('attraction', 'attraction.id', '=', 'attraction_hashtag.attraction_id')
            ->join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
            ->whereIn('attraction_hashtag.hashtag_id', $p_HashtagIds)
            ->where('attraction.atracao', 1)
            ->where('attraction_photo.is_cover', 1)
            ->select(['attraction.id', 'attraction.nome', 'attraction.descricao_curta', 'attraction.slug', 'attraction.trade', 'attraction_photo.url']);
        if(!empty($p_HashtagIds)){
            $v_Query->orderByRaw('FIELD(attraction_hashtag.hashtag_id, ' . implode(',', $p_HashtagIds) . ')');
        }
        //print($v_Query->toSql());
        //exit();
        return $v_Query->orderBy('attraction.destaque', 'desc')
            ->get()
            ->unique('id')
            ->take($p_Quantity);
    }

    public static function getHashtagTrade($p_Types, $p_HashtagIds, $p_Quantity)
    {
        $v_Query = AttractionHashtag::join('attraction', 'attraction.id', '=', 'attraction_hashtag.attraction_id')
            ->join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
            ->join('city', 'city.id', '=', 'attraction.city_id')
            ->whereIn('attraction.item_inventario', $p_Types)
            ->whereNotIn('type_id', [63, 65, 67, 68])
            ->whereIn('attraction_hashtag.hashtag_id', $p_HashtagIds)
            ->where('attraction.trade', 1)
            ->where('attraction_photo.is_cover', 1)
            ->select(['attraction.*', 'attraction_photo.url', 'city.name as cidade']);
        if(!empty($p_HashtagIds)){
            $v_Query->orderByRaw('FIELD(attraction_hashtag.hashtag_id, ' . implode(',', $p_HashtagIds) . ')');
        }
        return $v_Query->orderBy('integrante_minas_recebe', 'DESC')
            ->orderBy('attraction.nome')
            ->groupBy('attraction.id')
            ->get()
            ->unique('id')
            ->take($p_Quantity);
    }
}
