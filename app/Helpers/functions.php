<?php
/**
 * Faz conversão de data de forma automática ou definida por formatIn/formatOut
 *
 * @param string $strDate data
 * @param string $formatIn formato de entrada
 * @param string $formatOut formato de saída desejado
 * @return void
 */
function conv_date($strDate, $formatIn = null, $formatOut = null)
{

    if ($strDate == '0000-00-00') {
        return '00-00-0000';
    }

    if ($strDate == '00-00-0000') {
        return '0000-00-00';
    }

    if ($strDate == '') {
        return "";
    }

    if ($formatIn != null && $formatOut != null) {
        return \DateTime::createFromFormat($formatIn, $strDate)->format($formatOut);
    }

    if (\DateTime::createFromFormat('d-m-Y', $strDate)) {
        return \DateTime::createFromFormat('d-m-Y', $strDate)->format('Y-m-d');
    } elseif (\DateTime::createFromFormat('Y-m-d', $strDate)) {
        return \DateTime::createFromFormat('Y-m-d', $strDate)->format('d-m-Y');
    } else {
        return "";
    }
}

/**
 * Função para converter data no formato yyyy-m-d para formato literal
 *
 * @param string $date data no formada YYYY-MM-DD
 * @param string $data_fim faz literal de duas datas como períodos
 * @return string retorna data no formato literal.
 */
function getLiteralDate($data_inicio = "now", $data_fim = null)
{

    $diasSemana = ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'];
    $mes = ['', 'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

    //$mes[strftime('%m', strtotime($data_inicio))]
    $inicio = strtotime($data_inicio);

    setlocale(LC_ALL, 'pt_BR.utf-8');
    date_default_timezone_set('America/Sao_Paulo');

    //print($data_inicio);exit();

    //só data de inicio
    if ($data_fim == null) {
        $inicio = strftime('%A, %d de %B de %Y',$inicio);
        $return = $inicio;
    } else {
        //se os anos forem diferentes
        if (date_format(date_create($data_inicio), 'Y') != date_format(date_create($data_fim), 'Y')) {
            $inicio = strftime('%A, %d de %B de %Y',$inicio);
            $fim = strftime('%A, %d de %B de %Y', strtotime($data_fim));
            $return = $inicio.' até '.$fim;

        //se os meses forem diferentes    
        } else if (date_format(date_create($data_inicio), 'm') != date_format(date_create($data_fim), 'm')) {
            $inicio = strftime('%A, %d de %B',$inicio);
            $fim = strftime('%A, %d de %B de %Y', strtotime($data_fim));
            $return = $inicio.' até '.$fim;
        //se os dias forem diferentes:    
        } else if (date_format(date_create($data_inicio), 'd') != date_format(date_create($data_fim), 'd')) {
            $inicio = strftime('%A, %d ',$inicio);
            $fim = strftime('%A, %d de %B de %Y', strtotime($data_fim));
            $return = $inicio.' até '.$fim;
        //teste, pelo menos vemos as datas 
        } else {
            $inicio = strftime('%A, %d de %B de %Y',$inicio);
            $fim = strftime('%A, %d de %B de %Y', strtotime($data_fim));
            $return = $inicio.' até '.$fim;}
    }
    //just for test
    if (mb_detect_encoding($return) != "UTF-8") {
        return utf8_encode($return);
    } else {
        return $return;
    }
}


function getShortDate($data_inicio = "now", $data_fim = null)
{

    //$diasSemana = ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'];
    //$mes = ['', 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro'];

    //$mes[strftime('%m', strtotime($data_inicio))]
    $inicio = strtotime($data_inicio);

    setlocale(LC_ALL, 'pt_BR.utf-8');
    date_default_timezone_set('America/Sao_Paulo');

    //print($data_inicio);exit();

    //só data de inicio
    if ($data_fim == null) {
        $inicio = strftime('%d %b %Y',$inicio);
        $return = $inicio;
    } else {
        //se os anos forem diferentes
        if (date_format(date_create($data_inicio), 'Y') != date_format(date_create($data_fim), 'Y')) {
            $inicio = strftime('%d %b %Y',$inicio);
            $fim = strftime('%d %b %Y', strtotime($data_fim));
            $return = $inicio.' - '.$fim;

        //se os meses forem diferentes    
        } else if (date_format(date_create($data_inicio), 'm') != date_format(date_create($data_fim), 'm')) {
            $inicio = strftime('%d %b',$inicio);
            $fim = strftime('%d %b %Y', strtotime($data_fim));
            $return = $inicio.'-'.$fim;
        //se os dias forem diferentes:    
        } else if (date_format(date_create($data_inicio), 'd') != date_format(date_create($data_fim), 'd')) {
            $inicio = strftime('%d',$inicio);
            $fim = strftime('%d %b %Y', strtotime($data_fim));
            $return = $inicio.'-'.$fim;
        //teste, pelo menos vemos as datas 
        } else {
            $inicio = strftime('%d %b %Y',$inicio);
            //$fim = strftime('%d %b %Y', strtotime($data_fim));
            $return = $inicio;
        }
    }
    //just for test
    if (mb_detect_encoding($return) != "UTF-8") {
        return utf8_encode($return);
    } else {
        return $return;
    }
}


/**
 * Função para retornar iniciais de mês
 *
 * @param integer $id indice do mês começando por 1 e terminado em 12
 * @return string retorna string vazia caso o índice esteja fora do intervalo 1 a 12
 */
function getMonthName($id = 1)
{

    $id = (int) $id;
    if ($id < 0 || $id > 12) {
        return '';
    }

    $month = ['', 'JAN', 'FEV', 'MAR', 'ABR', 'MAI', 'JUN', 'JUL', 'AGO', 'SET', 'OUT', 'NOV', 'DEZ'];
    return $month[$id];
}

/**
 * Função de Apoio, converte tipo e subtipo cadastrados
 * nos formulários B1, B2 e etc no tipo de atividade do CADASTUR.
 * Esta rotina é chamada principalmente no momento de consutar CNPJ no sistema
 * CADASTUR
 * @param type $type
 * @param type $subtype
 * @return int
 */
function getTypeCadastur($type, $subtype = -1)
{

    /*
    1-Acampamento Turístico
    2-Agência de Turismo
    3-Meio de Hospedagem
    4-Organizadora de Eventos
    5-Parque Temático
    6-Transportadora Turística
    7-Empreendimento de Apoio ao Turismo Náutico ou à Pesca Desportiva
    8-Casa de Espetáculos e Equipamento de Animação Turística
    9-Centro de Convenções
    10-Prestador Especializado em Segmentos Turísticos
    11-Prestador de Infraestrutura de Apoio para Eventos
    12-Locadora de Veículos para Turistas
    13-Parque Aquático e Empreendimento de Lazer
    14-Restaurante, Cafeteria, Bar e Similares
     */

    if ($type >= 56 && $type <= 60) {
        return 14;
    }

    if (($type >= 63 && $type <= 64) || $type == 256) {
        return 2;
    }

    if ($type >= 65 && $type <= 68) {
        return 6;
    }

    if (($type >= 69 && $type <= 73) || ($type >= 291 && $type <= 294)) {
        return 9;
    }

    if ($type >= 83 && $type <= 85) {
        return 8;
    }
    if ($type >= 86 && $type <= 76) {
        return 13;
    }
    if ($type == 74) {
        return 5;
    }

    if ($type == 90 || $type == 253) {
        return 10;
    }

    switch ($type) {
        case 37:
            //if (($subtype >= 38) && ($subtype <= 43)) {
            //    return 3;
            //} else {
            //    return 0;
            //}
            return 3;
        case 50:
            if ($subtype == 51) {
                return 1;
            } else {
                return 3;
            }
        case 72:
            return 4;
        case 73:
            return 11;
        case 70:
            return 9;
        default:
            return 0;
    }
}
/**
 * Função para executar curl pela linha de comando usando configurações colocadas no arquivo .env
 *
 * @param string $params Parametros para curl na linha de comando
 * @return void
 */
function curlByExec($params)
{

    if ($_SERVER['HTTP_HOST']=='localhost:8000'){
        $config = '';

        $config = (env('FTP_PROXY', '') != '') ? 'ftp_proxy=' . env('FTP_PROXY', '') : '';
        $config .= (env('HTTP_PROXY', '') != '') ? ' http_proxy=' . env('HTTP_PROXY', '') : '';
        $config .= (env('HTTPS_PROXY', '') != '') ? ' https_proxy=' . env('HTTPS_PROXY', '') : '';

        return exec(" $config curl $params");
    }else{

        return exec("curl $params");
    }

}



/**
 * Rotina de apoio, para realizar chamadas HTTP via CURL. Até o momento não foi
 * usada pois não é eficiente em ambiente com proxy.
 * @param type $url
 * @return type
 */
function getWebPage($url)
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true, // return web page
        CURLOPT_HEADER => false, // don't return headers
        CURLOPT_FOLLOWLOCATION => true, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_USERAGENT => "spider", // who am i
        CURLOPT_AUTOREFERER => true, // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
        CURLOPT_TIMEOUT => 120, // timeout on response
        CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false, // Disabled SSL Cert checks
    );

    $ch = curl_init($url);
    curl_setopt_array($ch, $options);
    $content = curl_exec($ch);
    curl_close($ch);
    return $content;
}

/**
 * Função de apoio que realiza chamadas HTML, esta é usada pois seu parâmetro de
 * conexão é montado por outra função: default_ops.
 *
 * @param string $url "URL a ser chamado"
 * @param boolean $exception indica se é para gerar exception caso a conexão tenha
 * algum problema
 * @return type
 * @throws Exception
 */
function get_file($url, $exception = false)
{
    $curl = curl_init();
    $options = default_ops(array(CURLOPT_URL => $url));
    curl_setopt_array($curl, $options);
    $output = curl_exec($curl);
    //print $output;
    $err = curl_errno($curl);
    $errmsg = curl_error($curl);
    if ($err != 0 && $exception) {
        print "ERROR MESSAGE=" . $err . " $errmsg";
    }
    $info = curl_getinfo($curl);
    if ($info['http_code'] != 200 && $exception) {
        throw new Exception('HTTP Error retrieving URL ' . $url . ' HTTP return code=' . $info['http_code'] . ' ' . $output);
    }

    return $output;
}

/**
 * Função que gera parâmetros para conexão, usada pela função get_file.
 * As configurações que exigem atenção são as que diz respeito ao PROXY, estes
 * devem ser configurados no arquivo .env
 * @param type $options
 * @return type
 */
function default_ops($options = array())
{
    $cookie_file = '/tmp/cookies.txt';
    $opts = array(
        CURLOPT_CONNECTTIMEOUT => 30, /* seconds */
        CURLOPT_TIMEOUT => 60,
        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.17) Gecko/20110420 Firefox/3.6.17',
        CURLOPT_HTTPHEADER => array("Expect:"),
        CURLOPT_HEADER => false, // return headers
        CURLOPT_FOLLOWLOCATION => false, // follow redirects
        CURLOPT_ENCODING => "", // handle all encodings
        CURLOPT_SSL_VERIFYHOST => false, // http://ademar.name/blog/2006/04/curl-ssl-certificate-problem-v.html
        CURLOPT_SSL_VERIFYPEER => false,
//    CURLOPT_VERBOSE        => true,
        CURLOPT_SSLVERSION => 3,
//    CURLOPT_COOKIEFILE     => $cookie_file,
        //    CURLOPT_COOKIEJAR      => $cookie_file,
        CURLOPT_RETURNTRANSFER => true,
//    CURLOPT_HEADERFUNCTION => 'readHeader',
        //    CURLOPT_USERPWD        => 'siteusername:sitepassword',
        CURLOPT_HTTPPROXYTUNNEL => env('CURLOPT_HTTPPROXYTUNNEL', false),
        CURLOPT_PROXY => env('CURLOPT_PROXY', ''),
        CURLOPT_PROXYPORT => env('CURLOPT_PROXYPORT', ''),
        CURLOPT_PROXYTYPE => env('CURLOPT_PROXYTYPE', CURLPROXY_HTTP),
        CURLOPT_PROXYAUTH => env('CURLOPT_PROXYAUTH', ''),
        CURLOPT_PROXYUSERPWD => sprintf('%s:%s', env('CURLOPT_PROXYUSER', ''), env('CURLOPT_PROXYPWD', '')),
    );
    //Parâmetros de PROXY, devem ser configurados no arquivo .env.
    if (env('CURL_USE_PROXY', 'false') == false) {
        unset($opts[CURLOPT_HTTPPROXYTUNNEL]);
        unset($opts[CURLOPT_PROXY]);
        unset($opts[CURLOPT_PROXYPORT]);
        unset($opts[CURLOPT_PROXYTYPE]);
        unset($opts[CURLOPT_PROXYAUTH]);
        unset($opts[CURLOPT_PROXYUSERPWD]);
    }

    foreach ($options as $key => $value) {
        $opts[$key] = $value;
    }
    return $opts;
}

//Propriedades para redimencionar

define('PROPORTIONAL_RESIZE', 0); //false/false //Redimenciona mantendo proporções o maior tamanho terá o tamanho exigido
define('FIXED_SIZE_WITH_BORDERS', 1); //true/false //Redimenciona mantendo proporções porem o lado menor é compensado com área branca em ambos os lados
define('PROPORTIONAL_RESIZE_MAX', 2); //false/true //Redimencion mantendo proporções o menor lado terá o tamanho exigido
define('PROPORTIONAL_RESIZE_MAX_CUT', 3); //true/true //Redimenciona mantedo proporções o menor lado terá o tamanho exigido a sobra do maior lado é cortado
/**
 * Undocumented function
 *
 * @param [type] $path URL da imagem
 * @param [type] $formatIn Formato de entrada
 * @param [type] $formatOut Formato de saída
 * @param [type] $width Largura máxima
 * @param [type] $height Altura máxima
 * @param [type] $resizeType Tipo de redimencionamento
 * @param boolean $cache Indica se o resultado será salvo
 * @return void
 */
function resizeImage_($path, $formatIn, $formatOut, $width, $height, $resizeType = PROPORTIONAL_RESIZE, $cache = true)
{

 
    $cache = true;

    if ($cache) {
        $baseName = md5($path) . '.' . $formatOut;


        $privatePath = getcwd().'/';
        //print($privatePath);exit();
        $publicPath = '/imagens/resized/' . $resizeType . "/" . $width . "/" . $height;
        $outPath = $privatePath . $publicPath;
        $fileNameOut = $outPath  . '/'. $baseName;

        if (!file_exists($outPath)) {
            mkdir($outPath, 0777, true);
        }

        
        if (file_exists($fileNameOut)) {
            header('Location: ' . $publicPath . '/' . $baseName);exit();
        }
        $ar = explode('/',$path);
        $filename = array_pop($ar);
        $filename = explode('?',$filename);
        $filename = array_shift($filename);

        
        if (strpos($path, 'instagram')> -1){
            # se for do instagram coloca em uma pasta específica
            $local_file =  $privatePath .  'imagens/instagram/'. $filename;  
            // local_file é o arquivo antes do tratamento

        } else{
            //os arquivos do portal são verificados de acordo com o local original
            if (strpos($path, '200.198.6.250')> -1){
               $path = str_replace('200.198.6.250', 'www.minasgerais.com.br',$path);
            }
            $ar = explode('minasgerais.com.br/',$path);
            $internalpath = array_pop($ar);
            $local_file =  $privatePath . $internalpath;  
            //print($local_file);
            //exit();

        }
     

        if ($_SERVER['HTTP_HOST']!= 'www.minasgerais.com.br') {
            #baixar imagen  
            //print($path);
            //exit();
            /* Necessário para funcionar assets que não estão em produção */
            if (strpos($path,'portal/assets') > 0){
                $local_file = str_replace('http://'.$_SERVER['HTTP_HOST'], $privatePath,$path);
                $path = str_replace($_SERVER['HTTP_HOST'], 'www.minasgerais.com.br',$path);
                
            }

        }   


        if (!file_exists($local_file) ) { //evita que a produção baixe seu próprio arquivo
            //print ('wget ' . $path . ' -O '. $local_file );
            //exit();
            exec('wget ' . $path . ' -O '.  $local_file , $r, $s);
        } 

        if (!file_exists($local_file)) {
            print ('wget ' . $path . ' -O '. $local_file );
            print_r($r);
            print('<br>');
            print_r($s);
            print('<br>');
            print_r($local_file);
            print('<br>');
            print_r($path);
            exit();
        } 
        
    } else{
        $local_file = $path;
    }





  //Tamanho da moudura
    $widthfix = $width;
    $heightfix = $height;
    if (filesize($local_file)==0){
        header("Location: /image/$width/$height/true/true/http://www.minasgerais.com.br/assets/img/aguardando.jpg");exit();
    }
    //Pega o tamnho da imagem
    list($width_orig, $height_orig) = getimagesize($local_file);

    //Abandona função caso não consiga ler o tamanho da imagem
    if (($width_orig == 0) || ($height_orig == 0)) {
        header("Location: /image/$width/$height/true/true/http://www.minasgerais.com.br/assets/img/aguardando.jpg");exit();
    }

    //Realiza os cálculos de proporção
    $ratio_orig = $width_orig / $height_orig;

    if ($width / $height > $ratio_orig) {
        if ($resizeType == 2 || $resizeType == 3) {
            $height = $width / $ratio_orig;
        } else {
            $width = $height * $ratio_orig;
        }

    } else {
        if ($resizeType == 2 || $resizeType == 3) {
            $width = $height * $ratio_orig;
        } else {
            $height = $width / $ratio_orig;
        }
    }

    if ($resizeType == 1 || $resizeType == 3) {
        // Cria imagem com proporsões fixadas
        $image_p = imagecreatetruecolor($widthfix, $heightfix);
    } else {
        // Cria imagem com as novas dimensões
        $image_p = imagecreatetruecolor($width, $height);
    }

    //Ablica cor de fundo branca e tranparênte caso o formato a ser salvo permita
    imagesavealpha($image_p, true);
    $color = imagecolorallocatealpha($image_p, 0xff, 0xff, 0xff, 127);
    imagefill($image_p, 0, 0, $color);

    //Verifica estenção e importa imagem
    switch ($formatIn) {
        case 'jpeg':
            $image = imagecreatefromjpeg($local_file);
            break;
        case 'jpg':
            $image = imagecreatefromjpeg($local_file);
            break;
        case 'png':
            $image = imagecreatefrompng($local_file);
            break;
        case 'gif':
            $image = imageCreateFromGif($local_file);
            break;
    }

    //Redimenciona centralizando a imagem

    if ($resizeType == 1 || $resizeType == 3) {
        $dst_x = ($widthfix - $width) / 2;
        $dst_y = ($heightfix - $height) / 2;
    } else {
        $dst_x = 0;
        $dst_y = 0;
    }

    imagecopyresampled($image_p, $image, $dst_x, $dst_y, 0, 0, $width, $height, $width_orig, $height_orig);



    //Salva Imagem
    switch ($formatOut) {
        case 'jpeg':
            if (!$cache) {
                header("Content-type: image/jpeg");
            }

            imagejpeg($image_p, $fileNameOut, 75);
            break;
        case 'jpg':
            if (!$cache) {
                header("Content-type: image/jpeg");
            }

            imagejpeg($image_p, $fileNameOut, 75);
            break;
        case 'png':
            if (!$cache) {
                header('Content-type: image/png');
            }

            imagepng($image_p, $fileNameOut);
            break;
        case 'gif':
            if (!$cache) {
                header("Content-type: image/gif");
            }

            imagegif($image_p, $fileNameOut, 75);
            break;
    }

    imagedestroy($image_p);

    if (file_exists($fileNameOut)) {
        header('Location: ' . $publicPath . '/' . $baseName);exit();
    } 

}

function get_instagram_images(){



        $local_file = getcwd().'/../instagram.json';

        if (is_file($local_file) && (filesize($local_file)>0)) {
            $contents = file_get_contents($local_file); 
            $listaInstagram = json_decode($contents);

            $v_InstagramPictures = $listaInstagram->data;
        } else {
            // Define lista de retorno como vazia 
            $v_InstagramPictures = [];
        }


        return $v_InstagramPictures;
}

function get_cities_data_especial(){

            //Pega nomes das cidades para preencher campo lista de cidades
            $ccity = DB::select("select distinct city_id, name from  (select e.id, e.city_id, c.name, e.nome, inicio, termino
            from event e, 
                (select e.id, min(date) as inicio
                from event_date ed, event e
                where ed.event_id=e.id
                group by e.id) as di,
                (select e.id, max(date) as termino
                from event_date ed, event e
                where ed.event_id=e.id
                group by e.id) as dt,
                city c, event_selected_classification ec
            where e.id = dt.id
            and ec.event_id = e.id
            and ec.event_classification_id=5
            and e.id = di.id
            and e.city_id = c.id
            and termino < '2019-04-22'
            and inicio >=  '2019-03-01' 
            and inicio < '2019-04-22'
            and e.revision_status_id = 4
            )  carnavais
            order by name");   
            //var_dump($ccity);
            $cids = [];
            foreach ($ccity as $row) {
                $cids[] = $row->city_id;
            }
    

            return $cids;
}
function get_limite_data_especial(){
    return "2019-04-21 00:00:00";  
}

function get_circuito($city_id){
    $rows =DB::select("select touristic_circuit_id
    from touristic_circuit_cities
    where city_id=$city_id");
    if (count($rows)==1){
        return $rows[0]->touristic_circuit_id;
    } else{
        return '';
    }
}

function get_city_slug($city_id){
    $rows =DB::select("select *
    from city
    where id=$city_id ");
    if (count($rows)==1){
        return $rows[0]->slug;
    } else{
        return '';
    }
}

function encurta($texto,$size=35){
    if (strlen($texto)> $size) { 
        return substr($texto,0,$size-3)."..."; 
    } else { 
        return $texto ;
    } 
}

function slugify($string) {
    $string = preg_replace('/[\t\n]/', ' ', $string);
    $string = preg_replace('/\s{2,}/', ' ', $string);
    $list = array(
        'Š' => 'S',
        'š' => 's',
        'Đ' => 'Dj',
        'đ' => 'dj',
        'Ž' => 'Z',
        'ž' => 'z',
        'Č' => 'C',
        'č' => 'c',
        'Ć' => 'C',
        'ć' => 'c',
        'À' => 'A',
        'Á' => 'A',
        'Â' => 'A',
        'Ã' => 'A',
        'Ä' => 'A',
        'Å' => 'A',
        'Æ' => 'A',
        'Ç' => 'C',
        'È' => 'E',
        'É' => 'E',
        'Ê' => 'E',
        'Ë' => 'E',
        'Ì' => 'I',
        'Í' => 'I',
        'Î' => 'I',
        'Ï' => 'I',
        'Ñ' => 'N',
        'Ò' => 'O',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ö' => 'O',
        'Ø' => 'O',
        'Ù' => 'U',
        'Ú' => 'U',
        'Û' => 'U',
        'Ü' => 'U',
        'Ý' => 'Y',
        'Þ' => 'B',
        'ß' => 'Ss',
        'à' => 'a',
        'á' => 'a',
        'â' => 'a',
        'ã' => 'a',
        'ä' => 'a',
        'å' => 'a',
        'æ' => 'a',
        'ç' => 'c',
        'è' => 'e',
        'é' => 'e',
        'ê' => 'e',
        'ë' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'î' => 'i',
        'ï' => 'i',
        'ð' => 'o',
        'ñ' => 'n',
        'ò' => 'o',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ö' => 'o',
        'ø' => 'o',
        'ù' => 'u',
        'ú' => 'u',
        'û' => 'u',
        'ý' => 'y',
        'ý' => 'y',
        'þ' => 'b',
        'ÿ' => 'y',
        'Ŕ' => 'R',
        'ŕ' => 'r',
        '/' => ' ',
        ' ' => ' ',
        '.' => ' ',
        '!' => ' ',
        '?' => ' ',
    );

    $string = strtr($string, $list);
    $string = preg_replace('/-{2,}/', '-', $string);
    $string = strtolower($string);

    return $string;
}