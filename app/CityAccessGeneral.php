<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class CityAccessGeneral extends Model
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    protected $revisionCreationsEnabled = true;
    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 500; //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.

    protected $table = 'city_access_general';
    protected $guarded = [];

    public static function getDT($p_City, $p_CreatedAt, $p_UpdatedAt, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = CityAccessGeneral::join('city', 'city.id', '=', 'city_access_general.city_id')->join('revision_status', 'revision_status.id', '=', 'city_access_general.revision_status_id')
                           ->select(DB::raw('SQL_CALC_FOUND_ROWS city_access_general.id, city.name as municipio, city_access_general.created_at, city_access_general.updated_at, revision_status.name as status'));

        if(UserType::isMunicipio())
            $v_Query->where('city.id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city.id', TouristicCircuitCities::getUserCircuitCities());

        if($p_City != '')
            $v_Query->where('city.name', 'LIKE', '%' . $p_City . '%');

        if($p_CreatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 0, 10));
            $v_Query->where('city_access_general.created_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_CreatedAt, 13, 23));
            $v_Query->where('city_access_general.created_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_UpdatedAt != '')
        {
            $v_StartDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 0, 10));
            $v_Query->where('city_access_general.updated_at', '>=', $v_StartDate->startOfDay()->format('Y-m-d H:i:s'));
            $v_EndDate = Carbon::createFromFormat('d/m/Y', substr($p_UpdatedAt, 13, 23));
            $v_Query->where('city_access_general.updated_at', '<=', $v_EndDate->endOfDay()->format('Y-m-d H:i:s'));
        }

        if($p_Status != '')
            $v_Query->where('city_access_general.revision_status_id',  $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('city_access_general.created_at', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('city_access_general.updated_at', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('revision_status.name', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['municipio'],
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['created_at'])->format('d/m/Y - H:i'),
                Carbon::createFromFormat('Y-m-d H:i:s', $v_QueryRes[$c_Index]['updated_at'])->format('d/m/Y - H:i'),
                $v_QueryRes[$c_Index]['status'],
                '<div class="actions-div">' .
                '<a href="' . url('admin/inventario/meios-acesso/geral/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                '<a href="' . url('admin/inventario/meios-acesso/geral/historico/' . $v_QueryRes[$c_Index]['id']) . '" title="Histórico" type="button" class="btn btn-success"><i class="fa fa-history"></i></a>' .
                ($v_IsParceiro ? '' : '<a href="' . url('admin/inventario/meios-acesso/geral/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = CityAccessGeneral::getTotalRows();

        if(UserType::isMunicipio())
            $v_DataTableAjax->recordsTotal = CityAccessGeneral::where('city_id', Auth::user()->city_id)->count();
        else if(UserType::isCircuito())
            $v_DataTableAjax->recordsTotal = CityAccessGeneral::whereIn('city_id', TouristicCircuitCities::getUserCircuitCities())->count();
        else
            $v_DataTableAjax->recordsTotal = CityAccessGeneral::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function getCityAccessGeneralIds()
    {
        return CityAccessGeneral::lists('city_id')->toArray();
    }

    public static function post($p_Id, $p_Data)
    {
        $v_PreviousStatus = 0;
        if($p_Id != null)
            $v_PreviousStatus = CityAccessGeneral::find($p_Id)->revision_status_id;

        $v_Item = CityAccessGeneral::updateOrCreate(['id' => $p_Id], $p_Data);
        if($p_Id != null && $v_PreviousStatus != 4){
            $v_City = City::find($v_Item->city_id)->name;
            $v_Subject = null;
            $v_Message = null;
            if($v_Item->revision_status_id == 4) {
                $v_Subject = 'Alterações aprovadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item A2.1 - ' . $v_City . ' foram aprovadas.</p><p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            else if($v_Item->revision_status_id == 5) {
                $v_Subject = 'Alterações rejeitadas';
                $v_Message = '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">As alterações do item A2.1 - ' . $v_City . ' foram rejeitadas.</p>';
                if(!empty($v_Item->comentario_revisao))
                    $v_Message .= '<p style="font-size: 16px;line-height: 24px;padding: 25px 0;">Motivo: ' . $v_Item->comentario_revisao . '</p>';
                $v_Message .= '<p id="call" style="font-size: 16px;line-height: 24px;color: #f26522;text-align: center;padding: 0;">Consulte-o no <a href="' . url('/admin') . '" style="color: #f26522;text-decoration: underline;">Descubra Mato Grosso</a>.</p>';
            }
            if($v_Message != null) {
                $v_Users = User::getItemUsers($v_Item->city_id, 'A2', $v_Item->id);
                foreach($v_Users as $c_User) {
                    Mail::send('emails.generic', ['p_Title' => $v_Subject, 'p_Msg' => $v_Message],
                        function ($message) use ($c_User, $v_Subject)
                        {
                            $message->to($c_User->email, $c_User->name)->subject('Descubra Mato Grosso - ' . $v_Subject);
                        });
                }
            }
        }
    }

    public static function getReport($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];

        $v_Query = CityAccessGeneral::leftJoin('city', 'city.id', '=', 'city_access_general.city_id')
                                    ->leftJoin('touristic_circuit_cities', 'city_access_general.city_id', '=', 'touristic_circuit_cities.city_id')
                                    ->leftJoin('touristic_circuit', 'touristic_circuit_cities.touristic_circuit_id', '=', 'touristic_circuit.id')
                                    ->leftJoin('destination', 'destination.city_id', '=', 'city_access_general.city_id')
                                    ->leftJoin('region', 'destination.region_id', '=', 'region.id')
                                    ->selectRaw('city_access_general.*, city.name as cidade, touristic_circuit.nome as circuito, region.name as regiao')
                                    ->groupBy('city_access_general.id');

        if(array_key_exists('city_id', $p_Data))
            $v_Query->whereIn('city_access_general.city_id', $p_Data['city_id']);

        if($v_CircuitId != '')
            $v_Query->where('touristic_circuit_cities.touristic_circuit_id', $v_CircuitId);

        if($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioA2-1.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    try {
                        array_push($v_Data, $c_Result->cidade);
                        array_push($v_Data, $c_Result->circuito);
                        array_push($v_Data, $c_Result->regiao);

                        $v_RodoviasJSON = empty($c_Result->rodovia_federal) ? [] : json_decode($c_Result->rodovia_federal,1);
                        $v_Rodovias = '';
                        foreach($v_RodoviasJSON as $c_Index => $c_Rodovia)
                            $v_Rodovias .= ($c_Index > 0 ? ';\n' : '') . $c_Rodovia['nome'] . ' - Sinalizaçao geral: ' . $c_Rodovia['sinalizacaoGeral'] . ' - Sinalizaçao turística: ' . $c_Rodovia['sinalizacaoTuristica'] . ' - Pavimentação: ' . $c_Rodovia['pavimentacao'] . ' - Conservação: ' . $c_Rodovia['conservacao'] . ' - Pedágio: ' . $c_Rodovia['pedagio'] . ($c_Rodovia['pedagio'] == 'Sim' ? (' - Valor: R$' . str_replace('.', ',', $c_Rodovia['valor'])) : '');
                        array_push($v_Data, stripcslashes($v_Rodovias));

                        $v_RodoviasJSON = empty($c_Result->rodovia_estadual) ? [] : json_decode($c_Result->rodovia_estadual,1);
                        $v_Rodovias = '';
                        foreach($v_RodoviasJSON as $c_Index => $c_Rodovia)
                            $v_Rodovias .= ($c_Index > 0 ? ';\n' : '') . $c_Rodovia['nome'] . ' - Sinalizaçao geral: ' . $c_Rodovia['sinalizacaoGeral'] . ' - Sinalizaçao turística: ' . $c_Rodovia['sinalizacaoTuristica'] . ' - Pavimentação: ' . $c_Rodovia['pavimentacao'] . ' - Conservação: ' . $c_Rodovia['conservacao'] . ' - Pedágio: ' . $c_Rodovia['pedagio'] . ($c_Rodovia['pedagio'] == 'Sim' ? (' - Valor: R$' . str_replace('.', ',', $c_Rodovia['valor'])) : '');
                        array_push($v_Data, stripcslashes($v_Rodovias));

                        $v_RodoviasJSON = empty($c_Result->rodovia_municipal) ? [] : json_decode($c_Result->rodovia_municipal,1);
                        $v_Rodovias = '';
                        foreach($v_RodoviasJSON as $c_Index => $c_Rodovia)
                            $v_Rodovias .= ($c_Index > 0 ? ';\n' : '') . $c_Rodovia['nome'] . ' - Sinalizaçao geral: ' . $c_Rodovia['sinalizacaoGeral'] . ' - Sinalizaçao turística: ' . $c_Rodovia['sinalizacaoTuristica'] . ' - Pavimentação: ' . $c_Rodovia['pavimentacao'] . ' - Conservação: ' . $c_Rodovia['conservacao'] . ' - Pedágio: ' . $c_Rodovia['pedagio'] . ($c_Rodovia['pedagio'] == 'Sim' ? (' - Valor: R$' . str_replace('.', ',', $c_Rodovia['valor'])) : '');
                        array_push($v_Data, stripcslashes($v_Rodovias));

                        array_push($v_Data, $c_Result->equipe_responsavel_responsavel);
                        array_push($v_Data, $c_Result->equipe_responsavel_instituicao);
                        array_push($v_Data, $c_Result->equipe_responsavel_telefone);
                        array_push($v_Data, $c_Result->equipe_responsavel_email);
                        array_push($v_Data, $c_Result->equipe_responsavel_observacao);

                        $sheet->row($v_CurrentRow,$v_Data);
                        $v_CurrentRow++;
                    } catch(\Exception $e){
                    }
                    $c_Result = null;
                }
                $sheet->setBorder('A2:I'.($v_CurrentRow-1), 'thin');
            });
        })->download('xlsx');
    }
}