<?php
namespace App;

use App\Http\Controllers\CircuitController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TouristicCircuit extends Model
{
    protected $table = 'touristic_circuit';
    protected $guarded = [];

    public static function getDT($p_Name, $p_City, $p_Responsible, $p_Status, $p_Order, $p_Start, $p_Length, $p_Draw)
    {
        $v_Query = TouristicCircuit::join('city', 'city.id', '=', 'touristic_circuit.city_id')
            ->leftJoin('user', 'user.id', '=', 'touristic_circuit.responsavel_id')
            ->select(DB::raw('SQL_CALC_FOUND_ROWS touristic_circuit.id, touristic_circuit.nome,
            touristic_circuit.status, user.name as responsavel, city.name as municipio'));

        if($p_Name != '')
            $v_Query->where('touristic_circuit.nome', 'LIKE', '%' . $p_Name . '%');

        if($p_City != '')
            $v_Query->where('touristic_circuit.city_id', $p_City);

        if($p_Responsible != '')
            $v_Query->where('user.name', 'LIKE', '%' . $p_Responsible . '%');

        if($p_Status != '')
            $v_Query->where('touristic_circuit.status', $p_Status);

        if($p_Order != null)
        {
            if($p_Order["column"] == 0)
                $v_Query->orderBy('touristic_circuit.nome', $p_Order["dir"]);
            if($p_Order["column"] == 1)
                $v_Query->orderBy('city.name', $p_Order["dir"]);
            if($p_Order["column"] == 2)
                $v_Query->orderBy('user.name', $p_Order["dir"]);
            if($p_Order["column"] == 3)
                $v_Query->orderBy('touristic_circuit.status', $p_Order["dir"]);
        }

        if($p_Length != -1)
            $v_Query->take($p_Length)->skip($p_Start);

        $v_QueryRes = $v_Query->get()->toArray();
        $v_Data = [];
        $v_IsParceiro = UserType::isParceiro();
        for($c_Index = 0 ; $c_Index < sizeof($v_QueryRes) ; $c_Index++)
        {
            array_push($v_Data, [
                $v_QueryRes[$c_Index]['nome'],
                $v_QueryRes[$c_Index]['municipio'],
                $v_QueryRes[$c_Index]['responsavel'],
                CircuitController::$m_CircuitStatus[$v_QueryRes[$c_Index]['status']],
                '<div class="actions-div">' .
                    '<a href="' . url('admin/circuitos/editar/' . $v_QueryRes[$c_Index]['id']) . '" title="Editar" type="button" class="btn btn-success"><i class="fa fa-edit"></i></a>' .
                    ($v_IsParceiro ? '' : '<a href="' . url('admin/circuitos/excluir/' . $v_QueryRes[$c_Index]['id']) . '" title="Excluir" type="button" class="btn btn-success delete-btn"><i class="fa fa-trash-o"></i></a>') .
                '</div>'
            ]);
        }

        $v_DataTableAjax = new \stdClass();
        $v_DataTableAjax->draw = $p_Draw;
        $v_DataTableAjax->recordsFiltered = TouristicCircuit::getTotalRows();
        $v_DataTableAjax->recordsTotal = TouristicCircuit::count();
        $v_DataTableAjax->data = $v_Data;
        return json_encode($v_DataTableAjax);
    }

    public static function getTotalRows()
    {
        return DB::select(DB::raw("SELECT FOUND_ROWS() AS total_rows"))[0]->total_rows;
    }

    public static function post($p_Data)
    {
        if(array_key_exists('id', $p_Data))
        {
            $v_Id = $p_Data['id'];
            unset($p_Data['id']);
            $p_Data['updated_by'] = Auth::id();
        }
        else
        {
            $v_Id =  null;
            $p_Data['updated_by'] = Auth::id();
            $p_Data['created_by'] = Auth::id();
        }

        unset($p_Data['_token']);

        $v_CircuitCities = $p_Data['municipios_participantes'];
        unset($p_Data['municipios_participantes']);

        array_walk($p_Data, function (&$c_Item) {
            $c_Item = ($c_Item === '') ? null : $c_Item;
        });

        $v_Circuit = TouristicCircuit::updateOrCreate(['id' => $v_Id], $p_Data);

        TouristicCircuitCities::updateTouristicCircuitCities($v_Circuit->id, $v_CircuitCities);
    }

    public static function getList()
    {
        return ['' => ''] + TouristicCircuit::orderBy('nome')->lists('nome', 'id')->toArray();
    }

    public static function getUserList()
    {
        return TouristicCircuit::orderBy('nome')->lists('nome', 'id')->toArray();
    }

    public static function getReportsQuery($p_Data)
    {
        $v_CircuitId = $p_Data['touristic_circuit_id'];
        $v_RegionId = $p_Data['region_id'];
        $v_Status = $p_Data['status'];

        $v_Query = TouristicCircuit::leftJoin('user', 'user.id', '=', 'touristic_circuit.responsavel_id')
                                   ->join('city', 'city.id', '=', 'touristic_circuit.city_id')
                                   ->leftJoin('destination', 'destination.city_id', '=', 'touristic_circuit.city_id')
                                   ->leftJoin('region', 'region.id', '=', 'destination.region_id')
                                   ->select('touristic_circuit.*', 'city.name as cidade', 'user.name as responsavel', 'region.name as regiao')
                                   ->groupBy('touristic_circuit.id');

        if($v_CircuitId != '')
            $v_Query->where('touristic_circuit.id', $v_CircuitId);

        if($v_RegionId != '')
            $v_Query->where('destination.region_id', $v_RegionId);

        if($v_Status != '')
            $v_Query->where('touristic_circuit.status', $v_Status);

        return $v_Query;
    }

    public static function getFullReports($p_Data)
    {
        $v_Query = TouristicCircuit::getReportsQuery($p_Data);
        $v_Results = $v_Query->with('cities')->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioCircuitosTuristicos.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    array_push($v_Data, $c_Result->nome);
                    array_push($v_Data, $c_Result->nome_oficial);
                    array_push($v_Data, CircuitController::$m_CircuitStatus[$c_Result->status]);
                    array_push($v_Data, $c_Result->situacao_certificacao);
                    array_push($v_Data, $c_Result->observacao);
                    array_push($v_Data, $c_Result->cidade);
                    array_push($v_Data, $c_Result->regiao);
                    array_push($v_Data, $c_Result->cep);
                    array_push($v_Data, $c_Result->bairro);
                    array_push($v_Data, $c_Result->logradouro);
                    array_push($v_Data, $c_Result->numero);
                    array_push($v_Data, $c_Result->complemento);
                    array_push($v_Data, $c_Result->telefone);
                    array_push($v_Data, $c_Result->email);
                    array_push($v_Data, $c_Result->site);
                    $v_Cities = '';
                    foreach($c_Result->cities as $c_Index => $c_City)
                        $v_Cities .= ($c_Index > 0 ? ', ' : '') . $c_City->name;
                    array_push($v_Data, $v_Cities);
                    array_push($v_Data, $c_Result->historico);
                    array_push($v_Data, $c_Result->contato_presidente_nome);
                    array_push($v_Data, $c_Result->contato_presidente_telefone);
                    array_push($v_Data, $c_Result->contato_presidente_email);
                    array_push($v_Data, $c_Result->contato_gestor_nome);
                    array_push($v_Data, $c_Result->contato_gestor_telefone);
                    array_push($v_Data, $c_Result->contato_gestor_email);
                    array_push($v_Data, $c_Result->contato_assessor_nome);
                    array_push($v_Data, $c_Result->contato_assessor_telefone);
                    array_push($v_Data, $c_Result->contato_assessor_email);
                    array_push($v_Data, $c_Result->contato_secretaria_nome);
                    array_push($v_Data, $c_Result->contato_secretaria_telefone);
                    array_push($v_Data, $c_Result->contato_secretaria_email);
                    array_push($v_Data, $c_Result->contato_tesoureiro_nome);
                    array_push($v_Data, $c_Result->contato_tesoureiro_telefone);
                    array_push($v_Data, $c_Result->contato_tesoureiro_email);
                    array_push($v_Data, $c_Result->responsavel);

                    $sheet->row($v_CurrentRow,$v_Data);
                    $v_CurrentRow++;
                }
                $sheet->setBorder('A2:AG'.(count($v_Results)+1), 'thin');
            });
        })->download('xlsx');
    }

    public static function getSimplifiedReports($p_Data)
    {
        $v_Query = TouristicCircuit::getReportsQuery($p_Data);
        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioResumoCircuitosTuristicos.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    array_push($v_Data, $c_Result->nome);
                    array_push($v_Data, $c_Result->nome_oficial);
                    array_push($v_Data, CircuitController::$m_CircuitStatus[$c_Result->status]);
                    array_push($v_Data, $c_Result->situacao_certificacao);
                    array_push($v_Data, $c_Result->site);
                    array_push($v_Data, $c_Result->regiao);
                    array_push($v_Data, $c_Result->contato_presidente_nome);
                    array_push($v_Data, $c_Result->contato_presidente_telefone);
                    array_push($v_Data, $c_Result->contato_presidente_email);
                    array_push($v_Data, $c_Result->contato_gestor_nome);
                    array_push($v_Data, $c_Result->contato_gestor_telefone);
                    array_push($v_Data, $c_Result->contato_gestor_email);
                    array_push($v_Data, $c_Result->contato_assessor_nome);
                    array_push($v_Data, $c_Result->contato_assessor_telefone);
                    array_push($v_Data, $c_Result->contato_assessor_email);
                    array_push($v_Data, $c_Result->contato_secretaria_nome);
                    array_push($v_Data, $c_Result->contato_secretaria_telefone);
                    array_push($v_Data, $c_Result->contato_secretaria_email);
                    array_push($v_Data, $c_Result->contato_tesoureiro_nome);
                    array_push($v_Data, $c_Result->contato_tesoureiro_telefone);
                    array_push($v_Data, $c_Result->contato_tesoureiro_email);
                    array_push($v_Data, $c_Result->responsavel);

                    $sheet->row($v_CurrentRow,$v_Data);
                    $v_CurrentRow++;
                }
                $sheet->setBorder('A2:V'.(count($v_Results)+1), 'thin');
            });
        })->download('xlsx');
    }

    public static function getContactsReports($p_Data)
    {
        $v_Query = TouristicCircuit::getReportsQuery($p_Data);
        $v_Results = $v_Query->get();

        $v_TemplateFile = public_path() . '/templates-relatorios/RelatorioContatosCircuitosTuristicos.xlsx';
        \Excel::load($v_TemplateFile, function($v_Template) use($v_Results){
            $v_Template->sheet('Relatorio', function($sheet) use($v_Results) {
                $v_CurrentRow = 2;
                foreach($v_Results as $c_Result)
                {
                    $v_Data = [];
                    array_push($v_Data, $c_Result->nome);
                    array_push($v_Data, $c_Result->contato_presidente_nome);
                    array_push($v_Data, $c_Result->contato_presidente_telefone);
                    array_push($v_Data, $c_Result->contato_presidente_email);
                    array_push($v_Data, $c_Result->contato_gestor_nome);
                    array_push($v_Data, $c_Result->contato_gestor_telefone);
                    array_push($v_Data, $c_Result->contato_gestor_email);
                    array_push($v_Data, $c_Result->contato_assessor_nome);
                    array_push($v_Data, $c_Result->contato_assessor_telefone);
                    array_push($v_Data, $c_Result->contato_assessor_email);
                    array_push($v_Data, $c_Result->contato_secretaria_nome);
                    array_push($v_Data, $c_Result->contato_secretaria_telefone);
                    array_push($v_Data, $c_Result->contato_secretaria_email);
                    array_push($v_Data, $c_Result->contato_tesoureiro_nome);
                    array_push($v_Data, $c_Result->contato_tesoureiro_telefone);
                    array_push($v_Data, $c_Result->contato_tesoureiro_email);
                    array_push($v_Data, $c_Result->responsavel);

                    $sheet->row($v_CurrentRow,$v_Data);
                    $v_CurrentRow++;
                }
                $sheet->setBorder('A2:Q'.(count($v_Results)+1), 'thin');
            });
        })->download('xlsx');
    }

    public function cities()
    {
        return $this->hasMany('App\TouristicCircuitCities')
                    ->join('city', 'city.id', '=', 'touristic_circuit_cities.city_id')
                    ->select('touristic_circuit_cities.touristic_circuit_id', 'city.name')
                    ->orderBy('name');
    }
}