<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TouristicRouteDay extends Model
{
    public $timestamps = false;
    protected $table = 'touristic_route_day';

    public static function post($p_TouristicRouteId, $p_Duration, $p_Data)
    {
        TouristicRouteDay::where('touristic_route_id', $p_TouristicRouteId)->delete();

        foreach($p_Data as $c_Index => $p_Description)
        {
            $v_TouristicRouteDay = new TouristicRouteDay();
            $v_TouristicRouteDay->touristic_route_id = $p_TouristicRouteId;
            $v_TouristicRouteDay->dia = ($c_Index%$p_Duration) + 1;
            $v_TouristicRouteDay->descricao = $p_Description;
            $v_TouristicRouteDay->save();
        }
    }

    public static function getDays($p_TouristicRouteId)
    {
        return TouristicRouteDay::where('touristic_route_id', $p_TouristicRouteId)->orderBy('dia')->get();
    }
}