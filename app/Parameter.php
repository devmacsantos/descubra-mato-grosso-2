<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Parameter extends Model
{
    public $timestamps = false;
    protected $table = 'parameter';

    public static function postParameter($p_Key, $p_Value)
    {
        $v_Parameter = new Parameter();
        $v_Parameter->key = $p_Key;
        $v_Parameter->value = $p_Value;
        $v_Parameter->save();
        return true;
    }

    public static function postParameters($p_Params)
    {
        foreach($p_Params as $c_Key => $c_Value)
        {
            $v_Parameter = Parameter::where('key', '=', $c_Key)->first();
            if($v_Parameter == null)
            {
                $v_Parameter = new Parameter();
                $v_Parameter->key = $c_Key;
            }
            $v_Parameter->value = $c_Value;
            $v_Parameter->save();
        }
    }

    public static function postVideoUrls($p_Params)
    {
        Parameter::where('key', 'like', 'video_gallery_url_%')->delete();
        foreach($p_Params as $c_Key => $c_Value)
        {
            $v_Parameter = new Parameter();
            $v_Parameter->key = 'video_gallery_url_' . $c_Key;
            $v_Parameter->value = $c_Value;
            $v_Parameter->save();
        }
    }

    public static function deleteParameter($p_Id)
    {
        $v_Parameter = Parameter::find($p_Id);
        if ($v_Parameter == null)
            return redirect()->back()->with('error_message', 'Esse parametro não existe.');
        $v_Parameter->delete();
        return redirect()->back()->with('message', 'Parametro deletado com sucesso!');
    }

    public static function getParameters()
    {
        $v_Query = Parameter::select('key', 'value');
        $v_Response['error'] = 'ok';
        $v_Response['parameters'] = $v_Query->get();
        return $v_Response;
    }

    public static function getParameter($p_Id)
    {
        $v_Query = Parameter::find($p_Id);
        $v_Query->select('key', 'value');
        return $v_Query->first();
    }

    public static function getParameterByKey($p_Key)
    {
        $v_Param = Parameter::where('key', '=', $p_Key)->first();
        if($v_Param == null)
            return null;
        else
            return $v_Param->value;
    }

    public static function getParameterLikeKey($p_Key)
    {
        return Parameter::where('key', 'like', $p_Key . '%')->get();
    }

    public static function getParametersByKeys($p_Keys)
    {
        return Parameter::whereIn('key', $p_Keys)->lists('value', 'key');
    }
}