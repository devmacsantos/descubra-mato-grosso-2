<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class TripAttractions extends Model
{
    public $timestamps = false;
    protected $table = 'trip_type_attractions';

    public static function updateTripTypes($p_AttractionId, $p_TripTypes)
    {
        TripAttractions::where('attraction_id', $p_AttractionId)->delete();
        foreach($p_TripTypes as $c_Type)
        {
            $v_Item = new TripAttractions();
            $v_Item->attraction_id = $p_AttractionId;
            $v_Item->trip_type_id = $c_Type;
            $v_Item->save();
        }
    }

    public static function getTripAttractions($p_TripTypeId, $p_Quantity)
    {
        return TripAttractions::join('attraction', 'attraction.id', '=', 'trip_type_attractions.attraction_id')
                              ->join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                              ->where('trip_type_attractions.trip_type_id', $p_TripTypeId)
                              ->where('attraction.atracao', 1)
                              ->where('attraction_photo.is_cover', 1)
                              ->select(['attraction.nome', 'attraction.slug', 'attraction.trade', 
                              'attraction_photo.url', 'attraction.descricao_curta', 'attraction.city_id'])
                              ->orderBy('attraction.destaque', 'desc')
                              ->take($p_Quantity)->get();
    }

    public static function getTripParks($p_TripTypeId, $p_Quantity)
    {
        return TripAttractions::join('attraction', 'attraction.id', '=', 'trip_type_attractions.attraction_id')
                              ->join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                              ->where('attraction.parque', 1)
                              ->where('attraction.atracao', 1)
                              ->where('trip_type_attractions.trip_type_id', $p_TripTypeId)
                              ->where('attraction_photo.is_cover', 1)
                              ->select(['attraction.nome', 'attraction.slug', 'attraction_photo.url',
                              'attraction.descricao_curta', 'attraction.city_id'])
                              ->orderBy('attraction.destaque', 'desc')
                              ->groupBy('attraction.id')
                              ->take($p_Quantity)->get();
    }

    public static function getTripAndCityParks($p_TripTypeId, $p_CityId, $p_Quantity)
    {
        return TripAttractions::join('attraction', 'attraction.id', '=', 'trip_type_attractions.attraction_id')
                              ->join('attraction_photo', 'attraction_photo.attraction_id', '=', 'attraction.id')
                              ->where('attraction.parque', 1)
                              ->where('attraction.atracao', 1)
                              ->where('attraction.city_id', $p_CityId)
                              ->where('trip_type_attractions.trip_type_id', $p_TripTypeId)
                              ->where('attraction_photo.is_cover', 1)
                              ->select(['attraction.nome', 'attraction.slug', 'attraction_photo.url', 
                              'attraction.descricao_curta', 'attraction.city_id'])
                              ->orderBy('attraction.destaque', 'desc')
                              ->groupBy('attraction.id')
                              ->take($p_Quantity)->get();
    }
}