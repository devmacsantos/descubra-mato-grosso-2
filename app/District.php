<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class District extends Model
{
    public $timestamps = false;
    protected $table = 'district';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:250'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:250'
    );

    public static function getList($p_CityId)
    {
        $v_List = District::where('city_id', $p_CityId)->orderBy('name')->lists('name', 'id')->toArray();
        return ['' => ''] + $v_List;
    }

    public static function getCityDistricts($p_CityId)
    {
        $v_Response['error'] = 'ok';
        $v_Response['data'] = District::where('city_id', $p_CityId)->orderBy('name')->lists('name', 'id')->toArray();
        return $v_Response;
    }

    public static function getCitiesDistricts($p_CityIds)
    {
        $v_Response['error'] = 'ok';
        $v_Response['data'] = District::whereIn('city_id', $p_CityIds)->orderBy('name')->lists('name', 'id')->toArray();
        return $v_Response;
    }

    public static function getAvailableCityDistricts($p_CityId)
    {
        $v_Response['error'] = 'ok';
        $v_Response['data'] = District::where('city_id', $p_CityId)->whereNotIn('id', Destination::getDistrictDestinationsIds())
            ->lists('name', 'id')->toArray();
        return $v_Response;
    }

    public static function post($p_Id, $p_CityId, $p_Name)
    {
        $v_District = District::findOrNew($p_Id);
        $v_District->city_id = $p_CityId;
        $v_District->name = $p_Name;
        $v_District->save();
    }
}