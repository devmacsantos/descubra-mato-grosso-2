<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileCarrier extends Model
{
    public $timestamps = false;
    protected $table = 'mobile_carrier';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:200'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:200'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_Carrier = MobileCarrier::findOrNew($p_Id);
        $v_Carrier->name = $p_Name;
        $v_Carrier->save();
    }

//    public static function getList()
//    {
//        $v_List = MobileCarrier::lists('name', 'id')->toArray();
//        return ['' => ''] + $v_List;
//    }
}