<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class InstitutionalPhoto extends Model
{
    public $timestamps = false;
    protected $table = 'institutional_photo';

    public static function updatePhotos($p_Photos, $p_PhotoInfo, $p_DeletedPhotos)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/paginas/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/conheca/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        foreach($p_DeletedPhotos as $c_PhotoId)
        {
            $v_Photo = InstitutionalPhoto::find($c_PhotoId);
            $v_OldFileName = explode('/', $v_Photo->url);
            $v_OldFileName = array_pop($v_OldFileName);
            $v_Photo->delete();
            \File::delete($v_Path . $v_OldFileName);
        }
        foreach($p_Photos as $c_Index => $c_File)
        {
            if($c_File != null)
            {
                $v_Photo = InstitutionalPhoto::findOrNew($p_PhotoInfo['id'][$c_Index]);
                if($p_PhotoInfo['type'][$c_Index] == 'testimonial')
                    $v_Photo->is_testimonial = 1;
                else
                    $v_Photo->is_testimonial = 0;

                $v_PhotoName =  time() . str_random(10) . '.jpg';
                $v_Image = Image::make($c_File);
                $v_Image->widen(1920, function ($constraint){
                    $constraint->upsize();
                });
                $v_Image->encode('jpg')->save($v_Path . $v_PhotoName);
                if ($v_Photo->url != null)
                {
                    $v_OldFileName = explode('/', $v_Photo->url);
                    $v_OldFileName = array_pop($v_OldFileName);
                    \File::delete($v_Path . $v_OldFileName);
                }
                $v_Photo->url = url('/imagens/paginas/conheca/' . $v_PhotoName);
                $v_Photo->save();
            }
        }
    }

    public static function getTestimonialPhoto()
    {
        return InstitutionalPhoto::where('is_testimonial', 1)->first();
    }

    public static function getPhotoGallery()
    {
        return InstitutionalPhoto::where('is_testimonial', 0)->get();
    }
}