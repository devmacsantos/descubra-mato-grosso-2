<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class InventoryPhoto extends Model
{
    public $timestamps = false;
    protected $table = 'inventory_photo';

    public static function updatePhotos($p_InventoryType, $p_Id, $p_Photos, $p_PhotoInfo, $p_DeletedPhotos)
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/inventario/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        foreach($p_DeletedPhotos as $c_PhotoId)
        {
            $v_Photo = InventoryPhoto::find($c_PhotoId);
            $v_OldFileName = explode('/', $v_Photo->url);
            $v_OldFileName = array_pop($v_OldFileName);
            $v_Photo->delete();
            \File::delete($v_Path . $v_OldFileName);
        }
        foreach($p_Photos as $c_Index => $c_File)
        {
            if($c_File != null)
            {
                $v_Photo = InventoryPhoto::findOrNew($p_PhotoInfo['id'][$c_Index]);
                if($p_PhotoInfo['type'][$c_Index] == 'cover')
                    $v_Photo->is_cover = 1;
                else
                    $v_Photo->is_cover = 0;

//                $v_Photo->creditos = $p_PhotoInfo['credits'][$c_Index];
                $v_Photo->item_inventario = $p_InventoryType;
                $v_Photo->item_inventario_id = $p_Id;
                $v_PhotoName =  time() . str_random(10) . '.jpg';
                $v_Image = Image::make($c_File);
                $v_Image->widen(1920, function ($constraint){
                    $constraint->upsize();
                });
                $v_Image->encode('jpg')->save($v_Path . $v_PhotoName);
                if ($v_Photo->url != null)
                {
                    $v_OldFileName = explode('/', $v_Photo->url);
                    $v_OldFileName = array_pop($v_OldFileName);
                    \File::delete($v_Path . $v_OldFileName);
                }
                $v_Photo->url = url('/imagens/inventario/' . $v_PhotoName);
                $v_Photo->save();
            }
        }
    }

    public static function deletePhotos($p_InventoryType, $p_Id)
    {
        $v_Photos = InventoryPhoto::where('item_inventario', $p_InventoryType)
                                  ->where('item_inventario_id', $p_Id)
                                  ->get();
        foreach($v_Photos as $c_Photo)
        {
            $v_Path = public_path() . '/imagens/inventario/';
            $v_FileName = explode('/', $c_Photo->url);
            $v_FileName = array_pop($v_FileName);
            \File::delete($v_Path . $v_FileName);
        }
    }

    public static function getCoverPhoto($p_InventoryType, $p_Id)
    {
        return InventoryPhoto::where('item_inventario', $p_InventoryType)
                             ->where('item_inventario_id', $p_Id)
                             ->where('is_cover', 1)->first();
    }

    public static function getPhotoGallery($p_InventoryType, $p_Id)
    {
        return InventoryPhoto::where('item_inventario', $p_InventoryType)
                             ->where('item_inventario_id', $p_Id)
                             ->where('is_cover', 0)->get();
    }

    public static function getItemPhotos($p_InventoryType, $p_Id)
    {
        return InventoryPhoto::where('item_inventario', $p_InventoryType)
                             ->where('item_inventario_id', $p_Id)
                             ->get();
    }
}