<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    public $timestamps = false;
    protected $table = 'region';

    public static $m_Rules = array
    (
        'nome' => 'required|min:1|max:200'
    );

    public static $m_RulesEdit = array
    (
        'id' => 'required|numeric|min:1',
        'nome' => 'required|min:1|max:200'
    );

    public static function post($p_Id, $p_Name)
    {
        $v_Region = Region::findOrNew($p_Id);
        $v_Region->name = $p_Name;
        $v_Region->save();
    }

    public static function getList()
    {
        return Region::orderBy('name')->lists('name', 'id')->toArray();
    }
}