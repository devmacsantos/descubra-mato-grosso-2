<?php

namespace App\Console\Commands;

use App\UserTradeItem;
use Illuminate\Console\Command;

class CheckRegisterExpiration extends Command
{
    protected $name = 'checkRegisterExpiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica validade dos cadastros do Trade';

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        UserTradeItem::sendExpiringTradeNotifications();
        UserTradeItem::processExpiredTrade();
    }
}
