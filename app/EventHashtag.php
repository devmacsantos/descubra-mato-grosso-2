<?php namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EventHashtag extends Model
{
    public $timestamps = false;
    protected $table = 'event_hashtag';

    public static function deleteEventHashtags($p_EventId)
    {
        EventHashtag::where('event_id', $p_EventId)->delete();
    }

    public static function addEventHashtag($p_EventId, $p_HashtagId)
    {
        $v_EventHashtag = new EventHashtag();
        $v_EventHashtag->event_id = $p_EventId;
        $v_EventHashtag->hashtag_id = $p_HashtagId;
        $v_EventHashtag->save();
    }

    public static function getList($p_EventId)
    {
        return EventHashtag::join('hashtag', 'hashtag.id', '=', 'event_hashtag.hashtag_id')
            ->where('event_id', $p_EventId)->lists('hashtag.name', 'hashtag.name')->toArray();
    }

    public static function getHashtagEvents($p_HashtagIds, $p_Quantity)
    {
        $v_Query = EventHashtag::join('event', 'event.id', '=', 'event_hashtag.event_id')
                           ->leftJoin(DB::raw('(SELECT event_id, is_cover, url FROM `event_photo` WHERE is_cover = 1) photo'), function($join) {
                               $join->on('event.id', '=', 'photo.event_id');
                           })
                           ->join('event_date', 'event_date.event_id', '=', 'event.id')
                           ->whereIn('event_hashtag.hashtag_id', $p_HashtagIds)
                           ->where('event.revision_status_id', 4)
//                           ->where('event_date.date', '>=', Carbon::now()->startOfDay()->format('Y-m-d H:i:s'))
                           ->select(['event.id', 'event.nome', 'event.city_id','event.descricao_curta', 'event.slug', 'photo.url']);
        if(!empty($p_HashtagIds)){
            $v_Query->orderByRaw('FIELD(event_hashtag.hashtag_id, ' . implode(',', $p_HashtagIds) . ')');
        }
        return $v_Query->orderBy('event.destaque', 'DESC')
                           ->orderBy('event_date.date')
                           ->orderBy('photo.is_cover','DESC')
                           ->get()
                           ->unique('id')
                           ->take($p_Quantity);
    }
}
