<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Attraction;
use App\AttractionHashtag;
use App\AttractionPhoto;
use App\Revision;
use App\AccomodationService;
use App\FoodDrinksService;
use App\TourismAgencyService;
use App\TourismTransportationService;
use App\EventService;
use App\RecreationService;
use App\OtherTourismService;

class ReviewAttractions extends Job implements SelfHandling, ShouldQueue {
    public $timeout = 86400;
    use InteractsWithQueue, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Attraction $attraction, AttractionPhoto $attractionPhoto, AttractionHashtag $attractionHashtag, AccomodationService $accomodationService, FoodDrinksService $foodDrinksService, TourismAgencyService $tourismAgencyService, TourismTransportationService $tourismTransportationService, EventService $eventService, RecreationService $recreationService, OtherTourismService $otherTourismService) {
        
        //Solicita Registros na tabela attraction que corespondam aos filtros na
        //função filterRequiredCadastur
        $attraction_rows = $attraction->filterRequiredCadastur();

        //$stat =0;
        //Passa pelos registros
        foreach ($attraction_rows as $attraction_row) {
            //Se o registro tem origem em B1 realiza verificação do registro pai
            //na tabela acomodações...
            if ($attraction_row->item_inventario == 'B1'){
              $return = $accomodationService->find($attraction_row->item_inventario_id);  
            };
           /* if ($attraction_row->item_inventario == 'B2'){
              $return = $foodDrinksService->find($attraction_row->item_inventario_id);  
            };*/
            if ($attraction_row->item_inventario == 'B3'){
               $return = $tourismAgencyService->find($attraction_row->item_inventario_id); 
            };
            if ($attraction_row->item_inventario == 'B4'){
                $return = $tourismTransportationService->find($attraction_row->item_inventario_id);
            };
            /*if ($attraction_row->item_inventario == 'B5'){
                $return = $eventService->find($attraction_row->item_inventario_id);
            };
            if ($attraction_row->item_inventario == 'B6'){
                $return = $recreationService->find($attraction_row->item_inventario_id);
            };
            if ($attraction_row->item_inventario == 'B7'){
                $return = $otherTourismService->find($attraction_row->item_inventario_id);
            };*/
            //Efetua comparações de dados entre tabelas
            if (!$return || isset($return->tem_cadastur) && ($return->tem_cadastur == 0) ){
                echo "<pre> \n";
                  var_dump(['item_inventario'=>$attraction_row->item_inventario, 'item_inventario_id'=>$attraction_row->item_inventario_id, 'nome'=>$attraction_row->nome]);
                
                if (isset($return)) 
                {
                  var_dump(['id'=>$return->id,'nome_fantasia'=>$return->nome_fantasia, 'tipo_atividade_cadastur'=>$return->tipo_atividade_cadastur, 'cnpj'=>$return->cnpj, 'registro_cadastur'=>$return->registro_cadastur]);
                } else {
                    echo "Não existe registro pai.";
                }
                echo "</pre> \n";

                //--        
                //foreach ($attractionPhoto->where('attraction_id',$attraction_row->item_inventario_id)->get() as $attractionPhoto_r){
                  $attractionPhoto->where('attraction_id',$attraction_row->item_inventario_id)->delete();  
                //}
                //foreach ($attractionHashtag->where('attraction_id',$attraction_row->item_inventario_id)->get() as $attractionHashtag_r){
                  $attractionHashtag->where('attraction_id',$attraction_row->item_inventario_id)->delete();  
                //};
                $attraction_row->delete();                
                
                unset($return);

                
                //$stat +=1;
       
            }
     
        }
       //echo "$stat"; 
        
    }

}
