<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Parameter;
use App\Revision;
use App\AccomodationService;
use App\FoodDrinksService;
use App\TourismAgencyService;
use App\TourismTransportationService;
use App\EventService;
use App\RecreationService;
use App\OtherTourismService;

use App\Jobs\ReviewAttractions;

class ReviewCadastur extends Job implements SelfHandling, ShouldQueue
{
    
    public $timeout = 86400;
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function AtualizarCadastur($tableData, $tableReference) {

        //Instacia Models de Parâmetros e revisão
        $parameter = new Parameter;
        $revision = new Revision;

        //Variavel de estatísticas
        $statisticas = array(
            'total_geral' => 0,
            'total_pesquisa' => 0,
            'total_update' => 0,
            'table' => '',
            'time' => date('d-m-y H:i:s')
        );

        //Total de Registros
        $statisticas['total_geral'] = count($tableData);
        $statisticas['table'] = $tableReference;

        //URL e senha para consulta cadastur
        $url = $parameter->where('key', 'cadastur_url')->get()[0]->value;
        $passwd = $parameter->where('key', 'cadastur_senha')->get()[0]->value;
        //Verifica se a url foi salva com /
        if (!(substr(trim($url), -1) == '/')) {
            $url = trim($url) . '/';
        }

        //Data para comparação -26 hrs
        $date = new \DateTime('-26 hour');
        //Formato de Data para função DateTime::createFromFormat 
        $format = "d/m/Y";

        foreach ($tableData as $tableRow) {

            //--Pesquisa se foi atualizado registro cadastur nas útimas 26 hras
            $arrayFilterRevision = array(
                'revisionable_type' => $tableReference,
                'revisionable_id' => $tableRow->id,
                'key' => 'registro_cadastur',
                'created_at' => $date);

            $revisions = $revision->filterAtualizaCadastur($arrayFilterRevision);
            //--
            //--Pega Código de Atividade CADASTUR
            if (isset($tableRow->sub_type_id)) {
                $codCadastur = getTypeCadastur($tableRow->type_id, $tableRow->sub_type_id);
            } else {
                $codCadastur = getTypeCadastur($tableRow->type_id);
            }
            //--
            //Usa as informações coletadas anteriormente para identificar se o 
            //registro é atualizável.
            //Verifica se não foi retornada registro de atualização nas útimas 26hrs
            //Se foi retornado um código de atividade CADASTUR
            //Se o registro possui CNPJ
            //if (count($revisions) == 0 && $codCadastur > 0 && isset($tableRow->cnpj)) {
            if ($codCadastur > 0 && isset($tableRow->cnpj)) {
                //-- Efetua pesquisa pelo CNPJ no site CADASTUR
                $newurl = trim($url) . trim($passwd) . "/cnpj/" . trim($tableRow->cnpj) . "/tipoAtividade/" . trim($codCadastur);
                $return_cadastur = json_decode(get_file($newurl));
                //--Soma estatítica
                $statisticas['total_pesquisa'] += 1;

                //Verifica se foi retornado mensagem Prestador nao encontrado na pesquisa do CADASTUR
                //
                if (isset($return_cadastur->cod) && ($return_cadastur->msg == "Prestador nao encontrado")) {
                    //Atualiza Registro removendo dados do CADASTUR
                    $update['id'] = $tableRow->id;
                    $update['registro_cadastur'] = NULL;
                    $update['validade_cadastur'] = NULL;
                    $update['tipo_atividade_cadastur'] = $codCadastur;
                    //isset funciona pelo fato de o valor padrão ser definido como 0 e não como null
                    if (isset($tableRow->tem_cadastur)) {
                        $update['tem_cadastur'] = 0;
                    }
                } else {
                    //Atualiza Registro removendo dados do CADASTUR
                    //$interval = \DateTime::createFromFormat($format, $return_cadastur->validadeCertificado)->diff(new \DateTime($tableRow->validade_cadastur));
                    $interval = \DateTime::createFromFormat($format, $return_cadastur->validadeCertificado)->diff(new \DateTime('NOW'));

                    $update['id'] = $tableRow->id;
                    $update['registro_cadastur'] = $return_cadastur->nuAtividadePj;

                    //Verifica se é necessário atualizar data de validade CADASTUR                    
                    if (!isset($tableRow->validade_cadastur) || \DateTime::createFromFormat($format, $return_cadastur->validadeCertificado)->diff(new \DateTime($tableRow->validade_cadastur))->days != 0) {
                        $update['validade_cadastur'] = explode("/", $return_cadastur->validadeCertificado)[2] . "/" . explode("/", $return_cadastur->validadeCertificado)[1] . "/" . explode("/", $return_cadastur->validadeCertificado)[0];
                    } else {
                        $update['validade_cadastur'] = $tableRow->validade_cadastur;
                    }
                    $update['tipo_atividade_cadastur'] = $codCadastur;
                    
                    if (isset($tableRow->tem_cadastur)) {
                        //No caso $interval->invert retorna 1 se a tada do cadastur for maior que a data atual
                        $update['tem_cadastur'] = $interval->invert;
                        //if ($interval->invert == 0){
                        //   dd($interval, $return_cadastur); 
                        //}
                    }
                }

                if (($update['registro_cadastur'] <> $tableRow->registro_cadastur) ||
                        ($update['validade_cadastur'] <> $tableRow->validade_cadastur) ||
                        ($update['tipo_atividade_cadastur'] <> $tableRow->tipo_atividade_cadastur) ||
                        (isset($tableRow->tem_cadastur) && ($update['tem_cadastur'] <> $tableRow->tem_cadastur))) {

                    //Soma estatística
                    $statisticas['total_update'] += 1;
                    //Realiza Update
                    $tableRow->update($update);
                }
            }
        }
        
        $date = new \DateTime();
        $result = $date->format('Y-m-d');
        
        file_put_contents(storage_path()."/AtualizarCadastur_$result", print_r($statisticas, true)."\n", FILE_APPEND);

    }    
    
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(AccomodationService $accomodationservice, FoodDrinksService $fooddrinksservice, TourismAgencyService $tourismagencyservice, TourismTransportationService $tourismtransportationservice, EventService $eventservice, RecreationService $recreationservice, OtherTourismService $othertourismservice)
    {
        //Cria log
        $date = new \DateTime();
        $result = $date->format('Y-m-d');
        
        file_put_contents(storage_path()."/AtualizarCadastur_$result", $date->format('Y-m-d H:i:s')." /n", FILE_APPEND);
        
        $credentials = array(
            'email' => env('USER_SYSTEM', ''),
            'password' => env('USER_SYSTEM_PASSWORD', ''),
        );

        if (\Auth::attempt($credentials)) {
            // Authentication passed...
            //return redirect()->intended('dashboard');

            /* Reconfigura a quantidade de Memória para Alocação do PHP de forma temporária */
            ini_set('memory_limit', '2048M');

            //Pega todas as acomodações    
            $accomodations = $accomodationservice->where('cnpj', '<>', '')->get();
            $this->AtualizarCadastur($accomodations, 'AccomodationService');

            $fooddrinks = $fooddrinksservice->where('cnpj', '<>', '')->get();
            $this->AtualizarCadastur($fooddrinks, 'FoodDrinksService');

            $tourismagency = $tourismagencyservice->where('cnpj', '<>', '')->get();
            $this->AtualizarCadastur($tourismagency, 'TourismAgencyService');

            $tourismtransportation = $tourismtransportationservice->where('cnpj', '<>', '')->get();
            $this->AtualizarCadastur($tourismtransportation, 'TourismTransportationService');

            $event = $eventservice->where('cnpj', '<>', '')->get();
            $this->AtualizarCadastur($event, 'EventService');

            $recreation = $recreationservice->where('cnpj', '<>', '')->get();
            $this->AtualizarCadastur($recreation, 'RecreationService');

            $othertourism = $fooddrinksservice->where('cnpj', '<>', '')->get();
            $this->AtualizarCadastur($othertourism, 'OtherTourismService');
            
        file_put_contents(storage_path()."/AtualizarCadastur_$result", $date->format('Y-m-d H:i:s')." /n", FILE_APPEND);
        } else {
            return "User/Error: Usuário ou senha invalidos.";
        }         
    }
}
