<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;
    protected $table = 'country';
    protected $guarded = [];

    public static function post($p_Id, $p_Data)
    {
        Country::updateOrCreate(['id' => $p_Id], $p_Data['country']);
    }

    public static function getList()
    {
        return country::orderBy('name')->lists('name', 'id')->toArray();
    }
}
