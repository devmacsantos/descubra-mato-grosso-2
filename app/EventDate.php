<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class EventDate extends Model
{
    public $timestamps = false;
    protected $table = 'event_date';

    public static function updateEventDate($p_EventId, $p_Dates)
    {
        EventDate::where('event_id', $p_EventId)->delete();
        foreach($p_Dates as $c_Date)
        {
            if($c_Date != '')
            {
                $v_EventDate = new EventDate();
                $v_EventDate->event_id = $p_EventId;
                $v_EventDate->date = Carbon::createFromFormat('d/m/Y', $c_Date)->format('Y-m-d H:i:s');
                $v_EventDate->save();
            }
        }
    }

    public static function getEventDates($p_EventId)
    {
        return EventDate::where('event_id', $p_EventId)->select(['date'])
                        ->orderBy('date')->get();
    }
}