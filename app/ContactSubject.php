<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class ContactSubject extends Model
{
    public $timestamps = false;
    protected $table = 'contact_subject';
    protected $guarded = [];

    public static function getList($p_Language)
    {
        $v_List = ContactSubject::lists('nome_' . $p_Language, 'id')->toArray();
        return ['' => trans('string.subject')] + $v_List;
    }

    public static function post($p_Id, $p_Data)
    {
        ContactSubject::updateOrCreate(['id' => $p_Id], $p_Data['idioma']);
    }

    public static function toggleActive($p_Id)
    {
        $v_ContactSubject = ContactSubject::find($p_Id);
        if ($v_ContactSubject == null)
            return redirect()->back()->with('error_message', 'Esse assunto não existe.');
        $v_ContactSubject->active = !$v_ContactSubject->active;
        $v_ContactSubject->save();
        return redirect()->back()->with('message', 'Assunto alterado com sucesso!');
    }
}