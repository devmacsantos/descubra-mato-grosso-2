<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::pattern('id', '\d+');
Route::pattern('p_Slug', '[a-z0-9-]+');
Route::pattern('p_CategorySlug', '[a-z0-9-]+');
Route::pattern('p_Lang', '(pt|en|es|fr)');

Route::group(['prefix' => 'image'], function ($p) {
    Route::get('/{w}/{h}/{fixed}/{altRatio}/{any}',function ($w, $h, $fixed, $altRatio, $any) {

        $posUlr = strpos($_SERVER['REQUEST_URI'], $any);
        $url = substr($_SERVER['REQUEST_URI'], $posUlr);
		$ext = pathinfo($any, PATHINFO_EXTENSION);

		$hostImage = parse_url($url, PHP_URL_HOST);

        $cached = true;

		//Define tipo de configuração para redimencionar imagem
		if ($fixed == $altRatio){
			if ($altRatio == 'true'){
				$paramResize = PROPORTIONAL_RESIZE_MAX_CUT;
			} else {
				$paramResize = PROPORTIONAL_RESIZE;
			}
		} else {
			if ($altRatio == 'true'){
				$paramResize = PROPORTIONAL_RESIZE_MAX;
			} else {
				$paramResize = FIXED_SIZE_WITH_BORDERS;
			}
		}
        //dd($url);
		//Redimenciona Imagem
		resizeImage_(url($url), $ext, $ext, $w, $h, $paramResize, $cached);

     })->where('any', '.*');
});



Route::get('/', function () {
    return redirect(url('/pt'));
});


Route::group(['prefix' => '/{p_Lang}/carnaval-2019'], function () {
    Route::get('/', 'CarnavalController@getPublicEvents');
    Route::any('/filtro', 'CarnavalController@getPublicEventsFilter');
    Route::get('/{d_Slug}/{p_Slug}', 'CarnavalController@getPublicEvent');
});

Route::group(['prefix' => '/{p_Lang}/semana-santa'], function () {
    Route::get('/', 'SemanasantaController@getPublicEvents');
    Route::any('/filtro', 'SemanasantaController@getPublicEventsFilter');
    Route::get('/{d_Slug}/{p_Slug}', 'SemanasantaController@getPublicEvent');
});

Route::get('/atualizar-cadastur', 'AtualizarCadasturController@index');
//Route::get('/corrige-atracao', 'AtualizarCadasturController@index');

//Route::get('/{p_Lang}/home', 'PublicController@getHome_');

Route::group(['prefix' => '/{p_Lang}/eventos'], function () {
    Route::get('/', 'EventController@getPublicEvents');
    Route::any('/filtro', 'EventController@getPublicEventsFilter');
    Route::get('/{d_Slug}', 'EventController@getPublicEventsCity');
    Route::get('/{d_Slug}/{p_Slug}', 'EventController@getPublicEvent');
});

Route::get('/{p_Lang}', 'PublicController@getHome');
Route::get('/{p_Lang}/doacao-de-midias', 'PublicController@getDonateMedia');
Route::post('/{p_Lang}/doacao-de-midias', 'PublicController@postDonatedMedia');

Route::get('/{p_Lang}/apoio-destino/{p_Slug}', 'PublicController@getDestinationSupport');
Route::get('/{p_Lang}/apoio/{c_slug}/{trade}/{p_Slug}', 'AttractionController@byTrade');
Route::get('/{p_Lang}/apoio/{c_slug}/{p_Slug}', 'AttractionController@getTrade');
Route::get('/{p_Lang}/apoio/{slug}', 'AttractionController@discoverTrade');
Route::get('/{p_Lang}/conheca', 'PublicController@getInstitutional');
Route::get('/{p_Lang}/conheca/galeria', 'PublicController@getPhotoGallery');
Route::get('/{p_Lang}/conheca/galeria/{p_Slug}', 'PublicController@getDestinationPhotoGallery');
//Route::get('/{p_Lang}/conheca/galeria-videos', 'PublicController@getVideoGallery');

Route::get('/{p_Lang}/o-que-fazer', 'PublicController@getWhatToDo');
Route::get('/{p_Lang}/o-que-fazer/{p_Slug}', 'PublicController@getTripCategory');
Route::get('/{p_Lang}/o-que-fazer/{p_CategorySlug}/{p_Slug}', 'PublicController@getTripType');

Route::get('/{p_Lang}/para-onde-ir', 'PublicController@getWhereToGo');
Route::get('/{p_Lang}/filtro-para-onde-ir', 'PublicController@getWhereToGoFilter');
Route::get('/{p_Lang}/roteiros', 'AttractionController@getPublicTouristicRoutes');
Route::get('/{p_Lang}/filtro-roteiros', 'AttractionController@getPublicTouristicRoutesByTripType');
Route::get('/{p_Lang}/roteiros-duracao', 'AttractionController@getPublicTouristicRoutesDurations');
Route::get('/{p_Lang}/roteiros/{p_Slug}', 'AttractionController@getPublicTouristicRoute');
Route::get('/{p_Lang}/destinos', 'PublicController@getDestinations');
Route::get('/{p_Lang}/filtro-destinos', 'PublicController@getDestinationsFilter');
Route::get('/{p_Lang}/destinos/{p_Slug}', 'PublicController@getDestination');
Route::get('/{p_Lang}/parques', 'AttractionController@getParks');
Route::get('/{p_Lang}/filtro-parques', 'AttractionController@getPublicParksByTripType');

Route::get('/{p_Lang}/atracoes/{c_slug}/{type}/{p_Slug}', 'AttractionController@byAttractionType');
Route::get('/{p_Lang}/atracoes/{c_slug}/{p_Slug}', 'AttractionController@getPublicAttraction');
Route::get('/{p_Lang}/atracoes/{slug}', 'AttractionController@discoverAttraction');
Route::get('/{p_Lang}/busca/{p_Type?}', 'PublicController@getSearch');
Route::get('/{p_Lang}/todas-atracoes/{p_Slug}', 'PublicController@getAllAttractions');
/*
Route::get('/{p_Lang}/eventos', 'EventController@getPublicEvents');
Route::get('/{p_Lang}/filtro-eventos', 'EventController@getPublicEventsFilter');
Route::get('/{p_Lang}/eventos/{p_Slug}', 'EventController@getPublicEvent');
 */
Route::get('/{p_Lang}/blog', 'BlogController@getBlog');
Route::get('/{p_Lang}/blog/busca', 'BlogController@getBlogSearch');
Route::get('/{p_Lang}/blog/artigo/{p_Slug}', 'BlogController@getArticle');

Route::get('/{p_Lang}/explore-o-mapa', 'PublicController@getExploreMap');
Route::get('/{p_Lang}/planeje-sua-viagem', 'PublicController@getPlanYourTrip');

Route::get('/{p_Lang}/favoritos', function ($p_Lang) {
    \App::setLocale($p_Lang);
    return view('public.favorites')->with(['p_Language' => $p_Lang]);
});

Route::get('/{p_Lang}/fale-conosco', 'PublicController@getContact');
Route::get('/{p_Lang}/fale-conosco-cidades/{p_State}', 'PublicController@getContactCities');
Route::post('/{p_Lang}/fale-conosco', 'PublicController@postContact');

Route::post('/{p_Lang}/newsletter', 'PublicController@subscribeToNewsletter');
Route::get('/{p_Lang}/newsletter/cancelar-assinatura/{p_Email}', 'PublicController@unsubscribeToNewsletter');

Route::get('/admin/relatorios/calendario-eventos/footer', 'ReportsController@getEventCalendarFooter');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', function () {
        if (Auth::check()) {
            //Verifica se o usuário é Admin/Master/Comunicação
            return redirect((Auth::user()->user_type_id == 1 || Auth::user()->user_type_id == 2 || Auth::user()->user_type_id == 3) ? url('/admin/destinos') : url('/admin/painel-geral'));
        } else {
            //Usuário não logado direcionado para Login
            return redirect(url('/admin/login'));
        }

    });
    Route::get('/login', function () {
        return view('admin.login');
    });
    Route::post('/login', 'UserController@postLogin');
    Route::get('/resetar-senha', 'UserController@getResetPassword');
    Route::post('/resetar-senha', 'UserController@postResetPassword');

    Route::get('/trade/cadastrar', function () {
        return view('admin.trade.register');
    });
    Route::post('/trade/cadastrar', 'UserController@postRegisterTradeUser');

    Route::group(['middleware' => 'auth'], function () {
        Route::get('/painel-geral', 'UserController@getDashboard');
        Route::get('/inventario/pesquisar', 'MainController@getInventorySearch');

        Route::get('distritosDisponiveisMunicipio', 'CityController@getAvailableCityDistricts');
        Route::get('circuitoTuristicoMunicipio', 'CityController@getCityTouristicCircuit');
        Route::get('regiaoMunicipio', 'CityController@getCityRegion');

        /*
        |--------------------------------------------------------------------------
        | Users
        |--------------------------------------------------------------------------
         */

        Route::get('/editar-perfil', 'UserController@editUserProfile');
        Route::post('/editar-perfil', 'UserController@postUserProfile');

        /*
        |--------------------------------------------------------------------------
        | Attraction
        |--------------------------------------------------------------------------
         */

        Route::get('dt/cidades/{p_CityId}/atracoes/{p_InventoryItem}', 'AttractionController@getDTAttractions');
        Route::get('cidades/{p_CityId}/atracoes/{p_InventoryItem}/editar/{p_Id?}', 'AttractionController@editAttraction');
        Route::post('cidades/{p_CityId}/atracoes/{p_InventoryItem}', 'AttractionController@postAttraction');

        Route::get('cidade/circuitos/{p_CityId}', 'CircuitController@getCityCircuits');
        Route::get('cidade/espacos-evento/{p_CityId}', 'EventController@getEventPlacesByCity');
        Route::get('circuitos/cidades/{p_Id?}', 'CircuitController@getCircuitCities');
        Route::get('distritosMunicipio', 'CityController@getCityDistricts');
        Route::get('distritosMunicipios', 'CityController@getCitiesDistricts');
        Route::get('subtipos', 'MainController@getInventorySubtypes');

        Route::group(['middleware' => 'adminMasterMunicipioCircuito'], function () {
            Route::get('equipes-responsaveis', 'MainController@getResponsibleTeam');
            Route::get('dt/equipes-responsaveis', 'MainController@getDTResponsibleTeam');
            Route::get('equipes-responsaveis/editar/{p_Id?}', 'MainController@editResponsibleTeam');
            Route::post('equipes-responsaveis', 'MainController@postResponsibleTeam');
            Route::get('equipes-responsaveis/excluir/{p_Id}', 'MainController@deleteResponsibleTeam');
        });

        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiro'], function () {
            /*
            |--------------------------------------------------------------------------
            | Module A
            |--------------------------------------------------------------------------
             */

            Route::get('inventario/informacoes', 'ModuleAController@getCityInfo');
            Route::get('dt/inventario/informacoes', 'ModuleAController@getDTCityInfo');
            Route::get('inventario/informacoes/editar/{p_Id?}', 'ModuleAController@editCityInfo');
            Route::get('inventario/informacoes/historico/{p_Id}', 'ModuleAController@getCityInfoHistory');

            Route::get('inventario/meios-acesso/geral', 'ModuleAController@getCityAccessGeneral');
            Route::get('dt/inventario/meios-acesso/geral', 'ModuleAController@getDTCityAccessGeneral');
            Route::get('inventario/meios-acesso/geral/editar/{p_Id?}', 'ModuleAController@editCityAccessGeneral');
            Route::get('inventario/meios-acesso/geral/historico/{p_Id}', 'ModuleAController@getCityAccessGeneralHistory');

            Route::get('inventario/meios-acesso', 'ModuleAController@getCityAccess');
            Route::get('dt/inventario/meios-acesso', 'ModuleAController@getDTCityAccess');
            Route::get('inventario/meios-acesso/editar/{p_Id?}', 'ModuleAController@editCityAccess');
            Route::get('inventario/meios-acesso/historico/{p_Id}', 'ModuleAController@getCityAccessHistory');

            Route::get('inventario/sistemas-seguranca', 'ModuleAController@getCitySecuritySystems');
            Route::get('dt/inventario/sistemas-seguranca', 'ModuleAController@getDTCitySecuritySystems');
            Route::get('inventario/sistemas-seguranca/editar/{p_Id?}', 'ModuleAController@editCitySecuritySystem');
            Route::get('inventario/sistemas-seguranca/historico/{p_Id}', 'ModuleAController@getCitySecuritySystemHistory');

            Route::get('inventario/sistemas-hospitalares', 'ModuleAController@getCityHospitalSystems');
            Route::get('dt/inventario/sistemas-hospitalares', 'ModuleAController@getDTCityHospitalSystems');
            Route::get('inventario/sistemas-hospitalares/editar/{p_Id?}', 'ModuleAController@editCityHospitalSystem');
            Route::get('inventario/sistemas-hospitalares/historico/{p_Id}', 'ModuleAController@getCityHospitalSystemHistory');

            Route::get('inventario/outros-servicos', 'ModuleAController@getCityOtherServices');
            Route::get('dt/inventario/outros-servicos', 'ModuleAController@getDTCityOtherServices');
            Route::get('inventario/outros-servicos/editar/{p_Id?}', 'ModuleAController@editCityOtherService');
            Route::get('inventario/outros-servicos/historico/{p_Id}', 'ModuleAController@getCityOtherServiceHistory');

            /*
            |--------------------------------------------------------------------------
            | Reports
            |--------------------------------------------------------------------------
             */

            Route::get('/relatorios/circuitos', 'ReportsController@getTouristicCircuitsReport');
            Route::get('/relatorios/circuitos/gerar', 'ReportsController@generateTouristicCircuitsReport');
            Route::get('/relatorios/circuitos-resumo/gerar', 'ReportsController@generateTouristicCircuitsSimplifiedReport');
            Route::get('/relatorios/circuitos-contatos/gerar', 'ReportsController@generateTouristicCircuitsContactsReport');
            Route::get('/relatorios/destinos', 'ReportsController@getDestinationsReport');
            Route::get('/relatorios/destinos/gerar', 'ReportsController@generateDestinationsReport');
            Route::get('/relatorios/eventos', 'ReportsController@getEventsReport');
            Route::get('/relatorios/eventos/gerar', 'ReportsController@generateEventsReport');
            Route::get('/relatorios/calendario-eventos', 'ReportsController@getCalendarEventsReport');
            Route::get('/relatorios/calendario-eventos/gerar', 'ReportsController@generateCalendarEventsReport');
            Route::get('/relatorios/eventos-permanentes', 'ReportsController@getPermanentEvents');
            Route::get('/relatorios/eventos-permanentes/gerar', 'ReportsController@generatePermanentEventsReport');

            Route::get('/relatorios/inventario/mapa-numerico', 'ReportsController@getInventoryReport');
            Route::get('/relatorios/inventario/mapa-numerico/gerar', 'ReportsController@generateInventoryNumericMap');

            Route::get('/relatorios/inventario/modulo-a', 'ReportsController@getModuleAReport');
            Route::get('/relatorios/inventario/modulo-a/{p_Item}/gerar', 'ReportsController@generateModuleAReport');
            Route::get('/relatorios/inventario/modulo-b', 'ReportsController@getModuleBReport');
            Route::get('/relatorios/inventario/modulo-b/{p_Item}/gerar', 'ReportsController@generateModuleBReport');
            Route::get('/relatorios/inventario/modulo-c', 'ReportsController@getModuleCReport');
            Route::get('/relatorios/inventario/modulo-c/{p_Item}/gerar', 'ReportsController@generateModuleCReport');
        });
        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiroTrade'], function () {
            /*
            |--------------------------------------------------------------------------
            | Module B
            |--------------------------------------------------------------------------
             */

            Route::get('inventario/servicos-hospedagem', 'ModuleBController@getAccomodationServices');
            Route::get('dt/inventario/servicos-hospedagem', 'ModuleBController@getDTAccomodationServices');
            Route::get('inventario/servicos-hospedagem/editar/{p_Id?}', 'ModuleBController@editAccomodationService');
            Route::get('inventario/servicos-hospedagem/historico/{p_Id}', 'ModuleBController@getAccomodationServiceHistory');

            Route::get('inventario/servicos-alimentos', 'ModuleBController@getFoodDrinksServices');
            Route::get('dt/inventario/servicos-alimentos', 'ModuleBController@getDTFoodDrinksServices');
            Route::get('inventario/servicos-alimentos/editar/{p_Id?}', 'ModuleBController@editFoodDrinksService');
            Route::get('inventario/servicos-alimentos/historico/{p_Id}', 'ModuleBController@getFoodDrinksServiceHistory');

            Route::get('inventario/servicos-agencias-turismo', 'ModuleBController@getTourismAgencyServices');
            Route::get('dt/inventario/servicos-agencias-turismo', 'ModuleBController@getDTTourismAgencyServices');
            Route::get('inventario/servicos-agencias-turismo/editar/{p_Id?}', 'ModuleBController@editTourismAgencyService');
            Route::get('inventario/servicos-agencias-turismo/historico/{p_Id}', 'ModuleBController@getTourismAgencyServiceHistory');

            Route::get('inventario/servicos-transporte-turistico', 'ModuleBController@getTourismTransportationServices');
            Route::get('dt/inventario/servicos-transporte-turistico', 'ModuleBController@getDTTourismTransportationServices');
            Route::get('inventario/servicos-transporte-turistico/editar/{p_Id?}', 'ModuleBController@editTourismTransportationService');
            Route::get('inventario/servicos-transporte-turistico/historico/{p_Id}', 'ModuleBController@getTourismTransportationServiceHistory');

            Route::get('inventario/servicos-lazer', 'ModuleBController@getRecreationServices');
            Route::get('dt/inventario/servicos-lazer', 'ModuleBController@getDTRecreationServices');
            Route::get('inventario/servicos-lazer/editar/{p_Id?}', 'ModuleBController@editRecreationService');
            Route::get('inventario/servicos-lazer/historico/{p_Id}', 'ModuleBController@getRecreationServiceHistory');

            Route::get('consulta-cnpj', 'ModuleBController@getCNPJAvailability');
            Route::post('solicitar-posse-cnpj', 'ModuleBController@requestCNPJOwnership');

            /*
            |--------------------------------------------------------------------------
            | Reports
            |--------------------------------------------------------------------------
             */

            Route::get('/relatorios/inventario/planejamento-georreferenciado', 'ReportsController@getInventoryGeoPlanningReport');
            Route::post('/relatorios/inventario/planejamento-georreferenciado', 'ReportsController@postInventoryGeoPlanningReport');
        });

        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiro'], function () {
            Route::get('inventario/servicos-eventos', 'ModuleBController@getEventServices');
            Route::get('dt/inventario/servicos-eventos', 'ModuleBController@getDTEventServices');
            Route::get('inventario/servicos-eventos/editar/{p_Id?}', 'ModuleBController@editEventService');
            Route::get('inventario/servicos-eventos/historico/{p_Id}', 'ModuleBController@getEventServiceHistory');

            Route::get('inventario/outros-servicos-turisticos', 'ModuleBController@getOtherTourismServices');
            Route::get('dt/inventario/outros-servicos-turisticos', 'ModuleBController@getDTOtherTourismServices');
            Route::get('inventario/outros-servicos-turisticos/editar/{p_Id?}', 'ModuleBController@editOtherTourismService');
            Route::get('inventario/outros-servicos-turisticos/historico/{p_Id}', 'ModuleBController@getOtherTourismServiceHistory');

            /*
            |--------------------------------------------------------------------------
            | Module C
            |--------------------------------------------------------------------------
             */

            Route::get('inventario/atrativos-naturais', 'ModuleCController@getNaturalAttractions');
            Route::get('dt/inventario/atrativos-naturais', 'ModuleCController@getDTNaturalAttractions');
            Route::get('inventario/atrativos-naturais/editar/{p_Id?}', 'ModuleCController@editNaturalAttraction');
            Route::get('inventario/atrativos-naturais/historico/{p_Id}', 'ModuleCController@getNaturalAttractionHistory');

            Route::get('inventario/atrativos-culturais', 'ModuleCController@getCulturalAttractions');
            Route::get('dt/inventario/atrativos-culturais', 'ModuleCController@getDTCulturalAttractions');
            Route::get('inventario/atrativos-culturais/editar/{p_Id?}', 'ModuleCController@editCulturalAttraction');
            Route::get('inventario/atrativos-culturais/historico/{p_Id}', 'ModuleCController@getCulturalAttractionHistory');

            Route::get('inventario/atrativos-atividades-economicas', 'ModuleCController@getEconomicActivityAttractions');
            Route::get('dt/inventario/atrativos-atividades-economicas', 'ModuleCController@getDTEconomicActivityAttractions');
            Route::get('inventario/atrativos-atividades-economicas/editar/{p_Id?}', 'ModuleCController@editEconomicActivityAttraction');
            Route::get('inventario/atrativos-atividades-economicas/historico/{p_Id}', 'ModuleCController@getEconomicActivityAttractionHistory');

            Route::get('inventario/realizacoes-contemporaneas', 'ModuleCController@getContemporaryRealizations');
            Route::get('dt/inventario/realizacoes-contemporaneas', 'ModuleCController@getDTContemporaryRealizations');
            Route::get('inventario/realizacoes-contemporaneas/editar/{p_Id?}', 'ModuleCController@editContemporaryRealization');
            Route::get('inventario/realizacoes-contemporaneas/historico/{p_Id}', 'ModuleCController@getContemporaryRealizationHistory');

            Route::get('inventario/produto-primario', 'ModuleCController@getGastronomicPrimaryProducts');
            Route::get('dt/inventario/produto-primario', 'ModuleCController@getDTGastronomicPrimaryProducts');
            Route::get('inventario/produto-primario/editar/{p_Id?}', 'ModuleCController@editGastronomicPrimaryProduct');
            Route::get('inventario/produto-primario/historico/{p_Id}', 'ModuleCController@getGastronomicPrimaryProductHistory');

            Route::get('inventario/produto-transformado', 'ModuleCController@getGastronomicTransformedProducts');
            Route::get('dt/inventario/produto-transformado', 'ModuleCController@getDTGastronomicTransformedProducts');
            Route::get('inventario/produto-transformado/editar/{p_Id?}', 'ModuleCController@editGastronomicTransformedProduct');
            Route::get('inventario/produto-transformado/historico/{p_Id}', 'ModuleCController@getGastronomicTransformedProductHistory');

            Route::get('inventario/prato-tipico', 'ModuleCController@getGastronomicTypicalDishes');
            Route::get('dt/inventario/prato-tipico', 'ModuleCController@getDTGastronomicTypicalDishes');
            Route::get('inventario/prato-tipico/editar/{p_Id?}', 'ModuleCController@editGastronomicTypicalDish');
            Route::get('inventario/prato-tipico/historico/{p_Id}', 'ModuleCController@getGastronomicTypicalDishHistory');
        });
        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizador'], function () {
            /*
            |--------------------------------------------------------------------------
            | Module A
            |--------------------------------------------------------------------------
             */

            Route::post('inventario/informacoes', 'ModuleAController@postCityInfo');
            Route::get('inventario/informacoes/excluir/{p_Id}', 'ModuleAController@deleteCityInfo');

            Route::post('inventario/meios-acesso/geral', 'ModuleAController@postCityAccessGeneral');
            Route::get('inventario/meios-acesso/geral/excluir/{p_Id}', 'ModuleAController@deleteCityAccessGeneral');

            Route::post('inventario/meios-acesso', 'ModuleAController@postCityAccess');
            Route::get('inventario/meios-acesso/excluir/{p_Id}', 'ModuleAController@deleteCityAccess');

            Route::post('inventario/sistemas-seguranca', 'ModuleAController@postCitySecuritySystem');
            Route::get('inventario/sistemas-seguranca/excluir/{p_Id}', 'ModuleAController@deleteCitySecuritySystem');

            Route::post('inventario/sistemas-hospitalares', 'ModuleAController@postCityHospitalSystem');
            Route::get('inventario/sistemas-hospitalares/excluir/{p_Id}', 'ModuleAController@deleteCityHospitalSystem');

            Route::post('inventario/outros-servicos', 'ModuleAController@postCityOtherService');
            Route::get('inventario/outros-servicos/excluir/{p_Id}', 'ModuleAController@deleteCityOtherService');

        });
        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizadorTrade'], function () {
            /*
            |--------------------------------------------------------------------------
            | Module B
            |--------------------------------------------------------------------------
             */

            Route::post('inventario/servicos-hospedagem', 'ModuleBController@postAccomodationService');
            Route::get('inventario/servicos-hospedagem/excluir/{p_Id}', 'ModuleBController@deleteAccomodationService');

            Route::post('inventario/servicos-alimentos', 'ModuleBController@postFoodDrinksService');
            Route::get('inventario/servicos-alimentos/excluir/{p_Id}', 'ModuleBController@deleteFoodDrinksService');

            Route::post('inventario/servicos-agencias-turismo', 'ModuleBController@postTourismAgencyService');
            Route::get('inventario/servicos-agencias-turismo/excluir/{p_Id}', 'ModuleBController@deleteTourismAgencyService');

            Route::post('inventario/servicos-transporte-turistico', 'ModuleBController@postTourismTransportationService');
            Route::get('inventario/servicos-transporte-turistico/excluir/{p_Id}', 'ModuleBController@deleteTourismTransportationService');

            Route::post('inventario/servicos-lazer', 'ModuleBController@postRecreationService');
            Route::get('inventario/servicos-lazer/excluir/{p_Id}', 'ModuleBController@deleteRecreationService');
        });
        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizador'], function () {

            Route::post('inventario/servicos-eventos', 'ModuleBController@postEventService');
            Route::get('inventario/servicos-eventos/excluir/{p_Id}', 'ModuleBController@deleteEventService');

            Route::post('inventario/outros-servicos-turisticos', 'ModuleBController@postOtherTourismService');
            Route::get('inventario/outros-servicos-turisticos/excluir/{p_Id}', 'ModuleBController@deleteOtherTourismService');

            /*
            |--------------------------------------------------------------------------
            | Module C
            |--------------------------------------------------------------------------
             */

            Route::post('inventario/atrativos-naturais', 'ModuleCController@postNaturalAttraction');
            Route::get('inventario/atrativos-naturais/excluir/{p_Id}', 'ModuleCController@deleteNaturalAttraction');

            Route::post('inventario/atrativos-culturais', 'ModuleCController@postCulturalAttraction');
            Route::get('inventario/atrativos-culturais/excluir/{p_Id}', 'ModuleCController@deleteCulturalAttraction');

            Route::post('inventario/atrativos-atividades-economicas', 'ModuleCController@postEconomicActivityAttraction');
            Route::get('inventario/atrativos-atividades-economicas/excluir/{p_Id}', 'ModuleCController@deleteEconomicActivityAttraction');

            Route::post('inventario/realizacoes-contemporaneas', 'ModuleCController@postContemporaryRealization');
            Route::get('inventario/realizacoes-contemporaneas/excluir/{p_Id}', 'ModuleCController@deleteContemporaryRealization');

            Route::post('inventario/produto-primario', 'ModuleCController@postGastronomicPrimaryProduct');
            Route::get('inventario/produto-primario/excluir/{p_Id}', 'ModuleCController@deleteGastronomicPrimaryProduct');

            Route::post('inventario/produto-transformado', 'ModuleCController@postGastronomicTransformedProduct');
            Route::get('inventario/produto-transformado/excluir/{p_Id}', 'ModuleCController@deleteGastronomicTransformedProduct');

            Route::post('inventario/prato-tipico', 'ModuleCController@postGastronomicTypicalDish');
            Route::get('inventario/prato-tipico/excluir/{p_Id}', 'ModuleCController@deleteGastronomicTypicalDish');
        });

        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiroTrade'], function () {
            /*
            |--------------------------------------------------------------------------
            | Events
            |--------------------------------------------------------------------------
             */

            Route::get('eventos', 'EventController@getEvents');
            Route::get('dt/eventos', 'EventController@getDTEvents');
            Route::get('eventos/historico/{p_Id}', 'EventController@getEventInfoHistory');
            Route::get('eventos/editar/{p_Id?}/{p_Dp?}', 'EventController@editEvent');
            Route::get('eventos/city/{city}/name/{name?}', 'EventController@eventByCityAndName');
            Route::get('eventos/adonarse/{event_id}/{user_id}', 'EventController@requestEventOwnership');
        });

        Route::group(['middleware' => 'adminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiroTrade'], function () {
            /*
            |--------------------------------------------------------------------------
            | Events
            |--------------------------------------------------------------------------
             */

            Route::post('eventos', 'EventController@postEvent');
//            Route::get('eventos/desativar/{p_Id}', 'EventController@toggleActive');
            Route::get('eventos/excluir/{p_Id}', 'EventController@deleteEvent');
        });

        Route::group(['middleware' => 'adminMasterComunicacaoParceiro'], function () {
            /*
            |--------------------------------------------------------------------------
            | TouristicCircuits
            |--------------------------------------------------------------------------
             */

            Route::get('circuitos', 'CircuitController@getCircuits');
            Route::get('dt/circuitos', 'CircuitController@getDTCircuits');
            Route::get('circuitos/editar/{p_Id?}', 'CircuitController@editCircuit');

            /*
            |--------------------------------------------------------------------------
            | TouristicRoute
            |--------------------------------------------------------------------------
             */

            Route::get('roteiros', 'AttractionController@getTouristicRoutes');
            Route::get('dt/roteiros', 'AttractionController@getDTTouristicRoute');
            Route::get('roteiros/editar/{p_Id?}', 'AttractionController@editTouristicRoute');
        });

        Route::group(['middleware' => 'adminOrMasterOrComunicacao'], function () {
            /*
            |--------------------------------------------------------------------------
            | Destination
            |--------------------------------------------------------------------------
             */

            Route::get('destinos', 'DestinationController@getDestinations');
            Route::get('dt/destinos', 'DestinationController@getDTDestinations');
            Route::get('destinos/editar/{p_Type}/{p_Id?}', 'DestinationController@editDestination');
            Route::post('destinos', 'DestinationController@postDestination');
            Route::get('destinos/desativar/{p_Id}', 'DestinationController@toggleActive');

            /*
            |--------------------------------------------------------------------------
            | TouristicCircuits
            |--------------------------------------------------------------------------
             */

            Route::get('circuitos/excluir/{p_Id}', 'CircuitController@deleteCircuit');
            Route::post('circuitos', 'CircuitController@postCircuit');

            /*
            |--------------------------------------------------------------------------
            | TouristicRoute
            |--------------------------------------------------------------------------
             */

            Route::post('roteiros', 'AttractionController@postTouristicRoute');
            Route::get('roteiros/desativar/{p_Id}', 'AttractionController@toggleActiveTouristicRoute');
            Route::get('roteiros/excluir/{p_Id}', 'AttractionController@deleteTouristicRoute');

            /*
            |--------------------------------------------------------------------------
            | Blog
            |--------------------------------------------------------------------------
             */

            Route::get('blog', 'BlogController@getArticles');
            Route::get('dt/blog', 'BlogController@getDTArticles');
            Route::get('blog/editar/{p_Id?}', 'BlogController@editArticle');
            Route::post('blog', 'BlogController@postArticle');
            Route::get('blog/desativar/{p_Id}', 'BlogController@toggleActive');
            Route::get('blog/excluir/{p_Id}', 'BlogController@deleteArticle');

            Route::post('upload-arquivo', 'PagesConfigController@uploadFile');

            /*
            |--------------------------------------------------------------------------
            | Donated Media
            |--------------------------------------------------------------------------
             */

            Route::get('doacoes', 'MainController@getDonatedMedia');
            Route::get('dt/doacoes', 'MainController@getDTDonatedMedia');
            Route::get('doacoes/termo/{p_Id}', 'ReportsController@getDonatedMediaTerm');
            Route::get('doacoes/excluir/{p_Id}', 'MainController@deleteDonatedMedia');
            Route::get('/relatorios/doacoes/gerar', 'ReportsController@generateDonatedMediaReport');
        });

        Route::group(['middleware' => 'adminOrComunicacao'], function () {
            /*
            |--------------------------------------------------------------------------
            | PagesConfiguration
            |--------------------------------------------------------------------------
             */

            Route::get('paginas/home/editar', 'PagesConfigController@editHome');
            Route::post('paginas/home', 'PagesConfigController@postHome');

            Route::get('paginas/conheca/editar', 'PagesConfigController@editInstitutional');
            Route::post('paginas/conheca', 'PagesConfigController@postInstitutional');

            Route::get('paginas/galeria-videos/editar', 'PagesConfigController@editVideoGallery');
            Route::post('paginas/galeria-videos', 'PagesConfigController@postVideoGallery');

            Route::get('paginas/planeje-sua-viagem/editar', 'PagesConfigController@editPlanYourTrip');
            Route::post('paginas/planeje-sua-viagem', 'PagesConfigController@postPlanYourTrip');

            Route::get('paginas/descricoes/{p_Page}/editar', 'PagesConfigController@editPageDescriptions');
            Route::post('paginas/descricoes', 'PagesConfigController@postPageDescriptions');

            Route::get('paginas/descricao/{p_Page}/editar', 'PagesConfigController@editPageSingleDescription');
            Route::post('paginas/descricao', 'PagesConfigController@postPageSingleDescription');

            Route::get('assuntos-fale-conosco', 'PagesConfigController@getContactSubjects');
            Route::get('assuntos-fale-conosco/editar/{p_Id?}', 'PagesConfigController@editContactSubject');
            Route::post('assuntos-fale-conosco', 'PagesConfigController@postContactSubject');
        });

        Route::group(['middleware' => 'adminOrMaster'], function () {
            Route::get('tratar-solicitacao-cnpj', 'ModuleBController@treatCNPJOwnershipRequest');
        });
        Route::group(['middleware' => 'admin'], function () {
            /*
            |--------------------------------------------------------------------------
            | WhatToDo
            |--------------------------------------------------------------------------
             */

            Route::get('categorias-viagem', 'PagesConfigController@getTripCategories');
            Route::get('categorias-viagem/editar/{p_Id?}', 'PagesConfigController@editTripCategory');
            Route::post('categorias-viagem', 'PagesConfigController@postTripCategory');

            Route::get('tipos-viagem', 'PagesConfigController@getTripTypes');
            Route::get('tipos-viagem/editar/{p_Id?}', 'PagesConfigController@editTripType');
            Route::post('tipos-viagem', 'PagesConfigController@postTripType');

            /*
            |--------------------------------------------------------------------------
            | Properties
            |--------------------------------------------------------------------------
             */

            Route::get('tooltips/{p_Type}', 'MainController@editTooltips');
            Route::post('tooltips/{p_Type}', 'MainController@postTooltips');

            Route::get('cadastur/editar', 'MainController@editCadasturAuth');
            Route::post('cadastur', 'MainController@postCadasturAuth');

            Route::get('atividades-economicas', 'MainController@getEconomicActivities');
            Route::get('atividades-economicas/editar/{p_Id?}', 'MainController@editEconomicActivity');
            Route::post('atividades-economicas', 'MainController@postEconomicActivity');

            Route::get('municipios', 'CityController@getCities');
            Route::get('dt/municipios', 'CityController@getDTCities');
            Route::get('municipios/editar/{p_Id?}', 'CityController@editCity');
            Route::post('municipios', 'CityController@postCity');

            Route::get('municipios/{p_CityId}/distritos', 'CityController@getDistricts');
            Route::get('municipios/{p_CityId}/distritos/editar/{p_Id?}', 'CityController@editDistrict');
            Route::post('municipios/{p_CityId}/distritos', 'CityController@postDistrict');

            Route::get('climas', 'MainController@getClimates');
            Route::get('climas/editar/{p_Id?}', 'MainController@editClimate');
            Route::post('climas', 'MainController@postClimate');

            Route::get('operadoras-telefonicas', 'MainController@getCarriers');
            Route::get('operadoras-telefonicas/editar/{p_Id?}', 'MainController@editCarrier');
            Route::post('operadoras-telefonicas', 'MainController@postCarrier');
            Route::get('operadoras-telefonicas/excluir/{p_Id}', 'MainController@deleteCarrier');

            Route::get('servicos-telefonicos', 'MainController@getCarrierServices');
            Route::get('servicos-telefonicos/editar/{p_Id?}', 'MainController@editCarrierService');
            Route::post('servicos-telefonicos', 'MainController@postCarrierService');
            Route::get('servicos-telefonicos/excluir/{p_Id}', 'MainController@deleteCarrierService');

            Route::get('servicos-abastecimento/{p_Type}', 'MainController@getSupplyTypes');
            Route::get('servicos-abastecimento/{p_Type}/editar/{p_Id?}', 'MainController@editSupplyType');
            Route::post('servicos-abastecimento/{p_Type}', 'MainController@postSupplyType');

            Route::get('rodovias/{p_Type}', 'MainController@getRoads');
            Route::get('rodovias/{p_Type}/editar/{p_Id?}', 'MainController@editRoad');
            Route::post('rodovias/{p_Type}', 'MainController@postRoad');
            Route::get('rodovias/{p_Type}/excluir/{p_Id}', 'MainController@deleteRoad');

            Route::get('eventos/categorias', 'EventController@getEventCategories');
            Route::get('eventos/categorias/editar/{p_Id?}', 'EventController@editEventCategory');
            Route::post('eventos/categorias', 'EventController@postEventCategory');

            Route::get('eventos/classificacao', 'EventClassificationController@index');
            Route::get('eventos/classificacao/{id}', 'EventClassificationController@show');
            Route::get('eventos/classificacao/criar', 'EventClassificationController@create');
            Route::post('eventos/classificacao', 'EventClassificationController@store');
            Route::put('eventos/classificacao/{id}', 'EventClassificationController@update');

            Route::get('configurar-calendario-eventos', 'MainController@editEventCalendarConfig');
            Route::post('configurar-calendario-eventos', 'MainController@postEventCalendarConfig');

            //--*******Capas*****Desenvolvimento*****
            //
            //Route::get('capas-relatorios', 'MainController@editReportCovers');
            //Route::get('capas-relatorios/editar/{p_Id?}', 'MainController@editEconomicActivity');

            Route::get('capas-relatorios', 'ReportCoverController@index');
            Route::get('capas-relatorios/{id}', 'ReportCoverController@show');
            Route::get('capas-relatorios/criar', 'ReportCoverController@create');
            Route::post('capas-relatorios', 'ReportCoverController@store');
            Route::put('capas-relatorios/{id}', 'ReportCoverController@update');
            //Route::delete('capas-relatorios/{id}', 'ReportCoverController@destroy');
            Route::get('capas-relatorios/delete/{id}', 'ReportCoverController@destroy');
            //--

            Route::get('partidos', 'MainController@getPoliticalParties');
            Route::get('partidos/editar/{p_Id?}', 'MainController@editPoliticalParty');
            Route::post('partidos', 'MainController@postPoliticalParty');

            Route::get('acoes-turismo', 'MainController@getTourismActions');
            Route::get('acoes-turismo/editar/{p_Id?}', 'MainController@editTourismAction');
            Route::post('acoes-turismo', 'MainController@postTourismAction');

            Route::get('formas-pagamento', 'MainController@getPaymentMethods');
            Route::get('formas-pagamento/editar/{p_Id?}', 'MainController@editPaymentMethod');
            Route::post('formas-pagamento', 'MainController@postPaymentMethod');
            Route::get('formas-pagamento/excluir/{p_Id}', 'MainController@deletePaymentMethod');

            Route::get('paises', 'MainController@getCountries');
            Route::get('paises/editar/{p_Id?}', 'MainController@editCountry');
            Route::post('paises', 'MainController@postCountry');
            Route::get('paises/excluir/{p_Id}', 'MainController@deleteCountry');

            Route::get('idiomas', 'MainController@getLanguages');
            Route::get('idiomas/editar/{p_Id?}', 'MainController@editLanguage');
            Route::post('idiomas', 'MainController@postLanguage');
            Route::get('idiomas/excluir/{p_Id}', 'MainController@deleteLanguage');

            Route::get('tipos', 'MainController@getTypes');
            Route::get('tipos/editar/{p_Id?}', 'MainController@editType');
            Route::post('tipos', 'MainController@postType');
            Route::get('tipos-inventario', 'MainController@getInventoryTypes');

            Route::get('regioes', 'CircuitController@getRegions');
            Route::get('regioes/editar/{p_Id?}', 'CircuitController@editRegion');
            Route::post('regioes', 'CircuitController@postRegion');

            /*
            |--------------------------------------------------------------------------
            | Users
            |--------------------------------------------------------------------------
             */

            Route::get('/usuarios', 'UserController@getUsers');
            Route::get('/dt/usuarios', 'UserController@getDTUsers');
            Route::get('/usuarios/editar/{p_Id?}', 'UserController@editUser');
            Route::post('/usuarios', 'UserController@postUser');
            Route::get('/usuarios/desativar/{p_Id}', 'UserController@toggleActive');
        });

        Route::get('/logout', function () {
            \Auth::logout();
            return redirect(url('/admin/login'));
        });
    });
});
