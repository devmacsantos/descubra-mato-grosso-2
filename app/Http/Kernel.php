<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
//        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'admin' => \App\Http\Middleware\Admin::class,
        'adminOrMasterOrComunicacao' => \App\Http\Middleware\AdminOrMasterOrComunicacao::class,
        'adminOrMaster' => \App\Http\Middleware\AdminOrMaster::class,
        'adminMasterComunicacaoParceiro' => \App\Http\Middleware\AdminMasterComunicacaoParceiro::class,
        'adminMasterMunicipioCircuito' => \App\Http\Middleware\AdminMasterMunicipioCircuito::class,
        'adminMasterComunicacaoMunicipioCircuito' => \App\Http\Middleware\AdminMasterComunicacaoMunicipioCircuito::class,
        'adminMasterComunicacaoMunicipioCircuitoParceiro' => \App\Http\Middleware\AdminMasterComunicacaoMunicipioCircuitoParceiro::class,
        'adminMasterComunicacaoMunicipioCircuitoFiscalizador' => \App\Http\Middleware\AdminMasterComunicacaoMunicipioCircuitoFiscalizador::class,
        'adminMasterComunicacaoMunicipioCircuitoFiscalizadorTrade' => \App\Http\Middleware\AdminMasterComunicacaoMunicipioCircuitoFiscalizadorTrade::class,
        'adminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiro' => \App\Http\Middleware\AdminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiro::class,
        'adminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiroTrade' => \App\Http\Middleware\AdminMasterComunicacaoMunicipioCircuitoFiscalizadorParceiroTrade::class,
        'adminOrComunicacao' => \App\Http\Middleware\AdminOrComunicacao::class,
        'blockedNotAdmin' => \App\Http\Middleware\BlockedNotAdmin::class,
    ];
}
