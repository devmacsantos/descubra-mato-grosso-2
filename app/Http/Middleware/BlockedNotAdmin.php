<?php

namespace App\Http\Middleware;

use App\UserType;
use Closure;
use Illuminate\Support\Facades\App;

class BlockedNotAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!UserType::isAdmin())
            return App::abort(403);
        return $next($request);
    }
}
