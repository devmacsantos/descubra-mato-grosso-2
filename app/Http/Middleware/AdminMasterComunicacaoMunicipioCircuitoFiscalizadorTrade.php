<?php

namespace App\Http\Middleware;

use App\UserType;
use Closure;

class AdminMasterComunicacaoMunicipioCircuitoFiscalizadorTrade
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!UserType::isAdmin() && !UserType::isMaster() && !UserType::isComunicacao() && !UserType::isMunicipio() && !UserType::isCircuito() && !UserType::isFiscalizador() && !UserType::isTrade())
            return redirect()->intended('/admin')->with('error_message', 'Acesso não autorizado!');
        return $next($request);
    }
}
