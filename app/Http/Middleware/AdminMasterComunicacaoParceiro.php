<?php

namespace App\Http\Middleware;

use App\UserType;
use Closure;

class AdminMasterComunicacaoParceiro
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!UserType::isAdmin() && !UserType::isMaster() && !UserType::isComunicacao() && !UserType::isParceiro())
            return redirect()->intended('/admin')->with('error_message', 'Acesso não autorizado!');
        return $next($request);
    }
}
