<?php

namespace App\Http\Controllers;

use App\InventoryPhoto;
use App\PaymentMethod;
use App\RevisionStatus;
use App\TouristicCircuitCities;
use App\TripType;
use App\TypeHasCadasturActivity;
use App\UserTradeItem;
use App\UserType;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\AccomodationService;
use App\City;
use App\CuisineNationality;
use App\CuisineRegion;
use App\CuisineService;
use App\CuisineTheme;
use App\EventService;
use App\FoodDrinksService;
use App\Language;
use App\OtherTourismService;
use App\RecreationService;
use App\TourismAgencyService;
use App\TourismTransportationService;
use App\Type;
use App\Voltage;
use \Illuminate\Support\Facades\Input;
use Validator;

class ModuleBController extends BaseController
{
	public static $m_AccomodationWithRegistrationTypes = [
		''=>'',
		'Apart-hotel/Flat/Condohotel'=>'Apart-hotel/Flat/Condohotel',
		'Hotel'=>'Hotel',
		'Hotel de lazer/Resort'=>'Hotel de lazer/Resort',
		'Hotel de selva/Lodge'=>'Hotel de selva/Lodge',
		'Hotel histórico'=>'Hotel histórico'
	];
	public static $m_AccomodationWithoutRegistrationTypes = [
		''=>'',
		'Hospedaria'=>'Hospedaria',
		'Motel'=>'Motel',
		'Pensão'=>'Pensão'
	];
	public static $m_AccomodationExtraHotelTypes = [
		''=>'',
		'Albergue'=>'Albergue',
		'Camping'=>'Camping',
		'Colônia de Férias'=>'Colônia de Férias'
	];

	/*
	|--------------------------------------------------------------------------
	| B1 - AccomodationService
	|--------------------------------------------------------------------------
	*/

	public function getAccomodationServices()
	{
            return view('admin.moduleB.accomodationService.list')->with(['p_Status' => RevisionStatus::getCompleteStatusList()]);
	}

	public function getDTAccomodationServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
                return AccomodationService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editAccomodationService($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = AccomodationService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_AccomodationService = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('B1', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('B1', $p_Id);
		}
		else
		{
			$v_AccomodationService = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();
                
		return view('admin.moduleB.accomodationService.edit')->with(['p_Cities' => $v_Cities,
		                                                             'p_CoverPhoto' => $v_CoverPhoto,
		                                                             'p_PhotoGallery' => $v_PhotoGallery,
		                                                             'p_AccomodationService' => $v_AccomodationService,
		                                                             'p_PaymentMethods' => PaymentMethod::getList(),
		                                                             'p_Types' => Type::getInventoryTypeListByName('B1'),
		                                                             'p_CadasturActivities' => TypeHasCadasturActivity::all(),
		                                                             'p_Voltages' => Voltage::getList(),
		                                                             'p_TripTypes' => TripType::getList()]);
	}

	public function postAccomodationService()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Restaurante = false;
		$v_Eventos = false;
		if($v_Id != null){
			$v_Service = AccomodationService::find($v_Id);
			$v_Restaurante = json_decode($v_Service->servicos_equipamentos_area_social,1)[0]['valor'];
			$v_Eventos = $v_Service->possui_espacos_eventos;
		}
		$v_Data = Input::all();

		$v_Service = AccomodationService::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data);

		if(!$v_Restaurante && array_key_exists('restaurante_aberto_publico', $v_Data))
			return redirect(url('/admin/inventario/servicos-alimentos' . (UserType::isTrade() ? '' : '/editar')))->with('message', 'Operação realizada com sucesso. Cadastre agora o restaurante');
		else if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Service->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Operação realizada com sucesso. Cadastre agora o espaço para eventos');
		}
		else
			return redirect(url('/admin/inventario/servicos-hospedagem'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getAccomodationServiceHistory($p_Id)
	{
		$v_AccomodationService = AccomodationService::findOrFail($p_Id);
		$v_Title = 'B1 - Serviços e equipamentos de hospedagem - Histórico - ' . City::find($v_AccomodationService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_AccomodationService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_AccomodationService->revisionHistory]);
	}

	public function deleteAccomodationService($p_Id)
	{
		AccomodationService::deleteAccomodationService($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| B2 - FoodDrinksService
	|--------------------------------------------------------------------------
	*/

	public function getFoodDrinksServices()
	{
		return view('admin.moduleB.foodDrinksService.list')->with(['p_Status' => RevisionStatus::getCompleteStatusList()]);
	}

	public function getDTFoodDrinksServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return FoodDrinksService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editFoodDrinksService($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = FoodDrinksService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_FoodDrinksService = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('B2', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('B2', $p_Id);
		}
		else
		{
			$v_FoodDrinksService = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleB.foodDrinksService.edit')->with(['p_Cities' => $v_Cities,
		                                                           'p_FoodDrinksService' => $v_FoodDrinksService,
		                                                           'p_Types' => Type::getInventoryTypeListByName('B2'),
		                                                           'p_CoverPhoto' => $v_CoverPhoto,
		                                                           'p_PhotoGallery' => $v_PhotoGallery,
		                                                           'p_PaymentMethods' => PaymentMethod::getList(),
		                                                           'p_LanguageNames' => Language::getNameList(),
		                                                           'p_CuisineNationalities' => CuisineNationality::getList(),
                                                                   'p_CuisineServices' => CuisineService::getList(),
                                                                   'p_CuisineThemes' => CuisineTheme::getList(),
                                                                   'p_CuisineRegions' => CuisineRegion::getList(),
                                                                   'p_TripTypes' => TripType::getList()]);
	}

	public function postFoodDrinksService()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Eventos = false;
		if($v_Id != null){
			$v_Service = FoodDrinksService::find($v_Id);
			$v_Eventos = $v_Service->possui_espacos_eventos;
		}
		$v_Data = Input::all();
		$v_Service = FoodDrinksService::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data['formulario']);

		if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Service->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Operação realizada com sucesso. Cadastre agora o espaço para eventos');
		}
		else
			return redirect(url('/admin/inventario/servicos-alimentos'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getFoodDrinksServiceHistory($p_Id)
	{
		$v_FoodDrinksService = FoodDrinksService::findOrFail($p_Id);
		$v_Title = 'B2 - Serviços e equipamentos de alimentos e bebidas - Histórico - ' . City::find($v_FoodDrinksService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_FoodDrinksService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_FoodDrinksService->revisionHistory]);
	}

	public function deleteFoodDrinksService($p_Id)
	{
		FoodDrinksService::deleteFoodDrinksService($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| B3 - TourismAgencyService
	|--------------------------------------------------------------------------
	*/

	public function getTourismAgencyServices()
	{
		return view('admin.moduleB.tourismAgencyService.list')->with(['p_Status' => RevisionStatus::getCompleteStatusList()]);
	}

	public function getDTTourismAgencyServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return TourismAgencyService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editTourismAgencyService($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = TourismAgencyService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_TourismAgencyService = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('B3', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('B3', $p_Id);
		}
		else {
			$v_TourismAgencyService = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleB.tourismAgencyService.edit')->with(['p_Cities' => $v_Cities,
		                                                              'p_TourismAgencyService' => $v_TourismAgencyService,
		                                                              'p_Types' => Type::getInventoryTypeListByName('B3'),
		                                                              'p_Languages' => Language::getList(),
		                                                              'p_LanguageNames' => Language::getNameList(),
		                                                              'p_CoverPhoto' => $v_CoverPhoto,
		                                                              'p_PhotoGallery' => $v_PhotoGallery,
		                                                              'p_TripTypes' => TripType::getList()]);
	}

	public function postTourismAgencyService()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		TourismAgencyService::post(Input::get('id'), $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::get('formulario'));
		return redirect(url('/admin/inventario/servicos-agencias-turismo'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getTourismAgencyServiceHistory($p_Id)
	{
		$v_TourismAgencyService = TourismAgencyService::findOrFail($p_Id);
		$v_Title = 'B3 - Serviços e equipamentos de agências de turismo - Histórico - ' . City::find($v_TourismAgencyService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_TourismAgencyService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_TourismAgencyService->revisionHistory]);
	}

	public function deleteTourismAgencyService($p_Id)
	{
		TourismAgencyService::deleteTourismAgencyService($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| B4 - TourismTransportationService
	|--------------------------------------------------------------------------
	*/

	public function getTourismTransportationServices()
	{

		return view('admin.moduleB.tourismTransportationService.list')->with(['p_Status' => RevisionStatus::getCompleteStatusList()]);
	}

	public function getDTTourismTransportationServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return TourismTransportationService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editTourismTransportationService($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = TourismTransportationService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_TransportationService = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('B4', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('B4', $p_Id);
		}
		else {
			$v_TransportationService = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleB.tourismTransportationService.edit')->with(['p_Cities' => $v_Cities,
		                                                                      'p_TransportationService' => $v_TransportationService,
		                                                                      'p_Types' => Type::getInventoryTypeListByName('B4'),
		                                                                      'p_CoverPhoto' => $v_CoverPhoto,
		                                                                      'p_PhotoGallery' => $v_PhotoGallery,
		                                                                      'p_TripTypes' => TripType::getList()]);
	}

	public function postTourismTransportationService()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		TourismTransportationService::post(Input::get('id'), $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::get('formulario'));
		return redirect(url('/admin/inventario/servicos-transporte-turistico'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getTourismTransportationServiceHistory($p_Id)
	{
		$v_TourismTransportationService = TourismTransportationService::findOrFail($p_Id);
		$v_Title = 'B4 - Serviços e equipamentos de transporte turístico - Histórico - ' . City::find($v_TourismTransportationService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_TourismTransportationService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_TourismTransportationService->revisionHistory]);
	}

	public function deleteTourismTransportationService($p_Id)
	{
		TourismTransportationService::deleteTourismTransportationService($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| B5 - EventService
	|--------------------------------------------------------------------------
	*/

	public function getEventServices()
	{

		return view('admin.moduleB.eventService.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTEventServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return EventService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editEventService($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = EventService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_EventService = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('B5', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('B5', $p_Id);
		}
		else
		{
			if(Input::get('link') == 1)
				$v_EventService = Session::get('EventServiceBasicFields');
			else
				$v_EventService = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleB.eventService.edit')->with(['p_Cities' => $v_Cities,
		                                                      'p_EventService' => $v_EventService,
                                                              'p_Types' => Type::getInventoryTypeListByName('B5'),
                                                              'p_CadasturActivities' => TypeHasCadasturActivity::all(),
                                                              'p_CoverPhoto' => $v_CoverPhoto,
                                                              'p_PhotoGallery' => $v_PhotoGallery,
                                                              'p_Linked' => Input::get('link'),
                                                              'p_TripTypes' => TripType::getList()]);
	}

	public function postEventService()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		EventService::post(Input::get('id'), $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::get('formulario'));
		return redirect(url('/admin/inventario/servicos-eventos'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getEventServiceHistory($p_Id)
	{
		$v_EventService = EventService::findOrFail($p_Id);
		$v_Title = 'B5 - Serviços e equipamentos de eventos - Histórico - ' . City::find($v_EventService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_EventService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_EventService->revisionHistory]);
	}

	public function deleteEventService($p_Id)
	{
		EventService::deleteEventService($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| B6 - RecreationService
	|--------------------------------------------------------------------------
	*/

	public function getRecreationServices()
	{

		return view('admin.moduleB.recreationService.list')->with(['p_Status' => RevisionStatus::getCompleteStatusList()]);
	}

	public function getDTRecreationServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return RecreationService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editRecreationService($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = RecreationService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_RecreationService = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('B6', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('B6', $p_Id);
		}
		else {
			$v_RecreationService = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleB.recreationService.edit')->with(['p_Cities' => $v_Cities,
		                                                           'p_RecreationService' => $v_RecreationService,
		                                                           'p_Types' => Type::getInventoryTypeListByName('B6'),
                                                                   'p_CadasturActivities' => TypeHasCadasturActivity::all(),
		                                                           'p_PaymentMethods' => PaymentMethod::getList(),
		                                                           'p_CoverPhoto' => $v_CoverPhoto,
		                                                           'p_PhotoGallery' => $v_PhotoGallery,
		                                                           'p_TripTypes' => TripType::getList()]);
	}

	public function postRecreationService()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Eventos = false;
		if($v_Id != null){
			$v_Service = RecreationService::find($v_Id);
			$v_Eventos = $v_Service->possui_espacos_eventos;
		}
		$v_Data = Input::all();
		$v_Service = RecreationService::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data['formulario']);

		if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Service->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Operação realizada com sucesso. Cadastre agora o espaço para eventos');
		}
		else
			return redirect(url('/admin/inventario/servicos-lazer'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getRecreationServiceHistory($p_Id)
	{
		$v_RecreationService = RecreationService::findOrFail($p_Id);
		$v_Title = 'B6 - Serviços e equipamentos de lazer - Histórico - ' . City::find($v_RecreationService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_RecreationService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_RecreationService->revisionHistory]);
	}

	public function deleteRecreationService($p_Id)
	{
		RecreationService::deleteRecreationService($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| B7 - OtherTourismService
	|--------------------------------------------------------------------------
	*/

	public function getOtherTourismServices()
	{
		return view('admin.moduleB.otherService.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTOtherTourismServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return OtherTourismService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editOtherTourismService($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Query = OtherTourismService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_OtherService = $v_Query->firstOrFail();
			$v_CoverPhoto = InventoryPhoto::getCoverPhoto('B7', $p_Id);
			$v_PhotoGallery = InventoryPhoto::getPhotoGallery('B7', $p_Id);
		}
		else
		{
			$v_OtherService = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
		}

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleB.otherService.edit')->with(['p_Cities' => $v_Cities,
		                                                      'p_OtherService' => $v_OtherService,
		                                                      'p_Types' => Type::getInventoryTypeListByName('B7'),
                                                              'p_CadasturActivities' => TypeHasCadasturActivity::all(),
		                                                      'p_LanguageNames' => Language::getNameList(),
		                                                      'p_CoverPhoto' => $v_CoverPhoto,
		                                                      'p_PhotoGallery' => $v_PhotoGallery,
		                                                      'p_TripTypes' => TripType::getList()]);
	}

	public function postOtherTourismService()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		$v_Id = Input::get('id');
		$v_Eventos = false;
		if($v_Id != null){
			$v_Service = OtherTourismService::find($v_Id);
			$v_Eventos = $v_Service->possui_espacos_eventos;
		}
		$v_Data = Input::all();
		$v_Service = OtherTourismService::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, $v_Data);

		if(!$v_Eventos && array_key_exists('possui_espacos_eventos', $v_Data['formulario'])){
			Session::put('EventServiceBasicFields', $v_Service->getEventBasicFields());
			return redirect(url('/admin/inventario/servicos-eventos/editar?link=1'))->with('message', 'Operação realizada com sucesso. Cadastre agora o espaço para eventos');
		}
		else
			return redirect(url('/admin/inventario/outros-servicos-turisticos'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getOtherTourismServiceHistory($p_Id)
	{
		$v_OtherService = OtherTourismService::findOrFail($p_Id);
		$v_Title = 'B7 - Outros serviços e equipamentos turísticos - Histórico - ' . City::find($v_OtherService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_OtherService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_OtherService->revisionHistory]);
	}

	public function deleteOtherTourismService($p_Id)
	{
		OtherTourismService::deleteOtherTourismService($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	public function getCNPJAvailability()
	{
		$v_Type = Input::get('tipo');
		$v_CNPJ = Input::get('cnpj');
		if($v_Type == 'B1')
			return AccomodationService::checkCNPJAvailability($v_CNPJ);
		if($v_Type == 'B2')
			return FoodDrinksService::checkCNPJAvailability($v_CNPJ);
		if($v_Type == 'B3')
			return TourismAgencyService::checkCNPJAvailability($v_CNPJ);
		if($v_Type == 'B4')
			return TourismTransportationService::checkCNPJAvailability($v_CNPJ);
		if($v_Type == 'B6')
			return RecreationService::checkCNPJAvailability($v_CNPJ);
		else
			return null;
	}

	public function requestCNPJOwnership()
	{
		$v_Type = Input::get('tipo');
		$v_CNPJ = Input::get('cnpj');
		if($v_Type == 'B1')
			$v_Id = AccomodationService::findIdByCNPJ($v_CNPJ);
		if($v_Type == 'B2')
			$v_Id = FoodDrinksService::findIdByCNPJ($v_CNPJ);
		if($v_Type == 'B3')
			$v_Id = TourismAgencyService::findIdByCNPJ($v_CNPJ);
		if($v_Type == 'B4')
			$v_Id = TourismTransportationService::findIdByCNPJ($v_CNPJ);
		if($v_Type == 'B6')
			$v_Id = RecreationService::findIdByCNPJ($v_CNPJ);
		if(!isset($v_Id))
			return null;
		else {
			UserTradeItem::requestTradeItemOwnership($v_Type, $v_Id);
			return redirect()->back()->with('message', 'Solicitação realizada! A SECRETARIA ADJUNTA DE TURISMO analisará seu pedido.');
		}
	}

	public function treatCNPJOwnershipRequest()
	{
		if(Input::get('aprovar'))
			UserTradeItem::approveOwnershipRequest(Input::get('tipo'), Input::get('id'), Input::get('user_id'));
		else
			UserTradeItem::rejectOwnershipRequest(Input::get('tipo'), Input::get('id'), Input::get('user_id'));
		return redirect()->back()->with('message', 'Operação realizada com sucesso!');
	}
}

