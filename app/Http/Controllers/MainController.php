<?php

/* lida com as requisições de edição de propriedades, configurações e recursos variados do sistema, como os tooltips do inventário, dados de autenticação do Cadastur, equipes responsáveis por coleta de dados, mídias doadas, etc. */

namespace App\Http\Controllers;

use App\City;
use App\Climate;
use App\Country;
use App\DonatedMedia;
use App\EconomicActivity;
use App\InventoryItem;
use App\Language;
use App\MobileCarrier;
use App\MobileCarrierService;
use App\Parameter;
use App\PaymentMethod;
use App\PoliticalParty;
use App\ResponsibleTeam;
use App\Road;
use App\SupplyType;
use App\TourismAction;
use App\TouristicCircuitCities;
use App\Type;
use App\UserType;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Validator;

class MainController extends BaseController
{
    
    /*
	|--------------------------------------------------------------------------
	| Tooltips
	|--------------------------------------------------------------------------
	*/

    public function editTooltips($p_Type)
    {
        return view('admin.properties.tooltips.edit')->with(['p_Tooltips' => Parameter::getParameterByKey('tooltips-' . $p_Type),
                                                             'p_Type' => $p_Type]);
    }

    public function postTooltips($p_Type)
    {
        Parameter::postParameters(Input::get('tooltips'));
        return redirect(url('/admin/tooltips/' . $p_Type))->with('message', 'Operação realizada com sucesso.');
    }
    /*
	|--------------------------------------------------------------------------
	| Cadastur config
	|--------------------------------------------------------------------------
	*/

    public function editCadasturAuth()
    {
        return view('admin.properties.cadastur.edit')->with(['p_Url' => Parameter::getParameterByKey('cadastur_url'),
                                                             'p_Pwd' => Parameter::getParameterByKey('cadastur_senha')]);
    }

    public function postCadasturAuth()
    {
        Parameter::postParameters(Input::get('data'));
        return redirect(url('/admin/cadastur/editar'))->with('message', 'Operação realizada com sucesso.');
    }
    /*
	|--------------------------------------------------------------------------
	| Event calendar config
	|--------------------------------------------------------------------------
	*/

    public function editEventCalendarConfig()
    {
        return view('admin.properties.eventCalendar.edit')->with(['p_EventCalendarCover' => Parameter::getParameterByKey('event_calendar_cover'),
                                                                  'p_EventCalendarHeader' => Parameter::getParameterByKey('event_calendar_header'),
                                                                  'p_EventCalendarLastPage' => Parameter::getParameterByKey('event_calendar_last_page')]);
    }

    public function postEventCalendarConfig()
    {
        $v_Path = public_path() . '/imagens/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);
        $v_Path .=  '/calendario-eventos/';
        if(!\File::exists($v_Path))
            \File::makeDirectory($v_Path);

        $v_Data = [];

        $v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];
        $v_DeleteCover = in_array('capa',$v_DeletedPhotos);
        $v_DeleteHeader = in_array('cabecalho',$v_DeletedPhotos);

        $v_Cover = Input::file('capa');
        if( ($v_Cover != null && $v_Cover->isValid()) || $v_DeleteCover){

            $v_OldFileName = explode('/', Parameter::getParameterByKey('event_calendar_cover'));
            $v_OldFileName = array_pop($v_OldFileName);
            \File::delete($v_Path . $v_OldFileName);

            if($v_DeleteCover)
                $v_Data['event_calendar_cover'] = null;
        }
        if($v_Cover != null && $v_Cover->isValid()){
            $v_Photo = Image::make($v_Cover);

            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $v_Photo->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $v_Photo->save($v_Path . $v_PhotoName);
            $v_Data['event_calendar_cover'] = $v_PhotoName;
        }

        $v_Header = Input::file('cabecalho');
        if( ($v_Header != null && $v_Header->isValid()) || $v_DeleteHeader){

            $v_OldFileName = explode('/', Parameter::getParameterByKey('event_calendar_header'));
            $v_OldFileName = array_pop($v_OldFileName);
            \File::delete($v_Path . $v_OldFileName);

            if($v_DeleteHeader)
                $v_Data['event_calendar_header'] = null;
        }
        if($v_Header != null && $v_Header->isValid()){
            $v_Photo = Image::make($v_Header);

            $v_PhotoName =  time() . str_random(10) . '.jpg';
            $v_Photo->widen(1920, function ($constraint){
                $constraint->upsize();
            });
            $v_Photo->save($v_Path . $v_PhotoName);
            $v_Data['event_calendar_header'] = $v_PhotoName;
        }
        $v_Data['event_calendar_last_page'] = Input::get('event_calendar_last_page');

        Parameter::postParameters($v_Data);
        return redirect(url('/admin/configurar-calendario-eventos'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| PoliticalParty
	|--------------------------------------------------------------------------
	*/

    public function getPoliticalParties()
    {
        return view('admin.properties.politicalParty.list')->with(['p_PoliticalParties' => PoliticalParty::all()]);
    }

    public function editPoliticalParty($p_Id = null)
    {
        if ($p_Id != null)
            $v_Party = PoliticalParty::findOrFail($p_Id);
        else
            $v_Party = null;
        return view('admin.properties.politicalParty.edit')->with(['p_PoliticalParty' => $v_Party]);
    }

    public function postPoliticalParty()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? PoliticalParty::$m_Rules : PoliticalParty::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_Abbreviation = Input::get('sigla');
        $v_Name = Input::get('nome');
        PoliticalParty::post($v_Id, $v_Abbreviation, $v_Name);
        return redirect(url('/admin/partidos'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| Climate
	|--------------------------------------------------------------------------
	*/

    public function getClimates()
    {
        return view('admin.properties.climate.list')->with(['p_Climates' => Climate::all()]);
    }

    public function editClimate($p_Id = null)
    {
        if ($p_Id != null)
            $v_Climate = Climate::findOrFail($p_Id);
        else
            $v_Climate = null;
        return view('admin.properties.climate.edit')->with(['p_Climate' => $v_Climate]);
    }

    public function postClimate()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? Climate::$m_Rules : Climate::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_Name = Input::get('nome');
        Climate::post($v_Id, $v_Name);
        return redirect(url('/admin/climas'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| PaymentMethod
	|--------------------------------------------------------------------------
	*/

    public function getPaymentMethods()
    {
        return view('admin.properties.paymentMethod.list')->with(['p_PaymentMethods' => PaymentMethod::all()]);
    }

    public function editPaymentMethod($p_Id = null)
    {
        if ($p_Id != null)
            $v_PaymentMethod = PaymentMethod::findOrFail($p_Id);
        else
            $v_PaymentMethod = null;
        return view('admin.properties.paymentMethod.edit')->with(['p_PaymentMethod' => $v_PaymentMethod]);
    }

    public function postPaymentMethod()
    {
        $v_Id = Input::get('id');
        PaymentMethod::post($v_Id, Input::all());
        return redirect(url('/admin/formas-pagamento'))->with('message', 'Operação realizada com sucesso.');
    }

    public function deletePaymentMethod($p_Id)
    {
        PaymentMethod::where('id', $p_Id)->delete();
        return redirect(url('/admin/formas-pagamento'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| SupplyType
	|--------------------------------------------------------------------------
	*/

    public function getSupplyTypes($p_Type)
    {
        return view('admin.properties.supplyType.list')->with(['p_Type' => $p_Type,
                                                    'p_SupplyTypes' => SupplyType::where('tipo', $p_Type)->get()]);
    }

    public function editSupplyType($p_Type, $p_Id = null)
    {
        if ($p_Id != null)
            $v_SupplyType = SupplyType::findOrFail($p_Id);
        else
            $v_SupplyType = null;
        return view('admin.properties.supplyType.edit')->with(['p_Type' => $p_Type,
                                                    'p_SupplyType' => $v_SupplyType]);
    }

    public function postSupplyType($p_Type)
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? SupplyType::$m_Rules : SupplyType::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_Name = Input::get('nome');
        SupplyType::post($v_Id, $p_Type, $v_Name);
        return redirect(url('/admin/servicos-abastecimento/'.$p_Type))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| EconomicActivity
	|--------------------------------------------------------------------------
	*/

    public function getEconomicActivities()
    {
        return view('admin.properties.economicActivity.list')->with(['p_EconomicActivities' => EconomicActivity::all()]);
    }

    public function editEconomicActivity($p_Id = null)
    {
        if ($p_Id != null)
            $v_EconomicActivity = EconomicActivity::findOrFail($p_Id);
        else
            $v_EconomicActivity = null;
        return view('admin.properties.economicActivity.edit')->with(['p_EconomicActivity' => $v_EconomicActivity]);
    }

    public function postEconomicActivity()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? EconomicActivity::$m_Rules : EconomicActivity::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_Name = Input::get('nome');
        EconomicActivity::post($v_Id, $v_Name);
        return redirect(url('/admin/atividades-economicas'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| MobileCarrier and MobileCarrierService
	|--------------------------------------------------------------------------
	*/

    public function getCarriers()
    {
        return view('admin.properties.mobileCarrier.list')->with(['p_Carriers' => MobileCarrier::all()]);
    }

    public function editCarrier($p_Id = null)
    {
        if ($p_Id != null)
            $v_Carrier = MobileCarrier::findOrFail($p_Id);
        else
            $v_Carrier = null;
        return view('admin.properties.mobileCarrier.edit')->with(['p_Carrier' => $v_Carrier]);
    }

    public function postCarrier()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? MobileCarrier::$m_Rules : MobileCarrier::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_Name = Input::get('nome');
        MobileCarrier::post($v_Id, $v_Name);
        return redirect(url('/admin/operadoras-telefonicas'))->with('message', 'Operação realizada com sucesso.');
    }

    public function deleteCarrier($p_Id)
    {
        MobileCarrier::where('id', $p_Id)->delete();
        return redirect(url('/admin/operadoras-telefonicas'))->with('message', 'Operação realizada com sucesso.');
    }

    public function getCarrierServices()
    {
        return view('admin.properties.mobileCarrierService.list')->with(['p_Services' => MobileCarrierService::all()]);
    }

    public function editCarrierService($p_Id = null)
    {
        if ($p_Id != null)
            $v_Service = MobileCarrierService::findOrFail($p_Id);
        else
            $v_Service = null;
        return view('admin.properties.mobileCarrierService.edit')->with(['p_Service' => $v_Service]);
    }

    public function postCarrierService()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? MobileCarrierService::$m_Rules : MobileCarrierService::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_Name = Input::get('nome');
        MobileCarrierService::post($v_Id, $v_Name);
        return redirect(url('/admin/servicos-telefonicos'))->with('message', 'Operação realizada com sucesso.');
    }

    public function deleteCarrierService($p_Id)
    {
        MobileCarrierService::where('id', $p_Id)->delete();
        return redirect(url('/admin/servicos-telefonicos'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| TourismAction
	|--------------------------------------------------------------------------
	*/

    public function getTourismActions()
    {
        return view('admin.properties.tourismAction.list')->with(['p_TourismActions' => TourismAction::all()]);
    }

    public function editTourismAction($p_Id = null)
    {
        if ($p_Id != null)
            $v_Action = TourismAction::findOrFail($p_Id);
        else
            $v_Action = null;
        return view('admin.properties.tourismAction.edit')->with(['p_TourismAction' => $v_Action]);
    }

    public function postTourismAction()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? TourismAction::$m_Rules : TourismAction::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_Name = Input::get('nome');
        TourismAction::post($v_Id, $v_Name);
        return redirect(url('/admin/acoes-turismo'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| EconomicActivity
	|--------------------------------------------------------------------------
	*/

    public function getRoads($p_Type)
    {
        return view('admin.properties.road.list')->with(['p_Roads' => Road::where('tipo', $p_Type)->get(),
                                              'p_Type' => $p_Type]);
    }

    public function editRoad($p_Type, $p_Id = null)
    {
        if ($p_Id != null)
            $v_Road = Road::findOrFail($p_Id);
        else
            $v_Road = null;
        return view('admin.properties.road.edit')->with(['p_Road' => $v_Road,
                                              'p_Type' => $p_Type]);
    }

    public function postRoad($p_Type)
    {
        $v_Id = Input::get('id');
        Road::post($v_Id, Input::get('dados'));
        return redirect(url('/admin/rodovias/' . $p_Type))->with('message', 'Operação realizada com sucesso.');
    }

    public function deleteRoad($p_Type, $p_Id)
    {
        Road::where('id', $p_Id)->delete();
        return redirect(url('/admin/rodovias/' . $p_Type))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| Type
	|--------------------------------------------------------------------------
	*/

    public function getTypes()
    {
        return view('admin.properties.type.list')->with(['p_Types' => Type::getAll()]);
    }

    public function editType($p_Id = null)
    {
        if ($p_Id != null)
            $v_Type  = Type::findOrFail($p_Id);
        else
            $v_Type = null;
        return view('admin.properties.type.edit')->with(['p_Type' => $v_Type,
                                              'p_InventoryItems' => InventoryItem::getList(),
                                              'p_SuperTypes' => Type::getSuper()]);
    }

    public function postType()
    {
        $v_Id = Input::get('id');
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? Type::$m_Rules : Type::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Id = Input::get('id');
        $v_InventoryItemId = Input::get('inventory_item_id');
        $v_SuperTypeId = Input::get('super_type_id') ? Input::get('super_type_id') : null;
        $v_Name = Input::get('nome');
        $v_NameEn = Input::get('nome_en');
        $v_NameEs = Input::get('nome_es');
        $v_NameFr = Input::get('nome_fr');

        Type::post($v_Id, 
                    $v_InventoryItemId, 
                    $v_SuperTypeId, 
                    $v_Name, 
                    $v_NameEn, 
                    $v_NameEs, 
                    $v_NameFr,
                    Input::get('slug'),
                    Input::get('trade'),
                    Input::get('sinonimos'),
                    Input::get('excluir')
                );
        return redirect(url('/admin/tipos'))->with('message', 'Operação realizada com sucesso.');
    }

    public function getInventoryTypes()
    {
        return Type::getInventoryTypeListById(Input::get('id'), Input::get('inventory_id'));
    }

    public function getInventorySubtypes()
    {
        return Type::getInventorySubtypeList(Input::get('super_type_id'));
    }

    /*
	|--------------------------------------------------------------------------
	| Donated Media
	|--------------------------------------------------------------------------
	*/

    public function getDonatedMedia()
    {
        return view('admin.donations.list');
    }

    public function getDTDonatedMedia()
    {
        $v_Columns = Input::get('columns');
        $v_Type = $v_Columns[0]['search']['value'];
        $v_CreatedAt = $v_Columns[1]['search']['value'];
        $v_City = $v_Columns[2]['search']['value'];
        $v_Description = $v_Columns[3]['search']['value'];
        $v_Name = $v_Columns[4]['search']['value'];
        $v_Phone = $v_Columns[5]['search']['value'];
        $v_Order = Input::get('order')[0];
        $v_Start = Input::get('start');
        $v_Length = Input::get('length');
        $v_Draw = Input::get('draw');
        return DonatedMedia::getDT($v_Type, $v_CreatedAt, $v_City, $v_Description, $v_Name, $v_Phone, $v_Order, $v_Start, $v_Length, $v_Draw);
    }

    public function deleteDonatedMedia($p_Id)
    {
        DonatedMedia::deleteMedia($p_Id);
        return redirect()->back()->with('message', 'Operação realizada com sucesso');
    }


    /*
    |--------------------------------------------------------------------------
    |  Inventory Search
    |--------------------------------------------------------------------------
    */

    public function getInventorySearch()
    {
        return view('admin.inventory.search')->with(['p_Cities' => ['' => ''] + City::getList(),
                                                     'p_Data' => Input::all()]);
    }

    /*
    |--------------------------------------------------------------------------
    |  Responsible Team
    |--------------------------------------------------------------------------
    */

    public function getResponsibleTeam()
    {
        return view('admin.responsibleTeam.list');
    }

    public function getDTResponsibleTeam()
    {
        $v_Columns = Input::get('columns');
        $v_Responsible = $v_Columns[0]['search']['value'];
        $v_Institution = $v_Columns[1]['search']['value'];
        $v_City = $v_Columns[2]['search']['value'];
        $v_UpdatedAt = $v_Columns[3]['search']['value'];
        $v_Order = Input::get('order')[0];
        $v_Start = Input::get('start');
        $v_Length = Input::get('length');
        $v_Draw = Input::get('draw');
        return ResponsibleTeam::getDT($v_Responsible, $v_Institution, $v_City, $v_UpdatedAt, $v_Order, $v_Start, $v_Length, $v_Draw);
    }

    public function editResponsibleTeam($p_Id = null)
    {
        return view('admin.responsibleTeam.edit')->with(['p_ResponsibleTeam' => $p_Id == null ? '' : ResponsibleTeam::findOrFail($p_Id),
                                                         'p_Cities' => City::getListExceptIds([],true)]);
    }

    public function postResponsibleTeam()
    {
        ResponsibleTeam::post(Input::get('id'), Input::all());
        return redirect(url('/admin/equipes-responsaveis'))->with('message', 'Operação realizada com sucesso');
    }

    public function deleteResponsibleTeam($p_Id)
    {
        $v_Query = ResponsibleTeam::where('id',$p_Id);
        if(UserType::isMunicipio())
            $v_Query->where('city_id', Auth::user()->city_id);
        else if(UserType::isCircuito())
            $v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
        $v_Query->delete();
        return redirect()->back()->with('message', 'Operação realizada com sucesso');
    }

    /*
	|--------------------------------------------------------------------------
	| Country
	|--------------------------------------------------------------------------
	*/

    public function getCountries()
    {
        return view('admin.properties.country.list')->with(['p_Countries' => Country::all()]);
    }

    public function editCountry($p_Id = null)
    {
        if ($p_Id != null)
            $v_Country = Country::findOrFail($p_Id);
        else
            $v_Country = null;
        return view('admin.properties.country.edit')->with(['p_Country' => $v_Country]);
    }

    public function postCountry()
    {
        $v_Id = Input::get('id');
        Country::post($v_Id, Input::all());
        return redirect(url('/admin/paises'))->with('message', 'Operação realizada com sucesso.');
    }

    public function deleteCountry($p_Id)
    {
        Country::where('id', $p_Id)->delete();
        return redirect(url('/admin/paises'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| Language
	|--------------------------------------------------------------------------
	*/

    public function getLanguages()
    {
        return view('admin.properties.language.list')->with(['p_Languages' => Language::all()]);
    }

    public function editLanguage($p_Id = null)
    {
        if ($p_Id != null)
            $v_Language = Language::findOrFail($p_Id);
        else
            $v_Language = null;
        return view('admin.properties.language.edit')->with(['p_Language' => $v_Language]);
    }

    public function postLanguage()
    {
        $v_Id = Input::get('id');
        Language::post($v_Id, Input::get('name'));
        return redirect(url('/admin/idiomas'))->with('message', 'Operação realizada com sucesso.');
    }

    public function deleteLanguage($p_Id)
    {
        Language::where('id', $p_Id)->delete();
        return redirect(url('/admin/idiomas'))->with('message', 'Operação realizada com sucesso.');
    }

}

