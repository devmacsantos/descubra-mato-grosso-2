<?php

/* lida com as requisições de listagem, histórico, edição, criação e remoção dos formulários do módulo A do inventário (informações básicas, meios de acesso, sistemas de segurança e de saúde e outros serviços referentes aos municípios) */

namespace App\Http\Controllers;

use App\City;
use App\CityAccess;
use App\CityAccessGeneral;
use App\CityHospitalSystem;
use App\CityInfo;
use App\CityOtherService;
use App\CitySecuritySystem;
use App\Climate;
use App\EconomicActivity;
use App\EventCategory;
use App\MobileCarrierService;
use App\PermanentEvent;
use App\PoliticalParty;
use App\RevisionStatus;
use App\Road;
use App\SupplyType;
use App\TourismAction;
use App\TouristicCircuitCities;
use App\Type;
use App\UserType;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Input;
use Validator;

class ModuleAController extends BaseController
{
	/*
	|--------------------------------------------------------------------------
	| A1 - CityInfo
	|--------------------------------------------------------------------------
	*/

	public function getCityInfo()
	{
		return view('admin.moduleA.cityInfo.list')->with(['p_AddBtnAvailable' => count(City::getListExceptIds(CityInfo::getCityInfoCityIds(), true)),
		                                                  'p_Status' => RevisionStatus::getList()]);
	}

	public function getDTCityInfo()
	{
		$v_Columns = Input::get('columns');
		$v_City = $v_Columns[0]['search']['value'];
		$v_CreatedAt = $v_Columns[1]['search']['value'];
		$v_UpdatedAt = $v_Columns[2]['search']['value'];
		$v_Status = $v_Columns[3]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return CityInfo::getDT($v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCityInfo($p_Id = null)
	{
		if($p_Id != null)
		{
			$v_Query = CityInfo::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_CityInfo = $v_Query->firstOrFail();
			$v_CityOptions = City::where('id',$v_CityInfo->city_id)->lists('name', 'id')->toArray();
			$v_Cities = City::getListExcept($v_CityInfo->city_id);
			$v_Events = PermanentEvent::getCityEvents($p_Id);
		}
		else
		{
			$v_CityInfo = null;
			$v_CityOptions = City::getListExceptIds(CityInfo::getCityInfoCityIds(), true);
			$v_Cities = City::getList();
			$v_Events = [];
		}

		return view('admin.moduleA.cityInfo.edit')->with(['p_CityOptions' => $v_CityOptions,
		                                                  'p_CityInfo' => $v_CityInfo,
		                                                  'p_Cities' => $v_Cities,
		                                                  'p_EconomicActivities' => EconomicActivity::getList(),
		                                                  'p_WaterSupply' => SupplyType::getList('agua'),
		                                                  'p_SewerSupply' => SupplyType::getList('esgotamento'),
		                                                  'p_EnergySupply' => SupplyType::getList('energia'),
		                                                  'p_GarbageSupply' => SupplyType::getList('coleta-lixo'),
		                                                  'p_TourismActions' => TourismAction::getList(),
		                                                  'p_PoliticalParties' => PoliticalParty::getList(),
		                                                  'p_Climates' => Climate::getList(),
		                                                  'p_EventCategories' => EventCategory::getList(),
		                                                  'p_AllCities' => City::getList(),
		                                                  'p_Events' => $v_Events,
		                                                  'p_MobileCarrierServices' => MobileCarrierService::getList()]);
	}

	public function postCityInfo()
	{
		CityInfo::post(Input::get('id'), Input::all());
		return redirect(url('/admin/inventario/informacoes'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getCityInfoHistory($p_Id)
	{
		$v_CityInfo = CityInfo::findOrFail($p_Id);
		$v_Title = 'A1 - Informações básicas do município - Histórico - ' . City::find($v_CityInfo->city_id)->name;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_History' => $v_CityInfo->revisionHistory]);
	}

	public function deleteCityInfo($p_Id)
	{
		$v_Query = CityInfo::where('id',$p_Id);
		if(UserType::isMunicipio())
			$v_Query->where('city_id', Auth::user()->city_id);
		else if(UserType::isCircuito())
			$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
		$v_Item = $v_Query->firstOrFail();
		if($v_Item){
			$v_Item->equipe_responsavel_observacao = 'DELETED';
			$v_Item->save();
			$v_Query->delete();
		}
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| A2.1 - CityAccessGeneral
	|--------------------------------------------------------------------------
	*/

	public function getCityAccessGeneral()
	{
		return view('admin.moduleA.cityAccessGeneral.list')->with(['p_AddBtnAvailable' => count(City::getListExceptIds(CityAccessGeneral::getCityAccessGeneralIds(), true)),
		                                                           'p_Status' => RevisionStatus::getList()]);
	}

	public function getDTCityAccessGeneral()
	{
		$v_Columns = Input::get('columns');
		$v_City = $v_Columns[0]['search']['value'];
		$v_CreatedAt = $v_Columns[1]['search']['value'];
		$v_UpdatedAt = $v_Columns[2]['search']['value'];
		$v_Status = $v_Columns[3]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return CityAccessGeneral::getDT($v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCityAccessGeneral($p_Id = null)
	{
		if($p_Id != null)
		{
			$v_Query = CityAccessGeneral::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_CityAccessGeneral = $v_Query->firstOrFail();
			$v_CityOptions = City::where('id',$v_CityAccessGeneral->city_id)->lists('name', 'id')->toArray();
		}
		else
		{
			$v_CityAccessGeneral = null;
			$v_CityOptions = City::getListExceptIds(CityAccessGeneral::getCityAccessGeneralIds(), true);
		}

		return view('admin.moduleA.cityAccessGeneral.edit')->with(['p_CityOptions' => $v_CityOptions,
		                                                           'p_CityAccessGeneral' => $v_CityAccessGeneral,
		                                                           'p_FederalRoads' => Road::getListByType('federal'),
		                                                           'p_StateRoads' => Road::getListByType('estadual')]);
	}

	public function postCityAccessGeneral()
	{
		CityAccessGeneral::post(Input::get('id'), Input::get('formulario'));
		return redirect(url('/admin/inventario/meios-acesso/geral'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getCityAccessGeneralHistory($p_Id)
	{
		$v_CityAccessGeneral = CityAccessGeneral::findOrFail($p_Id);
		$v_Title = 'A2.1 - Meios de acesso ao município - Geral - Histórico - ' . City::find($v_CityAccessGeneral->city_id)->name;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_History' => $v_CityAccessGeneral->revisionHistory]);
	}

	public function deleteCityAccessGeneral($p_Id)
	{
		$v_Query = CityAccessGeneral::where('id',$p_Id);
		if(UserType::isMunicipio())
			$v_Query->where('city_id', Auth::user()->city_id);
		else if(UserType::isCircuito())
			$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
		$v_Item = $v_Query->firstOrFail();
		if($v_Item){
			$v_Item->equipe_responsavel_observacao = 'DELETED';
			$v_Item->save();
			$v_Query->delete();
		}
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| A2.2 - CityAccess
	|--------------------------------------------------------------------------
	*/

	public function getCityAccess()
	{
		return view('admin.moduleA.cityAccess.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTCityAccess()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_Type = $v_Columns[1]['search']['value'];
		$v_City = $v_Columns[2]['search']['value'];
		$v_CreatedAt = $v_Columns[3]['search']['value'];
		$v_UpdatedAt = $v_Columns[4]['search']['value'];
		$v_Status = $v_Columns[5]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return CityAccess::getDT($v_Name, $v_Type, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCityAccess($p_Id = null)
	{
		if($p_Id != null)
		{
			$v_Query = CityAccess::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_CityAccess = $v_Query->firstOrFail();
		}
		else
			$v_CityAccess = null;

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleA.cityAccess.edit')->with(['p_Cities' => $v_Cities,
		                                                    'p_CityAccess' => $v_CityAccess,
		                                                    'p_Types' => Type::getInventoryTypeListByName('A2')]);
	}

	public function postCityAccess()
	{
		CityAccess::post(Input::get('id'), Input::get('formulario'));
		return redirect(url('/admin/inventario/meios-acesso'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getCityAccessHistory($p_Id)
	{
		$v_CityAccess = CityAccess::findOrFail($p_Id);
		$v_Title = 'A2.2 - Meios de acesso ao município - Histórico - ' . City::find($v_CityAccess->city_id)->name;
		$v_Subtitle = 'Tipo: ' . $v_CityAccess->tipo . ' - Nome: ' . $v_CityAccess->nome;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_CityAccess->revisionHistory]);
	}

	public function deleteCityAccess($p_Id)
	{
		$v_Query = CityAccess::where('id',$p_Id);
		if(UserType::isMunicipio())
			$v_Query->where('city_id', Auth::user()->city_id);
		else if(UserType::isCircuito())
			$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
		$v_Item = $v_Query->firstOrFail();
		if($v_Item){
			$v_Item->equipe_responsavel_observacao = 'DELETED';
			$v_Item->save();
			$v_Query->delete();
		}
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| A4 - CitySecuritySystem
	|--------------------------------------------------------------------------
	*/

	public function getCitySecuritySystems()
	{
		return view('admin.moduleA.citySecuritySystem.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTCitySecuritySystems()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return CitySecuritySystem::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCitySecuritySystem($p_Id = null)
	{
		if($p_Id != null)
		{
			$v_Query = CitySecuritySystem::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_SecuritySystem = $v_Query->firstOrFail();
		}
		else
			$v_SecuritySystem = null;

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleA.citySecuritySystem.edit')->with(['p_Cities' => $v_Cities,
		                                                            'p_SecuritySystem' => $v_SecuritySystem,
		                                                            'p_Types' => Type::getInventoryTypeListByName('A4'),
		                                                            'p_EconomicActivities' => EconomicActivity::getOptionalList()]);
	}

	public function postCitySecuritySystem()
	{
		CitySecuritySystem::post(Input::get('id'), Input::get('formulario'));
		return redirect(url('/admin/inventario/sistemas-seguranca'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getCitySecuritySystemHistory($p_Id)
	{
		$v_CitySecuritySystem = CitySecuritySystem::findOrFail($p_Id);
		$v_Title = 'A4 - Sistemas de segurança - Histórico - ' . City::find($v_CitySecuritySystem->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_CitySecuritySystem->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_CitySecuritySystem->revisionHistory]);
	}

	public function deleteCitySecuritySystem($p_Id)
	{
		$v_Query = CitySecuritySystem::where('id',$p_Id);
		if(UserType::isMunicipio())
			$v_Query->where('city_id', Auth::user()->city_id);
		else if(UserType::isCircuito())
			$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
		$v_Item = $v_Query->firstOrFail();
		if($v_Item){
			$v_Item->equipe_responsavel_observacao = 'DELETED';
			$v_Item->save();
			$v_Query->delete();
		}
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| A5 - CityHospitalSystem
	|--------------------------------------------------------------------------
	*/

	public function getCityHospitalSystems()
	{
		return view('admin.moduleA.cityHospitalSystem.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTCityHospitalSystems()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return CityHospitalSystem::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCityHospitalSystem($p_Id = null)
	{
		if($p_Id != null)
		{
			$v_Query = CityHospitalSystem::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_HospitalSystem = $v_Query->firstOrFail();
		}
		else
			$v_HospitalSystem = null;

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleA.cityHospitalSystem.edit')->with(['p_Cities' => $v_Cities,
		                                                            'p_HospitalSystem' => $v_HospitalSystem,
		                                                            'p_Types' => Type::getInventoryTypeListByName('A5'),
		                                                            'p_EconomicActivities' => EconomicActivity::getOptionalList()]);
	}

	public function postCityHospitalSystem()
	{
		CityHospitalSystem::post(Input::get('id'), Input::all());
		return redirect(url('/admin/inventario/sistemas-hospitalares'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getCityHospitalSystemHistory($p_Id)
	{
		$v_CityHospitalSystem = CityHospitalSystem::findOrFail($p_Id);
		$v_Title = 'A5 - Sistemas hospitalares - Histórico - ' . City::find($v_CityHospitalSystem->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_CityHospitalSystem->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_CityHospitalSystem->revisionHistory]);
	}

	public function deleteCityHospitalSystem($p_Id)
	{
		$v_Query = CityHospitalSystem::where('id',$p_Id);
		if(UserType::isMunicipio())
			$v_Query->where('city_id', Auth::user()->city_id);
		else if(UserType::isCircuito())
			$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
		$v_Item = $v_Query->firstOrFail();
		if($v_Item){
			$v_Item->equipe_responsavel_observacao = 'DELETED';
			$v_Item->save();
			$v_Query->delete();
		}
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| A7 - CityOtherService
	|--------------------------------------------------------------------------
	*/

	public function getCityOtherServices()
	{
		return view('admin.moduleA.cityOtherService.list')->with(['p_Status' => RevisionStatus::getList()]);
	}

	public function getDTCityOtherServices()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_CreatedAt = $v_Columns[2]['search']['value'];
		$v_UpdatedAt = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return CityOtherService::getDT($v_Name, $v_City, $v_CreatedAt, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCityOtherService($p_Id = null)
	{
		if($p_Id != null)
		{
			$v_Query = CityOtherService::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_OtherService = $v_Query->firstOrFail();
		}
		else
			$v_OtherService = null;

		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = City::getList();

		return view('admin.moduleA.cityOtherService.edit')->with(['p_Cities' => $v_Cities,
		                                                          'p_OtherService' => $v_OtherService,
		                                                          'p_Types' => Type::getInventoryTypeListByName('A7'),
		                                                          'p_EconomicActivities' => EconomicActivity::getOptionalList()]);
	}

	public function postCityOtherService()
	{
		CityOtherService::post(Input::get('id'), Input::get('formulario'));
		return redirect(url('/admin/inventario/outros-servicos'))->with('message', 'Operação realizada com sucesso.');
	}

	public function getCityOtherServiceHistory($p_Id)
	{
		$v_CityOtherService = CityOtherService::findOrFail($p_Id);
		$v_Title = 'A7 - Outros serviços - Histórico - ' . City::find($v_CityOtherService->city_id)->name;
		$v_Subtitle = 'Nome: ' . $v_CityOtherService->nome_fantasia;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_Subtitle' => $v_Subtitle,
		                                         'p_History' => $v_CityOtherService->revisionHistory]);
	}

	public function deleteCityOtherService($p_Id)
	{
		$v_Query = CityOtherService::where('id',$p_Id);
		if(UserType::isMunicipio())
			$v_Query->where('city_id', Auth::user()->city_id);
		else if(UserType::isCircuito())
			$v_Query->whereIn('city_id', TouristicCircuitCities::getUserCircuitCities());
		$v_Item = $v_Query->firstOrFail();
		if($v_Item){
			$v_Item->equipe_responsavel_observacao = 'DELETED';
			$v_Item->save();
			$v_Query->delete();
		}
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}
}

