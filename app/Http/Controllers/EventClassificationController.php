<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\EventClassification;

class EventClassificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(EventClassification $eventclassification)
    {
       $eventclassifications = $eventclassification->all();
        return view('admin.properties.eventClassification.list', compact('eventclassifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.properties.eventClassification.edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, EventClassification $eventclassifications)
    {
        $form = $request->except('_token');

        /**Trata datas vindas do formulário**/
        if ($form['initial_date'] != '' && $form['final_date'] != ''){
            $form['initial_date'] = conv_date($form['initial_date']);
            $form['final_date'] = conv_date($form['final_date']);
            
            /***Verifica se o período de exibição não conflita com outro existente*/
            if (count($eventclassifications
                                ->where('initial_date', '<=' , $form['initial_date'] )
                                ->where('final_date' , '>=' , $form['initial_date'])
                                ->where('public', '1')

                                ->orWhere('initial_date', '<=' , $form['final_date'] )
                                ->where('final_date' , '>=' , $form['final_date'])
                                ->where('public', '1')
                                
                                
                                ->orWhere('initial_date', '>=' , $form['initial_date'] )
                                ->where('initial_date' , '<=' , $form['final_date'])
                                ->where('public', '1')

                                ->orWhere('final_date', '>=' , $form['initial_date'] )
                                ->where('final_date' , '<=' , $form['final_date'])
                                ->where('public', '1')

                                ->get()) > 0){
                                    /**Converte data novamente */
                                    $form['initial_date'] = conv_date($form['initial_date']);
                                    $form['final_date'] = conv_date($form['final_date']);

                                    $old = $form;
                                    return view('admin.properties.eventClassification.edit', compact('old'))->with('messageErro', 'Período de exibição conflitante!');
                                }
        } else {
            $form['initial_date'] = '';
            $form['final_date'] = '';
            $form['public'] = 0;
        }


        $status = $eventclassifications->create($form);

        if ($status) {
            return redirect(url('/admin/eventos/classificacao'))->with('message', 'Classificação salva com sucesso!');
        } else {
            //Não será exibido, o Laravel exibirá mensagem de erro!
            return redirect(url('/admin/eventos/classificacao'))->with('message', 'Erro ao salvar classificação!');;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(EventClassification $eventclassifications, $id)
    {
        $eventclassification = $eventclassifications->find($id);

        $eventclassification->initial_date = conv_date($eventclassification->initial_date);
        $eventclassification->final_date = conv_date($eventclassification->final_date);

        //dd($eventclassification);
        return view('admin.properties.eventClassification.edit', compact('eventclassification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventClassification $eventclassifications, $id)
    {
        $eventclassification = $eventclassifications->find($id);

        $form = $request->except('_token');

        /**Trata datas vindas do formulário**/
        if ($form['initial_date'] != '' && $form['final_date'] != ''){
            $form['initial_date'] = conv_date($form['initial_date']);
            $form['final_date'] = conv_date($form['final_date']);
            
            /***Verifica se o período de exibição não conflita com outro existente*/
            if (count($eventclassifications
                                ->where('initial_date', '<=' , $form['initial_date'] )
                                ->where('final_date' , '>=' , $form['initial_date'])
                                ->where('public', '1')
                                ->where('id','<>', $form['id'])

                                ->orWhere('initial_date', '<=' , $form['final_date'] )
                                ->where('final_date' , '>=' , $form['final_date'])
                                ->where('public', '1')
                                ->where('id','<>', $form['id'])
                                
                                ->orWhere('initial_date', '>=' , $form['initial_date'] )
                                ->where('initial_date' , '<=' , $form['final_date'])
                                ->where('public', '1')
                                ->where('id','<>', $form['id'])

                                ->orWhere('final_date', '>=' , $form['initial_date'] )
                                ->where('final_date' , '<=' , $form['final_date'])
                                ->where('public', '1')
                                ->where('id','<>', $form['id'])

                                ->get()) > 0){
                                    /**Converte data novamente */
                                    $form['initial_date'] = conv_date($form['initial_date']);
                                    $form['final_date'] = conv_date($form['final_date']);

                                    $old = $form;
                                    return view('admin.properties.eventClassification.edit', compact('old'))->with('messageErro', 'Período de exibição conflitante!');
                                }
        } else {
            $form['initial_date'] = '';
            $form['final_date'] = '';
            $form['public'] = 0;
        }

        $status = $eventclassification->update($form);
        
        if ($status) {
            return redirect(url('/admin/eventos/classificacao'))->with('message', 'Classificação salva com sucesso!');
        } else {
            //Não será exibido, o Laravel exibirá mensagem de erro!
            return redirect(url('/admin/eventos/classificacao'))->with('message', 'Erro ao salvar classificação!');;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
