<?php

/* lida com as requisições de listagem, edição, criação e remoção de circuitos turísticos, e da listagem, edição e criação de regiões. */

namespace App\Http\Controllers;

use App\City;
use App\Region;
use App\TouristicCircuit;
use App\TouristicCircuitCities;
use App\User;
use \Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Validator;

class CircuitController extends BaseController
{
	public static $m_CircuitStatus = array
	(
		'0' => 'Não associado',
		'1' => 'Não certificado',
		'2' => 'Em processo',
		'3' => 'Certificado'
	);

	/*
	|--------------------------------------------------------------------------
	| TouristicCircuit
	|--------------------------------------------------------------------------
	*/

	public function getCircuits()
	{
		return view('admin.touristicCircuit.list')->with(['p_Cities' => City::getList()]);
	}

	public function getDTCircuits()
	{
		$v_Columns = Input::get('columns');
		$v_Name = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_Responsible = $v_Columns[2]['search']['value'];
		$v_Status = $v_Columns[3]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return TouristicCircuit::getDT($v_Name, $v_City, $v_Responsible, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editCircuit($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Circuit = TouristicCircuit::findOrFail($p_Id);
			$v_CircuitCities = TouristicCircuitCities::getSelectedCities($p_Id);
		}
		else
		{
			$v_Circuit = null;
			$v_CircuitCities = [];
		}

		return view('admin.touristicCircuit.edit')->with(['p_Cities' => City::getList(),
		                                                  'p_CircuitCities' => $v_CircuitCities,
		                                                  'p_Users' => User::getList(),
		                                                  'p_Circuit' => $v_Circuit]);
	}

    public function postCircuit()
    {
	    TouristicCircuit::post(Input::all());
        return redirect(url('/admin/circuitos'))->with('message', 'Circuito salvo com sucesso!');
    }

	public function getCircuitCities()
	{
		return TouristicCircuitCities::getCircuitCities(Input::get('id'));
	}

	public function getCityCircuits($p_CityId)
	{
		return TouristicCircuitCities::getCityCircuit($p_CityId);
	}

	public function deleteCircuit($p_Id)
	{
		TouristicCircuit::where('id', $p_Id)->delete();
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| Region
	|--------------------------------------------------------------------------
	*/

	public function getRegions()
	{
		return view('admin.properties.region.list')->with(['p_Regions' => Region::all()]);
	}

	public function editRegion($p_Id = null)
	{
		if ($p_Id != null)
			$v_Region = Region::findOrFail($p_Id);
		else
			$v_Region = null;
		return view('admin.properties.region.edit')->with(['p_Region' => $v_Region]);
	}

	public function postRegion()
	{
		$v_Id = Input::get('id');
		$v_Validator = Validator::make(Input::all(), $v_Id == null ? Region::$m_Rules : Region::$m_RulesEdit);
		if($v_Validator->fails())
			return redirect()->back()->withInput()->withErrors($v_Validator);

		$v_Id = Input::get('id');
		$v_Name = Input::get('nome');
		Region::post($v_Id, $v_Name);
		return redirect(url('/admin/regioes'))->with('message', 'Operação realizada com sucesso.');
	}
}