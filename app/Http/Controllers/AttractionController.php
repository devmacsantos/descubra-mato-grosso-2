<?php

/* trata as requisições do site público do sistema relativas a atrações, trade e roteiros turísticos. Além disso lida com as requisições de listagem, edição, criação, ativação e desativação dos roteiros turísticas. */

namespace App\Http\Controllers;

use App\Attraction;
use App\AttractionPhoto;
use App\CuisineNationality;
use App\CuisineRegion;
use App\CuisineService;
use App\CuisineTheme;
use App\Destination;
use App\ItemsPageDescriptions;
use App\PaymentMethod;
use App\TouristicRoute;
use App\TouristicRouteDay;
use App\TouristicRouteDestinations;
use App\TouristicRouteHashtag;
use App\TouristicRoutePhoto;
use App\TripAttractions;
use App\TripCategory;
use App\TripTouristicRoutes;
use App\TripType;
use App\Type;
use App\City;
use \Illuminate\Support\Facades\Input;
use Validator;

class AttractionController extends BaseController
{
	/*
	|--------------------------------------------------------------------------
	| Attraction
	|--------------------------------------------------------------------------
	*/

	public function getParks($p_Lang)
	{
		\App::setLocale($p_Lang);
		$v_Content = ItemsPageDescriptions::getByLanguage($p_Lang, 'parques');
		$v_Parks = Attraction::getParks(25);

		$v_TripCategories = TripCategory::getList($p_Lang);
		$v_TripTypes = TripType::getFilterList();

		return view('public.parks')->with(['p_Content' => $v_Content,
		                                   'p_Parks' => $v_Parks,
		                                   'p_TripCategories' => $v_TripCategories,
		                                   'p_TripTypes' => $v_TripTypes,
		                                   'p_Language' => $p_Lang]);
	}

	public function getPublicParksByTripType($p_Lang)
	{
		\App::setLocale($p_Lang);
		$v_TripTypeId = Input::get('trip_type_id');
		if($v_TripTypeId != null)
		{
			$v_Response['error'] = 'ok';
			$v_Response['data'] = TripAttractions::getTripParks($v_TripTypeId, 24);
		}
		else
			$v_Response['error'] = trans('whereToGo.choose_trip_type');

		return $v_Response;
	}

	public function discoverAttraction($p_Lang, $slug)
	{
		\App::setLocale($p_Lang);

		$v_Attraction = Attraction::where('slug',$slug)
									->first();
		//print_r($v_Attraction);
		//exit();

		
		if ($v_Attraction){
			//print_r($v_Attraction['city_id']);
			//exit();
			$city = City::where('id', $v_Attraction->city_id)->first();
			//print_r($v_Attraction['city_id']);
			//exit();
			return redirect(url('/pt/atracoes/'.$city->slug.'/'.$v_Attraction['slug']),301);
		} else{
			$url = url('/pt/destinos/'.$slug);
			//print_r($url);
			//exit();			
			return redirect($url);
		}

	}

	public function byAttractionType($p_Lang, $c_slug,$type,$p_Slug)
	{
		\App::setLocale($p_Lang);
		$v_Attraction = Attraction::getBySlug($p_Slug);

		$m_slug = get_city_slug($v_Attraction->city_id);
		if ($m_slug != $c_slug){
			return redirect(url('/pt/atracoes/'.$m_slug.'/'.$v_Attraction->slug));
		}
                //dd($v_Attraction);
                
		if($v_Attraction->parque)
		{
			$v_CityAttractions = Attraction::getCityMainAttractions($v_Attraction->city_id, 5, $v_Attraction->id);
			$v_OtherCitiesAttractions = Attraction::getOtherCitiesAttractions($v_Attraction->city_id, $v_Attraction->latitude, $v_Attraction->longitude, 4);

			return view('public.park')->with(['p_Content' => $v_Attraction,
			                                  'p_Destination' => Destination::getDestinationByCityId($v_Attraction->city_id),
			                                  'p_PhotoGallery' => AttractionPhoto::getPhotoGallery($v_Attraction->id),
			                                  'p_CityAttractions' => $v_CityAttractions,
			                                  'p_OtherCitiesAttractions' => $v_OtherCitiesAttractions,
			                                  'p_AccomodationList' => Type::getPublicInventoryTypeListByName('B1', $p_Lang),
			                                  'p_FoodDrinkList' => Type::getPublicInventoryTypeListByName('B2', $p_Lang),
			                                  'p_ServiceList' => Type::getPublicInventoryTypeServiceList($p_Lang),
			                                  'p_RecreationList' => Type::getPublicInventoryTypeListByName('B6', $p_Lang),
			                                  'p_Accomodations' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B1']),
			                                  'p_FoodDrinks' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B2']),
			                                  'p_Services' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B3', 'B4']),
			                                  'p_Recreations' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B6']),
			                                  'p_Language' => $p_Lang]);
		}
		else
			return view('public.attraction')->with(['p_Content' => $v_Attraction,
													'p_Destination' => Destination::getDestinationByCityId($v_Attraction->city_id),
			                                        'p_PhotoGallery' => AttractionPhoto::getPhotoGallery($v_Attraction->id, 6),
			                                        'p_PaymentMethods' => PaymentMethod::getLanguageList($p_Lang),
			                                        'p_CuisineNationalities' => CuisineNationality::getLanguageList($p_Lang),
			                                        'p_CuisineServices' => CuisineService::getLanguageList($p_Lang),
			                                        'p_CuisineThemes' => CuisineTheme::getLanguageList($p_Lang),
			                                        'p_CuisineRegions' => CuisineRegion::getLanguageList($p_Lang),
			                                        'p_Language' => $p_Lang]);
	}







	public function getPublicAttraction($p_Lang, $c_slug, $p_Slug)
	{
		\App::setLocale($p_Lang);
		$v_Attraction = Attraction::getBySlug($p_Slug);
		//print(gettype($v_Attraction));exit();
		if (gettype($v_Attraction)=="NULL"){
			return redirect(url('/pt/destinos/'.$c_slug));
			//print_r($p_Slug);exit();
		}
		$m_slug = get_city_slug($v_Attraction->city_id);
		if ($m_slug != $c_slug){
			return redirect(url('/pt/atracoes/'.$m_slug.'/'.$v_Attraction->type_slug.'/'.$v_Attraction->slug));
		}
                //dd($v_Attraction);
                
		if($v_Attraction->parque)
		{
			$v_CityAttractions = Attraction::getCityMainAttractions($v_Attraction->city_id, 5, $v_Attraction->id);
			$v_OtherCitiesAttractions = Attraction::getOtherCitiesAttractions($v_Attraction->city_id, $v_Attraction->latitude, $v_Attraction->longitude, 4);

			return view('public.park')->with(['p_Content' => $v_Attraction,
			                                  'p_Destination' => Destination::getDestinationByCityId($v_Attraction->city_id),
			                                  'p_PhotoGallery' => AttractionPhoto::getPhotoGallery($v_Attraction->id),
			                                  'p_CityAttractions' => $v_CityAttractions,
			                                  'p_OtherCitiesAttractions' => $v_OtherCitiesAttractions,
			                                  'p_AccomodationList' => Type::getPublicInventoryTypeListByName('B1', $p_Lang),
			                                  'p_FoodDrinkList' => Type::getPublicInventoryTypeListByName('B2', $p_Lang),
			                                  'p_ServiceList' => Type::getPublicInventoryTypeServiceList($p_Lang),
			                                  'p_RecreationList' => Type::getPublicInventoryTypeListByName('B6', $p_Lang),
			                                  'p_Accomodations' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B1']),
			                                  'p_FoodDrinks' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B2']),
			                                  'p_Services' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B3', 'B4']),
			                                  'p_Recreations' => Attraction::getDestinationTrade($v_Attraction->city_id, ['B6']),
			                                  'p_Language' => $p_Lang]);
		}
		else
			return view('public.attraction')->with(['p_Content' => $v_Attraction,
													'p_Destination' => Destination::getDestinationByCityId($v_Attraction->city_id),
			                                        'p_PhotoGallery' => AttractionPhoto::getPhotoGallery($v_Attraction->id, 6),
			                                        'p_PaymentMethods' => PaymentMethod::getLanguageList($p_Lang),
			                                        'p_CuisineNationalities' => CuisineNationality::getLanguageList($p_Lang),
			                                        'p_CuisineServices' => CuisineService::getLanguageList($p_Lang),
			                                        'p_CuisineThemes' => CuisineTheme::getLanguageList($p_Lang),
			                                        'p_CuisineRegions' => CuisineRegion::getLanguageList($p_Lang),
			                                        'p_Language' => $p_Lang]);
	}



	public function byTrade($p_Lang, $c_slug, $trade, $p_Slug)
	{
		

		\App::setLocale($p_Lang);
		$v_Attraction = Attraction::getBySlug($p_Slug, true);



		return view('public.trade')->with(['p_Content' => $v_Attraction,
		                                   'p_PhotoGallery' => AttractionPhoto::getPhotoGallery($v_Attraction->id, 6),
		                                   'p_PaymentMethods' => PaymentMethod::getLanguageList($p_Lang),
		                                   'p_CuisineNationalities' => CuisineNationality::getLanguageList($p_Lang),
		                                   'p_CuisineServices' => CuisineService::getLanguageList($p_Lang),
		                                   'p_CuisineThemes' => CuisineTheme::getLanguageList($p_Lang),
		                                   'p_CuisineRegions' => CuisineRegion::getLanguageList($p_Lang),
		                                   'p_Language' => $p_Lang]);
	}





	public function getTrade($p_Lang, $c_slug, $p_Slug)
	{
		

		\App::setLocale($p_Lang);
		$v_Attraction = Attraction::getBySlug($p_Slug, true);

		if (gettype($v_Attraction)=="NULL"){
			return redirect(url('/pt/destinos/'.$c_slug));
			//print_r($p_Slug);exit();
		}

		return view('public.trade')->with(['p_Content' => $v_Attraction,
		                                   'p_PhotoGallery' => AttractionPhoto::getPhotoGallery($v_Attraction->id, 6),
		                                   'p_PaymentMethods' => PaymentMethod::getLanguageList($p_Lang),
		                                   'p_CuisineNationalities' => CuisineNationality::getLanguageList($p_Lang),
		                                   'p_CuisineServices' => CuisineService::getLanguageList($p_Lang),
		                                   'p_CuisineThemes' => CuisineTheme::getLanguageList($p_Lang),
		                                   'p_CuisineRegions' => CuisineRegion::getLanguageList($p_Lang),
		                                   'p_Language' => $p_Lang]);
	}

	public function discoverTrade($p_Lang, $slug)
	{
		\App::setLocale($p_Lang);

		$v_Attraction = Attraction::where('slug',$slug)->first();
		//print_r($v_Attraction);
		//exit();

		
		if ($v_Attraction){
			//print_r($v_Attraction['city_id']);
			//exit();
			$city = City::where('id', $v_Attraction->city_id)->first();
			//print_r($v_Attraction['city_id']);
			//exit();
			return redirect(url('/pt/apoio/'.$city->slug.'/'.$v_Attraction->slug),301);
		} else{
			$url = url('/pt/destinos/'.$slug);
			//print_r($url);
			//exit();			
			return redirect($url);
		}

	}

	/*
	|--------------------------------------------------------------------------
	| TouristicRoute
	|--------------------------------------------------------------------------
	*/

	public function getPublicTouristicRoutes($p_Lang)
	{
		\App::setLocale($p_Lang);
		$v_Content = ItemsPageDescriptions::getByLanguage($p_Lang, 'roteiros');
		$v_TouristicRoutes = TouristicRoute::getRoutes(25);

		$v_TripCategories = TripCategory::getList($p_Lang);
		$v_TripTypes = TripType::getFilterList();
		return view('public.touristicRoutes')->with(['p_Content' => $v_Content,
		                                             'p_TouristicRoutes' => $v_TouristicRoutes,
		                                             'p_TripCategories' => $v_TripCategories,
		                                             'p_TripTypes' => $v_TripTypes,
		                                             'p_Language' => $p_Lang]);
	}

	public function getPublicTouristicRoutesByTripType($p_Lang)
	{
		$v_TripTypeId = Input::get('trip_type_id');
		$v_Duration = Input::get('duracao');
		if(!empty($v_TripTypeId))
		{
			$v_Response['error'] = 'ok';
			$v_Response['data'] = TripTouristicRoutes::getTripTouristicRoutesWithDuration($v_TripTypeId, $v_Duration, 24);
		}
		else if(!empty($v_Duration))
		{
			$v_Response['error'] = 'ok';
			$v_Response['data'] = TouristicRoute::getRoutesByDuration($v_Duration, 24);
		}
		else
			$v_Response['error'] = trans('whereToGo.choose_trip_type');

		return $v_Response;
	}

	public function getPublicTouristicRoutesDurations($p_Lang)
	{
		\App::setLocale($p_Lang);
		$v_Duration = Input::get('d');
		$v_TouristicRoutes = TouristicRoute::getRoutesByDuration($v_Duration, 44);
		return view('public.touristicRoutesDuration')->with(['p_TouristicRoutes' => $v_TouristicRoutes,
		                                                     'p_Duration' => $v_Duration,
		                                                     'p_Language' => $p_Lang]);
	}

	public function getPublicTouristicRoute($p_Lang, $p_Slug)
	{
		\App::setLocale($p_Lang);
		$v_TouristicRoute = TouristicRoute::getRoute($p_Slug);
		$v_Destinations = TouristicRouteDestinations::getTouristicRouteDestinations($v_TouristicRoute->id);
		$v_Attractions = Attraction::getTouristicRouteAttractions($v_Destinations, 16);

		return view('public.touristicRoute')->with(['p_Content' => $v_TouristicRoute,
		                                            'p_PhotoGallery' => TouristicRoutePhoto::getPhotoGallery($v_TouristicRoute->id),
		                                            'p_Destinations' => $v_Destinations,
		                                            'p_Attractions' => $v_Attractions,
		                                            'p_MapPhoto' => TouristicRoutePhoto::getMapPhoto($v_TouristicRoute->id),
		                                            'p_TestimonialPhoto' => TouristicRoutePhoto::getTestimonialPhoto($v_TouristicRoute->id),
		                                            'p_DayDescriptions' => TouristicRouteDay::getDays($v_TouristicRoute->id),
		                                            'p_Language' => $p_Lang]);
	}

	public function getTouristicRoutes()
	{
		return view('admin.touristicRoute.list');
	}

	public function getDTTouristicRoute()
	{
		$v_Columns = Input::get('columns');
		$v_CreatedAt = $v_Columns[0]['search']['value'];
		$v_UpdatedAt = $v_Columns[1]['search']['value'];
		$v_Name = $v_Columns[2]['search']['value'];
		$v_Duration = $v_Columns[3]['search']['value'];
		$v_Status = $v_Columns[4]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return TouristicRoute::getDT($v_CreatedAt, $v_UpdatedAt, $v_Name, $v_Duration, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editTouristicRoute($p_Id = null)
	{
		if ($p_Id != null)
		{
			$v_Route = TouristicRoute::findOrFail($p_Id);
			$v_RouteDestinations = TouristicRouteDestinations::getSelectedDestinations($p_Id);
			$v_CoverPhoto = TouristicRoutePhoto::getCoverPhoto($p_Id);
			$v_MapPhoto = TouristicRoutePhoto::getMapPhoto($p_Id);
			$v_TestimonialPhoto = TouristicRoutePhoto::getTestimonialPhoto($p_Id);
			$v_PhotoGallery = TouristicRoutePhoto::getPhotoGallery($p_Id);
			$v_Hashtags = TouristicRouteHashtag::getList($p_Id);
			$v_Days = TouristicRouteDay::getDays($p_Id);
			$v_SelectedTripTypes = TripTouristicRoutes::getSelectedTypes($p_Id);
		}
		else
		{
			$v_Route = null;
			$v_RouteDestinations = [];
			$v_CoverPhoto = null;
			$v_MapPhoto = null;
			$v_TestimonialPhoto = null;
			$v_PhotoGallery = [];
			$v_Hashtags = [];
			$v_Days = [];
			$v_SelectedTripTypes = [];
		}

		return view('admin.touristicRoute.edit')->with(['p_Route' => $v_Route,
		                                                'p_Destinations' => Destination::getList(),
		                                                'p_Days' => $v_Days,
		                                                'p_RouteDestinations' => $v_RouteDestinations,
		                                                'p_MapPhoto' => $v_MapPhoto,
		                                                'p_CoverPhoto' => $v_CoverPhoto,
		                                                'p_TestimonialPhoto' => $v_TestimonialPhoto,
		                                                'p_PhotoGallery' => $v_PhotoGallery,
		                                                'p_Hashtags' => $v_Hashtags,
		                                                'p_TripTypes' => TripType::getList(),
		                                                'p_SelectedTripTypes' => $v_SelectedTripTypes]);
	}

	public function postTouristicRoute()
	{
		$v_Id = Input::get('id');

		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		TouristicRoute::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::all());

		return redirect(url('/admin/roteiros'))->with('message', 'Operação realizada com sucesso.');
	}

	public function toggleActiveTouristicRoute($p_Id)
	{
		$v_Route = TouristicRoute::findOrFail($p_Id);
		$v_Route->active = !$v_Route->active;
		$v_Route->save();
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	public function deleteTouristicRoute($p_Id)
	{
		TouristicRoute::deleteTouristicRoute($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}
}

