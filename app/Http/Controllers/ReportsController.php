<?php

namespace App\Http\Controllers;

use App\AccomodationService;
use App\City;
use App\CityAccess;
use App\CityAccessGeneral;
use App\CityHospitalSystem;
use App\CityInfo;
use App\CityOtherService;
use App\CitySecuritySystem;
use App\ContemporaryRealization;
use App\CulturalAttraction;
use App\Destination;
use App\DonatedMedia;
use App\EconomicActivityAttraction;
use App\EventCategory;
use App\Event;
use App\EventService;
use App\EventByDates;
use App\FoodDrinksService;
use App\GastronomicPrimaryProduct;
use App\GastronomicTransformedProduct;
use App\GastronomicTypicalDish;
use App\NaturalAttraction;
use App\OtherTourismService;
use App\Parameter;
use App\PermanentEvent;
use App\RecreationService;
use App\Region;
use App\RevisionStatus;
use App\TourismAgencyService;
use App\TourismTransportationService;
use App\TouristicCircuit;
use App\Type;
use App\EventClassification;
use Barryvdh\Snappy\Facades\SnappyPdf;
use \Illuminate\Support\Facades\Input;
use Validator;
use App\ReportCover;

class ReportsController extends BaseController
{
    public function generateDonatedMediaReport()
    {
        DonatedMedia::getReport(Input::all());
    }

    public function getDonatedMediaTerm($p_Id)
    {
        $v_PDF = SnappyPdf::loadView('admin.donations.donationTerm', ['p_Donation' => DonatedMedia::findOrFail($p_Id)]);
        return $v_PDF->download('TermoDoacaoMidia.pdf');
    }

    public function getInventoryReport()
    {
        return view('admin.reports.inventoryNumericMap')->with(['p_Cities' => City::getList(),
                                                                'p_Circuits' => TouristicCircuit::getList(),
                                                                'p_Regions' => [''=>''] + Region::getList()]);
    }

    public function generateInventoryNumericMap()
    {
        //TODO falta filtro de situação(pq não é mais status ativo/inativo)
        //TODO conferir filtro de "exibido para o publico", no momento só afeta as contagens, contando só itens dos municipios que estão publicados

        $v_PDF = SnappyPdf::loadView('admin.reports.inventoryNumericMapReport', ['p_Cities' => City::getCitiesInventoryReport(Input::all()),
                                                                                 'p_Filters' => Input::all()]);
        return $v_PDF->setOrientation('landscape')->download('RelatorioInventario-MapaNumerico.pdf');
    }

    public function getEventsReport()
    {
        return view('admin.reports.events')->with(['p_Cities' => City::getList(),
                                                   'p_Circuits' => TouristicCircuit::getList(),
                                                   'p_Regions' => [''=>''] + Region::getList(),
                                                   'p_Categories' => ['' => ''] + EventCategory::getList(),
                                                   'p_Classification' => ['' => ''] + EventClassification::getList()]);
    }

    public function generateEventsReport()
    {
        Event::getReport(Input::all());
    }


    public function getCalendarEventsReport(ReportCover $reportcover)
    {
        $reportcovers = $reportcover->lists('label', 'id')->toArray();
        return view('admin.reports.calendarEvents')->with(['p_Cities' => City::getList(),
                                                           'p_Circuits' => TouristicCircuit::getList(),
                                                           'p_Regions' => [''=>''] + Region::getList(),
                                                           'p_Categories' => EventCategory::getList(),
                                                           'p_Classification' => EventClassification::getList(),
                                                           'p_ReportCovers' => $reportcovers]);
    }

    public function generateCalendarEventsReport(ReportCover $reportcover)
    {
        ini_set('memory_limit', '1G');
        if (strpos(url(), 'localhost') === false)
            $v_Footer = url('/admin/relatorios/calendario-eventos/footer');
        else
            $v_Footer = 'http://localhost/minasgerais/admin/relatorios/calendario-eventos/footer';

        //$v_Events = Event::getCalendarReportData(Input::all());
        $v_Events = EventByDates::getCalendarReportData(Input::all());
        
        
        
        //Dados de Capa e cabeçalho 
        $p_reportcover = $reportcover->find(Input::all()['report_cover']);
        
        $v_PDF = SnappyPdf::loadView('admin.reports.calendarEventsReport', ['p_Events' => $v_Events,
                                                                            'p_EventCalendarLastPage' =>  $p_reportcover->message])
                          ->setOption('header-html', view('admin.reports.calendarEventsReportHeader', ['p_EventCalendarHeader' =>  $p_reportcover->pheader]));

        if(count($v_Events) < 300){
            $v_PDF->setOption('footer-html', $v_Footer);
        }

        $v_Cover = $p_reportcover->pcover;
        if ($v_Cover != null){
            $v_PDF->setOption('cover', view('admin.reports.calendarEventsReportCover', ['p_EventCalendarCover' => $v_Cover]));
        }

        return $v_PDF->download('CalendarioEventos.pdf');
    }

    public function getEventCalendarFooter()
    {
        return view('admin.reports.calendarEventsReportFooter')->with(['p_Page' => Input::get('page'),
                                                                       'p_LastPage' => Input::get('topage')]);
    }

    public function getPermanentEvents()
    {
        return view('admin.reports.permanentEvents')->with(['p_Cities' => City::getList(),
                                                            'p_Categories' => EventCategory::getList()]);
    }

    public function generatePermanentEventsReport()
    {
        ini_set('memory_limit', '1G');
        PermanentEvent::getReport(Input::all());
    }

    public function getDestinationsReport()
    {
        return view('admin.reports.destinations')->with(['p_Cities' => City::getList(),
                                                         'p_Circuits' => TouristicCircuit::getList(),
                                                         'p_Regions' => [''=>''] + Region::getList()]);
    }

    public function generateDestinationsReport()
    {
        ini_set('memory_limit', '1G');
        Destination::getReport(Input::all());
    }

    public function getTouristicCircuitsReport()
    {
        return view('admin.reports.touristicCircuits')->with(['p_Circuits' => TouristicCircuit::getList(),
                                                              'p_Regions' => [''=>''] + Region::getList()]);
    }

    public function generateTouristicCircuitsReport()
    {
        return TouristicCircuit::getFullReports(Input::all());
    }

    public function generateTouristicCircuitsSimplifiedReport()
    {
        return TouristicCircuit::getSimplifiedReports(Input::all());
    }

    public function generateTouristicCircuitsContactsReport()
    {
        return TouristicCircuit::getContactsReports(Input::all());
    }

    public function getModuleAReport()
    {
        return view('admin.reports.moduleA')->with(['p_Cities' => City::getList(),
                                                    'p_Circuits' => TouristicCircuit::getList(),
                                                    'p_Regions' => [''=>''] + Region::getList()]);
    }

    public function generateModuleAReport($p_Item)
    {
        ini_set('memory_limit', '1G');
        if($p_Item == 'A1')
            CityInfo::getReport(Input::all());
        else if($p_Item == 'A2.1')
            CityAccessGeneral::getReport(Input::all());
        else if($p_Item == 'A2.2')
            CityAccess::getReport(Input::all());
        else if($p_Item == 'A4')
            CitySecuritySystem::getReport(Input::all());
        else if($p_Item == 'A5')
            CityHospitalSystem::getReport(Input::all());
        else if($p_Item == 'A7')
            CityOtherService::getReport(Input::all());
    }

    public function getModuleBReport()
    {
        $v_Types['B1'] = Type::getInventoryTypeListByName('B1');
        $v_Types['B2'] = Type::getInventoryTypeListByName('B2');
        $v_Types['B3'] = Type::getInventoryTypeListByName('B3');
        $v_Types['B4'] = Type::getInventoryTypeListByName('B4');
        $v_Types['B5'] = Type::getInventoryTypeListByName('B5');
        $v_Types['B6'] = Type::getInventoryTypeListByName('B6');
        $v_Types['B7'] = Type::getInventoryTypeListByName('B7');
        return view('admin.reports.moduleB')->with(['p_Cities' => City::getList(),
                                                    'p_Circuits' => TouristicCircuit::getList(),
                                                    'p_Regions' => [''=>''] + Region::getList(),
                                                    'p_Types' => $v_Types]);
    }

    public function generateModuleBReport($p_Item)
    {
        ini_set('memory_limit', '1G');
        if($p_Item == 'B1')
            AccomodationService::getReport(Input::all());
        else if($p_Item == 'B2')
            FoodDrinksService::getReport(Input::all());
        else if($p_Item == 'B3')
            TourismAgencyService::getReport(Input::all());
        else if($p_Item == 'B4')
            TourismTransportationService::getReport(Input::all());
        else if($p_Item == 'B5')
            EventService::getReport(Input::all());
        else if($p_Item == 'B6')
            RecreationService::getReport(Input::all());
        else if($p_Item == 'B7')
            OtherTourismService::getReport(Input::all());
    }

    public function getModuleCReport()
    {
        $v_Types['C1'] = Type::getInventoryTypeListByName('C1');
        $v_Types['C2'] = Type::getInventoryTypeListByName('C2');
        $v_Types['C3'] = Type::getInventoryTypeListByName('C3');
        $v_Types['C4'] = Type::getInventoryTypeListByName('C4');
        return view('admin.reports.moduleC')->with(['p_Cities' => City::getList(),
                                                    'p_Circuits' => TouristicCircuit::getList(),
                                                    'p_Regions' => [''=>''] + Region::getList(),
                                                    'p_Types' => $v_Types]);
    }

    public function generateModuleCReport($p_Item)
    {
        ini_set('memory_limit', '1G');
        if($p_Item == 'C1')
            NaturalAttraction::getReport(Input::all());
        else if($p_Item == 'C2')
            CulturalAttraction::getReport(Input::all());
        else if($p_Item == 'C3')
            EconomicActivityAttraction::getReport(Input::all());
        else if($p_Item == 'C4')
            ContemporaryRealization::getReport(Input::all());
        else if($p_Item == 'C6.1')
            GastronomicPrimaryProduct::getReport(Input::all());
        else if($p_Item == 'C6.2')
            GastronomicTransformedProduct::getReport(Input::all());
        else if($p_Item == 'C6.3')
            GastronomicTypicalDish::getReport(Input::all());
    }

    public function getInventoryGeoPlanningReport()
    {
        return view('admin.reports.inventoryGeoPlanning')->with(['p_Cities' => City::getList(),
                                                                 'p_Circuits' => TouristicCircuit::getUserList(),
                                                                 'p_Status' => RevisionStatus::getCompleteStatusList(),
                                                                 'p_Regions' => Region::getList()]);
    }

    public function postInventoryGeoPlanningReport()
    {
        $v_ReportData = [];
        $v_ReportData['A4'] = CitySecuritySystem::getGeoReport(Input::all());
        $v_ReportData['A5'] = CityHospitalSystem::getGeoReport(Input::all());
        $v_ReportData['A7'] = CityOtherService::getGeoReport(Input::all());

        $v_ReportData['B1'] = AccomodationService::getGeoReport(Input::all());
        $v_ReportData['B2'] = FoodDrinksService::getGeoReport(Input::all());
        $v_ReportData['B3'] = TourismAgencyService::getGeoReport(Input::all());
        $v_ReportData['B4'] = TourismTransportationService::getGeoReport(Input::all());
        $v_ReportData['B5'] = EventService::getGeoReport(Input::all());
        $v_ReportData['B6'] = RecreationService::getGeoReport(Input::all());
        $v_ReportData['B7'] = OtherTourismService::getGeoReport(Input::all());

        $v_ReportData['C1'] = NaturalAttraction::getGeoReport(Input::all());
        $v_ReportData['C2'] = CulturalAttraction::getGeoReport(Input::all());
        $v_ReportData['C3'] = EconomicActivityAttraction::getGeoReport(Input::all());
        $v_ReportData['C4'] = ContemporaryRealization::getGeoReport(Input::all());

        return view('admin.reports.inventoryGeoPlanning')->with(['p_Cities' => City::getList(),
                                                                 'p_Circuits' => TouristicCircuit::getUserList(),
                                                                 'p_Status' => RevisionStatus::getCompleteStatusList(),
                                                                 'p_Regions' => Region::getList(),
                                                                 'p_ReportData' => $v_ReportData,
                                                                 'p_Filters' => Input::all(),
                                                                 'p_ShowMap' => true]);
    }
}

