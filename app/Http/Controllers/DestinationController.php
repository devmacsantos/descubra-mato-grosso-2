<?php

/* lida com as requisições de listagem, edição, criação, ativação e desativação de destinos do portal. */

namespace App\Http\Controllers;

use App\City;
use App\Destination;
use App\DestinationHashtag;
use App\DestinationPhoto;
use App\Region;
use App\TripDestinations;
use App\TripType;
use \Illuminate\Support\Facades\Input;
use Validator;

class DestinationController extends BaseController
{
    public function getDestinations()
    {
        return view('admin.destination.list');
    }

    public function getDTDestinations()
    {
        $v_Columns = Input::get('columns');
		$v_Type = $v_Columns[0]['search']['value'];
		$v_Circuit = $v_Columns[1]['search']['value'];
	    $v_City = $v_Columns[2]['search']['value'];
	    $v_District = $v_Columns[3]['search']['value'];
	    $v_UpdatedAt = $v_Columns[4]['search']['value'];
	    $v_Status = $v_Columns[5]['search']['value'];
        $v_Order = Input::get('order')[0];
        $v_Start = Input::get('start');
        $v_Length = Input::get('length');
        $v_Draw = Input::get('draw');
        return Destination::getDT($v_Type,  $v_Circuit, $v_City, $v_District, $v_UpdatedAt, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
    }

    public function editDestination($p_Type, $p_Id = null)
    {
	    if($p_Id != null)
	    {
			$v_Destination = Destination::findOrFail($p_Id);
		    $v_CoverPhoto = DestinationPhoto::getCoverPhoto($v_Destination->id);
		    $v_MapPhoto = DestinationPhoto::getMapPhoto($v_Destination->id);
		    $v_TestimonialPhoto = DestinationPhoto::getTestimonialPhoto($v_Destination->id);
		    $v_PhotoGallery = DestinationPhoto::getPhotoGallery($v_Destination->id);
		    $v_Hashtags = DestinationHashtag::getList($v_Destination->id);
		    $v_SelectedTripTypes = TripDestinations::getSelectedTypes($v_Destination->id);
		    $v_Cities = City::getList();
	    }
	    else
	    {
		    $v_Destination = null;
		    $v_CoverPhoto = null;
		    $v_MapPhoto = null;
		    $v_TestimonialPhoto = null;
		    $v_PhotoGallery = [];
		    $v_Hashtags = [];
		    $v_SelectedTripTypes = [];
		    if($p_Type == 'municipio')
			    $v_Cities = City::getListExceptIds(Destination::getCityDestinationsIds());
		    else
			    $v_Cities = City::getList();
	    }

	    return view('admin.destination.edit')->with(['p_Cities' => $v_Cities,
	                                                 'p_Destination' => $v_Destination,
	                                                 'p_Type' => $p_Type,
	                                                 'p_Regions' => [''=>''] + Region::getList(),
	                                                 'p_Hashtags' => $v_Hashtags,
	                                                 'p_MapPhoto' => $v_MapPhoto,
	                                                 'p_CoverPhoto' => $v_CoverPhoto,
	                                                 'p_TestimonialPhoto' => $v_TestimonialPhoto,
	                                                 'p_PhotoGallery' => $v_PhotoGallery,
	                                                 'p_TripTypes' => TripType::getList(),
	                                                 'p_SelectedTripTypes' => $v_SelectedTripTypes]);
    }

	public function postDestination()
	{
		$v_PhotoFiles = Input::file('photo');
		$v_Photos = [];
		foreach($v_PhotoFiles['file'] as $c_Image)
		{
			if($c_Image != null && $c_Image->isValid())
				array_push($v_Photos, $c_Image);
			else
				array_push($v_Photos, null);
		}
		$v_PhotoInfo = Input::get('photo');
		$v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

		Destination::post($v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::all());
		return redirect(url('/admin/destinos'))->with('message', 'Operação realizada com sucesso.');
	}

	public function toggleActive($p_Id)
	{
		$v_Destination = Destination::findOrFail($p_Id);
		$v_Destination->publico = !$v_Destination->publico;
		$v_Destination->save();
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}
}

