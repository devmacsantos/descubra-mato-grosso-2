<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\City;
use App\Event;
use App\EventCategory;
use App\EventCities;
use App\EventDate;
use App\EventByDates;
use App\EventHashtag;
use App\EventPhoto;
use App\EventSelectedCategories;
use App\EventsByDateCategoryClassification;
use App\EventClassification;
use App\EventSelectedClassification;
use App\Destination;
use App\DestinationPhoto;
use App\EventService;
use App\ItemsPageDescriptions;
use App\RevisionStatus;
use App\TouristicCircuit;
use App\TouristicCircuitCities;
use App\TripEvents;
use App\TripType;
use App\UserType;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Input;
use DB;
use App\UserTradeItem;
use Validator;


class CarnavalController extends BaseController
{

	public function getPublicEvents(Request $request, City $city, Event $event, EventClassification $EventClassification, $p_Lang='pt'){

		\App::setLocale($p_Lang);

		//Pega nomes das cidades para preencher campo lista de cidades
		$cids = get_cities_data_especial();
		$cities = $city->whereIn('id', $cids)->get(['id', 'name']);
		
		//$cities = get_cities_data_especial($city);
        //var_dump($cities);

		//Configura data para consultas sem Recomendações
		$initDate = new \DateTime();
		$finalDate = new \DateTime();
		$finalDate->add(new \DateInterval('P30D'));		


		return redirect(url('/pt/eventos/'));
			
		/*return redirect(url('/pt/carnaval-2019/')); */
		//CARNAVAL BH
		$carnavais = DB::select("select distinct id, city_id, cidade 
		from  (
			select e.id, e.city_id, c.name as cidade, e.nome, inicio, termino
			from event e, 
			(select e.id, min(date) as inicio
			from event_date ed, event e
			where ed.event_id=e.id
			group by e.id) as di,
			(select e.id, max(date) as termino
			from event_date ed, event e
			where ed.event_id=e.id
			group by e.id) as dt,
			city c
		where e.id = dt.id
		and e.id = di.id
		and e.city_id = c.id
		and e.city_id=66
		and (e.nome like '%CARNA%' or e.nome like '%BLOCO%' or e.nome like '%FOLIA%' )
		and termino < '2019-03-11'
		and inicio > curdate()
		and inicio < '2019-03-11'
		and e.revision_status_id = 4
		)  carnavais
		order by inicio");
		//var_dump($ccity);
		//Pega nomes das cidades para preencher campo lista de cidades

		foreach ($carnavais as $row) {
			$cbhids[] = $row->id;
		}			

		$themeEvent = $event->filter(['id'=>$cbhids]);



        /** Pega lista de fotos do instagram**/
        $v_InstagramPictures = get_instagram_images();


	return view('public.carnaval.index', compact('cids','cities', 'eventCarousel', 'featuredEvent', 'themeEvent', 'classification','v_InstagramPictures'))->with([
		'p_Language' => $p_Lang, 'title'=>'Descubra Mato Grosso - Eventos']);

	}

	public function getPublicEventsFilter(Request $request, City $city, Event $event, EventCategory $eventCategory, $p_Lang){
		\App::setLocale($p_Lang);


		$cids = get_cities_data_especial();

		$cities = $city->whereIn('id', $cids)->get(['id', 'name']);

		//Recebe dados do formulário para realizar consulta
		$filter = $request->except('_token');

		//Confira informações de data para realizar consulta
		if (isset($filter['date'])){
			$dates = explode('-',$filter['date']);
			$filter['initialDate'] = \DateTime::createFromFormat('d/m/Y', trim($dates[0]))->format('Y-m-d');
			$filter['finalDate'] = \DateTime::createFromFormat('d/m/Y', trim($dates[1]))->format('Y-m-d');
		}

		//Define status do evento para 4 (Aprovado)
		$filter['status'] = 4;

		/**Filtro por recomendação**/
		
		$cidade = (isset($filter['city']) && $filter['city'] > 0)?$filter['city']:'';
		$nome = (isset($filter['event']) && $filter['event'] != '')?$filter['event']:'';
		$initialDate = (isset($filter['date']))?$filter['initialDate']:'';
		$finalDate = (isset($filter['date']))?$filter['finalDate']:'';
		$Categoria = (isset($filter['category_id']) && $filter['category_id'] > 0)?$filter['category_id']:'';		
		

		$filtro_recomendacao = NULL;
		if(!isset($filtro_recomendacao)) {

			 if ($cidade){
				 $filtra_cidade = "and e.city_id=$cidade";
			 } else {
				 $filtra_cidade = '';
			 }
			 $carnavais = DB::select("select distinct id, city_id, cidade 
			 from  (
				 select e.id, e.city_id, c.name as cidade, e.nome, inicio, termino
			 		from event e, 
				 (select e.id, min(date) as inicio
				 from event_date ed, event e
				 where ed.event_id=e.id
				 group by e.id) as di,
				 (select e.id, max(date) as termino
				 from event_date ed, event e
				 where ed.event_id=e.id
				 group by e.id) as dt,
				 city c
			 where e.id = dt.id
			 and e.id = di.id
			 and e.city_id = c.id
			 $filtra_cidade
			 and (e.nome like '%CARNA%' or e.nome like '%BLOCO%' or e.nome like '%FOLIA%' )
			 and termino > curdate()
			 and termino < '2019-03-11'
			 and inicio > '2019-02-01'
			 and inicio < '2019-03-11'
			 and e.revision_status_id = 4
			 )  carnavais
			 order by inicio");
			 //var_dump($ccity);
			 //Pega nomes das cidades para preencher campo lista de cidades
			 $eids = NULL;
			 if (count($carnavais)> 0){
				foreach ($carnavais as $row) {
					$eids[] = $row->id;
				}	
			 } else {
				 $eids = array();
			 }
			

			if ((!$nome) && (count($eids) > 0)){ 
				//Solicita Filtro
				$vEvents =	$event->filter(['id'=>$eids], 12);
				//$vEvents = $event->filter($filter, 12);
			} else{ // a recomendação não achou nada 
				if (isset($filter['city']) && $filter['city'] > 0) $data['city_id'] = $filter['city'];
				if (isset($filter['event']) && $filter['event'] != '') $data['nome'] = $filter['event'];
				if (isset($filter['category_id']) && $filter['category_id'] > 0) $data['category'] = $filter['category_id'];
				if (isset($filter['date'])) $data['inicio'] = $filter['initialDate'];
				if (isset($filter['date'])) $data['fim'] = $filter['finalDate'];
	
				//Solicita Filtro
				$vEvents = $event->filter($filter, 12);
			}

			
		}

		if (count($vEvents) == 1){
			$event = $vEvents[0];
			return redirect(url('/pt/carnaval-2019/'.$event->Destination->slug.'/' .$event->slug));
		}


		//Foma o título da página
		$cidade = (isset($filter['city']) && $filter['city'] > 0)?" em ".$city->find($filter['city'])['name']:'';
		$nome = (isset($filter['event']) && $filter['event'] != '')?" por ".$filter['event']:'';
		$data = (isset($filter['date']))?" no período:".$filter['date']:'';
		$Categoria = (isset($filter['category_id']) && $filter['category_id'] > 0)?" na categoria ".$eventCategory->find($filter['category_id'])['nome_pt']:'';

        /** Pega lista de fotos do instagram**/
        $v_InstagramPictures = get_instagram_images();

		//Retorna a View que exibirá os dados filtrados
		return view('public.carnaval.filtered', compact('vEvents', 'filter', 'cities', 'debug','v_InstagramPictures'))->with([
			'p_Language' => $p_Lang, 'title'=>'Descubra Mato Grosso - Eventos: Busca '.$nome.$cidade.$data.$Categoria, 'textFilter'=>$nome.$cidade.$data.$Categoria]);	
	}


	public function getPublicEvent(City $city, Event $events, EventByDates $vEventDates, Destination $destination, DestinationPhoto $destinationPhoto, $p_Lang, $d_Slug,$p_Slug){
		\App::setLocale($p_Lang);

		$priorityCities = ['Aiuruoca', 'Belo Horizonte', 'Brumadinho', 'Caeté', 'Camanducaia', 'Cambuquira', 'Capitólio', 'Carrancas', 'Caxambu', 'Congonhas', 'Cordisburgo', 'Diamantina', 'Extrema', 'Gonçalves', 'Lagoa Santa', 'Lambari', 'Mariana', 'Ouro Preto', 'Pedro Leopoldo', 'Poços de Caldas', 'Sabará', 'São João Batista do Glória', 'São Lourenço', 'São Thomé das Letras', 'Serro', 'Sete Lagoas', 'Tiradentes'];

		//Pega dados do Evento via slug
		$event = $events->filter(['slug'=>$p_Slug]);

		if (count($event) > 0){
			$event = $event[0];
		} else {
			return redirect(url($p_Lang.'/carnaval-2019'));
		}

		$cids = get_cities_data_especial();

		$cities = $city->whereIn('id', $cids)->get(['id', 'name']);


		//Configura data para consultas sem Recomendações
		$initDate = new \DateTime();
		$finalDate = new \DateTime();
		$finalDate->add(new \DateInterval('P30D'));
		


		/* Pega Foto da cidade. */
		/** Processo pode ser simplificado criando função de relacionamento e filtro na mesma**/
		$dest = $destination->where('nome', $event->City->name)->first();
		if ($dest != null && ($photo = $destinationPhoto->where('destination_id', $dest->id)->where('is_cover', '1')->first()) != null){
			$cityPhoto = $photo->url;
			$citySlug = $dest->slug;
		} else {
			$cityPhoto = "http://minasgerais.com.br/imagens/destinos/1504999048PIBfUwX3XV.jpg";
			$citySlug = null;
		}

        /** Pega lista de fotos do instagram**/
        $v_InstagramPictures = get_instagram_images();		

		//Retorna View que exibe detalhes do evento
		return view('public.carnaval.show', compact('event', 'cities',  'cityPhoto', 'citySlug','v_InstagramPictures'))->with([
			'p_Language' => $p_Lang, 'title'=>'Descubra Mato Grosso - Eventos: '.$event->nome]);
	}


	/** Fim de funções antigas**/

	public function getEvents()
	{
		return view('admin.event.list')->with(['p_Categories' => EventCategory::getList(),
											   'p_Status' => RevisionStatus::getCompleteStatusList(),
											   'p_city' => City::getList()]);
	}

	public function getEventInfoHistory($p_Id)
	{
		$v_EventInfo = Event::findOrFail($p_Id);

		$v_Title = 'Eventos: ' . $v_EventInfo->nome;
		return view('admin.util.history')->with(['p_Title' => $v_Title,
		                                         'p_History' => $v_EventInfo->revisionHistory]);
	}


	public function getDTEvents()
	{
		$v_Columns = Input::get('columns');
		$v_Date = $v_Columns[0]['search']['value'];
		$v_UpdatedAt = $v_Columns[1]['search']['value'];
		$v_City = $v_Columns[2]['search']['value'];
		$v_Name = $v_Columns[3]['search']['value'];
		$v_CategoryId = $v_Columns[4]['search']['value'];
		$v_Status = $v_Columns[5]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return Event::getDT($v_Date, $v_UpdatedAt, $v_City, $v_Name, $v_CategoryId, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editEvent(Event $event, EventSelectedClassification $eventselectedclassification, EventClassification $eventclassification, $p_Id = null, $p_Dp=null)
	{
		if ($p_Id != null)
		{
			$v_Query = Event::where('id',$p_Id);
			if(UserType::isMunicipio())
				$v_Query->where('event.city_id', Auth::user()->city_id);
			else if(UserType::isCircuito())
				$v_Query->whereIn('event.city_id', TouristicCircuitCities::getUserCircuitCities());
			$v_Event = $v_Query->firstOrFail();
			$v_CoverPhoto = EventPhoto::getCoverPhoto($p_Id);
			$v_PhotoGallery = EventPhoto::getPhotoGallery($p_Id);
			$v_Dates = EventDate::getEventDates($p_Id);
			$v_Hashtags = EventHashtag::getList($p_Id);
			$v_EventCities = EventCities::getSelectedCities($p_Id);
			$v_SelectedCategories = EventSelectedCategories::getSelectedCategories($p_Id);
			$v_SelectedTripTypes = TripEvents::getSelectedTypes($v_Event->id);
			$v_SelectedEventClassification = $eventselectedclassification->where('event_id', $p_Id)->lists('event_classification_id')->toArray();
                        //Precisa ser melhorado
			//$selected_classifications = $eventselectedclassification->where('event_id', $p_Id)->lists('id');
			//foreach ($selected_classifications as $selected_classification) {
				//$classf_nome = $eventclassification->find($selected_classification->event_classification_id)->nome;
				//array_push($v_SelectedEventClassification, [$classf_nome, $selected_classification->event_classification_id]); 	
				//$v_SelectedEventClassification[$selected_classification->event_classification_id] = $classf_nome;
			//} 

			//dd($v_SelectedEventClassification);
			
		}
		else
		{
			$v_Event = null;
			$v_CoverPhoto = null;
			$v_PhotoGallery = [];
			$v_Dates = [];
			$v_Hashtags = [];
			$v_EventCities = [];
			$v_SelectedCategories = [];
			$v_SelectedTripTypes = [];
			$v_SelectedEventClassification = [];
		}

                
                if ($p_Dp == 'duplicar'){
                    $v_Event->id = null;
                    $v_Event->created_at = null;
                    $v_Event->updated_at = null;
                    $v_Event->updated_by = null;
                    $v_Event->created_by = null;
                    $v_Event->comentario_revisao = null;
                    $v_Event->revision_status_id = 0;
                    
                    $v_CoverPhoto = null;
                    $v_PhotoGallery=[];
                    $v_Dates = [];
                    
                }
                
                $v_AllCities = City::getList();
		if(UserType::isMunicipio())
			$v_Cities = City::where('id',Auth::user()->city_id)->lists('name', 'id')->toArray();
		else if(UserType::isCircuito())
			$v_Cities = City::whereIn('id',TouristicCircuitCities::getUserCircuitCities())->lists('name', 'id')->toArray();
		else
			$v_Cities = $v_AllCities;

		//dd($v_SelectedEventClassification);

		return view('admin.event.edit')->with(['p_Cities' => $v_Cities,
		                                       'p_AllCities' => $v_AllCities,
		                                       'p_EventCities' => $v_EventCities,
		                                       'p_Dates' => $v_Dates,
		                                       'p_TouristicCircuits' => TouristicCircuit::getList(),
		                                       'p_Categories' => EventCategory::getList(),
		                                       'p_SelectedCategories' => $v_SelectedCategories,
		                                       'p_Hashtags' => $v_Hashtags,
		                                       'p_Event' => $v_Event,
		                                       'p_CoverPhoto' => $v_CoverPhoto,
		                                       'p_PhotoGallery' => $v_PhotoGallery,
		                                       'p_EventPlaces' => [''=>''] + EventService::getEventPlacesByCity($v_Event == null ? null : $v_Event->city_id),
											   'p_TripTypes' => TripType::getList(),
											   'p_SelectedTripTypes' => $v_SelectedTripTypes,
											   'p_EventClassification' => EventClassification::lists('nome', 'id'),
											   'p_SelectedEventClassification' => $v_SelectedEventClassification,]);
	}

	public function getEventPlacesByCity($p_CityId){
		$v_Response['error'] = 'ok';
		$v_Response['data'] = EventService::getEventPlacesByCity($p_CityId);
		return $v_Response;
	}

    public function postEvent(EventSelectedClassification $eventselectedclassification)
    {
        $v_Id = Input::get('id');
	    //TODO arrumar validação
        $v_Validator = Validator::make(Input::all(), $v_Id == null ? Event::$m_Rules : Event::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput(Input::except(['datas']))->withErrors($v_Validator);

	    $v_PhotoFiles = Input::file('photo');
	    $v_Photos = [];
	    foreach($v_PhotoFiles['file'] as $c_Image)
	    {
		    if($c_Image != null && $c_Image->isValid())
			    array_push($v_Photos, $c_Image);
		    else
			    array_push($v_Photos, null);
	    }
	    $v_PhotoInfo = Input::get('photo');
	    $v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

        $v_Event = Event::post($v_Id, $v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::all());
		
		//Salva Classificação
        if (isset(Input::all()['classificacao_evento'])){
            $selected_classfs = Input::all()['classificacao_evento'];
        } else {
            $selected_classfs = [];    
        }

        $eventselectedclassification->where('event_id', $v_Event->id)->delete();
        
        foreach ($selected_classfs as $selected_classf) {
            //dd($selected_classf);
            $insert['event_id'] = $v_Event->id;
            $insert['event_classification_id'] = $selected_classf;
            $eventselectedclassification->create($insert);
        }
        
        return redirect(url('/admin/eventos'))->with('message', 'Evento salvo com sucesso!');
    }

//	public function toggleActive($p_Id)
//	{
//		$v_Query = Event::where('id',$p_Id);
//		if(UserType::isMunicipio())
//			$v_Query->where('event.city_id', Auth::user()->city_id);
//		else if(UserType::isCircuito())
//			$v_Query->whereIn('event.city_id', TouristicCircuitCities::getUserCircuitCities());
//		$v_Event = $v_Query->firstOrFail();
//		$v_Event->active = !$v_Event->active;
//		$v_Event->save();
//		return redirect()->back()->with('message', 'Operação realizada com sucesso');
//	}

	public function deleteEvent($p_Id)
	{
		Event::deleteEvent($p_Id);
		return redirect()->back()->with('message', 'Operação realizada com sucesso');
	}

	/*
	|--------------------------------------------------------------------------
	| EventCategory
	|--------------------------------------------------------------------------
	*/

	public function getEventCategories()
	{
		return view('admin.properties.eventCategory.list')->with(['p_Categories' => EventCategory::all()]);
	}

	public function editEventCategory($p_Id = null)
	{
		if ($p_Id != null)
			$v_Category = EventCategory::findOrFail($p_Id);
		else
			$v_Category = null;
		return view('admin.properties.eventCategory.edit')->with(['p_Category' => $v_Category]);
	}

	public function postEventCategory()
	{
		$v_Id = Input::get('id');
		EventCategory::post($v_Id, Input::all());
		return redirect(url('/admin/eventos/categorias'))->with('message', 'Operação realizada com sucesso.');
	}
/**
 * Retorna JSON com resultado de consulta realizada
 *
 * @param EventsByDateCategoryClassification $event Injeção de dependência para Model
 * @param Integer $city ID da cidade para consulta
 * @param Integer $name Nome do evento a ser consultado
 * @return void
 */
	public function eventByCityAndName(EventsByDateCategoryClassification $event, $city, $name=null){
		$events = $event
				->where('city_id', $city)
				->where('name', 'like', "%$name%")
				->groupby('event_id')
                ->distinct('event_id')
                ->orderBy('start', 'asc')
				->get();

		return json_encode($events);
	}
	/**
	 * Usado para solicitar mudança de posse de evento
	 *
	 * @param EventsByDateCategoryClassification $event Injeção de dependência para Model
	 * @param UserTradeItem $userTradeItem $event Injeção de dependência para Model
	 * @param Integer $event_id ID do evento a ser solicitado posse
	 * @param Integer $user_id ID do usuário que passara a ser dono do evento
	 * @return void Retorna o redirecionamento para página de Eventos(...)
	 */
	public function requestEventOwnership(EventsByDateCategoryClassification $event, UserTradeItem $userTradeItem, $event_id, $user_id) {

		$conv = $event->where('event_id', $event_id)->first();

		$ant = $userTradeItem->where('user_id', $user_id)->where('id', $event_id)->where('type', 'EV')->first();

		if (isset($ant) ) {
			return redirect()->back()->with('message', 'Ítem já solicitado!');
		} elseif($conv->Event->created_by ==  $user_id){
			return redirect()->back()->with('message', 'O Evento solicitado já pertence ao usuário!');
		}

		if (isset($conv)){
			UserTradeItem::requestTradeItemOwnership('EV', $event_id);
			return redirect()->back()->with('message', 'Solicitação realizada! A SECRETARIA ADJUNTA DE TURISMO analisará seu pedido.');
		}

	}

}

