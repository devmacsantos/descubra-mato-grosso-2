<?php

namespace App\Http\Controllers;


use App\City;
use App\TouristicCircuit;
use App\TouristicCircuitCities;
use App\User;
use App\UserType;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Validator;

class UserController extends BaseController
{
	public function postLogin()
	{
		if(User::isActive(Input::get('email')) === false)
			return redirect()->back()->with('error_message', 'Esse usuário está desativado.');

		if (Auth::attempt(['email'=> Input::get('email'), 'password'=>Input::get('password')]))
		{
			if(UserType::isAdmin() || UserType::isMaster() || UserType::isCircuito() || UserType::isMunicipio() || UserType::isTrade())
				return redirect(url('/admin/painel-geral'));
			if(UserType::isComunicacao())
				return redirect(url('/admin/destinos'));
			else //parceiro e fiscalizador
				return redirect(url('/admin/inventario/informacoes'));
		}
		else
			return redirect()->back()->withInput()->with('error_message', 'A combinação está incorreta!');
	}

	public function getResetPassword()
	{
		return view('admin.requestReset');
	}

	public function postResetPassword()
	{
		return User::resetPassword(Input::get('email'));
	}

	public function getUsers()
	{
		return view('admin/user/list', ['p_UserTypes' => UserType::getList()]);
	}

	public function getDTUsers()
	{
		$v_Columns = Input::get('columns');
		$v_Circuit = $v_Columns[0]['search']['value'];
		$v_City = $v_Columns[1]['search']['value'];
		$v_Name = $v_Columns[2]['search']['value'];
		$v_Email = $v_Columns[3]['search']['value'];
		$v_TypeId = $v_Columns[4]['search']['value'];
		$v_Status = $v_Columns[5]['search']['value'];
		$v_Order = Input::get('order')[0];
		$v_Start = Input::get('start');
		$v_Length = Input::get('length');
		$v_Draw = Input::get('draw');
		return User::getDT($v_Circuit, $v_City, $v_Name, $v_Email, $v_TypeId, $v_Status, $v_Order, $v_Start, $v_Length, $v_Draw);
	}

	public function editUser($p_Id = null)
	{
		if ($p_Id != null)
			$v_User = User::findOrFail($p_Id);
		else
			$v_User = null;

		//TODO remover isso??
		$v_Trades = [];
//		$v_Trades = Attraction::getTradeList();

		return view('admin/user/edit', ['p_User' => $v_User,
		                                'p_Cities' => City::getList(),
		                                'p_Circuits' => TouristicCircuit::getUserList(),
		                                'p_Trades' => $v_Trades,
		                                'p_UserTypes' => UserType::getList()]);
	}

	public function postUser()
	{
		$v_Id = Input::get('id');
		$v_Rules = $v_Id == null ? User::$m_Rules : User::getEditRules($v_Id);
		$v_Validator = Validator::make(array_merge(Input::all(),Input::get('usuario')), $v_Rules);
		if ($v_Validator->fails())
			return redirect()->back()->withInput()->withErrors($v_Validator);

		$v_Pwd = Input::get('senha');
		return User::post($v_Id, Input::get('usuario'), $v_Pwd);
	}

	public function editUserProfile()
	{
		$v_User = User::join('user_type','user_type.id','=','user.user_type_id')
				->select('user.*', 'user_type.name as user_type')
				->findOrFail(Auth::id());

		$v_City = null;
		$v_Circuit = null;
		$v_Trade = [];
		if($v_User->user_type_id == 4)
			$v_City = City::find($v_User->city_id);
		else if($v_User->user_type_id == 5)
			$v_Circuit = TouristicCircuit::find($v_User->touristic_circuit_id);
//		else if($v_User->attraction_id != null)
//			$v_Trades = Attraction::getTradeList();

		return view('admin/user/editProfile', ['p_User' => $v_User,
		                                       'p_City' => $v_City,
		                                       'p_Circuit' => $v_Circuit,
		                                       'p_Trades' => $v_Trade]);
	}

	public function postUserProfile()
	{
		return User::postProfile(Auth::id(), Input::get('usuario'), Input::get('senha'));
	}

	public function toggleActive($p_Id)
	{
		User::toggleActive($p_Id);
		return redirect()->back()->with('message', 'O usuário foi alterado');
	}

	public function getDashboard()
	{
		$v_Query = null;
		if(UserType::isMunicipio())
		{
			$v_Query = 'revision_status_id in (1,5) and city_id = ' . Auth::user()->city_id;
		}
		else if(UserType::isCircuito())
		{
			$v_Cities = TouristicCircuitCities::getUserCircuitCities();
			if(count($v_Cities))
			{
				$v_Query = 'revision_status_id in (1,2,5) and city_id in (';
				foreach($v_Cities as $c_CityIndex => $c_CityId){
					$v_Query .= $c_CityId . (($c_CityIndex == count($v_Cities) - 1) ? '' : ',');
				}
				$v_Query .= ')';
			}
		}
		else if(UserType::isMaster() || UserType::isAdmin())
		{
			$v_Query = 'revision_status_id in (1,2,3)';

		}
//		else if(UserType::isComunicacao())
//		{
//			$v_Query = 'revision_status_id in (1,2,3,5)';
//		}

		return view('admin.dashboard.dashboard', ['p_Query' => $v_Query]);
	}

	public function postRegisterTradeUser()
	{
		$v_Validator = Validator::make(Input::all(), User::$m_RulesTradeUser);
		if ($v_Validator->fails())
			return redirect()->back()->withInput()->withErrors($v_Validator);

		return User::postRegisterTradeUser(Input::get('nome'), Input::get('email'), Input::get('senha'));
	}
}

