<?php

namespace App\Http\Controllers;

use App\Attraction;
use App\ContactSubject;
use App\InstitutionalPhoto;
use App\ItemsHome;
use App\ItemsInstitutional;
use App\ItemsPageDescriptions;
use App\Parameter;
use App\TripCategory;
use App\TripType;
use \Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Validator;

class PagesConfigController extends BaseController
{

    /*
	|--------------------------------------------------------------------------
	| Home
	|--------------------------------------------------------------------------
	*/

    public function editHome()
    {
        return view('admin.publicPages.editHome')->with(['p_Portuguese' => ItemsHome::getByLanguage('pt'),
                                                         'p_English' => ItemsHome::getByLanguage('en'),
                                                         'p_Spanish' => ItemsHome::getByLanguage('es'),
                                                         'p_French' => ItemsHome::getByLanguage('fr'),
                                                         'p_InstagramClientId' => Parameter::getParameterByKey('instagram-client-id'),
                                                         'p_InstagramClientSecret' => Parameter::getParameterByKey('instagram-client-secret'),
                                                         'p_InstagramRedirectUri' => Parameter::getParameterByKey('instagram-redirect-uri'),
                                                         'p_InstagramAccessToken' => Parameter::getParameterByKey('instagram-access-token'),
                                                         'p_Apps' => Parameter::getParameterByKey('home-apps')]);
    }

    public function postHome()
    {
        $v_Id = Input::get('id')[0];

        $v_Validator = Validator::make(Input::all(), $v_Id == null ? ItemsHome::$m_Rules : ItemsHome::$m_RulesEdit);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Image = Input::file('foto_capa');
        if($v_Image != null && $v_Image->isValid())
            $v_CoverPhoto = Image::make($v_Image->getRealPath());
        else
            $v_CoverPhoto = null;

        ItemsHome::post($v_CoverPhoto, Input::all());
        Parameter::postParameters(Input::get('instagram'));

        //Apps
        $v_PhotoFiles = Input::file('foto_app');
        $v_PhotoNames = Input::get('foto_app_nome');
        foreach($v_PhotoFiles as $c_Index => $c_Image)
        {
            if($c_Image != null && $c_Image->isValid()){
                $v_Photo = Image::make($c_Image);

                $v_Path = public_path() . '/imagens/';
                if(!\File::exists($v_Path))
                    \File::makeDirectory($v_Path);
                $v_Path .=  '/paginas/';
                if(!\File::exists($v_Path))
                    \File::makeDirectory($v_Path);
                $v_Path .=  '/home-apps/';
                if(!\File::exists($v_Path))
                    \File::makeDirectory($v_Path);
                $v_PhotoName =  explode('/',$v_PhotoNames[$c_Index]);
                $v_PhotoName =  array_pop($v_PhotoName);
                $v_Photo->widen(1920, function ($constraint){
                    $constraint->upsize();
                });
                $v_Photo->save($v_Path . $v_PhotoName);
            }
        }
        Parameter::postParameters(['home-apps' => Input::get('apps_data')]);
        return redirect()->back()->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| Insitutional
	|--------------------------------------------------------------------------
	*/

    public function editInstitutional()
    {
        return view('admin.publicPages.editInstitutional')->with(['p_Portuguese' => ItemsInstitutional::getByLanguage('pt'),
                                                                  'p_English' => ItemsInstitutional::getByLanguage('en'),
                                                                  'p_Spanish' => ItemsInstitutional::getByLanguage('es'),
                                                                  'p_French' => ItemsInstitutional::getByLanguage('fr'),
                                                                  'p_TestimonialPhoto' => InstitutionalPhoto::getTestimonialPhoto(),
                                                                  'p_PhotoGallery' => InstitutionalPhoto::getPhotoGallery()]);
    }

    public function postInstitutional()
    {
        $v_PhotoFiles = Input::file('photo');
        $v_Photos = [];
        foreach($v_PhotoFiles['file'] as $c_Image)
        {
            if($c_Image != null && $c_Image->isValid())
                array_push($v_Photos, $c_Image);
            else
                array_push($v_Photos, null);
        }
        $v_PhotoInfo = Input::get('photo');
        $v_DeletedPhotos = Input::has('delete_photo') ? Input::get('delete_photo') : [];

        ItemsInstitutional::post($v_Photos, $v_PhotoInfo, $v_DeletedPhotos, Input::all());
        return redirect()->back()->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| VideoGallery
	|--------------------------------------------------------------------------
	*/

    public function editVideoGallery()
    {
        return view('admin.publicPages.editVideoGallery')->with(['p_Videos' => Parameter::getParameterLikeKey('video_gallery_url_')]);
    }

    public function postVideoGallery()
    {
        Parameter::postVideoUrls(Input::get('video_url'));
        return redirect()->back()->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| PageDescriptions
	|--------------------------------------------------------------------------
	*/

    public function editPageDescriptions($p_Page)
    {
	    return view('admin.publicPages.editPageDescriptions')->with(['p_Portuguese' => ItemsPageDescriptions::getByLanguage('pt', $p_Page),
	                                                                 'p_English' => ItemsPageDescriptions::getByLanguage('en', $p_Page),
	                                                                 'p_Spanish' => ItemsPageDescriptions::getByLanguage('es', $p_Page),
	                                                                 'p_French' => ItemsPageDescriptions::getByLanguage('fr', $p_Page),
	                                                                 'p_Page' => $p_Page]);
    }

    public function postPageDescriptions()
    {
	    ItemsPageDescriptions::post(Input::all());
        return redirect()->back()->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| TripCategory
	|--------------------------------------------------------------------------
	*/

    public function getTripCategories()
    {
        return view('admin.tripCategory.list')->with(['p_Categories' => TripCategory::getCategoryList()]);
    }

    public function editTripCategory($p_Id = null)
    {
        if ($p_Id != null)
            $v_Category = TripCategory::findOrFail($p_Id);
        else
            $v_Category = null;

        return view('admin.tripCategory.edit')->with(['p_Category' => $v_Category]);
    }

    public function postTripCategory()
    {
        $v_Id = Input::get('id');

        $v_Image = Input::file('foto_capa');
        if($v_Image != null && $v_Image->isValid())
            $v_CoverPhoto = Image::make($v_Image->getRealPath());
        else
            $v_CoverPhoto = null;

        TripCategory::post($v_Id, $v_CoverPhoto, Input::all());
        return redirect(url('/admin/categorias-viagem'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| TripType
	|--------------------------------------------------------------------------
	*/

    public function getTripTypes()
    {
        return view('admin.tripType.list')->with(['p_Types' => TripType::getTypeList()]);
    }

    public function editTripType($p_Id = null)
    {
        if ($p_Id != null)
            $v_Type = TripType::findOrFail($p_Id);
        else
            $v_Type = null;

        return view('admin.tripType.edit')->with(['p_Type' => $v_Type,
                                                  'p_Categories' => TripCategory::getList(),
                                                  'p_Attractions' => Attraction::getList()]);
    }

    public function postTripType()
    {
        $v_Id = Input::get('id');

        $v_Image = Input::file('foto_capa');
        if($v_Image != null && $v_Image->isValid())
            $v_CoverPhoto = Image::make($v_Image->getRealPath());
        else
            $v_CoverPhoto = null;

        TripType::post($v_Id, $v_CoverPhoto, Input::all());
        return redirect(url('/admin/tipos-viagem'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| PageSingleDescription
	|--------------------------------------------------------------------------
	*/

    public function editPageSingleDescription($p_Page)
    {
	    return view('admin.publicPages.editPageSingleDescription')
		    ->with(['p_Portuguese' => Parameter::getParameterByKey($p_Page . '_pt'),
		            'p_English' => Parameter::getParameterByKey($p_Page . '_en'),
		            'p_Spanish' => Parameter::getParameterByKey($p_Page . '_es'),
		            'p_French' => Parameter::getParameterByKey($p_Page . '_fr'),
		            'p_Page' => $p_Page]);
    }

    public function postPageSingleDescription()
    {
        Parameter::postParameters(Input::get('idioma'));
        return redirect()->back()->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| PlanYourTrip
	|--------------------------------------------------------------------------
	*/

    public function editPlanYourTrip()
    {
	    $v_Languages = [Parameter::getParameterByKey('planeje-sua-viagem-descricao_pt'),
		    Parameter::getParameterByKey('planeje-sua-viagem-descricao_en'),
		    Parameter::getParameterByKey('planeje-sua-viagem-descricao_es'),
		    Parameter::getParameterByKey('planeje-sua-viagem-descricao_fr')];
	    $v_Files = [Parameter::getParameterByKey('planeje-sua-viagem-guia_pt'),
		    Parameter::getParameterByKey('planeje-sua-viagem-guia_en'),
		    Parameter::getParameterByKey('planeje-sua-viagem-guia_es'),
		    Parameter::getParameterByKey('planeje-sua-viagem-guia_fr')];
	    $v_Information = [Parameter::getParameterByKey('planeje-sua-viagem-informacoes_pt'),
		    Parameter::getParameterByKey('planeje-sua-viagem-informacoes_en'),
		    Parameter::getParameterByKey('planeje-sua-viagem-informacoes_es'),
		    Parameter::getParameterByKey('planeje-sua-viagem-informacoes_fr')];
	    $v_Directions = [Parameter::getParameterByKey('planeje-sua-viagem-como-chegar_pt'),
		    Parameter::getParameterByKey('planeje-sua-viagem-como-chegar_en'),
		    Parameter::getParameterByKey('planeje-sua-viagem-como-chegar_es'),
		    Parameter::getParameterByKey('planeje-sua-viagem-como-chegar_fr')];

	    return view('admin.publicPages.editPlanYourTrip')->with(['p_Languages' => $v_Languages,
	                                                             'p_Files' => $v_Files,
	                                                             'p_Information' => $v_Information,
	                                                             'p_Directions' => $v_Directions]);
    }

    public function postPlanYourTrip()
    {
	    $v_Path = public_path() . '/arquivos/';
	    if(!\File::exists($v_Path))
		    \File::makeDirectory($v_Path);

	    $v_Data = Input::get('idioma');
	    $v_Files = Input::file('guia');
	    foreach($v_Files as $c_Index => $c_File)
	    {
		    if($c_File != null && $c_File->isValid())
		    {
			    $v_FileName = 'guia_' . BaseController::$m_Languages[$c_Index] . '.pdf';
			    $c_File->move($v_Path, $v_FileName);
			    $v_Data['planeje-sua-viagem-guia_' . BaseController::$m_Languages[$c_Index]] = url('/arquivos/' . $v_FileName);
		    }
	    }

        Parameter::postParameters($v_Data);
        return redirect()->back()->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| ContactSubject
	|--------------------------------------------------------------------------
	*/

    public function getContactSubjects()
    {
        return view('admin.contactSubject.list')->with(['p_Subjects' => ContactSubject::all()]);
    }

    public function editContactSubject($p_Id = null)
    {
        if($p_Id != null)
            $v_Subject = ContactSubject::find($p_Id);
        else
            $v_Subject = null;
        return view('admin.contactSubject.edit')->with(['p_Subject' => $v_Subject]);
    }

    public function postContactSubject()
    {
        $v_Id = Input::get('id');
        ContactSubject::post($v_Id, Input::all());
        return redirect(url('/admin/assuntos-fale-conosco'))->with('message', 'Operação realizada com sucesso.');
    }

    /*
	|--------------------------------------------------------------------------
	| File upload via CKEditor
	|--------------------------------------------------------------------------
	*/

    public function uploadFile()
    {
        $v_CKEditorFuncNum = Input::get('CKEditorFuncNum');
        $v_File = Input::hasFile('upload') ? Input::file('upload') : null;

        if ($v_File != null && $v_File->isValid())
        {
            $v_Path = public_path() . '/arquivos/';
            if(!\File::exists($v_Path))
                \File::makeDirectory($v_Path);
            $v_Path .=  '/uploads/';
            if(!\File::exists($v_Path))
                \File::makeDirectory($v_Path);

            $v_Extension = $v_File->getClientOriginalExtension();
            $v_FileName = time() . str_random(10) . '.' . $v_Extension;

            $v_File->move($v_Path, $v_FileName);
            $v_FileUrl = url('/arquivos/uploads/' . $v_FileName);
            return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($v_CKEditorFuncNum, '$v_FileUrl', 'Arquivo enviado com sucesso');</script>";
        }
        return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($v_CKEditorFuncNum, '', 'Erro ao enviar arquivo');</script>";
    }
}

