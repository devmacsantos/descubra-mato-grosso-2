<?php

namespace App\Http\Controllers;

use App\Attraction;
use App\AttractionHashtag;
use App\BlogArticle;
use App\BlogArticleHashtag;
use App\City;
use App\ContactCity;
use App\ContactSubject;
use App\Contact;
use App\Destination;
use App\DestinationHashtag;
use App\DestinationPhoto;
use App\Event;
use App\EventHashtag;
use App\EventByDates;
use App\Hashtag;
use App\InstitutionalPhoto;
use App\ItemsPageDescriptions;
use App\ItemsInstitutional;
use App\Newsletter;
use App\Parameter;
use App\DonatedMedia;
use App\TouristicRoute;
use App\TouristicRouteHashtag;
use App\TripCategory;
use App\TripDestinations;
use App\TripType;
use App\Type;
use \Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Validator;
use App\ItemsHome;
use Larabros\Elogram\Client;
use Illuminate\Support\Facades\DB;

class PublicController extends BaseController
{
    
    public function getHome(Parameter $parameter, Destination $destination, EventByDates $vEventDates, $p_Lang){
        \App::setLocale($p_Lang);
        
		//Configura data para consultas sem Recomendações
		$initDate = new \DateTime();
		$finalDate = new \DateTime();
		$finalDate->add(new \DateInterval('P30D'));

        //SEm sistema de recomendaação
        $eventCarousel = NULL;
		if (!isset($eventCarousel) || (isset($eventCarousel) && count($eventCarousel) < 1)){

		//Seleciona eventos ocorrendo no intervalo de 30 dias nas cidades TOPs
		$eventCarousel = $vEventDates
					->where('revision_status_id', '4')
					->where('featured', '1')
					->whereBetween('start', [$initDate->format('Y-m-d')." 00:00:01", $finalDate->format('Y-m-d')." 23:59:59"])
					->whereBetween('end', [$initDate->format('Y-m-d')." 00:00:01", $finalDate->format('Y-m-d')." 23:59:59"])
					->whereIn('city', ['Cuiabá', 'Várzea Grande', 'Campo Novo dos Parecis', 'Tangará da Serra', 'Poconé', 'Sapezal'])
					->orderBy('start', 'asc')
					->get();
		} 

        /** Destinos surpreendentes**/
        $destinations = $destination->getDestinations(6);

         /** Pega lista de fotos do instagram**/
         $v_InstagramPictures = get_instagram_images();
        

        return view('public.home.index', 
        compact('eventCarousel', 'destination', 'destinations', 'v_InstagramPictures')
        )->with(['p_Language'=>$p_Lang]);
    }    
    
    
    public function getHome_($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Content = ItemsHome::getByLanguage($p_Lang);

        $v_Destinations = Destination::getDestinations(5);
        $v_FeaturedAttractions = Attraction::getFeaturedAttractions(5);

        $v_InstagramPictures = [];
		
        try {
            $v_Client = new Client(Parameter::getParameterByKey('instagram-client-id'),
                Parameter::getParameterByKey('instagram-client-secret'),
                '{"access_token":"' . Parameter::getParameterByKey('instagram-access-token') . '"}',
                Parameter::getParameterByKey('instagram-redirect-uri'));
            $v_Response = $v_Client->users()->getMedia();
            $v_Response = $v_Response->getRaw();
            if($v_Response['meta']['code'] == 200)
                $v_InstagramPictures = $v_Response['data'];
        } catch (\Exception $e) {
        }

        return view('public.home')->with(['p_Content' => $v_Content,
                                          'p_Apps' => Parameter::getParameterByKey('home-apps'),
                                          'p_Categories' => TripCategory::getCategories(8),
                                          'p_Destinations' => $v_Destinations,
                                          'p_FeaturedAttractions' => $v_FeaturedAttractions,
                                          'p_AccomodationList' => Type::getPublicInventoryTypeListByName('B1', $p_Lang),
                                          'p_FoodDrinkList' => Type::getPublicInventoryTypeListByName('B2', $p_Lang),
                                          'p_ServiceList' => Type::getPublicInventoryTypeServiceList($p_Lang),
                                          'p_RecreationList' => Type::getPublicInventoryTypeListByName('B6', $p_Lang),
                                          'p_DestinationList' => Destination::getSlugList(),
                                          'p_InstagramPictures' => $v_InstagramPictures,
                                          'p_Language' => $p_Lang]);
    }

    public function getInstitutional($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Content = ItemsInstitutional::getByLanguage($p_Lang);

        $v_Articles = BlogArticle::getArticles(4);
	    foreach($v_Articles as $c_Article)
		    $c_Article->hashtags = array_keys(BlogArticleHashtag::getList($c_Article->id));

        return view('public.institutional')->with(['p_Content' => $v_Content,
                                                   'p_TestimonialPhoto' => InstitutionalPhoto::getTestimonialPhoto(),
                                                   'p_PhotoGallery' => InstitutionalPhoto::getPhotoGallery(),
                                                   'p_Articles' => $v_Articles,
                                                   'p_Language' => $p_Lang]);
    }

    public function getPhotoGallery($p_Lang)
    {
        \App::setLocale($p_Lang);

        $v_Destinations = Destination::getDestinations(25)->toArray();
        $v_Index = array_rand($v_Destinations);
        $v_MainDestination = $v_Destinations[$v_Index];
        unset($v_Destinations[$v_Index]);

        return view('public.photoGallery')->with(['p_Content' => $v_MainDestination,
                                                  'p_PhotoGallery' => DestinationPhoto::getPhotoGallery($v_MainDestination['id'], true),
                                                  'p_Destinations' => $v_Destinations,
                                                  'p_DestinationList' => Destination::getSlugList(),
                                                  'p_Language' => $p_Lang]);
    }

    public function getDestinationPhotoGallery($p_Lang, $p_Slug)
    {
        \App::setLocale($p_Lang);
        $v_MainDestination = Destination::getDestination($p_Slug);
        return view('public.photoDestinationGallery')->with(['p_Content' => $v_MainDestination,
                                                             'p_PhotoGallery' => DestinationPhoto::getPhotoGallery($v_MainDestination['id'], true),
                                                             'p_Language' => $p_Lang]);
    }

//    public function getVideoGallery($p_Lang)
//    {
//        \App::setLocale($p_Lang);
//        //TODO carregar todos os videos, em ordem aleatoria (mostrar cidade, atracao, video (ou thumb))
//        $p_Videos = Parameter::getParameterLikeKey('video_gallery_url_');
//        return view('public.videoGallery')->with(['p_Videos' => Parameter::getParameterLikeKey('video_gallery_url_'),
//                                                    'p_Language' => $p_Lang]);
//    }

    public function getWhatToDo($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Content = ItemsPageDescriptions::getByLanguage($p_Lang, 'o-que-fazer');

        return view('public.whatToDo')->with(['p_Content' => $v_Content,
                                              'p_Categories' => TripCategory::getCategories(6),
                                              'p_Types' => TripType::getHeaderTypes(12),
                                              'p_Language' => $p_Lang]);
    }

    public function getTripCategory($p_Lang, $p_Slug)
    {
	    \App::setLocale($p_Lang);

	    $v_Content = TripCategory::getCategory($p_Slug);
	    $v_TripTypes = TripType::getCategoryTypes($v_Content->id, 20);

        return view('public.tripCategory')->with(['p_Content' => $v_Content,
                                                  'p_TripTypes' => $v_TripTypes,
                                                  'p_Language' => $p_Lang]);
    }

    public function getTripType($p_Lang, $p_CategorySlug, $p_Slug)
    {
	    \App::setLocale($p_Lang);

	    $v_Content = TripType::getType($p_Slug);
	    $v_Places = TripType::getTripTypePlaces($v_Content->id, 64);
        return view('public.tripType')->with(['p_Content' => $v_Content,
                                              'p_Places' => $v_Places,
                                              'p_Language' => $p_Lang]);
    }

    public function getWhereToGo($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Content = ItemsPageDescriptions::getByLanguage($p_Lang, 'para-onde-ir');

        $v_Destinations = Destination::getDestinations(23);
        foreach($v_Destinations as $c_Item)
            $c_Item->tipo = 'destinos';
        $v_Attractions = Attraction::getParks(23);
        foreach($v_Attractions as $c_Item)
            $c_Item->tipo = 'atracoes';
        $v_TouristicRoutes = TouristicRoute::getRoutes(23);
        foreach($v_TouristicRoutes as $c_Item)
            $c_Item->tipo = 'roteiros';

        $v_Places = array_merge($v_Destinations->toArray(), $v_Attractions->toArray(), $v_TouristicRoutes->toArray());
        shuffle($v_Places);
        $v_Places = array_slice($v_Places, 0, 23);

        $v_TripCategories = TripCategory::getList($p_Lang);
        $v_TripTypes = TripType::getFilterList();

        return view('public.whereToGo')->with(['p_Content' => $v_Content,
                                               'p_Places' => $v_Places,
                                               'p_TripCategories' => $v_TripCategories,
                                               'p_TripTypes' => $v_TripTypes,
                                               'p_DestinationList' => Destination::getPublicList(),
                                               'p_Language' => $p_Lang]);
    }

    public function getWhereToGoFilter($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_TripTypeId = Input::get('trip_type_id');
        $v_DestinationId = Input::get('destination_id');
        if(!empty($v_TripTypeId))
        {
            $v_Response['error'] = 'ok';
            $v_Response['data'] = TripType::getWhereToGoPlacesFilter($v_TripTypeId, $v_DestinationId, 52);
        }
        else
            $v_Response['error'] = trans('events.fill_form_message');

        return $v_Response;
    }

    public function getDestinations($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Content = ItemsPageDescriptions::getByLanguage($p_Lang, 'destinos');
	    $v_Destinations = Destination::getDestinations(25);
        $v_DestinationList = Destination::getCompleteList();

	    $v_TripCategories = ['' => ''] + TripCategory::getList($p_Lang);
	    $v_TripTypes = TripType::getFilterList();

        return view('public.destinations')->with(['p_Content' => $v_Content,
                                                  'p_Destinations' => $v_Destinations,
                                                  'p_DestinationList' => $v_DestinationList,
                                                  'p_TripCategories' => $v_TripCategories,
                                                  'p_TripTypes' => $v_TripTypes,
                                                  'p_Language' => $p_Lang]);
    }

    public function getDestinationsFilter($p_Lang)
    {
        $v_TripTypeId = Input::get('trip_type_id');
        $v_HashtagIds = null;
        if($v_TripTypeId != null)
        {
            $v_Response['error'] = 'ok';
            $v_Response['data'] = TripDestinations::getTripDestinations($v_TripTypeId, 24);
        }
        else
            $v_Response['error'] = trans('events.fill_form_message');

        return $v_Response;
    }

    public function getDestination(Destination $destination,$p_Lang, $p_Slug)
    {
        \App::setLocale($p_Lang);
        $v_Destination = Destination::getDestination($p_Slug);
        $v_CityAttractions = Attraction::getCityMainAttractions($v_Destination->city_id, 8);
        $v_OtherCitiesAttractions = Attraction::getOtherCitiesAttractions($v_Destination->city_id, $v_Destination->latitude, $v_Destination->longitude, 4);
        $v_CityEvents = Event::getFilteredEvents($v_Destination->city_id, '', 5);

        return view('public.destination')->with(['p_Content' => $v_Destination,
                                                 'destination' => $destination,
                                                 'tipo'=>Type::getList(),
                                                 'p_TestimonialPhoto' => DestinationPhoto::getTestimonialPhoto($v_Destination->id),
                                                 'p_MapPhoto' => DestinationPhoto::getMapPhoto($v_Destination->id),
                                                 'p_PhotoGallery' => DestinationPhoto::getPhotoGallery($v_Destination->id),
                                                 'p_CityAttractions' => $v_CityAttractions,
                                                 'p_OtherCitiesAttractions' => $v_OtherCitiesAttractions,
                                                 'p_CityEvents' => $v_CityEvents,
                                                 'p_AccomodationList' => Type::getPublicInventoryTypeListByName('B1', $p_Lang),
                                                 'p_FoodDrinkList' => Type::getPublicInventoryTypeListByName('B2', $p_Lang),
                                                 'p_ServiceList' => Type::getPublicInventoryTypeServiceList($p_Lang),
                                                 'p_RecreationList' => Type::getPublicInventoryTypeListByName('B6', $p_Lang),
                                                 'p_Accomodations' => Attraction::getDestinationTrade($v_Destination->city_id, ['B1']),
                                                 'p_FoodDrinks' => Attraction::getDestinationTrade($v_Destination->city_id, ['B2']),
                                                 'p_Services' => Attraction::getDestinationTrade($v_Destination->city_id, ['B3', 'B4']),
                                                 'p_Recreations' => Attraction::getDestinationTrade($v_Destination->city_id, ['B6']),
                                                 'p_Language' => $p_Lang]);
    }

    public function getExploreMap($p_Lang)
    {
	    \App::setLocale($p_Lang);
        $v_Attractions = array_merge(Attraction::getMapCulturalAttractions(), Attraction::getMapNaturalAttractions());
        return view('public.exploreMap')->with(['p_Content' => Parameter::getParameterByKey('explore-o-mapa_' . $p_Lang),
                                                'p_Destinations' => Destination::getCompleteList(),
                                                'p_Attractions' => $v_Attractions,
                                                'p_Language' => $p_Lang]);
    }

    public function getPlanYourTrip($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Destinations = Destination::getCompleteList();
	    $v_Content = Parameter::getParametersByKeys(['planeje-sua-viagem-descricao_' . $p_Lang,
												    'planeje-sua-viagem-guia_' . $p_Lang,
												    'planeje-sua-viagem-informacoes_' . $p_Lang,
												    'planeje-sua-viagem-como-chegar_' . $p_Lang]);

        return view('public.planYourTrip')->with(['p_Content' => $v_Content->toArray(),
                                                  'p_Destinations' => $v_Destinations,
                                                  'p_AccomodationList' => Type::getPublicInventoryTypeListByName('B1', $p_Lang),
                                                  'p_FoodDrinkList' => Type::getPublicInventoryTypeListByName('B2', $p_Lang),
                                                  'p_ServiceList' => Type::getPublicInventoryTypeServiceList($p_Lang),
                                                  'p_RecreationList' => Type::getPublicInventoryTypeListByName('B6', $p_Lang),
                                                  'p_DestinationList' => Destination::getSlugList(),
                                                  'p_Language' => $p_Lang]);
    }


	public function getSearch(Destination $destination, $p_Lang, $p_Type = null)
	{
		\App::setLocale($p_Lang);

        $v_SearchString = Input::get('q');
        //tira stopword
        $stopwords = [' mg','-mg'];


        $v_SearchString = str_replace($stopwords,[''],$v_SearchString);        

        $destinos = [];
        $atracoes = [];
        $eventos = [];
        $blogs = [];

        //se a busca não está vazia
		if($v_SearchString != null && strlen(trim($v_SearchString)) > 0)
		{
            $v_SearchString = trim($v_SearchString);


            $v_SearchString = str_replace(['"',"'"],'',$v_SearchString);
            $v_SearchString = slugify($v_SearchString);

            $a_search = explode(' ',$v_SearchString);

            //verifica se há event_words:
            $event_words=['carnaval',
                'carnavais',   
                'cavalgada',
                'comida di buteco',  
                'congresso',
                'congressos',
                'debate',
                'debates',
                'encontro',
                'evento',
                'eventos',
                'exposição',
                'exposicao',
                'feira',
                'feiras',
                'fest',
                'festa',
                'festas',
                'festival',
                'festivais',
                'folia',
                'show',
                'shows',
                'reunião',
                'reuniao',
                'reveillon',
                'reveilon',
                'reveion',
                'reveiom',
                ];
            
                  
            foreach($a_search as $word){
                
                if (strlen($word) >= 3){
                    if (in_array(strtolower($word),$event_words)) {
                        if ($word=='reveion' or $word=='reveiom'){
                            $v_SearchString = str_replace($word,'reveillon',$v_SearchString);
                            
                        }
                        return redirect(url("/pt/eventos/filtro?category_id=0&&city=0&event=".urlencode(preg_replace("/[\=,\&,\?]/", " ", $v_SearchString))));
                        
                    }
                }
            }

            $search = "";
            foreach($a_search as $word){
                if (strlen($word) >= 3){
                    if (strlen($search)>0) $search = $search." ";
                    $search = $search.'+'.$word;
                }
            }

            $exclude = "";
            foreach($a_search as $word){
                if (strlen($word) >= 3){
                    if (strlen($exclude)>0) $exclude = $exclude." ";
                    $exclude = $exclude.'-'.$word;
                }
            }
            //print($search);exit();
            //print($search);

            function destinos ($search, $type="IN BOOLEAN MODE"){
                $rows = DB::select("
                select 
                    distinct d.*, 
                    ph.url
                from  
                    destination d, 
                    destination_photo ph, 
                    destination_hashtag dh, 
                    hashtag h,
                    region r
                where 
                    d.id=ph.destination_id
                    and ph.is_cover = 1
                    and d.publico=1
                    and dh.destination_id=d.id
                    and dh.hashtag_id=h.id
                    and d.region_id=r.id
                    and (
                        MATCH (nome) AGAINST ('$search' $type)
                        or MATCH (h.name) AGAINST ('$search' $type)
                        or MATCH (r.name) AGAINST ('$search' $type)
                        ) 
                    ");

                return $rows;
                            
            }
            function nomes_destinos ($search, $type="IN BOOLEAN MODE"){
                $rows = DB::select("
                select 
                    distinct d.*, 
                    ph.url
                from  
                    destination d, 
                    destination_photo ph
                where 
                    d.id=ph.destination_id
                    and ph.is_cover = 1
                    and d.publico=1
                    and MATCH (nome) AGAINST ('$search' $type) 
                    ");

                return $rows;
                            
            }

            //inicio busca 'rigida'
            $hsearch = '\"'.$v_SearchString.'\"';
            $destinos = destinos($hsearch);
            

            function atracoes($search, $hsearch,$filtracidade='', $type="IN BOOLEAN MODE"){
                if ($type=="IN BOOLEAN MODE"){
                    $excluir = DB::select("select distinct a.*,ph.url, c.name as cidade, c.slug as c_slug
                    from  attraction a left join attraction_hashtag ah on a.id = ah.attraction_id 
                    left join hashtag h on ah.hashtag_id = h.id
                    inner join attraction_photo ph on a.id=ph.attraction_id
                    inner join city c on a.city_id=c.id
                    where ph.is_cover=1
                    and (MATCH (nome_pt,nome_oficial,type_slug,slug_trade,sinonimos,city_name) AGAINST ('$search' IN BOOLEAN MODE) 
                    or MATCH (h.name) AGAINST ('$hsearch' IN BOOLEAN MODE))  
                    and MATCH(antonimos) AGAINST('$search' IN BOOLEAN MODE)
                    $filtracidade
                    ");

                    $eids = "";
                    foreach($excluir as $row){
                        if (strlen($eids)>0) {
                            $eids = $eids.", ".$row->id;
                        } else {
                            $eids = $row->id;
                        }
                    }                
                    
                    if (strlen($eids) > 0){
                        $exclude = "and a.id not in ($eids)";
                    } else {
                        $exclude = "";
                    }

                } else {
                    $exclude = "";
                }

                $rows = DB::select("select distinct a.*,ph.url, c.name as cidade, c.slug as c_slug
                from  attraction a left join attraction_hashtag ah on a.id = ah.attraction_id 
                left join hashtag h on ah.hashtag_id = h.id
                inner join attraction_photo ph on a.id=ph.attraction_id
                inner join city c on a.city_id=c.id
                where ph.is_cover=1
                and (MATCH (nome_pt,nome_oficial,type_slug,slug_trade,sinonimos,city_name) AGAINST ('$search' IN BOOLEAN MODE) 
                or MATCH (h.name) AGAINST ('$hsearch' IN BOOLEAN MODE))  


                order by a.item_inventario desc
                ");
                return $rows;
            }
            
            $filtracidade = '';
            if(count($destinos)==1){
                //print_r($destinos);
                $filtracidade="and a.city_id=".$destinos[0]->city_id;
            }
            $atracoes = atracoes($search, $hsearch,$filtracidade);
            //print_r(count($atracoes));exit();
            function nomes_atracoes($search,$cidades=''){
                $rows = DB::select("select distinct a.*,ph.url, c.name as cidade, c.slug as c_slug
                from  attraction a, attraction_photo ph, city c, attraction_hashtag ah, hashtag h
                where a.id=ph.attraction_id
                and ph.is_cover = 1
                and a.id = ah.attraction_id
                and ah.hashtag_id = h.id
                and a.city_id=c.id
                $cidades
                and (MATCH (nome_pt,nome_oficial,type_slug,slug_trade,sinonimos,city_name) AGAINST ('$search' IN NATURAL LANGUAGE MODE) 
                )  
                ");
                return $rows;
            }
            function blog($search, $hsearch){

                $rows = DB::select("
                select 
                    distinct b.*
                from  
                    blog_article b,
                    blog_article_hashtag bh,
                    hashtag h
                where 
                    b.id = bh.blog_article_id
                    and b.active=1
                    and bh.hashtag_id = h.id
                    and (MATCH (titulo_pt,conteudo) AGAINST ('$search' IN BOOLEAN MODE) 
                     or MATCH (h.name) AGAINST ('$hsearch' IN BOOLEAN MODE))  
                order by
                    created_at
                ");        
                return $rows;        
            }
            $blogs = blog($search,$hsearch);

            function eventos($search,$cidades=''){
                $rows = DB::select("
                select distinct 
                    e.*, 
                    ph.url, 
                    c.name as cidade,
                    c.slug as c_slug,
                    di.inicio, 
                    dt.termino
                from 
                    event e, 
                    event_photo ph, 
                    city c, 
                    event_hashtag eh, 
                    hashtag h, 
                        (select e.id, min(date) as inicio
                        from event_date ed, event e
                        where ed.event_id=e.id
                    group by e.id) as di,
                        (select e.id, max(date) as termino
                        from event_date ed, event e
                        where ed.event_id=e.id
                    group by e.id) as dt
                where ph.event_id = e.id
                    and e.id=di.id
                    and e.id=dt.id
                    and e.city_id = c.id 
                    and e.id = eh.event_id
                    and eh.hashtag_id = h.id
                    and e.revision_status_id=4
                    and ph.is_cover = 1
                    and termino > current_date
                    $cidades
                    and (MATCH (e.nome) AGAINST ('$search'  IN BOOLEAN MODE) 
                        or MATCH (h.name) AGAINST ('$search'  IN BOOLEAN MODE))  ");

                return $rows;

    
            }    
            //eventos
            if(count($destinos)==1){
                //print_r($destinos);
                $filtracidade="and e.city_id=".$destinos[0]->city_id;
                //print($filtracidade);exit();
                $eventos = DB::select("select distinct e.*, ph.url, c.name as cidade,c.slug as c_slug,
                di.inicio, dt.termino
                from event e, event_photo ph, city c, event_hashtag eh, hashtag h, 
                    (select e.id, min(date) as inicio
                    from event_date ed, event e
                    where ed.event_id=e.id
                    group by e.id) as di,
                    (select e.id, max(date) as termino
                    from event_date ed, event e
                    where ed.event_id=e.id
                    group by e.id) as dt
                where ph.event_id = e.id
                and e.id=di.id
                and e.id=dt.id
                and e.city_id = c.id 
                and e.id = eh.event_id
                and eh.hashtag_id = h.id
                and e.revision_status_id=4
                and ph.is_cover = 1
                and termino > current_date
                $filtracidade");

            } else {
                $eventos = eventos($search);
            }
        }
        function outras_atracoes($city_id){
            return DB::select("select distinct a.*,ph.url, c.name as cidade, c.slug as c_slug
            from  attraction a, attraction_photo ph, city c, attraction_hashtag ah, hashtag h
            where a.id=ph.attraction_id
            and ph.is_cover = 1
            and a.id = ah.attraction_id
            and ah.hashtag_id = h.id
            and a.city_id=c.id
            and a.city_id = $city_id
            limit 12");
        }
        function outros_eventos($cidades, $not_in=''){
            return DB::select("select distinct e.*, ph.url, c.name as cidade,c.slug as c_slug,
            di.inicio, dt.termino
            from event e, event_photo ph, city c, event_hashtag eh, hashtag h, 
                (select e.id, min(date) as inicio
                from event_date ed, event e
                where ed.event_id=e.id
                group by e.id) as di,
                (select e.id, max(date) as termino
                from event_date ed, event e
                where ed.event_id=e.id
                group by e.id) as dt
            where ph.event_id = e.id
            and e.id=di.id
            and e.id=dt.id
            and e.city_id = c.id 
            and e.id = eh.event_id
            and eh.hashtag_id = h.id
            and e.revision_status_id=4
            and ph.is_cover = 1
            and termino > current_date
            $cidades
            $not_in
            order by di.inicio
            limit 12");
        }
        $mais_atracoes = null;
        $mais_eventos = null;
        if(count($destinos)==0){
            if (count($atracoes)==1 & count($eventos)==0){
                $mais_eventos = outros_eventos('and e.city_id in ('.$atracoes[0]->city_id.') ');
                $mais_atracoes = outras_atracoes($atracoes[0]->city_id); 
            } elseif (count($eventos)==1 & count($atracoes)==0){
                $mais_atracoes = outras_atracoes($eventos[0]->city_id); 
                $mais_eventos = outros_eventos('and e.city_id in ('.$eventos[0]->city_id.') ');
            } elseif (count($eventos)==1 & count($atracoes)==1){
                if ($eventos[0]->city_id == $atracoes[0]->city_id){
                    $mais_atracoes = outras_atracoes($atracoes[0]->city_id);
                    $mais_eventos = outros_eventos('and e.city_id in ('.$eventos[0]->city_id.') ');
                    
                }
            } elseif (count($destinos)==0 
                    & count($atracoes)==0 
                    & count($eventos)==0 
                    & count($blogs)==0
                    ) {
                //busca em modo permissivo
                $hsearch = $v_SearchString;
                $search = $v_SearchString;
                $a_search = explode(' ',$search);
            
                $cities = '';
                $e_cidades = '';
                $a_cidades = '';
                foreach ($a_search as $destino){
                    $destinos = nomes_destinos($destino);
                    if (count($destinos)){
                        
                        $ids = '';
                        foreach($destinos as $row){
                            if (strlen($ids)>0){
                                $ids = $ids.', '.$row->city_id;
                            } else{
                                $ids = $ids.$row->city_id;
                            }

                        }
                        $e_cidades = 'and e.city_id in ('.$ids.')';
                        $a_cidades = 'and a.city_id in ('.$ids.')';
                        break;    

                    }
                }

                $eventos = eventos($search,$e_cidades);
                $ids = '';
                foreach($eventos as $row){
                    if (strlen($ids)>0){
                        $ids = $ids.', '.$row->id;
                    } else{
                        $ids = $ids.$row->id;
                    }

                }
                $e_not_in = 'and e.id not in ('.$ids.')';
                //print($e_not_in);exit();
                //$mais_eventos = outros_eventos($e_cidades,$e_not_in);
                $atracoes = nomes_atracoes($search,$a_cidades);

            }
        }    
        
    
                    
        

        return view('public.home.busca',
                    compact('atracoes',
                    'destinos',
                    'eventos',
                    'blogs',
                    'mais_atracoes',
                    'mais_eventos')
                    )->with(['p_SearchString' => $v_SearchString,
                            'tipo'=>Type::getList(),
                            'p_Search' => true,
                            'p_Language' => $p_Lang]);

        
    }


	public function getAllAttractions($p_Lang, $p_Slug){
		\App::setLocale($p_Lang);

        $v_Destination = Destination::getDestination($p_Slug);
        $v_Attractions = Attraction::getCityMainAttractions($v_Destination->city_id, 1000000);

        return view('public.searchResults')->with(['p_SearchString' => $v_Destination->nome,
                                                   'p_Destinations' => [],
                                                   'p_TouristicRoutes' => [],
                                                   'p_Attractions' => $v_Attractions,
                                                   'p_Accomodations' => [],
                                                   'p_FoodDrinks' => [],
                                                   'p_Services' => [],
                                                   'p_Recreations' => [],
                                                   'p_AccomodationList' => [],
                                                   'p_FoodDrinkList' => [],
                                                   'p_ServiceList' => [],
                                                   'p_RecreationList' => [],
                                                   'p_MoreResultsPage' => true,
                                                   'p_Search' => false,
                                                   'p_Language' => $p_Lang]);
	}

    public function getContact($p_Lang)
    {
        \App::setLocale($p_Lang);

        return view('public.contact')->with(['p_Content' => Parameter::getParameterByKey('fale-conosco_' . $p_Lang),
                                             'p_Subjects' => ContactSubject::getList($p_Lang),
                                             'p_States' => ContactCity::getStateList(),
                                             'p_Language' => $p_Lang]);
    }

    public function getContactCities($p_Lang, $p_State)
    {
        \App::setLocale($p_Lang);

        return ContactCity::getCitiesByState($p_State);
    }

    public function postContact($p_Lang)
    {
        $v_ClientIp = Request::getClientIp();
        if(Contact::invalid($v_ClientIp))
            return redirect((url($p_Lang . '/fale-conosco?tentativas-excessivas')));
        $v_Validator = Validator::make(Input::all(), Contact::$m_Rules);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Name = Input::get('nome');
        $v_Email = Input::get('email');
        $v_SubjectId = Input::get('assunto');
        $v_Msg = Input::get('mensagem');
        $v_City = Input::get('cidade');

        Contact::post($v_Name, $v_Email, $v_SubjectId, $v_Msg, $v_City, $v_ClientIp);
        return redirect(url($p_Lang . '/fale-conosco?mensagem-enviada'));
    }

    public function getMinas360($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_CategoryFilter = Input::get('q');
        $v_TripTypeIds = $v_CategoryFilter ? TripType::getCategoryTypesBySlugs($v_CategoryFilter) : null;

        $v_Destinations = Destination::get360Destinations($v_TripTypeIds);
        foreach($v_Destinations as $c_Item)
            $c_Item->destino = 1;
        $v_Attractions = Attraction::get360Attractions($v_TripTypeIds);
        $v_TouristicRoutes = TouristicRoute::get360Routes($v_TripTypeIds);

        $v_Places = array_merge($v_Destinations->toArray(), $v_Attractions->toArray(), $v_TouristicRoutes->toArray());
        shuffle($v_Places);

        return view('public.threeHundredAndSixty')->with(['p_Categories' => TripCategory::getCategories(100),
                                                          'p_CategoryFilter' => $v_CategoryFilter,
                                                          'p_Places' => $v_Places,
                                                          'p_Language' => $p_Lang]);
    }

    public function getDonateMedia($p_Lang)
    {
        \App::setLocale($p_Lang);
        return view('public.donateMedia')->with(['p_Content' => ItemsPageDescriptions::getByLanguage($p_Lang, 'doacao-de-midias'),
                                                 'p_Cities' => ['' => ''] + City::getList(),
                                                 'p_Language' => $p_Lang]);
    }

    public function postDonatedMedia($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Validator = Validator::make(Input::get('doacao'), DonatedMedia::$m_Rules);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_File = Input::hasFile('arquivo') ? Input::file('arquivo') : null;
        if ($v_File == [])
            return redirect()->back()->withInput()->with('error_message', trans('donateMedia.file_required'));

        DonatedMedia::post(Input::get('doacao'), $v_File);
        return redirect()->back()->with('message', trans('donateMedia.success'));
    }


    public function subscribeToNewsletter($p_Lang)
    {
        \App::setLocale($p_Lang);
        $v_Validator = Validator::make(Input::all(), Newsletter::$m_Rules);
        $v_Email = Input::get('email');
        if($v_Validator->fails())
        {
            if(Newsletter::where('email', $v_Email))
                return [0, trans('menu.registered_email')];
            else
                return [0, trans('menu.invalid_email')];
        }

        Newsletter::post($v_Email);
        return [1, trans('menu.subscription_success')];
    }

    public function unsubscribeToNewsletter($p_Lang, $p_Email)
    {
        \App::setLocale($p_Lang);
	    //TODO revisar
        $v_Validator = Validator::make(['email' => $p_Email], Newsletter::$m_UnsubscribeRules);
        if($v_Validator->fails())
            return redirect()->back()->withInput()->withErrors($v_Validator);

        $v_Email = Input::get('email');
        Newsletter::unsubscribe($v_Email);
        return redirect(url($p_Lang))->with('message', trans('menu.unsubscription_success'));
    }

    public function getDestinationSupport($p_Lang, $p_Slug)
    {
        \App::setLocale($p_Lang);
        $v_Destination = Destination::getDestination($p_Slug);
        $v_AttractionType = Input::get('tipo_atrativo');
        $v_AccomodationType = Input::get('tipo_acomodacao');
        $v_FoodDrinkType = Input::get('tipo_estabelecimento');
        $v_ServiceType = Input::get('tipo_servico');
        $v_RecreationType = Input::get('tipo_lazer');
        $v_FilterType = '';
        $v_FilterValue = '';
        if(!is_null($v_AttractionType)) {
            $v_FilterType = 'atrativo';
            $v_FilterValue = $v_AttractionType;
        }
        else if(!is_null($v_AccomodationType)) {
            $v_FilterType = 'acomodacao';
            $v_FilterValue = $v_AccomodationType;
            $v_AccomodationType = explode(',', $v_AccomodationType);
        }
        else if(!is_null($v_FoodDrinkType)) {
            $v_FilterType = 'estabelecimento';
            $v_FilterValue = $v_FoodDrinkType;
            $v_FoodDrinkType = explode(',', $v_FoodDrinkType);
        }
        else if(!is_null($v_ServiceType)) {
            $v_FilterType = 'servico';
            $v_FilterValue = $v_ServiceType;
            $v_ServiceType = explode(',', $v_ServiceType);
        }
        else if(!is_null($v_RecreationType)) {
            $v_FilterType = 'lazer';
            $v_FilterValue = $v_RecreationType;
            $v_RecreationType = explode(',', $v_RecreationType);
        }

        return view('public.destinationSupport')->with(['p_Destination' => $v_Destination,
                                                        'p_PhotoGallery' => DestinationPhoto::getPhotoGallery($v_Destination->id, true),
                                                        'p_DestinationList' => Destination::getSlugList(),
                                                        'p_FilterType' => $v_FilterType,
                                                        'p_FilterValue' => $v_FilterValue,
                                                        'p_Attractions' => Attraction::getAttractionByCityAndType($v_Destination->city_id, $v_AttractionType),
                                                        'p_Accomodations' => Attraction::getTradeByCityAndType($v_Destination->city_id, ['B1'], $v_AccomodationType),
                                                        'p_FoodDrinks' => Attraction::getTradeByCityAndType($v_Destination->city_id, ['B2'], $v_FoodDrinkType),
                                                        'p_Services' => Attraction::getTradeByCityAndType($v_Destination->city_id, ['B3', 'B4'], $v_ServiceType),
                                                        'p_Recreations' => Attraction::getTradeByCityAndType($v_Destination->city_id, ['B6'], $v_RecreationType),
                                                        'p_AccomodationList' => Type::getPublicInventoryTypeListByName('B1', $p_Lang),
                                                        'p_FoodDrinkList' => Type::getPublicInventoryTypeListByName('B2', $p_Lang),
                                                        'p_ServiceList' => Type::getPublicInventoryTypeServiceList($p_Lang),
                                                        'p_RecreationList' => Type::getPublicInventoryTypeListByName('B6', $p_Lang),
                                                        'p_Language' => $p_Lang]);
    }

    public function teste()
    {
        return view('admin.reports.inventory');
        $v_Cronograma = null;
        \Excel::create('Relatorio', function($excel) use($v_Cronograma) {
            $excel->sheet('Relatorio', function($sheet) use($v_Cronograma) {
                $sheet->loadView('admin.reports.inventory', ['p_Cronograma' => $v_Cronograma]);
            });
        })->download('xls');
    }
}

